# Phobos API

Phobos is a release management and deployment orchestration tool.

## Get started

Instructions on building a binary from source can be found [here](https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-docs/-/blob/main/docs/setup/api/install.md).

## Installing locally (Requires Docker)

In order to use Phobos locally, a `docker-compose.yml` file is available under the `docker-compose` directory. The use of this requires the installation of Docker. Learn more [here](https://docs.docker.com/get-docker/).

    1. Clone the repository and change to 'docker-compose' directory.
    2. Use 'docker compose up' command to launch Phobos using Docker. We could additionally, pass in a `-d` flag to the command to launch everything in the background like so 'docker compose up -d'.

At this point, Docker should begin pulling down all the images needed to provision Phobos. Once this is complete, the Phobos UI will be available at `http://localhost:9001/`.

When first going to `http://localhost:9001/`, the [KeyCloak](https://www.keycloak.org/) login screen will display, which prompts you to create a profile using the KeyCloak identity provider (IDP). Use `martian` for both the username and password to complete authentication.

Now the Phobos CLI can be used to interact with Phobos. Use the following commands to create a profile named `local`, which points to the API endpoint and then logs in to Phobos via `local`:

```bash
phobos configure --profile local --http-endpoint http://localhost:9000
phobos -p local sso login
```

**Congratulations! The Phobos CLI is now ready to issue commands, use the `-h` flag for more info!**

## Running Phobos API from local source

In order to run Phobos API from local source we suggest you do the following:

    1. Stop the phobos-api docker container if it is running.
    2. Copy the `env.example` file in the root folder and paste it as `.env`.
    3. Open the Phobos API folder in Visual Studio Code.
    4. Install the recommended extensions.
    5. Click the `Run and Debug` menu on the left hand side of Visual Studio Code.
    6. Click the Start Debugging button next to Launch API.

At this point you can interact with the Phobos UI at `http://localhost:9001/` and it will be communicating with your local Phobos API.

## Documentation

- Phobos API documentation is available at https://gitlab.com/infor-cloud/martian-cloud/phobos/phobos-docs/-/blob/main/docs.

## Security

If you've discovered a security vulnerability in the Phobos API, please let us know by creating a **confidential** issue in this project.

## Statement of support

Please submit any bugs or feature requests for Phobos. Of course, MR's are even better. :)

## License

Phobos API is distributed under [Mozilla Public License v2.0](https://www.mozilla.org/en-US/MPL/2.0/).
