#!/bin/sh

# This script is used by the job dispatcher to copy files to the custom container.

# Exit on error
set -e

VOLUME_DIR=/phobos-data

# Volume should be mounted to /phobos-data before running this script.
if [ ! -d $VOLUME_DIR ]; then
  echo "Data directory $VOLUME_DIR does not exist. Please ensure data volume is mounted to $VOLUME_DIR and required files are present."
  exit 1
fi

# These will need to created on some unix distros.
# Phobos CLI will need to be in the PATH and the CA certificates file will need to be in the right place.
mkdir -p /etc/ssl/certs /usr/local/bin

# Move the files to the right place.
mv $VOLUME_DIR/phobos /usr/local/bin
mv $VOLUME_DIR/ca-certificates.crt /etc/ssl/certs
mv $VOLUME_DIR/job .

# Make the binaries executable.
chmod +x /usr/local/bin/phobos job

# Start the job.
exec "$@"
