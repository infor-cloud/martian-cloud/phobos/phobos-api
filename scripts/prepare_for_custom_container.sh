#!/bin/sh

# This script is used by the job dispatcher to prepare for custom container.

# Exit on error
set -e

# Our "volume". Data directory must be created before this script is run with the "phobos" user as the owner.
VOLUME_DIR=/phobos-data

# Files we'll be sharing with the custom container.
FILES_TO_COPY="/app/job /usr/local/bin/phobos /etc/ssl/certs/ca-certificates.crt custom_container_entrypoint.sh"

# The user executing this script won't have permissions to create the data directory.
if [ ! -d $VOLUME_DIR ]; then
  echo "Data directory $VOLUME_DIR does not exist. Please ensure the data directory exists and is owned by the 'phobos' user"
  exit 1
fi

# Copy the files to the data directory.
cp -t $VOLUME_DIR $FILES_TO_COPY
