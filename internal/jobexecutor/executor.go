// Package jobexecutor provides the functionality to execute a job
package jobexecutor

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/hcl/v2"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/json"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/volume"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// waitCancelError is the duration to sleep if cancel wait gets error
	waitCancelError = 10 * time.Second

	// waitForcedCancel is the duration to sleep between polls looking for forced cancel.
	waitForcedCancel = 30 * time.Second

	// jobCancellationSubscriptionTimeout is the context timeout used for the job cancellation subscription.
	jobCancellationSubscriptionTimeout = time.Minute

	// defaultVolumeSizeLimit is the volume size limit used when nothing
	// is set on the job config.
	defaultVolumeSizeLimit = 1024 * 1024 * 5 // 5Mib
)

var (
	// ProhibitedEnvironmentVariables is a list of environment variables that cannot
	// be set on the environment.
	prohibitedEnvironmentVariables = map[string]struct{}{
		"PHOBOS_REATTACH_PROVIDERS":   {},
		"PHOBOS_DISABLE_PLUGIN_TLS":   {},
		"PHOBOS_SKIP_PROVIDER_VERIFY": {},
	}
)

type logSink struct {
	logger logger.Logger
	client client.Client
	jobID  string
}

func (l *logSink) Write(ctx context.Context, startOffset int32, buffer []byte) error {
	l.logger.Infof("SENDING OUTPUT: %s", string(buffer))
	return l.client.WriteLogs(ctx, l.jobID, startOffset, buffer)
}

// JobConfig is used to configure the job
type JobConfig struct {
	JobID           string
	JobToken        string
	HTTPEndpoint    string
	VolumeSizeLimit int
}

// JobExecutor executes a job
type JobExecutor struct {
	cfg             *JobConfig
	client          client.Client
	logger          logger.Logger
	logStreamWriter *logstream.Writer
	cancellableCtx  context.Context
	cancelFunc      context.CancelFunc
}

// NewJobExecutor creates a new JobExecutor
func NewJobExecutor(
	ctx context.Context,
	cfg *JobConfig,
	client client.Client,
	logger logger.Logger,
) (*JobExecutor, error) {
	logWriter, err := logstream.NewWriter(&logSink{jobID: cfg.JobID, client: client, logger: logger}, logger)
	if err != nil {
		return nil, fmt.Errorf("failed to create log stream writer: %w", err)
	}

	if cfg.VolumeSizeLimit <= 0 {
		// Nothing is set, so use default volume size limit.
		cfg.VolumeSizeLimit = defaultVolumeSizeLimit
	}

	ctx, cancel := context.WithCancel(ctx)

	return &JobExecutor{cfg, client, logger, logWriter, ctx, cancel}, nil
}

// Execute executes the job associated with the JobExecutor instance
func (j *JobExecutor) Execute(ctx context.Context) error {
	j.logStreamWriter.Start()

	// Create terminal UI
	ui := terminal.NonInteractiveUI(ctx, j.logStreamWriter)

	// Add a defer to handle any panics that may occur during job execution
	defer func() {
		if rErr := recover(); rErr != nil {
			j.handleJobFailure(ctx, ui, fmt.Errorf("an unexpected error occurred: %v", rErr))
		}
	}()

	if _, err := j.client.SetJobStatus(ctx, j.cfg.JobID, string(models.JobRunning)); err != nil {
		if status.Code(err) == codes.InvalidArgument {
			// Invalid argument means the job was already canceled
			j.cancelFunc()
			j.handleJobCanceled(ctx, ui)
		} else {
			j.handleJobFailure(ctx, ui, err)
		}

		return fmt.Errorf("failed to update job status to running: %w", err)
	}

	pluginLogger := hclog.New(&hclog.LoggerOptions{
		Name:   "phobos",
		Level:  hclog.LevelFromString("ERROR"),
		Color:  hclog.ForceColor,
		Output: j.logStreamWriter,
	})

	if err := j.execute(ctx, ui, pluginLogger); err != nil {
		if j.cancellableCtx.Err() != nil || status.Code(err) == codes.Canceled {
			j.handleJobCanceled(ctx, ui)
		} else {
			j.handleJobFailure(ctx, ui, err)
		}

		// return nil here since a job failure should not be considered an error
		return nil
	}

	// Flush all logs
	j.logStreamWriter.Close()

	if _, err := j.client.SetJobStatus(ctx, j.cfg.JobID, string(models.JobSucceeded)); err != nil {
		j.logger.Errorf("failed to update job status to succeeded: %v", err)
		return err
	}

	return nil
}

func (j *JobExecutor) handleJobFailure(ctx context.Context, ui terminal.UI, err error) {
	ui.Output("Job failed: %v", err, terminal.WithErrorStyle())

	// Flush all logs
	j.logStreamWriter.Close()

	if _, err := j.client.SetJobStatus(ctx, j.cfg.JobID, string(models.JobFailed)); err != nil {
		j.logger.Errorf("failed to update job status to %s: %v", models.JobFailed, err)
	}
}

func (j *JobExecutor) handleJobCanceled(ctx context.Context, ui terminal.UI) {
	ui.Output("Job gracefully canceled while in progress", terminal.WithWarningStyle())

	// Flush all logs
	j.logStreamWriter.Close()

	if _, err := j.client.SetJobStatus(ctx, j.cfg.JobID, string(models.JobCanceled)); err != nil {
		j.logger.Errorf("failed to update job status to %s: %v", models.JobCanceled, err)
	}
}

func (j *JobExecutor) execute(ctx context.Context, ui terminal.UI, pluginLogger hclog.Logger) error {
	j.logger.Infof("Querying for job %s", j.cfg.JobID)

	job, err := j.client.GetJob(ctx, j.cfg.JobID)
	if err != nil {
		return fmt.Errorf("failed to get job: %w", err)
	}

	if job.Status == string(models.JobCanceling) {
		j.cancelFunc()
		return errors.New("job cancelled")
	}

	jobData, ok := job.Data.(*pb.Job_TaskData)
	if !ok {
		return fmt.Errorf("invalid job type received: %s", job.Type)
	}
	pipelineID := jobData.TaskData.PipelineId
	taskPath := jobData.TaskData.TaskPath

	ui.Output("Executing task %s...", taskPath)

	pluginmanager := plugin.New(pluginLogger)
	defer pluginmanager.Close()

	mountManager, err := volume.NewMountManager(j.logger, ui, j.client, j.cfg.VolumeSizeLimit)
	if err != nil {
		return fmt.Errorf("failed to initialize volume mount manager: %v", err)
	}

	pluginInstaller, err := plugin.NewInstaller(pluginLogger, j.cfg.HTTPEndpoint)
	if err != nil {
		return fmt.Errorf("failed to create plugin installer: %w", err)
	}

	j.startCancellationMonitor(ctx, ui, job.MaxJobDuration)

	if job.Image != nil {
		ui.Output("Running on image %s", *job.Image)
	}

	pipeline, err := j.client.GetPipeline(j.cancellableCtx, pipelineID)
	if err != nil {
		return fmt.Errorf("failed to get pipeline: %w", err)
	}

	task, ok := getTaskFromPipeline(pipeline, taskPath)
	if !ok {
		return fmt.Errorf("the pipeline does not contain a task with path %s", taskPath)
	}

	variables, err := j.client.GetPipelineVariables(j.cancellableCtx, pipelineID)
	if err != nil {
		return fmt.Errorf("failed to get pipeline variables: %w", err)
	}

	ui.Output("Resolved pipeline variables")

	// Convert HCL variables to map for easier processing.
	hclVariables := map[string]string{}
	for _, variable := range variables.HclVariables {
		hclVariables[variable.Key] = variable.GetValue()
	}

	rawHCL, err := j.client.GetPipelineTemplate(j.cancellableCtx, pipeline.PipelineTemplateId)
	if err != nil {
		return fmt.Errorf("failed to get pipeline template: %w", err)
	}

	hclConfig, _, err := config.NewPipelineTemplate(bytes.NewReader(rawHCL), hclVariables)
	if err != nil {
		return err
	}

	hclTask, ok := hclConfig.GetTask(taskPath)
	if !ok {
		return fmt.Errorf("task with path %s not found in hcl config", taskPath)
	}

	jwtExpiration := time.Now().Add(time.Duration(job.MaxJobDuration) * time.Minute)

	jwtMap := map[string]cty.Value{}
	for _, jwtHCL := range hclConfig.JSONWebTokens {
		// Create tokens
		token, jErr := j.client.CreatePipelineJWT(j.cancellableCtx, pipelineID, jwtHCL.Audience, jwtExpiration)
		if jErr != nil {
			return fmt.Errorf("failed to create pipeline task token: %w", jErr)
		}
		jwtMap[jwtHCL.Name] = cty.StringVal(token)
	}

	vcsTokenMap := map[string]cty.Value{}
	for _, vcsTokenHCL := range hclConfig.VCSTokens {
		tokenRenewer, jErr := newVCSTokenRenewer(j.client, j.logger, vcsTokenHCL.ProviderID, pipelineID)
		if jErr != nil {
			return fmt.Errorf("failed to create VCS token setter: %w", jErr)
		}
		defer tokenRenewer.cleanup()

		if jErr := tokenRenewer.start(j.cancellableCtx); jErr != nil {
			return fmt.Errorf("failed to set VCS token file: %w", jErr)
		}

		token, tokenFilePath := tokenRenewer.getTokenInfo()
		vcsTokenMap[vcsTokenHCL.Name] = cty.ObjectVal(map[string]cty.Value{
			"value":     cty.StringVal(token),
			"file_path": cty.StringVal(tokenFilePath),
		})
	}

	sm, err := hclConfig.BuildStateMachine()
	if err != nil {
		return err
	}

	// Set environment variables
	if err = j.setEnvironmentVariables(ui, variables.EnvironmentVariables); err != nil {
		return fmt.Errorf("failed to set environment variables: %w", err)
	}

	// Load HCL variables to pass to the HCL context
	contextVariables, err := j.createCtyVariables(ui, hclConfig.GetVariableBodies(), hclVariables)
	if err != nil {
		return fmt.Errorf("failed to load hcl variables: %w", err)
	}

	// Create HCL context
	evalCtx := hclctx.New(map[string]cty.Value{
		"var":       cty.ObjectVal(contextVariables),
		"jwt":       cty.ObjectVal(jwtMap),
		"vcs_token": cty.ObjectVal(vcsTokenMap),
	})

	if len(task.Actions) > 0 {
		pluginWorkingDir, err := os.MkdirTemp("", "plugin-work-dir-*")
		if err != nil {
			return fmt.Errorf("failed to create temporary working dir: %v", err)
		}

		defer func() {
			if rErr := os.RemoveAll(pluginWorkingDir); rErr != nil {
				j.logger.Errorf("failed to remove temporary working dir: %v", rErr)
			}
		}()

		// Now that plugins have been initialized, mount any required volume.
		if len(hclConfig.Volumes) > 0 {
			ui.Output("Mounting volumes...")

			for _, mountPoint := range hclTask.MountPoints {
				volumeToMount, ok := hclConfig.GetVolume(mountPoint.Volume)
				if !ok {
					return fmt.Errorf("failed to get volume in hcl config %q", mountPoint.Volume)
				}

				// Get the instance.
				mounter, gErr := mountManager.GetMounter(volume.Type(volumeToMount.Type))
				if gErr != nil {
					return gErr
				}

				// Make sure any volumes mounts are cleaned up afterwards.
				defer func() {
					if cErr := mounter.Close(); cErr != nil {
						j.logger.Errorf("failed to close volume mounter: %v", cErr)
					}
				}()

				// Mount the volume.
				if err = mounter.MountVolume(j.cancellableCtx, volumeToMount, mountPoint, pluginWorkingDir); err != nil {
					return fmt.Errorf("failed to mount volume %q: %v", volumeToMount.Name, err)
				}
			}

			ui.Output("Volumes have been successfully mounted!")
		}

		ui.Output("Initializing plugins...")

		pluginRequirements := map[string]config.PluginRequirement{}
		if hclConfig.PluginRequirements != nil {
			pluginRequirements, err = hclConfig.PluginRequirements.Requirements()
			if err != nil {
				return err
			}
		}

		builtInPluginVersion, err := pluginmanager.GetVersionForBuiltInPlugins(j.cancellableCtx)
		if err != nil {
			return fmt.Errorf("failed to get builtin plugin version from plugin manager: %w", err)
		}

		for _, p := range hclConfig.Plugins {
			launchPluginOpts := []plugin.LaunchOption{
				plugin.WithDir(pluginWorkingDir),
			}

			// For built-in plugins the source is just the plugin's name.
			pluginSource := p.Name

			requirement, required := pluginRequirements[p.Name]
			if required {
				source, err := plugin.ParseSource(requirement.Source, &j.cfg.HTTPEndpoint)
				if err != nil {
					return fmt.Errorf("failed to parse plugin source %s: %w", requirement.Source, err)
				}

				// For any required plugins, the source is the fully qualified representation.
				pluginSource = source.String()

				// Handle local plugin.
				if requirement.IsLocal() {
					// No installation needed for a local plugin.
					// Pass the value of the 'replace' option (i.e. where the plugin binary is located on the filesystem).
					launchPluginOpts = append(launchPluginOpts, plugin.WithBinaryPath(*requirement.Replace))
				} else {
					// Handle remote plugin.
					ui.Output("- Installing %s plugin...", p.Name)

					// Ensure latest version meeting constraints (if any).
					version, err := pluginInstaller.EnsureLatestVersion(
						j.cancellableCtx,
						source.String(),
						plugin.WithConstraints(requirement.Version),
						plugin.WithToken(&j.cfg.JobToken),
					)
					if err != nil {
						return err
					}

					// Install plugin
					if err := pluginInstaller.InstallPlugin(
						j.cancellableCtx,
						source.String(),
						version,
						plugin.WithToken(&j.cfg.JobToken),
					); err != nil {
						return err
					}

					launchPluginOpts = append(launchPluginOpts, plugin.WithVersion(version))

					ui.Output("- Installed %s plugin with version %s", p.Name, version)
				}
			}

			// Log the version for built-in plugins.
			if pluginSource == p.Name {
				ui.Output("- Using %s plugin with version %s", p.Name, builtInPluginVersion)
			}

			if err := pluginmanager.LaunchPlugin(ctx, pluginSource, launchPluginOpts...); err != nil {
				return fmt.Errorf("failed to launch plugin %s: %w", p.Name, err)
			}
		}

		// Start go-routine to shutdown plugins if context is cancelled
		go func() {
			// Wait for context cancellation
			<-j.cancellableCtx.Done()

			// Gracefully shutdown plugins
			for _, p := range pluginmanager.GetPlugins() {
				if err := p.GracefulShutdown(ctx); err != nil {
					if status.Code(err) != codes.Canceled {
						ui.Output("failed to gracefully shutdown plugin: %v", err, terminal.WithErrorStyle())
					}
				}
			}
		}()

		for _, p := range hclConfig.Plugins {
			pluginInstance, err := pluginmanager.GetPlugin(p.Name)
			if err != nil {
				return fmt.Errorf("failed to get plugin from plugin manager %s: %w", p.Name, err)
			}

			diag := pluginInstance.Configure(ctx, p.Body, evalCtx.Context())
			if diag.HasErrors() {
				return fmt.Errorf("failed to configure plugin %v", diag.Error())
			}
			ui.Output("- Initialized %s plugin", p.Name)
		}

		ui.Output("Plugins have been successfully initialized!")

		actions := task.Actions
		for i, action := range actions {
			ui.Output("Executing action %s (%d of %d actions)...", action.Name, i+1, len(actions), terminal.WithHeaderStyle())

			// Set action to in progress
			if err := j.client.SetPipelineActionStatus(ctx, pipelineID, action.Path, statemachine.RunningNodeStatus); err != nil {
				return fmt.Errorf("failed to set action status to running for action %s: %w", action.Path, err)
			}

			// Get traversal context for action which allows us to get the available action outputs up until this point
			traversalCtx, err := sm.GetTraversalContextForNode(action.Path)
			if err != nil {
				return err
			}

			// Get the outputs for the visited actions
			actionPaths := []string{}
			for _, node := range traversalCtx.VisitedActions() {
				actionPaths = append(actionPaths, node.Path())
			}

			availableOutputs, err := j.getOutputsForActions(j.cancellableCtx, pipelineID, actionPaths)
			if err != nil {
				return fmt.Errorf("failed to get available outputs for actions: %w", err)
			}

			hclAction, found := hclConfig.GetAction(action.Path)
			if !found {
				return fmt.Errorf("action with path %s not found in hcl config", action.Path)
			}

			plugin, err := pluginmanager.GetPlugin(hclAction.PluginName())
			if err != nil {
				return fmt.Errorf("failed to get plugin from plugin manager %s: %w", hclAction.PluginName(), err)
			}

			outputs, actionErr := plugin.ExecuteAction(ctx, hclAction.PluginActionName(), hclAction.Remain, evalCtx.NewActionContext(availableOutputs).Context(), ui)
			if actionErr != nil {
				if statusErr := j.client.SetPipelineActionStatus(ctx, pipelineID, action.Path, statemachine.FailedNodeStatus); statusErr != nil {
					return fmt.Errorf("failed to set action status to failed for action %s: %w", action.Path, statusErr)
				}

				return fmt.Errorf("failed to execute action %s: %w", action.Name, actionErr)
			}

			if err := j.client.SetPipelineActionOutputs(ctx, pipelineID, action.Path, outputs); err != nil {
				return fmt.Errorf("failed to set action outputs for action %s: %w", action.Path, err)
			}

			// Check if action was cancelled while in progress
			if j.cancellableCtx.Err() != nil {
				if statusErr := j.client.SetPipelineActionStatus(ctx, pipelineID, action.Path, statemachine.CanceledNodeStatus); statusErr != nil {
					return fmt.Errorf("failed to set action status to failed for action %s: %w", action.Path, statusErr)
				}

				return fmt.Errorf("action %s was canceled while in progress: %w", action.Name, actionErr)
			}

			if err := j.client.SetPipelineActionStatus(ctx, pipelineID, action.Path, statemachine.SucceededNodeStatus); err != nil {
				return fmt.Errorf("failed to set action status to succeeded for action %s: %w", action.Path, err)
			}

			ui.Output("\nAction %s has been successfully executed!", action.Path, terminal.WithSuccessStyle())
		}
	}

	// Check success condition if it's defined
	if task.HasSuccessCondition {
		r := hclTask.SuccessCondition.Range()
		expression := string(rawHCL[r.Start.Byte:r.End.Byte])
		ui.Output("Evaluating task success expression:", terminal.WithHeaderStyle())
		ui.Output("${%s}\n", expression, terminal.WithInfoStyle())

		traversalCtx, err := sm.GetTraversalContextForNode(taskPath)
		if err != nil {
			return err
		}

		if err = j.evaluateTaskSuccessCondition(pipelineID, hclTask.SuccessCondition, traversalCtx, evalCtx); err != nil {
			return err
		}
	}

	ui.Output("\nTask %s has been successfully executed!", taskPath, terminal.WithSuccessStyle())

	return nil
}

func (j *JobExecutor) evaluateTaskSuccessCondition(
	pipelineID string,
	successCondition hcl.Expression,
	traversalCtx *statemachine.TraversalContext,
	evalCtx *hclctx.EvalContext,
) error {
	// Get the outputs for the visited actions
	actionPaths := []string{}
	for _, node := range traversalCtx.VisitedActions() {
		actionPaths = append(actionPaths, node.Path())
	}

	availableOutputs, err := j.getOutputsForActions(j.cancellableCtx, pipelineID, actionPaths)
	if err != nil {
		return fmt.Errorf("failed to get available outputs for actions: %w", err)
	}

	// Evaluate success condition
	val, diag := successCondition.Value(evalCtx.NewActionContext(availableOutputs).Context())
	if diag.HasErrors() {
		return fmt.Errorf("failed to evaluate success condition: %v", diag.Error())
	}

	if !val.IsNull() {
		if val.Type() != cty.Bool {
			return fmt.Errorf("task success condition must evaluate to a boolean, got %s", val.Type().FriendlyName())
		}

		if !val.True() {
			return errors.New("task success condition evaluated to false")
		}
	}

	return nil
}

func (j *JobExecutor) getOutputsForActions(ctx context.Context, pipelineID string, actionPaths []string) (map[string]map[string]cty.Value, error) {
	response := map[string]map[string]cty.Value{}

	if len(actionPaths) > 0 {
		actions, err := j.client.GetPipelineActionOutputs(ctx, pipelineID, actionPaths)
		if err != nil {
			return nil, err
		}

		for _, action := range actions {
			for _, output := range action.Outputs {
				if _, ok := response[action.ActionPath]; !ok {
					response[action.ActionPath] = map[string]cty.Value{}
				}

				typ, err := json.UnmarshalType(output.Type)
				if err != nil {
					return nil, fmt.Errorf("failed to unmarshal type for output %s: %w", output.Name, err)
				}

				val, err := json.Unmarshal(output.Value, typ)
				if err != nil {
					return nil, fmt.Errorf("failed to unmarshal value for output %s: %w", output.Name, err)
				}

				response[action.ActionPath][output.Name] = val
			}
		}
	}

	return response, nil
}

// createCtyVariables creates the HCL variables from the pipeline variables
// and returns a map of variable names and their values.
func (*JobExecutor) createCtyVariables(
	ui terminal.UI,
	hclVariables map[string]hcl.Body,
	variableValues map[string]string,
) (map[string]cty.Value, error) {
	contextVariables, err := variable.GetCtyValuesForVariables(hclVariables, variableValues, false)
	if err != nil {
		return nil, err
	}

	// Log a warning for any HCL variables that have a value but are not defined in the HCL config.
	for k := range variableValues {
		if _, found := contextVariables[k]; !found {
			ui.Output("Value provided for an undefined HCL variable %q", k, terminal.WithWarningStyle())
		}
	}

	return contextVariables, nil
}

// setEnvironmentVariables sets the environment variables from the pipeline variables
func (*JobExecutor) setEnvironmentVariables(
	ui terminal.UI,
	environmentVariables []*pb.PipelineVariable,
) error {
	for _, variable := range environmentVariables {
		// Check if the environment variable is prohibited
		if _, ok := prohibitedEnvironmentVariables[variable.Key]; ok {
			ui.Output("Environment variable %s is currently not supported by the job executor", variable.Key, terminal.WithWarningStyle())
			continue
		}

		if err := os.Setenv(variable.Key, variable.Value); err != nil {
			return fmt.Errorf("failed to set environment variable %q: %w", variable.Key, err)
		}
	}

	return nil
}

func (j *JobExecutor) startCancellationMonitor(
	ctx context.Context,
	ui terminal.UI,
	maxJobDuration int32,
) {
	// Listen for job timeout
	go func() {
		select {
		case <-j.cancellableCtx.Done():
		case <-time.After(time.Duration(maxJobDuration) * time.Minute):
			ui.Output("Max job duration exceeded. Job will gracefully cancel now...", terminal.WithErrorStyle())
			// Cancel job since job timeout has been reached
			j.cancelFunc()
		}
	}()

	// Listen for graceful cancel
	go func() {
		for {
			if j.cancellableCtx.Err() != nil {
				// Context canceled
				return
			}

			canceled, err := j.waitForJobCancellation(ctx)
			if err != nil {
				if status.Code(err) == codes.Canceled {
					// Context canceled
					return
				}

				ui.Output("Received error when listening for graceful job cancellation: %v", err, terminal.WithErrorStyle())
				time.Sleep(waitCancelError)
				continue
			}

			if canceled {
				ui.Output("Received a graceful job cancel request")

				// Cancel the context
				j.cancelFunc()

				return
			}
		}
	}()

	// Listen for force cancel
	go func() {
		// Wait for graceful cancel before checking for force cancel
		<-j.cancellableCtx.Done()

		for {
			job, err := j.client.GetJob(ctx, j.cfg.JobID)
			if err != nil {
				if ctx.Err() != nil {
					// If the context is canceled, it means the job was already gracefully cancelled,
					// so there is no need to take any additional forced cancel action.
					return
				}

				ui.Output("Received error when listening for forced job cancellation: %v", err, terminal.WithErrorStyle())
				time.Sleep(waitCancelError)
				continue
			}

			if job.ForceCanceled {
				ui.Output("Received a forced job cancel request")

				// Kill the main process to force the job to terminate
				os.Exit(1)
			}

			time.Sleep(waitForcedCancel)
		}
	}()
}

func (j *JobExecutor) waitForJobCancellation(ctx context.Context) (bool, error) {
	for {
		timeoutCtx, timeoutCancel := context.WithTimeout(ctx, jobCancellationSubscriptionTimeout)
		defer timeoutCancel()

		stream, err := j.client.SubscribeToJobCancellationEvent(timeoutCtx, j.cfg.JobID)
		if err != nil {
			if status.Code(err) == codes.DeadlineExceeded {
				// Retry if context timed out.
				continue
			}

			return false, err
		}

		for {
			event, err := stream.Recv()
			if err != nil {
				if status.Code(err) == codes.DeadlineExceeded {
					// Retry if context timed out.
					break
				}

				if err == io.EOF {
					// Stream closed
					return false, nil
				}

				return false, err
			}

			if event.Job.Status == string(models.JobCanceling) {
				return true, nil
			}
		}
	}
}

func getTaskFromPipeline(pipeline *pb.Pipeline, taskPath string) (*pb.PipelineTask, bool) {
	for _, stage := range pipeline.Stages {
		for _, task := range stage.Tasks {
			if task.Path == taskPath {
				return task, true
			}
		}
	}
	return nil, false
}
