package jobexecutor

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestVCSRenewer_newVCSTokenRenewer(t *testing.T) {
	mockClient := client.NewMockClient(t)
	logger, _ := logger.NewForTest()
	providerID := "providerID"
	pipelineID := "pipelineID"

	actual, err := newVCSTokenRenewer(mockClient, logger, providerID, pipelineID)
	require.NoError(t, err)
	assert.Equal(t, mockClient, actual.client)
	assert.Equal(t, logger, actual.logger)
	assert.Equal(t, providerID, actual.providerID)
	assert.Equal(t, pipelineID, actual.pipelineID)
	assert.FileExists(t, actual.tokenInfo.tokenFilePath)

	// cleanup
	actual.cleanup()
	assert.NoFileExists(t, actual.tokenInfo.tokenFilePath)
}

func TestVCSRenewer_start(t *testing.T) {
	mockClient := client.NewMockClient(t)
	logger, _ := logger.NewForTest()
	providerID := "providerID"
	pipelineID := "pipelineID"
	token := "token"
	expiresAt := time.Now().Add(time.Hour)

	mockClient.On("CreateVCSTokenForPipeline", mock.Anything, pipelineID, providerID).Return(&pb.CreateVCSTokenForPipelineResponse{
		AccessToken: token,
		ExpiresAt:   timestamppb.New(expiresAt),
	}, nil).Twice()

	renewer, err := newVCSTokenRenewer(mockClient, logger, providerID, pipelineID)
	require.NoError(t, err)
	defer renewer.cleanup()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err = renewer.start(ctx)
	require.NoError(t, err)

	// Check if the token is renewed and written to the file
	token, tokenFilePath := renewer.getTokenInfo()
	require.NotEmpty(t, token)
	fileContent, err := os.ReadFile(tokenFilePath)
	require.NoError(t, err)
	assert.Equal(t, token, string(fileContent))

	// Simulate token expiration and check if it gets renewed
	renewer.tokenInfo.mutex.Lock()
	expiredTime := time.Now().Add(-time.Minute)
	renewer.tokenInfo.expiresAt = &expiredTime
	renewer.tokenInfo.mutex.Unlock()

	time.Sleep(tokenRefreshInterval + time.Second)

	token, _ = renewer.getTokenInfo()
	assert.NotEmpty(t, token)
}
