package jobexecutor

import (
	"context"
	"fmt"
	"os"
	"sync"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const (
	// seconds of leeway before expiration time
	expirationLeeway = 60 * time.Second
	// how often the goroutine should check if the token is expired and renew it
	tokenRefreshInterval = 30 * time.Second
)

// tokenInfo encapsulates the token information and uses a mutex to synchronize access to the token.
type tokenInfo struct {
	expiresAt     *time.Time
	token         string
	tokenFilePath string
	mutex         sync.RWMutex
}

// vcsTokenRenewer renews the VCS token.
type vcsTokenRenewer struct {
	tokenInfo  *tokenInfo
	client     client.Client
	logger     logger.Logger
	providerID string
	pipelineID string
}

// newVCSTokenRenewer creates a new VCS token renewer.
func newVCSTokenRenewer(client client.Client, logger logger.Logger, providerID, pipelineID string) (*vcsTokenRenewer, error) {
	tokenFile, err := os.CreateTemp("", "vcs-token-*")
	if err != nil {
		return nil, fmt.Errorf("failed to create token file: %w", err)
	}

	return &vcsTokenRenewer{
		client:     client,
		logger:     logger,
		providerID: providerID,
		pipelineID: pipelineID,
		tokenInfo: &tokenInfo{
			tokenFilePath: tokenFile.Name(),
		},
	}, nil
}

// cleanup removes the token file.
func (r *vcsTokenRenewer) cleanup() {
	os.Remove(r.tokenInfo.tokenFilePath)
}

// start renews the token and starts a goroutine to periodically renew the token.
func (r *vcsTokenRenewer) start(ctx context.Context) error {
	r.logger.Infow("starting vcs token renewer", "provider_id", r.providerID, "pipeline_id", r.pipelineID)

	if err := r.renewToken(ctx); err != nil {
		return err
	}

	r.tokenInfo.mutex.RLock()
	if r.tokenInfo.expiresAt == nil {
		r.tokenInfo.mutex.RUnlock()
		return nil
	}
	r.tokenInfo.mutex.RUnlock()

	// Start a goroutine to periodically renew the token.
	go func() {
		for {
			select {
			case <-ctx.Done():
				r.logger.Infow("stopping vcs token renewer", "provider_id", r.providerID, "pipeline_id", r.pipelineID)
				return
			case <-time.After(tokenRefreshInterval):
				if r.isTokenExpired() {
					if err := r.renewToken(ctx); err != nil {
						r.logger.Errorf("failed to renew vcs token: %v", err)
					}
				}
			}
		}
	}()

	return nil
}

// getTokenInfo returns the token and the token file path.
func (r *vcsTokenRenewer) getTokenInfo() (string, string) {
	r.tokenInfo.mutex.RLock()
	defer r.tokenInfo.mutex.RUnlock()

	return r.tokenInfo.token, r.tokenInfo.tokenFilePath
}

// isTokenExpired checks if the token is expired.
func (r *vcsTokenRenewer) isTokenExpired() bool {
	r.tokenInfo.mutex.RLock()
	defer r.tokenInfo.mutex.RUnlock()

	return r.tokenInfo.expiresAt != nil && time.Now().After(r.tokenInfo.expiresAt.Add(-expirationLeeway))
}

// renewToken renews the access token.
func (r *vcsTokenRenewer) renewToken(ctx context.Context) error {
	r.logger.Infow("renewing vcs token", "provider_id", r.providerID, "pipeline_id", r.pipelineID)

	response, err := r.client.CreateVCSTokenForPipeline(ctx, r.pipelineID, r.providerID)
	if err != nil {
		return fmt.Errorf("failed to renew vcs token: %w", err)
	}

	r.tokenInfo.mutex.Lock()
	defer r.tokenInfo.mutex.Unlock()

	if response.ExpiresAt != nil {
		expiresWhen := response.ExpiresAt.AsTime()
		r.tokenInfo.expiresAt = &expiresWhen
	}
	r.tokenInfo.token = response.AccessToken

	if err := os.WriteFile(r.tokenInfo.tokenFilePath, []byte(response.AccessToken), 0600); err != nil {
		return fmt.Errorf("failed to write vcs token to file: %w", err)
	}

	return nil
}
