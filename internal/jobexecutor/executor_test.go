package jobexecutor

import (
	"context"
	"io"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
)

// testConfig is a test config used to parse variable blocks.
type testConfig struct {
	Variables []*config.Variable `hcl:"variable,block"`
}

func (tc testConfig) GetVariableBodies() map[string]hcl.Body {
	variableBodies := map[string]hcl.Body{}
	for _, v := range tc.Variables {
		variableBodies[v.Name] = v.Body
	}
	return variableBodies
}

// testVariable is a test variable used to create a test HCL variable.
type testVariable struct {
	Type    *string
	Default *cty.Value
	Name    string
}

func TestCreateCtyVariables(t *testing.T) {
	type testCase struct {
		expectError string
		hclVariable *testVariable
		pbVariable  *pb.PipelineVariable
		expectValue cty.Value
		name        string
	}

	testCases := []testCase{
		{
			name: "primitive HCL variable with matching type and value",
			hclVariable: &testVariable{
				Name: "test",
				Type: ptr.String("string"),
			},
			pbVariable: &pb.PipelineVariable{
				Key:   "test",
				Value: "value",
			},
			expectValue: cty.StringVal("value"),
		},
		{
			name: "no value provided for HCL variable so default is used",
			hclVariable: &testVariable{
				Name:    "test",
				Default: ptrCtyVal(cty.StringVal("value")),
			},
			expectValue: cty.StringVal("value"),
		},
		{
			name: "no value or default provided for HCL variable",
			hclVariable: &testVariable{
				Name: "test",
				Type: ptr.String("string"),
			},
			expectError: "hcl variable \"test\" defined in configuration but no value provided",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// Create a test config.
			configData, err := createTestConfig(test.hclVariable)
			require.NoError(t, err)

			// Decode the test config.
			var config testConfig
			err = hclsimple.Decode("test.hcl", configData, nil, &config)
			require.NoError(t, err)

			pbVariables := map[string]string{}
			if test.pbVariable != nil {
				pbVariables[test.pbVariable.Key] = test.pbVariable.Value
			}

			// Create a test UI and discard all output.
			ui := terminal.NonInteractiveUI(ctx, io.Discard)

			executor := &JobExecutor{}
			createdVariables, err := executor.createCtyVariables(ui, config.GetVariableBodies(), pbVariables)

			if test.expectError != "" {
				assert.Contains(t, err.Error(), test.expectError)
				return
			}

			require.NoError(t, err)

			assert.Len(t, createdVariables, 1)
			assert.Equal(t, test.expectValue, createdVariables[test.hclVariable.Name])
		})
	}
}

// createTestConfig creates a test config with the given variable.
// Produces:
//
//	variable "example" {
//	   type    = string
//	   default = "value"
//	}
func createTestConfig(v *testVariable) ([]byte, error) {
	f := hclwrite.NewEmptyFile()
	root := f.Body()

	variableBlock := hclwrite.NewBlock("variable", []string{v.Name})
	variableBody := variableBlock.Body()

	if v.Type != nil {
		variableBody.SetAttributeRaw("type", hclwrite.TokensForIdentifier(*v.Type))
	}

	if v.Default != nil {
		variableBody.SetAttributeValue("default", *v.Default)
	}

	root.AppendBlock(variableBlock)

	return f.Bytes(), nil
}

// ptrCtyVal returns a pointer to the given cty.Value.
func ptrCtyVal(v cty.Value) *cty.Value {
	return &v
}
