// Package volume is used to mount volumes inside the job executor.
package volume

//go:generate go tool mockery --name Mounter --inpackage --case underscore

import (
	"context"
	"fmt"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/volume/vcs"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// Type represents the volume type.
type Type string

// Type constants.
const (
	VCSType Type = "vcs"
)

// Mounter encapsulates the logic for mounting different types of volumes.
type Mounter interface {
	MountVolume(ctx context.Context, volume *config.Volume, mount *config.MountPoint, directory string) error
	Close() error
}

// MountManager encapsulates the logic for accessing different mounter instances.
type MountManager struct {
	logger          logger.Logger
	ui              terminal.UI
	client          client.Client
	mounterMap      map[Type]Mounter
	volumeSizeLimit int
}

// NewMountManager returns an instance of MountManager.
func NewMountManager(
	logger logger.Logger,
	ui terminal.UI,
	client client.Client,
	volumeSizeLimit int,
) (*MountManager, error) {
	vcsMounter, err := vcs.New(logger, client, ui, volumeSizeLimit)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize %s volume mounter: %v", VCSType, err)
	}

	return &MountManager{
		logger: logger,
		ui:     ui,
		client: client,
		mounterMap: map[Type]Mounter{
			VCSType: vcsMounter,
		},
		volumeSizeLimit: volumeSizeLimit,
	}, nil
}

// GetMounter returns the Mounter instance associated with the type.
func (m *MountManager) GetMounter(t Type) (Mounter, error) {
	mounter, ok := m.mounterMap[t]
	if !ok {
		return nil, fmt.Errorf("%s volume mounter is not supported", t)
	}

	return mounter, nil
}
