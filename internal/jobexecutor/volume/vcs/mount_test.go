package vcs

import (
	"context"
	"encoding/base64"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"google.golang.org/grpc"
)

const (
	// mockArchiveData is a sample repository data archive as a Base64 string.
	mockArchiveData = `H4sIAAAAAAAAA+3RSwrCMBSF4YxdRVxANWnz2II7cFwwaiFYaSO4fNuBYMUiQouI/ze5gzs4B04K
bcqacK4znRdrMQvV8db2V3urHu+d0MapQrnCu1wonXuvhLTz1Bm6tKlspBRlW50Ox/HId/8flQb7
76sYVumaps3oB3bGjO+v7dP+piOkmrbGa3++/ybEWMtt3cTdcvHtMgAAAAAAAAAAAAAAAAA+cgOS
fswDACgAAA`
)

// mockArchiveStream is the stream used to return the archive data.
type mockArchiveStream struct {
	grpc.ClientStream
	data   []byte
	called bool
}

// Recv implements the gen.VCSProviders_GetRepositoryArchiveClient interface.
func (m *mockArchiveStream) Recv() (*gen.GetRepositoryArchiveResponse, error) {
	if m.called {
		// All data was returned already.
		return nil, io.EOF
	}

	m.called = true

	return &gen.GetRepositoryArchiveResponse{
		// Return the mock data.
		ChunkData: m.data,
	}, nil
}

func TestNew(t *testing.T) {
	logger, _ := logger.NewForTest()
	mockClient := client.NewMockClient(t)
	mockUI := terminal.NonInteractiveUI(context.TODO(), io.Discard)
	volumeSizeLimit := 50000

	expect := &Mounter{
		logger:          logger,
		client:          mockClient,
		ui:              mockUI,
		volumeSizeLimit: volumeSizeLimit,
	}

	actual, err := New(logger, mockClient, mockUI, volumeSizeLimit)
	require.NoError(t, err)

	assert.Equal(t, expect, actual)
}

func TestMount(t *testing.T) {
	providerID := "provider-1"
	repositoryPath := "martian/tools"
	ref := "feature/branch"

	volume := &config.Volume{
		Name: "my-vcs-volume",
		Type: "vcs",
		VCSOptions: &config.VCSOptions{
			ProviderID:     providerID,
			RepositoryPath: repositoryPath,
			Ref:            &ref,
		},
	}

	mountPoint := &config.MountPoint{
		Volume: "my-vcs-volume",
	}

	type testCase struct {
		name        string
		data        string
		expectError string
		sizeLimit   int
	}

	testCases := []testCase{
		{
			name:      "volume mounted",
			data:      mockArchiveData,
			sizeLimit: 1000,
		},
		{
			name:        "no data returned",
			sizeLimit:   1000,
			expectError: "archive download endpoint returned no data",
		},
		{
			name:        "data exceeds limit",
			data:        mockArchiveData,
			sizeLimit:   1,
			expectError: "volume exceeds the maximum configured limit",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockClient := client.NewMockClient(t)

			// Create a temp dir for the testing.
			workingDir, err := os.MkdirTemp("", "test-dir")
			require.NoError(t, err)
			defer os.RemoveAll(workingDir)

			decodedArchiveData, err := base64.RawStdEncoding.DecodeString(test.data)
			require.NoError(t, err)

			mockClient.On("GetRepositoryArchive", mock.Anything, providerID, repositoryPath, &ref).Return(&mockArchiveStream{data: decodedArchiveData}, nil)

			logger, _ := logger.NewForTest()

			mounter := &Mounter{
				logger:          logger,
				client:          mockClient,
				ui:              terminal.NonInteractiveUI(context.TODO(), io.Discard),
				volumeSizeLimit: test.sizeLimit,
			}

			err = mounter.MountVolume(ctx, volume, mountPoint, workingDir)

			if test.expectError != "" {
				assert.ErrorContains(t, err, test.expectError)
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestClose(t *testing.T) {
	mounter := &Mounter{}
	assert.Nil(t, mounter.Close())
}
