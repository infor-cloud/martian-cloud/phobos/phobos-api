// Package vcs handles the mounting of VCS volumes (repositories).
package vcs

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/hashicorp/go-getter"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

var (
	// Un-tarring of repository archive done with Hashicorp's go-getter library.
	tgz = getter.TarGzipDecompressor{}
)

// Mounter implements the VCS-specific mounting.
type Mounter struct {
	logger          logger.Logger
	client          client.Client
	ui              terminal.UI
	volumeSizeLimit int
}

// New returns an instance of Mounter.
func New(
	logger logger.Logger,
	client client.Client,
	ui terminal.UI,
	volumeSizeLimit int,
) (*Mounter, error) {
	return &Mounter{
		logger:          logger,
		client:          client,
		ui:              ui,
		volumeSizeLimit: volumeSizeLimit,
	}, nil
}

// MountVolume mounts the specified volume.
func (m *Mounter) MountVolume(ctx context.Context, volume *config.Volume, mount *config.MountPoint, directory string) error {
	m.ui.Output("- Mounting %s vcs volume...", volume.Name)

	// Create a temporary directory for decompressing.
	decompressDir, err := os.MkdirTemp("", "vcs-volume-*")
	if err != nil {
		return err
	}

	// Create a temporary file for downloading the archive.
	tempArchive, err := os.CreateTemp("", "*-vcs-archive.tar.gz")
	if err != nil {
		return fmt.Errorf("failed to create temporary archive file: %v", err)
	}

	defer func() {
		if rErr := os.RemoveAll(decompressDir); rErr != nil {
			m.logger.Errorf("failed to remove temporary decompress directory: %v", rErr)
		}
		if rErr := os.Remove(tempArchive.Name()); rErr != nil {
			m.logger.Errorf("failed to remove temporary archive file: %v", rErr)
		}
	}()

	archiveStream, err := m.client.GetRepositoryArchive(ctx, volume.VCSOptions.ProviderID, volume.VCSOptions.RepositoryPath, volume.VCSOptions.Ref)
	if err != nil {
		return fmt.Errorf("failed to get repository archive: %v", err)
	}

	// Now, download the repository (i.e. volume).
	var totalBytesReceived int
	for {
		message, sErr := archiveStream.Recv()
		if sErr != nil {
			if sErr == io.EOF {
				// All done.
				break
			}

			return fmt.Errorf("get repository archive stream returned an error: %v", sErr)
		}

		if received := len(message.ChunkData); received > 0 {
			totalBytesReceived += received

			// Make sure we aren't exceeding the maximum limits (in bytes).
			if totalBytesReceived > m.volumeSizeLimit {
				return fmt.Errorf("volume exceeds the maximum configured limit of %d", m.volumeSizeLimit)
			}

			if _, wErr := tempArchive.Write(message.ChunkData); wErr != nil {
				return fmt.Errorf("failed to write data to file: %v", wErr)
			}
		}
	}

	// Verify we have actually downloaded something.
	if totalBytesReceived == 0 {
		return errors.New("archive download endpoint returned no data")
	}

	// Rewind file to start as we'll need to extract it now.
	if _, err = tempArchive.Seek(0, io.SeekStart); err != nil {
		return fmt.Errorf("failed to seek download file: %v", err)
	}

	// Set a limit on the overall size of decompressed files.
	tgz.FileSizeLimit = int64(m.volumeSizeLimit)

	// Decompress the tar file.
	if err = tgz.Decompress(decompressDir, tempArchive.Name(), true, 0o000); err != nil {
		return fmt.Errorf("failed to decompress tar file: %v", err)
	}

	// Default to passed in directory for mounting.
	mountDirectory := directory
	if mount.Path != nil {
		// Append the path on the mount point and use that as the mounting directory.
		mountDirectory = filepath.Join(directory, *mount.Path)
	}

	dirEntries, err := os.ReadDir(decompressDir)
	if err != nil {
		return fmt.Errorf("failed to read directory: %v", err)
	}

	if len(dirEntries) == 0 {
		return fmt.Errorf("no data was decompressed")
	}

	// Copy only the repo contents hence we're explicitly passing in the repo directory's name here.
	if err := m.copyDirectory(filepath.Join(decompressDir, dirEntries[0].Name()), mountDirectory); err != nil {
		return fmt.Errorf("failed to create copy decompressed contents: %v", err)
	}

	m.ui.Output("- Mounted %s vcs volume", volume.Name)

	return nil
}

// Close cleans up any resources.
func (m *Mounter) Close() error {
	return nil
}

// copyDirectory copies the source directory into the destination.
func (m *Mounter) copyDirectory(
	sourceDir,
	destinationDir string,
) error {
	// Create the destination dir.
	if err := os.MkdirAll(destinationDir, 0700); err != nil {
		return fmt.Errorf("failed to create destination directory: %v", err)
	}

	return filepath.WalkDir(sourceDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			// Catch any incoming error.
			return err
		}

		fileInfo, err := d.Info()
		if err != nil {
			return fmt.Errorf("failed to get file info: %v", err)
		}

		// Trim the sourceDir prefix, so we only get intermediary directories.
		fileDestination := filepath.Join(destinationDir, strings.TrimPrefix(path, sourceDir))

		// Copy dir.
		if d.IsDir() {
			return os.MkdirAll(fileDestination, fileInfo.Mode())
		}

		// Copy file.
		sourceFile, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("failed to open source file: %v", err)
		}

		destinationFile, err := os.OpenFile(fileDestination, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, fileInfo.Mode()) // nosemgrep: gosec.G304-1
		if err != nil {
			return fmt.Errorf("failed to create destination file: %v", err)
		}

		if _, err := io.Copy(destinationFile, sourceFile); err != nil {
			return fmt.Errorf("failed to mount file: %v", err)
		}

		return nil
	})
}
