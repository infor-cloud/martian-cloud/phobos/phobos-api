// Package client is the gateway for the job executor to interface with the Phobos API.
package client

//go:generate go tool mockery --name Client --inpackage --case underscore

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/zclconf/go-cty/cty"
	cjson "github.com/zclconf/go-cty/cty/json"
	grpc "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	statemachine "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// Client is the client that the job executor will use to interface with the phobos api
type Client interface {
	Close() error
	SetJobStatus(ctx context.Context, jobID string, status string) (*pb.Job, error)
	WriteLogs(ctx context.Context, jobID string, startOffset int32, buffer []byte) error
	GetJob(ctx context.Context, jobID string) (*pb.Job, error)
	GetPipelineTemplate(ctx context.Context, pipelineID string) ([]byte, error)
	SetPipelineActionStatus(ctx context.Context, pipelineID string, actionPath string, state statemachine.NodeStatus) error
	GetPipeline(ctx context.Context, pipelineID string) (*pb.Pipeline, error)
	GetPipelineActionOutputs(ctx context.Context, pipelineID string, actionPaths []string) ([]*pb.PipelineActionOutputs, error)
	SetPipelineActionOutputs(ctx context.Context, pipelineID string, actionPath string, outputs map[string]cty.Value) error
	GetPipelineVariables(ctx context.Context, pipelineID string) (*pb.GetPipelineVariablesResponse, error)
	CreatePipelineJWT(ctx context.Context, pipelineID string, audience string, expiration time.Time) (string, error)
	SubscribeToJobCancellationEvent(ctx context.Context, jobID string) (pb.Jobs_SubscribeToJobCancellationEventClient, error)
	GetRepositoryArchive(ctx context.Context, vcsProviderID, repositoryPath string, ref *string) (pb.VCSProviders_GetRepositoryArchiveClient, error)
	CreateVCSTokenForPipeline(ctx context.Context, pipelineID, vcsProviderID string) (*pb.CreateVCSTokenForPipelineResponse, error)
}

type grpcClient struct {
	client *grpc.Client
}

// GRPCClientOptions are the options for the grpc client
type GRPCClientOptions struct {
	HTTPEndpoint string
	Token        string
}

// staticTokenGetter returns a static token
type staticTokenGetter struct {
	token string
}

func (g staticTokenGetter) Token(_ context.Context) (string, error) {
	return g.token, nil
}

type leveledLogger struct {
	logger logger.Logger
}

func (l *leveledLogger) Error(msg string, keysAndValues ...interface{}) {
	l.logger.Errorf(msg+": %s", l.formatInterfaceArray(keysAndValues))
}

func (l *leveledLogger) Info(msg string, keysAndValues ...interface{}) {
	l.logger.Infof(msg+": %s", l.formatInterfaceArray(keysAndValues))
}

func (l *leveledLogger) Debug(msg string, keysAndValues ...interface{}) {
	// Intentionally log debug at info level since the client logs important http status
	// messages at debug level which we'd like to capture without debug logging enabled.
	l.Info(msg, keysAndValues...)
}

func (l *leveledLogger) Warn(msg string, keysAndValues ...interface{}) {
	l.logger.Infof(msg+": %s", l.formatInterfaceArray(keysAndValues))
}

func (l *leveledLogger) formatInterfaceArray(data []interface{}) string {
	stringValues := make([]string, len(data))
	for i, item := range data {
		stringValues[i] = fmt.Sprintf("%v", item)
	}
	return strings.Join(stringValues, ", ")
}

// NewGRPCClient returns a new grpc client
func NewGRPCClient(ctx context.Context, logger logger.Logger, options *GRPCClientOptions) (Client, error) {
	client, err := grpc.New(ctx, &grpc.Config{
		HTTPEndpoint:  options.HTTPEndpoint,
		TLSSkipVerify: false,
		TokenGetter:   staticTokenGetter{token: options.Token},
		Logger:        &leveledLogger{logger: logger},
	})
	if err != nil {
		return nil, err
	}

	return &grpcClient{
		client: client,
	}, nil
}

func (c *grpcClient) Close() error {
	return c.client.Close()
}

func (c *grpcClient) CreatePipelineJWT(ctx context.Context, pipelineID string, audience string, expiration time.Time) (string, error) {
	response, err := c.client.PipelinesClient.CreatePipelineJWT(ctx, &pb.CreatePipelineJWTRequest{
		PipelineId: pipelineID,
		Audience:   audience,
		Expiration: timestamppb.New(expiration),
	})
	if err != nil {
		return "", err
	}

	return response.Token, nil
}

func (c *grpcClient) SetJobStatus(ctx context.Context, jobID string, status string) (*pb.Job, error) {
	return c.client.JobsClient.SetJobStatus(ctx, &pb.SetJobStatusInput{
		JobId:  jobID,
		Status: status,
	})
}

func (c *grpcClient) WriteLogs(ctx context.Context, jobID string, startOffset int32, buffer []byte) error {
	_, err := c.client.JobsClient.WriteLogs(ctx, &pb.WriteLogsRequest{
		JobId:       jobID,
		StartOffset: startOffset,
		Logs:        buffer,
	})

	return err
}

func (c *grpcClient) GetJob(ctx context.Context, jobID string) (*pb.Job, error) {
	return c.client.JobsClient.GetJob(ctx, &pb.GetJobInput{
		Id: jobID,
	})
}

func (c *grpcClient) GetPipelineTemplate(ctx context.Context, id string) ([]byte, error) {
	res, err := c.client.PipelineTemplatesClient.GetPipelineTemplateData(ctx, &pb.GetPipelineTemplateDataRequest{
		Id: id,
	})
	if err != nil {
		return nil, err
	}

	buf := []byte{}

	var message pb.GetPipelineTemplateDataResponse
	for {
		err = res.RecvMsg(&message)
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		if len(message.GetChunkData()) > 0 {
			buf = append(buf, message.ChunkData...)
		}
		message.ChunkData = message.ChunkData[:0]
	}

	if len(buf) == 0 {
		return nil, fmt.Errorf("no data returned when querying for pipeline template: %s", id)
	}

	return buf, nil
}

func (c *grpcClient) SetPipelineActionStatus(ctx context.Context, pipelineID string, actionPath string, status statemachine.NodeStatus) error {
	pbStatus := pb.PipelineNodeStatus(pb.PipelineNodeStatus_value[string(status)])
	_, err := c.client.PipelinesClient.SetPipelineActionStatus(ctx, &pb.SetPipelineActionStatusRequest{
		PipelineId: pipelineID,
		ActionPath: actionPath,
		Status:     pbStatus,
	})
	return err
}

func (c *grpcClient) GetPipeline(ctx context.Context, pipelineID string) (*pb.Pipeline, error) {
	return c.client.PipelinesClient.GetPipelineByID(ctx, &pb.GetPipelineByIdRequest{
		Id: pipelineID,
	})
}

func (c *grpcClient) GetPipelineActionOutputs(ctx context.Context, pipelineID string, actionPaths []string) ([]*pb.PipelineActionOutputs, error) {
	response, err := c.client.PipelinesClient.GetPipelineActionOutputs(ctx, &pb.GetPipelineActionOutputsRequest{
		PipelineId:  pipelineID,
		ActionPaths: actionPaths,
	})
	if err != nil {
		return nil, err
	}
	return response.Actions, nil
}

func (c *grpcClient) SetPipelineActionOutputs(ctx context.Context, pipelineID string, actionPath string, outputs map[string]cty.Value) error {
	pbOutputs := []*pb.PipelineActionOutput{}

	for k, v := range outputs {
		tBuf, err := v.Type().MarshalJSON()
		if err != nil {
			return fmt.Errorf("failed to marshal type for output %s: %w", k, err)
		}

		vBuf, err := cjson.Marshal(v, v.Type())
		if err != nil {
			return fmt.Errorf("failed to marshal value for output %s: %w", k, err)
		}

		pbOutputs = append(pbOutputs, &pb.PipelineActionOutput{
			Name:  k,
			Type:  tBuf,
			Value: vBuf,
		})
	}

	_, err := c.client.PipelinesClient.SetPipelineActionOutputs(ctx, &pb.SetPipelineActionOutputsRequest{
		PipelineId: pipelineID,
		ActionPath: actionPath,
		Outputs:    pbOutputs,
	})

	return err
}

func (c *grpcClient) GetPipelineVariables(ctx context.Context, pipelineID string) (*pb.GetPipelineVariablesResponse, error) {
	response, err := c.client.PipelinesClient.GetPipelineVariables(ctx, &pb.GetPipelineVariablesRequest{
		PipelineId: pipelineID,
	})
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (c *grpcClient) SubscribeToJobCancellationEvent(ctx context.Context, jobID string) (pb.Jobs_SubscribeToJobCancellationEventClient, error) {
	stream, err := c.client.JobsClient.SubscribeToJobCancellationEvent(ctx, &pb.JobCancellationEventRequest{
		JobId: jobID,
	})
	if err != nil {
		return nil, err
	}

	return stream, nil
}

func (c *grpcClient) GetRepositoryArchive(ctx context.Context, vcsProviderID, repositoryPath string, ref *string) (pb.VCSProviders_GetRepositoryArchiveClient, error) {
	stream, err := c.client.VCSProvidersClient.GetRepositoryArchive(ctx, &pb.GetRepositoryArchiveRequest{
		ProviderId:     vcsProviderID,
		RepositoryPath: repositoryPath,
		Ref:            ref,
	})
	if err != nil {
		return nil, err
	}

	return stream, nil
}

func (c *grpcClient) CreateVCSTokenForPipeline(ctx context.Context, pipelineID, vcsProviderID string) (*pb.CreateVCSTokenForPipelineResponse, error) {
	return c.client.VCSProvidersClient.CreateVCSTokenForPipeline(ctx, &pb.CreateVCSTokenForPipelineRequest{
		PipelineId: pipelineID,
		ProviderId: vcsProviderID,
	})
}
