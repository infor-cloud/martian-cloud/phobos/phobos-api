package gid

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestType_IsValid(t *testing.T) {
	tests := []struct {
		name    string
		t       Type
		wantErr bool
	}{
		{"Valid UserType", UserType, false},
		{"Invalid Type", Type("INVALID"), true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.t.IsValid()
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestNewGlobalID(t *testing.T) {
	id := uuid.New().String()
	gid := NewGlobalID(UserType, id)
	assert.Equal(t, UserType, gid.Type)
	assert.Equal(t, id, gid.ID)
}

func TestGlobalID_String(t *testing.T) {
	id := uuid.New().String()
	gid := NewGlobalID(UserType, id)
	encoded := gid.String()
	decodedGID, err := ParseGlobalID(encoded)
	assert.NoError(t, err)
	assert.Equal(t, gid.Type, decodedGID.Type)
	assert.Equal(t, gid.ID, decodedGID.ID)
}

func TestParseGlobalID(t *testing.T) {
	id := uuid.New().String()
	gid := NewGlobalID(UserType, id)
	encoded := gid.String()

	parsedGID, err := ParseGlobalID(encoded)
	assert.NoError(t, err)
	assert.Equal(t, gid.Type, parsedGID.Type)
	assert.Equal(t, gid.ID, parsedGID.ID)

	_, err = ParseGlobalID("INVALID")
	assert.Error(t, err)

	// Case where UUID isn't valid
	invalidUUID := "invalid-uuid"
	invalidGID := NewGlobalID(UserType, invalidUUID)
	encodedInvalid := invalidGID.String()

	_, err = ParseGlobalID(encodedInvalid)
	assert.Error(t, err)
}

func TestToGlobalID(t *testing.T) {
	id := uuid.New().String()
	globalID := ToGlobalID(UserType, id)
	parsedGID, err := ParseGlobalID(globalID)
	assert.NoError(t, err)
	assert.Equal(t, UserType, parsedGID.Type)
	assert.Equal(t, id, parsedGID.ID)
}

func TestFromGlobalID(t *testing.T) {
	id := uuid.New().String()
	globalID := ToGlobalID(UserType, id)
	dbID := FromGlobalID(globalID)
	assert.Equal(t, id, dbID)

	invalidID := FromGlobalID("INVALID")
	assert.Equal(t, "invalid[INVALID]", invalidID)
}
