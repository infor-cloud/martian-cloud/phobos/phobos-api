// Package gid package
package gid

import (
	"encoding/base32"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// encoding is the base32 encoding used for global IDs
var encoding = base32.NewEncoding("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567").WithPadding(base32.NoPadding)

// Type is the type of the global ID
type Type string

// Type constants
const (
	UserType                 Type = "U"
	RoleType                 Type = "RL"
	OrganizationType         Type = "O"
	MembershipType           Type = "M"
	ResourceLimitType        Type = "RLM"
	ProjectType              Type = "P"
	ProjectVariableType      Type = "PVAR"
	ProjectVariableSetType   Type = "PVS"
	TeamType                 Type = "T"
	TeamMemberType           Type = "TM"
	PipelineTemplateType     Type = "PT"
	PipelineType             Type = "PI"
	PipelineApprovalType     Type = "PA"
	AgentType                Type = "A"
	AgentSessionType         Type = "AS"
	JobType                  Type = "J"
	EnvironmentType          Type = "E"
	ServiceAccountType       Type = "SA"
	SCIMTokenType            Type = "ST"
	LogStreamType            Type = "LS"
	PipelineActionOutputType Type = "PAO"
	ApprovalRuleType         Type = "AR"
	LifecycleTemplateType    Type = "LT"
	ReleaseLifecycleType     Type = "RLC"
	ReleaseType              Type = "R"
	ActivityEventType        Type = "AE"
	CommentType              Type = "C"
	PluginType               Type = "PL"
	PluginVersionType        Type = "PV"
	PluginPlatformType       Type = "PP"
	ToDoItemType             Type = "TI"
	VCSProviderType          Type = "VP"
	ThreadType               Type = "TH"
	EnvironmentRuleType      Type = "ER"
	MetricType               Type = "MET"
)

// IsValid returns true if this is a valid Type enum
func (t Type) IsValid() error {
	switch t {
	case UserType,
		OrganizationType,
		MembershipType,
		RoleType,
		ResourceLimitType,
		ProjectType,
		ProjectVariableType,
		ProjectVariableSetType,
		TeamType,
		TeamMemberType,
		PipelineTemplateType,
		PipelineType,
		PipelineApprovalType,
		AgentType,
		AgentSessionType,
		JobType,
		EnvironmentType,
		ServiceAccountType,
		SCIMTokenType,
		LogStreamType,
		PipelineActionOutputType,
		ApprovalRuleType,
		LifecycleTemplateType,
		ReleaseLifecycleType,
		ReleaseType,
		ActivityEventType,
		CommentType,
		PluginType,
		PluginVersionType,
		PluginPlatformType,
		ToDoItemType,
		VCSProviderType,
		ThreadType,
		EnvironmentRuleType:
		return nil
	}
	return errors.New("invalid ID type %s", t, errors.WithErrorCode(errors.EInvalid))
}

// GlobalID is a model ID with type information
type GlobalID struct {
	Type Type
	ID   string
}

// NewGlobalID returns a new GlobalID
func NewGlobalID(modelType Type, modelID string) *GlobalID {
	return &GlobalID{Type: modelType, ID: modelID}
}

// String returns the string representation of the global ID
func (g *GlobalID) String() string {
	return encoding.EncodeToString([]byte(fmt.Sprintf("%s_%s", g.ID, g.Type)))
}

// ParseGlobalID parses a global ID string and returns a GlobalID type
func ParseGlobalID(globalID string) (*GlobalID, error) {
	decodedBytes, err := encoding.DecodeString(globalID)
	if err != nil {
		return nil, errors.Wrap(err, "Invalid ID", errors.WithErrorCode(errors.EInvalid))
	}

	decodedGlobalID := string(decodedBytes)

	index := strings.LastIndex(decodedGlobalID, "_")
	if index == -1 {
		return nil, errors.New("Invalid ID", errors.WithErrorCode(errors.EInvalid))
	}

	t := Type(decodedGlobalID[index+1:])
	if err := t.IsValid(); err != nil {
		return nil, err
	}

	if err := uuid.Validate(decodedGlobalID[:index]); err != nil {
		return nil, errors.Wrap(err, "Invalid ID", errors.WithErrorCode(errors.EInvalid))
	}

	return NewGlobalID(t, decodedGlobalID[:index]), nil
}

// ToGlobalID converts a model type and DB ID to a global ID string
func ToGlobalID(idType Type, id string) string {
	return NewGlobalID(idType, id).String()
}

// FromGlobalID converts a global ID string to a DB ID string
func FromGlobalID(globalID string) string {
	gid, err := ParseGlobalID(globalID)
	if err != nil {
		return fmt.Sprintf("invalid[%s]", globalID)
	}
	return gid.ID
}
