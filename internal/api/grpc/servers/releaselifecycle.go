package servers

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
)

// ReleaseLifecycleServer embeds the UnimplementedReleaseLifecyclesServer.
type ReleaseLifecycleServer struct {
	pb.UnimplementedReleaseLifecyclesServer
	urnResolver             *urn.Resolver
	releaseLifecycleService releaselifecycle.Service
}

// NewReleaseLifecycleServer returns an instance of ReleaseLifecycleServer.
func NewReleaseLifecycleServer(releaseLifecycleService releaselifecycle.Service, urnResolver *urn.Resolver,
) *ReleaseLifecycleServer {
	return &ReleaseLifecycleServer{
		releaseLifecycleService: releaseLifecycleService,
		urnResolver:             urnResolver,
	}
}

// GetReleaseLifecycleByID returns a ReleaseLifecycle by an ID.
func (s *ReleaseLifecycleServer) GetReleaseLifecycleByID(ctx context.Context,
	req *pb.GetReleaseLifecycleByIdRequest,
) (*pb.ReleaseLifecycle, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	gotReleaseLifecycle, err := s.releaseLifecycleService.GetReleaseLifecycleByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBReleaseLifecycle(gotReleaseLifecycle), nil
}

// GetReleaseLifecycles returns a paginated list of ReleaseLifecycles.
func (s *ReleaseLifecycleServer) GetReleaseLifecycles(ctx context.Context,
	req *pb.GetReleaseLifecyclesRequest) (*pb.GetReleaseLifecyclesResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.ReleaseLifecycleSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &releaselifecycle.GetReleaseLifecyclesInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		Search:            req.Search,
	}

	for _, s := range req.Scopes {
		input.ReleaseLifecycleScopes = append(input.ReleaseLifecycleScopes, models.ScopeType(s.String()))
	}

	if req.OrgId != nil {
		orgID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetOrgId())
		if rErr != nil {
			return nil, rErr
		}

		input.OrganizationID = &orgID
	}

	if req.ProjectId != nil {
		projectID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if rErr != nil {
			return nil, rErr
		}

		input.ProjectID = &projectID
	}

	result, err := s.releaseLifecycleService.GetReleaseLifecycles(ctx, input)
	if err != nil {
		return nil, err
	}

	releaseLifecycles := result.ReleaseLifecycles

	pbReleaseLifecycles := make([]*pb.ReleaseLifecycle, len(releaseLifecycles))
	for ix := range releaseLifecycles {
		pbReleaseLifecycles[ix] = s.toPBReleaseLifecycle(releaseLifecycles[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(releaseLifecycles) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(releaseLifecycles[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(releaseLifecycles[len(releaseLifecycles)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetReleaseLifecyclesResponse{
		PageInfo:          pageInfo,
		ReleaseLifecycles: pbReleaseLifecycles,
	}, nil
}

// CreateReleaseLifecycle creates a new ReleaseLifecycle.
func (s *ReleaseLifecycleServer) CreateReleaseLifecycle(ctx context.Context,
	req *pb.CreateReleaseLifecycleRequest) (*pb.ReleaseLifecycle, error) {
	lifecycleTemplateID, err := s.urnResolver.ResolveResourceID(ctx, req.GetLifecycleTemplateId())
	if err != nil {
		return nil, err
	}

	toCreate := &releaselifecycle.CreateReleaseLifecycleInput{
		Scope:               models.ScopeType(req.Scope.String()),
		LifecycleTemplateID: lifecycleTemplateID,
		Name:                req.GetName(),
	}

	if req.OrgId != nil {
		orgID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetOrgId())
		if rErr != nil {
			return nil, rErr
		}

		toCreate.OrganizationID = &orgID
	}

	if req.ProjectId != nil {
		projectID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if rErr != nil {
			return nil, rErr
		}

		toCreate.ProjectID = &projectID
	}

	createdReleaseLifecycle, err := s.releaseLifecycleService.CreateReleaseLifecycle(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBReleaseLifecycle(createdReleaseLifecycle), nil
}

// UpdateReleaseLifecycle updates an existing ReleaseLifecycle.
func (s *ReleaseLifecycleServer) UpdateReleaseLifecycle(ctx context.Context,
	req *pb.UpdateReleaseLifecycleRequest) (*pb.ReleaseLifecycle, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	lifecycleTemplateID, err := s.urnResolver.ResolveResourceID(ctx, req.GetLifecycleTemplateId())
	if err != nil {
		return nil, err
	}

	toUpdate := &releaselifecycle.UpdateReleaseLifecycleInput{
		ID:                  id,
		LifecycleTemplateID: lifecycleTemplateID,
	}

	updatedReleaseLifecycle, err := s.releaseLifecycleService.UpdateReleaseLifecycle(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBReleaseLifecycle(updatedReleaseLifecycle), nil
}

// DeleteReleaseLifecycle deletes an existing ReleaseLifecycle.
func (s *ReleaseLifecycleServer) DeleteReleaseLifecycle(ctx context.Context,
	req *pb.DeleteReleaseLifecycleRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	toDelete := &releaselifecycle.DeleteReleaseLifecycleInput{
		ID: id,
	}

	if req.Version != nil {
		v, err := strconv.Atoi(req.GetVersion())
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := s.releaseLifecycleService.DeleteReleaseLifecycle(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// toPBReleaseLifecycle converts from ReleaseLifecycle model to ProtoBuf model.
func (*ReleaseLifecycleServer) toPBReleaseLifecycle(pt *models.ReleaseLifecycle) *pb.ReleaseLifecycle {
	var projectID *string
	if pt.ProjectID != nil {
		projectID = ptr.String(gid.ToGlobalID(gid.ProjectType, *pt.ProjectID))
	}

	return &pb.ReleaseLifecycle{
		Metadata:            toPBMetadata(&pt.Metadata, gid.ReleaseLifecycleType),
		CreatedBy:           pt.CreatedBy,
		Scope:               string(pt.Scope),
		OrgId:               gid.ToGlobalID(gid.OrganizationType, pt.OrganizationID),
		ProjectId:           projectID,
		LifecycleTemplateId: gid.ToGlobalID(gid.LifecycleTemplateType, pt.LifecycleTemplateID),
		Name:                pt.Name,
	}
}
