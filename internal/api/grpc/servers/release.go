package servers

import (
	"context"
	"strconv"
	"time"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/release"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// ReleaseServer embeds the UnimplementedReleasesServer.
type ReleaseServer struct {
	pb.UnimplementedReleasesServer
	urnResolver    *urn.Resolver
	releaseService release.Service
}

// NewReleaseServer returns an instance of ReleaseServer.
func NewReleaseServer(releaseService release.Service, urnResolver *urn.Resolver) *ReleaseServer {
	return &ReleaseServer{
		releaseService: releaseService,
		urnResolver:    urnResolver,
	}
}

// GetReleaseByID returns a Release by an ID.
func (s *ReleaseServer) GetReleaseByID(ctx context.Context, req *pb.GetReleaseByIdRequest) (*pb.Release, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	gotRelease, err := s.releaseService.GetReleaseByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBRelease(gotRelease), nil
}

// GetReleases returns a paginated list of Releases.
func (s *ReleaseServer) GetReleases(ctx context.Context, req *pb.GetReleasesRequest) (*pb.GetReleasesResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.ReleaseSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.ProjectId)
	if err != nil {
		return nil, err
	}

	input := &release.GetReleasesInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		ProjectID:         &projectID,
	}

	result, err := s.releaseService.GetReleases(ctx, input)
	if err != nil {
		return nil, err
	}

	releases := result.Releases

	pbReleases := make([]*pb.Release, len(releases))
	for ix := range releases {
		pbReleases[ix] = s.toPBRelease(releases[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(releases) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(releases[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(releases[len(releases)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetReleasesResponse{
		PageInfo: pageInfo,
		Releases: pbReleases,
	}, nil
}

// CreateRelease creates a Release.
func (s *ReleaseServer) CreateRelease(ctx context.Context, req *pb.CreateReleaseRequest) (*pb.Release, error) {
	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.ProjectId)
	if err != nil {
		return nil, err
	}

	lifecycleID, err := s.urnResolver.ResolveResourceID(ctx, req.LifecycleId)
	if err != nil {
		return nil, err
	}

	// Maintain a template ID cache since most deployment templates
	// will likely be the same across environments.
	templateIDCache := map[string]string{}
	templatesInput := make([]release.DeploymentTemplateInput, len(req.DeploymentTemplates))
	for ix, template := range req.DeploymentTemplates {
		templateID, ok := templateIDCache[template.PipelineTemplateId]
		if !ok {
			templateID, err = s.urnResolver.ResolveResourceID(ctx, template.PipelineTemplateId)
			if err != nil {
				return nil, err
			}
		}

		templatesInput[ix] = release.DeploymentTemplateInput{
			PipelineTemplateID: templateID,
			EnvironmentName:    template.EnvironmentName,
		}
	}

	variablesInput := []release.VariableInput{}
	for _, variable := range req.HclVariables {
		variablesInput = append(variablesInput, release.VariableInput{
			EnvironmentName: variable.EnvironmentName,
			Key:             variable.Key,
			Value:           variable.Value,
			Category:        models.HCLVariable,
		})
	}

	for _, variable := range req.EnvironmentVariables {
		variablesInput = append(variablesInput, release.VariableInput{
			EnvironmentName: variable.EnvironmentName,
			Key:             variable.Key,
			Value:           variable.Value,
			Category:        models.EnvironmentVariable,
		})
	}

	userParticipantIDs := make([]string, len(req.UserParticipantIds))
	for ix, id := range req.UserParticipantIds {
		userParticipantIDs[ix], err = s.urnResolver.ResolveResourceID(ctx, id)
		if err != nil {
			return nil, err
		}
	}

	teamParticipantIDs := make([]string, len(req.TeamParticipantIds))
	for ix, id := range req.TeamParticipantIds {
		teamParticipantIDs[ix], err = s.urnResolver.ResolveResourceID(ctx, id)
		if err != nil {
			return nil, err
		}
	}

	var dueDate *time.Time
	if req.DueDate != nil {
		dueDate = ptr.Time(req.DueDate.AsTime())
	}

	toCreate := &release.CreateReleaseInput{
		SemanticVersion:     req.SemanticVersion,
		ProjectID:           projectID,
		LifecycleID:         lifecycleID,
		DeploymentTemplates: templatesInput,
		Variables:           variablesInput,
		DueDate:             dueDate,
		Notes:               req.Notes,
		UserParticipantIDs:  userParticipantIDs,
		TeamParticipantIDs:  teamParticipantIDs,
		VariableSetRevision: req.VariableSetRevision,
	}

	createdRelease, err := s.releaseService.CreateRelease(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBRelease(createdRelease), nil
}

// UpdateRelease updates a Release.
func (s *ReleaseServer) UpdateRelease(ctx context.Context, req *pb.UpdateReleaseRequest) (*pb.Release, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toUpdate := &release.UpdateReleaseInput{
		ReleaseID: id,
		Notes:     req.Notes,
	}

	if req.DueDate != nil {
		toUpdate.DueDate = ptr.Time(req.DueDate.AsTime())
	}

	updatedRelease, err := s.releaseService.UpdateRelease(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBRelease(updatedRelease), nil
}

// DeleteRelease deletes a Release.
func (s *ReleaseServer) DeleteRelease(ctx context.Context, req *pb.DeleteReleaseRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &release.DeleteReleaseInput{
		ID: id,
	}

	if req.Version != nil {
		v, err := strconv.Atoi(req.GetVersion())
		if err != nil {
			return nil, err
		}

		toDelete.MetadataVersion = &v
	}

	if err := s.releaseService.DeleteRelease(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// AddParticipantToRelease adds a participant to a Release.
func (s *ReleaseServer) AddParticipantToRelease(ctx context.Context, req *pb.AddParticipantToReleaseRequest) (*pb.Release, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.ReleaseId)
	if err != nil {
		return nil, err
	}

	toAdd := &release.ParticipantInput{
		ReleaseID: id,
	}

	if req.UserId != nil {
		userID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetUserId())
		if rErr != nil {
			return nil, rErr
		}

		toAdd.UserID = &userID
	}

	if req.TeamId != nil {
		teamID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetTeamId())
		if rErr != nil {
			return nil, rErr
		}

		toAdd.TeamID = &teamID
	}

	updatedRelease, err := s.releaseService.AddParticipantToRelease(ctx, toAdd)
	if err != nil {
		return nil, err
	}

	return s.toPBRelease(updatedRelease), nil
}

// RemoveParticipantFromRelease removes a participant from a Release.
func (s *ReleaseServer) RemoveParticipantFromRelease(ctx context.Context, req *pb.RemoveParticipantFromReleaseRequest) (*pb.Release, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.ReleaseId)
	if err != nil {
		return nil, err
	}

	toRemove := &release.ParticipantInput{
		ReleaseID: id,
	}

	if req.UserId != nil {
		userID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetUserId())
		if rErr != nil {
			return nil, rErr
		}

		toRemove.UserID = &userID
	}

	if req.TeamId != nil {
		teamID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetTeamId())
		if rErr != nil {
			return nil, rErr
		}

		toRemove.TeamID = &teamID
	}

	updatedRelease, err := s.releaseService.RemoveParticipantFromRelease(ctx, toRemove)
	if err != nil {
		return nil, err
	}

	return s.toPBRelease(updatedRelease), nil
}

// UpdateReleaseDeploymentPipeline updates a release deployment pipeline.
func (s *ReleaseServer) UpdateReleaseDeploymentPipeline(ctx context.Context, req *pb.UpdateReleaseDeploymentPipelineRequest) (*pb.Pipeline, error) {
	releaseID, err := s.urnResolver.ResolveResourceID(ctx, req.ReleaseId)
	if err != nil {
		return nil, err
	}

	toUpdate := &release.UpdateDeploymentPipelineInput{
		ReleaseID:       releaseID,
		EnvironmentName: req.EnvironmentName,
	}

	if req.PipelineTemplateId != nil {
		templateID, rErr := s.urnResolver.ResolveResourceID(ctx, *req.PipelineTemplateId)
		if rErr != nil {
			return nil, rErr
		}

		toUpdate.PipelineTemplateID = &templateID
	}

	for _, hclVar := range req.HclVariables {
		toUpdate.Variables = append(toUpdate.Variables, pipeline.Variable{
			Key:      hclVar.Key,
			Value:    hclVar.Value,
			Category: models.HCLVariable,
		})
	}

	for _, environmentVar := range req.EnvironmentVariables {
		toUpdate.Variables = append(toUpdate.Variables, pipeline.Variable{
			Key:      environmentVar.Key,
			Value:    environmentVar.Value,
			Category: models.EnvironmentVariable,
		})
	}

	pipeline, err := s.releaseService.UpdateDeploymentPipeline(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return toPBPipeline(pipeline), nil
}

// toPBRelease converts from Release model to ProtoBuf model.
func (s *ReleaseServer) toPBRelease(r *models.Release) *pb.Release {
	pbRelease := &pb.Release{
		Metadata:        toPBMetadata(&r.Metadata, gid.ReleaseType),
		CreatedBy:       r.CreatedBy,
		SemanticVersion: r.SemanticVersion,
		ProjectId:       gid.ToGlobalID(gid.ProjectType, r.ProjectID),
		PreRelease:      r.PreRelease,
		Latest:          r.Latest,
	}

	for _, userID := range r.UserParticipantIDs {
		pbRelease.UserParticipantIds = append(pbRelease.UserParticipantIds, gid.ToGlobalID(gid.UserType, userID))
	}

	for _, teamID := range r.TeamParticipantIDs {
		pbRelease.TeamParticipantIds = append(pbRelease.TeamParticipantIds, gid.ToGlobalID(gid.TeamType, teamID))
	}

	if r.DueDate != nil {
		pbRelease.DueDate = timestamppb.New(*r.DueDate)
	}

	return pbRelease
}
