// Package servers implements the gRPC servers.
package servers

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/project"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
)

// ProjectServer embeds the UnimplementedProjectsServer.
type ProjectServer struct {
	pb.UnimplementedProjectsServer
	projectService project.Service
	urnResolver    *urn.Resolver
}

// NewProjectServer returns an instance of ProjectServer.
func NewProjectServer(
	projectService project.Service,
	urnResolver *urn.Resolver,
) *ProjectServer {
	return &ProjectServer{
		projectService: projectService,
		urnResolver:    urnResolver,
	}
}

// GetProjectByID returns an Project by an ID.
func (s *ProjectServer) GetProjectByID(ctx context.Context, req *pb.GetProjectByIdRequest) (*pb.Project, error) {
	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotProj, err := s.projectService.GetProjectByID(ctx, projectID)
	if err != nil {
		return nil, err
	}

	return s.toPBProject(gotProj), nil
}

// GetProjects returns a paginated list of Projects.
func (s *ProjectServer) GetProjects(ctx context.Context, req *pb.GetProjectsRequest) (*pb.GetProjectsResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.ProjectSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &project.GetProjectsInput{
		Search:            req.Search,
		Sort:              &sort,
		PaginationOptions: paginationOpts,
	}

	result, err := s.projectService.GetProjects(ctx, input)
	if err != nil {
		return nil, err
	}

	projects := result.Projects

	pbProjs := make([]*pb.Project, len(projects))
	for ix := range projects {
		pbProjs[ix] = s.toPBProject(&projects[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(projects) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&projects[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&projects[len(projects)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetProjectsResponse{
		PageInfo: pageInfo,
		Projects: pbProjs,
	}, nil
}

// CreateProject creates a new Project.
func (s *ProjectServer) CreateProject(ctx context.Context, req *pb.CreateProjectRequest) (*pb.Project, error) {
	orgID, err := s.urnResolver.ResolveResourceID(ctx, req.OrgId)
	if err != nil {
		return nil, err
	}

	toCreate := &project.CreateProjectInput{
		Name:        req.GetName(),
		Description: req.GetDescription(),
		OrgID:       orgID,
	}

	createdProj, err := s.projectService.CreateProject(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBProject(createdProj), nil
}

// UpdateProject returns the updated Project.
func (s *ProjectServer) UpdateProject(ctx context.Context, req *pb.UpdateProjectRequest) (*pb.Project, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toUpdate := &project.UpdateProjectInput{
		ID:          id,
		Description: req.Description,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toUpdate.Version = &v
	}

	updatedProj, err := s.projectService.UpdateProject(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBProject(updatedProj), nil
}

// DeleteProject deletes an Project.
func (s *ProjectServer) DeleteProject(ctx context.Context, req *pb.DeleteProjectRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &project.DeleteProjectInput{
		ID: id,
	}

	if req.Version != nil {
		v, err := strconv.Atoi(req.GetVersion())
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := s.projectService.DeleteProject(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// toPBProject converts from Project model to ProtoBuf model.
func (*ProjectServer) toPBProject(proj *models.Project) *pb.Project {
	return &pb.Project{
		Metadata:    toPBMetadata(&proj.Metadata, gid.ProjectType),
		Name:        proj.Name,
		Description: proj.Description,
		CreatedBy:   proj.CreatedBy,
		OrgId:       gid.ToGlobalID(gid.OrganizationType, proj.OrgID),
	}
}
