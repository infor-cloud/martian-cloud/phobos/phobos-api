package servers

import (
	"context"
	"fmt"
	"io"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/grpc/reader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	// chunkSize defines the size of the chunks to send to the client (should be multiple of 3 for base64 encoding)
	chunkSize = 1024 * 1024 * 3
)

// PipelineTemplateServer embeds the UnimplementedPipelineTemplatesServer.
type PipelineTemplateServer struct {
	pb.UnimplementedPipelineTemplatesServer
	pipelineTemplateService pipelinetemplate.Service
	urnResolver             *urn.Resolver
	logger                  logger.Logger
	maxUploadSize           int64
}

// NewPipelineTemplateServer returns an instance of PipelineTemplateServer.
func NewPipelineTemplateServer(
	logger logger.Logger,
	pipelineTemplateService pipelinetemplate.Service,
	urnResolver *urn.Resolver,
	maxUploadSize int64,
) *PipelineTemplateServer {
	return &PipelineTemplateServer{
		pipelineTemplateService: pipelineTemplateService,
		urnResolver:             urnResolver,
		maxUploadSize:           maxUploadSize,
		logger:                  logger,
	}
}

// GetPipelineTemplateByID returns a PipelineTemplate by an ID.
func (s *PipelineTemplateServer) GetPipelineTemplateByID(ctx context.Context,
	req *pb.GetPipelineTemplateByIdRequest,
) (*pb.PipelineTemplate, error) {
	templateID, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotPipelineTemplate, err := s.pipelineTemplateService.GetPipelineTemplateByID(ctx, templateID)
	if err != nil {
		return nil, err
	}

	return s.toPBPipelineTemplate(gotPipelineTemplate), nil
}

// GetPipelineTemplates returns a paginated list of PipelineTemplates.
func (s *PipelineTemplateServer) GetPipelineTemplates(ctx context.Context, req *pb.GetPipelineTemplatesRequest) (*pb.GetPipelineTemplatesResponse, error) {
	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.ProjectId)
	if err != nil {
		return nil, err
	}

	// GetSort() will default to a sort option if nothing was specified.
	sort := db.PipelineTemplateSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &pipelinetemplate.GetPipelineTemplatesInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		ProjectID:         projectID,
		Versioned:         req.Versioned, // *bool to allow filter by versioned, by non-versioned, or no filter
		Latest:            req.Latest,
		Name:              req.Name,
		Search:            req.Search,
	}

	result, err := s.pipelineTemplateService.GetPipelineTemplates(ctx, input)
	if err != nil {
		return nil, err
	}

	pipelineTemplates := result.PipelineTemplates

	pbPipelineTemplates := make([]*pb.PipelineTemplate, len(pipelineTemplates))
	for ix := range pipelineTemplates {
		pbPipelineTemplates[ix] = s.toPBPipelineTemplate(&pipelineTemplates[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(pipelineTemplates) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&pipelineTemplates[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&pipelineTemplates[len(pipelineTemplates)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetPipelineTemplatesResponse{
		PageInfo:          pageInfo,
		PipelineTemplates: pbPipelineTemplates,
	}, nil
}

// CreatePipelineTemplate creates a new PipelineTemplate.
func (s *PipelineTemplateServer) CreatePipelineTemplate(ctx context.Context, req *pb.CreatePipelineTemplateRequest) (*pb.PipelineTemplate, error) {
	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.ProjId)
	if err != nil {
		return nil, err
	}

	toCreate := &pipelinetemplate.CreatePipelineTemplateInput{
		ProjectID:       projectID,
		Versioned:       req.GetVersioned(),
		Name:            req.Name,
		SemanticVersion: req.SemanticVersion,
	}

	createdPipelineTemplate, err := s.pipelineTemplateService.CreatePipelineTemplate(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBPipelineTemplate(createdPipelineTemplate), nil
}

// UploadPipelineTemplate uploads to a PipelineTemplate.
func (s *PipelineTemplateServer) UploadPipelineTemplate(stream pb.PipelineTemplates_UploadPipelineTemplateServer) error {
	// First message is request metadata.
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	templateID, err := s.urnResolver.ResolveResourceID(stream.Context(), req.GetInfo().GetId())
	if err != nil {
		return err
	}

	// Use a reader that limits the size of request body
	return s.pipelineTemplateService.UploadPipelineTemplate(
		stream.Context(),
		&pipelinetemplate.UploadPipelineTemplateInput{
			ID: templateID,
			Reader: reader.NewLimitReader(func() ([]byte, error) {
				chunkReq, err := stream.Recv()
				if err != nil {
					// Return the error whether it's EOF or something else.
					return nil, err
				}

				return chunkReq.GetChunkData(), nil
			}, s.maxUploadSize),
		})
}

// GetPipelineTemplateData returns a stream of PipelineTemplate data.
func (s *PipelineTemplateServer) GetPipelineTemplateData(req *pb.GetPipelineTemplateDataRequest, server pb.PipelineTemplates_GetPipelineTemplateDataServer) error {
	templateID, err := s.urnResolver.ResolveResourceID(server.Context(), req.Id)
	if err != nil {
		return err
	}

	rdr, err := s.pipelineTemplateService.GetPipelineTemplateData(server.Context(), templateID)
	if err != nil {
		return err
	}
	defer func() {
		if err = rdr.Close(); err != nil {
			s.logger.Errorf("error closing pipeline template data reader: %v", err)
		}
	}()

	chunk := &pb.GetPipelineTemplateDataResponse{ChunkData: make([]byte, chunkSize)}

	var n int
	for {
		n, err = rdr.Read(chunk.ChunkData)
		if err != nil && err != io.EOF {
			return fmt.Errorf("error reading pipeline template data: %w", err)
		}

		if n > 0 {
			chunk.ChunkData = chunk.ChunkData[:n]
			serverErr := server.Send(chunk)
			if serverErr != nil {
				return fmt.Errorf("error sending pipeline template data: %w", serverErr)
			}
		}

		if err == io.EOF {
			return nil
		}
	}
}

// DeletePipelineTemplate deletes a PipelineTemplate.
func (s *PipelineTemplateServer) DeletePipelineTemplate(ctx context.Context,
	req *pb.DeletePipelineTemplateRequest) (*emptypb.Empty, error) {
	templateID, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &pipelinetemplate.DeletePipelineTemplateInput{
		ID: templateID,
	}

	if req.Version != nil {
		v, err := strconv.Atoi(req.GetVersion())
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := s.pipelineTemplateService.DeletePipelineTemplate(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// toPBPipelineTemplate converts from PipelineTemplate model to ProtoBuf model.
func (*PipelineTemplateServer) toPBPipelineTemplate(pt *models.PipelineTemplate) *pb.PipelineTemplate {
	return &pb.PipelineTemplate{
		Metadata:        toPBMetadata(&pt.Metadata, gid.PipelineTemplateType),
		CreatedBy:       pt.CreatedBy,
		ProjId:          gid.ToGlobalID(gid.ProjectType, pt.ProjectID),
		Status:          string(pt.Status),
		Versioned:       pt.Versioned,
		Name:            pt.Name,
		SemanticVersion: pt.SemanticVersion,
		Latest:          pt.Latest,
	}
}
