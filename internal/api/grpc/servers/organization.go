// Package servers implements the gRPC servers.
package servers

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/organization"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
)

// OrganizationServer embeds the UnimplementedOrganizationsServer.
type OrganizationServer struct {
	pb.UnimplementedOrganizationsServer
	organizationService organization.Service
	urnResolver         *urn.Resolver
}

// NewOrganizationServer returns an instance of OrganizationServer.
func NewOrganizationServer(
	organizationService organization.Service,
	urnResolver *urn.Resolver,
) *OrganizationServer {
	return &OrganizationServer{
		organizationService: organizationService,
		urnResolver:         urnResolver,
	}
}

// GetOrganizationByID returns an Organization by an ID.
func (s *OrganizationServer) GetOrganizationByID(ctx context.Context, req *pb.GetOrganizationByIdRequest) (*pb.Organization, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotOrg, err := s.organizationService.GetOrganizationByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBOrganization(gotOrg), nil
}

// GetOrganizations returns a paginated list of Organizations.
func (s *OrganizationServer) GetOrganizations(ctx context.Context, req *pb.GetOrganizationsRequest) (*pb.GetOrganizationsResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.OrganizationSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &organization.GetOrganizationsInput{
		Search:            req.Search,
		Sort:              &sort,
		PaginationOptions: paginationOpts,
	}

	result, err := s.organizationService.GetOrganizations(ctx, input)
	if err != nil {
		return nil, err
	}

	organizations := result.Organizations

	pbOrgs := make([]*pb.Organization, len(organizations))
	for ix := range organizations {
		pbOrgs[ix] = s.toPBOrganization(&organizations[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(organizations) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&organizations[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&organizations[len(organizations)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetOrganizationsResponse{
		PageInfo:      pageInfo,
		Organizations: pbOrgs,
	}, nil
}

// CreateOrganization creates a new Organization.
func (s *OrganizationServer) CreateOrganization(ctx context.Context, req *pb.CreateOrganizationRequest) (*pb.Organization, error) {
	toCreate := &organization.CreateOrganizationInput{
		Name:        req.GetName(),
		Description: req.GetDescription(),
	}

	createdOrg, err := s.organizationService.CreateOrganization(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBOrganization(createdOrg), nil
}

// UpdateOrganization returns the updated Organization.
func (s *OrganizationServer) UpdateOrganization(ctx context.Context, req *pb.UpdateOrganizationRequest) (*pb.Organization, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toUpdate := &organization.UpdateOrganizationInput{
		ID:          id,
		Description: req.Description,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toUpdate.Version = &v
	}

	updatedOrg, err := s.organizationService.UpdateOrganization(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBOrganization(updatedOrg), nil
}

// DeleteOrganization deletes an Organization.
func (s *OrganizationServer) DeleteOrganization(ctx context.Context, req *pb.DeleteOrganizationRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &organization.DeleteOrganizationInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toDelete.Version = &v
	}

	if err := s.organizationService.DeleteOrganization(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// toPBOrganization converts from Organization model to ProtoBuf model.
func (*OrganizationServer) toPBOrganization(org *models.Organization) *pb.Organization {
	return &pb.Organization{
		Metadata:    toPBMetadata(&org.Metadata, gid.OrganizationType),
		Name:        org.Name,
		Description: org.Description,
		CreatedBy:   org.CreatedBy,
	}
}
