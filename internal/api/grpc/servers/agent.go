package servers

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// AgentServer embeds the UnimplementedAgentsServer.
type AgentServer struct {
	pb.UnimplementedAgentsServer
	agentService agent.Service
	urnResolver  *urn.Resolver
}

// NewAgentServer returns an instance of AgentServer.
func NewAgentServer(
	agentService agent.Service,
	urnResolver *urn.Resolver,
) *AgentServer {
	return &AgentServer{
		agentService: agentService,
		urnResolver:  urnResolver,
	}
}

// CreateAgentSession creates an AgentSession.
func (s *AgentServer) CreateAgentSession(ctx context.Context, req *pb.CreateAgentSessionRequest) (*pb.AgentSession, error) {
	agentID, err := s.urnResolver.ResolveResourceID(ctx, req.AgentId)
	if err != nil {
		return nil, err
	}

	input := &agent.CreateAgentSessionInput{
		AgentID: agentID,
	}

	createdAgentSession, err := s.agentService.CreateAgentSession(ctx, input)
	if err != nil {
		return nil, err
	}

	return s.toPBAgentSession(createdAgentSession), nil
}

// GetAgentByID returns an Agent by an ID.
func (s *AgentServer) GetAgentByID(ctx context.Context, req *pb.GetAgentByIDRequest) (*pb.Agent, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotAgent, err := s.agentService.GetAgentByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBAgent(gotAgent), nil
}

// GetAgents returns a paginated list of Agents.
func (s *AgentServer) GetAgents(ctx context.Context, req *pb.GetAgentsRequest) (*pb.GetAgentsResponse, error) {
	sort := db.AgentSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	orgID, err := s.urnResolver.ResolveResourceID(ctx, models.OrganizationResource.BuildPRN(req.OrganizationName))
	if err != nil {
		return nil, err
	}

	input := &agent.GetAgentsInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		OrganizationID:    &orgID,
	}

	result, err := s.agentService.GetAgents(ctx, input)
	if err != nil {
		return nil, err
	}

	agents := result.Agents

	pbAgents := make([]*pb.Agent, len(agents))
	for ix := range agents {
		pbAgents[ix] = s.toPBAgent(&agents[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(agents) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&agents[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&agents[len(agents)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetAgentsResponse{
		PageInfo: pageInfo,
		Agents:   pbAgents,
	}, nil
}

// CreateAgent creates an Agent.
func (s *AgentServer) CreateAgent(ctx context.Context, req *pb.CreateAgentRequest) (*pb.Agent, error) {
	orgID, err := s.urnResolver.ResolveResourceID(ctx, models.OrganizationResource.BuildPRN(req.OrganizationName))
	if err != nil {
		return nil, err
	}
	input := &agent.CreateAgentInput{
		Type:            models.OrganizationAgent,
		Name:            req.Name,
		Description:     req.Description,
		OrganizationID:  &orgID,
		Disabled:        req.Disabled,
		RunUntaggedJobs: req.RunUntaggedJobs,
		Tags:            req.Tags,
	}

	createdAgent, err := s.agentService.CreateAgent(ctx, input)
	if err != nil {
		return nil, err
	}

	return s.toPBAgent(createdAgent), nil
}

// UpdateAgent updates an Agent.
func (s *AgentServer) UpdateAgent(ctx context.Context, req *pb.UpdateAgentRequest) (*pb.Agent, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	input := &agent.UpdateAgentInput{
		ID:              id,
		Description:     req.Description,
		Disabled:        req.Disabled,
		RunUntaggedJobs: req.RunUntaggedJobs,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		input.MetadataVersion = &v
	}

	updatedAgent, err := s.agentService.UpdateAgent(ctx, input)
	if err != nil {
		return nil, err
	}

	return s.toPBAgent(updatedAgent), nil
}

// DeleteAgent deletes an Agent.
func (s *AgentServer) DeleteAgent(ctx context.Context, req *pb.DeleteAgentRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	input := &agent.DeleteAgentInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		input.MetadataVersion = &v
	}

	err = s.agentService.DeleteAgent(ctx, input)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// AssignServiceAccountToAgent assigns a service account to an agent.
func (s *AgentServer) AssignServiceAccountToAgent(ctx context.Context, req *pb.AssignServiceAccountToAgentRequest) (*emptypb.Empty, error) {
	agentID, err := s.urnResolver.ResolveResourceID(ctx, req.AgentId)
	if err != nil {
		return nil, err
	}

	serviceAccountID, err := s.urnResolver.ResolveResourceID(ctx, req.ServiceAccountId)
	if err != nil {
		return nil, err
	}

	input := &agent.AssignServiceAccountToAgentInput{
		ID:               agentID,
		ServiceAccountID: serviceAccountID,
	}

	err = s.agentService.AssignServiceAccountToAgent(ctx, input)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// UnassignServiceAccountFromAgent unassigns a service account from an agent.
func (s *AgentServer) UnassignServiceAccountFromAgent(ctx context.Context, req *pb.UnassignServiceAccountFromAgentRequest) (*emptypb.Empty, error) {
	agentID, err := s.urnResolver.ResolveResourceID(ctx, req.AgentId)
	if err != nil {
		return nil, err
	}

	serviceAccountID, err := s.urnResolver.ResolveResourceID(ctx, req.ServiceAccountId)
	if err != nil {
		return nil, err
	}

	input := &agent.UnassignServiceAccountFromAgentInput{
		ID:               agentID,
		ServiceAccountID: serviceAccountID,
	}

	err = s.agentService.UnassignServiceAccountFromAgent(ctx, input)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// SendAgentSessionHeartbeat sends an agent session heartbeat.
func (s *AgentServer) SendAgentSessionHeartbeat(ctx context.Context, req *pb.AgentSessionHeartbeatRequest) (*emptypb.Empty, error) {
	agentSessionID, err := s.urnResolver.ResolveResourceID(ctx, req.AgentSessionId)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, s.agentService.AcceptAgentSessionHeartbeat(ctx, agentSessionID)
}

// CreateAgentSessionError creates an agent session error.
func (s *AgentServer) CreateAgentSessionError(ctx context.Context, req *pb.CreateAgentSessionErrorRequest) (*emptypb.Empty, error) {
	agentSessionID, err := s.urnResolver.ResolveResourceID(ctx, req.AgentSessionId)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, s.agentService.CreateAgentSessionError(ctx, agentSessionID, req.ErrorMessage)
}

// toPBAgent converts an Agent to a protobuf Agent.
func (*AgentServer) toPBAgent(a *models.Agent) *pb.Agent {
	agt := &pb.Agent{
		Metadata:        toPBMetadata(&a.Metadata, gid.AgentType),
		Name:            a.Name,
		Description:     a.Description,
		Type:            string(a.Type),
		CreatedBy:       a.CreatedBy,
		Disabled:        a.Disabled,
		Tags:            a.Tags,
		RunUntaggedJobs: a.RunUntaggedJobs,
	}

	if a.Type.Equals(models.OrganizationAgent) {
		agt.OrganizationId = ptr.String(gid.ToGlobalID(gid.OrganizationType, *a.OrganizationID))
	}

	return agt
}

// toPBAgentSession converts an AgentSession to a protobuf AgentSession.
func (*AgentServer) toPBAgentSession(session *models.AgentSession) *pb.AgentSession {
	agtSession := &pb.AgentSession{
		Metadata:      toPBMetadata(&session.Metadata, gid.AgentSessionType),
		AgentId:       session.AgentID,
		LastContacted: timestamppb.New(session.LastContactTimestamp),
	}

	return agtSession
}
