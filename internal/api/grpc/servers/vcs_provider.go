package servers

import (
	"context"
	"fmt"
	"io"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// VCSProviderServer embeds the UnimplementedVCSProvidersServer.
type VCSProviderServer struct {
	pb.UnimplementedVCSProvidersServer
	logger      logger.Logger
	vcsService  vcs.Service
	urnResolver *urn.Resolver
}

// NewVCSProviderServer returns an instance of VCSProviderServer.
func NewVCSProviderServer(
	logger logger.Logger,
	vcsService vcs.Service,
	urnResolver *urn.Resolver,
) *VCSProviderServer {
	return &VCSProviderServer{
		logger:      logger,
		vcsService:  vcsService,
		urnResolver: urnResolver,
	}
}

// GetVCSProviderByID returns a VCSProvider by its ID.
func (s *VCSProviderServer) GetVCSProviderByID(ctx context.Context, req *pb.GetVCSProviderByIDRequest) (*pb.VCSProvider, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	provider, err := s.vcsService.GetVCSProviderByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBVCSProvider(provider), nil
}

// GetVCSProviders returns a paginated list of VCSProviders.
func (s *VCSProviderServer) GetVCSProviders(ctx context.Context, req *pb.GetVCSProvidersRequest) (*pb.GetVCSProvidersResponse, error) {
	sort := db.VCSProviderSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &vcs.GetVCSProvidersInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		Search:            req.Search,
	}

	if req.OrganizationId != nil {
		orgID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetOrganizationId())
		if rErr != nil {
			return nil, rErr
		}

		input.OrganizationID = &orgID
	}

	if req.ProjectId != nil {
		projectID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if rErr != nil {
			return nil, rErr
		}

		input.ProjectID = &projectID
	}

	dbResult, err := s.vcsService.GetVCSProviders(ctx, input)
	if err != nil {
		return nil, err
	}

	vcsProviders := dbResult.VCSProviders

	pbVCSProviders := make([]*pb.VCSProvider, len(vcsProviders))
	for ix := range vcsProviders {
		pbVCSProviders[ix] = s.toPBVCSProvider(&vcsProviders[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     dbResult.PageInfo.HasNextPage,
		HasPreviousPage: dbResult.PageInfo.HasPreviousPage,
		TotalCount:      dbResult.PageInfo.TotalCount,
	}

	if len(vcsProviders) > 0 {
		pageInfo.StartCursor, err = dbResult.PageInfo.Cursor(&vcsProviders[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = dbResult.PageInfo.Cursor(&vcsProviders[len(vcsProviders)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetVCSProvidersResponse{
		PageInfo:     pageInfo,
		VcsProviders: pbVCSProviders,
	}, nil
}

// CreateVCSProvider creates a new VCSProvider.
func (s *VCSProviderServer) CreateVCSProvider(ctx context.Context, req *pb.CreateVCSProviderRequest) (*pb.CreateVCSProviderResponse, error) {
	var response *vcs.CreateVCSProviderResponse

	switch models.ScopeType(req.Scope.String()) {
	case models.OrganizationScope:
		if req.OrganizationId == nil {
			return nil, errors.New("organization id is required for a organization vcs provider", errors.WithErrorCode(errors.EInvalid))
		}

		organizationID, err := s.urnResolver.ResolveResourceID(ctx, req.GetOrganizationId())
		if err != nil {
			return nil, err
		}

		response, err = s.vcsService.CreateOrganizationVCSProvider(ctx, &vcs.CreateOrganizationVCSProviderInput{
			Name:                req.Name,
			URL:                 req.Url,
			Description:         req.Description,
			OAuthClientID:       req.OauthClientId,
			OAuthClientSecret:   req.OauthClientSecret,
			ExtraOAuthScopes:    req.ExtraOauthScopes,
			PersonalAccessToken: req.PersonalAccessToken,
			OrganizationID:      organizationID,
			Type:                models.VCSProviderType(req.Type.String()),
			AuthType:            models.VCSProviderAuthType(req.AuthType.String()),
		})
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if req.ProjectId == nil {
			return nil, errors.New("project id is required for a project vcs provider", errors.WithErrorCode(errors.EInvalid))
		}

		projectID, err := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if err != nil {
			return nil, err
		}

		response, err = s.vcsService.CreateProjectVCSProvider(ctx, &vcs.CreateProjectVCSProviderInput{
			Name:                req.Name,
			URL:                 req.Url,
			Description:         req.Description,
			ProjectID:           projectID,
			OAuthClientID:       req.OauthClientId,
			OAuthClientSecret:   req.OauthClientSecret,
			ExtraOAuthScopes:    req.ExtraOauthScopes,
			PersonalAccessToken: req.PersonalAccessToken,
			Type:                models.VCSProviderType(req.Type.String()),
			AuthType:            models.VCSProviderAuthType(req.AuthType.String()),
		})
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unknown vcs provider scope %s", req.Scope, errors.WithErrorCode(errors.EInvalid))
	}

	return &pb.CreateVCSProviderResponse{
		VcsProvider:           s.toPBVCSProvider(response.VCSProvider),
		OauthAuthorizationUrl: response.OAuthAuthorizationURL,
	}, nil
}

// UpdateVCSProvider updates a existing VCSProvider.
func (s *VCSProviderServer) UpdateVCSProvider(ctx context.Context, req *pb.UpdateVCSProviderRequest) (*pb.VCSProvider, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toUpdate := &vcs.UpdateVCSProviderInput{
		ID:                  id,
		Description:         req.Description,
		OAuthClientID:       req.OauthClientId,
		OAuthClientSecret:   req.OauthClientSecret,
		ExtraOAuthScopes:    req.ExtraOauthScopes,
		PersonalAccessToken: req.PersonalAccessToken,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toUpdate.Version = &v
	}

	updatedProvider, err := s.vcsService.UpdateVCSProvider(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBVCSProvider(updatedProvider), nil
}

// DeleteVCSProvider deletes a VCSProvider.
func (s *VCSProviderServer) DeleteVCSProvider(ctx context.Context, req *pb.DeleteVCSProviderRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &vcs.DeleteVCSProviderInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toDelete.Version = &v
	}

	if err = s.vcsService.DeleteVCSProvider(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// ResetVCSProviderOAuthToken resets the OAuth token used to authenticate with
// the remote VCS system.
func (s *VCSProviderServer) ResetVCSProviderOAuthToken(ctx context.Context, req *pb.ResetVCSProviderOAuthTokenRequest) (*pb.ResetVCSProviderOAuthTokenResponse, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.ProviderId)
	if err != nil {
		return nil, err
	}

	resetResponse, err := s.vcsService.ResetVCSProviderOAuthToken(ctx, &vcs.ResetVCSProviderOAuthTokenInput{
		ProviderID: id,
	})
	if err != nil {
		return nil, err
	}

	return &pb.ResetVCSProviderOAuthTokenResponse{
		VcsProvider:           s.toPBVCSProvider(resetResponse.VCSProvider),
		OauthAuthorizationUrl: resetResponse.OAuthAuthorizationURL,
	}, nil
}

// GetRepositoryArchive returns the archive for a given repository.
func (s *VCSProviderServer) GetRepositoryArchive(req *pb.GetRepositoryArchiveRequest, server pb.VCSProviders_GetRepositoryArchiveServer) error {
	providerID, err := s.urnResolver.ResolveResourceID(server.Context(), req.ProviderId)
	if err != nil {
		return err
	}

	reader, err := s.vcsService.GetRepositoryArchive(server.Context(), &vcs.GetRepositoryArchiveInput{
		ProviderID:     providerID,
		Ref:            req.Ref,
		RepositoryPath: req.RepositoryPath,
	})
	if err != nil {
		return err
	}

	// Make we close the reader once we're done.
	defer func() {
		if err = reader.Close(); err != nil {
			s.logger.Errorf("error closing data reader in grpc GetRepositoryArchive: %v", err)
		}
	}()

	chunk := &pb.GetRepositoryArchiveResponse{ChunkData: make([]byte, chunkSize)}

	var n int
	for {
		n, err = reader.Read(chunk.ChunkData)
		if err != nil && err != io.EOF {
			return fmt.Errorf("error reading repository archive data: %w", err)
		}

		if n > 0 {
			chunk.ChunkData = chunk.ChunkData[:n]
			serverErr := server.Send(chunk)
			if serverErr != nil {
				return fmt.Errorf("error sending repository archive data: %w", serverErr)
			}
		}

		if err == io.EOF {
			return nil
		}
	}
}

// CreateVCSTokenForPipeline creates a new VCS token for a pipeline.
func (s *VCSProviderServer) CreateVCSTokenForPipeline(ctx context.Context, req *pb.CreateVCSTokenForPipelineRequest) (*pb.CreateVCSTokenForPipelineResponse, error) {
	providerID, err := s.urnResolver.ResolveResourceID(ctx, req.ProviderId)
	if err != nil {
		return nil, err
	}

	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	response, err := s.vcsService.CreateVCSTokenForPipeline(ctx, &vcs.CreateVCSTokenForPipelineInput{
		ProviderID: providerID,
		PipelineID: pipelineID,
	})
	if err != nil {
		return nil, err
	}

	var expiresAt *timestamppb.Timestamp
	if response.ExpiresAt != nil {
		expiresAt = timestamppb.New(*response.ExpiresAt)
	}

	return &pb.CreateVCSTokenForPipelineResponse{
		AccessToken: response.AccessToken,
		ExpiresAt:   expiresAt,
	}, nil
}

// toPBVCSProvider converts a VCSProvider model to a protobuf equivalent.
func (s *VCSProviderServer) toPBVCSProvider(vp *models.VCSProvider) *pb.VCSProvider {
	provider := &pb.VCSProvider{
		Metadata:         toPBMetadata(&vp.Metadata, gid.VCSProviderType),
		Name:             vp.Name,
		Description:      vp.Description,
		Scope:            string(vp.Scope),
		Type:             string(vp.Type),
		Url:              vp.URL.String(),
		CreatedBy:        vp.CreatedBy,
		OrganizationId:   gid.ToGlobalID(gid.OrganizationType, vp.OrganizationID),
		AuthType:         string(vp.AuthType),
		ExtraOauthScopes: vp.ExtraOAuthScopes,
	}

	if vp.ProjectID != nil {
		provider.ProjectId = ptr.String(gid.ToGlobalID(gid.ProjectType, *vp.ProjectID))
	}

	return provider
}
