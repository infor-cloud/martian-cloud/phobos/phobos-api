package servers

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
)

// ServiceAccountServer embeds the UnimplementedServiceAccountsServer.
type ServiceAccountServer struct {
	pb.UnimplementedServiceAccountsServer
	serviceAccountService serviceaccount.Service
	urnResolver           *urn.Resolver
}

// NewServiceAccountServer returns an instance of ServiceAccountServer.
func NewServiceAccountServer(
	serviceAccountService serviceaccount.Service,
	urnResolver *urn.Resolver,
) *ServiceAccountServer {
	return &ServiceAccountServer{
		serviceAccountService: serviceAccountService,
		urnResolver:           urnResolver,
	}
}

// GetServiceAccountByID returns a service account by its ID.
func (s *ServiceAccountServer) GetServiceAccountByID(ctx context.Context, req *pb.GetServiceAccountByIDRequest) (*pb.ServiceAccount, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotSa, err := s.serviceAccountService.GetServiceAccountByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBServiceAccount(gotSa), nil
}

// GetServiceAccounts returns a list of service accounts.
func (s *ServiceAccountServer) GetServiceAccounts(ctx context.Context, req *pb.GetServiceAccountsRequest) (*pb.GetServiceAccountsResponse, error) {
	sort := db.ServiceAccountSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &serviceaccount.GetServiceAccountsInput{
		Search:            req.Search,
		Sort:              &sort,
		PaginationOptions: paginationOpts,
	}

	for _, s := range req.Scopes {
		input.ServiceAccountScopes = append(input.ServiceAccountScopes, models.ScopeType(s.String()))
	}

	if req.OrganizationId != nil {
		orgID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetOrganizationId())
		if rErr != nil {
			return nil, rErr
		}

		input.OrganizationID = &orgID
	}

	if req.ProjectId != nil {
		projectID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if rErr != nil {
			return nil, rErr
		}

		input.ProjectID = &projectID
	}

	if req.AgentId != nil {
		agentID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetOrganizationId())
		if rErr != nil {
			return nil, rErr
		}

		input.AgentID = &agentID
	}

	dbResult, err := s.serviceAccountService.GetServiceAccounts(ctx, input)
	if err != nil {
		return nil, err
	}

	serviceAccounts := dbResult.ServiceAccounts

	pbServiceAccounts := make([]*pb.ServiceAccount, len(serviceAccounts))
	for ix := range serviceAccounts {
		pbServiceAccounts[ix] = s.toPBServiceAccount(serviceAccounts[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     dbResult.PageInfo.HasNextPage,
		HasPreviousPage: dbResult.PageInfo.HasPreviousPage,
		TotalCount:      dbResult.PageInfo.TotalCount,
	}

	if len(serviceAccounts) > 0 {
		pageInfo.StartCursor, err = dbResult.PageInfo.Cursor(serviceAccounts[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = dbResult.PageInfo.Cursor(serviceAccounts[len(serviceAccounts)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetServiceAccountsResponse{
		PageInfo:        pageInfo,
		ServiceAccounts: pbServiceAccounts,
	}, nil
}

// CreateServiceAccount creates a new service account.
func (s *ServiceAccountServer) CreateServiceAccount(ctx context.Context, req *pb.CreateServiceAccountRequest) (*pb.ServiceAccount, error) {
	toCreate := &serviceaccount.CreateServiceAccountInput{
		Name:              req.Name,
		Description:       req.Description,
		Scope:             models.ScopeType(req.Scope.String()),
		OIDCTrustPolicies: s.fromPBTrustPolicies(req.OidcTrustPolicies),
	}

	if req.OrgId != nil {
		orgID, err := s.urnResolver.ResolveResourceID(ctx, req.GetOrgId())
		if err != nil {
			return nil, err
		}

		toCreate.OrgID = &orgID
	}

	if req.ProjectId != nil {
		projectID, err := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if err != nil {
			return nil, err
		}

		toCreate.ProjectID = &projectID
	}

	createdSA, err := s.serviceAccountService.CreateServiceAccount(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBServiceAccount(createdSA), nil
}

// UpdateServiceAccount updates an existing service account.
func (s *ServiceAccountServer) UpdateServiceAccount(ctx context.Context, req *pb.UpdateServiceAccountRequest) (*pb.ServiceAccount, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toUpdate := &serviceaccount.UpdateServiceAccountInput{
		ID:                id,
		Description:       req.Description,
		OIDCTrustPolicies: s.fromPBTrustPolicies(req.OidcTrustPolicies),
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toUpdate.MetadataVersion = &v
	}

	updatedSA, err := s.serviceAccountService.UpdateServiceAccount(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBServiceAccount(updatedSA), nil
}

// DeleteServiceAccount deletes an existing service account.
func (s *ServiceAccountServer) DeleteServiceAccount(ctx context.Context, req *pb.DeleteServiceAccountRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &serviceaccount.DeleteServiceAccountInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toDelete.MetadataVersion = &v
	}

	err = s.serviceAccountService.DeleteServiceAccount(ctx, toDelete)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// CreateToken creates a new token for a service account using an existing
// token.
func (s *ServiceAccountServer) CreateToken(ctx context.Context, req *pb.CreateTokenRequest) (*pb.CreateTokenResponse, error) {
	// Error is deliberately ignored here as the create token request is not authenticated.
	// CreateToken should return custom errors instead.
	serviceAccountID, _ := s.urnResolver.ResolveResourceID(auth.WithCaller(ctx, &auth.SystemCaller{}), req.ServiceAccountId)

	toCreate := &serviceaccount.CreateTokenInput{
		ServiceAccountID: serviceAccountID,
		Token:            req.Token,
	}

	resp, err := s.serviceAccountService.CreateToken(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return &pb.CreateTokenResponse{
		Token:     resp.Token,
		ExpiresIn: resp.ExpiresIn,
	}, nil
}

// fromPBTrustPolicies converts a list of trust policies from their protobuf representation.
func (*ServiceAccountServer) fromPBTrustPolicies(policies []*pb.OIDCTrustPolicy) []models.OIDCTrustPolicy {
	trustPolicies := make([]models.OIDCTrustPolicy, len(policies))
	for i, policy := range policies {
		trustPolicies[i] = models.OIDCTrustPolicy{
			Issuer:      policy.Issuer,
			BoundClaims: policy.BoundClaims,
		}
	}

	return trustPolicies
}

// toPBServiceAccount converts a service account to its protobuf representation.
func (*ServiceAccountServer) toPBServiceAccount(sa *models.ServiceAccount) *pb.ServiceAccount {
	trustPolicies := make([]*pb.OIDCTrustPolicy, len(sa.OIDCTrustPolicies))
	for i, policy := range sa.OIDCTrustPolicies {
		trustPolicies[i] = &pb.OIDCTrustPolicy{
			Issuer:      policy.Issuer,
			BoundClaims: policy.BoundClaims,
		}
	}

	var organizationID *string
	if sa.Scope.Equals(models.OrganizationScope) {
		organizationID = ptr.String(gid.ToGlobalID(gid.OrganizationType, *sa.OrganizationID))
	}

	var projectID *string
	if sa.ProjectID != nil {
		projectID = ptr.String(gid.ToGlobalID(gid.ProjectType, *sa.ProjectID))
	}

	return &pb.ServiceAccount{
		Metadata:          toPBMetadata(&sa.Metadata, gid.ServiceAccountType),
		Name:              sa.Name,
		Description:       sa.Description,
		OrganizationId:    organizationID,
		CreatedBy:         sa.CreatedBy,
		Scope:             string(sa.Scope),
		ProjectId:         projectID,
		OidcTrustPolicies: trustPolicies,
	}
}
