package servers

import (
	"context"
	"strconv"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/grpc/reader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	// maxPluginVersionReadmeUploadSize defines the maximum size of the plugin version readme upload.
	maxPluginVersionReadmeUploadSize = 1024 * 1024 * 5 // 5MiB
	// maxPluginVersionShasumUploadSize defines the maximum size of the plugin version shasum upload.
	maxPluginVersionShasumUploadSize = 1024 * 1024 * 5 // 5MiB
	// maxPluginVersionSchemaUploadSize defines the maximum size of the plugin version schema
	maxPluginVersionSchemaUploadSize = 1024 * 1024 * 5 // 5MiB
	// maxPluginVersionDocFileUploadSize defines the maximum size of a plugin doc file
	maxPluginVersionDocFileUploadSize = 1024 * 1024 * 5 // 5MiB
)

// PluginRegistryServer embeds the UnimplementedPluginRegistryServer.
type PluginRegistryServer struct {
	pb.UnimplementedPluginRegistryServer
	pluginRegistryService     pluginregistry.Service
	urnResolver               *urn.Resolver
	maxPluginBinaryUploadSize int64
}

// NewPluginRegistryServer returns an instance of PluginRegistryServer.
func NewPluginRegistryServer(
	pluginRegistryService pluginregistry.Service,
	urnResolver *urn.Resolver,
	maxPluginBinaryUploadSize int64,
) *PluginRegistryServer {
	return &PluginRegistryServer{
		pluginRegistryService:     pluginRegistryService,
		urnResolver:               urnResolver,
		maxPluginBinaryUploadSize: maxPluginBinaryUploadSize,
	}
}

// GetPluginByID returns a plugin by its ID.
func (s *PluginRegistryServer) GetPluginByID(ctx context.Context, req *pb.GetPluginByIdRequest) (*pb.Plugin, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	plugin, err := s.pluginRegistryService.GetPluginByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBPlugin(plugin), nil
}

// GetPlugins returns a paginated list of plugins.
func (s *PluginRegistryServer) GetPlugins(ctx context.Context, req *pb.GetPluginsRequest) (*pb.GetPluginsResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.PluginSortableField(s.trimSortPrefix(req.GetSort().String()))

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &pluginregistry.GetPluginsInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		Search:            req.Search,
	}

	if req.OrganizationId != nil {
		organizationID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetOrganizationId())
		if rErr != nil {
			return nil, rErr
		}

		input.OrganizationID = &organizationID
	}

	result, err := s.pluginRegistryService.GetPlugins(ctx, input)
	if err != nil {
		return nil, err
	}

	plugins := result.Plugins

	pbPlugins := make([]*pb.Plugin, len(plugins))
	for ix := range plugins {
		pbPlugins[ix] = s.toPBPlugin(&plugins[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(plugins) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&plugins[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&plugins[len(plugins)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetPluginsResponse{
		PageInfo: pageInfo,
		Plugins:  pbPlugins,
	}, nil
}

// GetPluginVersionByID returns a plugin version by its ID.
func (s *PluginRegistryServer) GetPluginVersionByID(ctx context.Context, req *pb.GetPluginVersionByIdRequest) (*pb.PluginVersion, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	pluginVersion, err := s.pluginRegistryService.GetPluginVersionByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBPluginVersion(pluginVersion), nil
}

// GetPluginVersions returns a paginated list of plugin versions.
func (s *PluginRegistryServer) GetPluginVersions(ctx context.Context, req *pb.GetPluginVersionsRequest) (*pb.GetPluginVersionsResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.PluginVersionSortableField(s.trimSortPrefix(req.GetSort().String()))

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	pluginID, err := s.urnResolver.ResolveResourceID(ctx, req.PluginId)
	if err != nil {
		return nil, err
	}

	// Only return versions with SHA sums uploaded.
	input := &pluginregistry.GetPluginVersionsInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		Latest:            req.Latest,
		SHASumsUploaded:   req.ShaSumsUploaded,
		SemanticVersion:   req.SemanticVersion,
		PluginID:          pluginID,
	}

	result, err := s.pluginRegistryService.GetPluginVersions(ctx, input)
	if err != nil {
		return nil, err
	}

	pluginVersions := result.PluginVersions

	pbPluginVersions := make([]*pb.PluginVersion, len(pluginVersions))
	for ix := range pluginVersions {
		pbPluginVersions[ix] = s.toPBPluginVersion(&pluginVersions[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(pluginVersions) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&pluginVersions[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&pluginVersions[len(pluginVersions)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetPluginVersionsResponse{
		PageInfo:       pageInfo,
		PluginVersions: pbPluginVersions,
	}, nil
}

// GetPluginPlatformByID returns a plugin platform by its ID.
func (s *PluginRegistryServer) GetPluginPlatformByID(ctx context.Context, req *pb.GetPluginPlatformByIdRequest) (*pb.PluginPlatform, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	pluginPlatform, err := s.pluginRegistryService.GetPluginPlatformByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBPluginPlatform(pluginPlatform), nil
}

// GetPluginPlatforms returns a paginated list of plugin platforms.
func (s *PluginRegistryServer) GetPluginPlatforms(ctx context.Context, req *pb.GetPluginPlatformsRequest) (*pb.GetPluginPlatformsResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.PluginPlatformSortableField(s.trimSortPrefix(req.GetSort().String()))

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	// Only return platforms with binaries uploaded.
	input := &pluginregistry.GetPluginPlatformsInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		BinaryUploaded:    req.BinaryUploaded,
		OperatingSystem:   req.OperatingSystem,
		Architecture:      req.Architecture,
	}

	if req.PluginId != nil {
		pluginID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetPluginId())
		if rErr != nil {
			return nil, rErr
		}
		input.PluginID = &pluginID
	}

	if req.PluginVersionId != nil {
		pluginVersionID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetPluginVersionId())
		if rErr != nil {
			return nil, rErr
		}
		input.PluginVersionID = &pluginVersionID
	}

	result, err := s.pluginRegistryService.GetPluginPlatforms(ctx, input)
	if err != nil {
		return nil, err
	}

	pluginPlatforms := result.PluginPlatforms

	pbPluginPlatforms := make([]*pb.PluginPlatform, len(pluginPlatforms))
	for ix := range pluginPlatforms {
		pbPluginPlatforms[ix] = s.toPBPluginPlatform(&pluginPlatforms[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(pluginPlatforms) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&pluginPlatforms[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&pluginPlatforms[len(pluginPlatforms)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetPluginPlatformsResponse{
		PageInfo:        pageInfo,
		PluginPlatforms: pbPluginPlatforms,
	}, nil
}

// CreatePlugin creates a new plugin.
func (s *PluginRegistryServer) CreatePlugin(ctx context.Context, req *pb.CreatePluginRequest) (*pb.Plugin, error) {
	organizationID, err := s.urnResolver.ResolveResourceID(ctx, req.GetOrganizationId())
	if err != nil {
		return nil, err
	}

	toCreate := &pluginregistry.CreatePluginInput{
		OrganizationID: organizationID,
		Name:           req.Name,
		RepositoryURL:  req.RepositoryUrl,
		Private:        req.Private,
	}

	createdPlugin, err := s.pluginRegistryService.CreatePlugin(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBPlugin(createdPlugin), nil
}

// CreatePluginVersion creates a new plugin version.
func (s *PluginRegistryServer) CreatePluginVersion(ctx context.Context, req *pb.CreatePluginVersionRequest) (*pb.PluginVersion, error) {
	pluginID, err := s.urnResolver.ResolveResourceID(ctx, req.PluginId)
	if err != nil {
		return nil, err
	}

	toCreate := &pluginregistry.CreatePluginVersionInput{
		PluginID:        pluginID,
		SemanticVersion: req.SemanticVersion,
		Protocols:       req.Protocols,
	}

	pluginVersion, err := s.pluginRegistryService.CreatePluginVersion(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBPluginVersion(pluginVersion), nil
}

// CreatePluginPlatform creates a new plugin platform.
func (s *PluginRegistryServer) CreatePluginPlatform(ctx context.Context, req *pb.CreatePluginPlatformRequest) (*pb.PluginPlatform, error) {
	pluginVersionID, err := s.urnResolver.ResolveResourceID(ctx, req.GetPluginVersionId())
	if err != nil {
		return nil, err
	}

	toCreate := &pluginregistry.CreatePluginPlatformInput{
		PluginVersionID: pluginVersionID,
		OperatingSystem: req.OperatingSystem,
		Architecture:    req.Architecture,
		SHASum:          req.ShaSum,
		Filename:        req.Filename,
	}

	createdPluginPlatform, err := s.pluginRegistryService.CreatePluginPlatform(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBPluginPlatform(createdPluginPlatform), nil
}

// UpdatePlugin updates a plugin.
func (s *PluginRegistryServer) UpdatePlugin(ctx context.Context, req *pb.UpdatePluginRequest) (*pb.Plugin, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toUpdate := &pluginregistry.UpdatePluginInput{
		ID:            id,
		RepositoryURL: req.RepositoryUrl,
		Private:       req.Private,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toUpdate.Version = &v
	}

	updatedPlugin, err := s.pluginRegistryService.UpdatePlugin(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBPlugin(updatedPlugin), nil
}

// UploadPluginVersionDocFile uploads a documentation file for a plugin version.
func (s *PluginRegistryServer) UploadPluginVersionDocFile(stream pb.PluginRegistry_UploadPluginVersionDocFileServer) error {
	// First message is request metadata.
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	info := req.GetInfo()

	pluginVersionID, err := s.urnResolver.ResolveResourceID(stream.Context(), info.PluginVersionId)
	if err != nil {
		return err
	}

	return s.pluginRegistryService.UploadPluginVersionDocFile(
		stream.Context(),
		&pluginregistry.UploadPluginVersionDocFileInput{
			PluginVersionID: pluginVersionID,
			Category:        models.PluginVersionDocCategory(info.Category.String()),
			Subcategory:     info.Subcategory,
			Name:            info.Name,
			Title:           info.Title,
			Body: reader.NewLimitReader(func() ([]byte, error) {
				chunkReq, err := stream.Recv()
				if err != nil {
					// Return the error whether it's EOF or something else.
					return nil, err
				}

				return chunkReq.GetChunkData(), nil
			}, maxPluginVersionDocFileUploadSize),
		},
	)
}

// UploadPluginVersionReadme uploads a readme for a plugin version.
func (s *PluginRegistryServer) UploadPluginVersionReadme(stream pb.PluginRegistry_UploadPluginVersionReadmeServer) error {
	// First message is request metadata.
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	pluginVersionID, err := s.urnResolver.ResolveResourceID(stream.Context(), req.GetInfo().PluginVersionId)
	if err != nil {
		return err
	}

	return s.pluginRegistryService.UploadPluginVersionReadme(
		stream.Context(),
		pluginVersionID,
		reader.NewLimitReader(func() ([]byte, error) {
			chunkReq, err := stream.Recv()
			if err != nil {
				// Return the error whether it's EOF or something else.
				return nil, err
			}

			return chunkReq.GetChunkData(), nil
		}, maxPluginVersionReadmeUploadSize),
	)
}

// UploadPluginVersionSchema uploads a schema for a plugin version.
func (s *PluginRegistryServer) UploadPluginVersionSchema(stream pb.PluginRegistry_UploadPluginVersionSchemaServer) error {
	// First message is request metadata.
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	pluginVersionID, err := s.urnResolver.ResolveResourceID(stream.Context(), req.GetInfo().PluginVersionId)
	if err != nil {
		return err
	}

	return s.pluginRegistryService.UploadPluginVersionSchema(
		stream.Context(),
		pluginVersionID,
		reader.NewLimitReader(func() ([]byte, error) {
			chunkReq, err := stream.Recv()
			if err != nil {
				// Return the error whether it's EOF or something else.
				return nil, err
			}

			return chunkReq.GetChunkData(), nil
		}, maxPluginVersionSchemaUploadSize),
	)
}

// UploadPluginVersionShaSums uploads SHA sums for a plugin version.
func (s *PluginRegistryServer) UploadPluginVersionShaSums(stream pb.PluginRegistry_UploadPluginVersionShaSumsServer) error {
	// First message is request metadata.
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	pluginVersionID, err := s.urnResolver.ResolveResourceID(stream.Context(), req.GetInfo().PluginVersionId)
	if err != nil {
		return err
	}

	return s.pluginRegistryService.UploadPluginVersionSHASums(
		stream.Context(),
		pluginVersionID,
		reader.NewLimitReader(func() ([]byte, error) {
			chunkReq, err := stream.Recv()
			if err != nil {
				// Return the error whether it's EOF or something else.
				return nil, err
			}

			return chunkReq.GetChunkData(), nil
		}, maxPluginVersionShasumUploadSize),
	)
}

// UploadPluginPlatformBinary uploads a plugin platform binary.
func (s *PluginRegistryServer) UploadPluginPlatformBinary(stream pb.PluginRegistry_UploadPluginPlatformBinaryServer) error {
	// First message is request metadata.
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	id, err := s.urnResolver.ResolveResourceID(stream.Context(), req.GetInfo().PluginPlatformId)
	if err != nil {
		return err
	}

	return s.pluginRegistryService.UploadPluginPlatformBinary(
		stream.Context(),
		id,
		reader.NewLimitReader(func() ([]byte, error) {
			chunkReq, err := stream.Recv()
			if err != nil {
				// Return the error whether it's EOF or something else.
				return nil, err
			}

			return chunkReq.GetChunkData(), nil
		}, s.maxPluginBinaryUploadSize),
	)
}

// DeletePlugin deletes a plugin.
func (s *PluginRegistryServer) DeletePlugin(ctx context.Context, req *pb.DeletePluginRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &pluginregistry.DeletePluginInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toDelete.Version = &v
	}

	if err = s.pluginRegistryService.DeletePlugin(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// DeletePluginVersion deletes a plugin version by its ID.
func (s *PluginRegistryServer) DeletePluginVersion(ctx context.Context, req *pb.DeletePluginVersionRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &pluginregistry.DeletePluginVersionInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toDelete.Version = &v
	}

	if err = s.pluginRegistryService.DeletePluginVersion(ctx, toDelete); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// DeletePluginPlatform deletes a plugin platform by its ID.
func (s *PluginRegistryServer) DeletePluginPlatform(ctx context.Context, req *pb.DeletePluginPlatformRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toDelete := &pluginregistry.DeletePluginPlatformInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		toDelete.Version = &v
	}

	err = s.pluginRegistryService.DeletePluginPlatform(ctx, toDelete)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// trimSortPrefix trims the sort prefix from the sort string.
func (*PluginRegistryServer) trimSortPrefix(sort string) string {
	if i := strings.Index(sort, "_"); i != -1 {
		// Trim	the prefix and return the rest of the string.
		return sort[i+1:]
	}

	// Not a valid sort, just return it as is.
	return sort
}

// toPBPlugin converts a plugin to a protobuf plugin.
func (*PluginRegistryServer) toPBPlugin(plugin *models.Plugin) *pb.Plugin {
	return &pb.Plugin{
		Metadata:       toPBMetadata(&plugin.Metadata, gid.PluginType),
		Name:           plugin.Name,
		CreatedBy:      plugin.CreatedBy,
		OrganizationId: gid.ToGlobalID(gid.OrganizationType, plugin.OrganizationID),
		RepositoryUrl:  plugin.RepositoryURL,
		Private:        plugin.Private,
	}
}

// toPBPluginVersion converts a plugin version to a protobuf plugin version.
func (*PluginRegistryServer) toPBPluginVersion(pluginVersion *models.PluginVersion) *pb.PluginVersion {
	return &pb.PluginVersion{
		Metadata:        toPBMetadata(&pluginVersion.Metadata, gid.PluginVersionType),
		PluginId:        gid.ToGlobalID(gid.PluginType, pluginVersion.PluginID),
		CreatedBy:       pluginVersion.CreatedBy,
		SemanticVersion: pluginVersion.SemanticVersion,
		Protocols:       pluginVersion.Protocols,
		ShaSumsUploaded: pluginVersion.SHASumsUploaded,
		ReadmeUploaded:  pluginVersion.ReadmeUploaded,
		Latest:          pluginVersion.Latest,
	}
}

// toPBPluginPlatform converts a plugin platform to a protobuf message.
func (*PluginRegistryServer) toPBPluginPlatform(pluginPlatform *models.PluginPlatform) *pb.PluginPlatform {
	return &pb.PluginPlatform{
		Metadata:        toPBMetadata(&pluginPlatform.Metadata, gid.PluginPlatformType),
		PluginVersionId: gid.ToGlobalID(gid.PluginVersionType, pluginPlatform.PluginVersionID),
		CreatedBy:       pluginPlatform.CreatedBy,
		OperatingSystem: pluginPlatform.OperatingSystem,
		Architecture:    pluginPlatform.Architecture,
		ShaSum:          pluginPlatform.SHASum,
		Filename:        pluginPlatform.Filename,
		BinaryUploaded:  pluginPlatform.BinaryUploaded,
	}
}
