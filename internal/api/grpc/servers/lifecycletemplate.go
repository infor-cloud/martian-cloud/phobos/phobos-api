package servers

import (
	"context"
	"fmt"
	"io"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/grpc/reader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// LifecycleTemplateServer embeds the UnimplementedLifecycleTemplatesServer.
type LifecycleTemplateServer struct {
	pb.UnimplementedLifecycleTemplatesServer
	lifecycleTemplateService lifecycletemplate.Service
	urnResolver              *urn.Resolver
	logger                   logger.Logger
	maxUploadSize            int64
}

// NewLifecycleTemplateServer returns an instance of LifecycleTemplateServer.
func NewLifecycleTemplateServer(logger logger.Logger,
	lifecycleTemplateService lifecycletemplate.Service,
	urnResolver *urn.Resolver,
	maxUploadSize int64,
) *LifecycleTemplateServer {
	return &LifecycleTemplateServer{
		lifecycleTemplateService: lifecycleTemplateService,
		urnResolver:              urnResolver,
		maxUploadSize:            maxUploadSize,
		logger:                   logger,
	}
}

// GetLifecycleTemplateByID returns a LifecycleTemplate by an ID.
func (s *LifecycleTemplateServer) GetLifecycleTemplateByID(ctx context.Context,
	req *pb.GetLifecycleTemplateByIdRequest,
) (*pb.LifecycleTemplate, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	gotLifecycleTemplate, err := s.lifecycleTemplateService.GetLifecycleTemplateByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBLifecycleTemplate(gotLifecycleTemplate), nil
}

// GetLifecycleTemplates returns a paginated list of LifecycleTemplates.
func (s *LifecycleTemplateServer) GetLifecycleTemplates(ctx context.Context,
	req *pb.GetLifecycleTemplatesRequest) (*pb.GetLifecycleTemplatesResponse, error) {
	// GetSort() will default to a sort option if nothing was specified.
	sort := db.LifecycleTemplateSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &lifecycletemplate.GetLifecycleTemplatesInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
	}

	for _, s := range req.Scopes {
		input.LifecycleTemplateScopes = append(input.LifecycleTemplateScopes, models.ScopeType(s.String()))
	}

	if req.OrgId != nil {
		orgID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetOrgId())
		if rErr != nil {
			return nil, rErr
		}

		input.OrganizationID = &orgID
	}

	if req.ProjectId != nil {
		projectID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if rErr != nil {
			return nil, rErr
		}

		input.ProjectID = &projectID
	}

	result, err := s.lifecycleTemplateService.GetLifecycleTemplates(ctx, input)
	if err != nil {
		return nil, err
	}

	lifecycleTemplates := result.LifecycleTemplates

	pbLifecycleTemplates := make([]*pb.LifecycleTemplate, len(lifecycleTemplates))
	for ix := range lifecycleTemplates {
		pbLifecycleTemplates[ix] = s.toPBLifecycleTemplate(lifecycleTemplates[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(lifecycleTemplates) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(lifecycleTemplates[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(lifecycleTemplates[len(lifecycleTemplates)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetLifecycleTemplatesResponse{
		PageInfo:           pageInfo,
		LifecycleTemplates: pbLifecycleTemplates,
	}, nil
}

// CreateLifecycleTemplate creates a new LifecycleTemplate.
func (s *LifecycleTemplateServer) CreateLifecycleTemplate(ctx context.Context,
	req *pb.CreateLifecycleTemplateRequest) (*pb.LifecycleTemplate, error) {
	toCreate := &lifecycletemplate.CreateLifecycleTemplateInput{
		Scope: models.ScopeType(req.Scope.String()),
	}

	if req.OrgId != nil {
		orgID, err := s.urnResolver.ResolveResourceID(ctx, req.GetOrgId())
		if err != nil {
			return nil, err
		}

		toCreate.OrganizationID = &orgID
	}

	if req.ProjectId != nil {
		projectID, err := s.urnResolver.ResolveResourceID(ctx, req.GetProjectId())
		if err != nil {
			return nil, err
		}

		toCreate.ProjectID = &projectID
	}

	createdLifecycleTemplate, err := s.lifecycleTemplateService.CreateLifecycleTemplate(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return s.toPBLifecycleTemplate(createdLifecycleTemplate), nil
}

// UploadLifecycleTemplate uploads to a LifecycleTemplate.
func (s *LifecycleTemplateServer) UploadLifecycleTemplate(stream pb.LifecycleTemplates_UploadLifecycleTemplateServer) error {
	// First message is request metadata.
	req, err := stream.Recv()
	if err != nil {
		return err
	}

	id, err := s.urnResolver.ResolveResourceID(stream.Context(), req.GetInfo().GetId())
	if err != nil {
		return err
	}

	// Use a reader that limits the size of request body
	return s.lifecycleTemplateService.UploadLifecycleTemplate(
		stream.Context(),
		&lifecycletemplate.UploadLifecycleTemplateInput{
			ID: id,
			Reader: reader.NewLimitReader(func() ([]byte, error) {
				chunkReq, err := stream.Recv()
				if err != nil {
					// Return the error whether it's EOF or something else.
					return nil, err
				}

				return chunkReq.GetChunkData(), nil
			}, s.maxUploadSize),
		})
}

// GetLifecycleTemplateData returns a stream of LifecycleTemplate data.
func (s *LifecycleTemplateServer) GetLifecycleTemplateData(req *pb.GetLifecycleTemplateDataRequest, server pb.LifecycleTemplates_GetLifecycleTemplateDataServer) error {
	id, err := s.urnResolver.ResolveResourceID(server.Context(), req.GetId())
	if err != nil {
		return err
	}

	rdr, err := s.lifecycleTemplateService.GetLifecycleTemplateData(server.Context(), id)
	if err != nil {
		return err
	}
	defer func() {
		if err = rdr.Close(); err != nil {
			s.logger.Errorf("error closing lifecycle template data reader: %v", err)
		}
	}()

	chunk := &pb.GetLifecycleTemplateDataResponse{ChunkData: make([]byte, chunkSize)}

	var n int
	for {
		n, err = rdr.Read(chunk.ChunkData)
		if err != nil && err != io.EOF {
			return fmt.Errorf("error reading lifecycle template data: %w", err)
		}

		if n > 0 {
			chunk.ChunkData = chunk.ChunkData[:n]
			serverErr := server.Send(chunk)
			if serverErr != nil {
				return fmt.Errorf("error sending lifecycle template data: %w", serverErr)
			}
		}

		if err == io.EOF {
			return nil
		}
	}
}

// toPBLifecycleTemplate converts from LifecycleTemplate model to ProtoBuf model.
func (*LifecycleTemplateServer) toPBLifecycleTemplate(pt *models.LifecycleTemplate) *pb.LifecycleTemplate {
	var projectID *string
	if pt.ProjectID != nil {
		projectID = ptr.String(gid.ToGlobalID(gid.ProjectType, *pt.ProjectID))
	}

	return &pb.LifecycleTemplate{
		Metadata:  toPBMetadata(&pt.Metadata, gid.LifecycleTemplateType),
		CreatedBy: pt.CreatedBy,
		Scope:     string(pt.Scope),
		OrgId:     gid.ToGlobalID(gid.OrganizationType, pt.OrganizationID),
		ProjectId: projectID,
		Status:    string(pt.Status),
	}
}
