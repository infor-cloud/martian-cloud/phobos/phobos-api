package servers

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"google.golang.org/protobuf/types/known/emptypb"
)

// PipelineServer embeds the UnimplementedPipelinesServer.
type PipelineServer struct {
	pb.UnimplementedPipelinesServer
	pipelineService pipeline.Service
	urnResolver     *urn.Resolver
}

// NewPipelineServer returns an instance of PipelineServer.
func NewPipelineServer(
	pipelineService pipeline.Service,
	urnResolver *urn.Resolver,
) *PipelineServer {
	return &PipelineServer{
		pipelineService: pipelineService,
		urnResolver:     urnResolver,
	}
}

// CreatePipelineJWT creates a new pipeline JWT
func (s *PipelineServer) CreatePipelineJWT(ctx context.Context, req *pb.CreatePipelineJWTRequest) (*pb.CreatePipelineJWTResponse, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	token, err := s.pipelineService.CreatePipelineJWT(ctx, &pipeline.CreatePipelineJWTInput{
		PipelineID: pipelineID,
		Audience:   req.Audience,
		Expiration: req.Expiration.AsTime(),
	})
	if err != nil {
		return nil, err
	}

	return &pb.CreatePipelineJWTResponse{
		Token: string(token),
	}, nil
}

// SetPipelineActionOutputs sets the outputs for a pipeline action.
func (s *PipelineServer) SetPipelineActionOutputs(ctx context.Context, req *pb.SetPipelineActionOutputsRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	outputs := []*pipeline.ActionOutput{}
	for _, output := range req.Outputs {
		outputs = append(outputs, &pipeline.ActionOutput{
			Name:  output.Name,
			Type:  output.Type,
			Value: output.Value,
		})
	}

	if err := s.pipelineService.SetPipelineActionOutputs(ctx, pipelineID, req.ActionPath, outputs); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// GetPipelineActionOutputs returns the outputs for a list of actions
func (s *PipelineServer) GetPipelineActionOutputs(ctx context.Context, req *pb.GetPipelineActionOutputsRequest) (*pb.GetPipelineActionOutputsResponse, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	outputs, err := s.pipelineService.GetPipelineActionOutputs(ctx, pipelineID, req.ActionPaths)
	if err != nil {
		return nil, err
	}

	outputMap := map[string][]*pb.PipelineActionOutput{}
	for _, output := range outputs {
		if _, ok := outputMap[output.ActionPath]; !ok {
			outputMap[output.ActionPath] = []*pb.PipelineActionOutput{}
		}

		outputMap[output.ActionPath] = append(outputMap[output.ActionPath], &pb.PipelineActionOutput{
			Name:  output.Name,
			Type:  output.Type,
			Value: output.Value,
		})
	}

	response := &pb.GetPipelineActionOutputsResponse{
		Actions: []*pb.PipelineActionOutputs{},
	}

	for k, v := range outputMap {
		response.Actions = append(response.Actions, &pb.PipelineActionOutputs{
			ActionPath: k,
			Outputs:    v,
		})
	}

	return response, nil
}

// SetPipelineActionStatus sets the status for a pipeline action.
func (s *PipelineServer) SetPipelineActionStatus(ctx context.Context, req *pb.SetPipelineActionStatusRequest) (*pb.Pipeline, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if err = s.pipelineService.SetPipelineActionStatus(ctx, pipelineID, req.ActionPath, statemachine.NodeStatus(req.Status.String())); err != nil {
		return nil, err
	}
	pipeline, err := s.pipelineService.GetPipelineByID(ctx, pipelineID)
	if err != nil {
		return nil, err
	}
	return toPBPipeline(pipeline), nil
}

// GetPipelineEventsStreaming returns a stream of pipeline events.
func (s *PipelineServer) GetPipelineEventsStreaming(req *pb.GetPipelineEventsStreamingRequest, server pb.Pipelines_GetPipelineEventsStreamingServer) error {
	pipelineID, err := s.urnResolver.ResolveResourceID(server.Context(), req.PipelineId)
	if err != nil {
		return err
	}

	pipelineEvents, err := s.pipelineService.SubscribeToPipeline(server.Context(), &pipeline.SubscribeToPipelineInput{
		PipelineID:      pipelineID,
		LastSeenVersion: req.LastSeenVersion,
	})
	if err != nil {
		return err
	}

	// Return events until the channel is closed
	for e := range pipelineEvents {
		// Return next chunk
		if err := server.Send(&pb.GetPipelineEventsStreamingResponse{
			Pipeline: toPBPipeline(e.Pipeline),
		}); err != nil {
			return err
		}
	}

	return nil
}

// GetPipelineByID returns an Pipeline by an ID.
func (s *PipelineServer) GetPipelineByID(ctx context.Context, req *pb.GetPipelineByIdRequest) (*pb.Pipeline, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotPipeline, err := s.pipelineService.GetPipelineByID(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	return toPBPipeline(gotPipeline), nil
}

// GetPipelines returns a paginated list of Pipelines.
func (s *PipelineServer) GetPipelines(ctx context.Context, req *pb.GetPipelinesRequest) (*pb.GetPipelinesResponse, error) {
	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.ProjectId)
	if err != nil {
		return nil, err
	}

	// GetSort() will default to a sort option if nothing was specified.
	sort := db.PipelineSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &pipeline.GetPipelinesInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		ProjectID:         projectID,
	}

	if req.PipelineTemplateId != nil {
		templateID, rErr := s.urnResolver.ResolveResourceID(ctx, req.GetPipelineTemplateId())
		if rErr != nil {
			return nil, rErr
		}

		input.PipelineTemplateID = &templateID
	}

	result, err := s.pipelineService.GetPipelines(ctx, input)
	if err != nil {
		return nil, err
	}

	pipelines := result.Pipelines

	pbPipelines := make([]*pb.Pipeline, len(pipelines))
	for ix := range pipelines {
		pbPipelines[ix] = toPBPipeline(&pipelines[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(pipelines) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&pipelines[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&pipelines[len(pipelines)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetPipelinesResponse{
		PageInfo:  pageInfo,
		Pipelines: pbPipelines,
	}, nil
}

// GetPipelineVariables returns a list of variables for a pipeline.
func (s *PipelineServer) GetPipelineVariables(ctx context.Context, req *pb.GetPipelineVariablesRequest) (*pb.GetPipelineVariablesResponse, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	variables, err := s.pipelineService.GetPipelineVariables(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	pbHCLVariables := []*pb.PipelineVariable{}
	pbEnvironmentVariables := []*pb.PipelineVariable{}
	for _, variable := range variables {
		switch variable.Category {
		case models.HCLVariable:
			pbHCLVariables = append(pbHCLVariables, &pb.PipelineVariable{
				Key:   variable.Key,
				Value: variable.Value,
			})
		case models.EnvironmentVariable:
			pbEnvironmentVariables = append(pbEnvironmentVariables, &pb.PipelineVariable{
				Key:   variable.Key,
				Value: variable.Value,
			})
		}
	}

	return &pb.GetPipelineVariablesResponse{
		HclVariables:         pbHCLVariables,
		EnvironmentVariables: pbEnvironmentVariables,
	}, nil
}

// CreatePipeline creates a new Pipeline.
func (s *PipelineServer) CreatePipeline(ctx context.Context, req *pb.CreatePipelineRequest) (*pb.Pipeline, error) {
	if req.Type != pb.PipelineType_DEPLOYMENT && req.Type != pb.PipelineType_RUNBOOK {
		return nil, errors.New("%s pipeline type must be either DEPLOYMENT or RUNBOOK", req.Type, errors.WithErrorCode(errors.EInvalid))
	}

	templateID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineTemplateId)
	if err != nil {
		return nil, err
	}

	variables := []pipeline.Variable{}
	for _, variable := range req.HclVariables {
		variables = append(variables, pipeline.Variable{
			Key:      variable.Key,
			Value:    variable.Value,
			Category: models.HCLVariable,
		})
	}

	for _, variable := range req.EnvironmentVariables {
		variables = append(variables, pipeline.Variable{
			Key:      variable.Key,
			Value:    variable.Value,
			Category: models.EnvironmentVariable,
		})
	}

	annotations := make([]*models.PipelineAnnotation, len(req.Annotations))
	for ix, annotation := range req.Annotations {
		annotations[ix] = &models.PipelineAnnotation{
			Key:   annotation.Key,
			Value: annotation.Value,
			Link:  annotation.Link,
		}
	}

	toCreate := &pipeline.CreatePipelineInput{
		EnvironmentName:     req.EnvironmentName,
		PipelineTemplateID:  templateID,
		Type:                models.PipelineType(req.Type.String()),
		Variables:           variables,
		When:                models.AutoPipeline,
		Annotations:         annotations,
		VariableSetRevision: req.VariableSetRevision,
	}

	createdPipeline, err := s.pipelineService.CreatePipeline(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	return toPBPipeline(createdPipeline), nil
}

// CancelPipeline deletes an Pipeline.
func (s *PipelineServer) CancelPipeline(ctx context.Context, req *pb.CancelPipelineRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toCancel := &pipeline.CancelPipelineInput{
		ID:    pipelineID,
		Force: req.Force,
	}

	if req.Version != nil {
		v, err := strconv.Atoi(req.GetVersion())
		if err != nil {
			return nil, err
		}

		toCancel.Version = &v
	}

	if _, err := s.pipelineService.CancelPipeline(ctx, toCancel); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// RetryPipelineTask retries a pipeline task.
func (s *PipelineServer) RetryPipelineTask(ctx context.Context, req *pb.RetryPipelineTaskRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if _, err := s.pipelineService.RetryPipelineTask(ctx, pipelineID, req.TaskPath); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// RunPipelineTask starts a pipeline task.
func (s *PipelineServer) RunPipelineTask(ctx context.Context, req *pb.RunPipelineTaskRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if _, err := s.pipelineService.RunPipelineTask(ctx, pipelineID, req.TaskPath); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// RunPipeline starts a pipeline.
func (s *PipelineServer) RunPipeline(ctx context.Context, req *pb.RunPipelineRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	if _, err := s.pipelineService.RunPipeline(ctx, pipelineID); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// RetryNestedPipeline retries a pipeline.
func (s *PipelineServer) RetryNestedPipeline(ctx context.Context, req *pb.RetryNestedPipelineRequest) (*emptypb.Empty, error) {
	parentPipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.ParentPipelineId)
	if err != nil {
		return nil, err
	}

	if _, err := s.pipelineService.RetryNestedPipeline(ctx, parentPipelineID, req.ParentNestedPipelineNodePath); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// ApprovePipeline approves a pipeline.
func (s *PipelineServer) ApprovePipeline(ctx context.Context, req *pb.ApprovePipelineRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if err := s.pipelineService.ApprovePipelineNode(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodeType:   statemachine.PipelineNodeType,
	}); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// RevokePipelineApproval revokes a pipeline approval.
func (s *PipelineServer) RevokePipelineApproval(ctx context.Context, req *pb.RevokePipelineApprovalRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if err := s.pipelineService.RevokePipelineNodeApproval(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodeType:   statemachine.PipelineNodeType,
	}); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// ApprovePipelineTask approves a pipeline task.
func (s *PipelineServer) ApprovePipelineTask(ctx context.Context, req *pb.ApprovePipelineTaskRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if err := s.pipelineService.ApprovePipelineNode(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodePath:   &req.TaskPath,
		NodeType:   statemachine.TaskNodeType,
	}); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// RevokePipelineTaskApproval revokes a pipeline task approval.
func (s *PipelineServer) RevokePipelineTaskApproval(ctx context.Context, req *pb.RevokePipelineTaskApprovalRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if err := s.pipelineService.RevokePipelineNodeApproval(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodePath:   &req.TaskPath,
		NodeType:   statemachine.TaskNodeType,
	}); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// SchedulePipelineNode schedules a pipeline node.
func (s *PipelineServer) SchedulePipelineNode(ctx context.Context, req *pb.SchedulePipelineNodeRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	options := &pipeline.SchedulePipelineNodeInput{
		PipelineID: pipelineID,
		NodePath:   req.NodePath,
		NodeType:   statemachine.NodeType(req.NodeType.String()),
	}

	if req.ScheduledStartTime != nil {
		options.ScheduledStartTime = ptr.Time(req.ScheduledStartTime.AsTime())
	}

	if req.CronSchedule != nil {
		options.CronSchedule = &models.CronSchedule{
			Expression: req.CronSchedule.Expression,
			Timezone:   req.CronSchedule.Timezone,
		}
	}

	if _, err := s.pipelineService.SchedulePipelineNode(ctx, options); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// CancelPipelineNodeSchedule cancels a pipeline node schedule.
func (s *PipelineServer) CancelPipelineNodeSchedule(ctx context.Context, req *pb.CancelPipelineNodeScheduleRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if _, err := s.pipelineService.UnschedulePipelineNode(ctx, &pipeline.UnschedulePipelineNodeInput{
		PipelineID: pipelineID,
		NodePath:   req.NodePath,
		NodeType:   statemachine.NodeType(req.NodeType.String()),
	}); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// DeferPipelineNode defers a pipeline node.
func (s *PipelineServer) DeferPipelineNode(ctx context.Context, req *pb.DeferPipelineNodeRequest) (*emptypb.Empty, error) {
	pipelineID, err := s.urnResolver.ResolveResourceID(ctx, req.PipelineId)
	if err != nil {
		return nil, err
	}

	if _, err := s.pipelineService.DeferPipelineNode(ctx, &pipeline.DeferPipelineNodeInput{
		PipelineID: pipelineID,
		Reason:     req.Reason,
		NodePath:   req.NodePath,
		NodeType:   statemachine.NodeType(req.NodeType.String()),
	}); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func toPBNodeStatus(status statemachine.NodeStatus) pb.PipelineNodeStatus {
	val := pb.PipelineNodeStatus_value[string(status)]
	return pb.PipelineNodeStatus(val)
}

func toPBPipelineOnErrorStrategy(onError statemachine.OnErrorStrategy) pb.PipelineOnErrorStrategy {
	val := pb.PipelineOnErrorStrategy_value[string(onError)]
	return pb.PipelineOnErrorStrategy(val)
}

// toPBPipeline converts from Pipeline model to ProtoBuf model.
func toPBPipeline(p *models.Pipeline) *pb.Pipeline {
	response := &pb.Pipeline{
		Metadata:               toPBMetadata(&p.Metadata, gid.PipelineType),
		CreatedBy:              p.CreatedBy,
		ProjectId:              gid.ToGlobalID(gid.ProjectType, p.ProjectID),
		PipelineTemplateId:     gid.ToGlobalID(gid.PipelineTemplateType, p.PipelineTemplateID),
		Status:                 toPBNodeStatus(p.Status),
		Type:                   string(p.Type),
		When:                   string(p.When),
		ParentPipelineNodePath: p.ParentPipelineNodePath,
		Superseded:             p.Superseded,
		ApprovalStatus:         ptr.String(string(p.ApprovalStatus)),
	}

	if p.ReleaseID != nil {
		response.ReleaseId = ptr.String(gid.ToGlobalID(gid.ReleaseType, *p.ReleaseID))
	}

	if p.ParentPipelineID != nil {
		response.ParentPipelineId = ptr.String(gid.ToGlobalID(gid.PipelineType, *p.ParentPipelineID))
	}

	for _, stage := range p.Stages {
		pbStage := &pb.PipelineStage{
			Path:   stage.Path,
			Name:   stage.Name,
			Status: toPBNodeStatus(stage.Status),
		}

		for _, nestedPipeline := range stage.NestedPipelines {
			pbPipeline := &pb.NestedPipeline{
				Path:             nestedPipeline.Path,
				Name:             nestedPipeline.Name,
				Status:           toPBNodeStatus(nestedPipeline.Status),
				EnvironmentName:  nestedPipeline.EnvironmentName,
				LatestPipelineId: gid.ToGlobalID(gid.PipelineType, nestedPipeline.LatestPipelineID),
				ApprovalStatus:   ptr.String(string(nestedPipeline.ApprovalStatus)),
				Dependencies:     nestedPipeline.Dependencies,
				Errors:           nestedPipeline.Errors,
				OnError:          toPBPipelineOnErrorStrategy(nestedPipeline.OnError),
			}

			if nestedPipeline.CronSchedule != nil {
				cronSchedule := nestedPipeline.CronSchedule

				pbPipeline.CronSchedule = &pb.PipelineCronSchedule{
					Expression: cronSchedule.Expression,
					Timezone:   models.DefaultCronScheduleTimezone.String(),
				}

				if cronSchedule.Timezone != nil {
					pbPipeline.CronSchedule.Timezone = *cronSchedule.Timezone
				}
			}

			pbStage.NestedPipelines = append(pbStage.NestedPipelines, pbPipeline)
		}

		pbStage.Tasks = toPBTasks(stage.Tasks)
		response.Stages = append(response.Stages, pbStage)
	}

	for _, approvalRuleID := range p.ApprovalRuleIDs {
		response.ApprovalRuleIds = append(response.ApprovalRuleIds, gid.ToGlobalID(gid.ApprovalRuleType, approvalRuleID))
	}

	return response
}

// toPBTasks converts from PipelineTask model to ProtoBuf model.
func toPBTasks(tasks []*models.PipelineTask) []*pb.PipelineTask {
	var pbTasks []*pb.PipelineTask
	for _, task := range tasks {
		var interval *int32
		if task.Interval != nil {
			intervalVal := int32(task.Interval.Seconds())
			interval = &intervalVal
		}

		pbTask := &pb.PipelineTask{
			Path:                task.Path,
			Name:                task.Name,
			When:                string(task.When),
			Interval:            interval,
			HasSuccessCondition: task.HasSuccessCondition,
			Status:              toPBNodeStatus(task.Status),
			ApprovalStatus:      ptr.String(string(task.ApprovalStatus)),
			Dependencies:        task.Dependencies,
			Errors:              task.Errors,
			Image:               task.Image,
			OnError:             toPBPipelineOnErrorStrategy(task.OnError),
			AttemptCount:        int32(task.AttemptCount),
		}

		if task.LatestJobID != nil {
			pbTask.LatestJobId = ptr.String(gid.ToGlobalID(gid.JobType, *task.LatestJobID))
		}

		if task.CronSchedule != nil {
			cronSchedule := task.CronSchedule

			pbTask.CronSchedule = &pb.PipelineCronSchedule{
				Expression: cronSchedule.Expression,
				Timezone:   models.DefaultCronScheduleTimezone.String(),
			}

			if cronSchedule.Timezone != nil {
				pbTask.CronSchedule.Timezone = *cronSchedule.Timezone
			}
		}

		for _, approvalRuleID := range task.ApprovalRuleIDs {
			pbTask.ApprovalRuleIds = append(pbTask.ApprovalRuleIds, gid.ToGlobalID(gid.ApprovalRuleType, approvalRuleID))
		}

		for _, action := range task.Actions {
			pbAction := &pb.PipelineAction{
				Path:   action.Path,
				Name:   action.Name,
				Status: toPBNodeStatus(action.Status),
			}
			pbTask.Actions = append(pbTask.Actions, pbAction)
		}

		pbTasks = append(pbTasks, pbTask)
	}

	return pbTasks
}
