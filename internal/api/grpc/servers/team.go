package servers

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/team"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
)

// TeamServer embeds the UnimplementedTeamsServer.
type TeamServer struct {
	pb.UnimplementedTeamsServer
	teamService team.Service
	urnResolver *urn.Resolver
}

// NewTeamServer returns an instance of TeamServer.
func NewTeamServer(
	teamService team.Service,
	urnResolver *urn.Resolver,
) *TeamServer {
	return &TeamServer{
		teamService: teamService,
		urnResolver: urnResolver,
	}
}

// GetTeamByID returns a team by ID.
func (s *TeamServer) GetTeamByID(ctx context.Context, req *pb.GetTeamByIdRequest) (*pb.Team, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotTeam, err := s.teamService.GetTeamByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBTeam(gotTeam), nil
}

// GetTeams returns a paginated list of teams.
func (s *TeamServer) GetTeams(ctx context.Context, req *pb.GetTeamsRequest) (*pb.GetTeamsResponse, error) {
	sort := db.TeamSortableField(req.GetSort().String())

	paginationOpts, err := fromPBPaginationOptions(req.PaginationOptions)
	if err != nil {
		return nil, err
	}

	input := &team.GetTeamsInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		Search:            req.Search,
	}

	result, err := s.teamService.GetTeams(ctx, input)
	if err != nil {
		return nil, err
	}

	teams := result.Teams

	pbTeams := make([]*pb.Team, len(teams))
	for ix := range teams {
		pbTeams[ix] = s.toPBTeam(&teams[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(teams) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&teams[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&teams[len(teams)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetTeamsResponse{
		PageInfo: pageInfo,
		Teams:    pbTeams,
	}, nil
}

// toPBTeam converts from a Team model to a ProtoBuf model.
func (*TeamServer) toPBTeam(team *models.Team) *pb.Team {
	return &pb.Team{
		Metadata:       toPBMetadata(&team.Metadata, gid.TeamType),
		Name:           team.Name,
		Description:    team.Description,
		ScimExternalId: team.SCIMExternalID,
	}
}
