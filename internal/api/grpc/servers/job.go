// Package servers implements the gRPC servers.
package servers

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	// Max size of log chunk per request
	logChunkLimit = 1024 * 1024
)

// JobServer embeds the UnimplementedJobsServer.
type JobServer struct {
	pb.UnimplementedJobsServer
	jobService  job.Service
	urnResolver *urn.Resolver
}

// NewJobServer returns an instance of JobServer.
func NewJobServer(
	jobService job.Service,
	urnResolver *urn.Resolver,
) *JobServer {
	return &JobServer{
		jobService:  jobService,
		urnResolver: urnResolver,
	}
}

// WriteLogs writes logs to the log stream for a particular job
func (s *JobServer) WriteLogs(ctx context.Context, req *pb.WriteLogsRequest) (*emptypb.Empty, error) {
	jobID, err := s.urnResolver.ResolveResourceID(ctx, req.JobId)
	if err != nil {
		return nil, err
	}

	_, err = s.jobService.WriteLogs(ctx, jobID, int(req.StartOffset), req.Logs)
	if err != nil {
		return nil, err
	}
	return &emptypb.Empty{}, nil
}

// GetLogsStreaming returns a stream of logs for a particular job
func (s *JobServer) GetLogsStreaming(req *pb.GetLogsStreamingRequest, server pb.Jobs_GetLogsStreamingServer) error {
	jobID, err := s.urnResolver.ResolveResourceID(server.Context(), req.JobId)
	if err != nil {
		return err
	}

	lastSeenLogSize := int(req.StartOffset)
	logEvents, err := s.jobService.SubscribeToLogStreamEvents(server.Context(), &job.LogStreamEventSubscriptionOptions{
		JobID:           jobID,
		LastSeenLogSize: &lastSeenLogSize,
	})
	if err != nil {
		return err
	}

	offset := int(req.StartOffset)

	// Return logs until the channel is closed
	for e := range logEvents {
		// Continue until we have returned all available logs based on the current log stream size
		for e.Size > offset {
			// Get next chunk of logs
			logs, err := s.jobService.ReadLogs(server.Context(), req.JobId, offset, logChunkLimit)
			if err != nil {
				return err
			}

			// Increase offset
			offset += len(logs)

			// Return next chunk
			if err := server.Send(&pb.GetLogsStreamingResponse{
				Logs: logs,
			}); err != nil {
				return err
			}
		}
	}

	return nil
}

// ClaimJob claims a new Job.
func (s *JobServer) ClaimJob(ctx context.Context, req *pb.ClaimJobRequest) (*pb.ClaimJobResponse, error) {

	agentID, err := s.urnResolver.ResolveResourceID(ctx, req.AgentId)
	if err != nil {
		return nil, err
	}

	claimedJob, err := s.jobService.ClaimJob(ctx, agentID)
	if err != nil {
		return nil, err
	}

	return &pb.ClaimJobResponse{
		Job:   s.toPBJob(claimedJob.Job),
		Token: claimedJob.Token,
	}, nil
}

// GetJob gets one Job.
func (s *JobServer) GetJob(ctx context.Context, input *pb.GetJobInput) (*pb.Job, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, input.Id)
	if err != nil {
		return nil, err
	}

	job, err := s.jobService.GetJobByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBJob(job), nil
}

// SetJobStatus sets the status of a Job.
func (s *JobServer) SetJobStatus(ctx context.Context, input *pb.SetJobStatusInput) (*pb.Job, error) {
	jobID, err := s.urnResolver.ResolveResourceID(ctx, input.JobId)
	if err != nil {
		return nil, err
	}

	job, err := s.jobService.SetJobStatus(ctx, jobID, models.JobStatus(input.Status))
	if err != nil {
		return nil, err
	}

	return s.toPBJob(job), nil
}

// SubscribeToJobCancellationEvent subscribes to job cancellation events.
func (s *JobServer) SubscribeToJobCancellationEvent(req *pb.JobCancellationEventRequest, stream pb.Jobs_SubscribeToJobCancellationEventServer) error {
	jobID, err := s.urnResolver.ResolveResourceID(stream.Context(), req.JobId)
	if err != nil {
		return err
	}

	events, err := s.jobService.SubscribeToCancellationEvent(stream.Context(), &job.CancellationSubscriptionsOptions{
		JobID: jobID,
	})
	if err != nil {
		return err
	}

	for e := range events {
		if err = stream.Send(&pb.JobCancellationEventResponse{
			Job: s.toPBJob(e.Job),
		}); err != nil {
			return err
		}
	}

	return nil
}

// CancelJob cancels a Job.
func (s *JobServer) CancelJob(ctx context.Context, req *pb.CancelJobRequest) (*emptypb.Empty, error) {
	jobID, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	toCancel := &job.CancelJobInput{
		ID:    jobID,
		Force: req.Force,
	}

	if req.Version != nil {
		v, err := strconv.Atoi(req.GetVersion())
		if err != nil {
			return nil, err
		}

		toCancel.Version = &v
	}

	if _, err := s.jobService.CancelJob(ctx, toCancel); err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// toPBJob converts from Job model to ProtoBuf model.
func (s *JobServer) toPBJob(p *models.Job) *pb.Job {
	job := &pb.Job{
		Metadata:        toPBMetadata(&p.Metadata, gid.JobType),
		Timestamps:      s.toPBTimestamps(&p.Timestamps),
		Status:          string(p.GetStatus()),
		Type:            string(p.Type),
		ProjectId:       gid.ToGlobalID(gid.ProjectType, p.ProjectID),
		AgentName:       p.AgentName,
		AgentType:       p.AgentType,
		MaxJobDuration:  p.MaxJobDuration,
		ForceCanceled:   p.ForceCanceled,
		ForceCanceledBy: p.ForceCanceledBy,
		Tags:            p.Tags,
		Image:           p.Image,
	}

	if p.CancelRequestedTimestamp != nil {
		job.CancelRequestedTimestamp = timestamppb.New(*p.CancelRequestedTimestamp)
		job.ForceCancelAvailableAt = timestamppb.New(p.GetForceCancelAvailableAt())
	}

	if p.AgentID != nil {
		job.AgentId = ptr.String(gid.ToGlobalID(gid.AgentType, *p.AgentID))
	}

	switch data := p.Data.(type) {
	case *models.JobTaskData:
		job.Data = &pb.Job_TaskData{
			TaskData: &pb.JobTaskData{
				PipelineId: gid.ToGlobalID(gid.PipelineType, data.PipelineID),
				TaskPath:   data.TaskPath,
			},
		}
	}

	return job
}

// toPBTimestamps converts from a Timestamps model to a ProtoBuf model.
func (*JobServer) toPBTimestamps(p *models.JobTimestamps) *pb.JobTimestamps {
	timestamps := &pb.JobTimestamps{}

	if p.QueuedTimestamp != nil {
		timestamps.QueuedTimestamp = timestamppb.New(*p.QueuedTimestamp)
	}

	if p.PendingTimestamp != nil {
		timestamps.PendingTimestamp = timestamppb.New(*p.PendingTimestamp)
	}

	if p.RunningTimestamp != nil {
		timestamps.RunningTimestamp = timestamppb.New(*p.RunningTimestamp)
	}

	if p.FinishedTimestamp != nil {
		timestamps.FinishedTimestamp = timestamppb.New(*p.FinishedTimestamp)
	}

	return timestamps
}
