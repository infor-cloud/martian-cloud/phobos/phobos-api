package servers

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/resourcelimit"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
)

// ResourceLimitServer embeds the UnimplementedResourceLimitsServer.
type ResourceLimitServer struct {
	pb.UnimplementedResourceLimitsServer
	resourceLimitService resourcelimit.Service
}

// NewResourceLimitServer returns an instance of ResourceLimitServer.
func NewResourceLimitServer(resourceLimitService resourcelimit.Service) *ResourceLimitServer {
	return &ResourceLimitServer{resourceLimitService: resourceLimitService}
}

// GetResourceLimits returns a list of ResourceLimits.
func (s *ResourceLimitServer) GetResourceLimits(ctx context.Context, _ *emptypb.Empty) (*pb.GetResourceLimitsResponse, error) {
	result, err := s.resourceLimitService.GetResourceLimits(ctx)
	if err != nil {
		return nil, err
	}

	pbLimits := make([]*pb.ResourceLimit, len(result))
	for ix := range result {
		pbLimits[ix] = s.toPBResourceLimit(&result[ix])
	}

	return &pb.GetResourceLimitsResponse{
		ResourceLimits: pbLimits,
	}, nil
}

// UpdateResourceLimit returns the updated ResourceLimit.
func (s *ResourceLimitServer) UpdateResourceLimit(ctx context.Context, req *pb.UpdateResourceLimitRequest) (*pb.ResourceLimit, error) {
	toUpdate := &resourcelimit.UpdateResourceLimitInput{
		Name:  req.Name,
		Value: int(req.Value),
	}

	if req.Version != nil {
		v, err := strconv.Atoi(req.GetVersion())
		if err != nil {
			return nil, err
		}

		toUpdate.MetadataVersion = &v
	}

	updatedLimit, err := s.resourceLimitService.UpdateResourceLimit(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	return s.toPBResourceLimit(updatedLimit), nil
}

// toPBResourceLimit converts from ResourceLimit model to ProtoBuf model.
func (*ResourceLimitServer) toPBResourceLimit(limit *models.ResourceLimit) *pb.ResourceLimit {
	return &pb.ResourceLimit{
		Metadata: toPBMetadata(&limit.Metadata, gid.ResourceLimitType),
		Name:     limit.Name,
		Value:    int32(limit.Value),
	}
}
