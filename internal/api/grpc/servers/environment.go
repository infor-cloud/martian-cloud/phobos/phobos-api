package servers

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/protobuf/types/known/emptypb"
)

// EnvironmentServer embeds the UnimplementedEnvironmentsServer.
type EnvironmentServer struct {
	pb.UnimplementedEnvironmentsServer
	environmentService environment.Service
	urnResolver        *urn.Resolver
}

// NewEnvironmentServer returns an instance of EnvironmentServer.
func NewEnvironmentServer(
	environmentService environment.Service,
	urnResolver *urn.Resolver,
) *EnvironmentServer {
	return &EnvironmentServer{
		environmentService: environmentService,
		urnResolver:        urnResolver,
	}
}

// GetEnvironmentByID returns an Environment by an ID.
func (s *EnvironmentServer) GetEnvironmentByID(ctx context.Context,
	req *pb.GetEnvironmentByIDRequest) (*pb.Environment, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	gotEnvironment, err := s.environmentService.GetEnvironmentByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return s.toPBEnvironment(gotEnvironment), nil
}

// GetEnvironments returns a paginated list of Environments.
func (s *EnvironmentServer) GetEnvironments(ctx context.Context, req *pb.GetEnvironmentsRequest) (*pb.GetEnvironmentsResponse, error) {
	sort := db.EnvironmentSortableField(req.GetSort().String())

	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.ProjectId)
	if err != nil {
		return nil, err
	}

	paginationOpts, err := fromPBPaginationOptions(req.GetPaginationOptions())
	if err != nil {
		return nil, err
	}

	input := &environment.GetEnvironmentsInput{
		Sort:              &sort,
		PaginationOptions: paginationOpts,
		ProjectID:         &projectID,
	}

	result, err := s.environmentService.GetEnvironments(ctx, input)
	if err != nil {
		return nil, err
	}

	environments := result.Environments

	pbEnvironments := make([]*pb.Environment, len(environments))
	for ix := range environments {
		pbEnvironments[ix] = s.toPBEnvironment(&environments[ix])
	}

	pageInfo := &pb.PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
		TotalCount:      result.PageInfo.TotalCount,
	}

	if len(environments) > 0 {
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&environments[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&environments[len(environments)-1])
		if err != nil {
			return nil, err
		}
	}

	return &pb.GetEnvironmentsResponse{
		PageInfo:     pageInfo,
		Environments: pbEnvironments,
	}, nil
}

// CreateEnvironment creates an Environment.
func (s *EnvironmentServer) CreateEnvironment(ctx context.Context, req *pb.CreateEnvironmentRequest) (*pb.Environment, error) {
	projectID, err := s.urnResolver.ResolveResourceID(ctx, req.ProjectId)
	if err != nil {
		return nil, err
	}

	input := &environment.CreateEnvironmentInput{
		Name:        req.Name,
		Description: req.Description,
		ProjectID:   projectID,
	}

	createdEnvironment, err := s.environmentService.CreateEnvironment(ctx, input)
	if err != nil {
		return nil, err
	}

	return s.toPBEnvironment(createdEnvironment), nil
}

// UpdateEnvironment updates an Environment.
func (s *EnvironmentServer) UpdateEnvironment(ctx context.Context, req *pb.UpdateEnvironmentRequest) (*pb.Environment, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	input := &environment.UpdateEnvironmentInput{
		ID:          id,
		Description: req.Description,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		input.MetadataVersion = &v
	}

	updatedEnvironment, err := s.environmentService.UpdateEnvironment(ctx, input)
	if err != nil {
		return nil, err
	}

	return s.toPBEnvironment(updatedEnvironment), nil
}

// DeleteEnvironment deletes an Environment.
func (s *EnvironmentServer) DeleteEnvironment(ctx context.Context, req *pb.DeleteEnvironmentRequest) (*emptypb.Empty, error) {
	id, err := s.urnResolver.ResolveResourceID(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	input := &environment.DeleteEnvironmentInput{
		ID: id,
	}

	if req.Version != nil {
		v, aErr := strconv.Atoi(req.GetVersion())
		if aErr != nil {
			return nil, aErr
		}

		input.MetadataVersion = &v
	}

	err = s.environmentService.DeleteEnvironment(ctx, input)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

// toPBEnvironment converts an Environment to a protobuf Environment.
func (*EnvironmentServer) toPBEnvironment(a *models.Environment) *pb.Environment {
	return &pb.Environment{
		Metadata:    toPBMetadata(&a.Metadata, gid.EnvironmentType),
		Name:        a.Name,
		Description: a.Description,
		ProjectId:   gid.ToGlobalID(gid.ProjectType, a.ProjectID),
		CreatedBy:   a.CreatedBy,
	}
}
