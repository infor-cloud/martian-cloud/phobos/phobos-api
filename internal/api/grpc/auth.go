package grpc

import (
	"context"
	goerrors "errors"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// authUnaryInterceptor returns a UnaryServerInterceptor for token-based authentication.
// It looks for the 'authorization' field in the metadata of the request to locate the token.
func authUnaryInterceptor(
	logger logger.Logger,
	authenticator *auth.Authenticator,
) grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		_ *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		ctxWithCaller, err := withCaller(ctx, authenticator)
		if err != nil {
			return nil, err
		}

		// Handle the rpc.
		resp, err := handler(ctxWithCaller, req)
		if err != nil {
			return nil, handleError(err, logger)
		}

		return resp, nil
	}
}

// wrappedStream wraps around the embedded grpc.ServerStream to return a custom context
type wrappedStream struct {
	grpc.ServerStream
	ctx context.Context
}

func (w *wrappedStream) Context() context.Context {
	return w.ctx
}

func newWrappedStream(ctx context.Context, s grpc.ServerStream) grpc.ServerStream {
	return &wrappedStream{s, ctx}
}

func authStreamInterceptor(
	logger logger.Logger,
	authenticator *auth.Authenticator,
) grpc.StreamServerInterceptor {
	return func(srv interface{}, ss grpc.ServerStream, _ *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		ctx, err := withCaller(ss.Context(), authenticator)
		if err != nil {
			return err
		}

		if err := handler(srv, newWrappedStream(ctx, ss)); err != nil {
			return handleError(err, logger)
		}

		return nil
	}
}

func handleError(err error, logger logger.Logger) error {
	// Log certain errors to make troubleshooting easier.
	switch errors.ErrorCode(err) {
	case errors.EInternal:
		// Don't log context deadline expired and context cancelled errors.
		if !(goerrors.Is(err, context.DeadlineExceeded) || errors.IsContextCanceledError(err)) {
			logger.Errorf("Unexpected gRPC error occurred: %s", err.Error())
		}
	}
	// Convert error to gRPC equivalent.
	return buildStatusError(err)
}

func withCaller(ctx context.Context, authenticator *auth.Authenticator) (context.Context, error) {
	// Get the metadata from the request context.
	meta, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Errorf(codes.InvalidArgument, "failed to extract metadata from request")
	}

	// metadata keys are always lowercase.
	var token string
	if authHeader, ok := meta["authorization"]; ok {
		token = authHeader[0]
	}

	// Authenticate the caller.
	caller, err := authenticator.Authenticate(ctx, token, false)
	if err != nil && errors.ErrorCode(err) != errors.EUnauthorized {
		return nil, buildStatusError(err)
	}

	if caller != nil {
		ctx = auth.WithCaller(ctx, caller)
	}

	return ctx, nil
}
