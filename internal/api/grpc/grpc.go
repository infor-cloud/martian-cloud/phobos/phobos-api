// Package grpc implements gRPC functionality.
package grpc

import (
	"net"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/grpc/servers"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/apiserver/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/organization"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/project"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/release"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/resourcelimit"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/team"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/version"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"
)

// ServerOptions contains the options to configure the gRPC server.
type ServerOptions struct {
	EnvironmentService       environment.Service
	OrganizationService      organization.Service
	ServiceAccountService    serviceaccount.Service
	ResourceLimitService     resourcelimit.Service
	ProjectService           project.Service
	JobService               job.Service
	PipelineService          pipeline.Service
	PipelineTemplateService  pipelinetemplate.Service
	LifecycleTemplateService lifecycletemplate.Service
	AgentService             agent.Service
	Listener                 net.Listener
	Logger                   logger.Logger
	TeamService              team.Service
	ReleaseLifecycleService  releaselifecycle.Service
	ReleaseService           release.Service
	PluginRegistryService    pluginregistry.Service
	VCSService               vcs.Service
	VersionService           version.Service
	Authenticator            *auth.Authenticator
	APIServerConfig          *config.Config
	URNResolver              *urn.Resolver
	OAuthProviders           []config.IdpConfig
}

// Server implements functions needed to configure, start and stop the gRPC server.
type Server struct {
	server  *grpc.Server
	options *ServerOptions
}

// NewServer creates a new gRPC server.
func NewServer(options *ServerOptions) *Server {
	opts := []grpc.ServerOption{
		grpc.ChainStreamInterceptor(
			authStreamInterceptor(options.Logger, options.Authenticator),
			otelgrpc.StreamServerInterceptor(),
		),
		grpc.ChainUnaryInterceptor(
			// Authentication.
			authUnaryInterceptor(options.Logger, options.Authenticator),
			// OTEL tracing.
			otelgrpc.UnaryServerInterceptor(),
		),
		grpc.KeepaliveParams(keepalive.ServerParameters{
			// Max time that a connection can exist without any active RPCs
			MaxConnectionIdle: 1 * time.Hour,
			// After this time the server will ping the client to determine if it's still alive
			Time: 1 * time.Minute,
			// The amount of time the server will wait for the client to respond to a ping
			Timeout: 30 * time.Second,
		}),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			// Minumum time a client should wait before sending a keepalive ping.
			// IMPORTANT: This value should be less than the client's keepalive time.
			MinTime: 30 * time.Second,
			// Permit keepalive pings without active streams.
			PermitWithoutStream: true,
		}),
	}

	s := grpc.NewServer(opts...)

	// Create servers instances.
	var (
		orgServer              = servers.NewOrganizationServer(options.OrganizationService, options.URNResolver)
		authSettingsServer     = servers.NewAuthSettingsServer(options.OAuthProviders)
		resLimServer           = servers.NewResourceLimitServer(options.ResourceLimitService)
		projServer             = servers.NewProjectServer(options.ProjectService, options.URNResolver)
		jobServer              = servers.NewJobServer(options.JobService, options.URNResolver)
		teamServer             = servers.NewTeamServer(options.TeamService, options.URNResolver)
		pipelineServer         = servers.NewPipelineServer(options.PipelineService, options.URNResolver)
		pipelineTemplateServer = servers.NewPipelineTemplateServer(options.Logger,
			options.PipelineTemplateService, options.URNResolver, options.APIServerConfig.TemplateMaxUploadSize)
		agentServer             = servers.NewAgentServer(options.AgentService, options.URNResolver)
		environmentServer       = servers.NewEnvironmentServer(options.EnvironmentService, options.URNResolver)
		serviceAccountServer    = servers.NewServiceAccountServer(options.ServiceAccountService, options.URNResolver)
		lifecycleTemplateServer = servers.NewLifecycleTemplateServer(options.Logger,
			options.LifecycleTemplateService, options.URNResolver, options.APIServerConfig.TemplateMaxUploadSize)
		releaseLifecycleServer = servers.NewReleaseLifecycleServer(options.ReleaseLifecycleService, options.URNResolver)
		releaseServer          = servers.NewReleaseServer(options.ReleaseService, options.URNResolver)
		pluginRegistryServer   = servers.NewPluginRegistryServer(options.PluginRegistryService, options.URNResolver,
			options.APIServerConfig.PluginBinaryMaxUploadSize)
		vcsProviderServer = servers.NewVCSProviderServer(options.Logger, options.VCSService, options.URNResolver)
		versionServer     = servers.NewVersionServer(options.VersionService)
	)

	// Register servers.
	pb.RegisterOrganizationsServer(s, orgServer)
	pb.RegisterAuthSettingsServer(s, authSettingsServer)
	pb.RegisterResourceLimitsServer(s, resLimServer)
	pb.RegisterProjectsServer(s, projServer)
	pb.RegisterJobsServer(s, jobServer)
	pb.RegisterTeamsServer(s, teamServer)
	pb.RegisterPipelinesServer(s, pipelineServer)
	pb.RegisterPipelineTemplatesServer(s, pipelineTemplateServer)
	pb.RegisterAgentsServer(s, agentServer)
	pb.RegisterEnvironmentsServer(s, environmentServer)
	pb.RegisterServiceAccountsServer(s, serviceAccountServer)
	pb.RegisterLifecycleTemplatesServer(s, lifecycleTemplateServer)
	pb.RegisterReleaseLifecyclesServer(s, releaseLifecycleServer)
	pb.RegisterReleasesServer(s, releaseServer)
	pb.RegisterPluginRegistryServer(s, pluginRegistryServer)
	pb.RegisterVCSProvidersServer(s, vcsProviderServer)
	pb.RegisterVersionServer(s, versionServer)

	// Enable reflection which makes it easier to use grpcui and alike
	// without needing to import proto files.
	reflection.Register(s)

	return &Server{
		options: options,
		server:  s,
	}
}

// Start starts the server.
func (s *Server) Start() error {
	s.options.Logger.Infof("gRPC server listening on %s", s.options.Listener.Addr())
	return s.server.Serve(s.options.Listener)
}

// Shutdown gracefully stops the server. Will block until
// all pending RPCs are finished.
func (s *Server) Shutdown() {
	s.options.Logger.Info("Gracefully shutting down gRPC server")
	s.server.GracefulStop()
	s.options.Logger.Info("Successfully shutdown gRPC server")
}
