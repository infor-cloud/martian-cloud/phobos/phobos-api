package grpc

import (
	"context"
	goerrors "errors"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// errorToStatusCode maps from API errors to gRPC status code.
var errorToStatusCode = map[errors.CodeType]codes.Code{
	errors.EInternal:        codes.Internal,
	errors.ETooLarge:        codes.InvalidArgument,
	errors.EInvalid:         codes.InvalidArgument,
	errors.ENotImplemented:  codes.Unimplemented,
	errors.EConflict:        codes.AlreadyExists,
	errors.EOptimisticLock:  codes.Aborted,
	errors.ENotFound:        codes.NotFound,
	errors.EForbidden:       codes.PermissionDenied,
	errors.ETooManyRequests: codes.ResourceExhausted,
	errors.EUnauthorized:    codes.Unauthenticated,
}

// buildStatusError builds a gRPC error from an API error.
func buildStatusError(err error) error {
	var grpcCode codes.Code
	var grpcMsg string

	switch {
	case goerrors.Is(err, context.DeadlineExceeded):
		grpcCode = codes.DeadlineExceeded
		grpcMsg = err.Error()
	case errors.IsContextCanceledError(err):
		grpcCode = codes.Canceled
		grpcMsg = err.Error()
	default:
		grpcCode = errorToStatusCode[errors.ErrorCode(err)]
		grpcMsg = errors.ErrorMessage(err)
	}

	return status.Error(grpcCode, grpcMsg)
}
