// Package middleware allows adding custom middleware(s) to
// the RESTful API, such as, JWT based authentication.
package middleware

import "net/http"

// Handler is used for returning middleware functions
type Handler func(next http.Handler) http.Handler
