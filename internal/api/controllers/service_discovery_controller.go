package controllers

import (
	"fmt"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/response"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// discoveryController implements the http.Handler interface and is used to serve the service discovery documents.
type discoveryController struct {
	logger            logger.Logger
	respWriter        response.Writer
	apiURL            string
	host              string
	port              string
	transportSecurity string
}

// NewServiceDiscoveryController returns a new discoveryHandler.
func NewServiceDiscoveryController(logger logger.Logger, respWriter response.Writer, apiURL, port string) (Controller, error) {
	parsedURL, err := url.Parse(apiURL)
	if err != nil {
		logger.Error("error parsing API URL", err)
		return nil, err
	}

	transportSecurity := "plaintext"
	if parsedURL.Scheme == "https" {
		transportSecurity = "tls"
	}

	return &discoveryController{
		logger:            logger,
		respWriter:        respWriter,
		apiURL:            parsedURL.String(),
		host:              parsedURL.Hostname(),
		port:              port,
		transportSecurity: transportSecurity,
	}, err
}

// RegisterRoutes adds the service discover routes to the router.
func (c *discoveryController) RegisterRoutes(router chi.Router) {
	router.Get("/.well-known/phobos.json", c.getServiceDiscovery)
}

func (c *discoveryController) getServiceDiscovery(w http.ResponseWriter, _ *http.Request) {
	body := map[string]any{
		"plugins.v1": fmt.Sprintf("%s/v1/plugin-registry/plugins/", c.apiURL),
		"grpc": &client.GRPCDiscoveryDocument{
			Host:              c.host,
			TransportSecurity: c.transportSecurity,
			Port:              c.port,
		},
	}

	c.respWriter.RespondWithJSON(w, body, http.StatusOK)
}
