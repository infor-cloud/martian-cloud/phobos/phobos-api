package controllers

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/response"
)

type health struct {
	Ok bool `json:"ok"`
}

var healthy = health{Ok: true}

type healthController struct {
	respWriter response.Writer
}

// NewHealthController creates an instance of HealthController
func NewHealthController(respWriter response.Writer) Controller {
	return &healthController{respWriter}
}

// RegisterRoutes adds health routes to the router
func (c *healthController) RegisterRoutes(router chi.Router) {
	router.Get("/health", c.GetHealth)
}

// GetHealth returns HTTP status 200
func (c *healthController) GetHealth(w http.ResponseWriter, _ *http.Request) {
	c.respWriter.RespondWithJSON(w, healthy, 200)
}
