package controllers

import (
	"net/http"

	"github.com/aws/smithy-go/ptr"
	"github.com/go-chi/chi/v5"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/middleware"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/response"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// pluginRegistryController is the controller for the plugin registry endpoints
type pluginRegistryController struct {
	logger                logger.Logger
	respWriter            response.Writer
	jwtAuthMiddleware     middleware.Handler
	pluginRegistryService pluginregistry.Service
}

// NewPluginRegistryController creates a new instance of the pluginRegistryController
func NewPluginRegistryController(
	logger logger.Logger,
	respWriter response.Writer,
	jwtAuthMiddleware middleware.Handler,
	pluginRegistryService pluginregistry.Service,
) Controller {
	return &pluginRegistryController{
		logger:                logger,
		respWriter:            respWriter,
		jwtAuthMiddleware:     jwtAuthMiddleware,
		pluginRegistryService: pluginRegistryService,
	}
}

// RegisterRoutes registers the plugin registry routes
func (c *pluginRegistryController) RegisterRoutes(router chi.Router) {
	// Require JWT authentication.
	router.Use(c.jwtAuthMiddleware)

	router.Get("/plugin-registry/plugins/{organization}/{name}/versions", c.GetVersions)
	router.Get("/plugin-registry/plugins/{organization}/{name}/{version}/download/{os}/{arch}", c.GetVersion)
}

// GetVersions returns the versions of a plugin
func (c *pluginRegistryController) GetVersions(w http.ResponseWriter, r *http.Request) {
	organization := chi.URLParam(r, "organization")
	name := chi.URLParam(r, "name")

	foundPlugin, err := c.pluginRegistryService.GetPluginByPRN(r.Context(), models.PluginResource.BuildPRN(organization, name))
	if err != nil {
		c.respWriter.RespondWithError(w, err)
		return
	}

	versionsResponse, err := c.pluginRegistryService.GetPluginVersions(r.Context(), &pluginregistry.GetPluginVersionsInput{
		PluginID:        foundPlugin.Metadata.ID,
		SHASumsUploaded: ptr.Bool(true),
	})
	if err != nil {
		c.respWriter.RespondWithError(w, err)
		return
	}

	platformsResponse, err := c.pluginRegistryService.GetPluginPlatforms(r.Context(), &pluginregistry.GetPluginPlatformsInput{
		PluginID:       &foundPlugin.Metadata.ID,
		BinaryUploaded: ptr.Bool(true),
	})
	if err != nil {
		c.respWriter.RespondWithError(w, err)
		return
	}

	platformMap := map[string][]*models.PluginPlatform{}
	for _, p := range platformsResponse.PluginPlatforms {
		pCopy := p
		if _, ok := platformMap[p.PluginVersionID]; !ok {
			platformMap[p.PluginVersionID] = []*models.PluginPlatform{}
		}
		platformMap[p.PluginVersionID] = append(platformMap[p.PluginVersionID], &pCopy)
	}

	response := &plugin.RegistryPluginVersionList{Versions: []plugin.RegistryPluginVersion{}}

	for _, v := range versionsResponse.PluginVersions {
		convertedVersion := plugin.RegistryPluginVersion{
			Version:   v.SemanticVersion,
			Protocols: v.Protocols,
			Platforms: []plugin.RegistryPluginPlatform{},
		}

		if platforms, ok := platformMap[v.Metadata.ID]; ok {
			for _, p := range platforms {
				convertedVersion.Platforms = append(convertedVersion.Platforms, plugin.RegistryPluginPlatform{
					OperatingSystem: p.OperatingSystem,
					Arch:            p.Architecture,
				})
			}
		}

		response.Versions = append(response.Versions, convertedVersion)
	}

	c.respWriter.RespondWithJSON(w, &response, 200)
}

// GetVersion returns the download information for a plugin version
func (c *pluginRegistryController) GetVersion(w http.ResponseWriter, r *http.Request) {
	organization := chi.URLParam(r, "organization")
	pluginName := chi.URLParam(r, "name")
	version := chi.URLParam(r, "version")
	os := chi.URLParam(r, "os")
	arch := chi.URLParam(r, "arch")

	pluginVersion, err := c.pluginRegistryService.GetPluginVersionByPRN(r.Context(), models.PluginVersionResource.BuildPRN(organization, pluginName, version))
	if err != nil {
		c.respWriter.RespondWithError(w, err)
		return
	}

	if !pluginVersion.SHASumsUploaded {
		c.respWriter.RespondWithError(w, errors.New("plugin version %s not found", version, errors.WithErrorCode(errors.ENotFound)))
		return
	}

	pluginPlatform, err := c.pluginRegistryService.GetPluginPlatformByPRN(r.Context(), models.PluginPlatformResource.BuildPRN(organization, pluginName, version, os, arch))
	if err != nil {
		c.respWriter.RespondWithError(w, err)
		return
	}

	if !pluginPlatform.BinaryUploaded {
		c.respWriter.RespondWithError(w, errors.New("plugin platform %s_%s not found", os, arch, errors.WithErrorCode(errors.ENotFound)))
		return
	}

	downloadURLs, err := c.pluginRegistryService.GetPluginPlatformDownloadURLs(r.Context(), pluginPlatform.Metadata.ID)
	if err != nil {
		c.respWriter.RespondWithError(w, err)
		return
	}

	downloadResponse := plugin.RegistryPluginDownloadResponse{
		Protocols:       pluginVersion.Protocols,
		OperatingSystem: pluginPlatform.OperatingSystem,
		Arch:            pluginPlatform.Architecture,
		Filename:        pluginPlatform.Filename,
		DownloadURL:     downloadURLs.DownloadURL,
		SHASumsURL:      downloadURLs.SHASumsURL,
		SHASum:          pluginPlatform.SHASum,
	}

	c.respWriter.RespondWithJSON(w, &downloadResponse, 200)
}
