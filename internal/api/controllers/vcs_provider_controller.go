package controllers

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/response"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const (
	// oAuthCallbackResponseBody is the response returned for a successful
	// OAuth flow completion.
	oAuthCallbackResponseBody = `
<html>
<head>
<title>VCS Provider OAuth Setup</title>
<style type="text/css">
body {
	font-family: monospace;
	color: #fff;
	background-color: #000;
}
</style>
</head>
<body>
<p>Phobos has authenticated with the VCS provider successfully. This page can now be closed.</p>
</body>
</html>
`
)

type vcsController struct {
	logger        logger.Logger
	respWriter    response.Writer
	authenticator *auth.Authenticator
	vcsService    vcs.Service
}

// NewVCSController creates an instance of vcsController.
func NewVCSController(
	logger logger.Logger,
	respWriter response.Writer,
	authenticator *auth.Authenticator,
	vcsService vcs.Service,
) Controller {
	return &vcsController{
		logger,
		respWriter,
		authenticator,
		vcsService,
	}
}

// RegisterRoutes adds routes to the router.
func (c *vcsController) RegisterRoutes(router chi.Router) {
	// OAuth handler.
	router.Get("/vcs/auth/callback", c.OAuthHandler)
}

func (c *vcsController) OAuthHandler(w http.ResponseWriter, r *http.Request) {
	queries := r.URL.Query()

	// Use the system caller.
	request := r.WithContext(auth.WithCaller(r.Context(), &auth.SystemCaller{}))

	if err := c.vcsService.ProcessOAuth(request.Context(), &vcs.ProcessOAuthInput{
		AuthorizationCode: queries.Get("code"),
		State:             queries.Get("state"),
	}); err != nil {
		// Return a simple EUnauthorized here.
		c.logger.Infof("Unauthorized request to %s %s: %v", r.Method, r.URL.Path, err)
		c.respWriter.RespondWithError(w, errors.New("Unauthorized", errors.WithErrorCode(errors.EUnauthorized)))
		return
	}

	// Return some HTML indicating the flow has completed.
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write([]byte(oAuthCallbackResponseBody)); err != nil {
		c.logger.Errorf("failed to write callback response body in OAuthHandler: %v", err)
		c.respWriter.RespondWithError(w, errors.New("Internal error has occurred"))
	}
}
