// Package response providers support for returning http responses
package response

import (
	"encoding/json"
	"net/http"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"

	te "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const contentTypeJSON = "application/json"

type errorResponse struct {
	Detail string `json:"detail"`
}

// Writer provides utility functions for responding to http requests
type Writer interface {
	RespondWithError(w http.ResponseWriter, err error)
	RespondWithJSON(w http.ResponseWriter, model interface{}, statusCode int)
}

type responseHelper struct {
	logger logger.Logger
}

var errorToStatusCode = map[te.CodeType]int{
	te.EInternal:        http.StatusInternalServerError,
	te.ENotImplemented:  http.StatusNotImplemented,
	te.EInvalid:         http.StatusBadRequest,
	te.EConflict:        http.StatusConflict,
	te.ENotFound:        http.StatusNotFound,
	te.EForbidden:       http.StatusForbidden,
	te.ETooManyRequests: http.StatusTooManyRequests,
	te.EUnauthorized:    http.StatusUnauthorized,
	te.ETooLarge:        http.StatusRequestEntityTooLarge,
}

// NewWriter creates an instance of Writer
func NewWriter(logger logger.Logger) Writer {
	return &responseHelper{logger}
}

// RespondWithError responds to an http request with an error response
func (rh *responseHelper) RespondWithError(w http.ResponseWriter, err error) {
	if !te.IsContextCanceledError(err) && te.ErrorCode(err) == te.EInternal {
		// Log the error message.
		rh.logger.Errorf("Unexpected error occurred: %s", err.Error())
	}
	rh.respondWithError(w, ErrorCodeToStatusCode(te.ErrorCode(err)), te.ErrorMessage(err))
}

// RespondWithJSON responds to an http request with a json payload
func (rh *responseHelper) RespondWithJSON(w http.ResponseWriter, model interface{}, statusCode int) {
	w.Header().Set("Content-Type", contentTypeJSON)
	w.WriteHeader(statusCode)

	if model != nil {
		response, err := json.Marshal(model)
		if err != nil {
			rh.RespondWithError(w, err)
			return
		}

		if _, err := w.Write(response); err != nil {
			rh.RespondWithError(w, err)
			return
		}
	}
}

func (rh *responseHelper) respondWithError(w http.ResponseWriter, code int, msg string) {
	rh.RespondWithJSON(w, &errorResponse{Detail: msg}, code)
}

// ErrorCodeToStatusCode maps an error code string to a
// http status code integer.
func ErrorCodeToStatusCode(code te.CodeType) int {
	// Otherwise map internal error codes to HTTP status codes.
	statusCode, ok := errorToStatusCode[code]
	if ok {
		return statusCode
	}
	return http.StatusInternalServerError
}
