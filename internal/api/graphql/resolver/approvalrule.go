package resolver

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/approvalrule"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
)

/* ApprovalRule Query Resolvers */

// ApprovalRuleConnectionQueryArgs are used to query an approval rule connection
type ApprovalRuleConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search *string
	Scopes *[]models.ScopeType
}

// ApprovalRuleEdgeResolver resolves approvalRule edges
type ApprovalRuleEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ApprovalRuleEdgeResolver) Cursor() (string, error) {
	approvalRule, ok := r.edge.Node.(*models.ApprovalRule)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(approvalRule)
	return *cursor, err
}

// Node returns a approval rule node
func (r *ApprovalRuleEdgeResolver) Node() (*ApprovalRuleResolver, error) {
	approvalRule, ok := r.edge.Node.(*models.ApprovalRule)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ApprovalRuleResolver{rule: approvalRule}, nil
}

// ApprovalRuleConnectionResolver resolves a approvalRule connection
type ApprovalRuleConnectionResolver struct {
	connection Connection
}

// NewApprovalRuleConnectionResolver creates a new ApprovalRuleConnectionResolver
func NewApprovalRuleConnectionResolver(ctx context.Context, input *approvalrule.GetApprovalRulesInput) (*ApprovalRuleConnectionResolver, error) {
	approvalRuleService := getApprovalRuleService(ctx)

	result, err := approvalRuleService.GetApprovalRules(ctx, input)
	if err != nil {
		return nil, err
	}

	rules := result.ApprovalRules

	// Create edges
	edges := make([]Edge, len(rules))
	for i, approvalRule := range rules {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: approvalRule}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(rules) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(rules[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(rules[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ApprovalRuleConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ApprovalRuleConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ApprovalRuleConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ApprovalRuleConnectionResolver) Edges() *[]*ApprovalRuleEdgeResolver {
	resolvers := make([]*ApprovalRuleEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ApprovalRuleEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ApprovalRuleResolver resolves an approval rule
type ApprovalRuleResolver struct {
	rule *models.ApprovalRule
}

// ID resolver
func (r *ApprovalRuleResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ApprovalRuleType, r.rule.Metadata.ID))
}

// Metadata resolver
func (r *ApprovalRuleResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.rule.Metadata}
}

// Users resolver
func (r *ApprovalRuleResolver) Users(ctx context.Context) ([]*UserResolver, error) {
	resolvers := []*UserResolver{}

	for _, userID := range r.rule.UserIDs {
		user, err := loadUser(ctx, userID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &UserResolver{user: user})
	}

	return resolvers, nil
}

// ServiceAccounts resolver
func (r *ApprovalRuleResolver) ServiceAccounts(ctx context.Context) ([]*ServiceAccountResolver, error) {
	resolvers := []*ServiceAccountResolver{}

	for _, serviceAccountID := range r.rule.ServiceAccountIDs {
		sa, err := loadServiceAccount(ctx, serviceAccountID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &ServiceAccountResolver{serviceAccount: sa})
	}

	return resolvers, nil
}

// Teams resolver
func (r *ApprovalRuleResolver) Teams(ctx context.Context) ([]*TeamResolver, error) {
	resolvers := []*TeamResolver{}

	for _, teamID := range r.rule.TeamIDs {
		team, err := loadTeam(ctx, teamID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &TeamResolver{team: team})
	}

	return resolvers, nil
}

// Name resolver
func (r *ApprovalRuleResolver) Name() string {
	return r.rule.Name
}

// Description resolver
func (r *ApprovalRuleResolver) Description() string {
	return r.rule.Description
}

// CreatedBy resolver
func (r *ApprovalRuleResolver) CreatedBy() string {
	return r.rule.CreatedBy
}

// ApprovalsRequired resolver
func (r *ApprovalRuleResolver) ApprovalsRequired() int32 {
	return int32(r.rule.ApprovalsRequired)
}

// Scope resolver
func (r *ApprovalRuleResolver) Scope() models.ScopeType {
	return r.rule.Scope
}

// Organization resolver
func (r *ApprovalRuleResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	org, err := loadOrganization(ctx, r.rule.OrgID)
	if err != nil {
		return nil, err
	}
	return &OrganizationResolver{organization: org}, nil
}

// Project resolver
func (r *ApprovalRuleResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.rule.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.rule.ProjectID)
	if err != nil {
		return nil, err
	}
	return &ProjectResolver{project: project}, nil
}

/* ApprovalRule Mutation Resolvers */

// ApprovalRuleMutationPayload is the response payload for a approvalRule mutation
type ApprovalRuleMutationPayload struct {
	ClientMutationID *string
	ApprovalRule     *models.ApprovalRule
	Problems         []Problem
}

// ApprovalRuleMutationPayloadResolver resolves a ApprovalRuleMutationPayload
type ApprovalRuleMutationPayloadResolver struct {
	ApprovalRuleMutationPayload
}

// ApprovalRule field resolver
func (r *ApprovalRuleMutationPayloadResolver) ApprovalRule() *ApprovalRuleResolver {
	if r.ApprovalRuleMutationPayload.ApprovalRule == nil {
		return nil
	}
	return &ApprovalRuleResolver{rule: r.ApprovalRuleMutationPayload.ApprovalRule}
}

// CreateApprovalRuleInput is the input for creating a new approval rule
type CreateApprovalRuleInput struct {
	ClientMutationID  *string
	Name              string
	Description       string
	Scope             models.ScopeType
	OrganizationID    *string // depending on scope, either OrgID or ProjectID is required
	ProjectID         *string
	Teams             []string
	Users             []string
	ServiceAccounts   []string
	ApprovalsRequired int32
}

// UpdateApprovalRuleInput is the input for updating an existing approval rule
type UpdateApprovalRuleInput struct {
	Metadata         *MetadataInput
	ClientMutationID *string
	Teams            *[]string
	Users            *[]string
	ServiceAccounts  *[]string
	Description      *string
	ID               string
}

// DeleteApprovalRuleInput is the input for deleting an approval rule
type DeleteApprovalRuleInput struct {
	Metadata         *MetadataInput
	ClientMutationID *string
	ID               string
}

func handleApprovalRuleMutationProblem(e error, clientMutationID *string) (*ApprovalRuleMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := ApprovalRuleMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ApprovalRuleMutationPayloadResolver{ApprovalRuleMutationPayload: payload}, nil
}

func createApprovalRuleMutation(ctx context.Context, input *CreateApprovalRuleInput) (*ApprovalRuleMutationPayloadResolver, error) {
	var allowedUserIDs, allowedServiceAccountIDs, allowedTeamIDs []string
	var err error

	allowedUserIDs, err = convertPRNList(ctx, input.Users)
	if err != nil {
		return nil, err
	}

	allowedServiceAccountIDs, err = convertPRNList(ctx, input.ServiceAccounts)
	if err != nil {
		return nil, err
	}

	allowedTeamIDs, err = convertPRNList(ctx, input.Teams)
	if err != nil {
		return nil, err
	}

	if (input.OrganizationID != nil) && (input.ProjectID != nil) {
		return nil, errors.New("specifying both orgID and project ID is not allowed", errors.WithErrorCode(errors.EInvalid))
	}

	var orgID, projectID *string
	urnResolver := getURNResolver(ctx)
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("organization ID is required for an organization approval rule",
				errors.WithErrorCode(errors.EInvalid))
		}

		oID, oErr := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if oErr != nil {
			return nil, oErr
		}

		orgID = &oID
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("project id is required for a project approval rule",
				errors.WithErrorCode(errors.EInvalid))
		}

		pID, pErr := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if pErr != nil {
			return nil, pErr
		}

		projectID = &pID
	default:
		return nil, errors.New("approval rule must have either organization or project scope",
			errors.WithErrorCode(errors.EInvalid))
	}

	createdRule, err := getApprovalRuleService(ctx).CreateApprovalRule(ctx,
		&approvalrule.CreateApprovalRuleInput{
			Scope:             input.Scope,
			OrgID:             orgID,
			ProjectID:         projectID,
			Name:              input.Name,
			Description:       input.Description,
			UserIDs:           allowedUserIDs,
			ServiceAccountIDs: allowedServiceAccountIDs,
			TeamIDs:           allowedTeamIDs,
			ApprovalsRequired: int(input.ApprovalsRequired),
		})
	if err != nil {
		return nil, err
	}

	payload := ApprovalRuleMutationPayload{ClientMutationID: input.ClientMutationID, ApprovalRule: createdRule, Problems: []Problem{}}
	return &ApprovalRuleMutationPayloadResolver{ApprovalRuleMutationPayload: payload}, nil
}

func updateApprovalRuleMutation(ctx context.Context, input *UpdateApprovalRuleInput) (*ApprovalRuleMutationPayloadResolver, error) {
	var err error

	ruleID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	updateRuleOptions := approvalrule.UpdateApprovalRuleInput{
		ID: ruleID,
	}

	if input.Description != nil {
		updateRuleOptions.Description = input.Description
	}

	if input.Users != nil {
		updateRuleOptions.UserIDs, err = convertPRNList(ctx, *input.Users)
		if err != nil {
			return nil, err
		}
	}

	if input.ServiceAccounts != nil {
		updateRuleOptions.ServiceAccountIDs, err = convertPRNList(ctx, *input.ServiceAccounts)
		if err != nil {
			return nil, err
		}
	}

	if input.Teams != nil {
		updateRuleOptions.TeamIDs, err = convertPRNList(ctx, *input.Teams)
		if err != nil {
			return nil, err
		}
	}

	if input.Metadata != nil {
		v, vErr := strconv.Atoi(input.Metadata.Version)
		if vErr != nil {
			return nil, vErr
		}
		updateRuleOptions.Version = &v
	}

	updatedRule, err := getApprovalRuleService(ctx).UpdateApprovalRule(ctx, &updateRuleOptions)
	if err != nil {
		return nil, err
	}

	payload := ApprovalRuleMutationPayload{ClientMutationID: input.ClientMutationID, ApprovalRule: updatedRule, Problems: []Problem{}}
	return &ApprovalRuleMutationPayloadResolver{ApprovalRuleMutationPayload: payload}, nil
}

func deleteApprovalRuleMutation(ctx context.Context, input *DeleteApprovalRuleInput) (*ApprovalRuleMutationPayloadResolver, error) {
	ruleID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	service := getApprovalRuleService(ctx)

	rule, err := service.GetApprovalRuleByID(ctx, ruleID)
	if err != nil {
		return nil, err
	}

	deleteOptions := &approvalrule.DeleteApprovalRuleInput{
		ID: ruleID,
	}

	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}
		deleteOptions.Version = &v
	}

	if err := service.DeleteApprovalRule(ctx, deleteOptions); err != nil {
		return nil, err
	}

	payload := ApprovalRuleMutationPayload{ClientMutationID: input.ClientMutationID, ApprovalRule: rule, Problems: []Problem{}}
	return &ApprovalRuleMutationPayloadResolver{ApprovalRuleMutationPayload: payload}, nil
}

/* ApprovalRule loader */

const approvalRuleLoaderKey = "approvalRule"

// RegisterApprovalRuleLoader registers a approvalRule loader function
func RegisterApprovalRuleLoader(collection *loader.Collection) {
	collection.Register(approvalRuleLoaderKey, approvalRuleBatchFunc)
}

func loadApprovalRule(ctx context.Context, id string) (*models.ApprovalRule, error) {
	ldr, err := loader.Extract(ctx, approvalRuleLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	approvalRule, ok := data.(*models.ApprovalRule)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return approvalRule, nil
}

func approvalRuleBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	rules, err := getApprovalRuleService(ctx).GetApprovalRulesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range rules {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
