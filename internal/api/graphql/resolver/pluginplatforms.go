package resolver

import (
	"context"

	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
)

/* PluginPlatform Query resolvers */

// PluginPlatformResolver resolves a plugin platform
type PluginPlatformResolver struct {
	pluginPlatform *models.PluginPlatform
}

// ID returns the plugin platform ID
func (r *PluginPlatformResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.PluginPlatformType, r.pluginPlatform.Metadata.ID))
}

// Metadata resolver
func (r *PluginPlatformResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.pluginPlatform.Metadata}
}

// CreatedBy resolver
func (r *PluginPlatformResolver) CreatedBy() string {
	return r.pluginPlatform.CreatedBy
}

// OS resolver
func (r *PluginPlatformResolver) OS() string {
	return r.pluginPlatform.OperatingSystem
}

// Arch resolver
func (r *PluginPlatformResolver) Arch() string {
	return r.pluginPlatform.Architecture
}

// SHASum resolver
func (r *PluginPlatformResolver) SHASum() string {
	return r.pluginPlatform.SHASum
}

// Filename resolver
func (r *PluginPlatformResolver) Filename() string {
	return r.pluginPlatform.Filename
}

// BinaryUploaded resolver
func (r *PluginPlatformResolver) BinaryUploaded() bool {
	return r.pluginPlatform.BinaryUploaded
}

// PluginVersion resolver
func (r *PluginPlatformResolver) PluginVersion(ctx context.Context) (*PluginVersionResolver, error) {
	pluginVersion, err := loadPluginVersion(ctx, r.pluginPlatform.PluginVersionID)
	if err != nil {
		return nil, err
	}

	return &PluginVersionResolver{pluginVersion: pluginVersion}, nil
}
