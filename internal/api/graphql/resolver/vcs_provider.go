package resolver

import (
	"context"
	"strconv"

	"github.com/graph-gophers/dataloader"
	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// VCSProviderConnectionQueryArgs are used to query a vcsProvider connection
type VCSProviderConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search *string
}

// VCSProviderEdgeResolver resolves vcsProvider edges
type VCSProviderEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *VCSProviderEdgeResolver) Cursor() (string, error) {
	vcsProvider, ok := r.edge.Node.(models.VCSProvider)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&vcsProvider)
	return *cursor, err
}

// Node returns a vcsProvider node
func (r *VCSProviderEdgeResolver) Node() (*VCSProviderResolver, error) {
	vcsProvider, ok := r.edge.Node.(models.VCSProvider)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &VCSProviderResolver{vcsProvider: &vcsProvider}, nil
}

// VCSProviderConnectionResolver resolves a vcs provider connection
type VCSProviderConnectionResolver struct {
	connection Connection
}

// NewVCSProviderConnectionResolver creates a new VCSProviderConnectionResolver
func NewVCSProviderConnectionResolver(ctx context.Context, input *vcs.GetVCSProvidersInput) (*VCSProviderConnectionResolver, error) {
	vcsService := getVCSService(ctx)

	result, err := vcsService.GetVCSProviders(ctx, input)
	if err != nil {
		return nil, err
	}

	vcsProviders := result.VCSProviders

	// Create edges
	edges := make([]Edge, len(vcsProviders))
	for i, vcsProvider := range vcsProviders {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: vcsProvider}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(vcsProviders) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&vcsProviders[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&vcsProviders[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &VCSProviderConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *VCSProviderConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *VCSProviderConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *VCSProviderConnectionResolver) Edges() *[]*VCSProviderEdgeResolver {
	resolvers := make([]*VCSProviderEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &VCSProviderEdgeResolver{edge: edge}
	}
	return &resolvers
}

// VCSProviderResolver resolves a vcsProvider resource
type VCSProviderResolver struct {
	vcsProvider *models.VCSProvider
}

// ID resolver
func (r *VCSProviderResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.VCSProviderType, r.vcsProvider.Metadata.ID))
}

// Name resolver
func (r *VCSProviderResolver) Name() string {
	return r.vcsProvider.Name
}

// Description resolver
func (r *VCSProviderResolver) Description() string {
	return r.vcsProvider.Description
}

// Metadata resolver
func (r *VCSProviderResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.vcsProvider.Metadata}
}

// CreatedBy resolver
func (r *VCSProviderResolver) CreatedBy() string {
	return r.vcsProvider.CreatedBy
}

// OrganizationName resolver
func (r *VCSProviderResolver) OrganizationName() string {
	return r.vcsProvider.GetOrgName()
}

// Organization resolver
func (r *VCSProviderResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	organization, err := loadOrganization(ctx, r.vcsProvider.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Project resolver
func (r *VCSProviderResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.vcsProvider.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.vcsProvider.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// URL resolver
func (r *VCSProviderResolver) URL() string {
	return r.vcsProvider.URL.String()
}

// Scope resolver
func (r *VCSProviderResolver) Scope() models.ScopeType {
	return r.vcsProvider.Scope
}

// Type resolver
func (r *VCSProviderResolver) Type() models.VCSProviderType {
	return r.vcsProvider.Type
}

// AuthType resolver
func (r *VCSProviderResolver) AuthType() models.VCSProviderAuthType {
	return r.vcsProvider.AuthType
}

// ExtraOAuthScopes resolver
func (r *VCSProviderResolver) ExtraOAuthScopes() []string {
	return r.vcsProvider.ExtraOAuthScopes
}

/* VCSProvider Mutation Resolvers */

// ResetVCSProviderOAuthTokenMutationPayload is the response payload for
// resetting a OAuth token.
type ResetVCSProviderOAuthTokenMutationPayload struct {
	ClientMutationID      *string
	VCSProvider           *models.VCSProvider
	OAuthAuthorizationURL string
	Problems              []Problem
}

// ResetVCSProviderOAuthTokenMutationPayloadResolver resolves a ResetVCSProviderOAuthTokenPayload
type ResetVCSProviderOAuthTokenMutationPayloadResolver struct {
	ResetVCSProviderOAuthTokenMutationPayload
}

// VCSProvider field resolver
func (r *ResetVCSProviderOAuthTokenMutationPayloadResolver) VCSProvider() *VCSProviderResolver {
	if r.ResetVCSProviderOAuthTokenMutationPayload.VCSProvider == nil {
		return nil
	}

	return &VCSProviderResolver{vcsProvider: r.ResetVCSProviderOAuthTokenMutationPayload.VCSProvider}
}

// VCSProviderMutationPayload is the response payload for a vcsProvider mutation
type VCSProviderMutationPayload struct {
	ClientMutationID      *string
	VCSProvider           *models.VCSProvider
	OAuthAuthorizationURL *string
	Problems              []Problem
}

// VCSProviderMutationPayloadResolver resolves a VCSProviderMutationPayload
type VCSProviderMutationPayloadResolver struct {
	VCSProviderMutationPayload
}

// VCSProvider field resolver
func (r *VCSProviderMutationPayloadResolver) VCSProvider() *VCSProviderResolver {
	if r.VCSProviderMutationPayload.VCSProvider == nil {
		return nil
	}

	return &VCSProviderResolver{vcsProvider: r.VCSProviderMutationPayload.VCSProvider}
}

// ResetVCSProviderOAuthTokenInput is the input for resetting a
// VCS provider's OAuth token.
type ResetVCSProviderOAuthTokenInput struct {
	ClientMutationID *string
	ProviderID       string
}

// CreateVCSProviderInput is the input for creating a VCS provider.
type CreateVCSProviderInput struct {
	ClientMutationID    *string
	URL                 *string
	OrganizationID      *string
	ProjectID           *string
	OAuthClientID       *string
	OAuthClientSecret   *string
	PersonalAccessToken *string
	ExtraOAuthScopes    *[]string
	Name                string
	Description         string
	Type                models.VCSProviderType
	Scope               models.ScopeType
	AuthType            models.VCSProviderAuthType
}

// UpdateVCSProviderInput is the input for updating a VCS provider.
type UpdateVCSProviderInput struct {
	ClientMutationID    *string
	Metadata            *MetadataInput
	Description         *string
	OAuthClientID       *string
	OAuthClientSecret   *string
	PersonalAccessToken *string
	ExtraOAuthScopes    *[]string
	ID                  string
}

// DeleteVCSProviderInput is the input for deleting a VCS provider.
type DeleteVCSProviderInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handleVCSProviderMutationProblem(e error, clientMutationID *string) (*VCSProviderMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}

	payload := VCSProviderMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &VCSProviderMutationPayloadResolver{VCSProviderMutationPayload: payload}, nil
}

func handleResetVCSProviderOAuthTokenMutationProblem(e error, clientMutationID *string) (*ResetVCSProviderOAuthTokenMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}

	payload := ResetVCSProviderOAuthTokenMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ResetVCSProviderOAuthTokenMutationPayloadResolver{ResetVCSProviderOAuthTokenMutationPayload: payload}, nil
}

func resetVCSProviderOAuthTokenMutation(ctx context.Context, input *ResetVCSProviderOAuthTokenInput) (*ResetVCSProviderOAuthTokenMutationPayloadResolver, error) {
	id, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ProviderID)
	if err != nil {
		return nil, err
	}

	response, err := getVCSService(ctx).ResetVCSProviderOAuthToken(ctx, &vcs.ResetVCSProviderOAuthTokenInput{ProviderID: id})
	if err != nil {
		return nil, err
	}

	payload := ResetVCSProviderOAuthTokenMutationPayload{
		ClientMutationID:      input.ClientMutationID,
		VCSProvider:           response.VCSProvider,
		OAuthAuthorizationURL: response.OAuthAuthorizationURL,
		Problems:              []Problem{},
	}
	return &ResetVCSProviderOAuthTokenMutationPayloadResolver{ResetVCSProviderOAuthTokenMutationPayload: payload}, nil
}

func createVCSProviderMutation(ctx context.Context, input *CreateVCSProviderInput) (*VCSProviderMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	vcsService := getVCSService(ctx)

	var response *vcs.CreateVCSProviderResponse
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("organization id is required for an organization vcs provider", errors.WithErrorCode(errors.EInvalid))
		}

		organizationID, err := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if err != nil {
			return nil, err
		}

		extraOauthScopes := []string{}
		if input.ExtraOAuthScopes != nil {
			extraOauthScopes = *input.ExtraOAuthScopes
		}

		response, err = vcsService.CreateOrganizationVCSProvider(ctx, &vcs.CreateOrganizationVCSProviderInput{
			Name:                input.Name,
			URL:                 input.URL,
			Description:         input.Description,
			OAuthClientID:       input.OAuthClientID,
			OAuthClientSecret:   input.OAuthClientSecret,
			ExtraOAuthScopes:    extraOauthScopes,
			PersonalAccessToken: input.PersonalAccessToken,
			OrganizationID:      organizationID,
			Type:                input.Type,
			AuthType:            input.AuthType,
		})
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("project id is required for a project vcs provider", errors.WithErrorCode(errors.EInvalid))
		}

		projectID, err := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if err != nil {
			return nil, err
		}

		extraOauthScopes := []string{}
		if input.ExtraOAuthScopes != nil {
			extraOauthScopes = *input.ExtraOAuthScopes
		}

		response, err = vcsService.CreateProjectVCSProvider(ctx, &vcs.CreateProjectVCSProviderInput{
			Name:                input.Name,
			URL:                 input.URL,
			Description:         input.Description,
			ProjectID:           projectID,
			OAuthClientID:       input.OAuthClientID,
			OAuthClientSecret:   input.OAuthClientSecret,
			ExtraOAuthScopes:    extraOauthScopes,
			PersonalAccessToken: input.PersonalAccessToken,
			Type:                input.Type,
			AuthType:            input.AuthType,
		})
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unknown vcs provider scope %s", input.Scope, errors.WithErrorCode(errors.EInvalid))
	}

	payload := VCSProviderMutationPayload{
		ClientMutationID:      input.ClientMutationID,
		VCSProvider:           response.VCSProvider,
		OAuthAuthorizationURL: response.OAuthAuthorizationURL,
		Problems:              []Problem{},
	}
	return &VCSProviderMutationPayloadResolver{VCSProviderMutationPayload: payload}, nil
}

func updateVCSProviderMutation(ctx context.Context, input *UpdateVCSProviderInput) (*VCSProviderMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	vcsService := getVCSService(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &vcs.UpdateVCSProviderInput{
		ID:                  id,
		Description:         input.Description,
		OAuthClientID:       input.OAuthClientID,
		OAuthClientSecret:   input.OAuthClientSecret,
		PersonalAccessToken: input.PersonalAccessToken,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.Version = &v
	}

	if input.ExtraOAuthScopes != nil {
		toUpdate.ExtraOAuthScopes = *input.ExtraOAuthScopes
	}

	updatedProvider, err := vcsService.UpdateVCSProvider(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := VCSProviderMutationPayload{ClientMutationID: input.ClientMutationID, VCSProvider: updatedProvider, Problems: []Problem{}}
	return &VCSProviderMutationPayloadResolver{VCSProviderMutationPayload: payload}, nil
}

func deleteVCSProviderMutation(ctx context.Context, input *DeleteVCSProviderInput) (*VCSProviderMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	vcsService := getVCSService(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	vcsProvider, err := vcsService.GetVCSProviderByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &vcs.DeleteVCSProviderInput{
		ID: id,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := vcsService.DeleteVCSProvider(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := VCSProviderMutationPayload{ClientMutationID: input.ClientMutationID, VCSProvider: vcsProvider, Problems: []Problem{}}
	return &VCSProviderMutationPayloadResolver{VCSProviderMutationPayload: payload}, nil
}

/* VCSProvider loader */

const vcsProviderLoaderKey = "vcsProvider"

// RegisterVCSProviderLoader registers a VCS provider loader function
func RegisterVCSProviderLoader(collection *loader.Collection) {
	collection.Register(vcsProviderLoaderKey, vcsProviderBatchFunc)
}

func loadVCSProvider(ctx context.Context, id string) (*models.VCSProvider, error) {
	ldr, err := loader.Extract(ctx, vcsProviderLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	vp, ok := data.(models.VCSProvider)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &vp, nil
}

func vcsProviderBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	service := getVCSService(ctx)

	providers, err := service.GetVCSProvidersByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range providers {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
