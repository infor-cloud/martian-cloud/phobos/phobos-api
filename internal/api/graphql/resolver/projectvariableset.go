package resolver

import (
	"context"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/projectvariable"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ProjectVariableSetConnectionQueryArgs are used to query a project variable set connection
type ProjectVariableSetConnectionQueryArgs struct {
	ConnectionQueryArgs
	RevisionSearch *string
}

// ProjectVariableSetEdgeResolver resolves project variable set edges
type ProjectVariableSetEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ProjectVariableSetEdgeResolver) Cursor() (string, error) {
	variableSet, ok := r.edge.Node.(*models.ProjectVariableSet)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(variableSet)
	return *cursor, err
}

// Node returns a project variable set  node
func (r *ProjectVariableSetEdgeResolver) Node() (*ProjectVariableSetResolver, error) {
	variableSet, ok := r.edge.Node.(*models.ProjectVariableSet)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ProjectVariableSetResolver{variableSet: variableSet}, nil
}

// ProjectVariableSetConnectionResolver resolves a project variable set  connection
type ProjectVariableSetConnectionResolver struct {
	connection Connection
}

// NewProjectVariableSetConnectionResolver creates a new ProjectVariableSetConnectionResolver
func NewProjectVariableSetConnectionResolver(ctx context.Context, input *projectvariable.GetProjectVariableSetsInput) (*ProjectVariableSetConnectionResolver, error) {
	service := getProjectVariableService(ctx)

	result, err := service.GetProjectVariableSets(ctx, input)
	if err != nil {
		return nil, err
	}

	variableSets := result.ProjectVariableSets

	// Create edges
	edges := make([]Edge, len(variableSets))
	for i, variableSet := range variableSets {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: variableSet}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(variableSets) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(variableSets[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(variableSets[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ProjectVariableSetConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ProjectVariableSetConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ProjectVariableSetConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ProjectVariableSetConnectionResolver) Edges() *[]*ProjectVariableSetEdgeResolver {
	resolvers := make([]*ProjectVariableSetEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ProjectVariableSetEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ProjectVariableSetResolver resolves a project variable set  resource
type ProjectVariableSetResolver struct {
	variableSet *models.ProjectVariableSet
}

// ID resolver
func (r *ProjectVariableSetResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ProjectVariableSetType, r.variableSet.Metadata.ID))
}

// Metadata resolver
func (r *ProjectVariableSetResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.variableSet.Metadata}
}

// Revision resolver
func (r *ProjectVariableSetResolver) Revision() string {
	return r.variableSet.Revision
}

// Latest resolver
func (r *ProjectVariableSetResolver) Latest() bool {
	return r.variableSet.Latest
}

// Variables resolver
func (r *ProjectVariableSetResolver) Variables(
	ctx context.Context,
	args *ProjectVariableConnectionQueryArgs,
) (*ProjectVariableConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	defaultSort := db.ProjectVariableSortableFieldKeyAsc
	input := &projectvariable.GetProjectVariablesInput{
		Sort:                 &defaultSort,
		PaginationOptions:    &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:            r.variableSet.ProjectID,
		ProjectVariableSetID: &r.variableSet.Metadata.ID,
		PipelineType:         args.PipelineType,
		Search:               args.Search,
	}

	if args.EnvironmentScopes != nil {
		input.EnvironmentScopes = *args.EnvironmentScopes
	}

	if args.Sort != nil {
		sort := db.ProjectVariableSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewProjectVariableConnectionResolver(ctx, input)
}

// Project resolver
func (r *ProjectVariableSetResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	project, err := loadProject(ctx, r.variableSet.ProjectID)
	if err != nil {
		return nil, err
	}
	return &ProjectResolver{project: project}, nil
}

/* ProjectVariableSet Mutations */

// ProjectVariableSetMutationPayload is the response payload for a project variable set  mutation
type ProjectVariableSetMutationPayload struct {
	ClientMutationID   *string
	ProjectVariableSet *models.ProjectVariableSet
	Problems           []Problem
}

// ProjectVariableSetMutationPayloadResolver resolves a ProjectVariableSetMutationPayload
type ProjectVariableSetMutationPayloadResolver struct {
	ProjectVariableSetMutationPayload
}

// ProjectVariableSet field resolver
func (r *ProjectVariableSetMutationPayloadResolver) ProjectVariableSet() *ProjectVariableSetResolver {
	if r.ProjectVariableSetMutationPayload.ProjectVariableSet == nil {
		return nil
	}

	return &ProjectVariableSetResolver{variableSet: r.ProjectVariableSetMutationPayload.ProjectVariableSet}
}

// CreateProjectVariableInput contains the input for creating a new project variable
type CreateProjectVariableInput struct {
	EnvironmentScope *string
	Key              string
	Value            string
	PipelineType     models.PipelineType
}

// ReplaceProjectVariableInput contains the input for replacing a project variable
type ReplaceProjectVariableInput struct {
	EnvironmentScope *string
	ID               string
	Key              string
	Value            string
	PipelineType     models.PipelineType
}

// SetProjectVariablesInput contains the input for setting project variables
type SetProjectVariablesInput struct {
	ClientMutationID *string
	ProjectID        string
	Variables        []*CreateProjectVariableInput
}

// PatchProjectVariablesInput contains the input for patching project variables
type PatchProjectVariablesInput struct {
	ClientMutationID  *string
	CreateVariables   *[]*CreateProjectVariableInput
	ReplaceVariables  *[]*ReplaceProjectVariableInput
	RemoveVariableIDs *[]string
	ProjectID         string
}

func handleProjectVariableSetMutationProblem(e error, clientMutationID *string) (*ProjectVariableSetMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := ProjectVariableSetMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ProjectVariableSetMutationPayloadResolver{ProjectVariableSetMutationPayload: payload}, nil
}

func setProjectVariablesMutation(ctx context.Context,
	input *SetProjectVariablesInput) (*ProjectVariableSetMutationPayloadResolver, error) {
	projectID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ProjectID)
	if err != nil {
		return nil, err
	}

	service := getProjectVariableService(ctx)

	variables := make([]*models.ProjectVariable, len(input.Variables))
	for i, variable := range input.Variables {
		variables[i] = &models.ProjectVariable{
			Key:          variable.Key,
			Value:        variable.Value,
			PipelineType: variable.PipelineType,
		}

		if variable.EnvironmentScope != nil {
			variables[i].EnvironmentScope = *variable.EnvironmentScope
		}
	}

	createdProjectVariableSet, err := service.SetProjectVariables(ctx, &projectvariable.SetProjectVariablesInput{
		ProjectID: projectID,
		Variables: variables,
	})
	if err != nil {
		return nil, err
	}

	payload := ProjectVariableSetMutationPayload{
		ClientMutationID:   input.ClientMutationID,
		ProjectVariableSet: createdProjectVariableSet, Problems: []Problem{},
	}
	return &ProjectVariableSetMutationPayloadResolver{ProjectVariableSetMutationPayload: payload}, nil
}

func patchProjectVariablesMutation(ctx context.Context,
	input *PatchProjectVariablesInput) (*ProjectVariableSetMutationPayloadResolver, error) {
	projectID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ProjectID)
	if err != nil {
		return nil, err
	}

	service := getProjectVariableService(ctx)

	var createVariables []*models.ProjectVariable
	var replaceVariables []*models.ProjectVariable
	var removeVariables []string

	if input.CreateVariables != nil {
		createVariables = make([]*models.ProjectVariable, len(*input.CreateVariables))
		for i, variable := range *input.CreateVariables {
			createVariables[i] = &models.ProjectVariable{
				Key:          variable.Key,
				Value:        variable.Value,
				PipelineType: variable.PipelineType,
			}
			if variable.EnvironmentScope != nil {
				createVariables[i].EnvironmentScope = *variable.EnvironmentScope
			}
		}
	}

	if input.ReplaceVariables != nil {
		replaceVariables = make([]*models.ProjectVariable, len(*input.ReplaceVariables))
		for i, variable := range *input.ReplaceVariables {
			id, rErr := getURNResolver(ctx).ResolveResourceID(ctx, variable.ID)
			if rErr != nil {
				return nil, rErr
			}
			replaceVariables[i] = &models.ProjectVariable{
				Metadata: models.ResourceMetadata{
					ID: id,
				},
				Key:          variable.Key,
				Value:        variable.Value,
				PipelineType: variable.PipelineType,
			}
			if variable.EnvironmentScope != nil {
				replaceVariables[i].EnvironmentScope = *variable.EnvironmentScope
			}
		}
	}

	if input.RemoveVariableIDs != nil {
		for _, gid := range *input.RemoveVariableIDs {
			id, rErr := getURNResolver(ctx).ResolveResourceID(ctx, gid)
			if rErr != nil {
				return nil, rErr
			}
			removeVariables = append(removeVariables, id)
		}
	}

	createdProjectVariableSet, err := service.PatchProjectVariables(ctx, &projectvariable.PatchProjectVariablesInput{
		ProjectID:         projectID,
		CreateVariables:   createVariables,
		ReplaceVariables:  replaceVariables,
		RemoveVariableIDs: removeVariables,
	})
	if err != nil {
		return nil, err
	}

	payload := ProjectVariableSetMutationPayload{
		ClientMutationID:   input.ClientMutationID,
		ProjectVariableSet: createdProjectVariableSet, Problems: []Problem{},
	}
	return &ProjectVariableSetMutationPayloadResolver{ProjectVariableSetMutationPayload: payload}, nil
}

/* ProjectVariableSet loader */

const variableSetLoaderKey = "variableSet"

// RegisterProjectVariableSetLoader registers a project variable set  loader function
func RegisterProjectVariableSetLoader(collection *loader.Collection) {
	collection.Register(variableSetLoaderKey, variableSetBatchFunc)
}

func loadProjectVariableSet(ctx context.Context, id string) (*models.ProjectVariableSet, error) {
	ldr, err := loader.Extract(ctx, variableSetLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	variableSet, ok := data.(*models.ProjectVariableSet)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return variableSet, nil
}

func variableSetBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	variableSets, err := getProjectVariableService(ctx).GetProjectVariableSetsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range variableSets {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
