package resolver

import (
	"context"
	"strconv"

	"github.com/graph-gophers/dataloader"
	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/comment"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// CommentResolver resolves a comment resource
type CommentResolver struct {
	comment *models.Comment
}

// ID resolver
func (r *CommentResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.CommentType, r.comment.Metadata.ID))
}

// Metadata resolver
func (r *CommentResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.comment.Metadata}
}

// Creator resolver
func (r *CommentResolver) Creator(ctx context.Context) (*MemberResolver, error) {
	memberResolver, err := makeMemberResolver(ctx, r.comment.UserID, nil, r.comment.ServiceAccountID)
	if errors.ErrorCode(err) == errors.ENotFound {
		// If the user or service account is not found, return nil
		return nil, nil
	}

	return memberResolver, err
}

// Thread resolver
func (r *CommentResolver) Thread(ctx context.Context) (*ThreadResolver, error) {
	thread, err := loadThread(ctx, r.comment.ThreadID)
	if err != nil {
		return nil, err
	}

	return &ThreadResolver{thread: thread}, nil
}

// Text resolver
func (r *CommentResolver) Text() string {
	return r.comment.Text
}

/* Comment Mutations */

// CommentMutationPayload is the response payload for a comment mutation
type CommentMutationPayload struct {
	ClientMutationID *string
	Comment          *models.Comment
	Problems         []Problem
}

// CommentMutationPayloadResolver resolves a CommentMutationPayload
type CommentMutationPayloadResolver struct {
	CommentMutationPayload
}

// Comment field resolver
func (r *CommentMutationPayloadResolver) Comment() *CommentResolver {
	if r.CommentMutationPayload.Comment == nil {
		return nil
	}

	return &CommentResolver{comment: r.CommentMutationPayload.Comment}
}

// CreatePipelineCommentInput contains the input for creating a new pipeline comment
type CreatePipelineCommentInput struct {
	ClientMutationID *string
	ThreadID         *string
	PipelineID       *string
	Text             string
}

// CreateReleaseCommentInput contains the input for creating a new release comment
type CreateReleaseCommentInput struct {
	ClientMutationID *string
	ThreadID         *string
	ReleaseID        *string
	Text             string
}

// UpdateCommentInput contains the input for updating a comment
type UpdateCommentInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	Text             string
	ID               string
}

// DeleteCommentInput contains the input for deleting a comment
type DeleteCommentInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handleCommentMutationProblem(e error, clientMutationID *string) (*CommentMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := CommentMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &CommentMutationPayloadResolver{CommentMutationPayload: payload}, nil
}

func createPipelineCommentMutation(
	ctx context.Context,
	input *CreatePipelineCommentInput,
) (*CommentMutationPayloadResolver, error) {
	var err error
	urnResolver := getURNResolver(ctx)

	commentCreateOptions := &comment.CreateCommentInput{
		Text: input.Text,
	}

	if input.PipelineID != nil {
		pipelineID, rErr := urnResolver.ResolveResourceID(ctx, *input.PipelineID)
		if rErr != nil {
			return nil, rErr
		}
		commentCreateOptions.PipelineID = &pipelineID
	}

	if input.ThreadID != nil {
		threadID, rErr := urnResolver.ResolveResourceID(ctx, *input.ThreadID)
		if rErr != nil {
			return nil, rErr
		}
		commentCreateOptions.ThreadID = &threadID
	}

	createdComment, err := getCommentService(ctx).CreateComment(ctx, commentCreateOptions)
	if err != nil {
		return nil, err
	}

	payload := CommentMutationPayload{
		ClientMutationID: input.ClientMutationID,
		Comment:          createdComment,
		Problems:         []Problem{},
	}
	return &CommentMutationPayloadResolver{CommentMutationPayload: payload}, nil
}

func createReleaseCommentMutation(
	ctx context.Context,
	input *CreateReleaseCommentInput,
) (*CommentMutationPayloadResolver, error) {
	var err error
	urnResolver := getURNResolver(ctx)

	commentCreateOptions := &comment.CreateCommentInput{
		Text: input.Text,
	}

	if input.ReleaseID != nil {
		releaseID, rErr := urnResolver.ResolveResourceID(ctx, *input.ReleaseID)
		if rErr != nil {
			return nil, rErr
		}
		commentCreateOptions.ReleaseID = &releaseID
	}

	if input.ThreadID != nil {
		threadID, rErr := urnResolver.ResolveResourceID(ctx, *input.ThreadID)
		if rErr != nil {
			return nil, rErr
		}
		commentCreateOptions.ThreadID = &threadID
	}

	createdComment, err := getCommentService(ctx).CreateComment(ctx, commentCreateOptions)
	if err != nil {
		return nil, err
	}

	payload := CommentMutationPayload{
		ClientMutationID: input.ClientMutationID,
		Comment:          createdComment,
		Problems:         []Problem{},
	}
	return &CommentMutationPayloadResolver{CommentMutationPayload: payload}, nil
}

func updateCommentMutation(
	ctx context.Context,
	input *UpdateCommentInput,
) (*CommentMutationPayloadResolver, error) {
	commentService := getCommentService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &comment.UpdateCommentInput{
		ID:   id,
		Text: input.Text,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.Version = &v
	}

	updatedComment, err := commentService.UpdateComment(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := CommentMutationPayload{
		ClientMutationID: input.ClientMutationID,
		Comment:          updatedComment,
		Problems:         []Problem{}}
	return &CommentMutationPayloadResolver{CommentMutationPayload: payload}, nil
}

func deleteCommentMutation(ctx context.Context, input *DeleteCommentInput) (*CommentMutationPayloadResolver, error) {
	commentService := getCommentService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toDelete := &comment.DeleteCommentInput{
		ID: id,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := commentService.DeleteComment(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := CommentMutationPayload{
		ClientMutationID: input.ClientMutationID,
		// Comment field is not added here since the parent resource (thread) will be deleted when
		// the last comment for a thread is deleted and the Thread() resolver will fail.
		Problems: []Problem{},
	}
	return &CommentMutationPayloadResolver{CommentMutationPayload: payload}, nil
}

/* Comment Subscriptions */

// CommentEventResolver resolves a comment event
type CommentEventResolver struct {
	event *comment.Event
}

// Action resolves the event action
func (r *CommentEventResolver) Action() string {
	return r.event.Action
}

// Comment resolves the comment
func (r *CommentEventResolver) Comment() *CommentResolver {
	return &CommentResolver{comment: r.event.Comment}
}

// CommentEventsSubscriptionInput is the input for subscribing to the events for a single comment
type CommentEventsSubscriptionInput struct {
	PipelineID *string
	ReleaseID  *string
}

func (r RootResolver) commentEventsSubscription(ctx context.Context, input *CommentEventsSubscriptionInput) (<-chan *CommentEventResolver, error) {
	commentService := getCommentService(ctx)
	urnResolver := getURNResolver(ctx)

	var pipelineID, releaseID *string
	if input.PipelineID != nil {
		p, err := urnResolver.ResolveResourceID(ctx, *input.PipelineID)
		if err != nil {
			return nil, err
		}
		pipelineID = &p
	}

	if input.ReleaseID != nil {
		r, err := urnResolver.ResolveResourceID(ctx, *input.ReleaseID)
		if err != nil {
			return nil, err
		}
		releaseID = &r
	}

	events, err := commentService.SubscribeToComments(ctx, &comment.SubscribeToCommentsInput{
		PipelineID: pipelineID,
		ReleaseID:  releaseID,
	})
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *CommentEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &CommentEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

/* Comment loader */

const commentLoaderKey = "comment"

// RegisterCommentLoader registers a comment loader function
func RegisterCommentLoader(collection *loader.Collection) {
	collection.Register(commentLoaderKey, commentBatchFunc)
}

func loadComment(ctx context.Context, id string) (*models.Comment, error) {
	ldr, err := loader.Extract(ctx, commentLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	comment, ok := data.(models.Comment)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &comment, nil
}

func commentBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	comments, err := getCommentService(ctx).GetCommentsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range comments {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
