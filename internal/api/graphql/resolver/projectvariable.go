package resolver

import (
	"context"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/projectvariable"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ProjectVariableConnectionQueryArgs are used to query a project variable connection
type ProjectVariableConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search            *string
	PipelineType      *models.PipelineType
	EnvironmentScopes *[]string
}

// ProjectVariableEdgeResolver resolves project variable edges
type ProjectVariableEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ProjectVariableEdgeResolver) Cursor() (string, error) {
	projectVariable, ok := r.edge.Node.(*models.ProjectVariable)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(projectVariable)
	return *cursor, err
}

// Node returns a project variable  node
func (r *ProjectVariableEdgeResolver) Node() (*ProjectVariableResolver, error) {
	projectVariable, ok := r.edge.Node.(*models.ProjectVariable)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ProjectVariableResolver{projectVariable: projectVariable}, nil
}

// ProjectVariableConnectionResolver resolves a project variable  connection
type ProjectVariableConnectionResolver struct {
	connection Connection
}

// NewProjectVariableConnectionResolver creates a new ProjectVariableConnectionResolver
func NewProjectVariableConnectionResolver(ctx context.Context, input *projectvariable.GetProjectVariablesInput) (*ProjectVariableConnectionResolver, error) {
	projectVariableService := getProjectVariableService(ctx)

	result, err := projectVariableService.GetProjectVariables(ctx, input)
	if err != nil {
		return nil, err
	}

	projectVariables := result.ProjectVariables

	// Create edges
	edges := make([]Edge, len(projectVariables))
	for i, projectVariable := range projectVariables {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: projectVariable}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(projectVariables) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(projectVariables[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(projectVariables[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ProjectVariableConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ProjectVariableConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ProjectVariableConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ProjectVariableConnectionResolver) Edges() *[]*ProjectVariableEdgeResolver {
	resolvers := make([]*ProjectVariableEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ProjectVariableEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ProjectVariableResolver resolves a project variable  resource
type ProjectVariableResolver struct {
	projectVariable *models.ProjectVariable
}

// ID resolver
func (r *ProjectVariableResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ProjectVariableType, r.projectVariable.Metadata.ID))
}

// Metadata resolver
func (r *ProjectVariableResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.projectVariable.Metadata}
}

// CreatedBy resolver
func (r *ProjectVariableResolver) CreatedBy() string {
	return r.projectVariable.CreatedBy
}

// Key resolver
func (r *ProjectVariableResolver) Key() string {
	return r.projectVariable.Key
}

// Value resolver
func (r *ProjectVariableResolver) Value() string {
	return r.projectVariable.Value
}

// PipelineType resolver
func (r *ProjectVariableResolver) PipelineType() models.PipelineType {
	return r.projectVariable.PipelineType
}

// EnvironmentScope resolver
func (r *ProjectVariableResolver) EnvironmentScope() string {
	return r.projectVariable.EnvironmentScope
}

// VariableSets resolver
func (r *ProjectVariableResolver) VariableSets(
	ctx context.Context,
	args *ProjectVariableSetConnectionQueryArgs,
) (*ProjectVariableSetConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &projectvariable.GetProjectVariableSetsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         r.projectVariable.ProjectID,
		VariableID:        &r.projectVariable.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.ProjectVariableSetSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewProjectVariableSetConnectionResolver(ctx, input)
}

// PreviousVersions resolver
func (r *ProjectVariableResolver) PreviousVersions(
	ctx context.Context,
	args *ProjectVariableConnectionQueryArgs,
) (*ProjectVariableConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	defaultSort := db.ProjectVariableSortableFieldCreatedAtDesc
	input := &projectvariable.GetProjectVariablesInput{
		Sort:                  &defaultSort,
		PaginationOptions:     &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:             r.projectVariable.ProjectID,
		PipelineType:          &r.projectVariable.PipelineType,
		VariableKey:           &r.projectVariable.Key,
		EnvironmentScopes:     []string{r.projectVariable.EnvironmentScope},
		CreatedAtTimeRangeEnd: r.projectVariable.Metadata.CreationTimestamp,
		ExcludeVariableIDs:    []string{r.projectVariable.Metadata.ID},
	}

	if args.Sort != nil {
		sort := db.ProjectVariableSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewProjectVariableConnectionResolver(ctx, input)
}

// Project resolver
func (r *ProjectVariableResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	project, err := loadProject(ctx, r.projectVariable.ProjectID)
	if err != nil {
		return nil, err
	}
	return &ProjectResolver{project: project}, nil
}

/* ProjectVariable loader */

const projectVariableLoaderKey = "projectVariable"

// RegisterProjectVariableLoader registers a project variable  loader function
func RegisterProjectVariableLoader(collection *loader.Collection) {
	collection.Register(projectVariableLoaderKey, projectVariableBatchFunc)
}

func loadProjectVariable(ctx context.Context, id string) (*models.ProjectVariable, error) {
	ldr, err := loader.Extract(ctx, projectVariableLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	projectVariable, ok := data.(*models.ProjectVariable)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return projectVariable, nil
}

func projectVariableBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	projectVariables, err := getProjectVariableService(ctx).GetProjectVariablesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range projectVariables {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
