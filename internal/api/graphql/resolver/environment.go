package resolver

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
)

/* Environment Query Resolvers */

// EnvironmentEdgeResolver resolves environment edges
type EnvironmentEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *EnvironmentEdgeResolver) Cursor() (string, error) {
	environment, ok := r.edge.Node.(models.Environment)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&environment)
	return *cursor, err
}

// Node returns an environment node
func (r *EnvironmentEdgeResolver) Node() (*EnvironmentResolver, error) {
	environment, ok := r.edge.Node.(models.Environment)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &EnvironmentResolver{environment: &environment}, nil
}

// EnvironmentConnectionResolver resolves an environment connection
type EnvironmentConnectionResolver struct {
	connection Connection
}

// NewEnvironmentConnectionResolver creates a new EnvironmentConnectionResolver
func NewEnvironmentConnectionResolver(ctx context.Context,
	input *environment.GetEnvironmentsInput) (*EnvironmentConnectionResolver, error) {
	service := getEnvironmentService(ctx)

	result, err := service.GetEnvironments(ctx, input)
	if err != nil {
		return nil, err
	}

	environments := result.Environments

	// Create edges
	edges := make([]Edge, len(environments))
	for i, environment := range environments {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: environment}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(environments) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&environments[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&environments[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &EnvironmentConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *EnvironmentConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *EnvironmentConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *EnvironmentConnectionResolver) Edges() *[]*EnvironmentEdgeResolver {
	resolvers := make([]*EnvironmentEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &EnvironmentEdgeResolver{edge: edge}
	}
	return &resolvers
}

// EnvironmentResolver resolves an environment resource
type EnvironmentResolver struct {
	environment *models.Environment
}

// ID resolver
func (r *EnvironmentResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.EnvironmentType, r.environment.Metadata.ID))
}

// Name resolver
func (r *EnvironmentResolver) Name() string {
	return r.environment.Name
}

// Description resolver
func (r *EnvironmentResolver) Description() string {
	return r.environment.Description
}

// Metadata resolver
func (r *EnvironmentResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.environment.Metadata}
}

// CreatedBy resolver
func (r *EnvironmentResolver) CreatedBy() string {
	return r.environment.CreatedBy
}

// Project resolver
func (r *EnvironmentResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	project, err := loadProject(ctx, r.environment.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// Pipelines resolver
func (r *EnvironmentResolver) Pipelines(ctx context.Context, args *PipelineConnectionQueryArgs) (*PipelineConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &pipeline.GetPipelinesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         r.environment.ProjectID,
		EnvironmentName:   &r.environment.Name,
		Started:           args.Started,
		Completed:         args.Completed,
		Superseded:        args.Superseded,
	}

	if args.PipelineTypes != nil {
		input.PipelineTypes = *args.PipelineTypes
	}

	if args.Sort != nil {
		sort := db.PipelineSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewPipelineConnectionResolver(ctx, input)
}

/* Environment Mutation Resolvers */

// EnvironmentMutationPayload is the response payload for an environment mutation
type EnvironmentMutationPayload struct {
	ClientMutationID *string
	Environment      *models.Environment
	Problems         []Problem
}

// EnvironmentMutationPayloadResolver resolves an environmentMutationPayload
type EnvironmentMutationPayloadResolver struct {
	EnvironmentMutationPayload
}

// Environment field resolver
func (r *EnvironmentMutationPayloadResolver) Environment() *EnvironmentResolver {
	if r.EnvironmentMutationPayload.Environment == nil {
		return nil
	}

	return &EnvironmentResolver{environment: r.EnvironmentMutationPayload.Environment}
}

// CreateEnvironmentInput contains the input for creating a new environment
type CreateEnvironmentInput struct {
	ClientMutationID *string
	ProjectID        string
	Name             string
	Description      string
}

// UpdateEnvironmentInput contains the input for updating an environment
type UpdateEnvironmentInput struct {
	ClientMutationID *string
	Description      *string
	Metadata         *MetadataInput
	ID               string
}

// DeleteEnvironmentInput contains the input for deleting an environment
type DeleteEnvironmentInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handleEnvironmentMutationProblem(e error, clientMutationID *string) (*EnvironmentMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}

	payload := EnvironmentMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &EnvironmentMutationPayloadResolver{EnvironmentMutationPayload: payload}, nil
}

func createEnvironmentMutation(ctx context.Context, input *CreateEnvironmentInput) (*EnvironmentMutationPayloadResolver, error) {
	projectID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ProjectID)
	if err != nil {
		return nil, err
	}

	createdEnvironment, err := getEnvironmentService(ctx).CreateEnvironment(ctx, &environment.CreateEnvironmentInput{
		Name:        input.Name,
		Description: input.Description,
		ProjectID:   projectID,
	})
	if err != nil {
		return nil, err
	}

	payload := EnvironmentMutationPayload{ClientMutationID: input.ClientMutationID, Environment: createdEnvironment, Problems: []Problem{}}
	return &EnvironmentMutationPayloadResolver{EnvironmentMutationPayload: payload}, nil
}

func updateEnvironmentMutation(ctx context.Context, input *UpdateEnvironmentInput) (*EnvironmentMutationPayloadResolver, error) {
	service := getEnvironmentService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &environment.UpdateEnvironmentInput{
		ID:          id,
		Description: input.Description,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.MetadataVersion = &v
	}

	environment, err := service.UpdateEnvironment(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := EnvironmentMutationPayload{ClientMutationID: input.ClientMutationID, Environment: environment, Problems: []Problem{}}
	return &EnvironmentMutationPayloadResolver{EnvironmentMutationPayload: payload}, nil
}

func deleteEnvironmentMutation(ctx context.Context, input *DeleteEnvironmentInput) (*EnvironmentMutationPayloadResolver, error) {
	service := getEnvironmentService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	gotEnvironment, err := service.GetEnvironmentByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &environment.DeleteEnvironmentInput{
		ID: gotEnvironment.Metadata.ID,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.MetadataVersion = &v
	}

	if err := service.DeleteEnvironment(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := EnvironmentMutationPayload{ClientMutationID: input.ClientMutationID, Environment: gotEnvironment, Problems: []Problem{}}
	return &EnvironmentMutationPayloadResolver{EnvironmentMutationPayload: payload}, nil
}

/* Environment loader */

const environmentLoaderKey = "environment"

// RegisterEnvironmentLoader registers an environment loader function
func RegisterEnvironmentLoader(collection *loader.Collection) {
	collection.Register(environmentLoaderKey, environmentBatchFunc)
}

func loadEnvironment(ctx context.Context, id string) (*models.Environment, error) {
	ldr, err := loader.Extract(ctx, environmentLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	environment, ok := data.(models.Environment)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &environment, nil
}

func environmentBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	environmentService := getEnvironmentService(ctx)

	environments, err := environmentService.GetEnvironmentsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range environments {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
