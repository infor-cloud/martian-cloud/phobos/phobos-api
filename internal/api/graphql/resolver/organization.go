package resolver

import (
	"context"
	"strconv"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/approvalrule"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/organization"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/project"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// OrganizationConnectionQueryArgs are used to query an organization connection
type OrganizationConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search *string
}

// OrganizationQueryArgs are used to query a single organization
type OrganizationQueryArgs struct {
	Name string
}

// OrganizationEdgeResolver resolves organization edges
type OrganizationEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *OrganizationEdgeResolver) Cursor() (string, error) {
	org, ok := r.edge.Node.(models.Organization)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&org)
	return *cursor, err
}

// Node returns a organization node
func (r *OrganizationEdgeResolver) Node() (*OrganizationResolver, error) {
	org, ok := r.edge.Node.(models.Organization)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &OrganizationResolver{organization: &org}, nil
}

// OrganizationConnectionResolver resolves an organization connection
type OrganizationConnectionResolver struct {
	connection Connection
}

// NewOrganizationConnectionResolver creates a new OrganizationConnectionResolver
func NewOrganizationConnectionResolver(ctx context.Context, input *organization.GetOrganizationsInput) (*OrganizationConnectionResolver, error) {
	organizationService := getOrganizationService(ctx)

	result, err := organizationService.GetOrganizations(ctx, input)
	if err != nil {
		return nil, err
	}

	organizations := result.Organizations

	// Create edges
	edges := make([]Edge, len(organizations))
	for i, organization := range organizations {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: organization}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(organizations) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&organizations[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&organizations[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &OrganizationConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *OrganizationConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *OrganizationConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *OrganizationConnectionResolver) Edges() *[]*OrganizationEdgeResolver {
	resolvers := make([]*OrganizationEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &OrganizationEdgeResolver{edge: edge}
	}
	return &resolvers
}

// OrganizationResolver resolves an organization resource
type OrganizationResolver struct {
	organization *models.Organization
}

// ID resolver
func (r *OrganizationResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.OrganizationType, r.organization.Metadata.ID))
}

// Name resolver
func (r *OrganizationResolver) Name() string {
	return r.organization.Name
}

// Description resolver
func (r *OrganizationResolver) Description() string {
	return r.organization.Description
}

// Metadata resolver
func (r *OrganizationResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.organization.Metadata}
}

// CreatedBy resolver
func (r *OrganizationResolver) CreatedBy() string {
	return r.organization.CreatedBy
}

// Memberships resolver
func (r *OrganizationResolver) Memberships(ctx context.Context, args *ConnectionQueryArgs) (*MembershipConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &membership.GetMembershipsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:    &r.organization.Metadata.ID,
		MembershipScopes:  []models.ScopeType{models.OrganizationScope},
	}

	if args.Sort != nil {
		sort := db.MembershipSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewMembershipConnectionResolver(ctx, input)
}

// MetricStats resolver
func (r *OrganizationResolver) MetricStats(input *struct {
	TimeRangeStart graphql.Time
	TimeRangeEnd   *graphql.Time
}) *MetricsStatisticsResolver {
	response := &MetricsStatisticsResolver{timeRangeStart: input.TimeRangeStart.Time, orgID: &r.organization.Metadata.ID}
	if input.TimeRangeEnd != nil {
		response.timeRangeEnd = &input.TimeRangeEnd.Time
	}
	return response
}

// Projects resolver
func (r *OrganizationResolver) Projects(ctx context.Context, args *ProjectConnectionQueryArgs) (*ProjectConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := project.GetProjectsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Organization:      r.organization,
		Search:            args.Search,
	}

	if args.Sort != nil {
		sort := db.ProjectSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewProjectConnectionResolver(ctx, &input)
}

// Agents resolver
func (r *OrganizationResolver) Agents(ctx context.Context, args *ConnectionQueryArgs) (*AgentConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	agentType := models.OrganizationAgent
	input := agent.GetAgentsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:    &r.organization.Metadata.ID,
		AgentType:         &agentType,
	}

	if args.Sort != nil {
		sort := db.AgentSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewAgentConnectionResolver(ctx, &input)
}

// ServiceAccounts resolver
func (r *OrganizationResolver) ServiceAccounts(ctx context.Context, args *ServiceAccountConnectionQueryArgs) (*ServiceAccountConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := serviceaccount.GetServiceAccountsInput{
		PaginationOptions:    &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:       &r.organization.Metadata.ID,
		Search:               args.Search,
		ServiceAccountScopes: []models.ScopeType{models.OrganizationScope},
	}

	if args.Sort != nil {
		sort := db.ServiceAccountSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewServiceAccountConnectionResolver(ctx, &input)
}

// ApprovalRules resolver
func (r *OrganizationResolver) ApprovalRules(ctx context.Context, args *ConnectionQueryArgs) (*ApprovalRuleConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := approvalrule.GetApprovalRulesInput{
		PaginationOptions:  &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:     &r.organization.Metadata.ID,
		ApprovalRuleScopes: []models.ScopeType{models.OrganizationScope},
	}

	if args.Sort != nil {
		sort := db.ApprovalRuleSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewApprovalRuleConnectionResolver(ctx, &input)
}

// LifecycleTemplates resolver
func (r *OrganizationResolver) LifecycleTemplates(ctx context.Context,
	args *ConnectionQueryArgs) (*LifecycleTemplateConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := lifecycletemplate.GetLifecycleTemplatesInput{
		PaginationOptions:       &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:          &r.organization.Metadata.ID,
		LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
	}

	if args.Sort != nil {
		sort := db.LifecycleTemplateSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewLifecycleTemplateConnectionResolver(ctx, &input)
}

// ReleaseLifecycles resolver
func (r *OrganizationResolver) ReleaseLifecycles(ctx context.Context,
	args *ReleaseLifecycleConnectionQueryArgs) (*ReleaseLifecycleConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := releaselifecycle.GetReleaseLifecyclesInput{
		PaginationOptions:      &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:         &r.organization.Metadata.ID,
		ReleaseLifecycleScopes: []models.ScopeType{models.OrganizationScope},
		Search:                 args.Search,
	}

	if args.Sort != nil {
		sort := db.ReleaseLifecycleSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewReleaseLifecycleConnectionResolver(ctx, &input)
}

// ActivityEvents resolver
func (r *OrganizationResolver) ActivityEvents(
	ctx context.Context,
	args *ActivityEventConnectionQueryArgs,
) (*ActivityEventConnectionResolver, error) {
	input, err := getActivityEventsInputFromQueryArgs(ctx, args)
	if err != nil {
		return nil, err
	}

	// Filter to this organization
	input.OrganizationID = &r.organization.Metadata.ID

	return NewActivityEventConnectionResolver(ctx, input)
}

// EnvironmentNames resolver
func (r *OrganizationResolver) EnvironmentNames(ctx context.Context) ([]string, error) {
	environments, err := getEnvironmentService(ctx).GetEnvironments(ctx, &environment.GetEnvironmentsInput{
		OrganizationID: &r.organization.Metadata.ID,
	})
	if err != nil {
		return nil, err
	}

	// Deduplicate environment names
	environmentNames := []string{}
	seenEnvironments := map[string]struct{}{}
	for _, env := range environments.Environments {
		if _, ok := seenEnvironments[env.Name]; !ok {
			seenEnvironments[env.Name] = struct{}{}
			environmentNames = append(environmentNames, env.Name)
		}
	}

	return environmentNames, nil
}

// VCSProviders resolver
func (r *OrganizationResolver) VCSProviders(ctx context.Context, args *VCSProviderConnectionQueryArgs) (*VCSProviderConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &vcs.GetVCSProvidersInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:    &r.organization.Metadata.ID,
		Search:            args.Search,
	}

	if args.Sort != nil {
		sort := db.VCSProviderSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewVCSProviderConnectionResolver(ctx, input)
}

// EnvironmentRules resolver
func (r *OrganizationResolver) EnvironmentRules(ctx context.Context, args *ConnectionQueryArgs) (*EnvironmentRuleConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &environment.GetEnvironmentRulesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		OrganizationID:    &r.organization.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.EnvironmentRuleSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewEnvironmentRuleConnectionResolver(ctx, input)
}

func organizationsQuery(ctx context.Context, args *OrganizationConnectionQueryArgs) (*OrganizationConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := organization.GetOrganizationsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Search:            args.Search,
	}

	if args.Sort != nil {
		sort := db.OrganizationSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewOrganizationConnectionResolver(ctx, &input)
}

/* Organization Mutations */

// OrganizationMutationPayload is the response payload for an organization mutation
type OrganizationMutationPayload struct {
	ClientMutationID *string
	Organization     *models.Organization
	Problems         []Problem
}

// OrganizationMutationPayloadResolver resolves a OrganizationMutationPayload
type OrganizationMutationPayloadResolver struct {
	OrganizationMutationPayload
}

// Organization field resolver
func (r *OrganizationMutationPayloadResolver) Organization() *OrganizationResolver {
	if r.OrganizationMutationPayload.Organization == nil {
		return nil
	}

	return &OrganizationResolver{organization: r.OrganizationMutationPayload.Organization}
}

// CreateOrganizationInput contains the input for creating a new organization
type CreateOrganizationInput struct {
	ClientMutationID *string
	Name             string
	Description      string
}

// UpdateOrganizationInput contains the input for updating a organization
type UpdateOrganizationInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	Description      *string
	ID               string
}

// DeleteOrganizationInput contains the input for deleting a organization
type DeleteOrganizationInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handleOrganizationMutationProblem(e error, clientMutationID *string) (*OrganizationMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := OrganizationMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &OrganizationMutationPayloadResolver{OrganizationMutationPayload: payload}, nil
}

func createOrganizationMutation(ctx context.Context, input *CreateOrganizationInput) (*OrganizationMutationPayloadResolver, error) {
	organizationService := getOrganizationService(ctx)

	organizationCreateOptions := &organization.CreateOrganizationInput{
		Name:        input.Name,
		Description: input.Description,
	}

	createdOrganization, err := organizationService.CreateOrganization(ctx, organizationCreateOptions)
	if err != nil {
		return nil, err
	}

	payload := OrganizationMutationPayload{ClientMutationID: input.ClientMutationID, Organization: createdOrganization, Problems: []Problem{}}
	return &OrganizationMutationPayloadResolver{OrganizationMutationPayload: payload}, nil
}

func updateOrganizationMutation(ctx context.Context, input *UpdateOrganizationInput) (*OrganizationMutationPayloadResolver, error) {
	organizationService := getOrganizationService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &organization.UpdateOrganizationInput{
		ID:          id,
		Description: input.Description,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.Version = &v
	}

	updatedOrg, err := organizationService.UpdateOrganization(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := OrganizationMutationPayload{ClientMutationID: input.ClientMutationID, Organization: updatedOrg, Problems: []Problem{}}
	return &OrganizationMutationPayloadResolver{OrganizationMutationPayload: payload}, nil
}

func deleteOrganizationMutation(ctx context.Context, input *DeleteOrganizationInput) (*OrganizationMutationPayloadResolver, error) {
	organizationService := getOrganizationService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	gotOrg, err := organizationService.GetOrganizationByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &organization.DeleteOrganizationInput{
		ID: gotOrg.Metadata.ID,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := organizationService.DeleteOrganization(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := OrganizationMutationPayload{ClientMutationID: input.ClientMutationID, Organization: gotOrg, Problems: []Problem{}}
	return &OrganizationMutationPayloadResolver{OrganizationMutationPayload: payload}, nil
}

/* Organization loader */

const organizationLoaderKey = "organization"

// RegisterOrganizationLoader registers a organization loader function
func RegisterOrganizationLoader(collection *loader.Collection) {
	collection.Register(organizationLoaderKey, organizationBatchFunc)
}

func loadOrganization(ctx context.Context, id string) (*models.Organization, error) {
	ldr, err := loader.Extract(ctx, organizationLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	organization, ok := data.(models.Organization)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &organization, nil
}

func organizationBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	organizations, err := getOrganizationService(ctx).GetOrganizationsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range organizations {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
