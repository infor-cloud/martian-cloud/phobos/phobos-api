package resolver

import (
	"context"
	"encoding/json"
	"strconv"

	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/todoitem"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

/* ToDoItem connection resolvers */

// ToDoItemConnectionQueryArgs represents the query arguments for the todo item connection resolver.
type ToDoItemConnectionQueryArgs struct {
	ConnectionQueryArgs
	OrganizationID *string
	ProjectID      *string
	Resolved       *bool
	TargetTypes    *[]models.ToDoItemTargetType
}

// ToDoItemEdgeResolver resolves todo item edges
type ToDoItemEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ToDoItemEdgeResolver) Cursor() (string, error) {
	todoItem, ok := r.edge.Node.(*models.ToDoItem)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(todoItem)
	return *cursor, err
}

// Node returns a todo item node
func (r *ToDoItemEdgeResolver) Node() (*ToDoItemResolver, error) {
	todoItem, ok := r.edge.Node.(*models.ToDoItem)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ToDoItemResolver{todoItem: todoItem}, nil
}

// ToDoItemConnectionResolver resolves an todo item connection
type ToDoItemConnectionResolver struct {
	connection Connection
}

// NewToDoItemConnectionResolver creates a new ToDoItemConnectionResolver
func NewToDoItemConnectionResolver(ctx context.Context, input *todoitem.GetToDoItemsInput) (*ToDoItemConnectionResolver, error) {
	result, err := getToDoItemService(ctx).GetToDoItems(ctx, input)
	if err != nil {
		return nil, err
	}

	todoItems := result.ToDoItems

	// Create edges
	edges := make([]Edge, len(todoItems))
	for i, todoItem := range todoItems {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: todoItem}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(todoItems) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(todoItems[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(todoItems[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ToDoItemConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ToDoItemConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ToDoItemConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ToDoItemConnectionResolver) Edges() *[]*ToDoItemEdgeResolver {
	resolvers := make([]*ToDoItemEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ToDoItemEdgeResolver{edge: edge}
	}
	return &resolvers
}

/* ToDoItem payload resolvers */

// ToDoItemPipelineTaskApprovalPayloadResolver resolves a ToDoItemPipelineTaskApprovalPayload
type ToDoItemPipelineTaskApprovalPayloadResolver struct {
	payload *models.ToDoItemPipelineTaskApprovalPayload
}

// TaskPath resolver
func (r *ToDoItemPipelineTaskApprovalPayloadResolver) TaskPath() string {
	return r.payload.TaskPath
}

// ToDoItemPayloadResolver resolves the payload union type
type ToDoItemPayloadResolver struct {
	result any
}

// ToToDoItemPipelineTaskApprovalPayload resolver
func (r *ToDoItemPayloadResolver) ToToDoItemPipelineTaskApprovalPayload() (*ToDoItemPipelineTaskApprovalPayloadResolver, bool) {
	res, ok := r.result.(*ToDoItemPipelineTaskApprovalPayloadResolver)
	return res, ok
}

/* ToDoItem resolver */

// ToDoItemResolver resolves a todo item.
type ToDoItemResolver struct {
	todoItem *models.ToDoItem
}

// ID resolver
func (r *ToDoItemResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ToDoItemType, r.todoItem.Metadata.ID))
}

// Metadata resolver
func (r *ToDoItemResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.todoItem.Metadata}
}

// Organization resolver
func (r *ToDoItemResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	organization, err := loadOrganization(ctx, r.todoItem.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Project resolver
func (r *ToDoItemResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.todoItem.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.todoItem.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// Target resolver
func (r *ToDoItemResolver) Target(ctx context.Context) (*NodeResolver, error) {
	switch r.todoItem.TargetType {
	case models.PipelineApprovalTarget,
		models.TaskApprovalTarget:
		pipeline, err := loadPipeline(ctx, r.todoItem.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &PipelineResolver{pipeline: pipeline}}, nil
	default:
		return nil, errors.New("invalid todo item target type %s", r.todoItem.TargetType)
	}
}

// TargetType resolver
func (r *ToDoItemResolver) TargetType() models.ToDoItemTargetType {
	return r.todoItem.TargetType
}

// Resolved resolver
func (r *ToDoItemResolver) Resolved(ctx context.Context) (bool, error) {
	caller := auth.GetCaller(ctx)
	if caller == nil {
		return false, errors.New("no caller found on context")
	}

	userCaller, ok := caller.(*auth.UserCaller)
	if !ok {
		return false, errors.New("unauthorized caller type")
	}

	return r.todoItem.IsResolved(userCaller.User.Metadata.ID), nil
}

// Resolvable resolver
func (r *ToDoItemResolver) Resolvable() bool {
	return r.todoItem.Resolvable
}

// Payload resolver
func (r *ToDoItemResolver) Payload() (*ToDoItemPayloadResolver, error) {
	if r.todoItem.Payload == nil {
		return nil, nil
	}

	switch r.todoItem.TargetType {
	case models.TaskApprovalTarget:
		var payload models.ToDoItemPipelineTaskApprovalPayload
		if err := json.Unmarshal(r.todoItem.Payload, &payload); err != nil {
			return nil, err
		}
		return &ToDoItemPayloadResolver{result: &ToDoItemPipelineTaskApprovalPayloadResolver{payload: &payload}}, nil
	default:
		return nil, errors.New("unknown todo item target type %s", r.todoItem.TargetType)
	}
}

// getToDoItemsInputFromQueryArgs is for the convenience of other modules in this package
// Other modules may need to modify the input before creating a resolver.
func getToDoItemsInputFromQueryArgs(ctx context.Context, args *ToDoItemConnectionQueryArgs) (*todoitem.GetToDoItemsInput, error) {
	urnResolver := getURNResolver(ctx)

	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := todoitem.GetToDoItemsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Resolved:          args.Resolved,
	}

	if args.OrganizationID != nil {
		organizationID, err := urnResolver.ResolveResourceID(ctx, *args.OrganizationID)
		if err != nil {
			return nil, err
		}

		input.OrganizationID = &organizationID
	}

	if args.ProjectID != nil {
		projectID, err := urnResolver.ResolveResourceID(ctx, *args.ProjectID)
		if err != nil {
			return nil, err
		}

		input.ProjectID = &projectID
	}

	if args.TargetTypes != nil {
		input.TargetTypes = *args.TargetTypes
	}

	if args.Sort != nil {
		sort := db.ToDoItemSortableField(*args.Sort)
		input.Sort = &sort
	}

	return &input, nil
}

/* ToDoItem mutation resolvers */

// ToDoItemMutationPayload is the payload for ToDoItem mutations.
type ToDoItemMutationPayload struct {
	ClientMutationID *string
	ToDoItem         *models.ToDoItem
	Problems         []Problem
}

// ToDoItemMutationPayloadResolver resolves a ToDoItemMutationPayload
type ToDoItemMutationPayloadResolver struct {
	ToDoItemMutationPayload
}

// ToDoItem field resolver
func (r *ToDoItemMutationPayloadResolver) ToDoItem() *ToDoItemResolver {
	if r.ToDoItemMutationPayload.ToDoItem == nil {
		return nil
	}

	return &ToDoItemResolver{todoItem: r.ToDoItemMutationPayload.ToDoItem}
}

// UpdateToDoItemInput contains the input for updating a ToDoItem.
type UpdateToDoItemInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	Resolved         *bool
	ID               string
}

func handleToDoItemMutationProblem(e error, clientMutationID *string) (*ToDoItemMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}

	payload := ToDoItemMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ToDoItemMutationPayloadResolver{ToDoItemMutationPayload: payload}, nil
}

func updateToDoItemMutation(ctx context.Context, input *UpdateToDoItemInput) (*ToDoItemMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &todoitem.UpdateToDoItemInput{
		ID:       id,
		Resolved: input.Resolved,
	}

	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.Version = &v
	}

	updatedItem, err := getToDoItemService(ctx).UpdateToDoItem(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := ToDoItemMutationPayload{ClientMutationID: input.ClientMutationID, ToDoItem: updatedItem, Problems: []Problem{}}
	return &ToDoItemMutationPayloadResolver{ToDoItemMutationPayload: payload}, nil
}

/* ToDoItem subscriptions */

// ToDoItemEventResolver resolves a todo item event
type ToDoItemEventResolver struct {
	event *todoitem.Event
}

// Action resolver
func (r *ToDoItemEventResolver) Action() string {
	return r.event.Action
}

// ToDoItem resolver
func (r *ToDoItemEventResolver) ToDoItem() *ToDoItemResolver {
	return &ToDoItemResolver{todoItem: r.event.ToDoItem}
}

// TotalUnresolved resolver
func (r *ToDoItemEventResolver) TotalUnresolved() int32 {
	return r.event.TotalUnresolved
}

func (r RootResolver) userToDoItemEventsSubscription(ctx context.Context) (<-chan *ToDoItemEventResolver, error) {
	events, err := getToDoItemService(ctx).CreateToDoItemSubscription(ctx)
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *ToDoItemEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &ToDoItemEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}
