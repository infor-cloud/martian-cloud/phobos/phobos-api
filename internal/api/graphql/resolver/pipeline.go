package resolver

import (
	"context"
	"fmt"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/comment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// PipelineJobsConnectionQueryArgs contains the arguments for querying pipeline jobs
type PipelineJobsConnectionQueryArgs struct {
	ConnectionQueryArgs
	TaskPath *string
}

// PipelineConnectionQueryArgs contains the arguments for querying pipeline jobs
type PipelineConnectionQueryArgs struct {
	ConnectionQueryArgs
	Started         *bool
	Completed       *bool
	Superseded      *bool
	PipelineTypes   *[]models.PipelineType
	EnvironmentName *string
}

// PipelineNodeQueryArgs contains the arguments for querying pipeline nodes
type PipelineNodeQueryArgs struct {
	PipelineID string
	NodePath   string
}

// PipelineEdgeResolver resolves pipeline edges
type PipelineEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *PipelineEdgeResolver) Cursor() (string, error) {
	pipeline, ok := r.edge.Node.(models.Pipeline)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&pipeline)
	return *cursor, err
}

// Node returns a pipeline node
func (r *PipelineEdgeResolver) Node() (*PipelineResolver, error) {
	pipeline, ok := r.edge.Node.(models.Pipeline)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &PipelineResolver{pipeline: &pipeline}, nil
}

// PipelineConnectionResolver resolves a pipeline connection
type PipelineConnectionResolver struct {
	connection Connection
}

// NewPipelineConnectionResolver creates a new PipelineConnectionResolver
func NewPipelineConnectionResolver(ctx context.Context, input *pipeline.GetPipelinesInput) (*PipelineConnectionResolver, error) {
	pipelineService := getPipelineService(ctx)

	result, err := pipelineService.GetPipelines(ctx, input)
	if err != nil {
		return nil, err
	}

	pipelines := result.Pipelines

	// Create edges
	edges := make([]Edge, len(pipelines))
	for i, pipeline := range pipelines {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: pipeline}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(pipelines) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&pipelines[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&pipelines[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &PipelineConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *PipelineConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *PipelineConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *PipelineConnectionResolver) Edges() *[]*PipelineEdgeResolver {
	resolvers := make([]*PipelineEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &PipelineEdgeResolver{edge: edge}
	}
	return &resolvers
}

// PipelineCronScheduleResolver resolves a pipeline cron schedule
type PipelineCronScheduleResolver struct {
	cronSchedule *models.CronSchedule
}

// Expression resolver
func (r *PipelineCronScheduleResolver) Expression() string {
	return r.cronSchedule.Expression
}

// Timezone resolver
func (r *PipelineCronScheduleResolver) Timezone() string {
	if r.cronSchedule.Timezone != nil {
		return *r.cronSchedule.Timezone
	}
	return models.DefaultCronScheduleTimezone.String()
}

// PipelineActionResolver resolves a pipeline action
type PipelineActionResolver struct {
	pipeline *models.Pipeline
	action   *models.PipelineAction
}

// Path resolver
func (r *PipelineActionResolver) Path() string {
	return r.action.Path
}

// Name resolver
func (r *PipelineActionResolver) Name() string {
	return r.action.Name
}

// Status resolver
func (r *PipelineActionResolver) Status() statemachine.NodeStatus {
	return r.action.Status
}

// Outputs resolver
func (r *PipelineActionResolver) Outputs(ctx context.Context) []*PipelineActionOutputResolver {
	service := getPipelineService(ctx)

	outputs, err := service.GetPipelineActionOutputs(ctx, r.pipeline.Metadata.ID, []string{r.action.Path})
	if err != nil {
		return nil
	}

	resolvers := make([]*PipelineActionOutputResolver, len(outputs))
	for i, output := range outputs {
		resolvers[i] = &PipelineActionOutputResolver{output: output}
	}
	return resolvers
}

// PipelineTaskResolver resolves a pipeline task
type PipelineTaskResolver struct {
	pipeline *models.Pipeline
	task     *models.PipelineTask
}

// Path resolver
func (r *PipelineTaskResolver) Path() string {
	return r.task.Path
}

// Name resolver
func (r *PipelineTaskResolver) Name() string {
	return r.task.Name
}

// Status resolver
func (r *PipelineTaskResolver) Status() statemachine.NodeStatus {
	return r.task.Status
}

// ApprovalStatus resolver
func (r *PipelineTaskResolver) ApprovalStatus() models.PipelineApprovalStatus {
	return r.task.ApprovalStatus
}

// AgentTags resolver
func (r *PipelineTaskResolver) AgentTags() []string {
	return r.task.AgentTags
}

// ApprovalRules resolver
func (r *PipelineTaskResolver) ApprovalRules(ctx context.Context) ([]*ApprovalRuleResolver, error) {
	resolvers := []*ApprovalRuleResolver{}

	for _, ruleID := range r.task.ApprovalRuleIDs {
		rule, err := loadApprovalRule(ctx, ruleID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &ApprovalRuleResolver{rule: rule})
	}

	return resolvers, nil
}

// Actions resolver
func (r *PipelineTaskResolver) Actions() []*PipelineActionResolver {
	resolvers := make([]*PipelineActionResolver, len(r.task.Actions))
	for i, action := range r.task.Actions {
		resolvers[i] = &PipelineActionResolver{action: action, pipeline: r.pipeline}
	}
	return resolvers
}

// When resolver
func (r *PipelineTaskResolver) When() string {
	return string(r.task.When)
}

// Interval resolver
func (r *PipelineTaskResolver) Interval() *int32 {
	if r.task.Interval == nil {
		return nil
	}
	return ptr.Int32(int32(r.task.Interval.Seconds()))
}

// ScheduledStartTime resolver
func (r *PipelineTaskResolver) ScheduledStartTime() *graphql.Time {
	if r.task.ScheduledStartTime == nil {
		return nil
	}
	return &graphql.Time{Time: *r.task.ScheduledStartTime}
}

// CronSchedule resolver
func (r *PipelineTaskResolver) CronSchedule() *PipelineCronScheduleResolver {
	if r.task.CronSchedule == nil {
		return nil
	}
	return &PipelineCronScheduleResolver{cronSchedule: r.task.CronSchedule}
}

// LastAttemptFinishedAt resolver
func (r *PipelineTaskResolver) LastAttemptFinishedAt() *graphql.Time {
	if r.task.LastAttemptFinishedAt == nil {
		return nil
	}
	return &graphql.Time{Time: *r.task.LastAttemptFinishedAt}
}

// MaxAttempts resolver
func (r *PipelineTaskResolver) MaxAttempts() int32 {
	return int32(r.task.MaxAttempts)
}

// AttemptCount resolver
func (r *PipelineTaskResolver) AttemptCount() int32 {
	return int32(r.task.AttemptCount)
}

// Approvals resolver
func (r *PipelineTaskResolver) Approvals(ctx context.Context) ([]*PipelineApprovalResolver, error) {
	approvals, err := getPipelineService(ctx).GetPipelineApprovals(ctx, &pipeline.GetPipelineApprovalsInput{
		PipelineID:   r.pipeline.Metadata.ID,
		NodePath:     &r.task.Path,
		ApprovalType: models.ApprovalTypeTask,
	})
	if err != nil {
		return nil, err
	}

	resolvers := make([]*PipelineApprovalResolver, len(approvals))
	for i, approval := range approvals {
		resolvers[i] = &PipelineApprovalResolver{approval: approval}
	}
	return resolvers, nil
}

// CurrentJob resolver
func (r *PipelineTaskResolver) CurrentJob(ctx context.Context) (*JobResolver, error) {
	if r.task.LatestJobID == nil {
		return nil, nil
	}

	job, err := loadJob(ctx, *r.task.LatestJobID)
	if err != nil {
		return nil, err
	}

	return &JobResolver{job: job}, nil
}

// Jobs resolver
func (r *PipelineTaskResolver) Jobs(ctx context.Context, args *ConnectionQueryArgs) (*JobConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	sortBy := db.JobSortableFieldCreatedAtDesc
	input := job.GetJobsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Sort:              &sortBy,
		ProjectID:         &r.pipeline.ProjectID,
		PipelineID:        &r.pipeline.Metadata.ID,
		PipelineTaskPath:  &r.task.Path,
	}

	if args.Sort != nil {
		sort := db.JobSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewJobConnectionResolver(ctx, &input)
}

// Dependencies resolver
func (r *PipelineTaskResolver) Dependencies() []string {
	return r.task.Dependencies
}

// Errors resolver
func (r *PipelineTaskResolver) Errors() []string {
	return r.task.Errors
}

// Image resolver
func (r *PipelineTaskResolver) Image() *string {
	return r.task.Image
}

// OnError resolver
func (r *PipelineTaskResolver) OnError() statemachine.OnErrorStrategy {
	return r.task.OnError
}

// PipelineTimestampsResolver resolves pipeline timestamps
type PipelineTimestampsResolver struct {
	timestamps *models.PipelineTimestamps
}

// StartedAt resolver
func (r *PipelineTimestampsResolver) StartedAt() *graphql.Time {
	if r.timestamps.StartedTimestamp == nil {
		return nil
	}

	return &graphql.Time{Time: *r.timestamps.StartedTimestamp}
}

// CompletedAt resolver
func (r *PipelineTimestampsResolver) CompletedAt() *graphql.Time {
	if r.timestamps.CompletedTimestamp == nil {
		return nil
	}

	return &graphql.Time{Time: *r.timestamps.CompletedTimestamp}
}

// NestedPipelineResolver resolves a nested pipeline
type NestedPipelineResolver struct {
	nestedPipeline *models.NestedPipeline
}

// Path resolver
func (r *NestedPipelineResolver) Path() string {
	return r.nestedPipeline.Path
}

// Name resolver
func (r *NestedPipelineResolver) Name() string {
	return r.nestedPipeline.Name
}

// Status resolver
func (r *NestedPipelineResolver) Status() statemachine.NodeStatus {
	return r.nestedPipeline.Status
}

// When resolver
func (r *NestedPipelineResolver) When(ctx context.Context) (string, error) {
	pipeline, err := loadPipeline(ctx, r.nestedPipeline.LatestPipelineID)
	if err != nil {
		return "", err
	}

	return string(pipeline.When), nil
}

// EnvironmentName resolver
func (r *NestedPipelineResolver) EnvironmentName() *string {
	return r.nestedPipeline.EnvironmentName
}

// ScheduledStartTime resolver
func (r *NestedPipelineResolver) ScheduledStartTime() *graphql.Time {
	if r.nestedPipeline.ScheduledStartTime == nil {
		return nil
	}
	return &graphql.Time{Time: *r.nestedPipeline.ScheduledStartTime}
}

// CronSchedule resolver
func (r *NestedPipelineResolver) CronSchedule() *PipelineCronScheduleResolver {
	if r.nestedPipeline.CronSchedule == nil {
		return nil
	}
	return &PipelineCronScheduleResolver{cronSchedule: r.nestedPipeline.CronSchedule}
}

// LatestPipeline resolver
func (r *NestedPipelineResolver) LatestPipeline(ctx context.Context) (*PipelineResolver, error) {
	pipeline, err := loadPipeline(ctx, r.nestedPipeline.LatestPipelineID)
	if err != nil {
		return nil, err
	}

	return &PipelineResolver{pipeline: pipeline}, nil
}

// Pipelines resolver
func (r *NestedPipelineResolver) Pipelines(ctx context.Context, args *ConnectionQueryArgs) (*PipelineConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	nestedPipeline, err := loadPipeline(ctx, r.nestedPipeline.LatestPipelineID)
	if err != nil {
		return nil, err
	}

	input := pipeline.GetPipelinesInput{
		PaginationOptions:            &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:                    nestedPipeline.ProjectID,
		ParentPipelineID:             nestedPipeline.ParentPipelineID,
		ParentNestedPipelineNodePath: nestedPipeline.ParentPipelineNodePath,
	}

	if args.Sort != nil {
		sort := db.PipelineSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewPipelineConnectionResolver(ctx, &input)
}

// ApprovalStatus resolver
func (r *NestedPipelineResolver) ApprovalStatus() models.PipelineApprovalStatus {
	return r.nestedPipeline.ApprovalStatus
}

// Dependencies resolver
func (r *NestedPipelineResolver) Dependencies() []string {
	return r.nestedPipeline.Dependencies
}

// Errors resolver
func (r *NestedPipelineResolver) Errors() []string {
	return r.nestedPipeline.Errors
}

// OnError resolver
func (r *NestedPipelineResolver) OnError() statemachine.OnErrorStrategy {
	return r.nestedPipeline.OnError
}

// PipelineStageResolver resolves a pipeline stage
type PipelineStageResolver struct {
	pipeline *models.Pipeline
	stage    *models.PipelineStage
}

// Path resolver
func (r *PipelineStageResolver) Path() string {
	return r.stage.Path
}

// Name resolver
func (r *PipelineStageResolver) Name() string {
	return r.stage.Name
}

// Status resolver
func (r *PipelineStageResolver) Status() statemachine.NodeStatus {
	return r.stage.Status
}

// Tasks resolver
func (r *PipelineStageResolver) Tasks() []*PipelineTaskResolver {
	resolvers := make([]*PipelineTaskResolver, len(r.stage.Tasks))
	for i, task := range r.stage.Tasks {
		resolvers[i] = &PipelineTaskResolver{pipeline: r.pipeline, task: task}
	}
	return resolvers
}

// NestedPipelines resolver
func (r *PipelineStageResolver) NestedPipelines() []*NestedPipelineResolver {
	resolvers := make([]*NestedPipelineResolver, len(r.stage.NestedPipelines))
	for i, nestedPipeline := range r.stage.NestedPipelines {
		resolvers[i] = &NestedPipelineResolver{nestedPipeline: nestedPipeline}
	}

	return resolvers
}

// PipelineVariableResolver resolves a pipeline variable
type PipelineVariableResolver struct {
	variable *pipeline.Variable
}

// Key resolver
func (r *PipelineVariableResolver) Key() string {
	return r.variable.Key
}

// Value resolver
func (r *PipelineVariableResolver) Value() string {
	return r.variable.Value
}

// Category resolver
func (r *PipelineVariableResolver) Category() models.VariableCategory {
	return r.variable.Category
}

// PipelineResolver resolves a pipeline resource
type PipelineResolver struct {
	pipeline *models.Pipeline
}

// ID resolver
func (r *PipelineResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.PipelineType, r.pipeline.Metadata.ID))
}

// Metadata resolver
func (r *PipelineResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.pipeline.Metadata}
}

// Timestamps resolver
func (r *PipelineResolver) Timestamps() *PipelineTimestampsResolver {
	return &PipelineTimestampsResolver{timestamps: &r.pipeline.Timestamps}
}

// Annotations resolver
func (r *PipelineResolver) Annotations() []*models.PipelineAnnotation {
	return r.pipeline.Annotations
}

// EnvironmentName resolver
func (r *PipelineResolver) EnvironmentName() *string {
	return r.pipeline.EnvironmentName
}

// CreatedBy resolver
func (r *PipelineResolver) CreatedBy() string {
	return r.pipeline.CreatedBy
}

// Status resolver
func (r *PipelineResolver) Status() statemachine.NodeStatus {
	return r.pipeline.Status
}

// When resolver
func (r *PipelineResolver) When() string {
	return string(r.pipeline.When)
}

// ScheduledStartTime resolver
func (r *PipelineResolver) ScheduledStartTime(ctx context.Context) (*graphql.Time, error) {
	// Scheduled start time will be nil if the pipeline is not a nested pipeline
	if r.pipeline.ParentPipelineID == nil {
		return nil, nil
	}
	parent, err := loadPipeline(ctx, *r.pipeline.ParentPipelineID)
	if err != nil {
		return nil, err
	}

	nestedNode, ok := parent.GetNestedPipeline(*r.pipeline.ParentPipelineNodePath)
	if !ok {
		return nil, fmt.Errorf("failed to find nested pipeline node for pipeline %s", gid.ToGlobalID(gid.PipelineType, r.pipeline.Metadata.ID))
	}

	if nestedNode.ScheduledStartTime == nil {
		return nil, nil
	}

	return &graphql.Time{Time: *nestedNode.ScheduledStartTime}, nil
}

// CronSchedule resolver
func (r *PipelineResolver) CronSchedule(ctx context.Context) (*PipelineCronScheduleResolver, error) {
	// Scheduled start time will be nil if the pipeline is not a nested pipeline
	if r.pipeline.ParentPipelineID == nil {
		return nil, nil
	}
	parent, err := loadPipeline(ctx, *r.pipeline.ParentPipelineID)
	if err != nil {
		return nil, err
	}

	nestedNode, ok := parent.GetNestedPipeline(*r.pipeline.ParentPipelineNodePath)
	if !ok {
		return nil, fmt.Errorf("failed to find nested pipeline node for pipeline %s", gid.ToGlobalID(gid.PipelineType, r.pipeline.Metadata.ID))
	}

	if nestedNode.CronSchedule == nil {
		return nil, nil
	}

	return &PipelineCronScheduleResolver{cronSchedule: nestedNode.CronSchedule}, nil
}

// Superseded resolver
func (r *PipelineResolver) Superseded() bool {
	return r.pipeline.Superseded
}

// Project resolver
func (r *PipelineResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	project, err := loadProject(ctx, r.pipeline.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// PipelineTemplate resolver
func (r *PipelineResolver) PipelineTemplate(ctx context.Context) (*PipelineTemplateResolver, error) {
	pipelineTemplate, err := loadPipelineTemplate(ctx, r.pipeline.PipelineTemplateID)
	if err != nil {
		return nil, err
	}

	return &PipelineTemplateResolver{pipelineTemplate: pipelineTemplate}, nil
}

// Stages resolver
func (r *PipelineResolver) Stages() []*PipelineStageResolver {
	resolvers := []*PipelineStageResolver{}
	for _, stage := range r.pipeline.Stages {
		resolvers = append(resolvers, &PipelineStageResolver{r.pipeline, stage})
	}
	return resolvers
}

// Jobs resolver
func (r *PipelineResolver) Jobs(ctx context.Context, args *PipelineJobsConnectionQueryArgs) (*JobConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := job.GetJobsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.pipeline.ProjectID,
		PipelineID:        &r.pipeline.Metadata.ID,
		PipelineTaskPath:  args.TaskPath,
	}

	if args.Sort != nil {
		sort := db.JobSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewJobConnectionResolver(ctx, &input)
}

// Type resolver
func (r *PipelineResolver) Type() models.PipelineType {
	return r.pipeline.Type
}

// ParentPipelineNodePath resolver
func (r *PipelineResolver) ParentPipelineNodePath() *string {
	return r.pipeline.ParentPipelineNodePath
}

// Release resolver
func (r *PipelineResolver) Release(ctx context.Context) (*ReleaseResolver, error) {
	if r.pipeline.ReleaseID == nil {
		return nil, nil
	}

	release, err := loadRelease(ctx, *r.pipeline.ReleaseID)
	if err != nil {
		return nil, err
	}

	return &ReleaseResolver{release: release}, nil
}

// ParentPipeline resolver
func (r *PipelineResolver) ParentPipeline(ctx context.Context) (*PipelineResolver, error) {
	if r.pipeline.ParentPipelineID == nil {
		return nil, nil
	}

	pipeline, err := loadPipeline(ctx, *r.pipeline.ParentPipelineID)
	if err != nil {
		return nil, err
	}

	return &PipelineResolver{pipeline: pipeline}, nil
}

// Variables resolver
func (r *PipelineResolver) Variables(ctx context.Context) ([]*PipelineVariableResolver, error) {
	resolvers := []*PipelineVariableResolver{}

	variables, err := getPipelineService(ctx).GetPipelineVariables(ctx, r.pipeline.Metadata.ID)
	if err != nil {
		return nil, err
	}

	for ix := range variables {
		resolvers = append(resolvers, &PipelineVariableResolver{variable: &variables[ix]})
	}

	return resolvers, nil
}

// ForceCancelAvailableAt resolver
func (r *PipelineResolver) ForceCancelAvailableAt(ctx context.Context) (*graphql.Time, error) {
	if !r.pipeline.Status.Equals(statemachine.CancelingNodeStatus) {
		return nil, nil
	}

	// Get the latest job currently being canceled, so we can determine
	// when this pipeline can be forcefully canceled.
	sort := db.JobSortableFieldCancelRequestedAtDesc
	jobsResult, err := getJobService(ctx).GetJobs(ctx, &job.GetJobsInput{
		Sort:       &sort,
		PipelineID: &r.pipeline.Metadata.ID,
		JobStatuses: []models.JobStatus{
			models.JobCanceling,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
	})
	if err != nil {
		return nil, err
	}

	if jobsResult.PageInfo.TotalCount == 0 {
		return nil, nil
	}

	return &graphql.Time{Time: jobsResult.Jobs[0].GetForceCancelAvailableAt()}, nil
}

// Approvals resolver
func (r *PipelineResolver) Approvals(ctx context.Context) ([]*PipelineApprovalResolver, error) {
	approvals, err := getPipelineService(ctx).GetPipelineApprovals(ctx, &pipeline.GetPipelineApprovalsInput{
		PipelineID:   r.pipeline.Metadata.ID,
		ApprovalType: models.ApprovalTypePipeline,
	})
	if err != nil {
		return nil, err
	}

	resolvers := make([]*PipelineApprovalResolver, len(approvals))
	for i, approval := range approvals {
		resolvers[i] = &PipelineApprovalResolver{approval: approval}
	}
	return resolvers, nil
}

// ApprovalStatus resolver
func (r *PipelineResolver) ApprovalStatus() models.PipelineApprovalStatus {
	return r.pipeline.ApprovalStatus
}

// Environment resolver
func (r *PipelineResolver) Environment(ctx context.Context) (*EnvironmentResolver, error) {
	if r.pipeline.EnvironmentName == nil {
		return nil, nil
	}

	env, err := getEnvironmentService(ctx).GetEnvironmentByPRN(
		ctx,
		models.EnvironmentResource.BuildPRN(r.pipeline.GetOrgName(), r.pipeline.GetProjectName(), *r.pipeline.EnvironmentName),
	)
	// Return nil if the environment is not found since it may have been deleted
	if errors.ErrorCode(err) == errors.ENotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &EnvironmentResolver{environment: env}, nil
}

// ApprovalRules resolver
func (r *PipelineResolver) ApprovalRules(ctx context.Context) ([]*ApprovalRuleResolver, error) {
	resolvers := []*ApprovalRuleResolver{}

	for _, ruleID := range r.pipeline.ApprovalRuleIDs {
		rule, err := loadApprovalRule(ctx, ruleID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &ApprovalRuleResolver{rule: rule})
	}

	return resolvers, nil
}

// PipelineNodeResolver resolves a pipeline node
type PipelineNodeResolver struct {
	result any
}

// ToPipelineStage resolver
func (r *PipelineNodeResolver) ToPipelineStage() (*PipelineStageResolver, bool) {
	resolver, ok := r.result.(*PipelineStageResolver)
	return resolver, ok
}

// ToPipelineTask resolver
func (r *PipelineNodeResolver) ToPipelineTask() (*PipelineTaskResolver, bool) {
	resolver, ok := r.result.(*PipelineTaskResolver)
	return resolver, ok
}

// ToPipelineAction resolver
func (r *PipelineNodeResolver) ToPipelineAction() (*PipelineActionResolver, bool) {
	resolver, ok := r.result.(*PipelineActionResolver)
	return resolver, ok
}

// ToNestedPipeline resolver
func (r *PipelineNodeResolver) ToNestedPipeline() (*NestedPipelineResolver, bool) {
	resolver, ok := r.result.(*NestedPipelineResolver)
	return resolver, ok
}

// pipelineNodeQuery queries a pipeline node by its path
func pipelineNodeQuery(ctx context.Context, args *PipelineNodeQueryArgs) (*PipelineNodeResolver, error) {
	nodePath := args.NodePath

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, args.PipelineID)
	if err != nil {
		return nil, err
	}

	pipeline, err := loadPipeline(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	node, found := pipeline.BuildStateMachine().GetNode(nodePath)
	if !found {
		return nil, errors.New("Node with path %s not found", nodePath, errors.WithErrorCode(errors.ENotFound))
	}

	var resolver any
	switch node.Type() {
	case statemachine.StageNodeType:
		stage, ok := pipeline.GetStage(nodePath)
		if !ok {
			return nil, errors.New("failed to find stage with path %s in pipeline", nodePath)
		}
		resolver = &PipelineStageResolver{pipeline: pipeline, stage: stage}
	case statemachine.TaskNodeType:
		task, ok := pipeline.GetTask(nodePath)
		if !ok {
			return nil, errors.New("failed to find task with path %s in pipeline", nodePath)
		}
		resolver = &PipelineTaskResolver{pipeline: pipeline, task: task}
	case statemachine.PipelineNodeType:
		nestedPipeline, ok := pipeline.GetNestedPipeline(nodePath)
		if !ok {
			return nil, errors.New("failed to find nested pipeline with path %s in pipeline", nodePath)
		}
		resolver = &NestedPipelineResolver{nestedPipeline: nestedPipeline}
	case statemachine.ActionNodeType:
		action, ok := pipeline.GetAction(nodePath)
		if !ok {
			return nil, errors.New("failed to find action with path %s in pipeline", nodePath)
		}
		resolver = &PipelineActionResolver{pipeline: pipeline, action: action}
	default:
		return nil, errors.New("Unknown node type")
	}

	return &PipelineNodeResolver{result: resolver}, nil
}

// Threads resolver
func (r *PipelineResolver) Threads(
	ctx context.Context,
	args *ConnectionQueryArgs,
) (*ThreadConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	sortBy := db.ThreadSortableFieldCreatedAtDesc
	input := &comment.GetThreadsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Sort:              &sortBy,
		PipelineID:        &r.pipeline.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.ThreadSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewThreadConnectionResolver(ctx, input)
}

/* Pipeline Mutations */

// PipelineMutationPayload is the response payload for a pipeline mutation
type PipelineMutationPayload struct {
	ClientMutationID *string
	Pipeline         *models.Pipeline
	Problems         []Problem
}

// PipelineMutationPayloadResolver resolves a PipelineMutationPayload
type PipelineMutationPayloadResolver struct {
	PipelineMutationPayload
}

// Pipeline field resolver
func (r *PipelineMutationPayloadResolver) Pipeline() *PipelineResolver {
	if r.PipelineMutationPayload.Pipeline == nil {
		return nil
	}

	return &PipelineResolver{pipeline: r.PipelineMutationPayload.Pipeline}
}

// CreatePipelineInput contains the input for creating a new pipeline
type CreatePipelineInput struct {
	ClientMutationID    *string
	Variables           *[]pipeline.Variable
	EnvironmentName     *string
	VariableSetRevision *string
	PipelineTemplateID  string
	PipelineType        models.PipelineType
}

// CancelPipelineInput contains the input for cancelling a pipeline
type CancelPipelineInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	Force            *bool
	ID               string
}

// ApprovePipelineInput contains the input for approving a pipeline.
type ApprovePipelineInput struct {
	ClientMutationID *string
	PipelineID       string
}

// RevokePipelineApprovalInput contains the input for revoking pipeline approval.
type RevokePipelineApprovalInput struct {
	ClientMutationID *string
	PipelineID       string
}

// ApprovePipelineTaskInput contains the input for approving a pipeline task
type ApprovePipelineTaskInput struct {
	ClientMutationID *string
	PipelineID       string
	TaskPath         string
}

// RevokePipelineTaskApprovalInput contains the input for revoking a pipeline task approval
type RevokePipelineTaskApprovalInput struct {
	ClientMutationID *string
	PipelineID       string
	TaskPath         string
}

// RetryPipelineTaskInput contains the input for rerunning a pipeline task
type RetryPipelineTaskInput struct {
	ClientMutationID *string
	PipelineID       string
	TaskPath         string
}

// RunPipelineTaskInput contains the input for starting a pipeline task
type RunPipelineTaskInput struct {
	ClientMutationID *string
	PipelineID       string
	TaskPath         string
}

// RunPipelineInput contains the input for starting a nested pipeline
type RunPipelineInput struct {
	ClientMutationID *string
	ID               string
}

// RetryNestedPipelineInput contains the input for rerunning a nested pipeline
type RetryNestedPipelineInput struct {
	ClientMutationID             *string
	ParentPipelineID             string
	ParentNestedPipelineNodePath string
}

// SchedulePipelineNodeInput contains the input for setting the scheduled start time for a pipeline node
type SchedulePipelineNodeInput struct {
	ClientMutationID   *string
	ScheduledStartTime *graphql.Time
	CronSchedule       *struct {
		Timezone   *string
		Expression string
	}
	PipelineID string
	NodeType   statemachine.NodeType
	NodePath   string
}

// CancelPipelineNodeScheduleInput contains the input for cancelling the scheduled start time for a pipeline node
type CancelPipelineNodeScheduleInput struct {
	ClientMutationID *string
	PipelineID       string
	NodeType         statemachine.NodeType
	NodePath         string
}

// DeferPipelineNodeInput contains the input for deferring a pipeline node
type DeferPipelineNodeInput struct {
	ClientMutationID *string
	Reason           string
	PipelineID       string
	NodeType         statemachine.NodeType
	NodePath         string
}

// UndeferPipelineNodeInput contains the input for undefering a pipeline node
type UndeferPipelineNodeInput struct {
	ClientMutationID *string
	PipelineID       string
	NodeType         statemachine.NodeType
	NodePath         string
}

func handlePipelineMutationProblem(e error, clientMutationID *string) (*PipelineMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := PipelineMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func createPipelineMutation(ctx context.Context, input *CreatePipelineInput) (*PipelineMutationPayloadResolver, error) {
	pipelineTemplateID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineTemplateID)
	if err != nil {
		return nil, err
	}

	toCreate := &pipeline.CreatePipelineInput{
		PipelineTemplateID:  pipelineTemplateID,
		Type:                input.PipelineType,
		EnvironmentName:     input.EnvironmentName,
		When:                models.AutoPipeline,
		VariableSetRevision: input.VariableSetRevision,
	}

	if input.Variables != nil {
		variables := []pipeline.Variable{}

		for _, v := range *input.Variables {
			variables = append(variables, pipeline.Variable{
				Key:      v.Key,
				Value:    v.Value,
				Category: v.Category,
			})
		}

		toCreate.Variables = variables
	}

	createdPipeline, err := getPipelineService(ctx).CreatePipeline(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: createdPipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func cancelPipelineMutation(ctx context.Context, input *CancelPipelineInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toCancel := &pipeline.CancelPipelineInput{
		ID: id,
	}

	if input.Force != nil {
		toCancel.Force = *input.Force
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, vErr := strconv.Atoi(input.Metadata.Version)
		if vErr != nil {
			return nil, vErr
		}

		toCancel.Version = &v
	}

	pipeline, err := pipelineService.CancelPipeline(ctx, toCancel)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func approvePipelineMutation(ctx context.Context, input *ApprovePipelineInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	if err = pipelineService.ApprovePipelineNode(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodeType:   statemachine.PipelineNodeType,
	}); err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.GetPipelineByID(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func revokePipelineApprovalMutation(ctx context.Context, input *RevokePipelineApprovalInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	if err = pipelineService.RevokePipelineNodeApproval(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodeType:   statemachine.PipelineNodeType,
	}); err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.GetPipelineByID(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func approvePipelineTaskMutation(ctx context.Context, input *ApprovePipelineTaskInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	if err = pipelineService.ApprovePipelineNode(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodeType:   statemachine.TaskNodeType,
		NodePath:   &input.TaskPath,
	}); err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.GetPipelineByID(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func revokePipelineTaskApprovalMutation(ctx context.Context,
	input *RevokePipelineTaskApprovalInput,
) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	if err = pipelineService.RevokePipelineNodeApproval(ctx, &pipeline.NodeApprovalInput{
		PipelineID: pipelineID,
		NodeType:   statemachine.TaskNodeType,
		NodePath:   &input.TaskPath,
	}); err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.GetPipelineByID(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func retryPipelineTaskMutation(ctx context.Context, input *RetryPipelineTaskInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.RetryPipelineTask(ctx, pipelineID, input.TaskPath)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func runPipelineTaskMutation(ctx context.Context, input *RunPipelineTaskInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.RunPipelineTask(ctx, pipelineID, input.TaskPath)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func runPipelineMutation(ctx context.Context, input *RunPipelineInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.RunPipeline(ctx, pipelineID)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func retryNestedPipelineMutation(ctx context.Context, input *RetryNestedPipelineInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	parentPipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ParentPipelineID)
	if err != nil {
		return nil, err
	}

	pipeline, err := pipelineService.RetryNestedPipeline(ctx, parentPipelineID, input.ParentNestedPipelineNodePath)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func schedulePipelineNodeMutation(ctx context.Context, input *SchedulePipelineNodeInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	options := &pipeline.SchedulePipelineNodeInput{
		PipelineID: pipelineID,
		NodeType:   input.NodeType,
		NodePath:   input.NodePath,
	}

	if input.ScheduledStartTime != nil {
		options.ScheduledStartTime = ptr.Time(input.ScheduledStartTime.Time)
	}

	if input.CronSchedule != nil {
		options.CronSchedule = &models.CronSchedule{
			Expression: input.CronSchedule.Expression,
			Timezone:   input.CronSchedule.Timezone,
		}
	}

	pipeline, err := pipelineService.SchedulePipelineNode(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func cancelPipelineNodeScheduleMutation(ctx context.Context, input *CancelPipelineNodeScheduleInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	options := &pipeline.UnschedulePipelineNodeInput{
		PipelineID: pipelineID,
		NodeType:   input.NodeType,
		NodePath:   input.NodePath,
	}

	pipeline, err := pipelineService.UnschedulePipelineNode(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func deferPipelineNodeMutation(ctx context.Context, input *DeferPipelineNodeInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	options := &pipeline.DeferPipelineNodeInput{
		PipelineID: pipelineID,
		Reason:     input.Reason,
		NodeType:   input.NodeType,
		NodePath:   input.NodePath,
	}

	pipeline, err := pipelineService.DeferPipelineNode(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func undeferPipelineNodeMutation(ctx context.Context, input *UndeferPipelineNodeInput) (*PipelineMutationPayloadResolver, error) {
	pipelineService := getPipelineService(ctx)

	pipelineID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	options := &pipeline.UndeferPipelineNodeInput{
		PipelineID: pipelineID,
		NodeType:   input.NodeType,
		NodePath:   input.NodePath,
	}

	pipeline, err := pipelineService.UndeferPipelineNode(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

/* Pipeline Subscriptions */

// PipelineEventResolver resolves a pipeline event
type PipelineEventResolver struct {
	event *pipeline.Event
}

// Action resolves the event action
func (r *PipelineEventResolver) Action() string {
	return r.event.Action
}

// Pipeline resolves the pipeline
func (r *PipelineEventResolver) Pipeline() *PipelineResolver {
	return &PipelineResolver{pipeline: r.event.Pipeline}
}

// ProjectPipelineEventsSubscriptionInput is the input for subscribing to pipeline events for a project
type ProjectPipelineEventsSubscriptionInput struct {
	ProjectID string
}

// PipelineEventsSubscriptionInput is the input for subscribing to the events for a single pipeline
type PipelineEventsSubscriptionInput struct {
	LastSeenVersion *string
	PipelineID      string
}

func (r RootResolver) projectPipelineEventsSubscription(ctx context.Context, input *ProjectPipelineEventsSubscriptionInput) (<-chan *PipelineEventResolver, error) {
	pipelineService := getPipelineService(ctx)
	urnResolver := getURNResolver(ctx)

	projectID, err := urnResolver.ResolveResourceID(ctx, input.ProjectID)
	if err != nil {
		return nil, err
	}

	events, err := pipelineService.SubscribeToProjectPipelines(ctx, &pipeline.SubscribeToProjectPipelinesInput{ProjectID: projectID})
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *PipelineEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &PipelineEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

func (r RootResolver) pipelineEventsSubscription(ctx context.Context, input *PipelineEventsSubscriptionInput) (<-chan *PipelineEventResolver, error) {
	pipelineService := getPipelineService(ctx)
	urnResolver := getURNResolver(ctx)

	pipelineID, err := urnResolver.ResolveResourceID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	events, err := pipelineService.SubscribeToPipeline(ctx, &pipeline.SubscribeToPipelineInput{
		PipelineID:      pipelineID,
		LastSeenVersion: input.LastSeenVersion,
	})
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *PipelineEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &PipelineEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

/* Pipeline loader */

const pipelineLoaderKey = "pipeline"

// RegisterPipelineLoader registers a pipeline loader function
func RegisterPipelineLoader(collection *loader.Collection) {
	collection.Register(pipelineLoaderKey, pipelineBatchFunc)
}

func loadPipeline(ctx context.Context, id string) (*models.Pipeline, error) {
	ldr, err := loader.Extract(ctx, pipelineLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	pipeline, ok := data.(models.Pipeline)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &pipeline, nil
}

func pipelineBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	pipelines, err := getPipelineService(ctx).GetPipelinesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range pipelines {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
