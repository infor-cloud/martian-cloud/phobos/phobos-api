package resolver

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"github.com/graph-gophers/dataloader"
	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

/* Plugin Query resolvers */

// PluginConnectionQueryArgs is the arguments for the plugin connection query
type PluginConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search *string
}

// PluginEdgeResolver resolves plugin edges
type PluginEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *PluginEdgeResolver) Cursor() (string, error) {
	plugin, ok := r.edge.Node.(models.Plugin)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&plugin)
	return *cursor, err
}

// Node returns a node
func (r *PluginEdgeResolver) Node() (*PluginResolver, error) {
	plugin, ok := r.edge.Node.(models.Plugin)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &PluginResolver{plugin: &plugin}, nil
}

// PluginConnectionResolver resolves plugin connections
type PluginConnectionResolver struct {
	connection Connection
}

// NewPluginConnectionResolver creates a new plugin connection resolver
func NewPluginConnectionResolver(ctx context.Context, input *pluginregistry.GetPluginsInput) (*PluginConnectionResolver, error) {
	result, err := getPluginRegistryService(ctx).GetPlugins(ctx, input)
	if err != nil {
		return nil, err
	}

	plugins := result.Plugins

	// Create edges
	edges := make([]Edge, len(plugins))
	for i, plugin := range plugins {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: plugin}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(plugins) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&plugins[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&plugins[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &PluginConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total count
func (r *PluginConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the page info
func (r *PluginConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the edges
func (r *PluginConnectionResolver) Edges() *[]*PluginEdgeResolver {
	resolvers := make([]*PluginEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &PluginEdgeResolver{edge: edge}
	}
	return &resolvers
}

// PluginResolver resolves a plugin
type PluginResolver struct {
	plugin *models.Plugin
}

// ID returns the ID
func (r *PluginResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.PluginType, r.plugin.Metadata.ID))
}

// Name resolver
func (r *PluginResolver) Name() string {
	return r.plugin.Name
}

// CreatedBy resolver
func (r *PluginResolver) CreatedBy() string {
	return r.plugin.CreatedBy
}

// Metadata resolver
func (r *PluginResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.plugin.Metadata}
}

// Private resolver
func (r *PluginResolver) Private() bool {
	return r.plugin.Private
}

// RepositoryURL resolver
func (r *PluginResolver) RepositoryURL() string {
	return r.plugin.RepositoryURL
}

// OrganizationName resolver
func (r *PluginResolver) OrganizationName() string {
	return r.plugin.GetOrgName()
}

// Organization resolver
func (r *PluginResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	organization, err := loadOrganization(ctx, r.plugin.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Versions resolver
func (r *PluginResolver) Versions(ctx context.Context, args *ConnectionQueryArgs) (*PluginVersionConnectionResolver, error) {
	input := &pluginregistry.GetPluginVersionsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		PluginID:          r.plugin.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.PluginVersionSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewPluginVersionConnectionResolver(ctx, input)
}

// LatestVersion resolver
func (r *PluginResolver) LatestVersion(ctx context.Context) (*PluginVersionResolver, error) {
	versionsResp, err := getPluginRegistryService(ctx).GetPluginVersions(ctx, &pluginregistry.GetPluginVersionsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
		PluginID: r.plugin.Metadata.ID,
		Latest:   ptr.Bool(true),
	})
	if err != nil {
		return nil, err
	}

	if versionsResp.PageInfo.TotalCount == 0 {
		return nil, nil
	}

	return &PluginVersionResolver{pluginVersion: &versionsResp.PluginVersions[0]}, nil
}

func pluginsQuery(ctx context.Context, args *PluginConnectionQueryArgs) (*PluginConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &pluginregistry.GetPluginsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Search:            args.Search,
	}

	if args.Sort != nil {
		sort := db.PluginSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewPluginConnectionResolver(ctx, input)
}

/* Plugin Mutation resolvers */

// PluginMutationPayload is the payload for plugin mutation
type PluginMutationPayload struct {
	ClientMutationID *string
	Plugin           *models.Plugin
	Problems         []Problem
}

// PluginMutationPayloadResolver resolves plugin mutation payload
type PluginMutationPayloadResolver struct {
	PluginMutationPayload
}

// Plugin field resolver
func (r *PluginMutationPayloadResolver) Plugin() *PluginResolver {
	if r.PluginMutationPayload.Plugin == nil {
		return nil
	}
	return &PluginResolver{plugin: r.PluginMutationPayload.Plugin}
}

// CreatePluginInput is the input for creating a plugin
type CreatePluginInput struct {
	ClientMutationID *string
	RepositoryURL    *string
	Private          *bool
	Name             string
	OrganizationID   string
}

// UpdatePluginInput is the input for updating a plugin
type UpdatePluginInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	RepositoryURL    *string
	Private          *bool
	ID               string
}

// DeletePluginInput is the input for deleting a plugin
type DeletePluginInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handlePluginMutationProblem(e error, clientMutationID *string) (*PluginMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := PluginMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &PluginMutationPayloadResolver{PluginMutationPayload: payload}, nil
}

func createPluginMutation(ctx context.Context, input *CreatePluginInput) (*PluginMutationPayloadResolver, error) {
	organizationID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.OrganizationID)
	if err != nil {
		return nil, err
	}

	toCreate := &pluginregistry.CreatePluginInput{
		Name:           input.Name,
		OrganizationID: organizationID,
		Private:        true,
	}

	if input.RepositoryURL != nil {
		toCreate.RepositoryURL = *input.RepositoryURL
	}

	if input.Private != nil {
		toCreate.Private = *input.Private
	}

	createdPlugin, err := getPluginRegistryService(ctx).CreatePlugin(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	payload := PluginMutationPayload{ClientMutationID: input.ClientMutationID, Plugin: createdPlugin, Problems: []Problem{}}
	return &PluginMutationPayloadResolver{PluginMutationPayload: payload}, nil
}

func updatePluginMutation(ctx context.Context, input *UpdatePluginInput) (*PluginMutationPayloadResolver, error) {
	pluginID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &pluginregistry.UpdatePluginInput{
		ID:            pluginID,
		RepositoryURL: input.RepositoryURL,
		Private:       input.Private,
	}

	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.Version = &v
	}

	updatedPlugin, err := getPluginRegistryService(ctx).UpdatePlugin(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := PluginMutationPayload{ClientMutationID: input.ClientMutationID, Plugin: updatedPlugin, Problems: []Problem{}}
	return &PluginMutationPayloadResolver{PluginMutationPayload: payload}, nil
}

func deletePluginMutation(ctx context.Context, input *DeletePluginInput) (*PluginMutationPayloadResolver, error) {
	pluginRegistryService := getPluginRegistryService(ctx)

	pluginID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	plugin, err := pluginRegistryService.GetPluginByID(ctx, pluginID)
	if err != nil {
		return nil, err
	}

	toDelete := &pluginregistry.DeletePluginInput{
		ID: pluginID,
	}

	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toDelete.Version = &v
	}

	if err = pluginRegistryService.DeletePlugin(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := PluginMutationPayload{ClientMutationID: input.ClientMutationID, Plugin: plugin, Problems: []Problem{}}
	return &PluginMutationPayloadResolver{PluginMutationPayload: payload}, nil
}

/* Plugin Loader */

const pluginLoaderKey = "pluginLoader"

// RegisterPluginLoader registers a plugin loader
func RegisterPluginLoader(collection *loader.Collection) {
	collection.Register(pluginLoaderKey, pluginBatchFunc)
}

func loadPlugin(ctx context.Context, id string) (*models.Plugin, error) {
	ldr, err := loader.Extract(ctx, pluginLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	plugin, ok := data.(models.Plugin)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &plugin, nil
}

func pluginBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	plugins, err := getPluginRegistryService(ctx).GetPluginsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range plugins {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
