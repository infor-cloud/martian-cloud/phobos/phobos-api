package resolver

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/aws/smithy-go/ptr"
	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ActivityEventConnectionQueryArgs represents the query arguments for the activity event connection resolver.
type ActivityEventConnectionQueryArgs struct {
	ConnectionQueryArgs
	UserID           *string
	ServiceAccountID *string
	OrganizationID   *string
	ProjectID        *string
	TimeRangeStart   *graphql.Time
	TimeRangeEnd     *graphql.Time
	Actions          *[]models.ActivityEventAction
	TargetTypes      *[]models.ActivityEventTargetType
}

// ActivityEventEdgeResolver resolves activity event edges
type ActivityEventEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ActivityEventEdgeResolver) Cursor() (string, error) {
	activityEvent, ok := r.edge.Node.(models.ActivityEvent)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&activityEvent)
	return *cursor, err
}

// Node returns an activity event node
func (r *ActivityEventEdgeResolver) Node() (*ActivityEventResolver, error) {
	activityEvent, ok := r.edge.Node.(models.ActivityEvent)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ActivityEventResolver{activityEvent: &activityEvent}, nil
}

// ActivityEventConnectionResolver resolves an activity event connection
type ActivityEventConnectionResolver struct {
	connection Connection
}

// NewActivityEventConnectionResolver creates a new ActivityEventConnectionResolver
func NewActivityEventConnectionResolver(ctx context.Context,
	input *activityevent.GetActivityEventsInput,
) (*ActivityEventConnectionResolver, error) {
	result, err := getActivityService(ctx).GetActivityEvents(ctx, input)
	if err != nil {
		return nil, err
	}

	activityEvents := result.ActivityEvents

	// Create edges
	edges := make([]Edge, len(activityEvents))
	for i, activityEvent := range activityEvents {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: activityEvent}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(activityEvents) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&activityEvents[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&activityEvents[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ActivityEventConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ActivityEventConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ActivityEventConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ActivityEventConnectionResolver) Edges() *[]*ActivityEventEdgeResolver {
	resolvers := make([]*ActivityEventEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ActivityEventEdgeResolver{edge: edge}
	}
	return &resolvers
}

/* Activity Events Payload Resolvers */

// ActivityEventCreateMembershipPayloadResolver resolves the payload for
// create membership activity events.
type ActivityEventCreateMembershipPayloadResolver struct {
	payload *models.ActivityEventCreateMembershipPayload
}

// Member resolver
func (r *ActivityEventCreateMembershipPayloadResolver) Member(ctx context.Context) (*MemberResolver, error) {
	resolver, err := makeMemberResolver(ctx,
		r.payload.UserID,
		r.payload.TeamID,
		r.payload.ServiceAccountID)
	if errors.ErrorCode(err) == errors.ENotFound {
		return nil, nil
	}
	return resolver, err
}

// Role resolver
func (r *ActivityEventCreateMembershipPayloadResolver) Role() string {
	return r.payload.Role
}

// ActivityEventRemoveMembershipPayloadResolver resolves the payload for
// remove membership activity events.
type ActivityEventRemoveMembershipPayloadResolver struct {
	payload *models.ActivityEventRemoveMembershipPayload
}

// Member resolver
func (r *ActivityEventRemoveMembershipPayloadResolver) Member(ctx context.Context) (*MemberResolver, error) {
	resolver, err := makeMemberResolver(ctx,
		r.payload.UserID,
		r.payload.TeamID,
		r.payload.ServiceAccountID)
	if errors.ErrorCode(err) == errors.ENotFound {
		return nil, nil
	}
	return resolver, err
}

// ActivityEventDeleteResourcePayloadResolver resolves the payload for
// delete resource activity events.
type ActivityEventDeleteResourcePayloadResolver struct {
	payload *models.ActivityEventDeleteResourcePayload
}

// Name resolver
func (r *ActivityEventDeleteResourcePayloadResolver) Name() *string {
	return r.payload.Name
}

// ID resolver
func (r *ActivityEventDeleteResourcePayloadResolver) ID() string {
	return r.payload.ID
}

// Type resolver
func (r *ActivityEventDeleteResourcePayloadResolver) Type() string {
	return r.payload.Type
}

// ActivityEventUpdateTeamPayloadResolver resolves the payload for
// team member activity events.
type ActivityEventUpdateTeamPayloadResolver struct {
	payload *models.ActivityEventUpdateTeamPayload
}

// ChangeType resolver
func (r *ActivityEventUpdateTeamPayloadResolver) ChangeType() string {
	return r.payload.ChangeType
}

// User resolver
func (r *ActivityEventUpdateTeamPayloadResolver) User(ctx context.Context) (*UserResolver, error) {
	user, err := loadUser(ctx, r.payload.UserID)
	if err != nil {
		// Return nil if user is not found since user may have been deleted
		if errors.ErrorCode(err) == errors.ENotFound {
			return nil, nil
		}
		return nil, err
	}
	return &UserResolver{user: user}, nil
}

// Maintainer resolver
func (r *ActivityEventUpdateTeamPayloadResolver) Maintainer() bool {
	return r.payload.Maintainer
}

// ActivityEventUpdateReleasePayloadResolver resolves the payload for
// update release activity events.
type ActivityEventUpdateReleasePayloadResolver struct {
	payload *models.ActivityEventUpdateReleasePayload
}

// Resource resolver
func (r *ActivityEventUpdateReleasePayloadResolver) Resource(ctx context.Context) (*NodeResolver, error) {
	switch r.payload.Type {
	case models.TargetUser.String():
		user, err := loadUser(ctx, r.payload.ID)
		if err != nil {
			if errors.ErrorCode(err) == errors.ENotFound {
				return nil, nil
			}
			return nil, err
		}
		return &NodeResolver{result: &UserResolver{user: user}}, nil
	case models.TargetTeam.String():
		team, err := loadTeam(ctx, r.payload.ID)
		if err != nil {
			if errors.ErrorCode(err) == errors.ENotFound {
				return nil, nil
			}
			return nil, err
		}
		return &NodeResolver{result: &TeamResolver{team: team}}, nil
	default:
		return nil, errors.New("unknown release resource type %s", r.payload.Type)
	}
}

// ChangeType resolver
func (r *ActivityEventUpdateReleasePayloadResolver) ChangeType() string {
	return r.payload.ChangeType
}

// Type resolver
func (r *ActivityEventUpdateReleasePayloadResolver) Type() string {
	return r.payload.Type
}

// ActivityEventUpdatePipelineNodePayloadResolver resolves the payload for
// update pipeline node activity events.
type ActivityEventUpdatePipelineNodePayloadResolver struct {
	payload *models.ActivityEventUpdatePipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventUpdatePipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventUpdatePipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// ActivityEventApprovePipelineNodePayloadResolver resolves the payload for
// approve pipeline node activity events.
type ActivityEventApprovePipelineNodePayloadResolver struct {
	payload *models.ActivityEventApprovePipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventApprovePipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventApprovePipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// ActivityEventRevokeApprovalPipelineNodePayloadResolver resolves the payload for
// revoke approval pipeline node activity events.
type ActivityEventRevokeApprovalPipelineNodePayloadResolver struct {
	payload *models.ActivityEventRevokeApprovalPipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventRevokeApprovalPipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventRevokeApprovalPipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// ActivityEventRetryPipelineNodePayloadResolver resolves the payload for
// retry pipeline node activity events.
type ActivityEventRetryPipelineNodePayloadResolver struct {
	payload *models.ActivityEventRetryPipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventRetryPipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventRetryPipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// ActivityEventSchedulePipelineNodePayloadResolver resolves the payload for
// schedule pipeline node activity events.
type ActivityEventSchedulePipelineNodePayloadResolver struct {
	payload *models.ActivityEventSchedulePipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventSchedulePipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventSchedulePipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// StartTime resolver
func (r *ActivityEventSchedulePipelineNodePayloadResolver) StartTime() *graphql.Time {
	if r.payload.StartTime == nil {
		return nil
	}

	return &graphql.Time{Time: *r.payload.StartTime}
}

// ActivityEventUnschedulePipelineNodePayloadResolver resolves the payload for
// unschedule pipeline node activity events.
type ActivityEventUnschedulePipelineNodePayloadResolver struct {
	payload *models.ActivityEventUnschedulePipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventUnschedulePipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventUnschedulePipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// ActivityEventStartPipelineNodePayloadResolver resolves the payload for
// start pipeline node activity events.
type ActivityEventStartPipelineNodePayloadResolver struct {
	payload *models.ActivityEventStartPipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventStartPipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventStartPipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// ActivityEventDeferPipelineNodePayloadResolver resolves the payload for
// defer pipeline node activity events.
type ActivityEventDeferPipelineNodePayloadResolver struct {
	payload *models.ActivityEventDeferPipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventDeferPipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventDeferPipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// Reason resolver
func (r *ActivityEventDeferPipelineNodePayloadResolver) Reason() string {
	return r.payload.Reason
}

// ActivityEventUndeferPipelineNodePayloadResolver resolves the payload for
// undefer pipeline node activity events.
type ActivityEventUndeferPipelineNodePayloadResolver struct {
	payload *models.ActivityEventUndeferPipelineNodePayload
}

// NodePath resolver
func (r *ActivityEventUndeferPipelineNodePayloadResolver) NodePath() string {
	return r.payload.NodePath
}

// NodeType resolver
func (r *ActivityEventUndeferPipelineNodePayloadResolver) NodeType() string {
	return r.payload.NodeType
}

// ActivityEventPayloadResolver resolves the Payload union type
type ActivityEventPayloadResolver struct {
	result interface{}
}

// ToActivityEventCreateMembershipPayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventCreateMembershipPayload() (*ActivityEventCreateMembershipPayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventCreateMembershipPayloadResolver)
	return res, ok
}

// ToActivityEventUpdateMembershipPayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventUpdateMembershipPayload() (*models.ActivityEventUpdateMembershipPayload, bool) {
	res, ok := r.result.(*models.ActivityEventUpdateMembershipPayload)
	return res, ok
}

// ToActivityEventRemoveMembershipPayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventRemoveMembershipPayload() (*ActivityEventRemoveMembershipPayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventRemoveMembershipPayloadResolver)
	return res, ok
}

// ToActivityEventDeleteResourcePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventDeleteResourcePayload() (*ActivityEventDeleteResourcePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventDeleteResourcePayloadResolver)
	return res, ok
}

// ToActivityEventUpdateTeamPayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventUpdateTeamPayload() (*ActivityEventUpdateTeamPayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventUpdateTeamPayloadResolver)
	return res, ok
}

// ToActivityEventUpdateReleasePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventUpdateReleasePayload() (*ActivityEventUpdateReleasePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventUpdateReleasePayloadResolver)
	return res, ok
}

// ToActivityEventUpdatePipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventUpdatePipelineNodePayload() (*ActivityEventUpdatePipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventUpdatePipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventApprovePipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventApprovePipelineNodePayload() (*ActivityEventApprovePipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventApprovePipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventRevokeApprovalPipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventRevokeApprovalPipelineNodePayload() (*ActivityEventRevokeApprovalPipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventRevokeApprovalPipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventRetryPipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventRetryPipelineNodePayload() (*ActivityEventRetryPipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventRetryPipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventSchedulePipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventSchedulePipelineNodePayload() (*ActivityEventSchedulePipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventSchedulePipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventUnschedulePipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventUnschedulePipelineNodePayload() (*ActivityEventUnschedulePipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventUnschedulePipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventStartPipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventStartPipelineNodePayload() (*ActivityEventStartPipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventStartPipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventDeferPipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventDeferPipelineNodePayload() (*ActivityEventDeferPipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventDeferPipelineNodePayloadResolver)
	return res, ok
}

// ToActivityEventUndeferPipelineNodePayload resolver
func (r *ActivityEventPayloadResolver) ToActivityEventUndeferPipelineNodePayload() (*ActivityEventUndeferPipelineNodePayloadResolver, bool) {
	res, ok := r.result.(*ActivityEventUndeferPipelineNodePayloadResolver)
	return res, ok
}

/* Activity Event Resolver */

// ActivityEventResolver resolves an activity event
type ActivityEventResolver struct {
	activityEvent *models.ActivityEvent
}

// ID resolver
func (r *ActivityEventResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ActivityEventType, r.activityEvent.Metadata.ID))
}

// Metadata resolver
func (r *ActivityEventResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.activityEvent.Metadata}
}

// Action resolver
func (r *ActivityEventResolver) Action() models.ActivityEventAction {
	return r.activityEvent.Action
}

// Initiator resolver
func (r *ActivityEventResolver) Initiator(ctx context.Context) (*MemberResolver, error) {
	return makeMemberResolver(ctx, r.activityEvent.UserID, nil, r.activityEvent.ServiceAccountID)
}

// Organization resolver
func (r *ActivityEventResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	if r.activityEvent.OrganizationID == nil {
		return nil, nil
	}

	organization, err := loadOrganization(ctx, *r.activityEvent.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Project resolver
func (r *ActivityEventResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.activityEvent.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.activityEvent.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// Target resolver
func (r *ActivityEventResolver) Target(ctx context.Context) (*NodeResolver, error) {
	// Query for target based on type
	switch r.activityEvent.TargetType {
	case models.TargetRole:
		role, err := loadRole(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &RoleResolver{role: role}}, nil
	case models.TargetOrganization:
		organization, err := loadOrganization(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &OrganizationResolver{organization: organization}}, nil
	case models.TargetProject:
		project, err := loadProject(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &ProjectResolver{project: project}}, nil
	case models.TargetProjectVariableSet:
		variableSet, err := loadProjectVariableSet(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &ProjectVariableSetResolver{variableSet: variableSet}}, nil
	case models.TargetGlobal:
		// Global targets aren't associated with anything.
		return nil, nil
	case models.TargetMembership:
		membership, err := loadMembership(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &MembershipResolver{membership: membership}}, nil
	case models.TargetAgent:
		agent, err := loadAgent(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &AgentResolver{agent: agent}}, nil
	case models.TargetPipelineTemplate:
		pipelineTemplate, err := loadPipelineTemplate(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &PipelineTemplateResolver{pipelineTemplate: pipelineTemplate}}, nil
	case models.TargetPipeline:
		pipeline, err := loadPipeline(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &PipelineResolver{pipeline: pipeline}}, nil
	case models.TargetTeam:
		team, err := loadTeam(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &TeamResolver{team: team}}, nil
	case models.TargetEnvironment:
		environment, err := loadEnvironment(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &EnvironmentResolver{environment: environment}}, nil
	case models.TargetServiceAccount:
		serviceAccount, err := loadServiceAccount(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &ServiceAccountResolver{serviceAccount: serviceAccount}}, nil
	case models.TargetApprovalRule:
		rule, err := loadApprovalRule(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &ApprovalRuleResolver{rule: rule}}, nil
	case models.TargetLifecycleTemplate:
		lifecycleTemplate, err := loadLifecycleTemplate(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &LifecycleTemplateResolver{lifecycleTemplate: lifecycleTemplate}}, nil
	case models.TargetReleaseLifecycle:
		releaseLifecycle, err := loadReleaseLifecycle(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &ReleaseLifecycleResolver{releaseLifecycle: releaseLifecycle}}, nil
	case models.TargetRelease:
		release, err := loadRelease(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &ReleaseResolver{release: release}}, nil
	case models.TargetPlugin:
		plugin, err := loadPlugin(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &PluginResolver{plugin: plugin}}, nil
	case models.TargetPluginVersion:
		pluginVersion, err := loadPluginVersion(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &PluginVersionResolver{pluginVersion: pluginVersion}}, nil
	case models.TargetComment:
		comment, err := loadComment(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &CommentResolver{comment: comment}}, nil
	case models.TargetUser:
		user, err := loadUser(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &UserResolver{user: user}}, nil
	case models.TargetVCSProvider:
		vcsProvider, err := loadVCSProvider(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &VCSProviderResolver{vcsProvider: vcsProvider}}, nil
	case models.TargetThread:
		thread, err := loadThread(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &ThreadResolver{thread: thread}}, nil
	case models.TargetEnvironmentRule:
		rule, err := loadEnvironmentRule(ctx, *r.activityEvent.TargetID)
		if err != nil {
			return nil, err
		}
		return &NodeResolver{result: &EnvironmentRuleResolver{rule: rule}}, nil
	default:
		return nil, errors.New("invalid target type: %s", r.activityEvent.TargetType, errors.WithErrorCode(errors.EInvalid))
	}
}

// TargetType resolver
func (r *ActivityEventResolver) TargetType() models.ActivityEventTargetType {
	return r.activityEvent.TargetType
}

// TargetID resolver
func (r *ActivityEventResolver) TargetID() *string {
	if r.activityEvent.TargetID == nil {
		return nil
	}

	return ptr.String(gid.ToGlobalID(gid.ActivityEventType, *r.activityEvent.TargetID))
}

// Payload resolver
func (r *ActivityEventResolver) Payload() (*ActivityEventPayloadResolver, error) {
	if r.activityEvent.Payload == nil {
		return nil, nil
	}

	switch {
	case r.activityEvent.Action == models.ActionCreateMembership:
		var payload models.ActivityEventCreateMembershipPayload
		if err := json.Unmarshal(r.activityEvent.Payload, &payload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventCreateMembershipPayloadResolver{payload: &payload}}, nil
	case (r.activityEvent.Action == models.ActionUpdate) &&
		(r.activityEvent.TargetType == models.TargetMembership):
		var payload models.ActivityEventUpdateMembershipPayload
		if err := json.Unmarshal(r.activityEvent.Payload, &payload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &payload}, nil

	case r.activityEvent.Action == models.ActionRemoveMembership:
		var payload models.ActivityEventRemoveMembershipPayload
		if err := json.Unmarshal(r.activityEvent.Payload, &payload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventRemoveMembershipPayloadResolver{payload: &payload}}, nil
	case r.activityEvent.Action == models.ActionDelete:
		var payload models.ActivityEventDeleteResourcePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &payload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventDeleteResourcePayloadResolver{payload: &payload}}, nil
	case (r.activityEvent.Action == models.ActionUpdate) &&
		(r.activityEvent.TargetType == models.TargetTeam):
		var addPayload models.ActivityEventUpdateTeamPayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}

		return &ActivityEventPayloadResolver{result: &ActivityEventUpdateTeamPayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionUpdate) &&
		(r.activityEvent.TargetType == models.TargetRelease):
		var addPayload models.ActivityEventUpdateReleasePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventUpdateReleasePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionUpdate) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventUpdatePipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventUpdatePipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionApprove) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventApprovePipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventApprovePipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionRevokeApproval) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventRevokeApprovalPipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventRevokeApprovalPipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionRetry) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventRetryPipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventRetryPipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionSchedule) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventSchedulePipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventSchedulePipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionUnschedule) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventUnschedulePipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventUnschedulePipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionStart) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventStartPipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventStartPipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionDefer) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventDeferPipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventDeferPipelineNodePayloadResolver{payload: &addPayload}}, nil
	case (r.activityEvent.Action == models.ActionUndefer) &&
		(r.activityEvent.TargetType == models.TargetPipeline):
		var addPayload models.ActivityEventUndeferPipelineNodePayload
		if err := json.Unmarshal(r.activityEvent.Payload, &addPayload); err != nil {
			return nil, err
		}
		return &ActivityEventPayloadResolver{result: &ActivityEventUndeferPipelineNodePayloadResolver{payload: &addPayload}}, nil
	default:
		return nil, fmt.Errorf("payload supplied without a supported target type %s and action %s", r.activityEvent.TargetType, r.activityEvent.Action)
	}
}

/* Activity Event Subscriptions */

// ActivityEventsSubscriptionInput represents the input for the activity events subscription
type ActivityEventsSubscriptionInput struct {
	OrganizationID *string
	ProjectID      *string
}

func (r RootResolver) liveActivityEventsSubscription(ctx context.Context, input *ActivityEventsSubscriptionInput) (<-chan *ActivityEventResolver, error) {
	activityService := getActivityService(ctx)
	urnResolver := getURNResolver(ctx)

	subscriptionInput := &activityevent.SubscribeToActivityEventsInput{}

	if input.OrganizationID != nil {
		organizationID, err := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if err != nil {
			return nil, err
		}

		subscriptionInput.OrganizationID = &organizationID
	}

	if input.ProjectID != nil {
		projectID, err := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if err != nil {
			return nil, err
		}

		subscriptionInput.ProjectID = &projectID
	}

	events, err := activityService.SubscribeToActivityEvents(ctx, subscriptionInput)
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *ActivityEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &ActivityEventResolver{activityEvent: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

/* Activity Events Queries */

func activityEventsQuery(ctx context.Context, args *ActivityEventConnectionQueryArgs) (*ActivityEventConnectionResolver, error) {
	input, err := getActivityEventsInputFromQueryArgs(ctx, args)
	if err != nil {
		return nil, err
	}

	// For the top-level activity events query, no changes need to be made to the input struct.

	return NewActivityEventConnectionResolver(ctx, input)
}

// getActivityEventsInputFromQueryArgs is for the convenience of other modules in this package
// Other modules may need to modify the input before creating a resolver.
func getActivityEventsInputFromQueryArgs(ctx context.Context,
	args *ActivityEventConnectionQueryArgs,
) (*activityevent.GetActivityEventsInput, error) {
	urnResolver := getURNResolver(ctx)

	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := activityevent.GetActivityEventsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
	}

	if args.UserID != nil {
		userID, err := urnResolver.ResolveResourceID(ctx, *args.UserID)
		if err != nil {
			return nil, err
		}

		input.UserID = &userID
	}

	if args.ServiceAccountID != nil {
		serviceAccountID, err := urnResolver.ResolveResourceID(ctx, *args.ServiceAccountID)
		if err != nil {
			return nil, err
		}

		input.ServiceAccountID = &serviceAccountID
	}

	if args.OrganizationID != nil {
		organizationID, err := urnResolver.ResolveResourceID(ctx, *args.OrganizationID)
		if err != nil {
			return nil, err
		}

		input.OrganizationID = &organizationID
	}

	if args.ProjectID != nil {
		projectID, err := urnResolver.ResolveResourceID(ctx, *args.ProjectID)
		if err != nil {
			return nil, err
		}

		input.ProjectID = &projectID
	}

	if args.TimeRangeStart != nil {
		input.TimeRangeStart = &args.TimeRangeStart.Time
	}

	if args.TimeRangeEnd != nil {
		input.TimeRangeEnd = &args.TimeRangeEnd.Time
	}

	if args.Actions != nil {
		input.Actions = *args.Actions
	}

	if args.TargetTypes != nil {
		input.TargetTypes = *args.TargetTypes
	}

	if args.Sort != nil {
		sort := db.ActivityEventSortableField(*args.Sort)
		input.Sort = &sort
	}

	return &input, nil
}
