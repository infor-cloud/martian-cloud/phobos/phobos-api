package resolver

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
)

/* Agent Query Resolvers */

// AgentEdgeResolver resolves agent edges
type AgentEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *AgentEdgeResolver) Cursor() (string, error) {
	agent, ok := r.edge.Node.(models.Agent)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&agent)
	return *cursor, err
}

// Node returns a agent node
func (r *AgentEdgeResolver) Node() (*AgentResolver, error) {
	agent, ok := r.edge.Node.(models.Agent)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &AgentResolver{agent: &agent}, nil
}

// AgentConnectionResolver resolves a agent connection
type AgentConnectionResolver struct {
	connection Connection
}

// NewAgentConnectionResolver creates a new AgentConnectionResolver
func NewAgentConnectionResolver(ctx context.Context, input *agent.GetAgentsInput) (*AgentConnectionResolver, error) {
	service := getAgentService(ctx)

	result, err := service.GetAgents(ctx, input)
	if err != nil {
		return nil, err
	}

	agents := result.Agents

	// Create edges
	edges := make([]Edge, len(agents))
	for i, agent := range agents {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: agent}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(agents) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&agents[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&agents[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &AgentConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *AgentConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *AgentConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *AgentConnectionResolver) Edges() *[]*AgentEdgeResolver {
	resolvers := make([]*AgentEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &AgentEdgeResolver{edge: edge}
	}
	return &resolvers
}

// AgentResolver resolves a agent resource
type AgentResolver struct {
	agent *models.Agent
}

// ID resolver
func (r *AgentResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.AgentType, r.agent.Metadata.ID))
}

// Name resolver
func (r *AgentResolver) Name() string {
	return r.agent.Name
}

// Description resolver
func (r *AgentResolver) Description() string {
	return r.agent.Description
}

// Type resolver
func (r *AgentResolver) Type() models.AgentType {
	return r.agent.Type
}

// Disabled resolver
func (r *AgentResolver) Disabled() bool {
	return r.agent.Disabled
}

// RunUntaggedJobs resolver
func (r *AgentResolver) RunUntaggedJobs() bool {
	return r.agent.RunUntaggedJobs
}

// Tags resolver
func (r *AgentResolver) Tags() []string {
	return r.agent.Tags
}

// Metadata resolver
func (r *AgentResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.agent.Metadata}
}

// CreatedBy resolver
func (r *AgentResolver) CreatedBy() string {
	return r.agent.CreatedBy
}

// Organization resolver
func (r *AgentResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	if r.agent.OrganizationID == nil {
		return nil, nil
	}

	organization, err := loadOrganization(ctx, *r.agent.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Sessions resolver
func (r *AgentResolver) Sessions(ctx context.Context, args *ConnectionQueryArgs) (*AgentSessionConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := agent.GetAgentSessionsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		AgentID:           r.agent.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.AgentSessionSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewAgentSessionConnectionResolver(ctx, &input)
}

// Jobs resolver
func (r *AgentResolver) Jobs(ctx context.Context, args *ConnectionQueryArgs) (*JobConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := job.GetJobsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		AgentID:           &r.agent.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.JobSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewJobConnectionResolver(ctx, &input)
}

// AssignedServiceAccounts resolver
func (r *AgentResolver) AssignedServiceAccounts(ctx context.Context, args *ConnectionQueryArgs) (*ServiceAccountConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := serviceaccount.GetServiceAccountsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		AgentID:           &r.agent.Metadata.ID,
		OrganizationID:    r.agent.OrganizationID,
	}

	return NewServiceAccountConnectionResolver(ctx, &input)
}

func sharedAgentsQuery(ctx context.Context, args *ConnectionQueryArgs) (*AgentConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	sharedAgentType := models.SharedAgent
	input := agent.GetAgentsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		AgentType:         &sharedAgentType,
	}

	if args.Sort != nil {
		sort := db.AgentSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewAgentConnectionResolver(ctx, &input)
}

/* Agent Mutation Resolvers */

// AgentMutationPayload is the response payload for a agent mutation
type AgentMutationPayload struct {
	ClientMutationID *string
	Agent            *models.Agent
	ServiceAccount   *models.ServiceAccount
	Problems         []Problem
}

// AgentMutationPayloadResolver resolves a AgentMutationPayload
type AgentMutationPayloadResolver struct {
	AgentMutationPayload
}

// Agent field resolver
func (r *AgentMutationPayloadResolver) Agent() *AgentResolver {
	if r.AgentMutationPayload.Agent == nil {
		return nil
	}

	return &AgentResolver{agent: r.AgentMutationPayload.Agent}
}

// ServiceAccount field resolver
func (r *AgentMutationPayloadResolver) ServiceAccount() *ServiceAccountResolver {
	if r.AgentMutationPayload.ServiceAccount == nil {
		return nil
	}

	return &ServiceAccountResolver{serviceAccount: r.AgentMutationPayload.ServiceAccount}
}

// CreateAgentInput contains the input for creating a new agent
type CreateAgentInput struct {
	ClientMutationID *string
	OrganizationID   *string
	Type             models.AgentType
	Name             string
	Description      string
	Tags             []string
	Disabled         bool
	RunUntaggedJobs  bool
}

// UpdateAgentInput contains the input for updating a agent
type UpdateAgentInput struct {
	ClientMutationID *string
	Description      *string
	Metadata         *MetadataInput
	Disabled         *bool
	RunUntaggedJobs  *bool
	Tags             *[]string
	ID               string
}

// DeleteAgentInput contains the input for deleting a agent
type DeleteAgentInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

// AssignServiceAccountToAgentInput is used to assign a service account to a agent
type AssignServiceAccountToAgentInput struct {
	ClientMutationID *string
	AgentID          string
	ServiceAccountID string
}

func handleAgentMutationProblem(e error, clientMutationID *string) (*AgentMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}

	payload := AgentMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &AgentMutationPayloadResolver{AgentMutationPayload: payload}, nil
}

func createAgentMutation(ctx context.Context, input *CreateAgentInput) (*AgentMutationPayloadResolver, error) {
	service := getAgentService(ctx)

	var orgID *string
	if input.OrganizationID != nil {
		id, err := getURNResolver(ctx).ResolveResourceID(ctx, *input.OrganizationID)
		if err != nil {
			return nil, err
		}
		orgID = &id
	}

	createdAgent, err := service.CreateAgent(ctx, &agent.CreateAgentInput{
		Type:            input.Type,
		Name:            input.Name,
		Description:     input.Description,
		OrganizationID:  orgID,
		Disabled:        input.Disabled,
		RunUntaggedJobs: input.RunUntaggedJobs,
		Tags:            input.Tags,
	})
	if err != nil {
		return nil, err
	}

	payload := AgentMutationPayload{ClientMutationID: input.ClientMutationID, Agent: createdAgent, Problems: []Problem{}}
	return &AgentMutationPayloadResolver{AgentMutationPayload: payload}, nil
}

func updateAgentMutation(ctx context.Context, input *UpdateAgentInput) (*AgentMutationPayloadResolver, error) {
	service := getAgentService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &agent.UpdateAgentInput{
		ID:              id,
		Description:     input.Description,
		Disabled:        input.Disabled,
		RunUntaggedJobs: input.RunUntaggedJobs,
	}

	if input.Tags != nil {
		toUpdate.Tags = *input.Tags
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.MetadataVersion = &v
	}

	agent, err := service.UpdateAgent(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := AgentMutationPayload{ClientMutationID: input.ClientMutationID, Agent: agent, Problems: []Problem{}}
	return &AgentMutationPayloadResolver{AgentMutationPayload: payload}, nil
}

func deleteAgentMutation(ctx context.Context, input *DeleteAgentInput) (*AgentMutationPayloadResolver, error) {
	service := getAgentService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	gotAgent, err := service.GetAgentByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &agent.DeleteAgentInput{
		ID: gotAgent.Metadata.ID,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.MetadataVersion = &v
	}

	if err := service.DeleteAgent(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := AgentMutationPayload{ClientMutationID: input.ClientMutationID, Agent: gotAgent, Problems: []Problem{}}
	return &AgentMutationPayloadResolver{AgentMutationPayload: payload}, nil
}

func assignServiceAccountToAgentMutation(ctx context.Context, input *AssignServiceAccountToAgentInput) (*AgentMutationPayloadResolver, error) {
	agentService := getAgentService(ctx)
	urnResolver := getURNResolver(ctx)

	agentID, err := urnResolver.ResolveResourceID(ctx, input.AgentID)
	if err != nil {
		return nil, err
	}

	serviceAccountID, err := urnResolver.ResolveResourceID(ctx, input.ServiceAccountID)
	if err != nil {
		return nil, err
	}

	gotAgent, err := agentService.GetAgentByID(ctx, agentID)
	if err != nil {
		return nil, err
	}

	toAssign := &agent.AssignServiceAccountToAgentInput{
		ID:               gotAgent.Metadata.ID,
		ServiceAccountID: serviceAccountID,
	}

	if err = agentService.AssignServiceAccountToAgent(ctx, toAssign); err != nil {
		return nil, err
	}

	serviceAccount, err := getServiceAccountService(ctx).GetServiceAccountByID(ctx, serviceAccountID)
	if err != nil {
		return nil, err
	}

	payload := AgentMutationPayload{ClientMutationID: input.ClientMutationID, Agent: gotAgent, ServiceAccount: serviceAccount, Problems: []Problem{}}
	return &AgentMutationPayloadResolver{AgentMutationPayload: payload}, nil
}

func unassignServiceAccountFromAgentMutation(ctx context.Context, input *AssignServiceAccountToAgentInput) (*AgentMutationPayloadResolver, error) {
	agentService := getAgentService(ctx)
	urnResolver := getURNResolver(ctx)

	agentID, err := urnResolver.ResolveResourceID(ctx, input.AgentID)
	if err != nil {
		return nil, err
	}

	serviceAccountID, err := urnResolver.ResolveResourceID(ctx, input.ServiceAccountID)
	if err != nil {
		return nil, err
	}

	gotAgent, err := agentService.GetAgentByID(ctx, agentID)
	if err != nil {
		return nil, err
	}

	toUnassign := &agent.UnassignServiceAccountFromAgentInput{
		ID:               gotAgent.Metadata.ID,
		ServiceAccountID: serviceAccountID,
	}

	if err = agentService.UnassignServiceAccountFromAgent(ctx, toUnassign); err != nil {
		return nil, err
	}

	serviceAccount, err := getServiceAccountService(ctx).GetServiceAccountByID(ctx, serviceAccountID)
	if err != nil {
		return nil, err
	}

	payload := AgentMutationPayload{ClientMutationID: input.ClientMutationID, Agent: gotAgent, ServiceAccount: serviceAccount, Problems: []Problem{}}
	return &AgentMutationPayloadResolver{AgentMutationPayload: payload}, nil
}

/* Agent loader */

const agentLoaderKey = "agent"

// RegisterAgentLoader registers a agent loader function
func RegisterAgentLoader(collection *loader.Collection) {
	collection.Register(agentLoaderKey, agentBatchFunc)
}

func loadAgent(ctx context.Context, id string) (*models.Agent, error) {
	ldr, err := loader.Extract(ctx, agentLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	agent, ok := data.(models.Agent)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &agent, nil
}

func agentBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	agentService := getAgentService(ctx)

	agents, err := agentService.GetAgentsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range agents {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
