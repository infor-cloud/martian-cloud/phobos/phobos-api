package resolver

import (
	"context"

	"github.com/aws/smithy-go/ptr"
	"github.com/graph-gophers/dataloader"
	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/comment"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ThreadEdgeResolver resolves thread edges
type ThreadEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ThreadEdgeResolver) Cursor() (string, error) {
	thread, ok := r.edge.Node.(models.Thread)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&thread)
	return *cursor, err
}

// Node returns a thread node
func (r *ThreadEdgeResolver) Node() (*ThreadResolver, error) {
	thread, ok := r.edge.Node.(models.Thread)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ThreadResolver{thread: &thread}, nil
}

// ThreadConnectionResolver resolves a thread connection
type ThreadConnectionResolver struct {
	connection Connection
}

// NewThreadConnectionResolver creates a new Thread connection
func NewThreadConnectionResolver(ctx context.Context, input *comment.GetThreadsInput) (*ThreadConnectionResolver, error) {
	commentService := getCommentService(ctx)

	result, err := commentService.GetThreads(ctx, input)
	if err != nil {
		return nil, err
	}

	threads := result.Threads

	// Create edges
	edges := make([]Edge, len(threads))
	for i, thread := range threads {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: thread}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(threads) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&threads[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&threads[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ThreadConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ThreadConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ThreadConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ThreadConnectionResolver) Edges() *[]*ThreadEdgeResolver {
	resolvers := make([]*ThreadEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ThreadEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ThreadResolver resolves a thread resource
type ThreadResolver struct {
	thread *models.Thread
}

// ID resolver
func (r *ThreadResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ThreadType, r.thread.Metadata.ID))
}

// Metadata resolver
func (r *ThreadResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.thread.Metadata}
}

// Pipeline resolver
func (r *ThreadResolver) Pipeline(ctx context.Context) (*PipelineResolver, error) {
	if r.thread.PipelineID == nil {
		return nil, nil
	}

	pipeline, err := loadPipeline(ctx, *r.thread.PipelineID)
	if err != nil {
		return nil, err
	}

	return &PipelineResolver{pipeline: pipeline}, nil
}

// Release resolver
func (r *ThreadResolver) Release(ctx context.Context) (*ReleaseResolver, error) {
	if r.thread.ReleaseID == nil {
		return nil, nil
	}

	release, err := loadRelease(ctx, *r.thread.ReleaseID)
	if err != nil {
		return nil, err
	}

	return &ReleaseResolver{release: release}, nil
}

// Comments resolver
func (r *ThreadResolver) Comments(ctx context.Context) ([]*CommentResolver, error) {
	resolvers := []*CommentResolver{}

	commentService := getCommentService(ctx)
	sort := db.CommentSortableFieldCreatedAtAsc
	comments, err := commentService.GetComments(ctx, &comment.GetCommentsInput{
		Sort:     &sort,
		ThreadID: r.thread.Metadata.ID,
	})
	if err != nil {
		return nil, err
	}

	for _, comment := range comments.Comments {
		copyOfComment := comment
		resolvers = append(resolvers, &CommentResolver{comment: &copyOfComment})
	}

	return resolvers, nil
}

// CommentCount resolver
func (r *ThreadResolver) CommentCount(ctx context.Context) (int32, error) {
	commentService := getCommentService(ctx)
	comments, err := commentService.GetComments(ctx, &comment.GetCommentsInput{
		ThreadID: r.thread.Metadata.ID,
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return 0, err
	}

	return comments.PageInfo.TotalCount, nil
}

// RootComment resolver
func (r *ThreadResolver) RootComment(ctx context.Context) (*CommentResolver, error) {
	commentService := getCommentService(ctx)
	sort := db.CommentSortableFieldCreatedAtAsc
	comments, err := commentService.GetComments(ctx, &comment.GetCommentsInput{
		Sort:     &sort,
		ThreadID: r.thread.Metadata.ID,
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
	})
	if err != nil {
		return nil, err
	}

	if len(comments.Comments) == 0 {
		// Should not happen since a root comment always exists if a thread exists (or vice-versa).
		return nil, errors.New("failed to get thread root comment")
	}

	return &CommentResolver{comment: &comments.Comments[0]}, nil
}

/* Thread loader */

const threadLoaderKey = "thread"

// RegisterThreadLoader registers a thread loader function
func RegisterThreadLoader(collection *loader.Collection) {
	collection.Register(threadLoaderKey, threadBatchFunc)
}

func loadThread(ctx context.Context, id string) (*models.Thread, error) {
	ldr, err := loader.Extract(ctx, threadLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	thread, ok := data.(models.Thread)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &thread, nil
}

func threadBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	threads, err := getCommentService(ctx).GetThreadsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range threads {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
