package resolver

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"

	"github.com/aws/smithy-go/ptr"
	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
)

/* AgentSession Query Resolvers */

// AgentSessionEdgeResolver resolves session edges
type AgentSessionEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *AgentSessionEdgeResolver) Cursor() (string, error) {
	session, ok := r.edge.Node.(models.AgentSession)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&session)
	return *cursor, err
}

// Node returns a session node
func (r *AgentSessionEdgeResolver) Node() (*AgentSessionResolver, error) {
	session, ok := r.edge.Node.(models.AgentSession)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &AgentSessionResolver{session: &session}, nil
}

// AgentSessionConnectionResolver resolves a session connection
type AgentSessionConnectionResolver struct {
	connection Connection
}

// NewAgentSessionConnectionResolver creates a new AgentSessionConnectionResolver
func NewAgentSessionConnectionResolver(ctx context.Context, input *agent.GetAgentSessionsInput) (*AgentSessionConnectionResolver, error) {
	service := getAgentService(ctx)

	result, err := service.GetAgentSessions(ctx, input)
	if err != nil {
		return nil, err
	}

	sessions := result.AgentSessions

	// Create edges
	edges := make([]Edge, len(sessions))
	for i, session := range sessions {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: session}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(sessions) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&sessions[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&sessions[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &AgentSessionConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *AgentSessionConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *AgentSessionConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *AgentSessionConnectionResolver) Edges() *[]*AgentSessionEdgeResolver {
	resolvers := make([]*AgentSessionEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &AgentSessionEdgeResolver{edge: edge}
	}
	return &resolvers
}

// AgentSessionResolver resolves a session resource
type AgentSessionResolver struct {
	session *models.AgentSession
}

// ID resolver
func (r *AgentSessionResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.AgentSessionType, r.session.Metadata.ID))
}

// Metadata resolver
func (r *AgentSessionResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.session.Metadata}
}

// LastContacted resolver
func (r *AgentSessionResolver) LastContacted() graphql.Time {
	return graphql.Time{Time: r.session.LastContactTimestamp}
}

// Active resolver
func (r *AgentSessionResolver) Active() bool {
	return r.session.Active()
}

// Internal resolver
func (r *AgentSessionResolver) Internal() bool {
	return r.session.Internal
}

// ErrorCount resolver
func (r *AgentSessionResolver) ErrorCount() int32 {
	return int32(r.session.ErrorCount)
}

// Agent resolver
func (r *AgentSessionResolver) Agent(ctx context.Context) (*AgentResolver, error) {
	agent, err := loadAgent(ctx, r.session.AgentID)
	if err != nil {
		return nil, err
	}
	return &AgentResolver{agent: agent}, nil
}

// ErrorLog resolver
func (r *AgentSessionResolver) ErrorLog(ctx context.Context) (*AgentSessionErrorLogResolver, error) {
	logStream, err := loadAgentSessionLogStream(ctx, r.session.Metadata.ID)
	if err != nil {
		return nil, err
	}

	return &AgentSessionErrorLogResolver{stream: logStream}, nil
}

// AgentSessionErrorLogResolver resolves a session error log
type AgentSessionErrorLogResolver struct {
	stream *models.LogStream
}

// LastUpdatedAt resolver
func (r *AgentSessionErrorLogResolver) LastUpdatedAt() (*graphql.Time, error) {
	return &graphql.Time{Time: *r.stream.Metadata.LastUpdatedTimestamp}, nil
}

// Size resolver
func (r *AgentSessionErrorLogResolver) Size() (int32, error) {
	return int32(r.stream.Size), nil
}

// Data resolver
func (r *AgentSessionErrorLogResolver) Data(ctx context.Context, args *JobLogsQueryArgs) (string, error) {
	buffer, err := getAgentService(ctx).ReadAgentSessionErrorLog(ctx, *r.stream.AgentSessionID, int(args.StartOffset), int(args.Limit))
	if err != nil {
		return "", err
	}
	return string(buffer), nil
}

/* AgentSession Subscriptions */

// AgentSessionEventResolver resolves a agent session event
type AgentSessionEventResolver struct {
	event *agent.SessionEvent
}

// Action resolves the event action
func (r *AgentSessionEventResolver) Action() string {
	return r.event.Action
}

// AgentSession resolves the agent session
func (r *AgentSessionEventResolver) AgentSession() *AgentSessionResolver {
	return &AgentSessionResolver{session: r.event.AgentSession}
}

// AgentSessionErrorLogEventResolver resolves a session error log event
type AgentSessionErrorLogEventResolver struct {
	event *logstream.LogEvent
}

// Completed resolver
func (j *AgentSessionErrorLogEventResolver) Completed() bool {
	return j.event.Completed
}

// Size resolver
func (j *AgentSessionErrorLogEventResolver) Size() int32 {
	return int32(j.event.Size)
}

// AgentSessionEventSubscriptionInput is the input for subscribing to agent sessions
type AgentSessionEventSubscriptionInput struct {
	OrganizationID *string
	AgentID        *string
	AgentType      *string
}

func (r RootResolver) agentSessionEventsSubscription(ctx context.Context, input *AgentSessionEventSubscriptionInput) (<-chan *AgentSessionEventResolver, error) {
	agentSessionService := getAgentService(ctx)
	urnResolver := getURNResolver(ctx)

	var orgID, agentID *string
	var agentType *models.AgentType

	if input.OrganizationID != nil {
		id, err := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if err != nil {
			return nil, err
		}
		orgID = &id
	}

	if input.AgentID != nil {
		id, err := urnResolver.ResolveResourceID(ctx, *input.AgentID)
		if err != nil {
			return nil, err
		}
		agentID = &id
	}

	if input.AgentType != nil {
		typ := models.AgentType(*input.AgentType)
		agentType = &typ
	}

	events, err := agentSessionService.SubscribeToAgentSessions(ctx, &agent.SubscribeToAgentSessionsInput{
		OrganizationID: orgID,
		AgentID:        agentID,
		AgentType:      agentType,
	})
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *AgentSessionEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &AgentSessionEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

// AgentSessionErrorLogSubscriptionInput is the input for subscribing to agent session error logs
type AgentSessionErrorLogSubscriptionInput struct {
	LastSeenLogSize *int32
	AgentSessionID  string
}

func (r RootResolver) agentSessionErrorLogSubscription(ctx context.Context, input *AgentSessionErrorLogSubscriptionInput) (<-chan *AgentSessionErrorLogEventResolver, error) {
	service := getAgentService(ctx)
	urnResolver := getURNResolver(ctx)

	sessionID, err := urnResolver.ResolveResourceID(ctx, input.AgentSessionID)
	if err != nil {
		return nil, err
	}

	options := &agent.SubscribeToAgentSessionErrorLogInput{
		AgentSessionID: sessionID,
	}
	if input.LastSeenLogSize != nil {
		options.LastSeenLogSize = ptr.Int(int(*input.LastSeenLogSize))
	}

	events, err := service.SubscribeToAgentSessionErrorLog(ctx, options)
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *AgentSessionErrorLogEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &AgentSessionErrorLogEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

/* AgentSessionLogStream loader */

const agentSessionLogStreamLoaderKey = "agentSessionLogStream"

// RegisterAgentSessionLogStreamLoader registers a agentSessionLogStream loader function
func RegisterAgentSessionLogStreamLoader(collection *loader.Collection) {
	collection.Register(agentSessionLogStreamLoaderKey, agentSessionLogStreamBatchFunc)
}

func loadAgentSessionLogStream(ctx context.Context, id string) (*models.LogStream, error) {
	ldr, err := loader.Extract(ctx, agentSessionLogStreamLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	agentSessionLogStream, ok := data.(models.LogStream)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &agentSessionLogStream, nil
}

func agentSessionLogStreamBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	agentSessionLogStreams, err := getAgentService(ctx).GetLogStreamsByAgentSessionIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range agentSessionLogStreams {
		// Use agent session ID as the key since that is the ID which was
		// used to query the data
		batch[*result.AgentSessionID] = result
	}

	return batch, nil
}
