package resolver

import (
	"context"
	"strconv"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// ReleaseLifecycleConnectionQueryArgs are used to query a release lifecycle connection
type ReleaseLifecycleConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search *string
	Scopes *[]models.ScopeType
}

// ReleaseLifecycleEdgeResolver resolves release lifecycle edges
type ReleaseLifecycleEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ReleaseLifecycleEdgeResolver) Cursor() (string, error) {
	releaseLifecycle, ok := r.edge.Node.(*models.ReleaseLifecycle)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(releaseLifecycle)
	return *cursor, err
}

// Node returns a release lifecycle  node
func (r *ReleaseLifecycleEdgeResolver) Node() (*ReleaseLifecycleResolver, error) {
	releaseLifecycle, ok := r.edge.Node.(*models.ReleaseLifecycle)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ReleaseLifecycleResolver{releaseLifecycle: releaseLifecycle}, nil
}

// ReleaseLifecycleConnectionResolver resolves a release lifecycle  connection
type ReleaseLifecycleConnectionResolver struct {
	connection Connection
}

// NewReleaseLifecycleConnectionResolver creates a new ReleaseLifecycleConnectionResolver
func NewReleaseLifecycleConnectionResolver(ctx context.Context, input *releaselifecycle.GetReleaseLifecyclesInput) (*ReleaseLifecycleConnectionResolver, error) {
	releaseLifecycleService := getReleaseLifecycleService(ctx)

	result, err := releaseLifecycleService.GetReleaseLifecycles(ctx, input)
	if err != nil {
		return nil, err
	}

	releaseLifecycles := result.ReleaseLifecycles

	// Create edges
	edges := make([]Edge, len(releaseLifecycles))
	for i, releaseLifecycle := range releaseLifecycles {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: releaseLifecycle}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(releaseLifecycles) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(releaseLifecycles[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(releaseLifecycles[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ReleaseLifecycleConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ReleaseLifecycleConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ReleaseLifecycleConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ReleaseLifecycleConnectionResolver) Edges() *[]*ReleaseLifecycleEdgeResolver {
	resolvers := make([]*ReleaseLifecycleEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ReleaseLifecycleEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ReleaseLifecycleResolver resolves a release lifecycle  resource
type ReleaseLifecycleResolver struct {
	releaseLifecycle *models.ReleaseLifecycle
}

// ID resolver
func (r *ReleaseLifecycleResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ReleaseLifecycleType, r.releaseLifecycle.Metadata.ID))
}

// Metadata resolver
func (r *ReleaseLifecycleResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.releaseLifecycle.Metadata}
}

// CreatedBy resolver
func (r *ReleaseLifecycleResolver) CreatedBy() string {
	return r.releaseLifecycle.CreatedBy
}

// Name resolver
func (r *ReleaseLifecycleResolver) Name() string {
	return r.releaseLifecycle.Name
}

// Scope resolver
func (r *ReleaseLifecycleResolver) Scope() models.ScopeType {
	return r.releaseLifecycle.Scope
}

// OrganizationName resolver
func (r *ReleaseLifecycleResolver) OrganizationName() string {
	return r.releaseLifecycle.GetOrgName()
}

// Organization resolver
func (r *ReleaseLifecycleResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	organization, err := loadOrganization(ctx, r.releaseLifecycle.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Project resolver
func (r *ReleaseLifecycleResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.releaseLifecycle.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.releaseLifecycle.ProjectID)
	if err != nil {
		return nil, err
	}
	return &ProjectResolver{project: project}, nil
}

// LifecycleTemplate resolver
func (r *ReleaseLifecycleResolver) LifecycleTemplate(ctx context.Context) (*LifecycleTemplateResolver, error) {
	lifecycleTemplate, err := loadLifecycleTemplate(ctx, r.releaseLifecycle.LifecycleTemplateID)
	if err != nil {
		return nil, err
	}

	return &LifecycleTemplateResolver{lifecycleTemplate: lifecycleTemplate}, nil
}

// EnvironmentNames resolver
func (r *ReleaseLifecycleResolver) EnvironmentNames() []string {
	return r.releaseLifecycle.EnvironmentNames
}

/* ReleaseLifecycle Mutations */

// ReleaseLifecycleMutationPayload is the response payload for a release lifecycle  mutation
type ReleaseLifecycleMutationPayload struct {
	ClientMutationID *string
	ReleaseLifecycle *models.ReleaseLifecycle
	Problems         []Problem
}

// ReleaseLifecycleMutationPayloadResolver resolves a ReleaseLifecycleMutationPayload
type ReleaseLifecycleMutationPayloadResolver struct {
	ReleaseLifecycleMutationPayload
}

// ReleaseLifecycle field resolver
func (r *ReleaseLifecycleMutationPayloadResolver) ReleaseLifecycle() *ReleaseLifecycleResolver {
	if r.ReleaseLifecycleMutationPayload.ReleaseLifecycle == nil {
		return nil
	}

	return &ReleaseLifecycleResolver{releaseLifecycle: r.ReleaseLifecycleMutationPayload.ReleaseLifecycle}
}

// CreateReleaseLifecycleInput contains the input for creating a new release lifecycle
type CreateReleaseLifecycleInput struct {
	ClientMutationID    *string
	Scope               models.ScopeType
	OrganizationID      *string // depending on scope, either OrgID or ProjectID is required
	ProjectID           *string
	Name                string
	LifecycleTemplateID string
}

// UpdateReleaseLifecycleInput contains the input for updating an existing release lifecycle
type UpdateReleaseLifecycleInput struct {
	ClientMutationID    *string
	Metadata            *MetadataInput
	ID                  string
	LifecycleTemplateID string
}

// DeleteReleaseLifecycleInput contains the input for deleting an existing release lifecycle
type DeleteReleaseLifecycleInput struct {
	ClientMutationID    *string
	Metadata            *MetadataInput
	ID                  string
	LifecycleTemplateID string
}

func handleReleaseLifecycleMutationProblem(e error, clientMutationID *string) (*ReleaseLifecycleMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := ReleaseLifecycleMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ReleaseLifecycleMutationPayloadResolver{ReleaseLifecycleMutationPayload: payload}, nil
}

func createReleaseLifecycleMutation(ctx context.Context,
	input *CreateReleaseLifecycleInput) (*ReleaseLifecycleMutationPayloadResolver, error) {
	lifecycleTemplateID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.LifecycleTemplateID)
	if err != nil {
		return nil, err
	}

	if (input.OrganizationID != nil) && (input.ProjectID != nil) {
		return nil, errors.New("specifying both orgID and project ID is not allowed", errors.WithErrorCode(errors.EInvalid))
	}

	var orgID, projectID *string
	urnResolver := getURNResolver(ctx)
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("organization ID is required for an organization release lifecycle",
				errors.WithErrorCode(errors.EInvalid))
		}

		oID, oErr := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if oErr != nil {
			return nil, oErr
		}

		orgID = &oID
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("project id is required for a project release lifecycle",
				errors.WithErrorCode(errors.EInvalid))
		}

		pID, pErr := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if pErr != nil {
			return nil, pErr
		}

		projectID = &pID
	default:
		return nil, errors.New("release lifecycle must have either organization or project scope",
			errors.WithErrorCode(errors.EInvalid))
	}

	createdReleaseLifecycle, err := getReleaseLifecycleService(ctx).
		CreateReleaseLifecycle(ctx, &releaselifecycle.CreateReleaseLifecycleInput{
			Name:                input.Name,
			Scope:               input.Scope,
			OrganizationID:      orgID,
			ProjectID:           projectID,
			LifecycleTemplateID: lifecycleTemplateID,
		})
	if err != nil {
		return nil, err
	}

	payload := ReleaseLifecycleMutationPayload{
		ClientMutationID: input.ClientMutationID,
		ReleaseLifecycle: createdReleaseLifecycle, Problems: []Problem{},
	}
	return &ReleaseLifecycleMutationPayloadResolver{ReleaseLifecycleMutationPayload: payload}, nil
}

func updateReleaseLifecycleMutation(ctx context.Context,
	input *UpdateReleaseLifecycleInput) (*ReleaseLifecycleMutationPayloadResolver, error) {
	id, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	lifecycleTemplateID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.LifecycleTemplateID)
	if err != nil {
		return nil, err
	}

	// ReleaseLifecycleID identifies the target; lifecycle template ID gets updated
	releaseLifecycleUpdateOptions := &releaselifecycle.UpdateReleaseLifecycleInput{
		ID:                  id,
		LifecycleTemplateID: lifecycleTemplateID,
	}

	if input.Metadata != nil {
		v, vErr := strconv.Atoi(input.Metadata.Version)
		if vErr != nil {
			return nil, vErr
		}
		releaseLifecycleUpdateOptions.Version = &v
	}

	updatedReleaseLifecycle, err := getReleaseLifecycleService(ctx).
		UpdateReleaseLifecycle(ctx, releaseLifecycleUpdateOptions)
	if err != nil {
		return nil, err
	}

	payload := ReleaseLifecycleMutationPayload{
		ClientMutationID: input.ClientMutationID,
		ReleaseLifecycle: updatedReleaseLifecycle,
		Problems:         []Problem{},
	}
	return &ReleaseLifecycleMutationPayloadResolver{ReleaseLifecycleMutationPayload: payload}, nil
}

func deleteReleaseLifecycleMutation(ctx context.Context,
	input *DeleteReleaseLifecycleInput) (*ReleaseLifecycleMutationPayloadResolver, error) {
	id, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	options := &releaselifecycle.DeleteReleaseLifecycleInput{
		ID: id,
	}

	if input.Metadata != nil {
		v, vErr := strconv.Atoi(input.Metadata.Version)
		if vErr != nil {
			return nil, vErr
		}
		options.Version = &v
	}

	service := getReleaseLifecycleService(ctx)

	lifecycle, err := service.GetReleaseLifecycleByID(ctx, id)
	if err != nil {
		return nil, err
	}

	if err := service.DeleteReleaseLifecycle(ctx, options); err != nil {
		return nil, err
	}

	payload := ReleaseLifecycleMutationPayload{
		ClientMutationID: input.ClientMutationID,
		ReleaseLifecycle: lifecycle,
		Problems:         []Problem{},
	}
	return &ReleaseLifecycleMutationPayloadResolver{ReleaseLifecycleMutationPayload: payload}, nil
}

/* ReleaseLifecycle loader */

const releaseLifecycleLoaderKey = "releaseLifecycle"

// RegisterReleaseLifecycleLoader registers a release lifecycle  loader function
func RegisterReleaseLifecycleLoader(collection *loader.Collection) {
	collection.Register(releaseLifecycleLoaderKey, releaseLifecycleBatchFunc)
}

func loadReleaseLifecycle(ctx context.Context, id string) (*models.ReleaseLifecycle, error) {
	ldr, err := loader.Extract(ctx, releaseLifecycleLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	releaseLifecycle, ok := data.(*models.ReleaseLifecycle)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return releaseLifecycle, nil
}

func releaseLifecycleBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	releaseLifecycles, err := getReleaseLifecycleService(ctx).GetReleaseLifecyclesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range releaseLifecycles {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
