package resolver

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
)

/* EnvironmentRule Query Resolvers */

// EnvironmentRuleEdgeResolver resolves environmentRule edges
type EnvironmentRuleEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *EnvironmentRuleEdgeResolver) Cursor() (string, error) {
	environmentRule, ok := r.edge.Node.(*models.EnvironmentRule)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(environmentRule)
	return *cursor, err
}

// Node returns a environment protection rule node
func (r *EnvironmentRuleEdgeResolver) Node() (*EnvironmentRuleResolver, error) {
	environmentRule, ok := r.edge.Node.(*models.EnvironmentRule)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &EnvironmentRuleResolver{rule: environmentRule}, nil
}

// EnvironmentRuleConnectionResolver resolves a environmentRule connection
type EnvironmentRuleConnectionResolver struct {
	connection Connection
}

// NewEnvironmentRuleConnectionResolver creates a new EnvironmentRuleConnectionResolver
func NewEnvironmentRuleConnectionResolver(ctx context.Context, input *environment.GetEnvironmentRulesInput) (*EnvironmentRuleConnectionResolver, error) {
	environmentService := getEnvironmentService(ctx)

	result, err := environmentService.GetEnvironmentRules(ctx, input)
	if err != nil {
		return nil, err
	}

	rules := result.EnvironmentRules

	// Create edges
	edges := make([]Edge, len(rules))
	for i, environmentRule := range rules {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: environmentRule}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(rules) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(rules[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(rules[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &EnvironmentRuleConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *EnvironmentRuleConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *EnvironmentRuleConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *EnvironmentRuleConnectionResolver) Edges() *[]*EnvironmentRuleEdgeResolver {
	resolvers := make([]*EnvironmentRuleEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &EnvironmentRuleEdgeResolver{edge: edge}
	}
	return &resolvers
}

// EnvironmentRuleResolver resolves a environment protection rule access rule
type EnvironmentRuleResolver struct {
	rule *models.EnvironmentRule
}

// ID resolver
func (r *EnvironmentRuleResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.EnvironmentRuleType, r.rule.Metadata.ID))
}

// Metadata resolver
func (r *EnvironmentRuleResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.rule.Metadata}
}

// CreatedBy resolver
func (r *EnvironmentRuleResolver) CreatedBy() string {
	return r.rule.CreatedBy
}

// Scope resolver
func (r *EnvironmentRuleResolver) Scope() models.ScopeType {
	return r.rule.Scope
}

// Organization resolver
func (r *EnvironmentRuleResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	org, err := loadOrganization(ctx, r.rule.OrgID)
	if err != nil {
		return nil, err
	}
	return &OrganizationResolver{organization: org}, nil
}

// Project resolver
func (r *EnvironmentRuleResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.rule.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.rule.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// OrganizationName resolver
func (r *EnvironmentRuleResolver) OrganizationName(ctx context.Context) (string, error) {
	org, err := loadOrganization(ctx, r.rule.OrgID)
	if err != nil {
		return "", err
	}
	return org.Name, nil
}

// ProjectName resolver
func (r *EnvironmentRuleResolver) ProjectName(ctx context.Context) (*string, error) {
	if r.rule.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.rule.ProjectID)
	if err != nil {
		return nil, err
	}

	return &project.Name, nil
}

// EnvironmentName resolver
func (r *EnvironmentRuleResolver) EnvironmentName() string {
	return r.rule.EnvironmentName
}

// Users resolver
func (r *EnvironmentRuleResolver) Users(ctx context.Context) ([]*UserResolver, error) {
	resolvers := []*UserResolver{}

	for _, userID := range r.rule.UserIDs {
		user, err := loadUser(ctx, userID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &UserResolver{user: user})
	}

	return resolvers, nil
}

// ServiceAccounts resolver
func (r *EnvironmentRuleResolver) ServiceAccounts(ctx context.Context) ([]*ServiceAccountResolver, error) {
	resolvers := []*ServiceAccountResolver{}

	for _, serviceAccountID := range r.rule.ServiceAccountIDs {
		sa, err := loadServiceAccount(ctx, serviceAccountID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &ServiceAccountResolver{serviceAccount: sa})
	}

	return resolvers, nil
}

// Teams resolver
func (r *EnvironmentRuleResolver) Teams(ctx context.Context) ([]*TeamResolver, error) {
	resolvers := []*TeamResolver{}

	for _, teamID := range r.rule.TeamIDs {
		team, err := loadTeam(ctx, teamID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &TeamResolver{team: team})
	}

	return resolvers, nil
}

// Roles resolver
func (r *EnvironmentRuleResolver) Roles(ctx context.Context) ([]*RoleResolver, error) {
	resolvers := []*RoleResolver{}

	for _, roleID := range r.rule.RoleIDs {
		role, err := loadRole(ctx, roleID)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, &RoleResolver{role: role})
	}

	return resolvers, nil
}

/* EnvironmentRule Mutation Resolvers */

// EnvironmentRuleMutationPayload is the response payload for a environmentRule mutation
type EnvironmentRuleMutationPayload struct {
	ClientMutationID *string
	EnvironmentRule  *models.EnvironmentRule
	Problems         []Problem
}

// EnvironmentRuleMutationPayloadResolver resolves a EnvironmentRuleMutationPayload
type EnvironmentRuleMutationPayloadResolver struct {
	EnvironmentRuleMutationPayload
}

// EnvironmentRule field resolver
func (r *EnvironmentRuleMutationPayloadResolver) EnvironmentRule() *EnvironmentRuleResolver {
	if r.EnvironmentRuleMutationPayload.EnvironmentRule == nil {
		return nil
	}
	return &EnvironmentRuleResolver{rule: r.EnvironmentRuleMutationPayload.EnvironmentRule}
}

// CreateEnvironmentRuleInput is the input for creating a new access rule
type CreateEnvironmentRuleInput struct {
	ClientMutationID *string
	Scope            models.ScopeType
	OrgID            *string // depending on scope, either OrgID or ProjectID is required
	ProjectID        *string
	EnvironmentName  string
	Users            []string
	Teams            []string
	ServiceAccounts  []string
	Roles            []string
}

// UpdateEnvironmentRuleInput is the input for updating an existing access rule
type UpdateEnvironmentRuleInput struct {
	Metadata         *MetadataInput
	ClientMutationID *string
	Users            *[]string
	Teams            *[]string
	ServiceAccounts  *[]string
	Roles            *[]string
	ID               string
}

// DeleteEnvironmentRuleInput is the input for deleting an access rule
type DeleteEnvironmentRuleInput struct {
	Metadata         *MetadataInput
	ClientMutationID *string
	ID               string
}

func handleEnvironmentRuleMutationProblem(e error, clientMutationID *string) (*EnvironmentRuleMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := EnvironmentRuleMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &EnvironmentRuleMutationPayloadResolver{EnvironmentRuleMutationPayload: payload}, nil
}

func createEnvironmentRuleMutation(ctx context.Context, input *CreateEnvironmentRuleInput) (*EnvironmentRuleMutationPayloadResolver, error) {
	var allowedUserIDs, allowedServiceAccountIDs, allowedTeamIDs, allowedRoleIDs []string
	var err error

	urnResolver := getURNResolver(ctx)

	allowedUserIDs, err = convertPRNList(ctx, input.Users)
	if err != nil {
		return nil, err
	}

	allowedServiceAccountIDs, err = convertPRNList(ctx, input.ServiceAccounts)
	if err != nil {
		return nil, err
	}

	allowedTeamIDs, err = convertPRNList(ctx, input.Teams)
	if err != nil {
		return nil, err
	}

	allowedRoleIDs, err = convertPRNList(ctx, input.Roles)
	if err != nil {
		return nil, err
	}

	if (input.OrgID != nil) && (input.ProjectID != nil) {
		return nil, errors.New("specifying both orgID and project ID is not allowed", errors.WithErrorCode(errors.EInvalid))
	}

	var createdRule *models.EnvironmentRule
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrgID == nil {
			return nil, errors.New("organization ID is required for an organization environment protection rule",
				errors.WithErrorCode(errors.EInvalid))
		}

		orgID, err := urnResolver.ResolveResourceID(ctx, *input.OrgID)
		if err != nil {
			return nil, err
		}

		createdRule, err = getEnvironmentService(ctx).CreateOrganizationEnvironmentRule(ctx,
			&environment.CreateOrganizationEnvironmentRuleInput{
				OrganizationID:    orgID,
				EnvironmentName:   input.EnvironmentName,
				UserIDs:           allowedUserIDs,
				ServiceAccountIDs: allowedServiceAccountIDs,
				TeamIDs:           allowedTeamIDs,
				RoleIDs:           allowedRoleIDs,
			})
		if err != nil {
			return nil, err
		}

	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("project id is required for a project environment protection rule",
				errors.WithErrorCode(errors.EInvalid))
		}

		projectID, err := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if err != nil {
			return nil, err
		}

		createdRule, err = getEnvironmentService(ctx).CreateProjectEnvironmentRule(ctx,
			&environment.CreateProjectEnvironmentRuleInput{
				ProjectID:         projectID,
				EnvironmentName:   input.EnvironmentName,
				UserIDs:           allowedUserIDs,
				ServiceAccountIDs: allowedServiceAccountIDs,
				TeamIDs:           allowedTeamIDs,
				RoleIDs:           allowedRoleIDs,
			})
		if err != nil {
			return nil, err
		}

	default:
		return nil, errors.New("environment protection rule must have either organization or project scope",
			errors.WithErrorCode(errors.EInvalid))
	}

	payload := EnvironmentRuleMutationPayload{
		ClientMutationID: input.ClientMutationID,
		EnvironmentRule:  createdRule,
		Problems:         []Problem{},
	}
	return &EnvironmentRuleMutationPayloadResolver{EnvironmentRuleMutationPayload: payload}, nil
}

func updateEnvironmentRuleMutation(ctx context.Context, input *UpdateEnvironmentRuleInput) (*EnvironmentRuleMutationPayloadResolver, error) {
	var err error

	ruleID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	updateRuleOptions := environment.UpdateEnvironmentRuleInput{
		ID: ruleID,
	}

	if input.Users != nil {
		updateRuleOptions.UserIDs, err = convertPRNList(ctx, *input.Users)
		if err != nil {
			return nil, err
		}
	}

	if input.ServiceAccounts != nil {
		updateRuleOptions.ServiceAccountIDs, err = convertPRNList(ctx, *input.ServiceAccounts)
		if err != nil {
			return nil, err
		}
	}

	if input.Teams != nil {
		updateRuleOptions.TeamIDs, err = convertPRNList(ctx, *input.Teams)
		if err != nil {
			return nil, err
		}
	}

	if input.Roles != nil {
		updateRuleOptions.RoleIDs, err = convertPRNList(ctx, *input.Roles)
		if err != nil {
			return nil, err
		}
	}

	if input.Metadata != nil {
		v, vErr := strconv.Atoi(input.Metadata.Version)
		if vErr != nil {
			return nil, vErr
		}
		updateRuleOptions.Version = &v
	}

	updatedRule, err := getEnvironmentService(ctx).UpdateEnvironmentRule(ctx, &updateRuleOptions)
	if err != nil {
		return nil, err
	}

	payload := EnvironmentRuleMutationPayload{
		ClientMutationID: input.ClientMutationID,
		EnvironmentRule:  updatedRule,
		Problems:         []Problem{},
	}
	return &EnvironmentRuleMutationPayloadResolver{EnvironmentRuleMutationPayload: payload}, nil
}

func deleteEnvironmentRuleMutation(ctx context.Context, input *DeleteEnvironmentRuleInput) (*EnvironmentRuleMutationPayloadResolver, error) {
	ruleID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	service := getEnvironmentService(ctx)

	rule, err := service.GetEnvironmentRuleByID(ctx, ruleID)
	if err != nil {
		return nil, err
	}

	deleteOptions := &environment.DeleteEnvironmentRuleInput{
		ID: ruleID,
	}

	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}
		deleteOptions.Version = &v
	}

	if err := service.DeleteEnvironmentRule(ctx, deleteOptions); err != nil {
		return nil, err
	}

	payload := EnvironmentRuleMutationPayload{
		ClientMutationID: input.ClientMutationID,
		EnvironmentRule:  rule,
		Problems:         []Problem{},
	}
	return &EnvironmentRuleMutationPayloadResolver{EnvironmentRuleMutationPayload: payload}, nil
}

/* EnvironmentRule loader */

const environmentRuleLoaderKey = "environmentRule"

// RegisterEnvironmentRuleLoader registers a environmentRule loader function
func RegisterEnvironmentRuleLoader(collection *loader.Collection) {
	collection.Register(environmentRuleLoaderKey, environmentRuleBatchFunc)
}

func loadEnvironmentRule(ctx context.Context, id string) (*models.EnvironmentRule, error) {
	ldr, err := loader.Extract(ctx, environmentRuleLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	environmentRule, ok := data.(*models.EnvironmentRule)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return environmentRule, nil
}

func environmentRuleBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	rules, err := getEnvironmentService(ctx).GetEnvironmentRulesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range rules {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
