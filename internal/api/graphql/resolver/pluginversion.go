package resolver

import (
	"context"
	"strconv"

	"github.com/graph-gophers/dataloader"
	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

/* PluginVersion Query resolvers */

// PluginVersionEdgeResolver resolves plugin version edges
type PluginVersionEdgeResolver struct {
	edge Edge
}

// Cursor returns the cursor
func (r *PluginVersionEdgeResolver) Cursor() (string, error) {
	pluginVersion, ok := r.edge.Node.(models.PluginVersion)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&pluginVersion)
	return *cursor, err
}

// Node returns the node
func (r *PluginVersionEdgeResolver) Node() (*PluginVersionResolver, error) {
	pluginVersion, ok := r.edge.Node.(models.PluginVersion)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &PluginVersionResolver{pluginVersion: &pluginVersion}, nil
}

// PluginVersionConnectionResolver resolves plugin version connections
type PluginVersionConnectionResolver struct {
	connection Connection
}

// NewPluginVersionConnectionResolver creates a new plugin version connection resolver
func NewPluginVersionConnectionResolver(ctx context.Context, input *pluginregistry.GetPluginVersionsInput) (*PluginVersionConnectionResolver, error) {
	result, err := getPluginRegistryService(ctx).GetPluginVersions(ctx, input)
	if err != nil {
		return nil, err
	}

	pluginVersions := result.PluginVersions

	// Create edges
	edges := make([]Edge, len(pluginVersions))
	for i, pluginVersion := range pluginVersions {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: pluginVersion}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(pluginVersions) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&pluginVersions[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&pluginVersions[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &PluginVersionConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total count
func (r *PluginVersionConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the page info
func (r *PluginVersionConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the edges
func (r *PluginVersionConnectionResolver) Edges() *[]*PluginVersionEdgeResolver {
	resolvers := make([]*PluginVersionEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &PluginVersionEdgeResolver{edge: edge}
	}
	return &resolvers
}

// PluginVersionDocFileMetadataResolver resolves a plugin version doc file metadata
type PluginVersionDocFileMetadataResolver struct {
	docMeta *models.PluginVersionDocFileMetadata
}

// Title resolver
func (r *PluginVersionDocFileMetadataResolver) Title() string {
	return r.docMeta.Title
}

// Category resolver
func (r *PluginVersionDocFileMetadataResolver) Category() string {
	return string(r.docMeta.Category)
}

// Subcategory resolver
func (r *PluginVersionDocFileMetadataResolver) Subcategory() string {
	return r.docMeta.Subcategory
}

// Name resolver
func (r *PluginVersionDocFileMetadataResolver) Name() string {
	return r.docMeta.Name
}

// FilePath resolver
func (r *PluginVersionDocFileMetadataResolver) FilePath() string {
	return r.docMeta.FilePath
}

// PluginVersionResolver resolves a plugin version
type PluginVersionResolver struct {
	pluginVersion *models.PluginVersion
}

// ID resolver
func (r *PluginVersionResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.PluginVersionType, r.pluginVersion.Metadata.ID))
}

// Metadata resolver
func (r *PluginVersionResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.pluginVersion.Metadata}
}

// Version resolver
func (r *PluginVersionResolver) Version() string {
	return r.pluginVersion.SemanticVersion
}

// CreatedBy resolver
func (r *PluginVersionResolver) CreatedBy() string {
	return r.pluginVersion.CreatedBy
}

// Protocols resolver
func (r *PluginVersionResolver) Protocols() []string {
	return r.pluginVersion.Protocols
}

// SHASumsUploaded resolver
func (r *PluginVersionResolver) SHASumsUploaded() bool {
	return r.pluginVersion.SHASumsUploaded
}

// ReadmeUploaded resolver
func (r *PluginVersionResolver) ReadmeUploaded() bool {
	return r.pluginVersion.ReadmeUploaded
}

// DocFiles resolver
func (r *PluginVersionResolver) DocFiles() []*PluginVersionDocFileMetadataResolver {
	docFiles := make([]*PluginVersionDocFileMetadataResolver, 0, len(r.pluginVersion.DocFiles))
	for _, docMeta := range r.pluginVersion.DocFiles {
		docFiles = append(docFiles, &PluginVersionDocFileMetadataResolver{docMeta: docMeta})
	}
	return docFiles
}

// Latest resolver
func (r *PluginVersionResolver) Latest() bool {
	return r.pluginVersion.Latest
}

// Readme resolver
func (r *PluginVersionResolver) Readme(ctx context.Context) (string, error) {
	if r.pluginVersion.ReadmeUploaded {
		return getPluginRegistryService(ctx).GetPluginVersionReadme(ctx, r.pluginVersion)
	}
	return "", nil
}

// Plugin resolver
func (r *PluginVersionResolver) Plugin(ctx context.Context) (*PluginResolver, error) {
	plugin, err := loadPlugin(ctx, r.pluginVersion.PluginID)
	if err != nil {
		return nil, err
	}

	return &PluginResolver{plugin: plugin}, nil
}

// Platforms resolver
func (r *PluginVersionResolver) Platforms(ctx context.Context) ([]*PluginPlatformResolver, error) {
	response, err := getPluginRegistryService(ctx).GetPluginPlatforms(ctx, &pluginregistry.GetPluginPlatformsInput{
		PluginVersionID: &r.pluginVersion.Metadata.ID,
	})
	if err != nil {
		return nil, err
	}

	resolvers := make([]*PluginPlatformResolver, len(response.PluginPlatforms))
	for i, platform := range response.PluginPlatforms {
		platformCopy := platform
		resolvers[i] = &PluginPlatformResolver{pluginPlatform: &platformCopy}
	}

	return resolvers, nil
}

// PluginDocFileQueryArgs are the arguments for querying a plugin doc file
type PluginDocFileQueryArgs struct {
	PluginVersionID string
	FilePath        string
}

func pluginDocFileQuery(ctx context.Context, args *PluginDocFileQueryArgs) (*string, error) {
	pluginVersionID, err := getURNResolver(ctx).ResolveResourceID(ctx, args.PluginVersionID)
	if err != nil {
		return nil, err
	}

	service := getPluginRegistryService(ctx)
	contents, err := service.GetPluginVersionDocFile(ctx, &pluginregistry.GetPluginVersionDocFileInput{
		PluginVersionID: pluginVersionID,
		FilePath:        args.FilePath,
	})
	if err != nil {
		return nil, err
	}
	return &contents, nil
}

/* PluginVersion Mutation resolvers */

// PluginVersionMutationPayload is the payload for plugin version mutations
type PluginVersionMutationPayload struct {
	ClientMutationID *string
	PluginVersion    *models.PluginVersion
	Problems         []Problem
}

// PluginVersionMutationPayloadResolver resolves plugin version mutation payload
type PluginVersionMutationPayloadResolver struct {
	PluginVersionMutationPayload
}

// PluginVersion field resolver
func (r *PluginVersionMutationPayloadResolver) PluginVersion() *PluginVersionResolver {
	if r.PluginVersionMutationPayload.PluginVersion == nil {
		return nil
	}
	return &PluginVersionResolver{pluginVersion: r.PluginVersionMutationPayload.PluginVersion}
}

// DeletePluginVersionInput is the input for deleting a plugin version
type DeletePluginVersionInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handlePluginVersionMutationProblem(e error, clientMutationID *string) (*PluginVersionMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := PluginVersionMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &PluginVersionMutationPayloadResolver{PluginVersionMutationPayload: payload}, nil
}

func deletePluginVersionMutation(ctx context.Context, input *DeletePluginVersionInput) (*PluginVersionMutationPayloadResolver, error) {
	pluginRegistryService := getPluginRegistryService(ctx)

	pluginVersionID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	pluginVersion, err := pluginRegistryService.GetPluginVersionByID(ctx, pluginVersionID)
	if err != nil {
		return nil, err
	}

	toDelete := &pluginregistry.DeletePluginVersionInput{
		ID: pluginVersionID,
	}

	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toDelete.Version = &v
	}

	if err = pluginRegistryService.DeletePluginVersion(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := PluginVersionMutationPayload{ClientMutationID: input.ClientMutationID, PluginVersion: pluginVersion, Problems: []Problem{}}
	return &PluginVersionMutationPayloadResolver{PluginVersionMutationPayload: payload}, nil
}

/* PluginVersion loader */

const pluginVersionLoaderKey = "pluginVersionLoader"

// RegisterPluginVersionLoader registers a plugin version loader
func RegisterPluginVersionLoader(collection *loader.Collection) {
	collection.Register(pluginVersionLoaderKey, pluginVersionBatchFunc)
}

func loadPluginVersion(ctx context.Context, id string) (*models.PluginVersion, error) {
	ldr, err := loader.Extract(ctx, pluginVersionLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	pluginVersion, ok := data.(models.PluginVersion)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &pluginVersion, nil
}

func pluginVersionBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	pluginVersions, err := getPluginRegistryService(ctx).GetPluginVersionsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range pluginVersions {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
