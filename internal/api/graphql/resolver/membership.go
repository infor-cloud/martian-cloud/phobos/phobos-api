package resolver

import (
	"context"
	"strconv"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

/* Membership Query Resolvers */

// MembershipConnectionQueryArgs are the arguments for the memberships query.
type MembershipConnectionQueryArgs struct {
	ConnectionQueryArgs
	Scope *models.ScopeType
}

// MembershipEdgeResolver resolves membership edges
type MembershipEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *MembershipEdgeResolver) Cursor() (string, error) {
	membership, ok := r.edge.Node.(models.Membership)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&membership)
	return *cursor, err
}

// Node returns a membership node
func (r *MembershipEdgeResolver) Node() (*MembershipResolver, error) {
	membership, ok := r.edge.Node.(models.Membership)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &MembershipResolver{membership: &membership}, nil
}

// MembershipConnectionResolver resolves a membership connection
type MembershipConnectionResolver struct {
	connection Connection
}

// NewMembershipConnectionResolver creates a new MembershipConnectionResolver
func NewMembershipConnectionResolver(ctx context.Context,
	input *membership.GetMembershipsInput,
) (*MembershipConnectionResolver, error) {
	service := getMembershipService(ctx)

	result, err := service.GetMemberships(ctx, input)
	if err != nil {
		return nil, err
	}

	memberships := result.Memberships

	// Create edges
	edges := make([]Edge, len(memberships))
	for i, membership := range memberships {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: membership}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(memberships) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&memberships[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&memberships[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &MembershipConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *MembershipConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *MembershipConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *MembershipConnectionResolver) Edges() *[]*MembershipEdgeResolver {
	resolvers := make([]*MembershipEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &MembershipEdgeResolver{edge: edge}
	}
	return &resolvers
}

// MemberResolver results the Member union type
type MemberResolver struct {
	result interface{}
}

// ToUser resolves user member types
func (r *MemberResolver) ToUser() (*UserResolver, bool) {
	res, ok := r.result.(*UserResolver)
	return res, ok
}

// ToTeam resolves team member types
func (r *MemberResolver) ToTeam() (*TeamResolver, bool) {
	res, ok := r.result.(*TeamResolver)
	return res, ok
}

// ToServiceAccount resolves service account member types
func (r *MemberResolver) ToServiceAccount() (*ServiceAccountResolver, bool) {
	res, ok := r.result.(*ServiceAccountResolver)
	return res, ok
}

// MembershipResolver resolves a membership resource
type MembershipResolver struct {
	membership *models.Membership
}

// ID resolver
func (r *MembershipResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.MembershipType, r.membership.Metadata.ID))
}

// Member resolver
func (r *MembershipResolver) Member(ctx context.Context) (*MemberResolver, error) {
	// Query for member based on type
	return makeMemberResolver(ctx, r.membership.UserID, r.membership.TeamID, r.membership.ServiceAccountID)
}

func makeMemberResolver(ctx context.Context, userID, teamID, serviceAccountID *string) (*MemberResolver, error) {
	if userID != nil {
		// Use resource loader to get user
		user, err := loadUser(ctx, *userID)
		if err != nil {
			return nil, err
		}
		return &MemberResolver{result: &UserResolver{user: user}}, nil
	}

	if teamID != nil {
		// Use resource loader to get team
		team, err := loadTeam(ctx, *teamID)
		if err != nil {
			return nil, err
		}
		return &MemberResolver{result: &TeamResolver{team: team}}, nil
	}

	if serviceAccountID != nil {
		sa, err := loadServiceAccount(ctx, *serviceAccountID)
		if err != nil {
			return nil, err
		}
		return &MemberResolver{result: &ServiceAccountResolver{serviceAccount: sa}}, nil
	}

	return nil, errors.New("UserID, TeamID or ServiceAccountID must be specified", errors.WithErrorCode(errors.EInvalid))
}

// Organization resolver
func (r *MembershipResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	if r.membership.OrganizationID == nil {
		return nil, nil
	}

	org, err := loadOrganization(ctx, *r.membership.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: org}, nil
}

// Project resolver.
func (r *MembershipResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.membership.ProjectID == nil {
		return nil, nil
	}

	proj, err := loadProject(ctx, *r.membership.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: proj}, nil
}

// Role resolver
func (r *MembershipResolver) Role(ctx context.Context) (*RoleResolver, error) {
	role, err := loadRole(ctx, r.membership.RoleID)
	if err != nil {
		return nil, err
	}

	return &RoleResolver{role: role}, nil
}

// Metadata resolver
func (r *MembershipResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.membership.Metadata}
}

// Scope resolver
func (r *MembershipResolver) Scope() models.ScopeType {
	return r.membership.Scope
}

func membershipsQuery(ctx context.Context, args *MembershipConnectionQueryArgs) (*MembershipConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := membership.GetMembershipsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
	}

	if args.Scope != nil {
		input.MembershipScopes = []models.ScopeType{*args.Scope}
	}

	if args.Sort != nil {
		sort := db.MembershipSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewMembershipConnectionResolver(ctx, &input)
}

// MembershipMutationPayload is the response payload for a membership mutation
type MembershipMutationPayload struct {
	ClientMutationID *string
	Membership       *models.Membership
	Problems         []Problem
}

// MembershipMutationPayloadResolver resolves an MembershipMutationPayload
type MembershipMutationPayloadResolver struct {
	MembershipMutationPayload
}

// Membership field resolver
func (r *MembershipMutationPayloadResolver) Membership() (*MembershipResolver, error) {
	if r.MembershipMutationPayload.Membership == nil {
		return nil, nil
	}

	return &MembershipResolver{membership: r.MembershipMutationPayload.Membership}, nil
}

// CreateMembershipInput is the input for creating a new membership
type CreateMembershipInput struct {
	ClientMutationID *string
	Username         *string
	TeamName         *string
	ServiceAccountID *string
	OrganizationID   *string
	ProjectID        *string
	Role             string
	Scope            models.ScopeType
}

// UpdateMembershipInput is the input for updating a membership
type UpdateMembershipInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
	Role             string
}

// DeleteMembershipInput is the input for deleting a membership
type DeleteMembershipInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handleMembershipMutationProblem(e error, clientMutationID *string) (*MembershipMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}

	payload := MembershipMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &MembershipMutationPayloadResolver{MembershipMutationPayload: payload}, nil
}

func createMembershipMutation(ctx context.Context,
	input *CreateMembershipInput,
) (*MembershipMutationPayloadResolver, error) {
	membershipService := getMembershipService(ctx)
	urnResolver := getURNResolver(ctx)

	role, err := getRoleService(ctx).GetRoleByName(ctx, input.Role)
	if err != nil {
		return nil, err
	}

	var (
		user *models.User
		team *models.Team
		sa   *models.ServiceAccount
	)

	if input.Username != nil {
		user, err = getUserService(ctx).GetUserByUsername(ctx, *input.Username)
		if err != nil {
			return nil, err
		}
	}

	if input.TeamName != nil {
		team, err = getTeamService(ctx).GetTeamByPRN(ctx, models.TeamResource.BuildPRN(*input.TeamName))
		if err != nil {
			return nil, err
		}
	}

	if input.ServiceAccountID != nil {
		serviceAccountID, rErr := urnResolver.ResolveResourceID(ctx, *input.ServiceAccountID)
		if rErr != nil {
			return nil, rErr
		}

		sa, err = getServiceAccountService(ctx).GetServiceAccountByID(ctx, serviceAccountID)
		if err != nil {
			return nil, err
		}
	}

	var createdMembership *models.Membership
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("OrganizationID must be specified for organization membership", errors.WithErrorCode(errors.EInvalid))
		}

		orgID, rErr := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if rErr != nil {
			return nil, rErr
		}

		createOptions := &membership.CreateOrganizationMembershipInput{
			RoleID:         role.Metadata.ID,
			OrganizationID: orgID,
			User:           user,
			Team:           team,
			ServiceAccount: sa,
		}

		createdMembership, err = membershipService.CreateOrganizationMembership(ctx, createOptions)
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("ProjectID must be specified for project membership", errors.WithErrorCode(errors.EInvalid))
		}

		projectID, rErr := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if rErr != nil {
			return nil, rErr
		}

		createOptions := &membership.CreateProjectMembershipInput{
			RoleID:         role.Metadata.ID,
			ProjectID:      projectID,
			User:           user,
			Team:           team,
			ServiceAccount: sa,
		}

		createdMembership, err = membershipService.CreateProjectMembership(ctx, createOptions)
		if err != nil {
			return nil, err
		}

	case models.GlobalScope:
		createOptions := &membership.CreateGlobalMembershipInput{
			RoleID:         role.Metadata.ID,
			User:           user,
			Team:           team,
			ServiceAccount: sa,
		}

		createdMembership, err = membershipService.CreateGlobalMembership(ctx, createOptions)
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unknown membership type %s", input.Scope, errors.WithErrorCode(errors.EInvalid))
	}

	payload := MembershipMutationPayload{
		ClientMutationID: input.ClientMutationID,
		Membership:       createdMembership,
		Problems:         []Problem{},
	}
	return &MembershipMutationPayloadResolver{MembershipMutationPayload: payload}, nil
}

func updateMembershipMutation(ctx context.Context,
	input *UpdateMembershipInput,
) (*MembershipMutationPayloadResolver, error) {
	service := getMembershipService(ctx)
	urnResolver := getURNResolver(ctx)

	role, err := getRoleService(ctx).GetRoleByName(ctx, input.Role)
	if err != nil {
		return nil, err
	}

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &membership.UpdateMembershipInput{
		RoleID: role.Metadata.ID,
		ID:     id,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.Version = &v
	}

	membership, err := service.UpdateMembership(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := MembershipMutationPayload{
		ClientMutationID: input.ClientMutationID,
		Membership:       membership,
		Problems:         []Problem{},
	}
	return &MembershipMutationPayloadResolver{MembershipMutationPayload: payload}, nil
}

func deleteMembershipMutation(ctx context.Context,
	input *DeleteMembershipInput,
) (*MembershipMutationPayloadResolver, error) {
	service := getMembershipService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	gotMembership, err := service.GetMembershipByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &membership.DeleteMembershipInput{
		ID: gotMembership.Metadata.ID,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toDelete.Version = &v
	}

	if err := service.DeleteMembership(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := MembershipMutationPayload{
		ClientMutationID: input.ClientMutationID,
		Membership:       gotMembership,
		Problems:         []Problem{},
	}
	return &MembershipMutationPayloadResolver{MembershipMutationPayload: payload}, nil
}

/* Membership loader */

const membershipLoaderKey = "membership"

// RegisterMembershipLoader registers a membership loader function
func RegisterMembershipLoader(collection *loader.Collection) {
	collection.Register(membershipLoaderKey, membershipBatchFunc)
}

func loadMembership(ctx context.Context, id string) (*models.Membership, error) {
	ldr, err := loader.Extract(ctx, membershipLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	membership, ok := data.(models.Membership)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &membership, nil
}

func membershipBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	memberships, err := getMembershipService(ctx).GetMembershipsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range memberships {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
