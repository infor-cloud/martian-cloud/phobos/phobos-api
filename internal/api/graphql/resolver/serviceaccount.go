package resolver

import (
	"context"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
)

/* ServiceAccount Query Resolvers */

// ServiceAccountConnectionQueryArgs are used to query a service account connection
type ServiceAccountConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search         *string
	Scopes         *[]models.ScopeType
	OrganizationID *string
	ProjectID      *string
}

// JWTClaim represents a claim that must be present in the JWT token
type JWTClaim struct {
	Name  string
	Value string
}

// OIDCTrustPolicy specifies the trust policies for a service account
type OIDCTrustPolicy struct {
	Issuer          string
	BoundClaimsType *models.BoundClaimsType
	BoundClaims     []JWTClaim
}

// ServiceAccountEdgeResolver resolves serviceAccount edges
type ServiceAccountEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ServiceAccountEdgeResolver) Cursor() (string, error) {
	serviceAccount, ok := r.edge.Node.(*models.ServiceAccount)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(serviceAccount)
	return *cursor, err
}

// Node returns a serviceAccount node
func (r *ServiceAccountEdgeResolver) Node() (*ServiceAccountResolver, error) {
	serviceAccount, ok := r.edge.Node.(*models.ServiceAccount)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ServiceAccountResolver{serviceAccount: serviceAccount}, nil
}

// ServiceAccountConnectionResolver resolves a serviceAccount connection
type ServiceAccountConnectionResolver struct {
	connection Connection
}

// NewServiceAccountConnectionResolver creates a new ServiceAccountConnectionResolver
func NewServiceAccountConnectionResolver(ctx context.Context, input *serviceaccount.GetServiceAccountsInput) (*ServiceAccountConnectionResolver, error) {
	saService := getServiceAccountService(ctx)

	result, err := saService.GetServiceAccounts(ctx, input)
	if err != nil {
		return nil, err
	}

	serviceAccounts := result.ServiceAccounts

	// Create edges
	edges := make([]Edge, len(serviceAccounts))
	for i, serviceAccount := range serviceAccounts {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: serviceAccount}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(serviceAccounts) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(serviceAccounts[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(serviceAccounts[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ServiceAccountConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ServiceAccountConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ServiceAccountConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ServiceAccountConnectionResolver) Edges() *[]*ServiceAccountEdgeResolver {
	resolvers := make([]*ServiceAccountEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ServiceAccountEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ServiceAccountResolver resolves a serviceAccount resource
type ServiceAccountResolver struct {
	serviceAccount *models.ServiceAccount
}

// ID resolver
func (r *ServiceAccountResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ServiceAccountType, r.serviceAccount.Metadata.ID))
}

// Name resolver
func (r *ServiceAccountResolver) Name() string {
	return r.serviceAccount.Name
}

// Description resolver
func (r *ServiceAccountResolver) Description() string {
	return r.serviceAccount.Description
}

// Scope resolver
func (r *ServiceAccountResolver) Scope() models.ScopeType {
	return r.serviceAccount.Scope
}

// Metadata resolver
func (r *ServiceAccountResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.serviceAccount.Metadata}
}

// Organization resolver
func (r *ServiceAccountResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	if r.serviceAccount.OrganizationID == nil {
		return nil, nil
	}

	organization, err := loadOrganization(ctx, *r.serviceAccount.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Project resolver
func (r *ServiceAccountResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.serviceAccount.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.serviceAccount.ProjectID)
	if err != nil {
		return nil, err
	}
	return &ProjectResolver{project: project}, nil
}

// CreatedBy resolver
func (r *ServiceAccountResolver) CreatedBy() string {
	return r.serviceAccount.CreatedBy
}

// Memberships resolver
func (r *ServiceAccountResolver) Memberships(ctx context.Context,
	args *ConnectionQueryArgs,
) (*MembershipConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &membership.GetMembershipsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ServiceAccount:    r.serviceAccount,
		MembershipScopes:  []models.ScopeType{models.OrganizationScope, models.ProjectScope},
	}

	if args.Sort != nil {
		sort := db.MembershipSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewMembershipConnectionResolver(ctx, input)
}

// OIDCTrustPolicies resolver
func (r *ServiceAccountResolver) OIDCTrustPolicies() []OIDCTrustPolicy {
	policies := []OIDCTrustPolicy{}
	for _, p := range r.serviceAccount.OIDCTrustPolicies {
		p := p
		policy := OIDCTrustPolicy{
			Issuer:          p.Issuer,
			BoundClaimsType: &p.BoundClaimsType,
			BoundClaims:     []JWTClaim{},
		}
		for k, v := range p.BoundClaims {
			policy.BoundClaims = append(policy.BoundClaims, JWTClaim{
				Name:  k,
				Value: v,
			})
		}
		policies = append(policies, policy)
	}
	return policies
}

// ActivityEvents resolver
func (r *ServiceAccountResolver) ActivityEvents(ctx context.Context, args *ActivityEventConnectionQueryArgs) (*ActivityEventConnectionResolver, error) {
	input, err := getActivityEventsInputFromQueryArgs(ctx, args)
	if err != nil {
		return nil, err
	}

	// Need to filter to this service account.
	input.ServiceAccountID = &r.serviceAccount.Metadata.ID

	return NewActivityEventConnectionResolver(ctx, input)
}

func serviceAccountsQuery(ctx context.Context, args *ServiceAccountConnectionQueryArgs) (*ServiceAccountConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := serviceaccount.GetServiceAccountsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Search:            args.Search,
	}

	if args.Scopes != nil {
		input.ServiceAccountScopes = *args.Scopes
	}

	if args.OrganizationID != nil {
		oID, oErr := getURNResolver(ctx).ResolveResourceID(ctx, *args.OrganizationID)
		if oErr != nil {
			return nil, oErr
		}

		input.OrganizationID = &oID
	}

	if args.ProjectID != nil {
		pID, pErr := getURNResolver(ctx).ResolveResourceID(ctx, *args.ProjectID)
		if pErr != nil {
			return nil, pErr
		}

		input.ProjectID = &pID
	}

	if args.Sort != nil {
		sort := db.ServiceAccountSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewServiceAccountConnectionResolver(ctx, &input)
}

/* ServiceAccount Mutation Resolvers */

// ServiceAccountMutationPayload is the response payload for a serviceAccount mutation
type ServiceAccountMutationPayload struct {
	ClientMutationID *string
	ServiceAccount   *models.ServiceAccount
	Problems         []Problem
}

// ServiceAccountMutationPayloadResolver resolves a ServiceAccountMutationPayload
type ServiceAccountMutationPayloadResolver struct {
	ServiceAccountMutationPayload
}

// ServiceAccount field resolver
func (r *ServiceAccountMutationPayloadResolver) ServiceAccount() *ServiceAccountResolver {
	if r.ServiceAccountMutationPayload.ServiceAccount == nil {
		return nil
	}

	return &ServiceAccountResolver{serviceAccount: r.ServiceAccountMutationPayload.ServiceAccount}
}

// CreateServiceAccountInput contains the input for creating a new serviceAccount
type CreateServiceAccountInput struct {
	ClientMutationID  *string
	Scope             models.ScopeType
	OrganizationID    *string // depending on scope, either OrgID or ProjectID might be required
	ProjectID         *string
	Name              string
	Description       string
	OIDCTrustPolicies []OIDCTrustPolicy
}

// UpdateServiceAccountInput contains the input for updating a serviceAccount
type UpdateServiceAccountInput struct {
	ClientMutationID  *string
	ID                string
	Metadata          *MetadataInput
	Description       *string
	OIDCTrustPolicies []OIDCTrustPolicy
}

// DeleteServiceAccountInput contains the input for deleting a serviceAccount
type DeleteServiceAccountInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handleServiceAccountMutationProblem(e error, clientMutationID *string) (*ServiceAccountMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}

	payload := ServiceAccountMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ServiceAccountMutationPayloadResolver{ServiceAccountMutationPayload: payload}, nil
}

func createServiceAccountMutation(ctx context.Context, input *CreateServiceAccountInput) (*ServiceAccountMutationPayloadResolver, error) {
	saService := getServiceAccountService(ctx)

	oidcTrustPolicies, err := convertOIDCTrustPolicies(input.OIDCTrustPolicies)
	if err != nil {
		return nil, err
	}

	if (input.OrganizationID != nil) && (input.ProjectID != nil) {
		return nil, errors.New("specifying both orgID and project ID is not allowed", errors.WithErrorCode(errors.EInvalid))
	}

	var orgID, projectID *string
	urnResolver := getURNResolver(ctx)
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("organization ID is required for an organization service account",
				errors.WithErrorCode(errors.EInvalid))
		}

		oID, oErr := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if oErr != nil {
			return nil, oErr
		}

		orgID = &oID
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("project id is required for a project service account",
				errors.WithErrorCode(errors.EInvalid))
		}

		pID, pErr := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if pErr != nil {
			return nil, pErr
		}

		projectID = &pID
	case models.GlobalScope:
		// global service accounts are allowed
		if (input.OrganizationID != nil) || (input.ProjectID != nil) {
			return nil, errors.New("global service account cannot have orgID or project ID", errors.WithErrorCode(errors.EInvalid))
		}
	default:
		return nil, errors.New("invalid scope for service account",
			errors.WithErrorCode(errors.EInvalid))
	}

	createdServiceAccount, err := saService.CreateServiceAccount(ctx, &serviceaccount.CreateServiceAccountInput{
		Name:              input.Name,
		Description:       input.Description,
		Scope:             input.Scope,
		OrgID:             orgID,
		ProjectID:         projectID,
		OIDCTrustPolicies: oidcTrustPolicies,
	})
	if err != nil {
		return nil, err
	}

	payload := ServiceAccountMutationPayload{ClientMutationID: input.ClientMutationID, ServiceAccount: createdServiceAccount, Problems: []Problem{}}
	return &ServiceAccountMutationPayloadResolver{ServiceAccountMutationPayload: payload}, nil
}

func updateServiceAccountMutation(ctx context.Context, input *UpdateServiceAccountInput) (*ServiceAccountMutationPayloadResolver, error) {
	saService := getServiceAccountService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &serviceaccount.UpdateServiceAccountInput{
		ID:          id,
		Description: input.Description,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.MetadataVersion = &v
	}

	// Update fields
	oidcTrustPolicies, err := convertOIDCTrustPolicies(input.OIDCTrustPolicies)
	if err != nil {
		return nil, err
	}

	toUpdate.OIDCTrustPolicies = oidcTrustPolicies

	serviceAccount, err := saService.UpdateServiceAccount(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := ServiceAccountMutationPayload{ClientMutationID: input.ClientMutationID, ServiceAccount: serviceAccount, Problems: []Problem{}}
	return &ServiceAccountMutationPayloadResolver{ServiceAccountMutationPayload: payload}, nil
}

func deleteServiceAccountMutation(ctx context.Context, input *DeleteServiceAccountInput) (*ServiceAccountMutationPayloadResolver, error) {
	saService := getServiceAccountService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	serviceAccount, err := saService.GetServiceAccountByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &serviceaccount.DeleteServiceAccountInput{
		ID: serviceAccount.Metadata.ID,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.MetadataVersion = &v
	}

	if err := saService.DeleteServiceAccount(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := ServiceAccountMutationPayload{ClientMutationID: input.ClientMutationID, ServiceAccount: serviceAccount, Problems: []Problem{}}
	return &ServiceAccountMutationPayloadResolver{ServiceAccountMutationPayload: payload}, nil
}

func convertOIDCTrustPolicies(src []OIDCTrustPolicy) ([]models.OIDCTrustPolicy, error) {
	policies := []models.OIDCTrustPolicy{}
	for _, p := range src {
		boundClaimsType := models.BoundClaimsTypeString
		if p.BoundClaimsType != nil {
			boundClaimsType = *p.BoundClaimsType
		}

		policy := models.OIDCTrustPolicy{
			Issuer:          p.Issuer,
			BoundClaimsType: boundClaimsType,
			BoundClaims:     map[string]string{},
		}

		for _, claim := range p.BoundClaims {
			if _, ok := policy.BoundClaims[claim.Name]; ok {
				return nil,
					errors.New(
						"Claim with name %s can only be defined once for each trust policy", claim.Name,
						errors.WithErrorCode(errors.EInvalid),
					)
			}
			policy.BoundClaims[claim.Name] = claim.Value
		}

		policies = append(policies, policy)
	}
	return policies, nil
}

/* ServiceAccount loader */

const serviceAccountLoaderKey = "serviceAccount"

// RegisterServiceAccountLoader registers a serviceAccount loader function
func RegisterServiceAccountLoader(collection *loader.Collection) {
	collection.Register(serviceAccountLoaderKey, serviceAccountBatchFunc)
}

func loadServiceAccount(ctx context.Context, id string) (*models.ServiceAccount, error) {
	ldr, err := loader.Extract(ctx, serviceAccountLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	serviceAccount, ok := data.(*models.ServiceAccount)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return serviceAccount, nil
}

func serviceAccountBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	serviceAccounts, err := getServiceAccountService(ctx).GetServiceAccountsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range serviceAccounts {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
