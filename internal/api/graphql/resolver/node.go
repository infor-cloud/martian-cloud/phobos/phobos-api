package resolver

import (
	"context"
	"fmt"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// NodeResolver resolves a node type
type NodeResolver struct {
	result interface{}
}

type idGetter interface {
	ID() graphql.ID
}

// ID resolver
func (r *NodeResolver) ID() (graphql.ID, error) {
	node, ok := r.result.(idGetter)
	if !ok {
		return "", fmt.Errorf("invalid node resolver")
	}
	return node.ID(), nil
}

// ToUser resolver
func (r *NodeResolver) ToUser() (*UserResolver, bool) {
	res, ok := r.result.(*UserResolver)
	return res, ok
}

// ToOrganization resolver
func (r *NodeResolver) ToOrganization() (*OrganizationResolver, bool) {
	res, ok := r.result.(*OrganizationResolver)
	return res, ok
}

// ToMembership resolver
func (r *NodeResolver) ToMembership() (*MembershipResolver, bool) {
	res, ok := r.result.(*MembershipResolver)
	return res, ok
}

// ToRole resolver
func (r *NodeResolver) ToRole() (*RoleResolver, bool) {
	res, ok := r.result.(*RoleResolver)
	return res, ok
}

// ToProject resolver
func (r *NodeResolver) ToProject() (*ProjectResolver, bool) {
	res, ok := r.result.(*ProjectResolver)
	return res, ok
}

// ToProjectVariable resolver
func (r *NodeResolver) ToProjectVariable() (*ProjectVariableResolver, bool) {
	res, ok := r.result.(*ProjectVariableResolver)
	return res, ok
}

// ToProjectVariableSet resolver
func (r *NodeResolver) ToProjectVariableSet() (*ProjectVariableSetResolver, bool) {
	res, ok := r.result.(*ProjectVariableSetResolver)
	return res, ok
}

// ToTeam resolver
func (r *NodeResolver) ToTeam() (*TeamResolver, bool) {
	res, ok := r.result.(*TeamResolver)
	return res, ok
}

// ToPipelineTemplate resolver
func (r *NodeResolver) ToPipelineTemplate() (*PipelineTemplateResolver, bool) {
	res, ok := r.result.(*PipelineTemplateResolver)
	return res, ok
}

// ToPipeline resolver
func (r *NodeResolver) ToPipeline() (*PipelineResolver, bool) {
	res, ok := r.result.(*PipelineResolver)
	return res, ok
}

// ToJob resolver
func (r *NodeResolver) ToJob() (*JobResolver, bool) {
	res, ok := r.result.(*JobResolver)
	return res, ok
}

// ToAgent resolver
func (r *NodeResolver) ToAgent() (*AgentResolver, bool) {
	res, ok := r.result.(*AgentResolver)
	return res, ok
}

// ToAgentSession resolver
func (r *NodeResolver) ToAgentSession() (*AgentSessionResolver, bool) {
	res, ok := r.result.(*AgentSessionResolver)
	return res, ok
}

// ToEnvironment resolver
func (r *NodeResolver) ToEnvironment() (*EnvironmentResolver, bool) {
	res, ok := r.result.(*EnvironmentResolver)
	return res, ok
}

// ToServiceAccount resolver
func (r *NodeResolver) ToServiceAccount() (*ServiceAccountResolver, bool) {
	res, ok := r.result.(*ServiceAccountResolver)
	return res, ok
}

// ToApprovalRule resolver
func (r *NodeResolver) ToApprovalRule() (*ApprovalRuleResolver, bool) {
	res, ok := r.result.(*ApprovalRuleResolver)
	return res, ok
}

// ToLifecycleTemplate resolver
func (r *NodeResolver) ToLifecycleTemplate() (*LifecycleTemplateResolver, bool) {
	res, ok := r.result.(*LifecycleTemplateResolver)
	return res, ok
}

// ToReleaseLifecycle resolver
func (r *NodeResolver) ToReleaseLifecycle() (*ReleaseLifecycleResolver, bool) {
	res, ok := r.result.(*ReleaseLifecycleResolver)
	return res, ok
}

// ToRelease resolver
func (r *NodeResolver) ToRelease() (*ReleaseResolver, bool) {
	res, ok := r.result.(*ReleaseResolver)
	return res, ok
}

// ToPlugin resolver
func (r *NodeResolver) ToPlugin() (*PluginResolver, bool) {
	res, ok := r.result.(*PluginResolver)
	return res, ok
}

// ToPluginVersion resolver
func (r *NodeResolver) ToPluginVersion() (*PluginVersionResolver, bool) {
	res, ok := r.result.(*PluginVersionResolver)
	return res, ok
}

// ToPluginPlatform resolver
func (r *NodeResolver) ToPluginPlatform() (*PluginPlatformResolver, bool) {
	res, ok := r.result.(*PluginPlatformResolver)
	return res, ok
}

// ToComment resolver
func (r *NodeResolver) ToComment() (*CommentResolver, bool) {
	res, ok := r.result.(*CommentResolver)
	return res, ok
}

// ToVCSProvider resolver
func (r *NodeResolver) ToVCSProvider() (*VCSProviderResolver, bool) {
	res, ok := r.result.(*VCSProviderResolver)
	return res, ok
}

// ToThread resolver
func (r *NodeResolver) ToThread() (*ThreadResolver, bool) {
	res, ok := r.result.(*ThreadResolver)
	return res, ok
}

// ToEnvironmentRule resolver
func (r *NodeResolver) ToEnvironmentRule() (*EnvironmentRuleResolver, bool) {
	res, ok := r.result.(*EnvironmentRuleResolver)
	return res, ok
}

func node(ctx context.Context, value string) (*NodeResolver, error) {
	if urn.IsPRN(value) {
		return resolveNodeByPRN(ctx, value)
	}

	return resolveNodeByID(ctx, value)
}

func resolveNodeByID(ctx context.Context, value string) (*NodeResolver, error) {
	var resolver any
	var retErr error

	parsedGlobalID, err := gid.ParseGlobalID(value)
	if err != nil {
		return nil, err
	}

	switch parsedGlobalID.Type {
	case gid.UserType:
		user, err := getUserService(ctx).GetUserByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &UserResolver{user: user}
	case gid.OrganizationType:
		organization, err := getOrganizationService(ctx).GetOrganizationByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &OrganizationResolver{organization: organization}
	case gid.MembershipType:
		membership, err := getMembershipService(ctx).GetMembershipByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &MembershipResolver{membership: membership}
	case gid.RoleType:
		gotRole, err := getRoleService(ctx).GetRoleByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &RoleResolver{role: gotRole}
	case gid.ProjectType:
		gotProject, err := getProjectService(ctx).GetProjectByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ProjectResolver{project: gotProject}
	case gid.ProjectVariableType:
		gotProjectVar, err := getProjectVariableService(ctx).GetProjectVariableByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ProjectVariableResolver{projectVariable: gotProjectVar}
	case gid.ProjectVariableSetType:
		gotProjectVarSet, err := getProjectVariableService(ctx).GetProjectVariableSetByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ProjectVariableSetResolver{variableSet: gotProjectVarSet}
	case gid.TeamType:
		team, err := getTeamService(ctx).GetTeamByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &TeamResolver{team: team}
	case gid.PipelineTemplateType:
		gotPipelineTemplate, err := getPipelineTemplateService(ctx).GetPipelineTemplateByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PipelineTemplateResolver{pipelineTemplate: gotPipelineTemplate}
	case gid.PipelineType:
		gotPipeline, err := getPipelineService(ctx).GetPipelineByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PipelineResolver{pipeline: gotPipeline}
	case gid.JobType:
		job, err := getJobService(ctx).GetJobByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &JobResolver{job: job}
	case gid.AgentType:
		agent, err := getAgentService(ctx).GetAgentByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &AgentResolver{agent: agent}
	case gid.AgentSessionType:
		session, err := getAgentService(ctx).GetAgentSessionByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &AgentSessionResolver{session: session}
	case gid.EnvironmentType:
		environment, err := getEnvironmentService(ctx).GetEnvironmentByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &EnvironmentResolver{environment: environment}
	case gid.ServiceAccountType:
		sa, err := getServiceAccountService(ctx).GetServiceAccountByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ServiceAccountResolver{serviceAccount: sa}
	case gid.ApprovalRuleType:
		rule, err := getApprovalRuleService(ctx).GetApprovalRuleByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ApprovalRuleResolver{rule: rule}
	case gid.LifecycleTemplateType:
		gotLifecycleTemplate, err := getLifecycleTemplateService(ctx).
			GetLifecycleTemplateByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &LifecycleTemplateResolver{lifecycleTemplate: gotLifecycleTemplate}
	case gid.ReleaseLifecycleType:
		gotReleaseLifecycle, err := getReleaseLifecycleService(ctx).
			GetReleaseLifecycleByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ReleaseLifecycleResolver{releaseLifecycle: gotReleaseLifecycle}
	case gid.ReleaseType:
		gotRelease, err := getReleaseService(ctx).GetReleaseByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ReleaseResolver{release: gotRelease}
	case gid.PluginType:
		plugin, err := getPluginRegistryService(ctx).GetPluginByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PluginResolver{plugin: plugin}
	case gid.PluginVersionType:
		pluginVersion, err := getPluginRegistryService(ctx).GetPluginVersionByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PluginVersionResolver{pluginVersion: pluginVersion}
	case gid.PluginPlatformType:
		pluginPlatform, err := getPluginRegistryService(ctx).GetPluginPlatformByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PluginPlatformResolver{pluginPlatform: pluginPlatform}
	case gid.CommentType:
		comment, err := getCommentService(ctx).GetCommentByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &CommentResolver{comment: comment}
	case gid.VCSProviderType:
		vcsProvider, err := getVCSService(ctx).GetVCSProviderByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &VCSProviderResolver{vcsProvider: vcsProvider}
	case gid.ThreadType:
		thread, err := getCommentService(ctx).GetThreadByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ThreadResolver{thread: thread}
	case gid.EnvironmentRuleType:
		environmentRule, err := getEnvironmentService(ctx).GetEnvironmentRuleByID(ctx, parsedGlobalID.ID)
		if err != nil {
			retErr = err
			break
		}
		resolver = &EnvironmentRuleResolver{rule: environmentRule}
	default:
		return nil, fmt.Errorf("node query doesn't support type %s", parsedGlobalID.Type)
	}

	if retErr != nil {
		if errors.ErrorCode(retErr) == errors.ENotFound {
			return nil, nil
		}
		return nil, retErr
	}

	return &NodeResolver{result: resolver}, nil
}

func resolveNodeByPRN(ctx context.Context, prn string) (*NodeResolver, error) {
	var resolver any
	var retErr error

	resourceType := urn.GetResourceType(prn)

	switch resourceType {
	case models.UserResource:
		user, err := getUserService(ctx).GetUserByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &UserResolver{user: user}
	case models.OrganizationResource:
		organization, err := getOrganizationService(ctx).GetOrganizationByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &OrganizationResolver{organization: organization}
	case models.MembershipResource:
		membership, err := getMembershipService(ctx).GetMembershipByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &MembershipResolver{membership: membership}
	case models.RoleResource:
		gotRole, err := getRoleService(ctx).GetRoleByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &RoleResolver{role: gotRole}
	case models.ProjectResource:
		gotProject, err := getProjectService(ctx).GetProjectByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ProjectResolver{project: gotProject}
	case models.ProjectVariableResource:
		gotProjectVariable, err := getProjectVariableService(ctx).GetProjectVariableByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ProjectVariableResolver{projectVariable: gotProjectVariable}
	case models.ProjectVariableSetResource:
		gotProjectVariableSet, err := getProjectVariableService(ctx).GetProjectVariableSetByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ProjectVariableSetResolver{variableSet: gotProjectVariableSet}
	case models.TeamResource:
		team, err := getTeamService(ctx).GetTeamByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &TeamResolver{team: team}
	case models.PipelineTemplateResource:
		gotPipelineTemplate, err := getPipelineTemplateService(ctx).GetPipelineTemplateByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PipelineTemplateResolver{pipelineTemplate: gotPipelineTemplate}
	case models.PipelineResource:
		gotPipeline, err := getPipelineService(ctx).GetPipelineByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PipelineResolver{pipeline: gotPipeline}
	case models.JobResource:
		job, err := getJobService(ctx).GetJobByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &JobResolver{job: job}
	case models.AgentResource:
		agent, err := getAgentService(ctx).GetAgentByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &AgentResolver{agent: agent}
	case models.AgentSessionResource:
		session, err := getAgentService(ctx).GetAgentSessionByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &AgentSessionResolver{session: session}
	case models.EnvironmentResource:
		environment, err := getEnvironmentService(ctx).GetEnvironmentByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &EnvironmentResolver{environment: environment}
	case models.ServiceAccountResource:
		sa, err := getServiceAccountService(ctx).GetServiceAccountByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ServiceAccountResolver{serviceAccount: sa}
	case models.ApprovalRuleResource:
		rule, err := getApprovalRuleService(ctx).GetApprovalRuleByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ApprovalRuleResolver{rule: rule}
	case models.LifecycleTemplateResource:
		lt, err := getLifecycleTemplateService(ctx).GetLifecycleTemplateByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &LifecycleTemplateResolver{lifecycleTemplate: lt}
	case models.ReleaseLifecycleResource:
		rl, err := getReleaseLifecycleService(ctx).GetReleaseLifecycleByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ReleaseLifecycleResolver{releaseLifecycle: rl}
	case models.ReleaseResource:
		gotRelease, err := getReleaseService(ctx).GetReleaseByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ReleaseResolver{release: gotRelease}
	case models.PluginResource:
		plugin, err := getPluginRegistryService(ctx).GetPluginByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PluginResolver{plugin: plugin}
	case models.PluginVersionResource:
		pluginVersion, err := getPluginRegistryService(ctx).GetPluginVersionByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PluginVersionResolver{pluginVersion: pluginVersion}
	case models.PluginPlatformResource:
		pluginPlatform, err := getPluginRegistryService(ctx).GetPluginPlatformByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &PluginPlatformResolver{pluginPlatform: pluginPlatform}
	case models.CommentResource:
		comment, err := getCommentService(ctx).GetCommentByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &CommentResolver{comment: comment}
	case models.VCSProviderResource:
		vcsProvider, err := getVCSService(ctx).GetVCSProviderByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &VCSProviderResolver{vcsProvider: vcsProvider}
	case models.ThreadResource:
		thread, err := getCommentService(ctx).GetThreadByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &ThreadResolver{thread: thread}
	case models.EnvironmentRuleResource:
		rule, err := getEnvironmentService(ctx).GetEnvironmentRuleByPRN(ctx, prn)
		if err != nil {
			retErr = err
			break
		}
		resolver = &EnvironmentRuleResolver{rule: rule}
	default:
		return nil, fmt.Errorf("node query doesn't support type %s", resourceType)
	}

	if retErr != nil {
		if errors.ErrorCode(retErr) == errors.ENotFound {
			return nil, nil
		}
		return nil, retErr
	}

	return &NodeResolver{result: resolver}, nil
}
