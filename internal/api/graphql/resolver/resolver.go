// Package resolver contains functionality for interacting
// with the API via GraphQL. It supports queries, mutations,
// and subscriptions.
package resolver

import (
	"context"
	"fmt"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/apiserver/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/approvalrule"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/comment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/metric"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/organization"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/project"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/projectvariable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/release"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/resourcelimit"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/role"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/scim"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/team"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/todoitem"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/user"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/version"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// Key type is used for attaching resolver state to the context
type key string

const (
	resolverStateKey key = "resolverState"
)

// State contains the services required by resolvers
type State struct {
	EnvironmentService       environment.Service
	MembershipService        membership.Service
	Logger                   logger.Logger
	OrganizationService      organization.Service
	ServiceAccountService    serviceaccount.Service
	ResourceLimitService     resourcelimit.Service
	ProjectService           project.Service
	ProjectVariableService   projectvariable.Service
	JobService               job.Service
	TeamService              team.Service
	SCIMService              scim.Service
	MetricService            metric.Service
	PipelineService          pipeline.Service
	AgentService             agent.Service
	UserService              user.Service
	RoleService              role.Service
	PipelineTemplateService  pipelinetemplate.Service
	ApprovalRuleService      approvalrule.Service
	LifecycleTemplateService lifecycletemplate.Service
	ReleaseLifecycleService  releaselifecycle.Service
	ReleaseService           release.Service
	ActivityEventService     activityevent.Service
	CommentService           comment.Service
	PluginRegistryService    pluginregistry.Service
	ToDoItemService          todoitem.Service
	VCSService               vcs.Service
	VersionService           version.Service
	URNResolver              *urn.Resolver
	Config                   *config.Config
}

// Attach is used to attach the resolver state to the context
func (r *State) Attach(ctx context.Context) context.Context {
	return context.WithValue(ctx, resolverStateKey, r)
}

func extract(ctx context.Context) *State {
	rs, ok := ctx.Value(resolverStateKey).(*State)
	if !ok {
		// Use panic here since this is not a recoverable error
		panic(fmt.Sprintf("unable to find %s resolver state on the request context", resolverStateKey))
	}

	return rs
}

func (k key) String() string {
	return fmt.Sprintf("gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/resolver %s", string(k))
}

func getUserService(ctx context.Context) user.Service {
	return extract(ctx).UserService
}

func getResourceLimitService(ctx context.Context) resourcelimit.Service {
	return extract(ctx).ResourceLimitService
}

func getProjectService(ctx context.Context) project.Service {
	return extract(ctx).ProjectService
}

func getPipelineTemplateService(ctx context.Context) pipelinetemplate.Service {
	return extract(ctx).PipelineTemplateService
}

func getPipelineService(ctx context.Context) pipeline.Service {
	return extract(ctx).PipelineService
}

// nolint
func getLogger(ctx context.Context) logger.Logger {
	return extract(ctx).Logger
}

func getConfig(ctx context.Context) *config.Config {
	return extract(ctx).Config
}

func getOrganizationService(ctx context.Context) organization.Service {
	return extract(ctx).OrganizationService
}

func getMembershipService(ctx context.Context) membership.Service {
	return extract(ctx).MembershipService
}

func getRoleService(ctx context.Context) role.Service {
	return extract(ctx).RoleService
}

func getJobService(ctx context.Context) job.Service {
	return extract(ctx).JobService
}

func getApprovalRuleService(ctx context.Context) approvalrule.Service {
	return extract(ctx).ApprovalRuleService
}

func getTeamService(ctx context.Context) team.Service {
	return extract(ctx).TeamService
}

func getSCIMService(ctx context.Context) scim.Service {
	return extract(ctx).SCIMService
}

func getAgentService(ctx context.Context) agent.Service {
	return extract(ctx).AgentService
}

func getEnvironmentService(ctx context.Context) environment.Service {
	return extract(ctx).EnvironmentService
}

func getServiceAccountService(ctx context.Context) serviceaccount.Service {
	return extract(ctx).ServiceAccountService
}

func getURNResolver(ctx context.Context) *urn.Resolver {
	return extract(ctx).URNResolver
}

func getLifecycleTemplateService(ctx context.Context) lifecycletemplate.Service {
	return extract(ctx).LifecycleTemplateService
}

func getReleaseLifecycleService(ctx context.Context) releaselifecycle.Service {
	return extract(ctx).ReleaseLifecycleService
}

func getReleaseService(ctx context.Context) release.Service {
	return extract(ctx).ReleaseService
}

func getActivityService(ctx context.Context) activityevent.Service {
	return extract(ctx).ActivityEventService
}

func getCommentService(ctx context.Context) comment.Service {
	return extract(ctx).CommentService
}

func getPluginRegistryService(ctx context.Context) pluginregistry.Service {
	return extract(ctx).PluginRegistryService
}

func getToDoItemService(ctx context.Context) todoitem.Service {
	return extract(ctx).ToDoItemService
}

func getVCSService(ctx context.Context) vcs.Service {
	return extract(ctx).VCSService
}

func getMetricService(ctx context.Context) metric.Service {
	return extract(ctx).MetricService
}

func getProjectVariableService(ctx context.Context) projectvariable.Service {
	return extract(ctx).ProjectVariableService
}

func getVersionService(ctx context.Context) version.Service {
	return extract(ctx).VersionService
}

func convertPRNList(ctx context.Context, prnList []string) ([]string, error) {
	urnResolver := getURNResolver(ctx)
	response := []string{}

	for _, prn := range prnList {
		id, err := urnResolver.ResolveResourceID(ctx, prn)
		if err != nil {
			if errors.ErrorCode(err) == errors.EInvalid {
				return nil, errors.New("invalid resource identifier: identifier must by either an ID or PRN", errors.WithErrorCode(errors.EInvalid))
			}
			return nil, err
		}
		response = append(response, id)
	}

	return response, nil
}
