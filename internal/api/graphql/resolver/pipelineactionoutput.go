package resolver

import (
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"

	graphql "github.com/graph-gophers/graphql-go"
)

/* PipelineActionOutput Query Resolvers */

// PipelineActionOutputResolver resolves an output resource
type PipelineActionOutputResolver struct {
	output *models.PipelineActionOutput
}

// ID resolver
func (r *PipelineActionOutputResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.PipelineActionOutputType, r.output.Metadata.ID))
}

// Name resolver
func (r *PipelineActionOutputResolver) Name() string {
	return r.output.Name
}

// Type resolver
func (r *PipelineActionOutputResolver) Type() string {
	return string(r.output.Type)
}

// Value resolver
func (r *PipelineActionOutputResolver) Value() string {
	return string(r.output.Value)
}

// Metadata resolver
func (r *PipelineActionOutputResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.output.Metadata}
}
