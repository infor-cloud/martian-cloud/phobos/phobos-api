package resolver

import (
	"context"
)

// RootResolver is the entry point for all top-level operations.
type RootResolver struct{}

// NewRootResolver creates a root resolver
func NewRootResolver() *RootResolver {
	return &RootResolver{}
}

/* Me query to get current authenticated subject */

// Me query returns the authenticated subject
func (r RootResolver) Me(ctx context.Context) (*MeResponseResolver, error) {
	return meQuery(ctx)
}

/* Node query */

// Node query returns a node by ID
func (r RootResolver) Node(ctx context.Context, args *struct{ ID string }) (*NodeResolver, error) {
	return node(ctx, args.ID)
}

/* User Queries and Mutations */

// Users query returns a user connection
func (r RootResolver) Users(ctx context.Context, args *UserConnectionQueryArgs) (*UserConnectionResolver, error) {
	return usersQuery(ctx, args)
}

/* Auth Settings Query */

// AuthSettings returns the configured auth settings
func (r RootResolver) AuthSettings(ctx context.Context) *AuthSettingsResolver {
	return authSettingsQuery(ctx)
}

/* Organizations Queries and Mutations */

// Organizations query returns a organizations connection
func (r RootResolver) Organizations(ctx context.Context, args *OrganizationConnectionQueryArgs) (*OrganizationConnectionResolver, error) {
	return organizationsQuery(ctx, args)
}

// CreateOrganization creates a new organization
func (r RootResolver) CreateOrganization(ctx context.Context, args *struct{ Input *CreateOrganizationInput }) (*OrganizationMutationPayloadResolver, error) {
	response, err := createOrganizationMutation(ctx, args.Input)
	if err != nil {
		return handleOrganizationMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateOrganization updates an existing organization
func (r RootResolver) UpdateOrganization(ctx context.Context, args *struct{ Input *UpdateOrganizationInput }) (*OrganizationMutationPayloadResolver, error) {
	response, err := updateOrganizationMutation(ctx, args.Input)
	if err != nil {
		return handleOrganizationMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteOrganization deletes a organization
func (r RootResolver) DeleteOrganization(ctx context.Context, args *struct{ Input *DeleteOrganizationInput }) (*OrganizationMutationPayloadResolver, error) {
	response, err := deleteOrganizationMutation(ctx, args.Input)
	if err != nil {
		return handleOrganizationMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Membership queries and Mutations */

// Memberships query returns a memberships connection
func (r RootResolver) Memberships(ctx context.Context, args *MembershipConnectionQueryArgs) (*MembershipConnectionResolver, error) {
	return membershipsQuery(ctx, args)
}

// CreateMembership creates a new membership
func (r RootResolver) CreateMembership(ctx context.Context,
	args *struct {
		Input *CreateMembershipInput
	},
) (*MembershipMutationPayloadResolver, error) {
	response, err := createMembershipMutation(ctx, args.Input)
	if err != nil {
		return handleMembershipMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateMembership updates an existing membership
func (r RootResolver) UpdateMembership(ctx context.Context,
	args *struct {
		Input *UpdateMembershipInput
	},
) (*MembershipMutationPayloadResolver, error) {
	response, err := updateMembershipMutation(ctx, args.Input)
	if err != nil {
		return handleMembershipMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteMembership updates an existing membership
func (r RootResolver) DeleteMembership(ctx context.Context,
	args *struct {
		Input *DeleteMembershipInput
	},
) (*MembershipMutationPayloadResolver, error) {
	response, err := deleteMembershipMutation(ctx, args.Input)
	if err != nil {
		return handleMembershipMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Role queries and mutations */

// AvailableRolePermissions returns a list of available role permissions.
func (r RootResolver) AvailableRolePermissions(ctx context.Context) ([]string, error) {
	return availableRolePermissionsQuery(ctx)
}

// Role query returns a role by name
func (r RootResolver) Role(ctx context.Context, args *RoleQueryArgs) (*RoleResolver, error) {
	return roleQuery(ctx, args)
}

// Roles query returns a roles connection
func (r RootResolver) Roles(ctx context.Context, args *RolesConnectionQueryArgs) (*RoleConnectionResolver, error) {
	return rolesQuery(ctx, args)
}

// CreateRole creates a role
func (r RootResolver) CreateRole(ctx context.Context, args *struct{ Input *CreateRoleInput }) (*RoleMutationPayloadResolver, error) {
	response, err := createRoleMutation(ctx, args.Input)
	if err != nil {
		return handleRoleMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateRole updates a role
func (r RootResolver) UpdateRole(ctx context.Context, args *struct{ Input *UpdateRoleInput }) (*RoleMutationPayloadResolver, error) {
	response, err := updateRoleMutation(ctx, args.Input)
	if err != nil {
		return handleRoleMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteRole updates a role
func (r RootResolver) DeleteRole(ctx context.Context, args *struct{ Input *DeleteRoleInput }) (*RoleMutationPayloadResolver, error) {
	response, err := deleteRoleMutation(ctx, args.Input)
	if err != nil {
		return handleRoleMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Resource Limits Query and Mutation */

// ResourceLimits returns the current resource limits
func (r RootResolver) ResourceLimits(ctx context.Context) ([]*ResourceLimitResolver, error) {
	return resourceLimitsQuery(ctx)
}

// UpdateResourceLimit creates or updates a resource limit
func (r RootResolver) UpdateResourceLimit(ctx context.Context,
	args *struct{ Input *UpdateResourceLimitInput }) (*ResourceLimitMutationPayloadResolver, error) {
	response, err := updateResourceLimitMutation(ctx, args.Input)
	if err != nil {
		return handleResourceLimitMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

/* Projects Queries and Mutations */

// Projects query returns a projects connection
func (r RootResolver) Projects(ctx context.Context, args *ProjectConnectionQueryArgs) (*ProjectConnectionResolver, error) {
	return projectsQuery(ctx, args)
}

// CreateProject creates a new project
func (r RootResolver) CreateProject(ctx context.Context, args *struct{ Input *CreateProjectInput }) (*ProjectMutationPayloadResolver, error) {
	response, err := createProjectMutation(ctx, args.Input)
	if err != nil {
		return handleProjectMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateProject updates an existing project
func (r RootResolver) UpdateProject(ctx context.Context, args *struct{ Input *UpdateProjectInput }) (*ProjectMutationPayloadResolver, error) {
	response, err := updateProjectMutation(ctx, args.Input)
	if err != nil {
		return handleProjectMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteProject deletes a project
func (r RootResolver) DeleteProject(ctx context.Context, args *struct{ Input *DeleteProjectInput }) (*ProjectMutationPayloadResolver, error) {
	response, err := deleteProjectMutation(ctx, args.Input)
	if err != nil {
		return handleProjectMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Project Variable Mutations */

// SetProjectVariables sets all the variables for a project
func (r RootResolver) SetProjectVariables(ctx context.Context, args *struct{ Input *SetProjectVariablesInput }) (*ProjectVariableSetMutationPayloadResolver, error) {
	response, err := setProjectVariablesMutation(ctx, args.Input)
	if err != nil {
		return handleProjectVariableSetMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// PatchProjectVariables patches the variables for a project
func (r RootResolver) PatchProjectVariables(ctx context.Context, args *struct{ Input *PatchProjectVariablesInput }) (*ProjectVariableSetMutationPayloadResolver, error) {
	response, err := patchProjectVariablesMutation(ctx, args.Input)
	if err != nil {
		return handleProjectVariableSetMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Job queries and Mutations */

// JobEvents subscribes to job events
func (r RootResolver) JobEvents(ctx context.Context, args *struct {
	Input *JobEventSubscriptionInput
}) (<-chan *JobEventResolver, error) {
	return r.jobEventsSubscription(ctx, args.Input)
}

// CancelJob cancels a job
func (r RootResolver) CancelJob(ctx context.Context, args *struct{ Input *CancelJobInput }) (*JobMutationPayloadResolver, error) {
	response, err := cancelJobMutation(ctx, args.Input)
	if err != nil {
		return handleJobMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// JobLogStreamEvents sets up a subscription for job log events
func (r RootResolver) JobLogStreamEvents(ctx context.Context, args *struct {
	Input *JobLogStreamSubscriptionInput
}) (<-chan *JobLogStreamEventResolver, error) {
	return r.jobLogStreamEventsSubscription(ctx, args.Input)
}

// JobCancellationEvent sets up a subscription for job cancellation event
func (r RootResolver) JobCancellationEvent(ctx context.Context, args *struct {
	Input *JobCancellationEventSubscriptionInput
},
) (<-chan *JobCancellationEventResolver, error) {
	return r.jobCancellationEventSubscription(ctx, args.Input)
}

/* PipelineTemplates Queries and Mutations */

// CreatePipelineTemplate creates a new pipeline template
func (r RootResolver) CreatePipelineTemplate(ctx context.Context, args *struct{ Input *CreatePipelineTemplateInput }) (*PipelineTemplateMutationPayloadResolver, error) {
	response, err := createPipelineTemplateMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineTemplateMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeletePipelineTemplate deletes a pipeline template
func (r RootResolver) DeletePipelineTemplate(ctx context.Context, args *struct{ Input *DeletePipelineTemplateInput }) (*PipelineTemplateMutationPayloadResolver, error) {
	response, err := deletePipelineTemplateMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineTemplateMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Pipelines Queries and Mutations */

// PipelineNode query returns a pipeline node by path
func (r RootResolver) PipelineNode(ctx context.Context, args *PipelineNodeQueryArgs) (*PipelineNodeResolver, error) {
	return pipelineNodeQuery(ctx, args)
}

// PipelineEvents subscribes to pipeline events for a single pipeline
func (r RootResolver) PipelineEvents(ctx context.Context, args *struct {
	Input *PipelineEventsSubscriptionInput
}) (<-chan *PipelineEventResolver, error) {
	return r.pipelineEventsSubscription(ctx, args.Input)
}

// ProjectPipelineEvents subscribes to pipeline events for a project
func (r RootResolver) ProjectPipelineEvents(ctx context.Context, args *struct {
	Input *ProjectPipelineEventsSubscriptionInput
}) (<-chan *PipelineEventResolver, error) {
	return r.projectPipelineEventsSubscription(ctx, args.Input)
}

// CreatePipeline creates a new pipeline
func (r RootResolver) CreatePipeline(ctx context.Context, args *struct{ Input *CreatePipelineInput }) (*PipelineMutationPayloadResolver, error) {
	response, err := createPipelineMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// CancelPipeline cancels a pipeline
func (r RootResolver) CancelPipeline(ctx context.Context, args *struct{ Input *CancelPipelineInput }) (*PipelineMutationPayloadResolver, error) {
	response, err := cancelPipelineMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// ApprovePipeline approves a pipeline
func (r RootResolver) ApprovePipeline(ctx context.Context, args *struct{ Input *ApprovePipelineInput }) (*PipelineMutationPayloadResolver, error) {
	response, err := approvePipelineMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RevokePipelineApproval revokes a pipeline approval
func (r RootResolver) RevokePipelineApproval(ctx context.Context, args *struct{ Input *RevokePipelineApprovalInput }) (*PipelineMutationPayloadResolver, error) {
	response, err := revokePipelineApprovalMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// ApprovePipelineTask approves a pipeline task
func (r RootResolver) ApprovePipelineTask(ctx context.Context, args *struct{ Input *ApprovePipelineTaskInput }) (*PipelineMutationPayloadResolver, error) {
	response, err := approvePipelineTaskMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RevokePipelineTaskApproval revokes a pipeline task approval
func (r RootResolver) RevokePipelineTaskApproval(ctx context.Context,
	args *struct {
		Input *RevokePipelineTaskApprovalInput
	},
) (*PipelineMutationPayloadResolver, error) {
	response, err := revokePipelineTaskApprovalMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RetryPipelineTask reruns a pipeline task
func (r RootResolver) RetryPipelineTask(ctx context.Context, args *struct{ Input *RetryPipelineTaskInput }) (*PipelineMutationPayloadResolver, error) {
	response, err := retryPipelineTaskMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RunPipelineTask starts a pipeline task
func (r RootResolver) RunPipelineTask(ctx context.Context, args *struct{ Input *RunPipelineTaskInput }) (*PipelineMutationPayloadResolver, error) {
	response, err := runPipelineTaskMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RunPipeline starts a pipeline
func (r RootResolver) RunPipeline(ctx context.Context, args *struct {
	Input *RunPipelineInput
}) (*PipelineMutationPayloadResolver, error) {
	response, err := runPipelineMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RetryNestedPipeline reruns a nested pipeline
func (r RootResolver) RetryNestedPipeline(ctx context.Context, args *struct {
	Input *RetryNestedPipelineInput
}) (*PipelineMutationPayloadResolver, error) {
	response, err := retryNestedPipelineMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// SchedulePipelineNode sets the scheduled start time for a pipeline node
func (r RootResolver) SchedulePipelineNode(ctx context.Context, args *struct {
	Input *SchedulePipelineNodeInput
}) (*PipelineMutationPayloadResolver, error) {
	response, err := schedulePipelineNodeMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// CancelPipelineNodeSchedule clears the scheduled start time for a pipeline node
func (r RootResolver) CancelPipelineNodeSchedule(ctx context.Context, args *struct {
	Input *CancelPipelineNodeScheduleInput
}) (*PipelineMutationPayloadResolver, error) {
	response, err := cancelPipelineNodeScheduleMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeferPipelineNode defers a pipeline node
func (r RootResolver) DeferPipelineNode(ctx context.Context, args *struct {
	Input *DeferPipelineNodeInput
}) (*PipelineMutationPayloadResolver, error) {
	response, err := deferPipelineNodeMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UndeferPipelineNode undefer a pipeline node
func (r RootResolver) UndeferPipelineNode(ctx context.Context, args *struct {
	Input *UndeferPipelineNodeInput
}) (*PipelineMutationPayloadResolver, error) {
	response, err := undeferPipelineNodeMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Teams Queries and Mutations */

// Teams query returns a teams connection
func (r RootResolver) Teams(ctx context.Context, args *TeamConnectionQueryArgs) (*TeamConnectionResolver, error) {
	return teamsQuery(ctx, args)
}

// CreateTeam creates a new team
func (r RootResolver) CreateTeam(ctx context.Context, args *struct{ Input *CreateTeamInput }) (*TeamMutationPayloadResolver, error) {
	response, err := createTeamMutation(ctx, args.Input)
	if err != nil {
		return handleTeamMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateTeam updates an existing team
func (r RootResolver) UpdateTeam(ctx context.Context, args *struct{ Input *UpdateTeamInput }) (*TeamMutationPayloadResolver, error) {
	response, err := updateTeamMutation(ctx, args.Input)
	if err != nil {
		return handleTeamMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteTeam deletes a team
func (r RootResolver) DeleteTeam(ctx context.Context, args *struct{ Input *DeleteTeamInput }) (*TeamMutationPayloadResolver, error) {
	response, err := deleteTeamMutation(ctx, args.Input)
	if err != nil {
		return handleTeamMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* TeamMember queries and Mutations */

// AddUserToTeam adds a user to a team.
func (r RootResolver) AddUserToTeam(ctx context.Context, args *struct{ Input *AddUserToTeamInput }) (*TeamMemberMutationPayloadResolver, error) {
	response, err := addUserToTeamMutation(ctx, args.Input)
	if err != nil {
		return handleTeamMemberMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateTeamMember updates an existing team member
func (r RootResolver) UpdateTeamMember(ctx context.Context, args *struct{ Input *UpdateTeamMemberInput }) (*TeamMemberMutationPayloadResolver, error) {
	response, err := updateTeamMemberMutation(ctx, args.Input)
	if err != nil {
		return handleTeamMemberMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RemoveUserFromTeam removes a user from a team.
func (r RootResolver) RemoveUserFromTeam(ctx context.Context, args *struct{ Input *RemoveUserFromTeamInput }) (*TeamMemberMutationPayloadResolver, error) {
	response, err := removeUserFromTeamMutation(ctx, args.Input)
	if err != nil {
		return handleTeamMemberMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* SCIM queries and mutations */

// CreateSCIMToken generates a token specifically for provisioning SCIM resources.
func (r RootResolver) CreateSCIMToken(ctx context.Context) (*SCIMTokenPayload, error) {
	response, err := createSCIMTokenMutation(ctx)
	if err != nil {
		return handleSCIMMutationProblem(err, nil)
	}

	return response, nil
}

/* Agent Queries and Mutations */

// SharedAgents query returns a shared agents connection
func (r RootResolver) SharedAgents(ctx context.Context, args *ConnectionQueryArgs) (*AgentConnectionResolver, error) {
	return sharedAgentsQuery(ctx, args)
}

// CreateAgent creates a new agent
func (r RootResolver) CreateAgent(ctx context.Context, args *struct{ Input *CreateAgentInput }) (*AgentMutationPayloadResolver, error) {
	response, err := createAgentMutation(ctx, args.Input)
	if err != nil {
		return handleAgentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateAgent updates an existing agent
func (r RootResolver) UpdateAgent(ctx context.Context, args *struct{ Input *UpdateAgentInput }) (*AgentMutationPayloadResolver, error) {
	response, err := updateAgentMutation(ctx, args.Input)
	if err != nil {
		return handleAgentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteAgent deletes an agent
func (r RootResolver) DeleteAgent(ctx context.Context, args *struct{ Input *DeleteAgentInput }) (*AgentMutationPayloadResolver, error) {
	response, err := deleteAgentMutation(ctx, args.Input)
	if err != nil {
		return handleAgentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Agent Session Subscriptions */

// AgentSessionEvents subscribes to agent session events
func (r RootResolver) AgentSessionEvents(ctx context.Context, args *struct {
	Input *AgentSessionEventSubscriptionInput
}) (<-chan *AgentSessionEventResolver, error) {
	return r.agentSessionEventsSubscription(ctx, args.Input)
}

// AgentSessionErrorLogEvents subscribes to agent session error log events
func (r RootResolver) AgentSessionErrorLogEvents(ctx context.Context, args *struct {
	Input *AgentSessionErrorLogSubscriptionInput
}) (<-chan *AgentSessionErrorLogEventResolver, error) {
	return r.agentSessionErrorLogSubscription(ctx, args.Input)
}

/* Environment Queries and Mutations */

// CreateEnvironment creates a new environment
func (r RootResolver) CreateEnvironment(ctx context.Context, args *struct{ Input *CreateEnvironmentInput }) (*EnvironmentMutationPayloadResolver, error) {
	response, err := createEnvironmentMutation(ctx, args.Input)
	if err != nil {
		return handleEnvironmentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateEnvironment updates an existing environment
func (r RootResolver) UpdateEnvironment(ctx context.Context, args *struct{ Input *UpdateEnvironmentInput }) (*EnvironmentMutationPayloadResolver, error) {
	response, err := updateEnvironmentMutation(ctx, args.Input)
	if err != nil {
		return handleEnvironmentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteEnvironment deletes an environment
func (r RootResolver) DeleteEnvironment(ctx context.Context, args *struct{ Input *DeleteEnvironmentInput }) (*EnvironmentMutationPayloadResolver, error) {
	response, err := deleteEnvironmentMutation(ctx, args.Input)
	if err != nil {
		return handleEnvironmentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// AssignServiceAccountToAgent assigns a service account to a agent
func (r RootResolver) AssignServiceAccountToAgent(ctx context.Context, args *struct {
	Input *AssignServiceAccountToAgentInput
}) (*AgentMutationPayloadResolver, error) {
	response, err := assignServiceAccountToAgentMutation(ctx, args.Input)
	if err != nil {
		return handleAgentMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

// UnassignServiceAccountFromAgent unassigns a service account from a agent
func (r RootResolver) UnassignServiceAccountFromAgent(ctx context.Context, args *struct {
	Input *AssignServiceAccountToAgentInput
}) (*AgentMutationPayloadResolver, error) {
	response, err := unassignServiceAccountFromAgentMutation(ctx, args.Input)
	if err != nil {
		return handleAgentMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

/* Service Account Queries and Mutations */

// ServiceAccounts query returns service accounts connection
func (r RootResolver) ServiceAccounts(ctx context.Context, args *ServiceAccountConnectionQueryArgs) (*ServiceAccountConnectionResolver, error) {
	return serviceAccountsQuery(ctx, args)
}

// CreateServiceAccount creates a new service account
func (r RootResolver) CreateServiceAccount(ctx context.Context, args *struct {
	Input *CreateServiceAccountInput
}) (*ServiceAccountMutationPayloadResolver, error) {
	response, err := createServiceAccountMutation(ctx, args.Input)
	if err != nil {
		return handleServiceAccountMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateServiceAccount updates an existing service account
func (r RootResolver) UpdateServiceAccount(ctx context.Context, args *struct{ Input *UpdateServiceAccountInput }) (*ServiceAccountMutationPayloadResolver, error) {
	response, err := updateServiceAccountMutation(ctx, args.Input)
	if err != nil {
		return handleServiceAccountMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteServiceAccount deletes a service account
func (r RootResolver) DeleteServiceAccount(ctx context.Context, args *struct{ Input *DeleteServiceAccountInput }) (*ServiceAccountMutationPayloadResolver, error) {
	response, err := deleteServiceAccountMutation(ctx, args.Input)
	if err != nil {
		return handleServiceAccountMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Lifecycle Queries and Mutations */

// CreateLifecycleTemplate creates a new lifecycle template
func (r RootResolver) CreateLifecycleTemplate(ctx context.Context, args *struct {
	Input *CreateLifecycleTemplateInput
}) (*LifecycleTemplateMutationPayloadResolver, error) {
	response, err := createLifecycleTemplateMutation(ctx, args.Input)
	if err != nil {
		return handleLifecycleTemplateMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// CreateReleaseLifecycle creates a new release lifecycle
func (r RootResolver) CreateReleaseLifecycle(ctx context.Context, args *struct {
	Input *CreateReleaseLifecycleInput
}) (*ReleaseLifecycleMutationPayloadResolver, error) {
	response, err := createReleaseLifecycleMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseLifecycleMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateReleaseLifecycle updates an existing release lifecycle
func (r RootResolver) UpdateReleaseLifecycle(ctx context.Context, args *struct {
	Input *UpdateReleaseLifecycleInput
}) (*ReleaseLifecycleMutationPayloadResolver, error) {
	response, err := updateReleaseLifecycleMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseLifecycleMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteReleaseLifecycle deletes an existing release lifecycle
func (r RootResolver) DeleteReleaseLifecycle(ctx context.Context, args *struct {
	Input *DeleteReleaseLifecycleInput
}) (*ReleaseLifecycleMutationPayloadResolver, error) {
	response, err := deleteReleaseLifecycleMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseLifecycleMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// AddParticipantToRelease adds a participant to a release
func (r RootResolver) AddParticipantToRelease(ctx context.Context, args *struct {
	Input *ReleaseParticipantInput
}) (*ReleaseMutationPayloadResolver, error) {
	response, err := addParticipantToReleaseMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// RemoveParticipantFromRelease removes a participant from a release
func (r RootResolver) RemoveParticipantFromRelease(ctx context.Context, args *struct {
	Input *ReleaseParticipantInput
}) (*ReleaseMutationPayloadResolver, error) {
	response, err := removeParticipantFromReleaseMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Approval Rules Queries and Mutations */

// CreateApprovalRule creates a new approval rule
func (r RootResolver) CreateApprovalRule(ctx context.Context, args *struct{ Input *CreateApprovalRuleInput }) (*ApprovalRuleMutationPayloadResolver, error) {
	response, err := createApprovalRuleMutation(ctx, args.Input)
	if err != nil {
		return handleApprovalRuleMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

// UpdateApprovalRule updates an existing approval rule
func (r RootResolver) UpdateApprovalRule(ctx context.Context, args *struct{ Input *UpdateApprovalRuleInput }) (*ApprovalRuleMutationPayloadResolver, error) {
	response, err := updateApprovalRuleMutation(ctx, args.Input)
	if err != nil {
		return handleApprovalRuleMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

// DeleteApprovalRule deletes a approval rule
func (r RootResolver) DeleteApprovalRule(ctx context.Context, args *struct{ Input *DeleteApprovalRuleInput }) (*ApprovalRuleMutationPayloadResolver, error) {
	response, err := deleteApprovalRuleMutation(ctx, args.Input)
	if err != nil {
		return handleApprovalRuleMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

/* Release queries and mutations */

// CreateRelease creates a new release
func (r RootResolver) CreateRelease(ctx context.Context, args *struct {
	Input *CreateReleaseInput
}) (*ReleaseMutationPayloadResolver, error) {
	response, err := createReleaseMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateRelease updates a release
func (r RootResolver) UpdateRelease(ctx context.Context, args *struct {
	Input *UpdateReleaseInput
}) (*ReleaseMutationPayloadResolver, error) {
	response, err := updateReleaseMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteRelease deletes a release
func (r RootResolver) DeleteRelease(ctx context.Context, args *struct {
	Input *DeleteReleaseInput
}) (*ReleaseMutationPayloadResolver, error) {
	response, err := deleteReleaseMutation(ctx, args.Input)
	if err != nil {
		return handleReleaseMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateReleaseDeploymentPipeline updates a release
func (r RootResolver) UpdateReleaseDeploymentPipeline(ctx context.Context, args *struct {
	Input *UpdateReleaseDeploymentPipelineInput
}) (*PipelineMutationPayloadResolver, error) {
	response, err := updateReleaseDeploymentPipelineMutation(ctx, args.Input)
	if err != nil {
		return handlePipelineMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* ActivityEvents Query */

// ActivityEvents query returns an activity event connection
func (r RootResolver) ActivityEvents(ctx context.Context, args *ActivityEventConnectionQueryArgs) (*ActivityEventConnectionResolver, error) {
	return activityEventsQuery(ctx, args)
}

// LiveActivityEvents subscribes to activity events
func (r RootResolver) LiveActivityEvents(ctx context.Context, args *struct {
	Input *ActivityEventsSubscriptionInput
}) (<-chan *ActivityEventResolver, error) {
	return r.liveActivityEventsSubscription(ctx, args.Input)
}

/* Pipeline Comment Queries and Mutations */

// CreatePipelineComment creates a new pipeline comment
func (r RootResolver) CreatePipelineComment(ctx context.Context, args *struct {
	Input *CreatePipelineCommentInput
}) (*CommentMutationPayloadResolver, error) {
	response, err := createPipelineCommentMutation(ctx, args.Input)
	if err != nil {
		return handleCommentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdatePipelineComment updates an existing pipeline comment
func (r RootResolver) UpdatePipelineComment(ctx context.Context, args *struct {
	Input *UpdateCommentInput
}) (*CommentMutationPayloadResolver, error) {
	response, err := updateCommentMutation(ctx, args.Input)
	if err != nil {
		return handleCommentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeletePipelineComment deletes a pipeline comment
func (r RootResolver) DeletePipelineComment(ctx context.Context, args *struct {
	Input *DeleteCommentInput
}) (*CommentMutationPayloadResolver, error) {
	response, err := deleteCommentMutation(ctx, args.Input)
	if err != nil {
		return handleCommentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Pipeline and Release Comment Subscriptions */

// CommentEvents subscribes to comment events for a single comment
func (r RootResolver) CommentEvents(ctx context.Context, args *struct {
	Input *CommentEventsSubscriptionInput
}) (<-chan *CommentEventResolver, error) {
	return r.commentEventsSubscription(ctx, args.Input)
}

/* Release Comment Queries and Mutations */

// CreateReleaseComment creates a new release comment
func (r RootResolver) CreateReleaseComment(ctx context.Context, args *struct {
	Input *CreateReleaseCommentInput
}) (*CommentMutationPayloadResolver, error) {
	response, err := createReleaseCommentMutation(ctx, args.Input)
	if err != nil {
		return handleCommentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateReleaseComment updates an existing release comment
func (r RootResolver) UpdateReleaseComment(ctx context.Context, args *struct {
	Input *UpdateCommentInput
}) (*CommentMutationPayloadResolver, error) {
	response, err := updateCommentMutation(ctx, args.Input)
	if err != nil {
		return handleCommentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteReleaseComment deletes a release comment
func (r RootResolver) DeleteReleaseComment(ctx context.Context, args *struct {
	Input *DeleteCommentInput
}) (*CommentMutationPayloadResolver, error) {
	response, err := deleteCommentMutation(ctx, args.Input)
	if err != nil {
		return handleCommentMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* Plugin Queries and Mutations */

// Plugins query returns a plugins connection
func (r RootResolver) Plugins(ctx context.Context, args *PluginConnectionQueryArgs) (*PluginConnectionResolver, error) {
	return pluginsQuery(ctx, args)
}

// PluginDocFile query returns a plugin doc file
func (r RootResolver) PluginDocFile(ctx context.Context, args *PluginDocFileQueryArgs) (*string, error) {
	return pluginDocFileQuery(ctx, args)
}

// CreatePlugin creates a new plugin
func (r RootResolver) CreatePlugin(ctx context.Context, args *struct{ Input *CreatePluginInput }) (*PluginMutationPayloadResolver, error) {
	response, err := createPluginMutation(ctx, args.Input)
	if err != nil {
		return handlePluginMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdatePlugin updates an existing plugin
func (r RootResolver) UpdatePlugin(ctx context.Context, args *struct{ Input *UpdatePluginInput }) (*PluginMutationPayloadResolver, error) {
	response, err := updatePluginMutation(ctx, args.Input)
	if err != nil {
		return handlePluginMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeletePlugin deletes a plugin
func (r RootResolver) DeletePlugin(ctx context.Context, args *struct{ Input *DeletePluginInput }) (*PluginMutationPayloadResolver, error) {
	response, err := deletePluginMutation(ctx, args.Input)
	if err != nil {
		return handlePluginMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* PluginVersion Mutations */

// DeletePluginVersion deletes a plugin version
func (r RootResolver) DeletePluginVersion(ctx context.Context, args *struct{ Input *DeletePluginVersionInput }) (*PluginVersionMutationPayloadResolver, error) {
	response, err := deletePluginVersionMutation(ctx, args.Input)
	if err != nil {
		return handlePluginVersionMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* ToDoItem Queries and Mutations */

// UserToDoItemEvents subscribes to a user's todo item events
func (r RootResolver) UserToDoItemEvents(ctx context.Context) (<-chan *ToDoItemEventResolver, error) {
	return r.userToDoItemEventsSubscription(ctx)
}

// UpdateToDoItem updates a todo item
func (r RootResolver) UpdateToDoItem(ctx context.Context, args *struct{ Input *UpdateToDoItemInput }) (*ToDoItemMutationPayloadResolver, error) {
	response, err := updateToDoItemMutation(ctx, args.Input)
	if err != nil {
		return handleToDoItemMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

/* VCSProvider queries and mutations */

// ResetVCSProviderOAuthToken returns a new OAuth authorization code URL that can
// be used to reset an OAuth token.
func (r RootResolver) ResetVCSProviderOAuthToken(ctx context.Context, args *struct {
	Input *ResetVCSProviderOAuthTokenInput
},
) (*ResetVCSProviderOAuthTokenMutationPayloadResolver, error) {
	response, err := resetVCSProviderOAuthTokenMutation(ctx, args.Input)
	if err != nil {
		return handleResetVCSProviderOAuthTokenMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// CreateVCSProvider creates a new vcs provider
func (r RootResolver) CreateVCSProvider(ctx context.Context,
	args *struct{ Input *CreateVCSProviderInput },
) (*VCSProviderMutationPayloadResolver, error) {
	response, err := createVCSProviderMutation(ctx, args.Input)
	if err != nil {
		return handleVCSProviderMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// UpdateVCSProvider updates a vcs provider
func (r RootResolver) UpdateVCSProvider(ctx context.Context,
	args *struct{ Input *UpdateVCSProviderInput },
) (*VCSProviderMutationPayloadResolver, error) {
	response, err := updateVCSProviderMutation(ctx, args.Input)
	if err != nil {
		return handleVCSProviderMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// DeleteVCSProvider deletes a vcs provider
func (r RootResolver) DeleteVCSProvider(ctx context.Context,
	args *struct{ Input *DeleteVCSProviderInput },
) (*VCSProviderMutationPayloadResolver, error) {
	response, err := deleteVCSProviderMutation(ctx, args.Input)
	if err != nil {
		return handleVCSProviderMutationProblem(err, args.Input.ClientMutationID)
	}

	return response, nil
}

// Metrics returns metrics
func (r RootResolver) Metrics(ctx context.Context, args *MetricConnectionQueryArgs) (*MetricConnectionResolver, error) {
	return metricsQuery(ctx, args)
}

// AggregatedMetricStatistics resolves aggregated statistics for metrics
func (r RootResolver) AggregatedMetricStatistics(ctx context.Context, args *AggregatedMetricStatsQueryArgs) ([]*AggregatedMetricStatisticsResultResolver, error) {
	return aggregatedMetricStatisticsQuery(ctx, args)
}

// MetricStatistics resolves statistics for metrics
func (r RootResolver) MetricStatistics(ctx context.Context, args *MetricStatsQueryArgs) (*MetricStatisticsResolver, error) {
	return metricStatisticsQuery(ctx, args)
}

/* Environment Rules Queries and Mutations */

// CreateEnvironmentRule creates a new environment rule
func (r RootResolver) CreateEnvironmentRule(
	ctx context.Context,
	args *struct{ Input *CreateEnvironmentRuleInput },
) (*EnvironmentRuleMutationPayloadResolver, error) {
	response, err := createEnvironmentRuleMutation(ctx, args.Input)
	if err != nil {
		return handleEnvironmentRuleMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

// UpdateEnvironmentRule updates an existing environment rule
func (r RootResolver) UpdateEnvironmentRule(
	ctx context.Context,
	args *struct{ Input *UpdateEnvironmentRuleInput },
) (*EnvironmentRuleMutationPayloadResolver, error) {
	response, err := updateEnvironmentRuleMutation(ctx, args.Input)
	if err != nil {
		return handleEnvironmentRuleMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

// DeleteEnvironmentRule deletes a environment rule
func (r RootResolver) DeleteEnvironmentRule(
	ctx context.Context,
	args *struct{ Input *DeleteEnvironmentRuleInput },
) (*EnvironmentRuleMutationPayloadResolver, error) {
	response, err := deleteEnvironmentRuleMutation(ctx, args.Input)
	if err != nil {
		return handleEnvironmentRuleMutationProblem(err, args.Input.ClientMutationID)
	}
	return response, nil
}

// Version returns the version of the API and its components
func (r RootResolver) Version(ctx context.Context) (*VersionResolver, error) {
	return versionQuery(ctx)
}
