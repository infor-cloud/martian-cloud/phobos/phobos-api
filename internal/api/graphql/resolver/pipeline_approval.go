package resolver

import (
	"context"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
)

// PipelineApprovalResolver resolves a pipeline approval
type PipelineApprovalResolver struct {
	approval *models.PipelineApproval
}

// ID resolver
func (r *PipelineApprovalResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.PipelineApprovalType, r.approval.Metadata.ID))
}

// Metadata resolver
func (r *PipelineApprovalResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.approval.Metadata}
}

// Approver resolver
func (r *PipelineApprovalResolver) Approver(ctx context.Context) (*MemberResolver, error) {
	return makeMemberResolver(ctx, r.approval.UserID, nil, r.approval.ServiceAccountID)
}

// ApprovalRules resolver
func (r *PipelineApprovalResolver) ApprovalRules(ctx context.Context) ([]*ApprovalRuleResolver, error) {
	service := getApprovalRuleService(ctx)
	rules, err := service.GetApprovalRulesByIDs(ctx, r.approval.ApprovalRuleIDs)
	if err != nil {
		return nil, err
	}
	resolvers := make([]*ApprovalRuleResolver, 0, len(rules))
	for _, rule := range rules {
		resolvers = append(resolvers, &ApprovalRuleResolver{rule})
	}
	return resolvers, nil
}
