package resolver

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// JobEdgeResolver resolves job edges
type JobEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *JobEdgeResolver) Cursor() (string, error) {
	job, ok := r.edge.Node.(models.Job)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&job)
	return *cursor, err
}

// Node returns a job node
func (r *JobEdgeResolver) Node() (*JobResolver, error) {
	job, ok := r.edge.Node.(models.Job)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &JobResolver{job: &job}, nil
}

// JobConnectionResolver resolves a job connection
type JobConnectionResolver struct {
	connection Connection
}

// NewJobConnectionResolver creates a new JobConnectionResolver
func NewJobConnectionResolver(ctx context.Context, input *job.GetJobsInput) (*JobConnectionResolver, error) {
	jobService := getJobService(ctx)

	result, err := jobService.GetJobs(ctx, input)
	if err != nil {
		return nil, err
	}

	jobs := result.Jobs

	// Create edges
	edges := make([]Edge, len(jobs))
	for i, job := range jobs {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: job}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(jobs) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&jobs[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&jobs[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &JobConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *JobConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *JobConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *JobConnectionResolver) Edges() *[]*JobEdgeResolver {
	resolvers := make([]*JobEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &JobEdgeResolver{edge: edge}
	}
	return &resolvers
}

/* Job Query Resolvers */

// JobLogsQueryArgs contains the options for querying job logs
type JobLogsQueryArgs struct {
	StartOffset int32
	Limit       int32
}

// JobTimestampsResolver resolves a job's timestamps
type JobTimestampsResolver struct {
	timestamps *models.JobTimestamps
}

// QueuedAt resolver
func (r *JobTimestampsResolver) QueuedAt() *graphql.Time {
	if r.timestamps.QueuedTimestamp == nil {
		return nil
	}
	return &graphql.Time{Time: *r.timestamps.QueuedTimestamp}
}

// PendingAt resolver
func (r *JobTimestampsResolver) PendingAt() *graphql.Time {
	if r.timestamps.PendingTimestamp == nil {
		return nil
	}
	return &graphql.Time{Time: *r.timestamps.PendingTimestamp}
}

// RunningAt resolver
func (r *JobTimestampsResolver) RunningAt() *graphql.Time {
	if r.timestamps.RunningTimestamp == nil {
		return nil
	}
	return &graphql.Time{Time: *r.timestamps.RunningTimestamp}
}

// FinishedAt resolver
func (r *JobTimestampsResolver) FinishedAt() *graphql.Time {
	if r.timestamps.FinishedTimestamp == nil {
		return nil
	}
	return &graphql.Time{Time: *r.timestamps.FinishedTimestamp}
}

// JobResolver resolves a job resource
type JobResolver struct {
	job *models.Job
}

// ID resolver
func (r *JobResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.JobType, r.job.Metadata.ID))
}

// Status resolver
func (r *JobResolver) Status() string {
	return string(r.job.GetStatus())
}

// Type resolver
func (r *JobResolver) Type() string {
	return string(r.job.Type)
}

// AgentName resolver
func (r *JobResolver) AgentName() *string {
	return r.job.AgentName
}

// AgentType resolver
func (r *JobResolver) AgentType() *string {
	return r.job.AgentType
}

// CancelRequestedAt resolver
func (r *JobResolver) CancelRequestedAt() *graphql.Time {
	if r.job.CancelRequestedTimestamp == nil {
		return nil
	}

	return &graphql.Time{Time: *r.job.CancelRequestedTimestamp}
}

// ForceCancelAvailableAt resolver
func (r *JobResolver) ForceCancelAvailableAt() *graphql.Time {
	if r.job.CancelRequestedTimestamp == nil {
		return nil
	}

	return &graphql.Time{Time: r.job.GetForceCancelAvailableAt()}
}

// Project resolver
func (r *JobResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	project, err := loadProject(ctx, r.job.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// ForceCanceled resolver
func (r *JobResolver) ForceCanceled() bool {
	return r.job.ForceCanceled
}

// ForceCanceledBy resolver
func (r *JobResolver) ForceCanceledBy() *string {
	return r.job.ForceCanceledBy
}

// Metadata resolver
func (r *JobResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.job.Metadata}
}

// Timestamps resolver
func (r *JobResolver) Timestamps() *JobTimestampsResolver {
	return &JobTimestampsResolver{timestamps: &r.job.Timestamps}
}

// LogLastUpdatedAt resolver
func (r *JobResolver) LogLastUpdatedAt(ctx context.Context) (*graphql.Time, error) {
	logStream, err := loadJobLogStream(ctx, r.job.Metadata.ID)
	if err != nil {
		return nil, err
	}

	// Service layer guarantees logStream will not be nil.

	return &graphql.Time{Time: *logStream.Metadata.LastUpdatedTimestamp}, nil
}

// MaxJobDuration resolver
func (r *JobResolver) MaxJobDuration() int32 {
	return r.job.MaxJobDuration
}

// Tags resolver
func (r *JobResolver) Tags() []string {
	return r.job.Tags
}

// LogSize resolver
func (r *JobResolver) LogSize(ctx context.Context) (int32, error) {
	logStream, err := loadJobLogStream(ctx, r.job.Metadata.ID)
	if err != nil {
		return 0, err
	}

	// Service layer guarantees logStream will not be nil.

	return int32(logStream.Size), nil
}

// Logs resolver
func (r *JobResolver) Logs(ctx context.Context, args *JobLogsQueryArgs) (string, error) {
	buffer, err := getJobService(ctx).ReadLogs(ctx, r.job.Metadata.ID, int(args.StartOffset), int(args.Limit))
	if err != nil {
		return "", err
	}
	return string(buffer), nil
}

// Agent resolver
func (r *JobResolver) Agent(ctx context.Context) (*AgentResolver, error) {
	if r.job.AgentID == nil {
		return nil, nil
	}

	agent, err := loadAgent(ctx, *r.job.AgentID)
	if err != nil {
		return nil, err
	}

	return &AgentResolver{agent: agent}, nil
}

// Data resolver
func (r *JobResolver) Data() (*JobDataResolver, error) {
	if r.job.Data == nil {
		return nil, nil
	}

	var result interface{}

	switch r.job.Type {
	case models.JobTaskType:
		taskData, ok := r.job.Data.(*models.JobTaskData)
		if !ok {
			return nil, errors.New("unexpected job data type")
		}
		result = &JobTaskData{
			PipelineID: gid.ToGlobalID(gid.PipelineType, taskData.PipelineID),
			TaskPath:   taskData.TaskPath,
		}
	default:
		return nil, errors.New("unexpected job type")
	}

	return &JobDataResolver{result: result}, nil
}

// Image resolver
func (r *JobResolver) Image() *string {
	return r.job.Image
}

// JobTaskData is the data for a task job type
type JobTaskData struct {
	PipelineID string
	TaskPath   string
}

// JobDataResolver resolves the JobData union type
type JobDataResolver struct {
	result interface{}
}

// ToJobTaskData resolver
func (r *JobDataResolver) ToJobTaskData() (*JobTaskData, bool) {
	res, ok := r.result.(*JobTaskData)
	return res, ok
}

/* Job Mutations */

// JobMutationPayload resolves a job mutation payload
type JobMutationPayload struct {
	ClientMutationID *string
	Job              *models.Job
	Problems         []Problem
}

// JobMutationPayloadResolver resolves a JobMutationPayload
type JobMutationPayloadResolver struct {
	JobMutationPayload
}

// Job field resolver
func (r *JobMutationPayloadResolver) Job() *JobResolver {
	if r.JobMutationPayload.Job == nil {
		return nil
	}

	return &JobResolver{job: r.JobMutationPayload.Job}
}

// CancelJobInput contains the input for canceling a job
type CancelJobInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	Force            *bool
	ID               string
}

func handleJobMutationProblem(e error, clientMutationID *string) (*JobMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := JobMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &JobMutationPayloadResolver{JobMutationPayload: payload}, nil
}

func cancelJobMutation(ctx context.Context, input *CancelJobInput) (*JobMutationPayloadResolver, error) {
	jobService := getJobService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toCancel := &job.CancelJobInput{
		ID: id,
	}

	if input.Force != nil {
		toCancel.Force = *input.Force
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, vErr := strconv.Atoi(input.Metadata.Version)
		if vErr != nil {
			return nil, vErr
		}

		toCancel.Version = &v
	}

	job, err := jobService.CancelJob(ctx, toCancel)
	if err != nil {
		return nil, err
	}

	payload := JobMutationPayload{ClientMutationID: input.ClientMutationID, Job: job, Problems: []Problem{}}
	return &JobMutationPayloadResolver{JobMutationPayload: payload}, nil
}

/* Job Subscriptions */

// JobLogStreamEventResolver resolves a job log event
type JobLogStreamEventResolver struct {
	event *logstream.LogEvent
}

// Completed resolver
func (j *JobLogStreamEventResolver) Completed() bool {
	return j.event.Completed
}

// Size resolver
func (j *JobLogStreamEventResolver) Size() int32 {
	return int32(j.event.Size)
}

// JobCancellationEventResolver resolves a job cancellation event
type JobCancellationEventResolver struct {
	event *job.CancellationEvent
}

// Job resolves a job
func (r *JobCancellationEventResolver) Job() *JobResolver {
	return &JobResolver{job: r.event.Job}
}

// JobEventResolver resolves a job event
type JobEventResolver struct {
	event *job.Event
}

// Action resolves the event action
func (r *JobEventResolver) Action() string {
	return r.event.Action
}

// Job resolves the job
func (r *JobEventResolver) Job() *JobResolver {
	return &JobResolver{job: r.event.Job}
}

// JobEventSubscriptionInput is the input for subscribing to jobs
type JobEventSubscriptionInput struct {
	ProjectID  *string
	AgentID    *string
	PipelineID *string
}

func (r RootResolver) jobEventsSubscription(ctx context.Context, input *JobEventSubscriptionInput) (<-chan *JobEventResolver, error) {
	jobService := getJobService(ctx)
	urnResolver := getURNResolver(ctx)

	var projID, agentID, pipelineID *string

	if input.ProjectID != nil {
		id, err := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if err != nil {
			return nil, err
		}
		projID = &id
	}

	if input.AgentID != nil {
		id, err := urnResolver.ResolveResourceID(ctx, *input.AgentID)
		if err != nil {
			return nil, err
		}
		agentID = &id
	}

	if input.PipelineID != nil {
		id, err := urnResolver.ResolveResourceID(ctx, *input.PipelineID)
		if err != nil {
			return nil, err
		}
		pipelineID = &id
	}

	events, err := jobService.SubscribeToJobs(ctx, &job.SubscribeToJobsInput{
		ProjectID:  projID,
		AgentID:    agentID,
		PipelineID: pipelineID,
	})
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *JobEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &JobEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

// JobLogStreamSubscriptionInput is the input for subscribing to job log events
type JobLogStreamSubscriptionInput struct {
	LastSeenLogSize *int32
	JobID           string
}

func (r RootResolver) jobLogStreamEventsSubscription(ctx context.Context, input *JobLogStreamSubscriptionInput) (<-chan *JobLogStreamEventResolver, error) {
	service := getJobService(ctx)
	urnResolver := getURNResolver(ctx)

	jobID, err := urnResolver.ResolveResourceID(ctx, input.JobID)
	if err != nil {
		return nil, err
	}

	options := &job.LogStreamEventSubscriptionOptions{
		JobID: jobID,
	}
	if input.LastSeenLogSize != nil {
		options.LastSeenLogSize = ptr.Int(int(*input.LastSeenLogSize))
	}

	events, err := service.SubscribeToLogStreamEvents(ctx, options)
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *JobLogStreamEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &JobLogStreamEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

// JobCancellationEventSubscriptionInput is the input for subscribing to job cancellation events
type JobCancellationEventSubscriptionInput struct {
	JobID string
}

func (r RootResolver) jobCancellationEventSubscription(ctx context.Context, input *JobCancellationEventSubscriptionInput) (<-chan *JobCancellationEventResolver, error) {
	service := getJobService(ctx)
	urnResolver := getURNResolver(ctx)

	jobID, err := urnResolver.ResolveResourceID(ctx, input.JobID)
	if err != nil {
		return nil, err
	}

	events, err := service.SubscribeToCancellationEvent(ctx, &job.CancellationSubscriptionsOptions{JobID: jobID})
	if err != nil {
		return nil, err
	}

	outgoing := make(chan *JobCancellationEventResolver)

	go func() {
		for event := range events {
			select {
			case <-ctx.Done():
			case outgoing <- &JobCancellationEventResolver{event: event}:
			}
		}

		close(outgoing)
	}()

	return outgoing, nil
}

/* Job loader */

const jobLoaderKey = "job"

// RegisterJobLoader registers a job loader function
func RegisterJobLoader(collection *loader.Collection) {
	collection.Register(jobLoaderKey, jobBatchFunc)
}

func loadJob(ctx context.Context, id string) (*models.Job, error) {
	ldr, err := loader.Extract(ctx, jobLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	job, ok := data.(models.Job)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &job, nil
}

func jobBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	jobs, err := getJobService(ctx).GetJobsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range jobs {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}

/* JobLogStream loader */

const jobLogStreamLoaderKey = "jobLogStream"

// RegisterJobLogStreamLoader registers a jobLogStream loader function
func RegisterJobLogStreamLoader(collection *loader.Collection) {
	collection.Register(jobLogStreamLoaderKey, jobLogStreamBatchFunc)
}

func loadJobLogStream(ctx context.Context, id string) (*models.LogStream, error) {
	ldr, err := loader.Extract(ctx, jobLogStreamLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	jobLogStream, ok := data.(models.LogStream)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &jobLogStream, nil
}

func jobLogStreamBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	jobLogStreams, err := getJobService(ctx).GetLogStreamsByJobIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range jobLogStreams {
		// Use job ID as the key since that is the ID which was
		// used to query the data
		batch[*result.JobID] = result
	}

	return batch, nil
}
