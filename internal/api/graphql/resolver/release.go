package resolver

import (
	"context"
	"strconv"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/comment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/release"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ReleaseEdgeResolver resolves the Release type
type ReleaseEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ReleaseEdgeResolver) Cursor() (string, error) {
	release, ok := r.edge.Node.(*models.Release)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(release)
	return *cursor, err
}

// Node returns a release node
func (r *ReleaseEdgeResolver) Node() (*ReleaseResolver, error) {
	release, ok := r.edge.Node.(*models.Release)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ReleaseResolver{release: release}, nil
}

// ReleaseConnectionResolver resolves a release connection
type ReleaseConnectionResolver struct {
	connection Connection
}

// NewReleaseConnectionResolver creates a new ReleaseConnectionResolver
func NewReleaseConnectionResolver(ctx context.Context, input *release.GetReleasesInput) (*ReleaseConnectionResolver, error) {
	releaseService := getReleaseService(ctx)

	result, err := releaseService.GetReleases(ctx, input)
	if err != nil {
		return nil, err
	}

	releases := result.Releases

	// Create edges
	edges := make([]Edge, len(releases))
	for i, release := range releases {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: release}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(releases) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(releases[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(releases[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ReleaseConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ReleaseConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ReleaseConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ReleaseConnectionResolver) Edges() *[]*ReleaseEdgeResolver {
	resolvers := make([]*ReleaseEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ReleaseEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ReleaseResolver resolves the Release type
type ReleaseResolver struct {
	release *models.Release
}

// ID returns the release ID
func (r *ReleaseResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ReleaseType, r.release.Metadata.ID))
}

// Metadata resolver
func (r *ReleaseResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.release.Metadata}
}

// CreatedBy resolver
func (r *ReleaseResolver) CreatedBy() string {
	return r.release.CreatedBy
}

// SemanticVersion resolver
func (r *ReleaseResolver) SemanticVersion() string {
	return r.release.SemanticVersion
}

// MetricStats resolver
func (r *ReleaseResolver) MetricStats() *MetricsStatisticsResolver {
	response := &MetricsStatisticsResolver{timeRangeStart: *r.release.Metadata.CreationTimestamp, releaseID: &r.release.Metadata.ID}
	return response
}

// Notes resolver
func (r *ReleaseResolver) Notes(ctx context.Context) (string, error) {
	service := getReleaseService(ctx)
	notes, err := service.GetReleaseNotes(ctx, r.release.Metadata.ID)
	if err != nil {
		return "", err
	}
	return notes, err
}

// Project resolver
func (r *ReleaseResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	project, err := loadProject(ctx, r.release.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// Pipeline resolver
func (r *ReleaseResolver) Pipeline(ctx context.Context) (*PipelineResolver, error) {
	// Releases aren't associated with a pipeline, so we need to get the pipeline by release ID.
	pipeline, err := getPipelineService(ctx).GetPipelineByReleaseID(ctx, r.release.Metadata.ID)
	if err != nil {
		return nil, err
	}

	return &PipelineResolver{pipeline: pipeline}, nil
}

// DeploymentPipelines resolver
func (r *ReleaseResolver) DeploymentPipelines(ctx context.Context, args *PipelineConnectionQueryArgs) (*PipelineConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &pipeline.GetPipelinesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         r.release.ProjectID,
		EnvironmentName:   args.EnvironmentName,
		ReleaseID:         &r.release.Metadata.ID,
		PipelineTypes:     []models.PipelineType{models.DeploymentPipelineType},
	}

	if args.Sort != nil {
		sort := db.PipelineSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewPipelineConnectionResolver(ctx, input)
}

// Threads resolver
func (r *ReleaseResolver) Threads(
	ctx context.Context,
	args *ConnectionQueryArgs,
) (*ThreadConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	sortBy := db.ThreadSortableFieldCreatedAtDesc
	input := &comment.GetThreadsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Sort:              &sortBy,
		ReleaseID:         &r.release.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.ThreadSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewThreadConnectionResolver(ctx, input)
}

// Participants resolver
func (r *ReleaseResolver) Participants(ctx context.Context) ([]*MemberResolver, error) {
	resolvers := []*MemberResolver{}

	for ix := range r.release.UserParticipantIDs {
		resolver, err := makeMemberResolver(ctx, &r.release.UserParticipantIDs[ix], nil, nil)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, resolver)
	}

	for ix := range r.release.TeamParticipantIDs {
		resolver, err := makeMemberResolver(ctx, nil, &r.release.TeamParticipantIDs[ix], nil)
		if err != nil {
			return nil, err
		}
		resolvers = append(resolvers, resolver)
	}

	return resolvers, nil
}

// DueDate resolver
func (r *ReleaseResolver) DueDate() *graphql.Time {
	if r.release.DueDate == nil {
		return nil
	}

	return &graphql.Time{Time: *r.release.DueDate}
}

// Completed resolver
func (r *ReleaseResolver) Completed(ctx context.Context) (bool, error) {
	pipeline, err := getPipelineService(ctx).GetPipelineByReleaseID(ctx, r.release.Metadata.ID)
	if err != nil {
		return false, err
	}

	completed := false
	switch pipeline.Status {
	case statemachine.FailedNodeStatus, statemachine.CanceledNodeStatus, statemachine.SucceededNodeStatus, statemachine.SkippedNodeStatus:
		completed = true
	}

	return completed, nil
}

// PreRelease resolver
func (r *ReleaseResolver) PreRelease() bool {
	return r.release.PreRelease
}

// Latest resolver
func (r *ReleaseResolver) Latest() bool {
	return r.release.Latest
}

// LifecyclePRN resolver
func (r *ReleaseResolver) LifecyclePRN() string {
	return r.release.LifecyclePRN
}

// ActivityEvents resolver
func (r *ReleaseResolver) ActivityEvents(ctx context.Context, args *ActivityEventConnectionQueryArgs) (*ActivityEventConnectionResolver, error) {
	input, err := getActivityEventsInputFromQueryArgs(ctx, args)
	if err != nil {
		return nil, err
	}

	// Filter to this release
	input.ProjectID = &r.release.ProjectID
	input.ReleaseTargetID = &r.release.Metadata.ID

	return NewActivityEventConnectionResolver(ctx, input)
}

/* Release Mutations */

// ReleaseMutationPayload is the response payload for a release mutation
type ReleaseMutationPayload struct {
	ClientMutationID *string
	Release          *models.Release
	Problems         []Problem
}

// ReleaseMutationPayloadResolver resolves a ReleaseMutationPayload
type ReleaseMutationPayloadResolver struct {
	ReleaseMutationPayload
}

// Release field resolver
func (r *ReleaseMutationPayloadResolver) Release() *ReleaseResolver {
	if r.ReleaseMutationPayload.Release == nil {
		return nil
	}

	return &ReleaseResolver{release: r.ReleaseMutationPayload.Release}
}

// DeploymentTemplateInput is the input for supplying a deployment template.
type DeploymentTemplateInput struct {
	EnvironmentName    string
	PipelineTemplateID string
}

// CreateReleaseInput contains the input for creating a new release
type CreateReleaseInput struct {
	ClientMutationID    *string
	Notes               *string
	DueDate             *graphql.Time
	VariableSetRevision *string
	ProjectID           string
	LifecycleID         string
	SemanticVersion     string
	DeploymentTemplates []DeploymentTemplateInput
	Variables           []release.VariableInput
	UserParticipants    []string
	TeamParticipants    []string
}

// UpdateReleaseInput contains the input for updating a release
type UpdateReleaseInput struct {
	ClientMutationID *string
	Notes            *string
	DueDate          *graphql.Time
	ReleaseID        string
}

// DeleteReleaseInput contains the input for deleting a release
type DeleteReleaseInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

// UpdateReleaseDeploymentPipelineInput contains the input for updating a release deployment
type UpdateReleaseDeploymentPipelineInput struct {
	ClientMutationID   *string
	PipelineTemplateID *string
	Variables          *[]pipeline.Variable
	ReleaseID          string
	EnvironmentName    string
}

// ReleaseParticipantInput contains the input for a release participant.
type ReleaseParticipantInput struct {
	ClientMutationID *string
	UserID           *string
	TeamID           *string
	ReleaseID        string
}

func handleReleaseMutationProblem(e error, clientMutationID *string) (*ReleaseMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := ReleaseMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ReleaseMutationPayloadResolver{ReleaseMutationPayload: payload}, nil
}

func createReleaseMutation(ctx context.Context, input *CreateReleaseInput) (*ReleaseMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	releaseService := getReleaseService(ctx)

	projectID, err := urnResolver.ResolveResourceID(ctx, input.ProjectID)
	if err != nil {
		return nil, err
	}

	lifecycleID, err := urnResolver.ResolveResourceID(ctx, input.LifecycleID)
	if err != nil {
		return nil, err
	}

	// Convert deployment templates
	deploymentTemplates := make([]release.DeploymentTemplateInput, len(input.DeploymentTemplates))
	for i, template := range input.DeploymentTemplates {
		templateID, rErr := urnResolver.ResolveResourceID(ctx, template.PipelineTemplateID)
		if rErr != nil {
			return nil, rErr
		}
		deploymentTemplates[i] = release.DeploymentTemplateInput{
			EnvironmentName:    template.EnvironmentName,
			PipelineTemplateID: templateID,
		}
	}

	// Convert participants
	userIDs, err := convertPRNList(ctx, input.UserParticipants)
	if err != nil {
		return nil, err
	}

	teamIDs, err := convertPRNList(ctx, input.TeamParticipants)
	if err != nil {
		return nil, err
	}

	toCreate := &release.CreateReleaseInput{
		ProjectID:           projectID,
		LifecycleID:         lifecycleID,
		SemanticVersion:     input.SemanticVersion,
		DeploymentTemplates: deploymentTemplates,
		Variables:           input.Variables,
		VariableSetRevision: input.VariableSetRevision,
		UserParticipantIDs:  userIDs,
		TeamParticipantIDs:  teamIDs,
	}

	if input.DueDate != nil {
		toCreate.DueDate = &input.DueDate.Time
	}

	if input.Notes != nil {
		toCreate.Notes = *input.Notes
	}

	createdRelease, err := releaseService.CreateRelease(ctx, toCreate)
	if err != nil {
		return nil, err
	}

	payload := ReleaseMutationPayload{ClientMutationID: input.ClientMutationID, Release: createdRelease, Problems: []Problem{}}
	return &ReleaseMutationPayloadResolver{ReleaseMutationPayload: payload}, nil
}

func updateReleaseMutation(ctx context.Context, input *UpdateReleaseInput) (*ReleaseMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	releaseService := getReleaseService(ctx)

	releaseID, err := urnResolver.ResolveResourceID(ctx, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	options := &release.UpdateReleaseInput{
		ReleaseID: releaseID,
		Notes:     input.Notes,
	}

	if input.DueDate != nil {
		options.DueDate = &input.DueDate.Time
	}

	updatedRelease, err := releaseService.UpdateRelease(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := ReleaseMutationPayload{ClientMutationID: input.ClientMutationID, Release: updatedRelease, Problems: []Problem{}}
	return &ReleaseMutationPayloadResolver{ReleaseMutationPayload: payload}, nil
}

func deleteReleaseMutation(ctx context.Context, input *DeleteReleaseInput) (*ReleaseMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	releaseService := getReleaseService(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	gotRelease, err := releaseService.GetReleaseByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &release.DeleteReleaseInput{
		ID: gotRelease.Metadata.ID,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, vErr := strconv.Atoi(input.Metadata.Version)
		if vErr != nil {
			return nil, vErr
		}

		toDelete.MetadataVersion = &v
	}

	if err = releaseService.DeleteRelease(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := ReleaseMutationPayload{ClientMutationID: input.ClientMutationID, Release: gotRelease, Problems: []Problem{}}
	return &ReleaseMutationPayloadResolver{ReleaseMutationPayload: payload}, nil
}

func updateReleaseDeploymentPipelineMutation(ctx context.Context, input *UpdateReleaseDeploymentPipelineInput) (*PipelineMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	releaseService := getReleaseService(ctx)

	releaseID, err := urnResolver.ResolveResourceID(ctx, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	options := &release.UpdateDeploymentPipelineInput{
		ReleaseID:       releaseID,
		EnvironmentName: input.EnvironmentName,
	}

	if input.Variables != nil {
		options.Variables = *input.Variables
	}

	if input.PipelineTemplateID != nil {
		templateID, rErr := urnResolver.ResolveResourceID(ctx, *input.PipelineTemplateID)
		if rErr != nil {
			return nil, rErr
		}
		options.PipelineTemplateID = &templateID
	}

	pipeline, err := releaseService.UpdateDeploymentPipeline(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := PipelineMutationPayload{ClientMutationID: input.ClientMutationID, Pipeline: pipeline, Problems: []Problem{}}
	return &PipelineMutationPayloadResolver{PipelineMutationPayload: payload}, nil
}

func addParticipantToReleaseMutation(ctx context.Context, input *ReleaseParticipantInput) (*ReleaseMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	releaseService := getReleaseService(ctx)

	releaseID, err := urnResolver.ResolveResourceID(ctx, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	options := &release.ParticipantInput{
		ReleaseID: releaseID,
	}

	if input.UserID != nil {
		userID, rErr := urnResolver.ResolveResourceID(ctx, *input.UserID)
		if rErr != nil {
			return nil, rErr
		}
		options.UserID = &userID
	}

	if input.TeamID != nil {
		teamID, rErr := urnResolver.ResolveResourceID(ctx, *input.TeamID)
		if rErr != nil {
			return nil, rErr
		}
		options.TeamID = &teamID
	}

	updatedRelease, err := releaseService.AddParticipantToRelease(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := ReleaseMutationPayload{ClientMutationID: input.ClientMutationID, Release: updatedRelease, Problems: []Problem{}}
	return &ReleaseMutationPayloadResolver{ReleaseMutationPayload: payload}, nil
}

func removeParticipantFromReleaseMutation(ctx context.Context, input *ReleaseParticipantInput) (*ReleaseMutationPayloadResolver, error) {
	urnResolver := getURNResolver(ctx)
	releaseService := getReleaseService(ctx)

	releaseID, err := urnResolver.ResolveResourceID(ctx, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	options := &release.ParticipantInput{
		ReleaseID: releaseID,
	}

	if input.UserID != nil {
		userID, rErr := urnResolver.ResolveResourceID(ctx, *input.UserID)
		if rErr != nil {
			return nil, rErr
		}
		options.UserID = &userID
	}

	if input.TeamID != nil {
		teamID, rErr := urnResolver.ResolveResourceID(ctx, *input.TeamID)
		if rErr != nil {
			return nil, rErr
		}
		options.TeamID = &teamID
	}

	updatedRelease, err := releaseService.RemoveParticipantFromRelease(ctx, options)
	if err != nil {
		return nil, err
	}

	payload := ReleaseMutationPayload{ClientMutationID: input.ClientMutationID, Release: updatedRelease, Problems: []Problem{}}
	return &ReleaseMutationPayloadResolver{ReleaseMutationPayload: payload}, nil
}

/* Release loader */

const releaseLoaderKey = "release"

// RegisterReleaseLoader registers a release loader function
func RegisterReleaseLoader(collection *loader.Collection) {
	collection.Register(releaseLoaderKey, releaseBatchFunc)
}

func loadRelease(ctx context.Context, id string) (*models.Release, error) {
	ldr, err := loader.Extract(ctx, releaseLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	release, ok := data.(*models.Release)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return release, nil
}

func releaseBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	releases, err := getReleaseService(ctx).GetReleasesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range releases {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
