package resolver

import (
	"context"
	"strconv"

	"github.com/aws/smithy-go/ptr"
	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/approvalrule"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/project"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/projectvariable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/release"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ProjectConnectionQueryArgs are used to query a project connection
type ProjectConnectionQueryArgs struct {
	ConnectionQueryArgs
	Search *string
	Latest *bool
}

// ProjectEdgeResolver resolves project edges
type ProjectEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *ProjectEdgeResolver) Cursor() (string, error) {
	proj, ok := r.edge.Node.(models.Project)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&proj)
	return *cursor, err
}

// Node returns a project node
func (r *ProjectEdgeResolver) Node() (*ProjectResolver, error) {
	proj, ok := r.edge.Node.(models.Project)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &ProjectResolver{project: &proj}, nil
}

// ProjectConnectionResolver resolves a project connection
type ProjectConnectionResolver struct {
	connection Connection
}

// NewProjectConnectionResolver creates a new ProjectConnectionResolver
func NewProjectConnectionResolver(ctx context.Context, input *project.GetProjectsInput) (*ProjectConnectionResolver, error) {
	projectService := getProjectService(ctx)

	result, err := projectService.GetProjects(ctx, input)
	if err != nil {
		return nil, err
	}

	projects := result.Projects

	// Create edges
	edges := make([]Edge, len(projects))
	for i, project := range projects {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: project}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(projects) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&projects[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&projects[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &ProjectConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *ProjectConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *ProjectConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *ProjectConnectionResolver) Edges() *[]*ProjectEdgeResolver {
	resolvers := make([]*ProjectEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &ProjectEdgeResolver{edge: edge}
	}
	return &resolvers
}

// ProjectResolver resolves a project resource
type ProjectResolver struct {
	project *models.Project
}

// ID resolver
func (r *ProjectResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.ProjectType, r.project.Metadata.ID))
}

// Name resolver
func (r *ProjectResolver) Name() string {
	return r.project.Name
}

// Description resolver
func (r *ProjectResolver) Description() string {
	return r.project.Description
}

// Metadata resolver
func (r *ProjectResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.project.Metadata}
}

// CreatedBy resolver
func (r *ProjectResolver) CreatedBy() string {
	return r.project.CreatedBy
}

// OrganizationID resolver
func (r *ProjectResolver) OrganizationID() string {
	return gid.ToGlobalID(gid.OrganizationType, r.project.OrgID)
}

// OrganizationName resolver
func (r *ProjectResolver) OrganizationName() string {
	return r.project.GetOrgName()
}

// MetricStats resolver
func (r *ProjectResolver) MetricStats(input *struct {
	TimeRangeStart graphql.Time
	TimeRangeEnd   *graphql.Time
}) *MetricsStatisticsResolver {
	response := &MetricsStatisticsResolver{timeRangeStart: input.TimeRangeStart.Time, projectID: &r.project.Metadata.ID}
	if input.TimeRangeEnd != nil {
		response.timeRangeEnd = &input.TimeRangeEnd.Time
	}
	return response
}

// Organization resolver
func (r *ProjectResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	org, err := loadOrganization(ctx, r.project.OrgID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: org}, nil
}

// LatestVariableSet resolver
func (r *ProjectResolver) LatestVariableSet(ctx context.Context) (*ProjectVariableSetResolver, error) {
	service := getProjectVariableService(ctx)

	resp, err := service.GetProjectVariableSets(ctx, &projectvariable.GetProjectVariableSetsInput{
		ProjectID: r.project.Metadata.ID,
		Latest:    ptr.Bool(true),
	})
	if err != nil {
		return nil, err
	}

	if len(resp.ProjectVariableSets) == 0 {
		return nil, nil
	}

	return &ProjectVariableSetResolver{variableSet: resp.ProjectVariableSets[0]}, nil
}

// PipelineTemplates resolver
func (r *ProjectResolver) PipelineTemplates(ctx context.Context,
	args *PipelineTemplateConnectionQueryArgs) (*PipelineTemplateConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := pipelinetemplate.GetPipelineTemplatesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         r.project.Metadata.ID,
		Versioned:         args.Versioned,
		Name:              args.Name,
		Search:            args.Search,
		Latest:            args.Latest,
	}

	if args.Sort != nil {
		sort := db.PipelineTemplateSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewPipelineTemplateConnectionResolver(ctx, &input)
}

// Pipelines resolver
func (r *ProjectResolver) Pipelines(ctx context.Context, args *PipelineConnectionQueryArgs) (*PipelineConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := pipeline.GetPipelinesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         r.project.Metadata.ID,
		Started:           args.Started,
	}

	if args.Sort != nil {
		sort := db.PipelineSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewPipelineConnectionResolver(ctx, &input)
}

// Memberships resolver
func (r *ProjectResolver) Memberships(ctx context.Context, args *ConnectionQueryArgs) (*MembershipConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &membership.GetMembershipsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
		MembershipScopes:  []models.ScopeType{models.OrganizationScope, models.ProjectScope},
	}

	if args.Sort != nil {
		sort := db.MembershipSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewMembershipConnectionResolver(ctx, input)
}

// Jobs resolver
func (r *ProjectResolver) Jobs(ctx context.Context, args *ConnectionQueryArgs) (*JobConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := job.GetJobsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.JobSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewJobConnectionResolver(ctx, &input)
}

// Releases resolver
func (r *ProjectResolver) Releases(ctx context.Context, args *ProjectConnectionQueryArgs) (*ReleaseConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &release.GetReleasesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
		Latest:            args.Latest,
	}

	if args.Sort != nil {
		sort := db.ReleaseSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewReleaseConnectionResolver(ctx, input)
}

// ActivityEvents resolver
func (r *ProjectResolver) ActivityEvents(ctx context.Context, args *ActivityEventConnectionQueryArgs) (*ActivityEventConnectionResolver, error) {
	input, err := getActivityEventsInputFromQueryArgs(ctx, args)
	if err != nil {
		return nil, err
	}

	// Filter to this project
	input.ProjectID = &r.project.Metadata.ID

	return NewActivityEventConnectionResolver(ctx, input)
}

// EnvironmentNames resolver
func (r *ProjectResolver) EnvironmentNames(ctx context.Context) ([]string, error) {
	environments, err := getEnvironmentService(ctx).GetEnvironments(ctx, &environment.GetEnvironmentsInput{
		ProjectID: &r.project.Metadata.ID,
	})
	if err != nil {
		return nil, err
	}

	// Deduplicate environment names
	environmentNames := []string{}
	for _, env := range environments.Environments {
		environmentNames = append(environmentNames, env.Name)
	}

	return environmentNames, nil
}

// Environments resolvers
func (r *ProjectResolver) Environments(ctx context.Context, args *ConnectionQueryArgs) (*EnvironmentConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := environment.GetEnvironmentsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.EnvironmentSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewEnvironmentConnectionResolver(ctx, &input)
}

// VCSProviders resolver
func (r *ProjectResolver) VCSProviders(ctx context.Context, args *VCSProviderConnectionQueryArgs) (*VCSProviderConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &vcs.GetVCSProvidersInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
		Search:            args.Search,
	}

	if args.Sort != nil {
		sort := db.VCSProviderSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewVCSProviderConnectionResolver(ctx, input)
}

// EnvironmentRules resolver
func (r *ProjectResolver) EnvironmentRules(ctx context.Context, args *ConnectionQueryArgs) (*EnvironmentRuleConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &environment.GetEnvironmentRulesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
	}

	if args.Sort != nil {
		sort := db.EnvironmentRuleSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewEnvironmentRuleConnectionResolver(ctx, input)
}

// ApprovalRules resolver
func (r *ProjectResolver) ApprovalRules(
	ctx context.Context,
	args *ApprovalRuleConnectionQueryArgs,
) (*ApprovalRuleConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &approvalrule.GetApprovalRulesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
	}

	if args.Scopes != nil {
		input.ApprovalRuleScopes = *args.Scopes
	}

	if args.Sort != nil {
		sort := db.ApprovalRuleSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewApprovalRuleConnectionResolver(ctx, input)
}

// VariableSets resolver
func (r *ProjectResolver) VariableSets(
	ctx context.Context,
	args *ProjectVariableSetConnectionQueryArgs,
) (*ProjectVariableSetConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &projectvariable.GetProjectVariableSetsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         r.project.Metadata.ID,
		RevisionSearch:    args.RevisionSearch,
	}

	if args.Sort != nil {
		sort := db.ProjectVariableSetSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewProjectVariableSetConnectionResolver(ctx, input)
}

// LifecycleTemplates resolver
func (r *ProjectResolver) LifecycleTemplates(
	ctx context.Context,
	args *LifecycleTemplateConnectionQueryArgs,
) (*LifecycleTemplateConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &lifecycletemplate.GetLifecycleTemplatesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
	}

	if args.Scopes != nil {
		input.LifecycleTemplateScopes = *args.Scopes
	}

	if args.Sort != nil {
		sort := db.LifecycleTemplateSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewLifecycleTemplateConnectionResolver(ctx, input)
}

// ReleaseLifecycles resolver
func (r *ProjectResolver) ReleaseLifecycles(
	ctx context.Context,
	args *ReleaseLifecycleConnectionQueryArgs,
) (*ReleaseLifecycleConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &releaselifecycle.GetReleaseLifecyclesInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
		Search:            args.Search,
	}

	if args.Scopes != nil {
		input.ReleaseLifecycleScopes = *args.Scopes
	}

	if args.Sort != nil {
		sort := db.ReleaseLifecycleSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewReleaseLifecycleConnectionResolver(ctx, input)
}

// ServiceAccounts resolver
func (r *ProjectResolver) ServiceAccounts(
	ctx context.Context,
	args *ServiceAccountConnectionQueryArgs,
) (*ServiceAccountConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := &serviceaccount.GetServiceAccountsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		ProjectID:         &r.project.Metadata.ID,
		Search:            args.Search,
	}

	if args.Scopes != nil {
		input.ServiceAccountScopes = *args.Scopes
	}

	if args.Sort != nil {
		sort := db.ServiceAccountSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewServiceAccountConnectionResolver(ctx, input)
}

func projectsQuery(ctx context.Context, args *ProjectConnectionQueryArgs) (*ProjectConnectionResolver, error) {
	if err := args.Validate(); err != nil {
		return nil, err
	}

	input := project.GetProjectsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		Search:            args.Search,
	}

	if args.Sort != nil {
		sort := db.ProjectSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewProjectConnectionResolver(ctx, &input)
}

/* Project Mutations */

// ProjectMutationPayload is the response payload for a project mutation
type ProjectMutationPayload struct {
	ClientMutationID *string
	Project          *models.Project
	Problems         []Problem
}

// ProjectMutationPayloadResolver resolves a ProjectMutationPayload
type ProjectMutationPayloadResolver struct {
	ProjectMutationPayload
}

// Project field resolver
func (r *ProjectMutationPayloadResolver) Project() *ProjectResolver {
	if r.ProjectMutationPayload.Project == nil {
		return nil
	}

	return &ProjectResolver{project: r.ProjectMutationPayload.Project}
}

// CreateProjectInput contains the input for creating a new project
type CreateProjectInput struct {
	ClientMutationID *string
	Name             string
	Description      string
	OrgName          string
}

// UpdateProjectInput contains the input for updating a project
type UpdateProjectInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	Description      *string
	ID               string
}

// DeleteProjectInput contains the input for deleting a project
type DeleteProjectInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handleProjectMutationProblem(e error, clientMutationID *string) (*ProjectMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := ProjectMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &ProjectMutationPayloadResolver{ProjectMutationPayload: payload}, nil
}

func createProjectMutation(ctx context.Context, input *CreateProjectInput) (*ProjectMutationPayloadResolver, error) {

	org, err := getOrganizationService(ctx).GetOrganizationByPRN(ctx, models.ResourceType.BuildPRN(models.OrganizationResource, input.OrgName))
	if err != nil {
		return nil, err
	}

	projectCreateOptions := &project.CreateProjectInput{
		Name:        input.Name,
		Description: input.Description,
		OrgID:       org.Metadata.ID,
	}

	createdProject, err := getProjectService(ctx).CreateProject(ctx, projectCreateOptions)
	if err != nil {
		return nil, err
	}

	payload := ProjectMutationPayload{ClientMutationID: input.ClientMutationID, Project: createdProject, Problems: []Problem{}}
	return &ProjectMutationPayloadResolver{ProjectMutationPayload: payload}, nil
}

func updateProjectMutation(ctx context.Context, input *UpdateProjectInput) (*ProjectMutationPayloadResolver, error) {
	projectService := getProjectService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	toUpdate := &project.UpdateProjectInput{
		ID:          id,
		Description: input.Description,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, cErr := strconv.Atoi(input.Metadata.Version)
		if cErr != nil {
			return nil, cErr
		}

		toUpdate.Version = &v
	}

	updatedProj, err := projectService.UpdateProject(ctx, toUpdate)
	if err != nil {
		return nil, err
	}

	payload := ProjectMutationPayload{ClientMutationID: input.ClientMutationID, Project: updatedProj, Problems: []Problem{}}
	return &ProjectMutationPayloadResolver{ProjectMutationPayload: payload}, nil
}

func deleteProjectMutation(ctx context.Context, input *DeleteProjectInput) (*ProjectMutationPayloadResolver, error) {
	projectService := getProjectService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	gotProj, err := projectService.GetProjectByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &project.DeleteProjectInput{
		ID: gotProj.Metadata.ID,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := projectService.DeleteProject(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := ProjectMutationPayload{ClientMutationID: input.ClientMutationID, Project: gotProj, Problems: []Problem{}}
	return &ProjectMutationPayloadResolver{ProjectMutationPayload: payload}, nil
}

/* Project loader */

const projectLoaderKey = "project"

// RegisterProjectLoader registers a project loader function
func RegisterProjectLoader(collection *loader.Collection) {
	collection.Register(projectLoaderKey, projectBatchFunc)
}

func loadProject(ctx context.Context, id string) (*models.Project, error) {
	ldr, err := loader.Extract(ctx, projectLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	project, ok := data.(models.Project)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &project, nil
}

func projectBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	projects, err := getProjectService(ctx).GetProjectsByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range projects {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
