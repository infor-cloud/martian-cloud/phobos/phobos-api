package resolver

import (
	"context"
	"io"
	"strconv"
	"strings"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// PipelineTemplateConnectionQueryArgs are used to query a pipeline template connection
type PipelineTemplateConnectionQueryArgs struct {
	ConnectionQueryArgs
	Name      *string
	Search    *string
	Versioned *bool
	Latest    *bool
}

// PipelineTemplateEdgeResolver resolves pipeline template edges
type PipelineTemplateEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *PipelineTemplateEdgeResolver) Cursor() (string, error) {
	pipelineTemplate, ok := r.edge.Node.(models.PipelineTemplate)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(&pipelineTemplate)
	return *cursor, err
}

// Node returns a pipeline template node
func (r *PipelineTemplateEdgeResolver) Node() (*PipelineTemplateResolver, error) {
	pipelineTemplate, ok := r.edge.Node.(models.PipelineTemplate)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &PipelineTemplateResolver{pipelineTemplate: &pipelineTemplate}, nil
}

// PipelineTemplateConnectionResolver resolves a pipeline template connection
type PipelineTemplateConnectionResolver struct {
	connection Connection
}

// NewPipelineTemplateConnectionResolver creates a new PipelineTemplateConnectionResolver
func NewPipelineTemplateConnectionResolver(ctx context.Context, input *pipelinetemplate.GetPipelineTemplatesInput) (*PipelineTemplateConnectionResolver, error) {
	pipelineTemplateService := getPipelineTemplateService(ctx)

	result, err := pipelineTemplateService.GetPipelineTemplates(ctx, input)
	if err != nil {
		return nil, err
	}

	pipelineTemplates := result.PipelineTemplates

	// Create edges
	edges := make([]Edge, len(pipelineTemplates))
	for i, pipelineTemplate := range pipelineTemplates {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: pipelineTemplate}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(pipelineTemplates) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(&pipelineTemplates[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(&pipelineTemplates[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &PipelineTemplateConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *PipelineTemplateConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *PipelineTemplateConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *PipelineTemplateConnectionResolver) Edges() *[]*PipelineTemplateEdgeResolver {
	resolvers := make([]*PipelineTemplateEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &PipelineTemplateEdgeResolver{edge: edge}
	}
	return &resolvers
}

// PipelineTemplateResolver resolves a pipeline template resource
type PipelineTemplateResolver struct {
	pipelineTemplate *models.PipelineTemplate
}

// ID resolver
func (r *PipelineTemplateResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.PipelineTemplateType, r.pipelineTemplate.Metadata.ID))
}

// Metadata resolver
func (r *PipelineTemplateResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.pipelineTemplate.Metadata}
}

// CreatedBy resolver
func (r *PipelineTemplateResolver) CreatedBy() string {
	return r.pipelineTemplate.CreatedBy
}

// Project resolver
func (r *PipelineTemplateResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	project, err := loadProject(ctx, r.pipelineTemplate.ProjectID)
	if err != nil {
		return nil, err
	}

	return &ProjectResolver{project: project}, nil
}

// Status resolver
func (r *PipelineTemplateResolver) Status() string {
	return string(r.pipelineTemplate.Status)
}

// HCLData resolver
func (r *PipelineTemplateResolver) HCLData(ctx context.Context) (string, error) {

	// If the pipeline template's status indicates that no data has been uploaded, just return empty string.
	// Otherwise, the attempt to fetch non-existent data will return an error.
	if r.pipelineTemplate.Status != models.PipelineTemplateUploaded {
		return "", nil
	}

	hclDataRdr, err := getPipelineTemplateService(ctx).GetPipelineTemplateData(ctx, r.pipelineTemplate.Metadata.ID)
	if err != nil {
		return "", err
	}

	hclData, err := io.ReadAll(hclDataRdr)
	if err != nil {
		return "", err
	}

	return string(hclData), nil
}

// Versioned resolver
func (r *PipelineTemplateResolver) Versioned() bool {
	return r.pipelineTemplate.Versioned
}

// Name resolver
func (r *PipelineTemplateResolver) Name() *string {
	return r.pipelineTemplate.Name
}

// SemanticVersion resolver
func (r *PipelineTemplateResolver) SemanticVersion() *string {
	return r.pipelineTemplate.SemanticVersion
}

// Latest resolver
func (r *PipelineTemplateResolver) Latest() bool {
	return r.pipelineTemplate.Latest
}

/* PipelineTemplate Mutations */

// PipelineTemplateMutationPayload is the response payload for a pipeline template mutation
type PipelineTemplateMutationPayload struct {
	ClientMutationID *string
	PipelineTemplate *models.PipelineTemplate
	Problems         []Problem
}

// PipelineTemplateMutationPayloadResolver resolves a PipelineTemplateMutationPayload
type PipelineTemplateMutationPayloadResolver struct {
	PipelineTemplateMutationPayload
}

// PipelineTemplate field resolver
func (r *PipelineTemplateMutationPayloadResolver) PipelineTemplate() *PipelineTemplateResolver {
	if r.PipelineTemplateMutationPayload.PipelineTemplate == nil {
		return nil
	}

	return &PipelineTemplateResolver{pipelineTemplate: r.PipelineTemplateMutationPayload.PipelineTemplate}
}

// CreatePipelineTemplateInput contains the input for creating a new pipeline template
type CreatePipelineTemplateInput struct {
	ClientMutationID *string
	Name             *string
	SemanticVersion  *string
	ProjectID        string
	HCLData          string
	Versioned        bool
}

// DeletePipelineTemplateInput contains the input for deleting a pipeline template
type DeletePipelineTemplateInput struct {
	ClientMutationID *string
	Metadata         *MetadataInput
	ID               string
}

func handlePipelineTemplateMutationProblem(e error, clientMutationID *string) (*PipelineTemplateMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := PipelineTemplateMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &PipelineTemplateMutationPayloadResolver{PipelineTemplateMutationPayload: payload}, nil
}

func createPipelineTemplateMutation(ctx context.Context, input *CreatePipelineTemplateInput) (*PipelineTemplateMutationPayloadResolver, error) {
	projectID, err := getURNResolver(ctx).ResolveResourceID(ctx, input.ProjectID)
	if err != nil {
		return nil, err
	}

	templateService := getPipelineTemplateService(ctx)

	pipelineTemplateCreateOptions := &pipelinetemplate.CreatePipelineTemplateInput{
		ProjectID:       projectID,
		Name:            input.Name,
		SemanticVersion: input.SemanticVersion,
		Versioned:       input.Versioned,
	}

	createdPipelineTemplate, err := templateService.CreatePipelineTemplate(ctx, pipelineTemplateCreateOptions)
	if err != nil {
		return nil, err
	}

	if err = templateService.UploadPipelineTemplate(ctx, &pipelinetemplate.UploadPipelineTemplateInput{
		ID:     createdPipelineTemplate.Metadata.ID,
		Reader: strings.NewReader(input.HCLData),
	}); err != nil {
		return nil, err
	}

	pipelineTemplate, err := templateService.GetPipelineTemplateByID(ctx, createdPipelineTemplate.Metadata.ID)
	if err != nil {
		return nil, io.ErrClosedPipe
	}

	payload := PipelineTemplateMutationPayload{ClientMutationID: input.ClientMutationID, PipelineTemplate: pipelineTemplate, Problems: []Problem{}}
	return &PipelineTemplateMutationPayloadResolver{PipelineTemplateMutationPayload: payload}, nil
}

func deletePipelineTemplateMutation(ctx context.Context, input *DeletePipelineTemplateInput) (*PipelineTemplateMutationPayloadResolver, error) {
	service := getPipelineTemplateService(ctx)
	urnResolver := getURNResolver(ctx)

	id, err := urnResolver.ResolveResourceID(ctx, input.ID)
	if err != nil {
		return nil, err
	}

	template, err := service.GetPipelineTemplateByID(ctx, id)
	if err != nil {
		return nil, err
	}

	toDelete := &pipelinetemplate.DeletePipelineTemplateInput{
		ID: id,
	}

	// Check if resource version is specified
	if input.Metadata != nil {
		v, err := strconv.Atoi(input.Metadata.Version)
		if err != nil {
			return nil, err
		}

		toDelete.Version = &v
	}

	if err := service.DeletePipelineTemplate(ctx, toDelete); err != nil {
		return nil, err
	}

	payload := PipelineTemplateMutationPayload{ClientMutationID: input.ClientMutationID, PipelineTemplate: template, Problems: []Problem{}}
	return &PipelineTemplateMutationPayloadResolver{PipelineTemplateMutationPayload: payload}, nil
}

/* PipelineTemplate loader */

const pipelineTemplateLoaderKey = "pipelineTemplate"

// RegisterPipelineTemplateLoader registers a pipeline template loader function
func RegisterPipelineTemplateLoader(collection *loader.Collection) {
	collection.Register(pipelineTemplateLoaderKey, pipelineTemplateBatchFunc)
}

func loadPipelineTemplate(ctx context.Context, id string) (*models.PipelineTemplate, error) {
	ldr, err := loader.Extract(ctx, pipelineTemplateLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	pipelineTemplate, ok := data.(models.PipelineTemplate)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return &pipelineTemplate, nil
}

func pipelineTemplateBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	pipelineTemplates, err := getPipelineTemplateService(ctx).GetPipelineTemplatesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range pipelineTemplates {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
