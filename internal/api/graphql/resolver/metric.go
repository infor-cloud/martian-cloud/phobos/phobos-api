package resolver

import (
	"context"
	"time"

	"github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/metric"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// maxAggregatedMetricStatsQueries is the maximum number of aggregated metric stats queries allowed
const maxAggregatedMetricStatsQueries = 30

// MetricConnectionQueryArgs are used to query a metric connection
type MetricConnectionQueryArgs struct {
	ConnectionQueryArgs
	TimeRangeStart  graphql.Time
	TimeRangeEnd    *graphql.Time
	OrganizationID  *string
	ProjectID       *string
	ReleaseID       *string
	PipelineID      *string
	EnvironmentName *string
	Tags            *[]*MetricTag
	MetricName      models.MetricName
}

// MetricStatsQueryArgs are the arguments for metrics stats query.
type MetricStatsQueryArgs struct {
	TimeRangeStart  graphql.Time
	TimeRangeEnd    *graphql.Time
	OrganizationID  *string
	ProjectID       *string
	ReleaseID       *string
	PipelineID      *string
	EnvironmentName *string
	Tags            *[]*MetricTag
	MetricName      models.MetricName
}

// AggregatedMetricStatsQuery is used to query aggregated metrics statistics
type AggregatedMetricStatsQuery struct {
	OrganizationID  *string
	ProjectID       *string
	ReleaseID       *string
	PipelineID      *string
	EnvironmentName *string
	Tags            *[]*MetricTag
	MetricName      models.MetricName
}

// AggregatedMetricStatsQueryArgs are the arguments for aggregated metrics stats query.
type AggregatedMetricStatsQueryArgs struct {
	BucketPeriod db.MetricBucketPeriod
	Queries      []*AggregatedMetricStatsQuery
	BucketCount  int32
}

// MetricEdgeResolver resolves metric edges
type MetricEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *MetricEdgeResolver) Cursor() (string, error) {
	metric, ok := r.edge.Node.(*models.Metric)
	if !ok {
		return "", errors.New("failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(metric)
	return *cursor, err
}

// Node returns a metric node
func (r *MetricEdgeResolver) Node() (*MetricResolver, error) {
	metric, ok := r.edge.Node.(*models.Metric)
	if !ok {
		return nil, errors.New("failed to convert node type")
	}

	return &MetricResolver{metric: metric}, nil
}

// MetricConnectionResolver resolves a metric connection
type MetricConnectionResolver struct {
	connection Connection
}

// NewMetricConnectionResolver creates a new MetricConnectionResolver
func NewMetricConnectionResolver(ctx context.Context, input *metric.GetMetricsInput) (*MetricConnectionResolver, error) {
	metricService := getMetricService(ctx)

	result, err := metricService.GetMetrics(ctx, input)
	if err != nil {
		return nil, err
	}

	metrics := result.Metrics

	// Create edges
	edges := make([]Edge, len(metrics))
	for i, metric := range metrics {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: metric}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(metrics) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(metrics[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(metrics[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &MetricConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *MetricConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *MetricConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *MetricConnectionResolver) Edges() *[]*MetricEdgeResolver {
	resolvers := make([]*MetricEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &MetricEdgeResolver{edge: edge}
	}
	return &resolvers
}

// MetricsStatisticsResolver resolves metrics statistics based on the provided filters
type MetricsStatisticsResolver struct {
	timeRangeStart time.Time
	timeRangeEnd   *time.Time
	releaseID      *string
	projectID      *string
	orgID          *string
}

// DeploymentRecoveryTime resolver
func (r *MetricsStatisticsResolver) DeploymentRecoveryTime(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricDeploymentRecoveryTime,
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// ApprovalWaitTime resolver
func (r *MetricsStatisticsResolver) ApprovalWaitTime(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricApprovalRequestWaitTime,
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// ReleaseLeadTime resolver
func (r *MetricsStatisticsResolver) ReleaseLeadTime(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricPipelineLeadTime,
		Tags: map[models.MetricTagName]string{
			models.MetricTagNamePipelineType: string(models.ReleaseLifecyclePipelineType),
		},
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// DeploymentSuccessCount resolver
func (r *MetricsStatisticsResolver) DeploymentSuccessCount(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricPipelineCompleted,
		Tags: map[models.MetricTagName]string{
			models.MetricTagNamePipelineType:   string(models.DeploymentPipelineType),
			models.MetricTagNamePipelineStatus: string(statemachine.SucceededNodeStatus),
		},
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// DeploymentCanceledCount resolver
func (r *MetricsStatisticsResolver) DeploymentCanceledCount(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricPipelineCompleted,
		Tags: map[models.MetricTagName]string{
			models.MetricTagNamePipelineType:   string(models.DeploymentPipelineType),
			models.MetricTagNamePipelineStatus: string(statemachine.CanceledNodeStatus),
		},
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// DeploymentFailureCount resolver
func (r *MetricsStatisticsResolver) DeploymentFailureCount(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricPipelineCompleted,
		Tags: map[models.MetricTagName]string{
			models.MetricTagNamePipelineType:   string(models.DeploymentPipelineType),
			models.MetricTagNamePipelineStatus: string(statemachine.FailedNodeStatus),
		},
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// DeploymentDuration resolver
func (r *MetricsStatisticsResolver) DeploymentDuration(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricPipelineDuration,
		Tags: map[models.MetricTagName]string{
			models.MetricTagNamePipelineType: string(models.DeploymentPipelineType),
		},
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// DeploymentLeadTime resolver
func (r *MetricsStatisticsResolver) DeploymentLeadTime(ctx context.Context) (*MetricStatisticsResolver, error) {
	service := getMetricService(ctx)
	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart: r.timeRangeStart,
		TimeRangeEnd:   r.timeRangeEnd,
		ReleaseID:      r.releaseID,
		ProjectID:      r.projectID,
		OrganizationID: r.orgID,
		MetricName:     models.MetricPipelineLeadTime,
		Tags: map[models.MetricTagName]string{
			models.MetricTagNamePipelineType: string(models.DeploymentPipelineType),
		},
	}

	stats, err := service.GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: stats}, nil
}

// MetricTag resolves a metric tag
type MetricTag struct {
	Name  models.MetricTagName
	Value string
}

// MetricResolver resolves a metric.
type MetricResolver struct {
	metric *models.Metric
}

// ID resolver
func (r *MetricResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.MetricType, r.metric.Metadata.ID))
}

// Metadata resolver
func (r *MetricResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.metric.Metadata}
}

// Name resolver
func (r *MetricResolver) Name() models.MetricName {
	return r.metric.Name
}

// EnvironmentName resolver
func (r *MetricResolver) EnvironmentName() *string {
	return r.metric.EnvironmentName
}

// OrganizationID resolver
func (r *MetricResolver) OrganizationID() *string {
	if r.metric.OrganizationID != nil {
		id := gid.ToGlobalID(gid.OrganizationType, *r.metric.OrganizationID)
		return &id
	}
	return nil
}

// ProjectID resolver
func (r *MetricResolver) ProjectID() *string {
	if r.metric.ProjectID != nil {
		id := gid.ToGlobalID(gid.ProjectType, *r.metric.ProjectID)
		return &id
	}
	return nil
}

// ReleaseID resolver
func (r *MetricResolver) ReleaseID() *string {
	if r.metric.ReleaseID != nil {
		id := gid.ToGlobalID(gid.ReleaseType, *r.metric.ReleaseID)
		return &id
	}
	return nil
}

// PipelineID resolver
func (r *MetricResolver) PipelineID() *string {
	if r.metric.PipelineID != nil {
		id := gid.ToGlobalID(gid.PipelineType, *r.metric.PipelineID)
		return &id
	}
	return nil
}

// Value resolver
func (r *MetricResolver) Value() float64 {
	return r.metric.Value
}

// Tags resolver
func (r *MetricResolver) Tags() ([]*MetricTag, error) {
	tags := make([]*MetricTag, len(r.metric.Tags))
	i := 0
	for name, value := range r.metric.Tags {
		tags[i] = &MetricTag{Name: name, Value: value}
		i++
	}
	return tags, nil
}

func metricsQuery(ctx context.Context, args *MetricConnectionQueryArgs) (*MetricConnectionResolver, error) {
	urnResolver := getURNResolver(ctx)

	input := &metric.GetMetricsInput{
		PaginationOptions: &pagination.Options{First: args.First, Last: args.Last, After: args.After, Before: args.Before},
		TimeRangeStart:    args.TimeRangeStart.Time,
		EnvironmentName:   args.EnvironmentName,
		MetricName:        args.MetricName,
		Tags:              map[models.MetricTagName]string{},
	}

	if args.Tags != nil {
		for _, tag := range *args.Tags {
			input.Tags[tag.Name] = tag.Value
		}
	}

	if args.TimeRangeEnd != nil {
		input.TimeRangeEnd = &args.TimeRangeEnd.Time
	}

	if args.OrganizationID != nil {
		organizationID, err := urnResolver.ResolveResourceID(ctx, *args.OrganizationID)
		if err != nil {
			return nil, err
		}

		input.OrganizationID = &organizationID
	}

	if args.ProjectID != nil {
		projectID, err := urnResolver.ResolveResourceID(ctx, *args.ProjectID)
		if err != nil {
			return nil, err
		}

		input.ProjectID = &projectID
	}

	if args.ReleaseID != nil {
		releaseID, err := urnResolver.ResolveResourceID(ctx, *args.ReleaseID)
		if err != nil {
			return nil, err
		}

		input.ReleaseID = &releaseID
	}

	if args.PipelineID != nil {
		pipelineID, err := urnResolver.ResolveResourceID(ctx, *args.PipelineID)
		if err != nil {
			return nil, err
		}

		input.PipelineID = &pipelineID
	}

	if err := args.Validate(); err != nil {
		return nil, err
	}

	if args.Sort != nil {
		sort := db.MetricSortableField(*args.Sort)
		input.Sort = &sort
	}

	return NewMetricConnectionResolver(ctx, input)
}

// MetricStatisticsResolver resolves metrics statistics.
type MetricStatisticsResolver struct {
	statistics *db.MetricStatistics
}

// Sum resolver
func (r *MetricStatisticsResolver) Sum() float64 {
	return r.statistics.Sum
}

// Minimum resolver
func (r *MetricStatisticsResolver) Minimum() float64 {
	return r.statistics.Minimum
}

// Maximum resolver
func (r *MetricStatisticsResolver) Maximum() float64 {
	return r.statistics.Maximum
}

// Average resolver
func (r *MetricStatisticsResolver) Average() float64 {
	return r.statistics.Average
}

// AggregatedMetricStatisticsResolver resolves aggregated metrics statistics.
type AggregatedMetricStatisticsResolver struct {
	*MetricStatisticsResolver
	Bucket graphql.Time
}

// AggregatedMetricStatisticsResultResolver resolves aggregated metrics statistics result.
type AggregatedMetricStatisticsResultResolver struct {
	OrganizationID  *string
	ProjectID       *string
	ReleaseID       *string
	PipelineID      *string
	EnvironmentName *string
	Tags            *[]*MetricTag
	MetricName      models.MetricName
	Data            []*AggregatedMetricStatisticsResolver
}

func metricStatisticsQuery(ctx context.Context, args *MetricStatsQueryArgs) (*MetricStatisticsResolver, error) {
	urnResolver := getURNResolver(ctx)

	input := &metric.GetMetricStatisticsInput{
		TimeRangeStart:  args.TimeRangeStart.Time,
		EnvironmentName: args.EnvironmentName,
		MetricName:      args.MetricName,
		Tags:            map[models.MetricTagName]string{},
	}

	if args.Tags != nil {
		for _, tag := range *args.Tags {
			input.Tags[tag.Name] = tag.Value
		}
	}

	if args.TimeRangeEnd != nil {
		input.TimeRangeEnd = &args.TimeRangeEnd.Time
	}

	if args.OrganizationID != nil {
		organizationID, err := urnResolver.ResolveResourceID(ctx, *args.OrganizationID)
		if err != nil {
			return nil, err
		}

		input.OrganizationID = &organizationID
	}

	if args.ProjectID != nil {
		projectID, err := urnResolver.ResolveResourceID(ctx, *args.ProjectID)
		if err != nil {
			return nil, err
		}

		input.ProjectID = &projectID
	}

	if args.ReleaseID != nil {
		releaseID, err := urnResolver.ResolveResourceID(ctx, *args.ReleaseID)
		if err != nil {
			return nil, err
		}

		input.ReleaseID = &releaseID
	}

	if args.PipelineID != nil {
		pipelineID, err := urnResolver.ResolveResourceID(ctx, *args.PipelineID)
		if err != nil {
			return nil, err
		}

		input.PipelineID = &pipelineID
	}

	statistics, err := getMetricService(ctx).GetMetricStatistics(ctx, input)
	if err != nil {
		return nil, err
	}

	return &MetricStatisticsResolver{statistics: statistics}, nil
}

func aggregatedMetricStatisticsQuery(ctx context.Context, args *AggregatedMetricStatsQueryArgs) ([]*AggregatedMetricStatisticsResultResolver, error) {
	urnResolver := getURNResolver(ctx)

	if len(args.Queries) > maxAggregatedMetricStatsQueries {
		return nil, errors.New("number of metric queries cannot exceed %d", maxAggregatedMetricStatsQueries, errors.WithErrorCode(errors.EInvalid))
	}

	result := make([]*AggregatedMetricStatisticsResultResolver, len(args.Queries))

	for i, query := range args.Queries {
		input := &metric.GetAggregatedMetricStatisticsInput{
			BucketPeriod:    args.BucketPeriod,
			BucketCount:     int(args.BucketCount),
			EnvironmentName: query.EnvironmentName,
			MetricName:      query.MetricName,
			Tags:            map[models.MetricTagName]string{},
		}

		if query.Tags != nil {
			for _, tag := range *query.Tags {
				input.Tags[tag.Name] = tag.Value
			}
		}

		if query.OrganizationID != nil {
			organizationID, err := urnResolver.ResolveResourceID(ctx, *query.OrganizationID)
			if err != nil {
				return nil, err
			}

			input.OrganizationID = &organizationID
		}

		if query.ProjectID != nil {
			projectID, err := urnResolver.ResolveResourceID(ctx, *query.ProjectID)
			if err != nil {
				return nil, err
			}

			input.ProjectID = &projectID
		}

		if query.ReleaseID != nil {
			releaseID, err := urnResolver.ResolveResourceID(ctx, *query.ReleaseID)
			if err != nil {
				return nil, err
			}

			input.ReleaseID = &releaseID
		}

		if query.PipelineID != nil {
			pipelineID, err := urnResolver.ResolveResourceID(ctx, *query.PipelineID)
			if err != nil {
				return nil, err
			}

			input.PipelineID = &pipelineID
		}

		results, err := getMetricService(ctx).GetAggregatedMetricStatistics(ctx, input)
		if err != nil {
			return nil, err
		}

		resolvers := make([]*AggregatedMetricStatisticsResolver, len(results))
		for i, result := range results {
			result := result
			resolvers[i] = &AggregatedMetricStatisticsResolver{
				MetricStatisticsResolver: &MetricStatisticsResolver{statistics: &result.Stats},
				Bucket:                   graphql.Time{Time: result.Bucket},
			}
		}

		result[i] = &AggregatedMetricStatisticsResultResolver{
			OrganizationID:  query.OrganizationID,
			ProjectID:       query.ProjectID,
			ReleaseID:       query.ReleaseID,
			PipelineID:      query.PipelineID,
			EnvironmentName: query.EnvironmentName,
			Tags:            query.Tags,
			MetricName:      query.MetricName,
			Data:            resolvers,
		}
	}

	return result, nil
}
