package resolver

import (
	"context"
	"io"
	"strings"

	"github.com/graph-gophers/dataloader"
	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/loader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// LifecycleTemplateConnectionQueryArgs are used to query a lifecycle template connection
type LifecycleTemplateConnectionQueryArgs struct {
	ConnectionQueryArgs
	Scopes *[]models.ScopeType
}

// LifecycleTemplateEdgeResolver resolves lifecycle template edges
type LifecycleTemplateEdgeResolver struct {
	edge Edge
}

// Cursor returns an opaque cursor
func (r *LifecycleTemplateEdgeResolver) Cursor() (string, error) {
	lifecycleTemplate, ok := r.edge.Node.(*models.LifecycleTemplate)
	if !ok {
		return "", errors.New("Failed to convert node type")
	}
	cursor, err := r.edge.CursorFunc(lifecycleTemplate)
	return *cursor, err
}

// Node returns a lifecycle template node
func (r *LifecycleTemplateEdgeResolver) Node() (*LifecycleTemplateResolver, error) {
	lifecycleTemplate, ok := r.edge.Node.(*models.LifecycleTemplate)
	if !ok {
		return nil, errors.New("Failed to convert node type")
	}

	return &LifecycleTemplateResolver{lifecycleTemplate: lifecycleTemplate}, nil
}

// LifecycleTemplateConnectionResolver resolves a lifecycle template connection
type LifecycleTemplateConnectionResolver struct {
	connection Connection
}

// NewLifecycleTemplateConnectionResolver creates a new LifecycleTemplateConnectionResolver
func NewLifecycleTemplateConnectionResolver(ctx context.Context,
	input *lifecycletemplate.GetLifecycleTemplatesInput) (*LifecycleTemplateConnectionResolver, error) {
	lifecycleTemplateService := getLifecycleTemplateService(ctx)

	result, err := lifecycleTemplateService.GetLifecycleTemplates(ctx, input)
	if err != nil {
		return nil, err
	}

	lifecycleTemplates := result.LifecycleTemplates

	// Create edges
	edges := make([]Edge, len(lifecycleTemplates))
	for i, lifecycleTemplate := range lifecycleTemplates {
		edges[i] = Edge{CursorFunc: result.PageInfo.Cursor, Node: lifecycleTemplate}
	}

	pageInfo := PageInfo{
		HasNextPage:     result.PageInfo.HasNextPage,
		HasPreviousPage: result.PageInfo.HasPreviousPage,
	}

	if len(lifecycleTemplates) > 0 {
		var err error
		pageInfo.StartCursor, err = result.PageInfo.Cursor(lifecycleTemplates[0])
		if err != nil {
			return nil, err
		}

		pageInfo.EndCursor, err = result.PageInfo.Cursor(lifecycleTemplates[len(edges)-1])
		if err != nil {
			return nil, err
		}
	}

	connection := Connection{
		TotalCount: result.PageInfo.TotalCount,
		PageInfo:   pageInfo,
		Edges:      edges,
	}

	return &LifecycleTemplateConnectionResolver{connection: connection}, nil
}

// TotalCount returns the total result count for the connection
func (r *LifecycleTemplateConnectionResolver) TotalCount() int32 {
	return r.connection.TotalCount
}

// PageInfo returns the connection page information
func (r *LifecycleTemplateConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{pageInfo: r.connection.PageInfo}
}

// Edges returns the connection edges
func (r *LifecycleTemplateConnectionResolver) Edges() *[]*LifecycleTemplateEdgeResolver {
	resolvers := make([]*LifecycleTemplateEdgeResolver, len(r.connection.Edges))
	for i, edge := range r.connection.Edges {
		resolvers[i] = &LifecycleTemplateEdgeResolver{edge: edge}
	}
	return &resolvers
}

// LifecycleTemplateResolver resolves a lifecycle template resource
type LifecycleTemplateResolver struct {
	lifecycleTemplate *models.LifecycleTemplate
}

// ID resolver
func (r *LifecycleTemplateResolver) ID() graphql.ID {
	return graphql.ID(gid.ToGlobalID(gid.LifecycleTemplateType, r.lifecycleTemplate.Metadata.ID))
}

// Metadata resolver
func (r *LifecycleTemplateResolver) Metadata() *MetadataResolver {
	return &MetadataResolver{metadata: &r.lifecycleTemplate.Metadata}
}

// CreatedBy resolver
func (r *LifecycleTemplateResolver) CreatedBy() string {
	return r.lifecycleTemplate.CreatedBy
}

// Scope resolver
func (r *LifecycleTemplateResolver) Scope() models.ScopeType {
	return r.lifecycleTemplate.Scope
}

// Organization resolver
func (r *LifecycleTemplateResolver) Organization(ctx context.Context) (*OrganizationResolver, error) {
	organization, err := loadOrganization(ctx, r.lifecycleTemplate.OrganizationID)
	if err != nil {
		return nil, err
	}

	return &OrganizationResolver{organization: organization}, nil
}

// Project resolver
func (r *LifecycleTemplateResolver) Project(ctx context.Context) (*ProjectResolver, error) {
	if r.lifecycleTemplate.ProjectID == nil {
		return nil, nil
	}

	project, err := loadProject(ctx, *r.lifecycleTemplate.ProjectID)
	if err != nil {
		return nil, err
	}
	return &ProjectResolver{project: project}, nil
}

// Status resolver
func (r *LifecycleTemplateResolver) Status() models.LifecycleTemplateStatus {
	return r.lifecycleTemplate.Status
}

// HCLData resolver
func (r *LifecycleTemplateResolver) HCLData(ctx context.Context) (string, error) {

	// If the lifecycle template's status indicates that no data has been uploaded, just return empty string.
	// Otherwise, the attempt to fetch non-existent data will return an error.
	if r.lifecycleTemplate.Status != models.LifecycleTemplateUploaded {
		return "", nil
	}

	hclDataRdr, err := getLifecycleTemplateService(ctx).GetLifecycleTemplateData(ctx, r.lifecycleTemplate.Metadata.ID)
	if err != nil {
		return "", err
	}

	hclData, err := io.ReadAll(hclDataRdr)
	if err != nil {
		return "", err
	}

	return string(hclData), nil
}

/* LifecycleTemplate Mutations */

// LifecycleTemplateMutationPayload is the response payload for a lifecycle template mutation
type LifecycleTemplateMutationPayload struct {
	ClientMutationID  *string
	LifecycleTemplate *models.LifecycleTemplate
	Problems          []Problem
}

// LifecycleTemplateMutationPayloadResolver resolves a LifecycleTemplateMutationPayload
type LifecycleTemplateMutationPayloadResolver struct {
	LifecycleTemplateMutationPayload
}

// LifecycleTemplate field resolver
func (r *LifecycleTemplateMutationPayloadResolver) LifecycleTemplate() *LifecycleTemplateResolver {
	if r.LifecycleTemplateMutationPayload.LifecycleTemplate == nil {
		return nil
	}

	return &LifecycleTemplateResolver{lifecycleTemplate: r.LifecycleTemplateMutationPayload.LifecycleTemplate}
}

// CreateLifecycleTemplateInput contains the input for creating a new lifecycle template
type CreateLifecycleTemplateInput struct {
	ClientMutationID *string
	Scope            models.ScopeType
	OrganizationID   *string // depending on scope, either OrgID or ProjectID is required
	ProjectID        *string
	HCLData          string
}

func handleLifecycleTemplateMutationProblem(e error, clientMutationID *string) (*LifecycleTemplateMutationPayloadResolver, error) {
	problem, err := buildProblem(e)
	if err != nil {
		return nil, err
	}
	payload := LifecycleTemplateMutationPayload{ClientMutationID: clientMutationID, Problems: []Problem{*problem}}
	return &LifecycleTemplateMutationPayloadResolver{LifecycleTemplateMutationPayload: payload}, nil
}

func createLifecycleTemplateMutation(ctx context.Context,
	input *CreateLifecycleTemplateInput) (*LifecycleTemplateMutationPayloadResolver, error) {
	service := getLifecycleTemplateService(ctx)
	urnResolver := getURNResolver(ctx)

	var orgID, projectID *string
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("organization ID is required for an organization lifecycle template",
				errors.WithErrorCode(errors.EInvalid))
		}

		oID, oErr := urnResolver.ResolveResourceID(ctx, *input.OrganizationID)
		if oErr != nil {
			return nil, oErr
		}

		orgID = &oID
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("project id is required for a project lifecycle template",
				errors.WithErrorCode(errors.EInvalid))
		}

		pID, pErr := urnResolver.ResolveResourceID(ctx, *input.ProjectID)
		if pErr != nil {
			return nil, pErr
		}

		projectID = &pID
	default:
		return nil, errors.New("lifecycle template must have either organization or project scope",
			errors.WithErrorCode(errors.EInvalid))
	}

	lifecycleTemplate, err := service.CreateLifecycleTemplate(ctx, &lifecycletemplate.CreateLifecycleTemplateInput{
		Scope:          input.Scope,
		OrganizationID: orgID,
		ProjectID:      projectID,
	})
	if err != nil {
		return nil, err
	}

	if err = service.UploadLifecycleTemplate(ctx, &lifecycletemplate.UploadLifecycleTemplateInput{
		ID:     lifecycleTemplate.Metadata.ID,
		Reader: strings.NewReader(input.HCLData),
	}); err != nil {
		return nil, err
	}

	lifecycleTemplate, err = service.GetLifecycleTemplateByID(ctx, lifecycleTemplate.Metadata.ID)
	if err != nil {
		return nil, io.ErrClosedPipe
	}

	payload := LifecycleTemplateMutationPayload{ClientMutationID: input.ClientMutationID, LifecycleTemplate: lifecycleTemplate, Problems: []Problem{}}
	return &LifecycleTemplateMutationPayloadResolver{LifecycleTemplateMutationPayload: payload}, nil
}

/* LifecycleTemplate loader */

const lifecycleTemplateLoaderKey = "lifecycleTemplate"

// RegisterLifecycleTemplateLoader registers a lifecycle template loader function
func RegisterLifecycleTemplateLoader(collection *loader.Collection) {
	collection.Register(lifecycleTemplateLoaderKey, lifecycleTemplateBatchFunc)
}

func loadLifecycleTemplate(ctx context.Context, id string) (*models.LifecycleTemplate, error) {
	ldr, err := loader.Extract(ctx, lifecycleTemplateLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(id))()
	if err != nil {
		return nil, err
	}

	lifecycleTemplate, ok := data.(*models.LifecycleTemplate)
	if !ok {
		return nil, errors.New("Wrong type")
	}

	return lifecycleTemplate, nil
}

func lifecycleTemplateBatchFunc(ctx context.Context, ids []string) (loader.DataBatch, error) {
	lifecycleTemplates, err := getLifecycleTemplateService(ctx).GetLifecycleTemplatesByIDs(ctx, ids)
	if err != nil {
		return nil, err
	}

	// Build map of results
	batch := loader.DataBatch{}
	for _, result := range lifecycleTemplates {
		batch[result.Metadata.ID] = result
	}

	return batch, nil
}
