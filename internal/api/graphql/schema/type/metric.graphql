enum MetricName {
  APPROVAL_REQUEST_CREATED
  APPROVAL_REQUEST_COMPLETED
  APPROVAL_REQUEST_WAIT_TIME
  PIPELINE_DURATION
  PIPELINE_COMPLETED
  DEPLOYMENT_RECOVERY_TIME
  DEPLOYMENT_UPDATED
  PIPELINE_LEAD_TIME
}

enum MetricTagName {
  PIPELINE_TYPE
  PIPELINE_STATUS
  RELEASE_LIFECYCLE
  APPROVAL_TARGET_TYPE
}

enum MetricSort {
  CREATED_AT_ASC
  CREATED_AT_DESC
}

enum MetricBucketPeriod {
  hour
  day
  month
}

type MetricConnection {
  totalCount: Int!
  pageInfo: PageInfo!
  edges: [MetricEdge]
}

type MetricEdge {
  cursor: String!
  node: Metric
}

type MetricTag {
  name: MetricTagName!
  value: String!
}

type Metric {
  id: ID!
  metadata: ResourceMetadata!
  name: MetricName!
  environmentName: String
  pipelineId: String
  releaseId: String
  projectId: String
  organizationId: String
  value: Float!
  tags: [MetricTag!]!
}

type MetricStatistics {
  sum: Float!
  minimum: Float!
  maximum: Float!
  average: Float!
}

type AggregatedMetricStatistics {
  bucket: Time!
  sum: Float!
  minimum: Float!
  maximum: Float!
  average: Float!
}

type AggregatedMetricStatisticsResult {
  metricName: MetricName!
  environmentName: String
  pipelineId: String
  releaseId: String
  projectId: String
  organizationId: String
  tags: [MetricTag!]
  data: [AggregatedMetricStatistics!]!
}

input AggregatedMetricStatisticsQuery {
  metricName: MetricName!
  environmentName: String
  pipelineId: String
  releaseId: String
  projectId: String
  organizationId: String
  tags: [MetricTagInput!]
}

input MetricTagInput {
  name: MetricTagName!
  value: String!
}
