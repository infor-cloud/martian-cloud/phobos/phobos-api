enum ServiceAccountSort {
  CREATED_AT_ASC
  CREATED_AT_DESC
  UPDATED_AT_ASC
  UPDATED_AT_DESC
}

enum BoundClaimsType {
  STRING
  GLOB
}

type ServiceAccountConnection {
  totalCount: Int!
  pageInfo: PageInfo!
  edges: [ServiceAccountEdge]
}

type ServiceAccountEdge {
  cursor: String!
  node: ServiceAccount
}

type CreateServiceAccountPayload {
  clientMutationId: String
  serviceAccount: ServiceAccount
  problems: [Problem!]!
}

type UpdateServiceAccountPayload {
  clientMutationId: String
  serviceAccount: ServiceAccount
  problems: [Problem!]!
}

type DeleteServiceAccountPayload {
  clientMutationId: String
  serviceAccount: ServiceAccount
  problems: [Problem!]!
}

type JWTClaim {
  name: String!
  value: String!
}

type OIDCTrustPolicy {
  issuer: String!
  boundClaimsType: BoundClaimsType!
  boundClaims: [JWTClaim!]!
}

type ServiceAccount implements Node {
  id: ID!
  metadata: ResourceMetadata!
  name: String!
  description: String!
  scope: ScopeType!
  organization: Organization
  project: Project
  createdBy: String!
  oidcTrustPolicies: [OIDCTrustPolicy!]!
  memberships(
    after: String
    before: String
    first: Int
    last: Int
  ): MembershipConnection!
  activityEvents(
    after: String
    before: String
    first: Int
    last: Int
    organizationId: String
    projectId: String
    timeRangeStart: Time
    timeRangeEnd: Time
    actions: [ActivityEventAction!]
    targetTypes: [ActivityEventTargetType!]
    sort: ActivityEventSort
  ): ActivityEventConnection!
}

input JWTClaimInput {
  name: String!
  value: String!
}

input OIDCTrustPolicyInput {
  issuer: String!
  boundClaimsType: BoundClaimsType
  boundClaims: [JWTClaimInput!]!
}

input CreateServiceAccountInput {
  clientMutationId: String
  name: String!
  description: String!
  scope: ScopeType!
  organizationId: String
  projectId: String
  oidcTrustPolicies: [OIDCTrustPolicyInput!]!
}

input UpdateServiceAccountInput {
  clientMutationId: String
  id: ID!
  metadata: ResourceMetadataInput
  description: String
  oidcTrustPolicies: [OIDCTrustPolicyInput!]!
}

input DeleteServiceAccountInput {
  clientMutationId: String
  id: ID!
  metadata: ResourceMetadataInput
}
