// Package urn provides the PRN (Phobos Resource Name) functionalities.
// Primarily responsible for resolving the UUID from either a GlobalID
// or a PRN.
package urn

import (
	"context"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/approvalrule"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/comment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/organization"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/project"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/projectvariable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/release"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/role"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/team"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/todoitem"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/user"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// ResolverOptions are the options for the resolver.
type ResolverOptions struct {
	OrganizationService      organization.Service
	AgentService             agent.Service
	ProjectService           project.Service
	ProjectVariableService   projectvariable.Service
	EnvironmentService       environment.Service
	ServiceAccountService    serviceaccount.Service
	UserService              user.Service
	TeamService              team.Service
	RoleService              role.Service
	JobService               job.Service
	MembershipService        membership.Service
	PipelineService          pipeline.Service
	PipelineTemplateService  pipelinetemplate.Service
	ApprovalRuleService      approvalrule.Service
	LifecycleTemplateService lifecycletemplate.Service
	ReleaseLifecycleService  releaselifecycle.Service
	ReleaseService           release.Service
	CommentService           comment.Service
	PluginRegistryService    pluginregistry.Service
	ToDoItemService          todoitem.Service
	VCSProviderService       vcs.Service
}

// Resolver resolves a resource UUID from either a GID or PRN.
type Resolver struct {
	options *ResolverOptions
}

// NewResolver creates a new resolver.
func NewResolver(options *ResolverOptions) *Resolver {
	return &Resolver{options: options}
}

// ResolveResourceID resolves a resource UUID from either a GID or PRN.
func (r *Resolver) ResolveResourceID(ctx context.Context, value string) (string, error) {
	if IsPRN(value) {
		return r.resolvePRN(ctx, value)
	}

	g, err := gid.ParseGlobalID(value)
	if err != nil {
		return "", err
	}

	return g.ID, nil
}

// resolvePRN resolves a resource UUID from a PRN.
func (r *Resolver) resolvePRN(ctx context.Context, prn string) (string, error) {
	resourceType := GetResourceType(prn)
	switch resourceType {
	case models.AgentResource:
		return r.resolveAgentID(ctx, prn)
	case models.AgentSessionResource:
		return r.resolveAgentSessionID(ctx, prn)
	case models.OrganizationResource:
		return r.resolveOrganizationID(ctx, prn)
	case models.ProjectResource:
		return r.resolveProjectID(ctx, prn)
	case models.ProjectVariableResource:
		return r.resolveProjectVariableID(ctx, prn)
	case models.ProjectVariableSetResource:
		return r.resolveProjectVariableSetID(ctx, prn)
	case models.EnvironmentResource:
		return r.resolveEnvironmentID(ctx, prn)
	case models.ServiceAccountResource:
		return r.resolveServiceAccountID(ctx, prn)
	case models.UserResource:
		return r.resolveUserID(ctx, prn)
	case models.TeamResource:
		return r.resolveTeamID(ctx, prn)
	case models.JobResource:
		return r.resolveJobID(ctx, prn)
	case models.PipelineTemplateResource:
		return r.resolvePipelineTemplateID(ctx, prn)
	case models.PipelineResource:
		return r.resolvePipelineID(ctx, prn)
	case models.MembershipResource:
		return r.resolveMembershipID(ctx, prn)
	case models.RoleResource:
		return r.resolveRoleID(ctx, prn)
	case models.ApprovalRuleResource:
		return r.resolveApprovalRuleID(ctx, prn)
	case models.LifecycleTemplateResource:
		return r.resolveLifecycleTemplateID(ctx, prn)
	case models.ReleaseLifecycleResource:
		return r.resolveReleaseLifecycleID(ctx, prn)
	case models.ReleaseResource:
		return r.resolveReleaseID(ctx, prn)
	case models.CommentResource:
		return r.resolveCommentID(ctx, prn)
	case models.PluginResource:
		return r.resolvePluginID(ctx, prn)
	case models.PluginVersionResource:
		return r.resolvePluginVersionID(ctx, prn)
	case models.PluginPlatformResource:
		return r.resolvePluginPlatformID(ctx, prn)
	case models.ToDoItemResource:
		return r.resolveToDoItemID(ctx, prn)
	case models.VCSProviderResource:
		return r.resolveVCSProviderID(ctx, prn)
	case models.ThreadResource:
		return r.resolveThreadID(ctx, prn)
	case models.EnvironmentRuleResource:
		return r.resolveEnvironmentRuleID(ctx, prn)
	}

	return "", errors.New("invalid prn resource type: %s", string(resourceType), errors.WithErrorCode(errors.EInvalid))
}

// resolveApprovalRuleID resolves an approval rule UUID from a PRN.
func (r *Resolver) resolveApprovalRuleID(ctx context.Context, prn string) (string, error) {
	approvalRule, err := r.options.ApprovalRuleService.GetApprovalRuleByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return approvalRule.Metadata.ID, nil
}

// resolveAgentID resolves an agent UUID from a PRN.
func (r *Resolver) resolveAgentID(ctx context.Context, prn string) (string, error) {
	agent, err := r.options.AgentService.GetAgentByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return agent.Metadata.ID, nil
}

// resolveAgentSessionID resolves an agent session UUID from a PRN.
func (r *Resolver) resolveAgentSessionID(ctx context.Context, prn string) (string, error) {
	session, err := r.options.AgentService.GetAgentSessionByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return session.Metadata.ID, nil
}

// resolveOrganizationID resolves an organization UUID from a PRN.
func (r *Resolver) resolveOrganizationID(ctx context.Context, prn string) (string, error) {
	organization, err := r.options.OrganizationService.GetOrganizationByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return organization.Metadata.ID, nil
}

// resolveProjectID resolves a project UUID from a PRN.
func (r *Resolver) resolveProjectID(ctx context.Context, prn string) (string, error) {
	project, err := r.options.ProjectService.GetProjectByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return project.Metadata.ID, nil
}

// resolveProjectVariableID resolves a project UUID from a PRN.
func (r *Resolver) resolveProjectVariableID(ctx context.Context, prn string) (string, error) {
	variable, err := r.options.ProjectVariableService.GetProjectVariableByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return variable.Metadata.ID, nil
}

// resolveProjectVariableSetID resolves a project UUID from a PRN.
func (r *Resolver) resolveProjectVariableSetID(ctx context.Context, prn string) (string, error) {
	variableSet, err := r.options.ProjectVariableService.GetProjectVariableSetByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return variableSet.Metadata.ID, nil
}

// resolveEnvironmentID resolves an environment UUID from a PRN.
func (r *Resolver) resolveEnvironmentID(ctx context.Context, prn string) (string, error) {
	environment, err := r.options.EnvironmentService.GetEnvironmentByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return environment.Metadata.ID, nil
}

// resolveServiceAccountID resolves a service account UUID from a PRN.
func (r *Resolver) resolveServiceAccountID(ctx context.Context, prn string) (string, error) {
	serviceAccount, err := r.options.ServiceAccountService.GetServiceAccountByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return serviceAccount.Metadata.ID, nil
}

// resolveUserID resolves a user UUID from a PRN.
func (r *Resolver) resolveUserID(ctx context.Context, prn string) (string, error) {
	user, err := r.options.UserService.GetUserByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return user.Metadata.ID, nil
}

// resolveTeamID resolves a team UUID from a PRN.
func (r *Resolver) resolveTeamID(ctx context.Context, prn string) (string, error) {
	team, err := r.options.TeamService.GetTeamByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return team.Metadata.ID, nil
}

// resolveJobID resolves a job UUID from a PRN.
func (r *Resolver) resolveJobID(ctx context.Context, prn string) (string, error) {
	job, err := r.options.JobService.GetJobByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return job.Metadata.ID, nil
}

// resolvePipelineTemplateID resolves a pipeline template UUID from a PRN.
func (r *Resolver) resolvePipelineTemplateID(ctx context.Context, prn string) (string, error) {
	template, err := r.options.PipelineTemplateService.GetPipelineTemplateByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return template.Metadata.ID, nil
}

// resolvePipelineID resolves a pipeline UUID from a PRN.
func (r *Resolver) resolvePipelineID(ctx context.Context, prn string) (string, error) {
	pipeline, err := r.options.PipelineService.GetPipelineByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return pipeline.Metadata.ID, nil
}

// resolveMembershipID resolves a membership UUID from a PRN.
func (r *Resolver) resolveMembershipID(ctx context.Context, prn string) (string, error) {
	membership, err := r.options.MembershipService.GetMembershipByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return membership.Metadata.ID, nil
}

// resolveRoleID resolves a role UUID from a PRN.
func (r *Resolver) resolveRoleID(ctx context.Context, prn string) (string, error) {
	role, err := r.options.RoleService.GetRoleByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return role.Metadata.ID, nil
}

// resolveLifecycleTemplateID resolves a lifecycle template UUID from a PRN.
func (r *Resolver) resolveLifecycleTemplateID(ctx context.Context, prn string) (string, error) {
	lifecycleTemplate, err := r.options.LifecycleTemplateService.GetLifecycleTemplateByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return lifecycleTemplate.Metadata.ID, nil
}

// resolveReleaseLifecycleID resolves a release lifecycle UUID from a PRN.
func (r *Resolver) resolveReleaseLifecycleID(ctx context.Context, prn string) (string, error) {
	releaseLifecycle, err := r.options.ReleaseLifecycleService.GetReleaseLifecycleByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return releaseLifecycle.Metadata.ID, nil
}

// resolveReleaseID resolves a release UUID from a PRN.
func (r *Resolver) resolveReleaseID(ctx context.Context, prn string) (string, error) {
	release, err := r.options.ReleaseService.GetReleaseByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return release.Metadata.ID, nil
}

// resolveCommentID resolves a comment UUID from a PRN.
func (r *Resolver) resolveCommentID(ctx context.Context, prn string) (string, error) {
	comment, err := r.options.CommentService.GetCommentByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return comment.Metadata.ID, nil
}

// resolvePluginID resolves a plugin UUID from a PRN.
func (r *Resolver) resolvePluginID(ctx context.Context, prn string) (string, error) {
	plugin, err := r.options.PluginRegistryService.GetPluginByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return plugin.Metadata.ID, nil
}

// resolvePluginVersionID resolves a plugin version UUID from a PRN.
func (r *Resolver) resolvePluginVersionID(ctx context.Context, prn string) (string, error) {
	pluginVersion, err := r.options.PluginRegistryService.GetPluginVersionByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return pluginVersion.Metadata.ID, nil
}

// resolvePluginPlatformID resolves a plugin platform UUID from a PRN.
func (r *Resolver) resolvePluginPlatformID(ctx context.Context, prn string) (string, error) {
	pluginPlatform, err := r.options.PluginRegistryService.GetPluginPlatformByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return pluginPlatform.Metadata.ID, nil
}

func (r *Resolver) resolveToDoItemID(ctx context.Context, prn string) (string, error) {
	todoItem, err := r.options.ToDoItemService.GetToDoItemByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return todoItem.Metadata.ID, nil
}

func (r *Resolver) resolveVCSProviderID(ctx context.Context, prn string) (string, error) {
	vcsProvider, err := r.options.VCSProviderService.GetVCSProviderByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return vcsProvider.Metadata.ID, nil
}

func (r *Resolver) resolveThreadID(ctx context.Context, prn string) (string, error) {
	thread, err := r.options.CommentService.GetThreadByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return thread.Metadata.ID, nil
}

func (r *Resolver) resolveEnvironmentRuleID(ctx context.Context, prn string) (string, error) {
	environmentRule, err := r.options.EnvironmentService.GetEnvironmentRuleByPRN(ctx, prn)
	if err != nil {
		return "", err
	}

	return environmentRule.Metadata.ID, nil
}

// IsPRN returns true if the value is a PRN i.e. begins with "prn:".
func IsPRN(value string) bool {
	return strings.HasPrefix(value, models.PRNPrefix)
}

// GetResourceType returns the resource type from a PRN.
func GetResourceType(prn string) models.ResourceType {
	if !IsPRN(prn) {
		return models.ResourceType("")
	}

	return models.ResourceType(strings.Split(prn[len(models.PRNPrefix):], ":")[0])
}
