package builder

import (
	"encoding/json"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
)

// ApprovalRule contains approval rule fields
type ApprovalRule struct {
	Name     string
	Required int
}

// ApprovalRequestEmail is the email builder for approval requests
type ApprovalRequestEmail struct {
	ReleaseVersion    string
	EnvironmentName   string
	PipelineType      models.PipelineType
	TodoTargetType    models.ToDoItemTargetType
	PipelineID        string
	NodePath          *string
	OrgName           string
	ProjectName       string
	EligibleApprovers []string
	ApprovalRules     []ApprovalRule
}

type approvalRequestEmailTemplateData struct {
	ApprovalRequestEmail
	NodeName string
}

// Type returns the type of email builder
func (ar *ApprovalRequestEmail) Type() EmailType {
	return ApprovalRequestEmailType
}

// Build returns the email html
func (ar *ApprovalRequestEmail) Build(templateCtx *TemplateContext) (string, error) {
	templateData := &approvalRequestEmailTemplateData{
		ApprovalRequestEmail: *ar,
	}

	if ar.NodePath != nil {
		parts := strings.Split(*ar.NodePath, ".")
		templateData.NodeName = parts[len(parts)-1]
	}

	html, err := templateCtx.ExecuteTemplate(ar.Type().TemplateFilename(), templateData)
	if err != nil {
		return "", err
	}
	return templateCtx.WrapInBaseTemplate(html)
}

// InitFromData creates the builder from raw data
func (ar *ApprovalRequestEmail) InitFromData(data []byte) error {
	return json.Unmarshal(data, ar)
}
