// Package email supports sending emails.
package email

//go:generate go tool mockery --name Client --inpackage --case underscore

import (
	"context"
	"fmt"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/asynctask"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/email/builder"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/plugin/email"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// Default max recipients for a single email
const defaultMaxRecipients = 50

// SendMailInput is the input for sending an email
type SendMailInput struct {
	UsersIDs       []string
	TeamsIDs       []string
	ExcludeUserIDs []string
	Subject        string
	Builder        builder.EmailBuilder
}

// Client is used for sending emails
type Client interface {
	SendMail(ctx context.Context, input *SendMailInput)
}

type client struct {
	taskManager   asynctask.Manager
	dbClient      *db.Client
	emailProvider email.Provider
	logger        logger.Logger
	templateCtx   *builder.TemplateContext
	maxRecipients int
}

// NewClient returns a new client
func NewClient(emailProvider email.Provider, taskManager asynctask.Manager, dbClient *db.Client, logger logger.Logger, frontendURL string, emailFooter string) Client {
	return &client{
		emailProvider: emailProvider,
		taskManager:   taskManager,
		dbClient:      dbClient,
		logger:        logger,
		templateCtx:   builder.NewTemplateContext(frontendURL, emailFooter),
		maxRecipients: defaultMaxRecipients,
	}
}

func (c *client) SendMail(_ context.Context, input *SendMailInput) {
	// Send emails using an async goroutine to avoid blocking the main thread
	c.taskManager.StartTask(func(ctx context.Context) {
		if err := c.sendMail(ctx, input); err != nil {
			// Log an error if the email(s) failed to send
			c.logger.Errorf("failed to send email of type %s: %v", string(input.Builder.Type()), err)
		}
	})
}

func (c *client) sendMail(ctx context.Context, input *SendMailInput) error {
	// Buid email template
	emailBody, err := input.Builder.Build(c.templateCtx)
	if err != nil {
		return err
	}

	userIDMap := make(map[string]struct{})
	excludeUserIDMap := make(map[string]struct{})

	for _, id := range input.UsersIDs {
		userIDMap[id] = struct{}{}
	}

	for _, id := range input.ExcludeUserIDs {
		excludeUserIDMap[id] = struct{}{}
	}

	if len(input.TeamsIDs) > 0 {
		resp, err := c.dbClient.TeamMembers.GetTeamMembers(ctx, &db.GetTeamMembersInput{
			Filter: &db.TeamMemberFilter{
				TeamIDs: input.TeamsIDs,
			},
		})
		if err != nil {
			return err
		}

		for _, member := range resp.TeamMembers {
			userIDMap[member.UserID] = struct{}{}
		}
	}

	userIDs := make([]string, 0, len(userIDMap))
	for id := range userIDMap {
		if _, ok := excludeUserIDMap[id]; !ok {
			userIDs = append(userIDs, id)
		}
	}

	addresses := []string{}

	if len(userIDs) > 0 {
		resp, err := c.dbClient.Users.GetUsers(ctx, &db.GetUsersInput{
			Filter: &db.UserFilter{
				UserIDs: userIDs,
			},
		})
		if err != nil {
			return err
		}

		for _, user := range resp.Users {
			addresses = append(addresses, user.Email)
		}
	}

	// Chunk addresses into groups of maxRecipients
	chunkedAddresses := [][]string{}
	for i := 0; i < len(addresses); i += c.maxRecipients {
		end := i + c.maxRecipients

		if end > len(addresses) {
			end = len(addresses)
		}
		chunkedAddresses = append(chunkedAddresses, addresses[i:end])
	}

	// Send email to each chunk, eventually this can be optimized to send each chunk in a separate goroutine
	for _, recipients := range chunkedAddresses {
		if err = c.emailProvider.SendMail(ctx, recipients, input.Subject, emailBody); err != nil {
			return fmt.Errorf("failed to send mail: %v", err)
		}
	}

	return nil
}
