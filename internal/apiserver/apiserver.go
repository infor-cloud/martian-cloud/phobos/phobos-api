// Package apiserver is used to initialize the api
package apiserver

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	agt "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/controllers"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/graphql/resolver"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/grpc"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/middleware"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/response"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/api/urn"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/apiserver/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/asynctask"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/email"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	phoboshttp "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/http"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/approvalrule"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/comment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/metric"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/organization"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline/eventhandlers"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pluginregistry"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/project"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/projectvariable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/release"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/releaselifecycle"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/resourcelimit"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/role"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/scim"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/serviceaccount"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/team"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/todoitem"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/user"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/version"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// APIServer represents an instance of a server
type APIServer struct {
	logger        logger.Logger
	taskManager   asynctask.Manager
	dbClient      *db.Client
	srv           *http.Server
	grpcServer    *grpc.Server
	traceShutdown func(context.Context) error
	tlsConfig     *tls.Config
	shutdownOnce  sync.Once
}

// New creates a new APIServer instance
func New(ctx context.Context, cfg *config.Config, logger logger.Logger, apiVersion string) (*APIServer, error) {
	openIDConfigFetcher := auth.NewOpenIDConfigFetcher()

	tlsConfig, err := loadTLSConfig(cfg, logger)
	if err != nil {
		return nil, fmt.Errorf("failed to load TLS config: %w", err)
	}

	var oauthProviders []auth.IdentityProviderConfig
	for _, idpConfig := range cfg.OauthProviders {
		idp, cErr := openIDConfigFetcher.GetOpenIDConfig(ctx, idpConfig.IssuerURL)
		if cErr != nil {
			return nil, fmt.Errorf("failed to get OIDC config for issuer %s %v", idpConfig.IssuerURL, cErr)
		}

		oauthProviders = append(oauthProviders, auth.IdentityProviderConfig{
			Issuer:        idp.Issuer,
			TokenEndpoint: idp.TokenEndpoint,
			AuthEndpoint:  idp.AuthEndpoint,
			JwksURI:       idp.JwksURI,
			ClientID:      idpConfig.ClientID,
			UsernameClaim: idpConfig.UsernameClaim,
		})
	}

	// Initialize a trace provider.
	traceProviderShutdown, err := tracing.NewProvider(ctx,
		&tracing.NewProviderInput{
			Enabled: cfg.OtelTraceEnabled,
			Type:    cfg.OtelTraceType,
			Host:    cfg.OtelTraceCollectorHost,
			Port:    cfg.OtelTraceCollectorPort,
			Version: apiVersion,
		})
	if err != nil {
		return nil, fmt.Errorf("failed to initialize trace provider: %w", err)
	}
	if !cfg.OtelTraceEnabled {
		logger.Info("Tracing is disabled.")
	}

	dbClient, err := db.NewClient(
		ctx,
		cfg.DBHost,
		cfg.DBPort,
		cfg.DBName,
		cfg.DBSSLMode,
		cfg.DBUsername,
		cfg.DBPassword,
		cfg.DBMaxConnections,
		cfg.DBAutoMigrateEnabled,
		logger,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create DB client %v", err)
	}

	pluginFactory, err := plugin.NewFactory(logger, cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize plugin factory %w", err)
	}

	pluginCatalog, err := pluginFactory.Build(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to build plugin catalog %w", err)
	}

	httpClient := phoboshttp.NewHTTPClient()

	phobosIDP := auth.NewIdentityProvider(pluginCatalog.JWSProvider, cfg.TokenIssuerURL)
	userAuth := auth.NewUserAuth(ctx, oauthProviders, logger, dbClient)
	authenticator := auth.NewAuthenticator(userAuth, phobosIDP, dbClient, cfg.TokenIssuerURL)

	respWriter := response.NewWriter(logger)

	taskManager := asynctask.NewManager(time.Duration(cfg.AsyncTaskTimeout) * time.Second)

	eventManager := events.NewEventManager(dbClient, logger)
	eventManager.Start(ctx)

	emailClient := email.NewClient(pluginCatalog.EmailProvider, taskManager, dbClient, logger, cfg.PhobosUIURL, cfg.EmailFooter)
	limits := limits.NewLimitChecker(dbClient)
	logStreamStore := logstream.NewLogStore(pluginCatalog.ObjectStore)
	manager := logstream.New(logStreamStore, dbClient, eventManager, logger)
	pipelineTemplateStore := pipelinetemplate.NewDataStore(pluginCatalog.ObjectStore)
	pipelineUpdater := pipeline.NewUpdater(dbClient, logger)
	pipelineSupervisor := pipeline.NewSupervisor(pipelineUpdater, dbClient, eventManager, logger)
	pipelineToDoItemManager := eventhandlers.NewToDoItemManager(logger, dbClient, pipelineUpdater, emailClient)
	pipelineMetricManager := eventhandlers.NewMetricManager(logger, dbClient, pipelineUpdater)
	lifecycleTemplateStore := lifecycletemplate.NewDataStore(pluginCatalog.ObjectStore)
	pipelineArtifactStore := pipeline.NewStore(pluginCatalog.ObjectStore)
	releaseDataStore := release.NewDataStore(pluginCatalog.ObjectStore)
	pluginRegistryStore := pluginregistry.NewRegistryStore(pluginCatalog.ObjectStore)

	var (
		versionService          = version.NewService(dbClient, apiVersion)
		activityService         = activityevent.NewService(logger, dbClient, eventManager)
		roleService             = role.NewService(logger, dbClient, limits, activityService)
		userService             = user.NewService(logger, dbClient)
		membershipService       = membership.NewService(logger, dbClient, activityService)
		organizationService     = organization.NewService(logger, dbClient, membershipService, activityService)
		resourceLimitService    = resourcelimit.NewService(logger, dbClient)
		projectService          = project.NewService(logger, dbClient, activityService)
		projectVariableService  = projectvariable.NewService(logger, dbClient, activityService, limits)
		agentService            = agent.NewService(logger, dbClient, limits, manager, eventManager, activityService)
		jobService              = job.NewService(logger, dbClient, phobosIDP, manager, eventManager, pipelineUpdater)
		pipelineTemplateService = pipelinetemplate.NewService(logger, dbClient, limits, pipelineTemplateStore, activityService)
		environmentService      = environment.NewService(logger, dbClient, limits, activityService)
		environmentGateKeeper   = pipeline.NewEnvironmentGatekeeper(dbClient)
		pipelineService         = pipeline.NewService(logger, dbClient, limits, pipelineTemplateStore, pipelineArtifactStore,
			pipelineUpdater, eventManager, activityService, phobosIDP, environmentService, environmentGateKeeper)
		teamService                   = team.NewService(logger, dbClient, activityService)
		scimService                   = scim.NewService(logger, dbClient, phobosIDP)
		serviceAccountService         = serviceaccount.NewService(logger, dbClient, limits, phobosIDP, openIDConfigFetcher, activityService)
		approvalRuleService           = approvalrule.NewService(logger, dbClient, limits, activityService)
		lifecycleTemplateService      = lifecycletemplate.NewService(logger, dbClient, limits, lifecycleTemplateStore, activityService)
		releaseLifecycleService       = releaselifecycle.NewService(logger, dbClient, lifecycleTemplateStore, limits, activityService)
		releaseService                = release.NewService(logger, dbClient, lifecycleTemplateStore, pipelineTemplateStore, pipelineTemplateService, pipelineService, limits, activityService, releaseDataStore)
		commentService                = comment.NewService(logger, dbClient, limits, eventManager, activityService)
		pluginRegistryService         = pluginregistry.NewService(logger, dbClient, pluginRegistryStore, limits, activityService)
		todoItemService               = todoitem.NewService(logger, dbClient, eventManager)
		metricService                 = metric.NewService(logger, dbClient)
		pipelineInitializationHandler = eventhandlers.NewInitializationHandler(logger, dbClient, pipelineUpdater, pipelineTemplateStore, pipelineArtifactStore)
	)

	vcsService, err := vcs.NewService(logger, dbClient, limits, activityService, httpClient, cfg.PhobosAPIURL)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize vcs service: %v", err)
	}

	if err = createAdminUser(ctx, cfg.AdminUserEmail, dbClient, logger); err != nil {
		return nil, err
	}

	urnResolverOptions := &urn.ResolverOptions{
		OrganizationService:      organizationService,
		AgentService:             agentService,
		ProjectService:           projectService,
		ProjectVariableService:   projectVariableService,
		EnvironmentService:       environmentService,
		ServiceAccountService:    serviceAccountService,
		UserService:              userService,
		TeamService:              teamService,
		RoleService:              roleService,
		JobService:               jobService,
		MembershipService:        membershipService,
		PipelineService:          pipelineService,
		PipelineTemplateService:  pipelineTemplateService,
		ApprovalRuleService:      approvalRuleService,
		LifecycleTemplateService: lifecycleTemplateService,
		ReleaseLifecycleService:  releaseLifecycleService,
		ReleaseService:           releaseService,
		CommentService:           commentService,
		PluginRegistryService:    pluginRegistryService,
		ToDoItemService:          todoItemService,
		VCSProviderService:       vcsService,
	}

	urnResolver := urn.NewResolver(urnResolverOptions)

	resolverState := resolver.State{
		Config:                   cfg,
		RoleService:              roleService,
		UserService:              userService,
		OrganizationService:      organizationService,
		MembershipService:        membershipService,
		ResourceLimitService:     resourceLimitService,
		ProjectService:           projectService,
		ProjectVariableService:   projectVariableService,
		JobService:               jobService,
		TeamService:              teamService,
		SCIMService:              scimService,
		PipelineTemplateService:  pipelineTemplateService,
		PipelineService:          pipelineService,
		AgentService:             agentService,
		EnvironmentService:       environmentService,
		ServiceAccountService:    serviceAccountService,
		URNResolver:              urnResolver,
		ApprovalRuleService:      approvalRuleService,
		LifecycleTemplateService: lifecycleTemplateService,
		ReleaseLifecycleService:  releaseLifecycleService,
		ReleaseService:           releaseService,
		ActivityEventService:     activityService,
		CommentService:           commentService,
		PluginRegistryService:    pluginRegistryService,
		ToDoItemService:          todoItemService,
		VCSService:               vcsService,
		MetricService:            metricService,
		VersionService:           versionService,
		Logger:                   logger,
	}

	graphqlHandler, err := graphql.NewGraphQL(&resolverState, logger, pluginCatalog.GraphqlRateLimitStore, cfg.MaxGraphQLComplexity, authenticator)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize graphql handler %w", err)
	}

	requireAuthenticatedCallerMiddleware := middleware.NewRequireAuthenticatedCallerMiddleware(logger, respWriter)

	routeBuilder := api.NewRouteBuilder(
		middleware.PrometheusMiddleware,
		middleware.NewAuthenticationMiddleware(authenticator, logger, respWriter),
		middleware.HTTPRateLimiterMiddleware(
			logger,
			respWriter,
			pluginCatalog.HTTPRateLimitStore,
		), // catch all calls, including GraphQL
	).WithSubRouter("/v1")

	v1RouteBuilder := routeBuilder.SubRouteBuilder("/v1")

	routeBuilder.AddHandler("/graphql", graphqlHandler)

	// Health endpoint.
	routeBuilder.AddRoutes(controllers.NewHealthController(
		respWriter,
	))
	routeBuilder.AddRoutes(controllers.NewOIDCController(
		respWriter,
		pluginCatalog.JWSProvider,
		cfg.TokenIssuerURL,
	))
	v1RouteBuilder.AddRoutes(controllers.NewSCIMController(
		logger,
		respWriter,
		requireAuthenticatedCallerMiddleware,
		userService,
		teamService,
		scimService,
	))
	v1RouteBuilder.AddRoutes(controllers.NewPluginRegistryController(
		logger,
		respWriter,
		requireAuthenticatedCallerMiddleware,
		pluginRegistryService,
	))
	v1RouteBuilder.AddRoutes(controllers.NewVCSController(
		logger,
		respWriter,
		authenticator,
		vcsService,
	))
	discoveryController, err := controllers.NewServiceDiscoveryController(
		logger,
		respWriter,
		cfg.PhobosAPIURL,
		cfg.GRPCServerPort,
	)
	if err != nil {
		return nil, err
	}
	routeBuilder.AddRoutes(discoveryController)

	var grpcListener net.Listener

	// Create a listener for gRPC.
	listener, err := net.Listen("tcp", fmt.Sprintf(":%v", cfg.GRPCServerPort))
	if err != nil {
		return nil, fmt.Errorf("failed to listen on port %v for gRPC server: %v", cfg.GRPCServerPort, err)
	}

	if cfg.TLSEnabled {
		grpcListener = tls.NewListener(listener, tlsConfig)
	} else {
		grpcListener = listener
	}

	// Create options to pass into the gRPC server.
	// GRPC does not deal with (pipeline or release) comments.
	opts := &grpc.ServerOptions{
		Listener:                 grpcListener,
		Logger:                   logger,
		Authenticator:            authenticator,
		OrganizationService:      organizationService,
		OAuthProviders:           cfg.OauthProviders,
		ResourceLimitService:     resourceLimitService,
		ProjectService:           projectService,
		JobService:               jobService,
		PipelineTemplateService:  pipelineTemplateService,
		PipelineService:          pipelineService,
		TeamService:              teamService,
		AgentService:             agentService,
		EnvironmentService:       environmentService,
		ServiceAccountService:    serviceAccountService,
		URNResolver:              urnResolver,
		APIServerConfig:          cfg,
		LifecycleTemplateService: lifecycleTemplateService,
		ReleaseLifecycleService:  releaseLifecycleService,
		ReleaseService:           releaseService,
		PluginRegistryService:    pluginRegistryService,
		VCSService:               vcsService,
		VersionService:           versionService,
	}

	// Create and start internal agents.
	if err := startInternalAgents(ctx, cfg, dbClient, agentService, jobService, logger); err != nil {
		return nil, err
	}

	// Register pipeline handlers.
	pipelineToDoItemManager.RegisterHandlers()
	pipelineMetricManager.RegisterHandlers()
	pipelineInitializationHandler.RegisterHandlers()

	// Start pipeline supervisor
	pipelineSupervisor.Start(ctx)

	return &APIServer{
		logger:      logger,
		dbClient:    dbClient,
		taskManager: taskManager,
		grpcServer:  grpc.NewServer(opts),
		srv: &http.Server{
			Addr:              fmt.Sprintf(":%v", cfg.HTTPServerPort),
			Handler:           routeBuilder.Router(),
			ReadHeaderTimeout: time.Minute,
			TLSConfig:         tlsConfig,
		},
		traceShutdown: traceProviderShutdown,
		tlsConfig:     tlsConfig,
	}, nil
}

// Start will start the server
func (api *APIServer) Start() {
	go func() {
		// Serve Prometheus endpoint on its own port since it
		// won't be publicly exposed
		promServer := &http.Server{
			Addr:              ":9090",
			Handler:           promhttp.Handler(),
			ReadHeaderTimeout: 3 * time.Second,
			TLSConfig:         api.tlsConfig,
		}

		api.logger.Infof("Prometheus server listening on %s", promServer.Addr)

		var err error
		if api.tlsConfig != nil {
			err = promServer.ListenAndServeTLS("", "")
		} else {
			err = promServer.ListenAndServe()
		}

		if err != nil {
			api.logger.Errorf("Prometheus server failed to start: %v", err)
			return
		}
	}()

	// Start gRPC server on its own port.
	go func() {
		if err := api.grpcServer.Start(); err != nil {
			api.logger.Errorf("GRPC startup error: %v", err)
			os.Exit(1)
		}
	}()

	// Start main server
	var err error
	if api.tlsConfig != nil {
		api.logger.Infof("HTTPS server listening on %s", api.srv.Addr)
		err = api.srv.ListenAndServeTLS("", "")
	} else {
		api.logger.Infof("HTTP server listening on %s", api.srv.Addr)
		err = api.srv.ListenAndServe()
	}

	if err != nil {
		api.logger.Errorf("HTTP server failed to start: %v", err)
	}
}

// Shutdown will shutdown the API server
func (api *APIServer) Shutdown(ctx context.Context) {
	api.shutdownOnce.Do(func() {
		api.logger.Info("Starting HTTP server shutdown")

		// Shutdown HTTP server
		if err := api.srv.Shutdown(ctx); err != nil {
			api.logger.Errorf("failed to shutdown HTTP server gracefully: %v", err)
		}

		api.logger.Info("HTTP server shutdown successfully")

		// Shutdown gRPC server. Messages are already logged.
		api.grpcServer.Shutdown()

		// Shutdown trace provider.
		if err := api.traceShutdown(ctx); err != nil {
			api.logger.Errorf("Shutdown trace provider failed: %w", err)
		} else {
			api.logger.Info("Shutdown trace provider successfully.")
		}

		api.logger.Info("Starting Async Task Manager shutdown")
		api.taskManager.Shutdown()
		api.logger.Info("Async Task Manager shutdown successfully")

		api.logger.Info("Completed graceful shutdown")
	})
}

func startInternalAgents(
	ctx context.Context,
	config *config.Config,
	dbClient *db.Client,
	agentService agent.Service,
	jobService job.Service,
	logger logger.Logger,
) error {
	agentClient := agt.NewInternalClient(agentService, jobService)

	for _, a := range config.InternalAgents {
		existingAgent, err := dbClient.Agents.GetAgentByPRN(ctx, models.AgentResource.BuildPRN(a.Name))
		if err != nil {
			return fmt.Errorf("failed to get an internal agent: %w", err)
		}

		var internalAgent *models.Agent
		if existingAgent != nil {
			// Use the existing agent.
			internalAgent = existingAgent
		} else {
			// Create DB entry for the agent
			internalAgent, err = dbClient.Agents.CreateAgent(ctx, &models.Agent{
				Type:            models.SharedAgent,
				Name:            a.Name,
				CreatedBy:       "system",
				Description:     "The default shared agent for the system.",
				RunUntaggedJobs: true,
				Tags:            []string{},
			})
			if err != nil {
				return fmt.Errorf("failed to create internal agent: %w", err)
			}
		}

		logger.Infof("starting internal agent %s", a.Name)

		runner, err := agt.NewAgent(
			ctx,
			gid.ToGlobalID(gid.AgentType, internalAgent.Metadata.ID),
			logger,
			agentClient,
			&agt.JobDispatcherSettings{
				DispatcherType: a.JobDispatcherType,
				PluginData:     a.JobDispatcherData,
			},
		)
		if err != nil {
			return fmt.Errorf("failed to create an internal agent instance: %w", err)
		}

		go runner.Start(auth.WithCaller(ctx, &auth.SystemCaller{}))
	}

	return nil
}

func createAdminUser(ctx context.Context, adminUserEmail string, dbClient *db.Client, logger logger.Logger) error {
	// Create the admin user if an email is provided.
	if adminUserEmail != "" {
		user, err := dbClient.Users.GetUserByEmail(ctx, adminUserEmail)
		if err != nil {
			return err
		}
		if user == nil {
			if _, err := dbClient.Users.CreateUser(ctx, &models.User{
				Username: auth.ParseUsername(adminUserEmail),
				Email:    adminUserEmail,
				Active:   true,
				Admin:    true,
			}); err != nil {
				return fmt.Errorf("failed to create admin user: %v", err)
			}
			logger.Infof("User with email %s created.", adminUserEmail)
		} else {
			logger.Infof("User with email %s already exists. Skipping creation.", adminUserEmail)
		}
	}

	return nil
}
