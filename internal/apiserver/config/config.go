// Package config contains the configuration for the API.
// It supports variables passed in via a YAML file or
// through the host.
package config

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/qiangxue/go-env"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gopkg.in/yaml.v2"
)

const (
	defaultHTTPServerPort            = "9000"
	defaultGRPCServerPort            = "9010"
	envOidcProviderConfigPrefix      = "PHOBOS_OAUTH_PROVIDERS_"
	envAgentConfigPrefix             = "PHOBOS_INTERNAL_AGENTS_"
	defaultMaxGraphQLComplexity      = 0
	defaultRateLimitStorePluginType  = "memory"
	defaultAsyncTaskTimeout          = 100 // seconds
	defaultDBAutoMigrateEnabled      = true
	defaultOtelTraceEnabled          = false
	defaultHTTPRateLimit             = 60                 // in calls per second
	defaultTemplateMaxUploadSize     = 1024 * 128         // 128KiB
	defaultPluginBinaryMaxUploadSize = 1024 * 1024 * 1024 // 1GiB
)

// IdpConfig contains the config fields for an Identity Provider
type IdpConfig struct {
	IssuerURL     string `yaml:"issuer_url"`
	ClientID      string `yaml:"client_id"`
	UsernameClaim string `yaml:"username_claim"`
	Scope         string `yaml:"scope"`
	LogoutURL     string `yaml:"logout_url"`
}

// AgentConfig contains the config fields for a system agent
type AgentConfig struct {
	JobDispatcherData map[string]string `yaml:"job_dispatcher_data"`
	Name              string            `yaml:"name"`
	JobDispatcherType string            `yaml:"job_dispatcher_type"`
}

// Config represents an application configuration.
type Config struct {
	// Plugin Data
	ObjectStorePluginData    map[string]string `yaml:"object_store_plugin_data"`
	RateLimitStorePluginData map[string]string `yaml:"rate_limit_store_plugin_data" env:"RATE_LIMIT_STORE_PLUGIN_DATA"`
	JWSProviderPluginData    map[string]string `yaml:"jws_provider_plugin_data"`
	EmailClientPluginData    map[string]string `yaml:"email_client_plugin_data"`

	// Plugin Type
	ObjectStorePluginType    string `yaml:"object_store_plugin_type" env:"OBJECT_STORE_PLUGIN_TYPE"`
	RateLimitStorePluginType string `yaml:"rate_limit_store_plugin_type" env:"RATE_LIMIT_STORE_PLUGIN_TYPE"`
	JWSProviderPluginType    string `yaml:"jws_provider_plugin_type" env:"JWS_PROVIDER_PLUGIN_TYPE"`
	EmailClientPluginType    string `yaml:"email_client_plugin_type" env:"EMAIL_CLIENT_PLUGIN_TYPE"`

	EmailFooter string `yaml:"email_footer" env:"EMAIL_FOOTER"`

	// The external facing URL for the Phobos API
	PhobosAPIURL string `yaml:"phobos_api_url" env:"API_URL"`
	// The external facing URL for the Phobos Frontend UI
	PhobosUIURL string `yaml:"phobos_ui_url" env:"UI_URL"`

	TLSEnabled bool `yaml:"tls_enabled" env:"TLS_ENABLED"`

	TLSCertFile string `yaml:"tls_cert_file" env:"TLS_CERT_FILE"`

	TLSKeyFile string `yaml:"tls_key_file" env:"TLS_KEY_FILE"`

	// The HTTP server port. Defaults to 9000
	HTTPServerPort string `yaml:"http_server_port" env:"HTTP_SERVER_PORT"`

	// The GRPC server port. Defaults to 9010.
	GRPCServerPort string `yaml:"grpc_server_port" env:"GRPC_SERVER_PORT"`

	TokenIssuerURL string `yaml:"TOKEN_ISSUER_URL" env:"TOKEN_ISSUER_URL"`

	// The URL for connecting to the database. required.
	DBHost     string `yaml:"db_host" env:"DB_HOST"`
	DBName     string `yaml:"db_name" env:"DB_NAME"`
	DBSSLMode  string `yaml:"db_ssl_mode" env:"DB_SSL_MODE"`
	DBUsername string `yaml:"db_username" env:"DB_USERNAME,secret"`
	DBPassword string `yaml:"db_password" env:"DB_PASSWORD,secret"`

	// AdminUserEmail is optional and will create a system admin user with this email.
	AdminUserEmail string `yaml:"admin_user_email" env:"ADMIN_USER_EMAIL"`

	OtelTraceType          string `yaml:"otel_trace_type" env:"OTEL_TRACE_TYPE"`
	OtelTraceCollectorHost string `yaml:"otel_trace_host" env:"OTEL_TRACE_HOST"`

	// The OIDC identity providers
	OauthProviders []IdpConfig `yaml:"oauth_providers"`

	InternalAgents []AgentConfig `yaml:"internal_agents"`

	// Database Configuration
	DBMaxConnections int `yaml:"db_max_connections" env:"DB_MAX_CONNECTIONS"`
	DBPort           int `yaml:"db_port" env:"DB_PORT"`

	MaxGraphQLComplexity int `yaml:"max_graphql_complexity" env:"MAX_GRAPHQL_COMPLEXITY"`

	// Timout for async background tasks
	AsyncTaskTimeout int `yaml:"async_task_timeout" env:"ASYNC_TASK_TIMEOUT"`

	OtelTraceCollectorPort int `yaml:"otel_trace_port" env:"OTEL_TRACE_PORT"`

	OtelTraceEnabled bool `yaml:"otel_trace_enabled" env:"OTEL_TRACE_ENABLED"`

	// Whether to auto migrate the database
	DBAutoMigrateEnabled bool `yaml:"db_auto_migrate_enabled" env:"DB_AUTO_MIGRATE_ENABLED"`

	// HTTP rate limit value
	HTTPRateLimit int `yaml:"http_rate_limit" env:"HTTP_RATE_LIMIT"`

	// Max upload size for GRPC template upload
	TemplateMaxUploadSize int64 `yaml:"template_max_upload_size" env:"TEMPLATE_MAX_UPLOAD_SIZE"`

	// Max upload size for GRPC plugin binary upload
	PluginBinaryMaxUploadSize int64 `yaml:"plugin_binary_max_upload_size" env:"PLUGIN_BINARY_MAX_UPLOAD_SIZE"`
}

// Validate validates the application configuration.
func (c Config) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.HTTPServerPort, is.Port),
		validation.Field(&c.GRPCServerPort, is.Port),
		validation.Field(&c.ObjectStorePluginType, validation.Required),
		validation.Field(&c.JWSProviderPluginType, validation.Required),
		validation.Field(&c.PhobosAPIURL, validation.Required),
	)
}

// Load returns an application configuration which is populated from the given configuration file and environment variables.
func Load(file string, logger logger.Logger) (*Config, error) {
	// default config
	c := Config{
		HTTPServerPort:            defaultHTTPServerPort,
		GRPCServerPort:            defaultGRPCServerPort,
		MaxGraphQLComplexity:      defaultMaxGraphQLComplexity,
		RateLimitStorePluginType:  defaultRateLimitStorePluginType,
		AsyncTaskTimeout:          defaultAsyncTaskTimeout,
		DBAutoMigrateEnabled:      defaultDBAutoMigrateEnabled,
		OtelTraceEnabled:          defaultOtelTraceEnabled,
		HTTPRateLimit:             defaultHTTPRateLimit,
		TemplateMaxUploadSize:     defaultTemplateMaxUploadSize,
		PluginBinaryMaxUploadSize: defaultPluginBinaryMaxUploadSize,
	}

	// load from YAML config file
	if file != "" {
		bytes, err := os.ReadFile(filepath.Clean(file))
		if err != nil {
			return nil, fmt.Errorf("failed to load config file: %w", err)
		}
		if err = yaml.Unmarshal(bytes, &c); err != nil {
			return nil, fmt.Errorf("failed to parse yaml config file: %w", err)
		}
	}

	// load from environment variables prefixed with "PHOBOS_"
	if err := env.New("PHOBOS_", logger.Infof).Load(&c); err != nil {
		return nil, fmt.Errorf("failed to load env variables: %w", err)
	}

	// Load OAUTH IDP config from environment is available
	oauthProviders, err := loadOauthConfigFromEnvironment()
	if err != nil {
		return nil, fmt.Errorf("failed to load oauth provider env variables: %w", err)
	}

	if len(oauthProviders) > 0 {
		c.OauthProviders = oauthProviders
	}

	agents, err := loadAgentConfigFromEnvironment()
	if err != nil {
		return nil, fmt.Errorf("failed to load agent env variables %w", err)
	}

	if len(agents) > 0 {
		c.InternalAgents = agents
	}

	if c.JWSProviderPluginData == nil {
		c.JWSProviderPluginData = make(map[string]string)
	}

	if c.ObjectStorePluginData == nil {
		c.ObjectStorePluginData = make(map[string]string)
	}

	if c.RateLimitStorePluginData == nil {
		c.RateLimitStorePluginData = make(map[string]string)
	}

	if c.EmailClientPluginData == nil {
		c.EmailClientPluginData = make(map[string]string)
	}

	// Load JWS Provider plugin data
	for k, v := range loadPluginData("PHOBOS_JWS_PROVIDER_PLUGIN_DATA_") {
		c.JWSProviderPluginData[k] = v
	}

	// Load Object Store plugin data
	for k, v := range loadPluginData("PHOBOS_OBJECT_STORE_PLUGIN_DATA_") {
		c.ObjectStorePluginData[k] = v
	}

	// Load Rate Limiter plugin data
	for k, v := range loadPluginData("PHOBOS_RATE_LIMIT_STORE_PLUGIN_DATA_") {
		c.RateLimitStorePluginData[k] = v
	}

	// Load Email Client plugin data
	for k, v := range loadPluginData("PHOBOS_EMAIL_CLIENT_PLUGIN_DATA_") {
		c.EmailClientPluginData[k] = v
	}

	// Default TokenIssuerURL to PhobosURL
	if c.TokenIssuerURL == "" {
		c.TokenIssuerURL = c.PhobosAPIURL
	}

	// Validation
	if err := c.Validate(); err != nil {
		return nil, fmt.Errorf("invalid config: %w", err)
	}

	return &c, nil
}

func loadPluginData(envPrefix string) map[string]string {
	pluginData := make(map[string]string)

	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)

		key := pair[0]
		val := pair[1]

		if strings.HasPrefix(key, envPrefix) {
			pluginDataKey := strings.ToLower(key[len(envPrefix):])
			pluginData[pluginDataKey] = val
		}
	}

	return pluginData
}

func loadOauthConfigFromEnvironment() ([]IdpConfig, error) {
	var idpConfigs []IdpConfig
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)

		key := pair[0]
		val := pair[1]
		if strings.HasPrefix(key, envOidcProviderConfigPrefix) && strings.HasSuffix(key, "_ISSUER_URL") {
			// Build IDP config
			index := key[len(envOidcProviderConfigPrefix) : len(key)-len("_ISSUER_URL")]
			issuerURL := val

			clientIDKey := envOidcProviderConfigPrefix + index + "_CLIENT_ID"
			usernameClaimKey := envOidcProviderConfigPrefix + index + "_USERNAME_CLAIM"
			scopeKey := envOidcProviderConfigPrefix + index + "_SCOPE"
			logoutKey := envOidcProviderConfigPrefix + index + "_LOGOUT_URL"

			clientID := os.Getenv(clientIDKey)
			usernameClaim := os.Getenv(usernameClaimKey)
			scope := os.Getenv(scopeKey)
			logoutURL := os.Getenv(logoutKey)

			if clientID == "" {
				return nil, errors.New(clientIDKey + " environment variable is required")
			}

			if usernameClaim == "" {
				usernameClaim = "sub"
			}

			idpConfigs = append(idpConfigs, IdpConfig{
				IssuerURL:     issuerURL,
				ClientID:      clientID,
				UsernameClaim: usernameClaim,
				Scope:         scope,
				LogoutURL:     logoutURL,
			})
		}
	}
	return idpConfigs, nil
}

func loadAgentConfigFromEnvironment() ([]AgentConfig, error) {
	var agentConfigs []AgentConfig
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)

		key := pair[0]
		val := pair[1]
		if strings.HasPrefix(key, envAgentConfigPrefix) && strings.HasSuffix(key, "_NAME") {
			// Build agent config
			index := key[len(envAgentConfigPrefix) : len(key)-len("_NAME")]
			name := val

			dispatcherTypeKey := envAgentConfigPrefix + index + "_JOB_DISPATCHER_TYPE"
			dispatcherDataKey := envAgentConfigPrefix + index + "_JOB_DISPATCHER_DATA_"

			dispatcherType := os.Getenv(dispatcherTypeKey)

			if dispatcherType == "" {
				return nil, errors.New(dispatcherTypeKey + " environment variable is required")
			}

			jobDispatcherData := make(map[string]string)

			// Load Job Dispatcher plugin data
			for k, v := range loadPluginData(dispatcherDataKey) {
				jobDispatcherData[k] = v
			}

			agentConfigs = append(agentConfigs, AgentConfig{
				Name:              name,
				JobDispatcherType: dispatcherType,
				JobDispatcherData: jobDispatcherData,
			})
		}
	}
	return agentConfigs, nil
}
