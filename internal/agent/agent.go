// Package agent provides the agent that will claim and run the jobs.
package agent

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/jobdispatcher"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/jobdispatcher/docker"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/jobdispatcher/ecs"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/jobdispatcher/kubernetes"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/jobdispatcher/local"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/metric"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const (
	checkRunsInterval       = 1 * time.Second
	checkRunsFailedInterval = 30 * time.Second
)

var (
	claimJobCount = metric.NewCounter("claim_job_count", "Amount of jobs claimed.")
	claimJobFails = metric.NewCounter("claim_job_fails_count", "Amount of jobs claims failed.")

	launchJobFails = metric.NewCounter("launch_job_fails", "Amount of launch jobs failed.")

	jobDispatchCount = metric.NewCounter("job_dispatch_count", "Amount of jobs dispatched.")
	jobDispatchTime  = metric.NewHistogram("job_dispatch_time", "Amount of time a job took to dispatch.", 1, 2, 8)
)

// launchJobOptions are the options for launching a job
type launchJobOptions struct {
	imageOverride *string
	jobID         string
	token         string
}

// JobDispatcherSettings defines the job dispatcher that'll be used for this agent
type JobDispatcherSettings struct {
	PluginData     map[string]string
	DispatcherType string
}

// Agent will claim the next available job and dispatch it using the configured job dispatcher
type Agent struct {
	jobDispatcher jobdispatcher.JobDispatcher
	logger        logger.Logger
	client        Client
	agentID       string
}

// NewAgent creates a new Agent
func NewAgent(
	ctx context.Context,
	agentID string,
	logger logger.Logger,
	client Client,
	jobDispatcherSettings *JobDispatcherSettings,
) (*Agent, error) {
	dispatcher, err := newJobDispatcherPlugin(ctx, logger, jobDispatcherSettings)
	if err != nil {
		return nil, fmt.Errorf("failed to create job dispatcher: %w", err)
	}

	return &Agent{agentID: agentID, jobDispatcher: dispatcher, logger: logger, client: client}, nil
}

// Start will start the agent so it can begin picking up jobs
func (r *Agent) Start(ctx context.Context) {
	defer r.logger.Info("Agent session has ended")

	r.logger.Info("Creating new agent session")

	sessionID, err := r.client.CreateSession(ctx, r.agentID)
	if err != nil {
		r.logger.Errorf("Failed to create agent session %v", err)
		return
	}

	ctx, cancel := context.WithCancel(ctx)

	// Send keep alive
	go func() {
		if err := r.sendHeartbeat(ctx, sessionID); err != nil {
			r.logger.Errorf("failed to send heartbeat: %v", err)
			cancel()
		}
	}()

	for {
		r.logger.Info("Waiting for next available run")

		resp, err := r.client.ClaimJob(ctx, &ClaimJobInput{
			AgentID: r.agentID,
		})
		claimJobCount.Inc()

		if err != nil {

			// Check if context has been canceled
			if ctx.Err() != nil {
				return
			}
			claimJobFails.Inc()
			r.handleError(ctx, sessionID, fmt.Errorf("failed to request next available job %v", err))

			select {
			case <-ctx.Done():
			case <-time.After(checkRunsFailedInterval):
			}
		} else {
			r.logger.Infof("Claimed job with ID %s", resp.JobID)

			if err := r.launchJob(
				ctx,
				&launchJobOptions{
					imageOverride: resp.ImageOverride,
					jobID:         resp.JobID,
					token:         resp.Token,
				},
			); err != nil {
				launchJobFails.Inc()
				r.handleError(ctx, sessionID, fmt.Errorf("failed to launch job %v", err))
			}

			select {
			case <-ctx.Done():
			case <-time.After(checkRunsInterval):
			}
		}
	}
}

func (r *Agent) handleError(ctx context.Context, sessionID string, err error) {
	r.logger.Error(err)
	if sErr := r.client.SendError(ctx, sessionID, err); sErr != nil {
		r.logger.Errorf("failed to send error %v", sErr)
	}
}

func (r *Agent) launchJob(ctx context.Context, o *launchJobOptions) error {
	// Set any overrides
	opts := []types.DispatchJobOption{}
	if o.imageOverride != nil {
		// Use custom image
		opts = append(opts, types.WithImage(*o.imageOverride))
	}

	// For measuring dispatch time in seconds.
	start := time.Now()
	executorID, err := r.jobDispatcher.DispatchJob(ctx, o.jobID, o.token, opts...)
	duration := time.Since(start)
	jobDispatchTime.Observe(float64(duration.Seconds()))
	jobDispatchCount.Inc()
	if err != nil {
		return err
	}

	r.logger.Infof("Job %s running in executor %s", o.jobID, executorID)

	return nil
}

func (r *Agent) sendHeartbeat(ctx context.Context, sessionID string) error {
	for {
		select {
		case <-ctx.Done():
			return nil
		case <-time.After(models.AgentSessionHeartbeatInterval):
			// Send heartbeat
			if err := r.client.SendHeartbeat(ctx, sessionID); err != nil {
				return err
			}
		}
	}
}

func newJobDispatcherPlugin(ctx context.Context, logger logger.Logger, settings *JobDispatcherSettings) (jobdispatcher.JobDispatcher, error) {
	var (
		plugin jobdispatcher.JobDispatcher
		err    error
	)

	switch settings.DispatcherType {
	case "kubernetes":
		plugin, err = kubernetes.New(ctx, settings.PluginData, logger)
	case "ecs":
		plugin, err = ecs.New(ctx, settings.PluginData, logger)
	case "docker":
		plugin, err = docker.New(settings.PluginData, logger)
	case "local":
		plugin, err = local.New(settings.PluginData, logger)
	default:
		err = fmt.Errorf("the specified Job Executor plugin %s is not currently supported", settings.DispatcherType)
	}

	return plugin, err
}
