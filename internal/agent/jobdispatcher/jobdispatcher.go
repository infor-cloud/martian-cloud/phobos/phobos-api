// Package jobdispatcher package
package jobdispatcher

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
)

// JobDispatcher is used to dispatch a job to various runtime environments
type JobDispatcher interface {
	DispatchJob(ctx context.Context, jobID string, token string, opts ...types.DispatchJobOption) (string, error)
}
