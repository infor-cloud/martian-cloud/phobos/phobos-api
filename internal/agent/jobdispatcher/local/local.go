// Package local package
package local

import (
	"context"
	"fmt"

	"github.com/dustin/go-humanize"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

var pluginDataRequiredFields = []string{"endpoint"}

// JobDispatcher is used for debugging jobs and should not be used in production
type JobDispatcher struct {
	logger          logger.Logger
	endpoint        string
	volumeSizeLimit int
}

// New creates a JobDispatcher
func New(pluginData map[string]string, logger logger.Logger) (*JobDispatcher, error) {
	for _, field := range pluginDataRequiredFields {
		if _, ok := pluginData[field]; !ok {
			return nil, fmt.Errorf("local job dispatcher requires plugin data '%s' field", field)
		}
	}

	var volumeSizeLimit int
	if vLimit, ok := pluginData["volume_size_limit"]; ok {
		tmp, mErr := humanize.ParseBytes(vLimit)
		if mErr != nil {
			return nil, fmt.Errorf("failed to parse job dispatcher 'volume_size_limit' config: %w", mErr)
		}
		if tmp <= 0 {
			return nil, fmt.Errorf("invalid value for 'volume_size_limit' config: %s", vLimit)
		}
		volumeSizeLimit = int(tmp)
	}

	return &JobDispatcher{
		logger:          logger,
		endpoint:        pluginData["endpoint"],
		volumeSizeLimit: volumeSizeLimit,
	}, nil
}

// DispatchJob will launch a local job executor that can be used to facilitate debugging
func (l *JobDispatcher) DispatchJob(ctx context.Context, jobID string, token string, opts ...types.DispatchJobOption) (string, error) {
	customOpts := types.GetDispatchJobOptions(opts...)

	if customOpts.Image() != nil {
		return "", fmt.Errorf("custom images are not supported by the local job dispatcher")
	}

	client, err := client.NewGRPCClient(ctx, l.logger, &client.GRPCClientOptions{
		HTTPEndpoint: l.endpoint,
		Token:        token,
	})
	if err != nil {
		return "", err
	}

	go func() {
		defer func() {
			if err := client.Close(); err != nil {
				l.logger.Errorf("error closing client in local job dispatcher: %v", err)
			}
		}()

		jobCtx, cancel := context.WithCancel(context.Background())
		defer cancel()

		// Create job config
		cfg := jobexecutor.JobConfig{
			JobID:           jobID,
			JobToken:        token,
			HTTPEndpoint:    l.endpoint,
			VolumeSizeLimit: l.volumeSizeLimit,
		}

		// Start the job executor
		executor, err := jobexecutor.NewJobExecutor(jobCtx, &cfg, client, l.logger)
		if err != nil {
			l.logger.Errorf("failed to create job executor in local job dispatcher: %v", err)
			return
		}

		if err := executor.Execute(jobCtx); err != nil {
			l.logger.Errorf("error running job in local job dispatcher: %v", err)
		}
	}()

	return "local", nil
}
