// Package kubernetes package
package kubernetes

//go:generate go tool mockery --name client --inpackage --case underscore

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	"github.com/dustin/go-humanize"
	"github.com/ryanuber/go-glob"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/jobdispatcher/kubernetes/configurer"
	ekscfg "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/jobdispatcher/kubernetes/configurer/eks"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
	v1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

var (
	pluginDataRequiredFields        = []string{"endpoint", "auth_type", "image", "memory_request", "memory_limit"}
	requireEKSIAMAuthFields         = []string{"region", "eks_cluster"}
	_                        client = (*k8sRunner)(nil)
)

type client interface {
	CreateJob(context.Context, *v1.Job) (*v1.Job, error)
}

type k8sRunner struct {
	logger     logger.Logger
	configurer configurer.Configurer
	namespace  string
}

// CreateJob get a kubernetes config, sets up the client and creates the batch job.
func (k *k8sRunner) CreateJob(ctx context.Context, job *v1.Job) (*v1.Job, error) {
	config, err := k.configurer.GetConfig(ctx)
	if err != nil {
		return nil, err
	}

	cs, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return cs.BatchV1().Jobs(k.namespace).Create(ctx, job, metav1.CreateOptions{})
}

// JobDispatcher uses a kubernetes client to dispatch jobs
type JobDispatcher struct {
	logger          logger.Logger
	client          client
	image           string
	endpoint        string
	memoryRequest   resource.Quantity
	memoryLimit     resource.Quantity
	volumeSizeLimit int
	allowedImages   []string
	securityContext *corev1.SecurityContext
}

// New creates a JobDispatcher
func New(ctx context.Context, pluginData map[string]string, logger logger.Logger) (*JobDispatcher, error) {
	for _, field := range pluginDataRequiredFields {
		if _, ok := pluginData[field]; !ok {
			return nil, fmt.Errorf("kubernetes job dispatcher requires plugin data '%s' field", field)
		}
	}

	var (
		c   configurer.Configurer
		err error
	)
	switch pluginData["auth_type"] {
	case "eks_iam":
		for _, field := range requireEKSIAMAuthFields {
			if _, ok := pluginData[field]; !ok {
				return nil, fmt.Errorf("auth_type 'eks_iam' requires plugin data '%s' field", field)
			}
		}

		c, err = ekscfg.New(ctx, pluginData["region"], pluginData["eks_cluster"])
		if err != nil {
			return nil, fmt.Errorf("failed to configure EKS IAM plugin: %v", err)
		}
	default:
		return nil, fmt.Errorf("kubernetes job dispatcher doesn't support auth_type '%s'", pluginData["auth_type"])
	}

	namespace := "default"
	if ns, ok := pluginData["namespace"]; ok {
		namespace = ns
	}

	var runAsUser *int64
	if runAsUserStr, ok := pluginData["security_context_run_as_user"]; ok {
		val, err := strconv.ParseInt(runAsUserStr, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to parse security_context_run_as_user for runner jobs: %v", err)
		}
		runAsUser = &val
	}

	var runAsGroup *int64
	if runAsGroupStr, ok := pluginData["security_context_run_as_group"]; ok {
		val, err := strconv.ParseInt(runAsGroupStr, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to parse security_context_run_as_group for runner jobs: %v", err)
		}
		runAsGroup = &val
	}

	var runAsNonRoot *bool
	if runAsNonRootStr, ok := pluginData["security_context_run_as_non_root"]; ok {
		val, err := strconv.ParseBool(runAsNonRootStr)
		if err != nil {
			return nil, fmt.Errorf("failed to parse security_context_run_as_non_root for runner jobs: %v", err)
		}
		runAsNonRoot = &val
	}

	var allowedImages []string
	if allowedImagesStr, ok := pluginData["allowed_images"]; ok {
		allowedImages = strings.Split(allowedImagesStr, ",")
	}

	memoryRequest, err := resource.ParseQuantity(pluginData["memory_request"])
	if err != nil {
		return nil, fmt.Errorf("failed to parse memory request for runner jobs: %v", err)
	}

	memoryLimit, err := resource.ParseQuantity(pluginData["memory_limit"])
	if err != nil {
		return nil, fmt.Errorf("failed to parse memory limit for runner jobs: %v", err)
	}

	var volumeSizeLimit int
	if vLimit, ok := pluginData["volume_size_limit"]; ok {
		tmp, mErr := humanize.ParseBytes(vLimit)
		if mErr != nil {
			return nil, fmt.Errorf("failed to parse job dispatcher 'volume_size_limit' config: %w", mErr)
		}
		if tmp <= 0 {
			return nil, fmt.Errorf("invalid value for 'volume_size_limit' config: %s", vLimit)
		}
		volumeSizeLimit = int(tmp)
	}

	return &JobDispatcher{
		logger:          logger,
		image:           pluginData["image"],
		endpoint:        pluginData["endpoint"],
		memoryRequest:   memoryRequest,
		memoryLimit:     memoryLimit,
		volumeSizeLimit: volumeSizeLimit,
		allowedImages:   allowedImages,
		securityContext: &corev1.SecurityContext{
			Privileged:               ptr.Bool(false),
			AllowPrivilegeEscalation: ptr.Bool(false),
			RunAsUser:                runAsUser,
			RunAsGroup:               runAsGroup,
			RunAsNonRoot:             runAsNonRoot,
			// TODO: Add host users option when user namespace feature is generally available
			//HostUsers:                    ptr.Bool(false),
			Capabilities: &corev1.Capabilities{
				Drop: []corev1.Capability{"NET_RAW"},
			},
		},
		client: &k8sRunner{
			logger:     logger,
			namespace:  namespace,
			configurer: c,
		},
	}, nil
}

// DispatchJob will start a kubernetes batch job to execute the job
func (j *JobDispatcher) DispatchJob(ctx context.Context, jobID string, token string, opts ...types.DispatchJobOption) (string, error) {
	overrides := types.GetDispatchJobOptions(opts...)

	var (
		image                     = j.image
		mainContainerCommand      []string
		mainContainerCommandArgs  []string
		initContainers            []corev1.Container
		podVolumes                []corev1.Volume
		mainContainerVolumeMounts []corev1.VolumeMount
	)

	if overrides.Image() != nil {
		image = *overrides.Image()

		if err := j.verifyAllowedImages(image); err != nil {
			return "", err
		}

		// Our main volume that shares data between init and main containers.
		podVolumes = []corev1.Volume{
			{
				Name:         "data",
				VolumeSource: corev1.VolumeSource{EmptyDir: &corev1.EmptyDirVolumeSource{}},
			},
		}

		// Init container that copies our job, phobos and ca-certificates.crt to the data volume.
		initContainers = []corev1.Container{
			{
				Name:  "init",
				Image: j.image,
				VolumeMounts: []corev1.VolumeMount{
					{
						Name:      "data",
						MountPath: "/phobos-data",
					},
				},
				SecurityContext: j.securityContext,
				WorkingDir:      "/app",
				Command:         []string{"./prepare_for_custom_container.sh"},
			},
		}

		// Main or custom container that runs the job.
		mainContainerVolumeMounts = []corev1.VolumeMount{
			{
				Name:      "data",
				MountPath: "/phobos-data",
			},
		}

		mainContainerCommandArgs = []string{"./job"}
		mainContainerCommand = []string{"/phobos-data/custom_container_entrypoint.sh"}

		j.logger.Infof("Using kubernetes job dispatcher with custom image '%s' for job %s", image, jobID)
	}

	k8sJob := &v1.Job{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "batch/v1",
			Kind:       "Job",
		},
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: "phobos-job-" + strings.ToLower(jobID[:8]),
		},
		Spec: v1.JobSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"cluster-autoscaler.kubernetes.io/safe-to-evict": "false",
					},
					Annotations: map[string]string{
						"job.phobos.io/id": jobID,
					},
				},
				Spec: corev1.PodSpec{
					InitContainers:               initContainers,
					Volumes:                      podVolumes,
					AutomountServiceAccountToken: ptr.Bool(false),
					Containers: []corev1.Container{
						{
							Name:            "main",
							Image:           image,
							Command:         mainContainerCommand,
							Args:            mainContainerCommandArgs,
							VolumeMounts:    mainContainerVolumeMounts,
							SecurityContext: j.securityContext,
							Env: []corev1.EnvVar{
								{
									Name:  "JOB_ID",
									Value: jobID,
								},
								{
									Name:  "JOB_TOKEN",
									Value: token,
								},
								{
									Name:  "ENDPOINT",
									Value: j.endpoint,
								},
								{
									Name:  "MEMORY_LIMIT",
									Value: j.memoryLimit.String(),
								},
								{
									Name:  "VOLUME_SIZE_LIMIT",
									Value: strconv.Itoa(j.volumeSizeLimit),
								},
							},
							Resources: corev1.ResourceRequirements{
								Requests: corev1.ResourceList{
									corev1.ResourceMemory: j.memoryRequest,
								},
								Limits: corev1.ResourceList{
									corev1.ResourceMemory: j.memoryLimit,
								},
							},
						},
					},
					RestartPolicy: corev1.RestartPolicyNever,
				},
			},
			// Disable retries
			BackoffLimit: ptr.Int32(0),
			// Remove once completed
			TTLSecondsAfterFinished: ptr.Int32(0),
		},
	}

	result, err := j.client.CreateJob(ctx, k8sJob)
	if err != nil {
		return "", fmt.Errorf("kubernetes job dispatcher failed to run for job %s: %v", jobID, err)
	}

	return string(result.UID), nil
}

func (j *JobDispatcher) verifyAllowedImages(image string) error {
	for _, pattern := range j.allowedImages {
		// First check for exact match
		if pattern == image {
			return nil
		}

		// Check for glob pattern match
		if match := glob.Glob(pattern, image); match {
			return nil
		}
	}

	return fmt.Errorf("image '%s' is not allowed, check the allowed_images settings for the agent", image)
}
