package kubernetes

import (
	"context"
	"fmt"
	"reflect"
	"strconv"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	v1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubetypes "k8s.io/apimachinery/pkg/types"
)

func Test_k8sRunner_CreateJob(t *testing.T) {
	type args struct {
		ctx context.Context
		job *v1.Job
	}
	tests := []struct {
		args    args
		k       *k8sRunner
		want    *v1.Job
		name    string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.k.CreateJob(tt.args.ctx, tt.args.job)
			if (err != nil) != tt.wantErr {
				t.Errorf("k8sRunner.CreateJob() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("k8sRunner.CreateJob() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNew(t *testing.T) {
	type args struct {
		ctx        context.Context
		pluginData map[string]string
		logger     logger.Logger
	}
	tests := []struct {
		args    args
		want    func(*testing.T, *JobDispatcher)
		name    string
		wantErr bool
	}{
		{
			name: "Missing Require Plugin Data fails",
			args: args{
				ctx:        context.TODO(),
				pluginData: map[string]string{},
				logger:     nil,
			},
			want: func(t *testing.T, jd *JobDispatcher) {
				if jd != nil {
					t.Errorf("New() = %v, want %v", jd, nil)
				}
			},
			wantErr: true,
		},
		{
			name: "Invalid auth_type fails",
			args: args{
				ctx: context.TODO(),
				pluginData: map[string]string{
					"endpoint":          "localhost",
					"auth_type":         "service-account",
					"image":             "hello-world",
					"memory_request":    "256Mi",
					"memory_limit":      "512Mi",
					"volume_size_limit": "5Mib",
				},
				logger: nil,
			},
			want: func(t *testing.T, jd *JobDispatcher) {
				if jd != nil {
					t.Errorf("New() = %v, want %v", jd, nil)
				}
			},
			wantErr: true,
		},
		{
			name: "Missing Require Plugin Data for eks_iam auth_type fails",
			args: args{
				ctx: context.TODO(),
				pluginData: map[string]string{
					"endpoint":          "localhost",
					"auth_type":         "eks_iam",
					"image":             "hello-world",
					"memory_request":    "256Mi",
					"memory_limit":      "512Mi",
					"volume_size_limit": "5Mib",
				},
				logger: nil,
			},
			want: func(t *testing.T, jd *JobDispatcher) {
				if jd != nil {
					t.Errorf("New() = %v, want %v", jd, nil)
				}
			},
			wantErr: true,
		},
		// // This can't be tested currently as there isn't a clear way
		// // to inject the eks configurer client
		// {
		// 	name: "Invalid value for requested memory fails",
		// 	args: args{
		// 		ctx: context.TODO(),
		// 		pluginData: map[string]string{
		// 			"endpoint":        "http://localhost",
		// 			"auth_type":      "eks_iam",
		// 			"image":          "hello-world",
		// 			"memory_request": "Two Hundred and Fifty-Six mebibytes",
		// 			"memory_limit":   "512Mi",
		// 		},
		// 		logger: nil,
		// 	},
		// 	want: func(t *testing.T, jd *JobDispatcher) {
		// 		if jd != nil {
		// 			t.Errorf("New() = %v, want %v", jd, nil)
		// 		}
		// 	},
		// 	wantErr: true,
		// },
		// {
		// 	name: "Invalid value for memory limit fails",
		// 	args: args{
		// 		ctx: context.TODO(),
		// 		pluginData: map[string]string{
		// 			"endpoint":        "http://localhost",
		// 			"auth_type":      "eks_iam",
		// 			"image":          "hello-world",
		// 			"memory_request": "256Mi",
		// 			"memory_limit":   "Five Hundred and Twelve mebibytes",
		// 		},
		// 		logger: nil,
		// 	},
		// 	want: func(t *testing.T, jd *JobDispatcher) {
		// 		if jd != nil {
		// 			t.Errorf("New() = %v, want %v", jd, nil)
		// 		}
		// 	},
		// 	wantErr: true,
		// },
		// {
		// 	name: "Default namespace is returned when no provided",
		// 	args: args{
		// 		ctx: context.TODO(),
		// 		pluginData: map[string]string{
		// 			"endpoint":        "http://localhost",
		// 			"auth_type":      "eks_iam",
		// 			"region":         "us-east-2",
		// 			"eks_cluster":    "test",
		// 			"image":          "hello-world",
		// 			"memory_request": "256Mi",
		// 			"memory_limit":   "512Mi",
		// 		},
		// 		logger: nil,
		// 	},
		// 	want: func(t *testing.T, jd *JobDispatcher) {
		// 		want := "default"

		// 		runner, ok := jd.client.(*k8sRunner)
		// 		if !ok {
		// 			t.Errorf("client returned wasn't a k8sRunner")
		// 		}
		// 		if runner.namespace != want {
		// 			t.Errorf("New() = %v, want %v", runner.namespace, want)
		// 		}
		// 	},
		// 	wantErr: false,
		// },
		// {
		// 	name: "Provided namespace doesn't default",
		// 	args: args{
		// 		ctx: context.TODO(),
		// 		pluginData: map[string]string{
		// 			"endpoint":        "http://localhost",
		// 			"auth_type":      "eks_iam",
		// 			"region":         "us-east-2",
		// 			"eks_cluster":    "test",
		// 			"image":          "hello-world",
		// 			"namespace":      "runner-ns",
		// 			"memory_request": "256Mi",
		// 			"memory_limit":   "512Mi",
		// 		},
		// 		logger: nil,
		// 	},
		// 	want: func(t *testing.T, jd *JobDispatcher) {
		// 		want := "runner-ns"

		// 		runner, ok := jd.client.(*k8sRunner)
		// 		if !ok {
		// 			t.Errorf("client returned wasn't a k8sRunner")
		// 		}
		// 		if runner.namespace != want {
		// 			t.Errorf("New() = %v, want %v", runner.namespace, want)
		// 		}
		// 	},
		// 	wantErr: false,
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.ctx, tt.args.pluginData, tt.args.logger)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			tt.want(t, got)
		})
	}
}

func TestJobDispatcher_DispatchJob(t *testing.T) {
	type args struct {
		ctx   context.Context
		job   *models.Job
		token string
	}
	tests := []struct {
		name    string
		j       *JobDispatcher
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "failed to create job",
			j: &JobDispatcher{
				logger:   nil,
				image:    "hello-world",
				endpoint: "http://localhost",
				client: func() client {
					client := &mockClient{}

					client.On("CreateJob", mock.Anything, mock.Anything).Return(nil, fmt.Errorf("failed to launch job")).Once()
					return client
				}(),
			},
			args: args{
				ctx: context.TODO(),
				job: &models.Job{
					Metadata: models.ResourceMetadata{
						ID: "test-job-123",
					},
				},
				token: "myToken",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "create job succeeds",
			j: &JobDispatcher{
				logger:   nil,
				image:    "hello-world",
				endpoint: "http://localhost",
				client: func() client {
					client := &mockClient{}

					client.On("CreateJob", mock.Anything, mock.Anything).Return(&v1.Job{
						ObjectMeta: metav1.ObjectMeta{
							UID: "id",
						},
					}, nil).Once()
					return client
				}(),
			},
			args: args{
				ctx: context.TODO(),
				job: &models.Job{
					Metadata: models.ResourceMetadata{
						ID: "test-job-123",
					},
				},
				token: "myToken",
			},
			want:    "id",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.j.DispatchJob(tt.args.ctx, tt.args.job.Metadata.ID, tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("JobDispatcher.DispatchJob() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("JobDispatcher.DispatchJob() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDispatchJob_customImage(t *testing.T) {
	jobID := "test-job-123"
	jobToken := "job-token"
	expectJobUID := "kube-job-uid"
	defaultImage := "default-image"
	customImage := "custom-image"
	phobosEndpoint := "http://localhost"
	volumeSizeLimit := 5
	memoryLimit := resource.MustParse("512Mi")
	memoryRequest := resource.MustParse("256Mi")

	t.Run("success with custom image", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		mockClient := newMockClient(t)

		kubeJob := &v1.Job{
			TypeMeta: metav1.TypeMeta{
				APIVersion: "batch/v1",
				Kind:       "Job",
			},
			ObjectMeta: metav1.ObjectMeta{
				GenerateName: "phobos-job-test-job",
			},
			Spec: v1.JobSpec{
				Template: corev1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"cluster-autoscaler.kubernetes.io/safe-to-evict": "false",
						},
						Annotations: map[string]string{
							"job.phobos.io/id": jobID,
						},
					},
					Spec: corev1.PodSpec{
						AutomountServiceAccountToken: ptr.Bool(false),
						InitContainers: []corev1.Container{
							{
								Name:            "init",
								Image:           defaultImage,
								SecurityContext: &corev1.SecurityContext{},
								VolumeMounts: []corev1.VolumeMount{
									{
										Name:      "data",
										MountPath: "/phobos-data",
									},
								},
								WorkingDir: "/app",
								Command:    []string{"./prepare_for_custom_container.sh"},
							},
						},
						Volumes: []corev1.Volume{
							{
								Name:         "data",
								VolumeSource: corev1.VolumeSource{EmptyDir: &corev1.EmptyDirVolumeSource{}},
							},
						},
						Containers: []corev1.Container{
							{
								Name:            "main",
								Image:           customImage,
								SecurityContext: &corev1.SecurityContext{},
								Args:            []string{"./job"},
								Command:         []string{"/phobos-data/custom_container_entrypoint.sh"},
								VolumeMounts: []corev1.VolumeMount{
									{
										Name:      "data",
										MountPath: "/phobos-data",
									},
								},
								Env: []corev1.EnvVar{
									{
										Name:  "JOB_ID",
										Value: jobID,
									},
									{
										Name:  "JOB_TOKEN",
										Value: jobToken,
									},
									{
										Name:  "ENDPOINT",
										Value: phobosEndpoint,
									},
									{
										Name:  "MEMORY_LIMIT",
										Value: memoryLimit.String(),
									},
									{
										Name:  "VOLUME_SIZE_LIMIT",
										Value: strconv.Itoa(volumeSizeLimit),
									},
								},
								Resources: corev1.ResourceRequirements{
									Requests: corev1.ResourceList{
										corev1.ResourceMemory: memoryRequest,
									},
									Limits: corev1.ResourceList{
										corev1.ResourceMemory: memoryLimit,
									},
								},
							},
						},
						RestartPolicy: corev1.RestartPolicyNever,
					},
				},
				// Disable retries
				BackoffLimit: ptr.Int32(0),
				// Remove once completed
				TTLSecondsAfterFinished: ptr.Int32(0),
			},
		}

		mockClient.On("CreateJob", mock.Anything, kubeJob).Return(
			func(_ context.Context, job *v1.Job) (*v1.Job, error) {
				job.UID = kubetypes.UID(expectJobUID)
				return job, nil
			},
		)

		logger, _ := logger.NewForTest()

		jd := &JobDispatcher{
			logger:          logger,
			client:          mockClient,
			image:           defaultImage,
			endpoint:        phobosEndpoint,
			memoryRequest:   memoryRequest,
			memoryLimit:     memoryLimit,
			volumeSizeLimit: volumeSizeLimit,
			allowedImages:   []string{"*"},
			securityContext: &corev1.SecurityContext{},
		}

		actualJobUID, err := jd.DispatchJob(ctx, jobID, jobToken, types.WithImage(customImage))

		require.NoError(t, err)
		require.Equal(t, expectJobUID, actualJobUID)
	})
}
