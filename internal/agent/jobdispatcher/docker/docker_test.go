package docker

import (
	"context"
	"fmt"
	"io"
	"strings"
	"testing"
	"time"

	"github.com/docker/docker/api/types/container"
	dockercontainer "github.com/docker/docker/api/types/container"
	dockerimage "github.com/docker/docker/api/types/image"
	"github.com/docker/docker/api/types/strslice"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNew(t *testing.T) {
	pluginData := map[string]string{
		"endpoint":          "testUrl",
		"host":              "http://localhost",
		"image":             "testImage",
		"volume_size_limit": "5Mib",
	}
	dispatcher, err := New(pluginData, logger.New())
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	assert.Equal(t, "testUrl", dispatcher.endpoint)
	assert.Equal(t, "testImage", dispatcher.image)
	assert.False(t, dispatcher.localImage)
	assert.NotNil(t, dispatcher.client)
	assert.Equal(t, 5242880, dispatcher.volumeSizeLimit)
}

func TestDispatchJob(t *testing.T) {
	// Test cases
	tests := []struct {
		containerCreateRetErr error
		containerStartRetErr  error
		name                  string
		jobID                 string
		bindPath              string
		username              string
		password              string
		expectTaskID          string
		expectErrorMsg        string
		expectAuthStr         string
		retOutput             dockercontainer.CreateResponse
		localImage            bool
	}{
		{
			name:       "local image with bind path",
			jobID:      "job1",
			localImage: true,
			bindPath:   "/test",
			retOutput: dockercontainer.CreateResponse{
				ID: "123",
			},
			expectTaskID: "123",
		},
		{
			name:       "remote image no auth",
			jobID:      "job1",
			localImage: false,
			retOutput: dockercontainer.CreateResponse{
				ID: "123",
			},
			expectTaskID: "123",
		},
		{
			name:       "remote image with auth",
			jobID:      "job1",
			localImage: false,
			username:   "admin",
			password:   "secret",
			retOutput: dockercontainer.CreateResponse{
				ID: "123",
			},
			expectTaskID:  "123",
			expectAuthStr: "eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJzZWNyZXQifQ==",
		},
		{
			name:                  "container create error",
			jobID:                 "job1",
			containerCreateRetErr: fmt.Errorf("couldn't build container"),
			expectErrorMsg:        "failed to create container: couldn't build container",
		},
		{
			name:  "container start error",
			jobID: "job1",
			retOutput: dockercontainer.CreateResponse{
				ID: "123",
			},
			containerStartRetErr: fmt.Errorf("couldn't start container"),
			expectErrorMsg:       "failed to start container: couldn't start container",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			endpoint := "https://test"
			token := "token1"
			image := "testimage"
			memoryLimit := uint64(0)
			volumeSizeLimit := 5242880

			client := mockClient{}
			client.Test(t)

			if !test.localImage {
				client.On("ImagePull", ctx, image, dockerimage.PullOptions{
					RegistryAuth: test.expectAuthStr,
				}).Return(io.NopCloser(strings.NewReader("")), nil)
			}

			hostConfig := &dockercontainer.HostConfig{}

			if test.bindPath != "" {
				hostConfig.Binds = []string{test.bindPath}
			}

			client.On("ContainerCreate", ctx, &dockercontainer.Config{
				Image: image,
				Env: []string{
					fmt.Sprintf("ENDPOINT=%s", endpoint),
					fmt.Sprintf("JOB_ID=%s", test.jobID),
					fmt.Sprintf("JOB_TOKEN=%s", token),
					fmt.Sprintf("MEMORY_LIMIT=%d", memoryLimit),
					fmt.Sprintf("VOLUME_SIZE_LIMIT=%d", volumeSizeLimit),
				},
			}, hostConfig, mock.Anything, mock.Anything, "").Return(test.retOutput, test.containerCreateRetErr)

			client.On("ContainerStart", ctx, test.retOutput.ID, dockercontainer.StartOptions{}).Return(test.containerStartRetErr)

			dispatcher := JobDispatcher{
				logger:           logger.New(),
				image:            image,
				bindPath:         test.bindPath,
				localImage:       test.localImage,
				registryUsername: test.username,
				registryPassword: test.password,
				endpoint:         endpoint,
				client:           &client,
				volumeSizeLimit:  volumeSizeLimit,
			}

			taskID, err := dispatcher.DispatchJob(ctx, test.jobID, token)
			if test.expectErrorMsg != "" {
				assert.EqualError(t, err, test.expectErrorMsg)
			} else {
				assert.Nil(t, err, "Unexpected error occurred %v", err)
			}

			assert.Equal(t, test.expectTaskID, taskID)
		})
	}
}

func TestDispatchJob_customImage(t *testing.T) {
	registryUsername := "admin"
	registryPassword := "secret"
	defaultImage := "phobos/job"
	endpoint := "https://test"
	volumeSizeLimit := 5242880
	jobID := "job1"
	token := "job-token"
	customImageName := "custom/image"
	dataContainerID := "123"
	mainContainerID := "456"
	encodedAuthString := "eyJ1c2VybmFtZSI6ImFkbWluIiwicGFzc3dvcmQiOiJzZWNyZXQifQ=="

	t.Run("success", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		mockClient := newMockClient(t)

		// Mocks for data container
		mockClient.On("ImagePull", mock.Anything, defaultImage, dockerimage.PullOptions{RegistryAuth: encodedAuthString}).Return(io.NopCloser(strings.NewReader("")), nil).Once()

		mockClient.On("ContainerCreate",
			mock.Anything,
			&container.Config{
				Image:      defaultImage,
				WorkingDir: "/app",
				Volumes: map[string]struct{}{
					"/phobos-data": {},
				},
				Entrypoint: strslice.StrSlice{"./prepare_for_custom_container.sh"},
			},
			&container.HostConfig{},
			mock.Anything,
			mock.Anything,
			"",
		).Return(container.CreateResponse{ID: dataContainerID}, nil).Once()

		mockClient.On("ContainerStart", mock.Anything, dataContainerID, container.StartOptions{}).Return(nil).Once()

		mockClient.On("ContainerRemove", mock.Anything, dataContainerID, container.RemoveOptions{Force: true}).Return(nil).Once()

		mockClient.On("ContainerWait", mock.Anything, dataContainerID, container.WaitConditionNotRunning).Return(
			func(_ context.Context, _ string, _ container.WaitCondition) (<-chan container.WaitResponse, <-chan error) {
				waitResponse := make(chan container.WaitResponse, 1)
				waitError := make(chan error, 1)
				waitResponse <- container.WaitResponse{StatusCode: 0}
				close(waitResponse)
				close(waitError)
				return waitResponse, waitError
			},
		).Once()

		// Mocks for main container
		mockClient.On("ImagePull", mock.Anything, customImageName, dockerimage.PullOptions{}).Return(io.NopCloser(strings.NewReader("")), nil).Once()

		mockClient.On("ContainerCreate",
			mock.Anything,
			&container.Config{
				Image: customImageName,
				Env: []string{
					"ENDPOINT=" + endpoint,
					"JOB_ID=" + jobID,
					"JOB_TOKEN=" + token,
					"MEMORY_LIMIT=0",
					"VOLUME_SIZE_LIMIT=" + fmt.Sprintf("%d", volumeSizeLimit),
				},
				Cmd:        strslice.StrSlice{"./job"},
				Entrypoint: strslice.StrSlice{"/phobos-data/custom_container_entrypoint.sh"},
			},
			&container.HostConfig{
				VolumesFrom: []string{dataContainerID},
			},
			mock.Anything,
			mock.Anything,
			"",
		).Return(container.CreateResponse{ID: mainContainerID}, nil).Once()

		mockClient.On("ContainerStart", mock.Anything, mainContainerID, container.StartOptions{}).Return(nil).Once()

		logger, _ := logger.NewForTest()

		dispatcher := JobDispatcher{
			logger:           logger,
			image:            defaultImage,
			client:           mockClient,
			registryUsername: registryUsername,
			registryPassword: registryPassword,
			volumeSizeLimit:  volumeSizeLimit,
			endpoint:         endpoint,
		}

		actualTaskID, err := dispatcher.DispatchJob(ctx, jobID, token, types.WithImage(customImageName))

		require.NoError(t, err)
		assert.Equal(t, mainContainerID, actualTaskID)
	})
}

func Test_waitForContainer(t *testing.T) {
	containerID := "123"

	type testCase struct {
		name          string
		waitResponse  container.WaitResponse
		waitError     error
		expectedError string
	}

	tests := []testCase{
		{
			name: "success",
			waitResponse: container.WaitResponse{
				StatusCode: 0,
			},
		},
		{
			name:          "error",
			waitError:     fmt.Errorf("container failed"),
			expectedError: "failed to wait for container: container failed",
		},
		{
			name: "non-zero exit code",
			waitResponse: container.WaitResponse{
				StatusCode: 1,
			},
			expectedError: "container exited with status code: 1",
		},
		{
			name: "wait response with error",
			waitResponse: container.WaitResponse{
				StatusCode: 1,
				Error: &dockercontainer.WaitExitError{
					Message: "container failed",
				},
			},
			expectedError: "container exited with error: container failed",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			client := newMockClient(t)

			client.On("ContainerWait", ctx, containerID, container.WaitConditionNotRunning).Return(
				func(_ context.Context, _ string, _ container.WaitCondition) (<-chan container.WaitResponse, <-chan error) {
					waitResponse := make(chan container.WaitResponse)
					waitError := make(chan error)
					go func() {
						// Add a delay to simulate the wait.
						time.Sleep(100 * time.Millisecond)
						defer close(waitResponse)
						defer close(waitError)

						if test.waitError != nil {
							waitError <- test.waitError
							return
						}

						waitResponse <- test.waitResponse
					}()

					return waitResponse, waitError
				},
			).Once()

			logger, _ := logger.NewForTest()

			dispatcher := JobDispatcher{
				logger: logger,
				client: client,
			}

			err := dispatcher.waitForContainer(ctx, containerID)

			if test.expectedError != "" {
				assert.EqualError(t, err, test.expectedError)
			} else {
				assert.Nil(t, err)
			}
		})
	}
}
