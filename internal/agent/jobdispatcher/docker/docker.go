// Package docker package
package docker

//go:generate go tool mockery --name client --inpackage --case underscore

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/image"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/registry"
	"github.com/docker/docker/api/types/strslice"
	dockerclient "github.com/docker/docker/client"
	"github.com/dustin/go-humanize"
	specs "github.com/opencontainers/image-spec/specs-go/v1"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const (
	// containerWaitTimeout is the maximum time to wait for an init container to finish.
	containerWaitTimeout = 30 * time.Second
)

var pluginDataRequiredFields = []string{"host", "image", "endpoint"}

type client interface {
	ImagePull(
		ctx context.Context,
		refStr string,
		options image.PullOptions,
	) (io.ReadCloser, error)
	ContainerCreate(
		ctx context.Context,
		config *container.Config,
		hostConfig *container.HostConfig,
		networkingConfig *network.NetworkingConfig,
		platform *specs.Platform,
		containerName string,
	) (container.CreateResponse, error)
	ContainerStart(
		ctx context.Context,
		containerID string,
		options container.StartOptions,
	) error
	ContainerRemove(
		ctx context.Context,
		containerID string,
		options container.RemoveOptions,
	) error
	ContainerWait(
		ctx context.Context,
		containerID string,
		condition container.WaitCondition,
	) (<-chan container.WaitResponse, <-chan error)
}

// JobDispatcher uses the local docker api to dispatch jobs
type JobDispatcher struct {
	logger           logger.Logger
	client           client
	image            string
	bindPath         string
	registryUsername string
	registryPassword string
	endpoint         string
	extraHosts       []string
	localImage       bool
	memoryLimit      int64 // in bytes, zero means unlimited
	volumeSizeLimit  int
}

// New creates a JobDispatcher
func New(pluginData map[string]string, logger logger.Logger) (*JobDispatcher, error) {
	for _, field := range pluginDataRequiredFields {
		if _, ok := pluginData[field]; !ok {
			return nil, fmt.Errorf("docker job dispatcher requires plugin data '%s' field", field)
		}
	}

	var localImage bool
	if _, ok := pluginData["local_image"]; ok {
		var err error
		localImage, err = strconv.ParseBool(pluginData["local_image"])
		if err != nil {
			return nil, fmt.Errorf("failed to parse job dispatcher 'local_image' config: %v", err)
		}
	}

	extraHosts := []string{}
	if _, ok := pluginData["extra_hosts"]; ok {
		extraHosts = append(extraHosts, strings.Split(pluginData["extra_hosts"], ",")...)
	}

	var memoryLimit int64
	if mLimit, ok := pluginData["memory_limit"]; ok {
		tmp, mErr := humanize.ParseBytes(mLimit)
		memoryLimit = int64(tmp)
		if mErr != nil {
			return nil, fmt.Errorf("failed to parse job dispatcher 'memory_limit' config: %w", mErr)
		}
		if memoryLimit < 0 {
			return nil, fmt.Errorf("invalid value for 'memory_limit' config: %s", mLimit)
		}
	}

	var volumeSizeLimit int
	if vLimit, ok := pluginData["volume_size_limit"]; ok {
		tmp, mErr := humanize.ParseBytes(vLimit)
		if mErr != nil {
			return nil, fmt.Errorf("failed to parse job dispatcher 'volume_size_limit' config: %w", mErr)
		}
		if tmp <= 0 {
			return nil, fmt.Errorf("invalid value for 'volume_size_limit' config: %s", vLimit)
		}
		volumeSizeLimit = int(tmp)
	}

	client, err := dockerclient.NewClientWithOpts(dockerclient.WithHost(pluginData["host"]), dockerclient.WithAPIVersionNegotiation())
	if err != nil {
		return nil, fmt.Errorf("job dispatcher failed to initialize docker cli: %v", err)
	}

	return &JobDispatcher{
		image:            pluginData["image"],
		bindPath:         pluginData["bind_path"],
		endpoint:         pluginData["endpoint"],
		registryUsername: pluginData["registry_username"],
		registryPassword: pluginData["registry_password"],
		extraHosts:       extraHosts,
		localImage:       localImage,
		client:           client,
		logger:           logger,
		memoryLimit:      memoryLimit,
		volumeSizeLimit:  volumeSizeLimit,
	}, nil
}

// DispatchJob will start a docker container to execute the job
func (j *JobDispatcher) DispatchJob(ctx context.Context, jobID string, token string, opts ...types.DispatchJobOption) (string, error) {
	customOpts := types.GetDispatchJobOptions(opts...)

	hostConfig := &container.HostConfig{}

	if len(j.extraHosts) > 0 {
		hostConfig.ExtraHosts = j.extraHosts
	}

	if j.bindPath != "" {
		hostConfig.Binds = []string{j.bindPath}
	}

	if j.memoryLimit != 0 {
		hostConfig.Memory = j.memoryLimit
		hostConfig.MemorySwap = j.memoryLimit
	}

	containerEnvs := []string{
		fmt.Sprintf("ENDPOINT=%s", j.endpoint),
		fmt.Sprintf("JOB_ID=%s", jobID),
		fmt.Sprintf("JOB_TOKEN=%s", token),
		fmt.Sprintf("MEMORY_LIMIT=%d", j.memoryLimit),
		fmt.Sprintf("VOLUME_SIZE_LIMIT=%d", j.volumeSizeLimit),
	}

	if customOpts.Image() != nil {
		customImageName := *customOpts.Image()

		j.logger.Infof("Using docker job dispatcher with custom image '%s' for job %s", customImageName, jobID)

		// Start the data container which will provide the job binary, certs, and other data.
		dataContainerID, err := j.initContainer(
			ctx,
			nil,
			&container.Config{
				Image:      j.image,
				WorkingDir: "/app",
				Volumes: map[string]struct{}{
					"/phobos-data": {},
				},
				Entrypoint: strslice.StrSlice{"./prepare_for_custom_container.sh"},
			},
			hostConfig,
		)
		if err != nil {
			return "", fmt.Errorf("failed to start data container: %w", err)
		}

		defer func() {
			if jErr := j.client.ContainerRemove(ctx, dataContainerID, container.RemoveOptions{Force: true}); jErr != nil {
				j.logger.Errorf("failed to remove data container: %v", jErr)
			}
		}()

		// Must wait for the data container to finish before copying the data.
		if err = j.waitForContainer(ctx, dataContainerID); err != nil {
			return "", fmt.Errorf("failed to wait for data container: %w", err)
		}

		// Add the data container as a volume source for the custom container.
		hostConfig.VolumesFrom = append(hostConfig.VolumesFrom, dataContainerID)

		// Start the custom container with the job binary, certs, and other data.
		return j.initContainer(
			ctx,
			customOpts,
			&container.Config{
				Image:      customImageName,
				Env:        containerEnvs,
				Cmd:        strslice.StrSlice{"./job"},
				Entrypoint: strslice.StrSlice{"/phobos-data/custom_container_entrypoint.sh"},
			},
			hostConfig,
		)
	}

	// Fall back to the default image.
	return j.initContainer(ctx, nil, &container.Config{Image: j.image, Env: containerEnvs}, hostConfig)
}

// initContainer creates a container with the given configuration and starts it.
func (j *JobDispatcher) initContainer(
	ctx context.Context,
	customOpts *types.DispatchJobOptions,
	cfg *container.Config,
	hostCfg *container.HostConfig,
) (string, error) {
	if !j.localImage || customOpts != nil && customOpts.Image() != nil {
		authStr, err := j.getRegistryAuth(customOpts)
		if err != nil {
			return "", fmt.Errorf("failed to get registry auth: %w", err)
		}

		out, err := j.client.ImagePull(ctx, cfg.Image, image.PullOptions{
			RegistryAuth: authStr,
		})
		if err != nil {
			return "", fmt.Errorf("failed to pull image: %w", err)
		}

		_, _ = io.Copy(os.Stdout, out)
	}

	resp, err := j.client.ContainerCreate(ctx, cfg, hostCfg, nil, nil, "")
	if err != nil {
		return "", fmt.Errorf("failed to create container: %w", err)
	}

	if err := j.client.ContainerStart(ctx, resp.ID, container.StartOptions{}); err != nil {
		return "", fmt.Errorf("failed to start container: %w", err)
	}

	return resp.ID, nil
}

// waitForContainer waits for the container to finish and returns an error if it failed.
func (j *JobDispatcher) waitForContainer(ctx context.Context, containerID string) error {
	waitRespChan, errorChan := j.client.ContainerWait(ctx, containerID, container.WaitConditionNotRunning)

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-time.After(containerWaitTimeout):
		return fmt.Errorf("timed out waiting for container to finish")
	case err := <-errorChan:
		if err != nil {
			return fmt.Errorf("failed to wait for container: %w", err)
		}
	case resp := <-waitRespChan:
		if resp.Error != nil {
			return fmt.Errorf("container exited with error: %s", resp.Error.Message)
		}

		if resp.StatusCode != 0 {
			return fmt.Errorf("container exited with status code: %d", resp.StatusCode)
		}
	}

	return nil
}

// getRegistryAuth returns the base64 encoded registry auth string for the
// given custom options or the default image.
func (j *JobDispatcher) getRegistryAuth(customOpts *types.DispatchJobOptions) (string, error) {
	if customOpts != nil && customOpts.Image() != nil {
		// Authentication for custom docker registries is not yet supported.
		return "", nil
	}

	if j.registryUsername != "" && j.registryPassword != "" {
		authConfig := registry.AuthConfig{
			Username: j.registryUsername,
			Password: j.registryPassword,
		}

		encodedAuth, err := json.Marshal(authConfig)
		if err != nil {
			return "", fmt.Errorf("error when encoding registry authConfig: %v", err)
		}

		return base64.URLEncoding.EncodeToString(encodedAuth), nil
	}
	return "", nil
}
