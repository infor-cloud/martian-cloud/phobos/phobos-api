// Package ecs package
package ecs

//go:generate go tool mockery --name client --inpackage --case underscore

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ecs"
	ecstypes "github.com/aws/aws-sdk-go-v2/service/ecs/types"
	"github.com/aws/smithy-go/ptr"
	"github.com/dustin/go-humanize"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

var pluginDataRequiredFields = []string{"endpoint", "region", "task_definition", "cluster", "subnets", "launch_type"}

type client interface {
	RunTask(ctx context.Context, params *ecs.RunTaskInput, optFns ...func(*ecs.Options)) (*ecs.RunTaskOutput, error)
}

// JobDispatcher uses the AWS ECS client to dispatch jobs
type JobDispatcher struct {
	logger          logger.Logger
	client          client
	taskDefinition  string
	cluster         string
	launchType      ecstypes.LaunchType
	endpoint        string
	subnets         []string
	volumeSizeLimit int
}

// New creates a JobDispatcher
func New(ctx context.Context, pluginData map[string]string, logger logger.Logger) (*JobDispatcher, error) {
	for _, field := range pluginDataRequiredFields {
		if _, ok := pluginData[field]; !ok {
			return nil, fmt.Errorf("ECS job dispatcher requires plugin data '%s' field", field)
		}
	}

	awsCfg, err := config.LoadDefaultConfig(ctx, config.WithRegion(pluginData["region"]))
	if err != nil {
		return nil, err
	}

	var launchType ecstypes.LaunchType
	switch pluginData["launch_type"] {
	case "ec2":
		launchType = ecstypes.LaunchTypeEc2
	case "fargate":
		launchType = ecstypes.LaunchTypeFargate
	default:
		return nil, fmt.Errorf("ECS job dispatcher requires a launch type of ec2 or fargate")
	}

	var volumeSizeLimit int
	if vLimit, ok := pluginData["volume_size_limit"]; ok {
		tmp, mErr := humanize.ParseBytes(vLimit)
		if mErr != nil {
			return nil, fmt.Errorf("failed to parse job dispatcher 'volume_size_limit' config: %w", mErr)
		}
		if tmp <= 0 {
			return nil, fmt.Errorf("invalid value for 'volume_size_limit' config: %s", vLimit)
		}
		volumeSizeLimit = int(tmp)
	}

	client := ecs.NewFromConfig(awsCfg)

	return &JobDispatcher{
		logger:          logger,
		taskDefinition:  pluginData["task_definition"],
		cluster:         pluginData["cluster"],
		launchType:      launchType,
		subnets:         strings.Split(pluginData["subnets"], ","),
		endpoint:        pluginData["endpoint"],
		client:          client,
		volumeSizeLimit: volumeSizeLimit,
	}, nil
}

// DispatchJob will start an ECS task to execute the job
func (j *JobDispatcher) DispatchJob(ctx context.Context, jobID string, token string, opts ...types.DispatchJobOption) (string, error) {
	customOpts := types.GetDispatchJobOptions(opts...)

	if customOpts.Image() != nil {
		return "", fmt.Errorf("custom images are not supported by the ECS job dispatcher")
	}

	input := ecs.RunTaskInput{
		TaskDefinition: &j.taskDefinition,
		LaunchType:     j.launchType,
		Cluster:        &j.cluster,
		NetworkConfiguration: &ecstypes.NetworkConfiguration{
			AwsvpcConfiguration: &ecstypes.AwsVpcConfiguration{
				AssignPublicIp: ecstypes.AssignPublicIpDisabled,
				Subnets:        j.subnets,
			},
		},
		Overrides: &ecstypes.TaskOverride{
			ContainerOverrides: []ecstypes.ContainerOverride{
				{
					Name: ptr.String("main"),
					Environment: []ecstypes.KeyValuePair{
						{Name: ptr.String("JOB_ID"), Value: &jobID},
						{Name: ptr.String("JOB_TOKEN"), Value: &token},
						{Name: ptr.String("ENDPOINT"), Value: &j.endpoint},
						{Name: ptr.String("VOLUME_SIZE_LIMIT"), Value: ptr.String(strconv.Itoa(j.volumeSizeLimit))},
					},
				},
			},
		},
	}
	output, err := j.client.RunTask(ctx, &input)
	if err != nil {
		return "", fmt.Errorf("ECS Job Dispatcher failed to run task for job %s: %v", jobID, err)
	}

	if len(output.Failures) > 0 {
		errors := []string{}
		if output.Failures[0].Reason != nil {
			errors = append(errors, *output.Failures[0].Reason)
		}
		if output.Failures[0].Detail != nil {
			errors = append(errors, *output.Failures[0].Detail)
		}
		return "", fmt.Errorf("failed to run task: %s", strings.Join(errors, "; "))
	}

	if len(output.Tasks) == 0 {
		return "", fmt.Errorf("no ECS tasks were created")
	}

	return *output.Tasks[0].TaskArn, nil
}
