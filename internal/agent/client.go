package agent

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/job"
)

// ClaimJobInput is the input for claiming the next available job
type ClaimJobInput struct {
	AgentID string
}

// ClaimJobResponse is the response when claiming a job
type ClaimJobResponse struct {
	JobID         string
	Token         string
	ImageOverride *string
}

// Client interface for claiming a job
type Client interface {
	CreateSession(ctx context.Context, agentID string) (string, error)
	SendHeartbeat(ctx context.Context, sessionID string) error
	ClaimJob(ctx context.Context, input *ClaimJobInput) (*ClaimJobResponse, error)
	SendError(ctx context.Context, sessionID string, err error) error
}

// internalClient is the client for the internal system agent.
type internalClient struct {
	jobService   job.Service
	agentService agent.Service
}

// NewInternalClient creates a new internal client
func NewInternalClient(agentService agent.Service, jobService job.Service) Client {
	return &internalClient{
		jobService:   jobService,
		agentService: agentService,
	}
}

func (a *internalClient) SendError(ctx context.Context, sessionID string, err error) error {
	return a.agentService.CreateAgentSessionError(ctx, gid.FromGlobalID(sessionID), err.Error())
}

func (a *internalClient) CreateSession(ctx context.Context, agentID string) (string, error) {
	session, err := a.agentService.CreateAgentSession(ctx, &agent.CreateAgentSessionInput{
		AgentID:  gid.FromGlobalID(agentID),
		Internal: true,
	})
	if err != nil {
		return "", err
	}
	return gid.ToGlobalID(gid.AgentSessionType, session.Metadata.ID), nil
}

func (a *internalClient) SendHeartbeat(ctx context.Context, sessionID string) error {
	return a.agentService.AcceptAgentSessionHeartbeat(ctx, gid.FromGlobalID(sessionID))
}

func (a *internalClient) ClaimJob(ctx context.Context, input *ClaimJobInput) (*ClaimJobResponse, error) {
	agentID := gid.FromGlobalID(input.AgentID)

	resp, err := a.jobService.ClaimJob(ctx, agentID)
	if err != nil {
		return nil, err
	}

	return &ClaimJobResponse{
		JobID:         gid.ToGlobalID(gid.JobType, resp.Job.Metadata.ID),
		Token:         resp.Token,
		ImageOverride: resp.Job.Image,
	}, nil
}
