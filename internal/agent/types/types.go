// Package types provides additional types used by different job dispatchers
package types

// DispatchJobOptions contains the options for dispatching a job
type DispatchJobOptions struct {
	image *string
}

// DispatchJobOption is a function that sets the options for dispatching a job
type DispatchJobOption func(*DispatchJobOptions)

// Image returns the image option for dispatching a job
func (o *DispatchJobOptions) Image() *string {
	return o.image
}

// WithImage sets the image option for dispatching a job
func WithImage(image string) DispatchJobOption {
	return func(o *DispatchJobOptions) {
		o.image = &image
	}
}

// GetDispatchJobOptions returns the options for dispatching a job
func GetDispatchJobOptions(opts ...DispatchJobOption) *DispatchJobOptions {
	options := &DispatchJobOptions{}
	for _, opt := range opts {
		opt(options)
	}
	return options
}
