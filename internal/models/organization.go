package models

// Organization represents a Phobos organization which users can belong to
type Organization struct {
	Name        string
	Description string
	CreatedBy   string
	Metadata    ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (o *Organization) ResolveMetadata(key string) (string, error) {
	return o.Metadata.resolveFieldValue(key)
}

// Validate returns an error if the model is not valid
func (o *Organization) Validate() error {
	// Verify name satisfies constraints
	if err := verifyValidName(o.Name); err != nil {
		return err
	}

	return verifyValidDescription(o.Description)
}
