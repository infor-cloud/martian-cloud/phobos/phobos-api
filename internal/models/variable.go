package models

import (
	"fmt"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// maxVariableValueLength is the maximum length for a variable's value field.
const maxVariableValueLength int = 5000

// ProjectVariableEnvironmentScopeAll is the environment scope for all environments
const ProjectVariableEnvironmentScopeAll = "*"

// VariableCategory is the type of variable category
type VariableCategory string

// VariableCategory constants
const (
	HCLVariable         VariableCategory = "HCL"
	EnvironmentVariable VariableCategory = "ENVIRONMENT"
)

// ProjectVariableSet represents a Phobos project variable set
type ProjectVariableSet struct {
	ProjectID string
	Revision  string
	Latest    bool
	Metadata  ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (pvs *ProjectVariableSet) ResolveMetadata(key string) (string, error) {
	return pvs.Metadata.resolveFieldValue(key)
}

// ProjectVariable represents a Phobos project variable
type ProjectVariable struct {
	Key              string
	Value            string
	EnvironmentScope string
	PipelineType     PipelineType
	CreatedBy        string
	ProjectID        string
	Metadata         ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (pv *ProjectVariable) ResolveMetadata(key string) (string, error) {
	val, err := pv.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "key":
			val = pv.Key
		default:
			return "", err
		}
	}

	return val, nil
}

// FQN returns a fully qualified name for the project variable
func (pv *ProjectVariable) FQN() string {
	return fmt.Sprintf("%s:%s:%s", string(pv.PipelineType), pv.EnvironmentScope, pv.Key)
}

// Validate returns an error if the model is not valid
func (pv *ProjectVariable) Validate() error {
	if len(pv.Key) == 0 {
		return errors.New("variable key cannot be empty", errors.WithErrorCode(errors.EInvalid))
	}

	if !config.NameRegex.MatchString(pv.Key) {
		return errors.New("invalid variable key, can only include lowercase letters and numbers with - and _ supported "+
			"in non leading or trailing positions. Max length is 64 characters", errors.WithErrorCode(errors.EInvalid))
	}

	if len(pv.Value) > maxVariableValueLength {
		return errors.New("variable value exceeds max character length of %d", maxVariableValueLength, errors.WithErrorCode(errors.EInvalid))
	}

	switch pv.PipelineType {
	case RunbookPipelineType:
		if pv.EnvironmentScope != "" {
			return errors.New("runbook variables cannot have an environment scope", errors.WithErrorCode(errors.EInvalid))
		}
	case DeploymentPipelineType:
		if pv.EnvironmentScope == "" {
			return errors.New(
				"deployment variables must have an environment scope set to %q for all environments or set to an environment name",
				ProjectVariableEnvironmentScopeAll,
				errors.WithErrorCode(errors.EInvalid),
			)
		}

		if pv.EnvironmentScope != ProjectVariableEnvironmentScopeAll {
			if err := verifyValidName(pv.EnvironmentScope); err != nil {
				return errors.Wrap(err, "invalid environment scope: %s", pv.EnvironmentScope, errors.WithErrorCode(errors.EInvalid))
			}
		}
	default:
		return errors.New("invalid pipeline type: project variables can only be set for DEPLOYMENT and RUNBOOK pipelines", errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}
