package models

import "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"

// EnvironmentRule represents a rule that can be used to control who can approve a task
type EnvironmentRule struct {
	Metadata          ResourceMetadata
	CreatedBy         string
	Scope             ScopeType
	OrgID             string  // required
	ProjectID         *string // required if project scope; must be nil if org scope
	EnvironmentName   string
	TeamIDs           []string
	UserIDs           []string
	ServiceAccountIDs []string
	RoleIDs           []string
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (e *EnvironmentRule) ResolveMetadata(key string) (string, error) {
	val, err := e.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "environment_name":
			val = e.EnvironmentName
		default:
			return "", err
		}
	}

	return val, nil
}

// Validate returns an error if the model is not valid
func (e *EnvironmentRule) Validate() error {

	// Verify that the environment name is a valid name.
	if err := verifyValidName(e.EnvironmentName); err != nil {
		return err
	}

	// Don't allow a rule with zero users, teams, service accounts, roles.
	if (len(e.UserIDs) + len(e.TeamIDs) + len(e.ServiceAccountIDs) + len(e.RoleIDs)) == 0 {
		return errors.New(
			"not allowed to create an environment protection rule with zero users, teams, service accounts, and roles",
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	return nil
}
