package models

import "strings"

// Project represents a Phobos project.
type Project struct {
	Name        string
	Description string
	CreatedBy   string
	OrgID       string
	Metadata    ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (o *Project) ResolveMetadata(key string) (string, error) {
	return o.Metadata.resolveFieldValue(key)
}

// Validate returns an error if the model is not valid
func (o *Project) Validate() error {
	// Verify name satisfies constraints
	if err := verifyValidName(o.Name); err != nil {
		return err
	}

	return verifyValidDescription(o.Description)
}

// GetOrgName returns the org name for the project
func (o *Project) GetOrgName() string {
	resourcePath := strings.Split(o.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[0]
}
