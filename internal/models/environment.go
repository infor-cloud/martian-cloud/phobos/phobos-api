package models

// Environment is responsible for claiming and executing jobs.
type Environment struct {
	ProjectID   string
	Name        string
	Description string
	CreatedBy   string
	Metadata    ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (e *Environment) ResolveMetadata(key string) (string, error) {
	val, err := e.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "name":
			return e.Name, nil
		default:
			return "", err
		}
	}

	return val, nil
}

// Validate returns an error if the model is not valid
func (e *Environment) Validate() error {
	if err := verifyValidName(e.Name); err != nil {
		return err
	}
	return verifyValidDescription(e.Description)
}
