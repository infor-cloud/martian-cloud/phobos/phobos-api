package models

import (
	"github.com/Masterminds/semver/v3"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// PipelineTemplate represents a Phobos pipeline template.

// PipelineTemplateStatus represents the status of a Phobos pipeline template.
type PipelineTemplateStatus string

// PipelineTemplateStatus values.
const (
	PipelineTemplatePending  PipelineTemplateStatus = "pending"
	PipelineTemplateUploaded PipelineTemplateStatus = "uploaded"
)

// PipelineTemplate represents a Phobos pipeline template.
type PipelineTemplate struct {
	CreatedBy       string
	ProjectID       string
	Status          PipelineTemplateStatus
	Name            *string
	SemanticVersion *string
	Metadata        ResourceMetadata
	Versioned       bool
	Latest          bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (o *PipelineTemplate) ResolveMetadata(key string) (string, error) {
	return o.Metadata.resolveFieldValue(key)
}

// Validate validates the pipeline template.
func (o *PipelineTemplate) Validate() error {
	if o.Name != nil {
		if err := verifyValidName(*o.Name); err != nil {
			return err
		}
	}

	// Versioned determines whether name and semantic version are required or not allowed.
	if o.Versioned {
		// Name and SemanticVersion are required.
		if o.Name == nil {
			return errors.New("a versioned pipeline template must have a name", errors.WithErrorCode(errors.EInvalid))
		}
		if o.SemanticVersion == nil {
			return errors.New("a versioned pipeline template must have a semantic version", errors.WithErrorCode(errors.EInvalid))
		}

		// We now know SemanticVersion isn't nil.
		_, err := semver.StrictNewVersion(*o.SemanticVersion)
		if err != nil {
			return errors.Wrap(err, "pipeline template semantic version is not a valid semantic version string", errors.WithErrorCode(errors.EInvalid))
		}
	} else {
		// Name and SemanticVersion are not allowed.
		if o.Name != nil {
			return errors.New("a non-versioned pipeline template must not have a name", errors.WithErrorCode(errors.EInvalid))
		}
		if o.SemanticVersion != nil {
			return errors.New("a non-versioned pipeline template must not have a semantic version", errors.WithErrorCode(errors.EInvalid))
		}
	}

	return nil
}
