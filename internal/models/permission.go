package models

import (
	"fmt"
	"sort"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// All possible Permissions.
var (
	ViewOrganization        = Permission{ResourceType: OrganizationResource, Action: ViewAction}
	ViewMembership          = Permission{ResourceType: MembershipResource, Action: ViewAction}
	CreateMembership        = Permission{ResourceType: MembershipResource, Action: CreateAction}
	UpdateMembership        = Permission{ResourceType: MembershipResource, Action: UpdateAction}
	DeleteMembership        = Permission{ResourceType: MembershipResource, Action: DeleteAction}
	ViewProject             = Permission{ResourceType: ProjectResource, Action: ViewAction}
	CreateProject           = Permission{ResourceType: ProjectResource, Action: CreateAction}
	UpdateProject           = Permission{ResourceType: ProjectResource, Action: UpdateAction}
	DeleteProject           = Permission{ResourceType: ProjectResource, Action: DeleteAction}
	ViewJob                 = Permission{ResourceType: JobResource, Action: ViewAction}
	ClaimJob                = Permission{ResourceType: JobResource, Action: ClaimAction}
	UpdateJobState          = Permission{ResourceType: JobResource, Action: UpdateAction}
	CreateTeam              = Permission{ResourceType: TeamResource, Action: CreateAction}
	UpdateTeam              = Permission{ResourceType: TeamResource, Action: UpdateAction}
	DeleteTeam              = Permission{ResourceType: TeamResource, Action: DeleteAction}
	CreateUser              = Permission{ResourceType: UserResource, Action: CreateAction}
	UpdateUser              = Permission{ResourceType: UserResource, Action: UpdateAction}
	DeleteUser              = Permission{ResourceType: UserResource, Action: DeleteAction}
	ViewPipelineTemplate    = Permission{ResourceType: PipelineTemplateResource, Action: ViewAction}
	CreatePipelineTemplate  = Permission{ResourceType: PipelineTemplateResource, Action: CreateAction}
	DeletePipelineTemplate  = Permission{ResourceType: PipelineTemplateResource, Action: DeleteAction}
	ViewPipeline            = Permission{ResourceType: PipelineResource, Action: ViewAction}
	ExecutePipeline         = Permission{ResourceType: PipelineResource, Action: ExecuteAction}
	UpdatePipelineState     = Permission{ResourceType: PipelineResource, Action: UpdateAction} // Special non-assignable permission.
	CancelPipeline          = Permission{ResourceType: PipelineResource, Action: CancelAction} // Cancelling a pipeline also allows cancelling jobs.
	ViewAgent               = Permission{ResourceType: AgentResource, Action: ViewAction}
	CreateAgent             = Permission{ResourceType: AgentResource, Action: CreateAction}
	UpdateAgent             = Permission{ResourceType: AgentResource, Action: UpdateAction}
	DeleteAgent             = Permission{ResourceType: AgentResource, Action: DeleteAction}
	CreateAgentSession      = Permission{ResourceType: AgentSessionResource, Action: CreateAction}
	UpdateAgentSession      = Permission{ResourceType: AgentSessionResource, Action: UpdateAction}
	ViewEnvironment         = Permission{ResourceType: EnvironmentResource, Action: ViewAction}
	CreateEnvironment       = Permission{ResourceType: EnvironmentResource, Action: CreateAction}
	UpdateEnvironment       = Permission{ResourceType: EnvironmentResource, Action: UpdateAction}
	DeleteEnvironment       = Permission{ResourceType: EnvironmentResource, Action: DeleteAction}
	ViewServiceAccount      = Permission{ResourceType: ServiceAccountResource, Action: ViewAction}
	CreateServiceAccount    = Permission{ResourceType: ServiceAccountResource, Action: CreateAction}
	UpdateServiceAccount    = Permission{ResourceType: ServiceAccountResource, Action: UpdateAction}
	DeleteServiceAccount    = Permission{ResourceType: ServiceAccountResource, Action: DeleteAction}
	ViewApprovalRule        = Permission{ResourceType: ApprovalRuleResource, Action: ViewAction}
	CreateApprovalRule      = Permission{ResourceType: ApprovalRuleResource, Action: CreateAction}
	UpdateApprovalRule      = Permission{ResourceType: ApprovalRuleResource, Action: UpdateAction}
	DeleteApprovalRule      = Permission{ResourceType: ApprovalRuleResource, Action: DeleteAction}
	ViewLifecycleTemplate   = Permission{ResourceType: LifecycleTemplateResource, Action: ViewAction}
	CreateLifecycleTemplate = Permission{ResourceType: LifecycleTemplateResource, Action: CreateAction}
	ViewReleaseLifecycle    = Permission{ResourceType: ReleaseLifecycleResource, Action: ViewAction}
	CreateReleaseLifecycle  = Permission{ResourceType: ReleaseLifecycleResource, Action: CreateAction}
	UpdateReleaseLifecycle  = Permission{ResourceType: ReleaseLifecycleResource, Action: UpdateAction}
	DeleteReleaseLifecycle  = Permission{ResourceType: ReleaseLifecycleResource, Action: DeleteAction}
	ViewRelease             = Permission{ResourceType: ReleaseResource, Action: ViewAction}
	CreateRelease           = Permission{ResourceType: ReleaseResource, Action: CreateAction}
	UpdateRelease           = Permission{ResourceType: ReleaseResource, Action: UpdateAction}
	DeleteRelease           = Permission{ResourceType: ReleaseResource, Action: DeleteAction}
	ViewVariable            = Permission{ResourceType: VariableResource, Action: ViewAction}
	ViewPlugin              = Permission{ResourceType: PluginResource, Action: ViewAction}
	CreatePlugin            = Permission{ResourceType: PluginResource, Action: CreateAction}
	UpdatePlugin            = Permission{ResourceType: PluginResource, Action: UpdateAction}
	DeletePlugin            = Permission{ResourceType: PluginResource, Action: DeleteAction}
	ViewVCSProvider         = Permission{ResourceType: VCSProviderResource, Action: ViewAction}
	CreateVCSProvider       = Permission{ResourceType: VCSProviderResource, Action: CreateAction}
	UpdateVCSProvider       = Permission{ResourceType: VCSProviderResource, Action: UpdateAction}
	DeleteVCSProvider       = Permission{ResourceType: VCSProviderResource, Action: DeleteAction}
	ViewComment             = Permission{ResourceType: CommentResource, Action: ViewAction}
	CreateComment           = Permission{ResourceType: CommentResource, Action: CreateAction}
	ViewEnvironmentRule     = Permission{ResourceType: EnvironmentRuleResource, Action: ViewAction}
	CreateEnvironmentRule   = Permission{ResourceType: EnvironmentRuleResource, Action: CreateAction}
	UpdateEnvironmentRule   = Permission{ResourceType: EnvironmentRuleResource, Action: UpdateAction}
	DeleteEnvironmentRule   = Permission{ResourceType: EnvironmentRuleResource, Action: DeleteAction}
)

// assignablePermissions contains all the permissions that
// may be assigned to Phobos roles.
var assignablePermissions = map[Permission]struct{}{
	ViewOrganization:        {},
	ViewMembership:          {},
	CreateMembership:        {},
	UpdateMembership:        {},
	DeleteMembership:        {},
	ViewProject:             {},
	CreateProject:           {},
	UpdateProject:           {},
	DeleteProject:           {},
	ViewJob:                 {},
	CreateUser:              {},
	UpdateUser:              {},
	DeleteUser:              {},
	ViewPipelineTemplate:    {},
	CreatePipelineTemplate:  {},
	DeletePipelineTemplate:  {},
	ViewPipeline:            {},
	ExecutePipeline:         {},
	CancelPipeline:          {},
	ViewAgent:               {},
	CreateAgent:             {},
	UpdateAgent:             {},
	DeleteAgent:             {},
	ViewEnvironment:         {},
	CreateEnvironment:       {},
	UpdateEnvironment:       {},
	DeleteEnvironment:       {},
	ViewServiceAccount:      {},
	CreateServiceAccount:    {},
	UpdateServiceAccount:    {},
	DeleteServiceAccount:    {},
	ViewApprovalRule:        {},
	CreateApprovalRule:      {},
	UpdateApprovalRule:      {},
	DeleteApprovalRule:      {},
	ViewLifecycleTemplate:   {},
	CreateLifecycleTemplate: {},
	ViewReleaseLifecycle:    {},
	CreateReleaseLifecycle:  {},
	UpdateReleaseLifecycle:  {},
	DeleteReleaseLifecycle:  {},
	ViewRelease:             {},
	CreateRelease:           {},
	UpdateRelease:           {},
	DeleteRelease:           {},
	ViewVariable:            {},
	ViewPlugin:              {},
	CreatePlugin:            {},
	UpdatePlugin:            {},
	DeletePlugin:            {},
	ViewVCSProvider:         {},
	CreateVCSProvider:       {},
	UpdateVCSProvider:       {},
	DeleteVCSProvider:       {},
	ViewComment:             {},
	CreateComment:           {},
	ViewEnvironmentRule:     {},
	CreateEnvironmentRule:   {},
	UpdateEnvironmentRule:   {},
	DeleteEnvironmentRule:   {},
}

// Action is an enum representing a CRUD action.
type Action string

// Action constants.
const (
	ViewAction    Action = "view"
	CreateAction  Action = "create"
	UpdateAction  Action = "update"
	DeleteAction  Action = "delete"
	ExecuteAction Action = "execute"
	ClaimAction   Action = "claim"
	CancelAction  Action = "cancel"
)

// HasViewerAccess returns true if Action is viewer access or greater.
func (p Action) HasViewerAccess() bool {
	switch p {
	case ViewAction,
		CreateAction,
		UpdateAction,
		DeleteAction,
		CancelAction,
		ExecuteAction:
		return true
	}

	return false
}

// Permission represents a level of access a subject has to a Phobos resource.
type Permission struct {
	ResourceType ResourceType `json:"resourceType"`
	Action       Action       `json:"action"`
}

// String returns the Permission as <resource_type:action> string.
func (p *Permission) String() string {
	return fmt.Sprintf("%s:%s", p.ResourceType, p.Action)
}

// GTE returns true if permission available is >= wanted permission.
func (p *Permission) GTE(want *Permission) bool {
	if p.String() == want.String() {
		// Both permissions are equal.
		return true
	}

	if p.ResourceType != want.ResourceType {
		// This permission is for a different resource type.
		return false
	}

	if want.Action == ViewAction {
		// Return true if permission grants ViewerAccess.
		return p.Action.HasViewerAccess()
	}

	// Permission action doesn't exist for resource type.
	return false
}

// IsAssignable returns true if permission is assignable to a role.
func (p *Permission) IsAssignable() bool {
	_, ok := assignablePermissions[*p]
	return ok
}

// GetAssignablePermissions returns a list of assignable permissions.
func GetAssignablePermissions() []string {
	assignable := []string{}
	for perm := range assignablePermissions {
		assignable = append(assignable, perm.String())
	}

	sort.Strings(assignable)
	return assignable
}

// ParsePermissions parses and normalizes a slice of
// permission strings and extracts a Permission that adheres
// to the format resource_type:action.
func ParsePermissions(perms []string) ([]Permission, error) {
	var parsedPerms []Permission
	for _, p := range perms {
		if strings.TrimSpace(p) == "" {
			// Skip any empty perms.
			continue
		}

		// Make sure there are exactly two parts.
		pair := strings.Split(p, ":")
		if len(pair) != 2 {
			return nil, errors.New("invalid permission: %s", p, errors.WithErrorCode(errors.EInvalid))
		}

		perm := Permission{
			ResourceType: ResourceType(strings.TrimSpace(pair[0])),
			Action:       Action(strings.TrimSpace(pair[1])),
		}

		parsedPerms = append(parsedPerms, perm)
	}

	return parsedPerms, nil
}
