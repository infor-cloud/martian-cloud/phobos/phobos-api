package models

import (
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	maxTagsPerAgent = 20
	maxTagLength    = 50
)

// AgentType constant
type AgentType string

// Equals returns true if the agent type is equal to the other agent type
func (a AgentType) Equals(other AgentType) bool {
	return a == other
}

// AgentType constants
const (
	OrganizationAgent AgentType = "ORGANIZATION"
	SharedAgent       AgentType = "SHARED"
)

// Agent is responsible for claiming and executing jobs.
type Agent struct {
	OrganizationID  *string
	Type            AgentType
	Name            string
	Description     string
	CreatedBy       string
	Metadata        ResourceMetadata
	Tags            []string
	Disabled        bool
	RunUntaggedJobs bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (a *Agent) ResolveMetadata(key string) (string, error) {
	return a.Metadata.resolveFieldValue(key)
}

// Validate returns an error if the model is not valid
func (a *Agent) Validate() error {
	if err := verifyValidName(a.Name); err != nil {
		return err
	}

	if err := verifyValidDescription(a.Description); err != nil {
		return err
	}

	if a.Type == "" {
		return errors.New("agent type must be specified", errors.WithErrorCode(errors.EInvalid))
	}

	if a.Type.Equals(SharedAgent) && a.OrganizationID != nil {
		return errors.New("shared agent cannot be associated with an organization", errors.WithErrorCode(errors.EInvalid))
	}

	if a.Type.Equals(OrganizationAgent) && a.OrganizationID == nil {
		return errors.New("organization agent must be associated with an organization", errors.WithErrorCode(errors.EInvalid))
	}

	if !a.RunUntaggedJobs && len(a.Tags) == 0 {
		return errors.New("at least one tag must be specified when the run untagged job setting is set to false", errors.WithErrorCode(errors.EInvalid))
	}

	if len(a.Tags) > maxTagsPerAgent {
		return errors.New("exceeded max number of tags per agent: %d", maxTagsPerAgent, errors.WithErrorCode(errors.EInvalid))
	}

	// Validate tags
	tagMap := map[string]struct{}{}
	for _, tag := range a.Tags {
		if _, ok := tagMap[tag]; ok {
			return errors.New("duplicate tag values are not allowed: tag %s has been duplicated", tag, errors.WithErrorCode(errors.EInvalid))
		}
		tagMap[tag] = struct{}{}

		if len(tag) > maxTagLength {
			return errors.New("tag %s exceeds max length of %d", tag, maxTagLength, errors.WithErrorCode(errors.EInvalid))
		}
	}

	return nil
}

// GetOrgName returns the org name for the agent
func (a *Agent) GetOrgName() string {
	resourcePath := strings.Split(a.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[0]
}
