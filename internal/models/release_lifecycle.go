package models

import (
	"strings"
)

// ReleaseLifecycle represents a Phobos release lifecycle.
type ReleaseLifecycle struct {
	Scope               ScopeType
	OrganizationID      string  // required (global scope not allowed)
	ProjectID           *string // required if project scope; must be nil if org scope
	Name                string
	CreatedBy           string
	LifecycleTemplateID string
	EnvironmentNames    []string
	Metadata            ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (r *ReleaseLifecycle) ResolveMetadata(key string) (string, error) {
	return r.Metadata.resolveFieldValue(key)
}

// GetOrgName returns the org name
func (r *ReleaseLifecycle) GetOrgName() string {
	resourcePath := strings.Split(r.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[0]
}

// Validate validates the ReleaseLifecycle model
func (r *ReleaseLifecycle) Validate() error {
	return verifyValidName(r.Name)
}
