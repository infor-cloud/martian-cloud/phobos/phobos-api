package models

import (
	"net/url"
	"strings"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	// maxOAuthScopesPerProvider is the maximum number of OAuth scopes a provider can have.
	// Derived approximation from GitHub and GitLab.
	// https://docs.github.com/en/apps/oauth-apps/building-oauth-apps/scopes-for-oauth-apps#available-scopes
	maxOAuthScopesPerProvider = 40
)

// VCSProviderType represents the vcs provider type.
type VCSProviderType string

// VCSProviderType enums.
const (
	GitHubType VCSProviderType = "GITHUB"
	GitLabType VCSProviderType = "GITLAB"
)

// VCSProviderAuthType represents the auth type a VCS provider uses.
type VCSProviderAuthType string

// VCSProviderAuthType enums.
const (
	AccessTokenType VCSProviderAuthType = "ACCESS_TOKEN"
	OAuthType       VCSProviderAuthType = "OAUTH"
)

// VCSProvider represents a Version Control System provider.
type VCSProvider struct {
	OAuthAccessTokenExpiresAt *time.Time
	URL                       url.URL
	CreatedBy                 string
	Name                      string
	Description               string
	OrganizationID            string
	Type                      VCSProviderType
	Scope                     ScopeType
	AuthType                  VCSProviderAuthType
	OAuthClientSecret         *string
	OAuthClientID             *string
	OAuthState                *string
	OAuthAccessToken          *string
	OAuthRefreshToken         *string
	PersonalAccessToken       *string
	ProjectID                 *string
	ExtraOAuthScopes          []string
	Metadata                  ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (v *VCSProvider) ResolveMetadata(key string) (string, error) {
	return v.Metadata.resolveFieldValue(key)
}

// GetOrgName returns the org name
func (v *VCSProvider) GetOrgName() string {
	resourcePath := strings.Split(v.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[0]
}

// Validate returns an error if the model is not valid
func (v *VCSProvider) Validate() error {
	// Verify name satisfies constraints
	if err := verifyValidName(v.Name); err != nil {
		return err
	}

	switch v.AuthType {
	case OAuthType:
		if v.PersonalAccessToken != nil {
			return errors.New("personal access token cannot be used for %q auth type",
				v.AuthType,
				errors.WithErrorCode(errors.EInvalid),
			)
		}
		if v.OAuthClientID == nil || v.OAuthClientSecret == nil {
			return errors.New("oauth client id and oauth client secret fields are required for %q auth type",
				v.AuthType,
				errors.WithErrorCode(errors.EInvalid),
			)
		}

		if len(v.ExtraOAuthScopes) > maxOAuthScopesPerProvider {
			return errors.New("maximum of %d OAuth scopes can be specified for %q auth type",
				maxOAuthScopesPerProvider,
				v.AuthType,
				errors.WithErrorCode(errors.EInvalid),
			)
		}
	case AccessTokenType:
		if v.OAuthClientID != nil || v.OAuthClientSecret != nil {
			return errors.New("oauth client id or oauth client secret cannot be specified for %q auth type",
				v.AuthType,
				errors.WithErrorCode(errors.EInvalid),
			)
		}
		if v.PersonalAccessToken == nil {
			return errors.New("personal access token field is required for %q auth type",
				v.AuthType,
				errors.WithErrorCode(errors.EInvalid),
			)
		}

		if len(v.ExtraOAuthScopes) > 0 {
			return errors.New("oauth scopes cannot be specified for %q auth type",
				v.AuthType,
				errors.WithErrorCode(errors.EInvalid),
			)
		}
	default:
		return errors.New("vcs provider auth type %s is not supported", v.AuthType)
	}

	// Verify description satisfies constraints
	return verifyValidDescription(v.Description)
}
