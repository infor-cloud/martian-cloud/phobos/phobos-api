package models

// LifecycleTemplateStatus represents the status of a Phobos lifecycle template.
type LifecycleTemplateStatus string

// LifecycleTemplateStatus values.
const (
	LifecycleTemplatePending  LifecycleTemplateStatus = "PENDING"
	LifecycleTemplateUploaded LifecycleTemplateStatus = "UPLOADED"
)

// LifecycleTemplate represents a Phobos lifecycle template.
type LifecycleTemplate struct {
	CreatedBy      string
	Scope          ScopeType
	OrganizationID string  // required (global scope not allowed)
	ProjectID      *string // required if project scope; must be nil if org scope
	Status         LifecycleTemplateStatus
	Metadata       ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (o *LifecycleTemplate) ResolveMetadata(key string) (string, error) {
	return o.Metadata.resolveFieldValue(key)
}
