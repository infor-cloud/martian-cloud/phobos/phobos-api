package models

import (
	"time"

	"github.com/Masterminds/semver/v3"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// ReleaseChangeType constants
const (
	AddParticipantReleaseChangeType    = "ADD_PARTICIPANT"
	RemoveParticipantReleaseChangeType = "REMOVE_PARTICIPANT"
)

// Release is composed of a set of assets, a snapshot of the automation
// used to deploy the assets, and the state of the automation
type Release struct {
	DueDate            *time.Time
	SemanticVersion    string
	ProjectID          string
	CreatedBy          string
	LifecyclePRN       string
	Metadata           ResourceMetadata
	UserParticipantIDs []string
	TeamParticipantIDs []string
	PreRelease         bool
	Latest             bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (r *Release) ResolveMetadata(key string) (string, error) {
	return r.Metadata.resolveFieldValue(key)
}

// Validate validates the model.
func (r *Release) Validate() error {
	if _, err := semver.StrictNewVersion(r.SemanticVersion); err != nil {
		return errors.Wrap(err, "invalid semantic version", errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}
