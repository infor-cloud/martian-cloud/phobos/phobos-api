package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

func TestHasViewerAccess(t *testing.T) {
	actions := []Action{
		ViewAction,
		CreateAction,
		UpdateAction,
		DeleteAction,
	}

	// Positive.
	for _, action := range actions {
		assert.True(t, action.HasViewerAccess())
	}

	// Negative
	assert.False(t, Action("other").HasViewerAccess())
}

func TestString(t *testing.T) {
	assert.Equal(t, "organization:view", ViewOrganization.String())
}

func TestGTE(t *testing.T) {
	testCases := []struct {
		have   *Permission
		want   *Permission
		name   string
		expect bool
	}{
		{
			name:   "permissions are equal",
			have:   &CreateMembership,
			want:   &CreateMembership,
			expect: true,
		},
		{
			name:   "permissions have different actions",
			have:   &CreateMembership,
			want:   &DeleteMembership,
			expect: false,
		},
		{
			name:   "permissions are for different resource types",
			have:   &CreateMembership,
			want:   &ViewOrganization,
			expect: false,
		},
		{
			name:   "permission grants view action",
			have:   &UpdateMembership,
			want:   &ViewMembership,
			expect: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.expect, test.have.GTE(test.want))
		})
	}
}

func TestIsAssignable(t *testing.T) {
	// Positive
	for perm := range assignablePermissions {
		assert.True(t, perm.IsAssignable())
	}
	// Negative
	perm := Permission{ResourceType: OrganizationResource, Action: CreateAction}
	assert.False(t, perm.IsAssignable())
}

func TestGetAssignablePermissions(t *testing.T) {
	assert.Equal(t, len(assignablePermissions), len(GetAssignablePermissions()))
}

func TestParsePermissions(t *testing.T) {
	testCases := []struct {
		name              string
		expectErrorCode   errors.CodeType
		input             []string
		expectPermissions []Permission
	}{
		{
			name: "successfully parse permissions",
			input: []string{
				"membership:create",
				"membership:view",
				" organization : view ", // Should parse these properly as well.
			},
			expectPermissions: []Permission{
				CreateMembership,
				ViewMembership,
				ViewOrganization,
			},
		},
		{
			name: "permissions are not proper format",
			input: []string{
				"organization.view",
				"invalid",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "permissions are empty",
			input: []string{
				"  ",
				"",
				"organization:view",
			},
			expectPermissions: []Permission{
				ViewOrganization,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualParsed, err := ParsePermissions(test.input)

			assert.Equal(t, test.expectPermissions, actualParsed)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
			} else if err != nil {
				t.Fatal(err)
			}
		})
	}
}
