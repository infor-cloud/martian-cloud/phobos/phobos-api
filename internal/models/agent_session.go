package models

import "time"

// AgentSessionHeartbeatInterval is the interval that agents should send heartbeats
const AgentSessionHeartbeatInterval = time.Minute

// AgentSession represents a session for an agent
type AgentSession struct {
	LastContactTimestamp time.Time
	AgentID              string
	Metadata             ResourceMetadata
	ErrorCount           int
	Internal             bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (a *AgentSession) ResolveMetadata(key string) (string, error) {
	val, err := a.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "last_contacted_at":
			val = a.LastContactTimestamp.Format(time.RFC3339Nano)
		default:
			return "", err
		}
	}

	return val, nil
}

// Active returns true if the session has received a heartbeat within the last heartbeat interval
func (a *AgentSession) Active() bool {
	// Check if the elapsed time since the last heartbeat exceeds the heartbeat interval plus some leeway
	return time.Since(a.LastContactTimestamp) <= (AgentSessionHeartbeatInterval + (5 * time.Second))
}
