package models

// MetricName represents the type of the metric.
type MetricName string

// MetricName constants.
const (
	// MetricPipelineCompleted is the number of completed pipelines
	MetricPipelineCompleted MetricName = "PIPELINE_COMPLETED"
	// MetricPipelineDuration is the time it takes for a pipeline to complete
	MetricPipelineDuration MetricName = "PIPELINE_DURATION"
	// MetricDeploymentRecoveryTime is the time it takes for a deployment to recover after it has failed
	MetricDeploymentRecoveryTime MetricName = "DEPLOYMENT_RECOVERY_TIME"
	// MetricDeploymentUpdated is the number of updates to a deployment
	MetricDeploymentUpdated MetricName = "DEPLOYMENT_UPDATED"
	// MetricPipelineLeadTime is the time from when a pipeline is created to when it is completed
	MetricPipelineLeadTime MetricName = "PIPELINE_LEAD_TIME"
	// MetricApprovalRequestWaitTime is the time it takes for an approval request to be approved
	MetricApprovalRequestWaitTime MetricName = "APPROVAL_REQUEST_WAIT_TIME"
	// MetricApprovalRequestCreated is the number of created approval requests
	MetricApprovalRequestCreated MetricName = "APPROVAL_REQUEST_CREATED"
	// MetricApprovalRequestCompleted is the number of completed approval requests
	MetricApprovalRequestCompleted MetricName = "APPROVAL_REQUEST_COMPLETED"
)

// MetricTagName represents the supported metric tag names
type MetricTagName string

const (
	// MetricTagNamePipelineType is the type of the pipeline
	MetricTagNamePipelineType MetricTagName = "PIPELINE_TYPE"
	// MetricTagNamePipelineStatus is the status of the pipeline
	MetricTagNamePipelineStatus MetricTagName = "PIPELINE_STATUS"
	// MetricTagNameApprovalTargetType is the type of the approval
	MetricTagNameApprovalTargetType MetricTagName = "APPROVAL_TARGET_TYPE"
	// MetricTagNameReleaseLifecycle is the name of the release lifecycle
	MetricTagNameReleaseLifecycle MetricTagName = "RELEASE_LIFECYCLE"
)

// Metric represents a resource metric.
type Metric struct {
	Tags            map[MetricTagName]string
	PipelineID      *string
	ReleaseID       *string
	ProjectID       *string
	OrganizationID  *string
	EnvironmentName *string
	Name            MetricName
	Metadata        ResourceMetadata
	Value           float64
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (m *Metric) ResolveMetadata(key string) (string, error) {
	return m.Metadata.resolveFieldValue(key)
}
