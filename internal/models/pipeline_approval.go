package models

// PipelineApprovalStatus represents the approval status of a pipeline node.
type PipelineApprovalStatus string

// ApprovalStatus enums.
const (
	ApprovalNotRequired PipelineApprovalStatus = "NOT_REQUIRED"
	ApprovalPending     PipelineApprovalStatus = "PENDING"
	ApprovalApproved    PipelineApprovalStatus = "APPROVED"
)

// PipelineApprovalType represents the pipeline node type the approval is for.
type PipelineApprovalType string

// ApprovalType enums.
const (
	ApprovalTypeTask     PipelineApprovalType = "TASK"
	ApprovalTypePipeline PipelineApprovalType = "PIPELINE" // nested or non-nested
)

// PipelineApproval defines an approval for a pipeline or task.
type PipelineApproval struct {
	UserID           *string
	ServiceAccountID *string
	TaskPath         *string
	PipelineID       string
	Type             PipelineApprovalType
	Metadata         ResourceMetadata
	ApprovalRuleIDs  []string
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (p *PipelineApproval) ResolveMetadata(key string) (string, error) {
	return p.Metadata.resolveFieldValue(key)
}
