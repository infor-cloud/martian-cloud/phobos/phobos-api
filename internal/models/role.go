package models

import (
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// Role defines a subject's ability to access or modify
// resources within Phobos. It provides a set of permissions
// that dictate which resources can be viewed or modified.
type Role struct {
	Name        string
	Description string
	CreatedBy   string
	permissions []Permission
	Metadata    ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (r *Role) ResolveMetadata(key string) (string, error) {
	val, err := r.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "name":
			val = r.Name
		default:
			return "", err
		}
	}

	return val, nil
}

// SetPermissions sets permissions for a role.
func (r *Role) SetPermissions(perms []Permission) {
	r.permissions = perms
}

// GetPermissions returns permissions for a role.
func (r *Role) GetPermissions() []Permission {
	if perms, ok := DefaultRoleID(r.Metadata.ID).Permissions(); ok {
		return perms
	}

	return r.permissions
}

// Validate returns an error if the model is not valid
func (r *Role) Validate() error {
	// Verify name satisfies constraints
	if err := verifyValidName(r.Name); err != nil {
		return err
	}

	// Validate and deduplicate permissions.
	seen := map[string]struct{}{}
	uniquePerms := []Permission{}
	for _, perm := range r.permissions {
		// Make sure the permission can be assigned.
		if !perm.IsAssignable() {
			return errors.New(
				"Permission '%s' cannot be assigned to a role", perm,
				errors.WithErrorCode(errors.EInvalid),
			)
		}

		if _, ok := seen[perm.String()]; ok {
			continue
		}

		seen[perm.String()] = struct{}{}
		uniquePerms = append(uniquePerms, perm)
	}

	// Assign perms.
	r.permissions = uniquePerms

	// Verify description satisfies constraints
	return verifyValidDescription(r.Description)
}

// DefaultRoleID represents the static UUIDs for default Phobos roles.
type DefaultRoleID string

// DefaultRoleID constants.
const (
	OwnerRoleID          DefaultRoleID = "3e8eb414-4504-4dc5-ad81-2f0d20db6ee4"
	ReleaseManagerRoleID DefaultRoleID = "c78c6d31-465f-4d16-a365-51f10a4ca37d"
	DeveloperRoleID      DefaultRoleID = "d50e782f-c82a-4e18-b68b-c7eb176f4e5e"
	ViewerRoleID         DefaultRoleID = "2e240dba-ee81-4f0b-bd97-b8d8b15b9119"
)

// String returns the ID as a string.
func (d DefaultRoleID) String() string {
	return string(d)
}

// Equals returns true if the both IDs are the same.
func (d DefaultRoleID) Equals(want string) bool {
	return d.String() == want
}

// IsDefaultRole returns true if ID belongs to a default role.
func (d DefaultRoleID) IsDefaultRole() bool {
	switch d {
	case OwnerRoleID,
		ReleaseManagerRoleID,
		DeveloperRoleID,
		ViewerRoleID:
		return true
	}

	return false
}

// Permissions returns the Permission set for a default Phobos role.
func (d DefaultRoleID) Permissions() ([]Permission, bool) {
	perms, ok := defaultRolePermissions[d]
	return perms, ok
}

// defaultRolePermissions is a map of default role's ID to its Permission set.
var defaultRolePermissions = map[DefaultRoleID][]Permission{
	// Owner Role
	OwnerRoleID: {
		ViewOrganization,
		ViewMembership,
		CreateMembership,
		UpdateMembership,
		DeleteMembership,
		ViewProject,
		CreateProject,
		UpdateProject,
		DeleteProject,
		ViewPipelineTemplate,
		CreatePipelineTemplate,
		DeletePipelineTemplate,
		ViewPipeline,
		ExecutePipeline,
		CancelPipeline,
		ViewAgent,
		CreateAgent,
		UpdateAgent,
		DeleteAgent,
		ViewEnvironment,
		CreateEnvironment,
		UpdateEnvironment,
		DeleteEnvironment,
		ViewServiceAccount,
		CreateServiceAccount,
		UpdateServiceAccount,
		DeleteServiceAccount,
		ViewLifecycleTemplate,
		CreateLifecycleTemplate,
		ViewReleaseLifecycle,
		CreateReleaseLifecycle,
		UpdateReleaseLifecycle,
		DeleteReleaseLifecycle,
		ViewApprovalRule,
		CreateApprovalRule,
		UpdateApprovalRule,
		DeleteApprovalRule,
		ViewRelease,
		CreateRelease,
		UpdateRelease,
		DeleteRelease,
		ViewVariable,
		ViewPlugin,
		CreatePlugin,
		UpdatePlugin,
		DeletePlugin,
		ViewVCSProvider,
		CreateVCSProvider,
		UpdateVCSProvider,
		DeleteVCSProvider,
		ViewComment,
		CreateComment,
		ViewEnvironmentRule,
		CreateEnvironmentRule,
		UpdateEnvironmentRule,
		DeleteEnvironmentRule,
		ViewJob,
	},
	// ReleaseManager Role.
	ReleaseManagerRoleID: {
		ViewOrganization,
		ViewMembership,
		ViewProject,
		ViewPipelineTemplate,
		CreatePipelineTemplate,
		DeletePipelineTemplate,
		ViewPipeline,
		ExecutePipeline,
		CancelPipeline,
		ViewAgent,
		ViewEnvironment,
		ViewServiceAccount,
		CreateServiceAccount,
		UpdateServiceAccount,
		DeleteServiceAccount,
		ViewLifecycleTemplate,
		CreateLifecycleTemplate,
		ViewReleaseLifecycle,
		CreateReleaseLifecycle,
		UpdateReleaseLifecycle,
		DeleteReleaseLifecycle,
		ViewApprovalRule,
		ViewRelease,
		CreateRelease,
		UpdateRelease,
		DeleteRelease,
		ViewVariable,
		ViewPlugin,
		CreatePlugin,
		UpdatePlugin,
		DeletePlugin,
		ViewVCSProvider,
		CreateVCSProvider,
		UpdateVCSProvider,
		DeleteVCSProvider,
		ViewComment,
		CreateComment,
		ViewEnvironmentRule,
		CreateEnvironmentRule,
		UpdateEnvironmentRule,
		DeleteEnvironmentRule,
		ViewJob,
	},
	// Developer Role.
	DeveloperRoleID: {
		ViewOrganization,
		ViewMembership,
		ViewProject,
		ViewPipelineTemplate,
		CreatePipelineTemplate,
		DeletePipelineTemplate,
		ViewPipeline,
		ExecutePipeline,
		CancelPipeline,
		ViewAgent,
		ViewEnvironment,
		ViewServiceAccount,
		CreateServiceAccount,
		UpdateServiceAccount,
		DeleteServiceAccount,
		ViewLifecycleTemplate,
		CreateLifecycleTemplate,
		ViewReleaseLifecycle,
		CreateReleaseLifecycle,
		UpdateReleaseLifecycle,
		DeleteReleaseLifecycle,
		ViewApprovalRule,
		ViewRelease,
		CreateRelease,
		UpdateRelease,
		DeleteRelease,
		ViewVariable,
		ViewPlugin,
		CreatePlugin,
		UpdatePlugin,
		DeletePlugin,
		ViewVCSProvider,
		CreateVCSProvider,
		UpdateVCSProvider,
		DeleteVCSProvider,
		ViewComment,
		CreateComment,
		ViewEnvironmentRule,
		ViewJob,
	},
	// Viewer Role.
	ViewerRoleID: {
		ViewOrganization,
		ViewMembership,
		ViewProject,
		ViewPipelineTemplate,
		ViewPipeline,
		ViewAgent,
		ViewEnvironment,
		ViewServiceAccount,
		ViewLifecycleTemplate,
		ViewReleaseLifecycle,
		ViewApprovalRule,
		ViewRelease,
		ViewVariable,
		ViewVCSProvider,
		ViewComment,
		ViewEnvironmentRule,
		ViewJob,
	},
}
