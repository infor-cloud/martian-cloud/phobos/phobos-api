package models

import (
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// ApprovalRule represents a rule that can be used to control who can approve a task
type ApprovalRule struct {
	Scope             ScopeType
	OrgID             string  // required (global scope not allowed)
	ProjectID         *string // required if project scope; must be nil if org scope
	Name              string
	Description       string
	CreatedBy         string
	Metadata          ResourceMetadata
	TeamIDs           []string
	UserIDs           []string
	ServiceAccountIDs []string
	ApprovalsRequired int
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (a *ApprovalRule) ResolveMetadata(key string) (string, error) {
	return a.Metadata.resolveFieldValue(key)
}

// Validate returns an error if the model is not valid
func (a *ApprovalRule) Validate() error {
	if err := verifyValidName(a.Name); err != nil {
		return err
	}

	if err := verifyValidDescription(a.Description); err != nil {
		return err
	}

	if a.ApprovalsRequired < 1 {
		return errors.New("approvals required must be greater than 0", errors.WithErrorCode(errors.EInvalid))
	}

	// We ignore team here since a team could have arbitrary number of users.
	if len(a.TeamIDs) == 0 && a.ApprovalsRequired > (len(a.UserIDs)+len(a.ServiceAccountIDs)) {
		return errors.New("approvals required cannot be greater than the number of users and service accounts", errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}
