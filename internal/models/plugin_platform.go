package models

// PluginPlatform represents a support platform for a plugin.
type PluginPlatform struct {
	PluginVersionID string
	OperatingSystem string
	Architecture    string
	SHASum          string
	Filename        string
	CreatedBy       string
	Metadata        ResourceMetadata
	BinaryUploaded  bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (p *PluginPlatform) ResolveMetadata(key string) (string, error) {
	return p.Metadata.resolveFieldValue(key)
}
