package models

import (
	"time"

	"github.com/aws/smithy-go/ptr"
)

// ActivityEventAction represents an action that can be performed on a resource.
type ActivityEventAction string

// ActivityEventAction constants.
const (
	ActionCreate           ActivityEventAction = "CREATE"
	ActionUpdate           ActivityEventAction = "UPDATE"
	ActionDelete           ActivityEventAction = "DELETE"
	ActionStart            ActivityEventAction = "START"
	ActionRetry            ActivityEventAction = "RETRY"
	ActionCreateMembership ActivityEventAction = "CREATE_MEMBERSHIP"
	ActionRemoveMembership ActivityEventAction = "REMOVE_MEMBERSHIP"
	ActionCancel           ActivityEventAction = "CANCEL"
	ActionDefer            ActivityEventAction = "DEFER"
	ActionUndefer          ActivityEventAction = "UNDEFER"
	ActionApprove          ActivityEventAction = "APPROVE"
	ActionRevokeApproval   ActivityEventAction = "REVOKE_APPROVAL"
	ActionSchedule         ActivityEventAction = "SCHEDULE"
	ActionUnschedule       ActivityEventAction = "UNSCHEDULE"
)

// ActivityEventTargetType represents a type of resource that can be acted upon.
type ActivityEventTargetType string

// ActivityEventTargetType constants.
const (
	TargetRole               ActivityEventTargetType = "ROLE"
	TargetOrganization       ActivityEventTargetType = "ORGANIZATION"
	TargetProject            ActivityEventTargetType = "PROJECT"
	TargetProjectVariableSet ActivityEventTargetType = "PROJECT_VARIABLE_SET"
	TargetGlobal             ActivityEventTargetType = "GLOBAL"
	TargetMembership         ActivityEventTargetType = "MEMBERSHIP"
	TargetAgent              ActivityEventTargetType = "AGENT"
	TargetPipelineTemplate   ActivityEventTargetType = "PIPELINE_TEMPLATE"
	TargetPipeline           ActivityEventTargetType = "PIPELINE"
	TargetTeam               ActivityEventTargetType = "TEAM"
	TargetEnvironment        ActivityEventTargetType = "ENVIRONMENT"
	TargetServiceAccount     ActivityEventTargetType = "SERVICE_ACCOUNT"
	TargetApprovalRule       ActivityEventTargetType = "APPROVAL_RULE"
	TargetLifecycleTemplate  ActivityEventTargetType = "LIFECYCLE_TEMPLATE"
	TargetReleaseLifecycle   ActivityEventTargetType = "RELEASE_LIFECYCLE"
	TargetRelease            ActivityEventTargetType = "RELEASE"
	TargetPlugin             ActivityEventTargetType = "PLUGIN"
	TargetPluginVersion      ActivityEventTargetType = "PLUGIN_VERSION"
	TargetComment            ActivityEventTargetType = "COMMENT"
	TargetUser               ActivityEventTargetType = "USER"
	TargetVCSProvider        ActivityEventTargetType = "VCS_PROVIDER"
	TargetThread             ActivityEventTargetType = "THREAD"
	TargetEnvironmentRule    ActivityEventTargetType = "ENVIRONMENT_RULE"
)

// String returns the string representation of the activity event target type.
func (t ActivityEventTargetType) String() string {
	return string(t)
}

// ActivityEventCreateMembershipPayload represents the payload for a
// create membership activity event.
type ActivityEventCreateMembershipPayload struct {
	UserID           *string `json:"userId"`
	ServiceAccountID *string `json:"serviceAccountId"`
	TeamID           *string `json:"teamId"`
	Role             string  `json:"role"`
}

// ActivityEventUpdateMembershipPayload represents the payload for a
// update membership activity event.
type ActivityEventUpdateMembershipPayload struct {
	PrevRole string `json:"prevRole"`
	NewRole  string `json:"newRole"`
}

// ActivityEventRemoveMembershipPayload represents the payload for a
// remove membership activity event.
type ActivityEventRemoveMembershipPayload struct {
	UserID           *string `json:"userId"`
	ServiceAccountID *string `json:"serviceAccountId"`
	TeamID           *string `json:"teamId"`
}

// ActivityEventDeleteResourcePayload represents the payload for a
// delete resource activity event.
type ActivityEventDeleteResourcePayload struct {
	Name *string `json:"name"`
	ID   string  `json:"id"`
	Type string  `json:"type"`
}

// ActivityEventUpdateTeamPayload represents the payload for a
// update team activity event.
type ActivityEventUpdateTeamPayload struct {
	UserID     string `json:"userId"`
	ChangeType string `json:"changeType"`
	Maintainer bool   `json:"maintainer"`
}

// ActivityEventUpdateReleasePayload represents the payload for a
// update release activity event.
type ActivityEventUpdateReleasePayload struct {
	ID         string `json:"id"`
	ChangeType string `json:"changeType"`
	Type       string `json:"type"`
}

// ActivityEventUpdatePipelineNodePayload represents the payload for a
// update pipeline node activity event.
type ActivityEventUpdatePipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
}

// ActivityEventApprovePipelineNodePayload represents the payload for an
// approve pipeline node activity event.
type ActivityEventApprovePipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
}

// ActivityEventRevokeApprovalPipelineNodePayload represents the payload for a
// revoke approval pipeline node activity event.
type ActivityEventRevokeApprovalPipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
}

// ActivityEventRetryPipelineNodePayload represents the payload for a
// retry pipeline node activity event.
type ActivityEventRetryPipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
}

// ActivityEventSchedulePipelineNodePayload represents the payload for a
// schedule pipeline node activity event.
type ActivityEventSchedulePipelineNodePayload struct {
	NodePath  string     `json:"nodePath"`
	NodeType  string     `json:"nodeType"`
	StartTime *time.Time `json:"startTime"`
}

// ActivityEventUnschedulePipelineNodePayload represents the payload for an
// unschedule pipeline node activity event.
type ActivityEventUnschedulePipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
}

// ActivityEventStartPipelineNodePayload represents the payload for a
// start pipeline node activity event.
type ActivityEventStartPipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
}

// ActivityEventDeferPipelineNodePayload represents the payload for a
// defer pipeline node activity event.
type ActivityEventDeferPipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
	Reason   string `json:"reason"`
}

// ActivityEventUndeferPipelineNodePayload represents the payload for an
// undefer pipeline node activity event.
type ActivityEventUndeferPipelineNodePayload struct {
	NodePath string `json:"nodePath"`
	NodeType string `json:"nodeType"`
}

// ActivityEvent represents an activity event.
type ActivityEvent struct {
	UserID           *string
	ServiceAccountID *string
	OrganizationID   *string
	ProjectID        *string
	TargetID         *string
	Payload          []byte
	Action           ActivityEventAction
	TargetType       ActivityEventTargetType
	Metadata         ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (a *ActivityEvent) ResolveMetadata(key string) (string, error) {
	val, err := a.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "user_id":
			val = ptr.ToString(a.UserID)
		case "service_account_id":
			val = ptr.ToString(a.ServiceAccountID)
		case "action":
			val = string(a.Action)
		case "target_type":
			val = string(a.TargetType)
		default:
			return "", err
		}
	}

	return val, nil
}
