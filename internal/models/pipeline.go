package models

import (
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/cronexpr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	maxPipelineAnnotations           = 10
	maxPipelineAnnotationKeyLength   = 63
	maxPipelineAnnotationValueLength = 253
)

// PipelineTaskWhenCondition defines the possible conditions for a task to be executed
type PipelineTaskWhenCondition string

// PipelineTaskWhenCondition Constants
const (
	AutoPipelineTask   PipelineTaskWhenCondition = "auto"
	ManualPipelineTask PipelineTaskWhenCondition = "manual"
)

// PipelineWhenCondition defines the possible conditions for a pipeline to be executed
type PipelineWhenCondition string

// PipelineWhenCondition Constants
const (
	AutoPipeline   PipelineWhenCondition = "auto"
	ManualPipeline PipelineWhenCondition = "manual"
)

// PipelineType represents the type of a pipeline
type PipelineType string

// PipelineType constants
const (
	// RunbookPipelineType is a pipeline that is executed on demand and is not associated with an environment
	RunbookPipelineType PipelineType = "RUNBOOK"
	// ReleaseLifecyclePipelineType is a pipeline that is executed as part of a release lifecycle
	ReleaseLifecyclePipelineType PipelineType = "RELEASE_LIFECYCLE"
	// NestedPipelineType is a pipeline that is executed as a nested pipeline
	NestedPipelineType PipelineType = "NESTED"
	// DeploymentPipelineType is a pipeline that is executed as part of a deployment
	DeploymentPipelineType PipelineType = "DEPLOYMENT"
)

// DefaultCronScheduleTimezone is the default timezone for cron schedules
var DefaultCronScheduleTimezone = time.UTC

// NestedPipeline represents a nested pipeline
type NestedPipeline struct {
	EnvironmentName    *string
	ScheduledStartTime *time.Time
	CronSchedule       *CronSchedule
	OnError            statemachine.OnErrorStrategy
	LatestPipelineID   string
	Name               string
	Path               string
	Status             statemachine.NodeStatus
	ApprovalStatus     PipelineApprovalStatus
	Dependencies       []string
	Errors             []string
}

// ClearErrors clears the errors for the nested pipeline
func (n *NestedPipeline) ClearErrors() {
	n.Errors = nil
}

// ClearSchedule clears the schedule for the nested pipeline
func (n *NestedPipeline) ClearSchedule() {
	n.ScheduledStartTime = nil
	n.CronSchedule = nil
}

// PipelineAction defines an action that will be executed using a provider plugin
type PipelineAction struct {
	Name       string
	PluginName string
	ActionName string
	Status     statemachine.NodeStatus
	Path       string
}

// CronSchedule represents a cron schedule for running this task
type CronSchedule struct {
	Timezone   *string
	Expression string
}

// Validate validates the cron schedule
func (c *CronSchedule) Validate() error {
	// Use validation logic from HCL config to avoid duplication of validation logic
	schedule := config.CronScheduleOptions{
		Timezone:   c.Timezone,
		Expression: c.Expression,
	}
	return schedule.Validate()
}

// NextTime returns the next time the cron schedule will run
func (c *CronSchedule) NextTime() (*time.Time, error) {
	// Default timezone to UTC if it's not specified
	location := DefaultCronScheduleTimezone
	if c.Timezone != nil {
		// Verify that the timezone is valid
		val, err := time.LoadLocation(*c.Timezone)
		if err != nil {
			return nil, fmt.Errorf("timezone in schedule is invalid %s: %v", *c.Timezone, err)
		}
		location = val
	}

	// Verify that the cron expression is valid
	exp, err := cronexpr.Parse(c.Expression)
	if err != nil {
		return nil, fmt.Errorf("cron pattern in schedule is invalid %s: %v", c.Expression, err)
	}

	// Get next time based on the cron expression
	nextTime := exp.Next(time.Now().In(location)).UTC()
	if nextTime.IsZero() {
		return nil, fmt.Errorf("cron pattern in schedule %s does not have a valid next time", c.Expression)
	}

	return &nextTime, nil
}

// PipelineTask is a task within a pipeline
type PipelineTask struct {
	// interval between retries when this is a repeated task
	Interval *time.Duration
	// LastAttemptFinishedAt is the time the last attempt finished
	LastAttemptFinishedAt *time.Time
	// scheduled start time for the task (optional), this field can only
	// be set when the task is manual
	ScheduledStartTime *time.Time
	// CronSchedule is the cron schedule for the task
	CronSchedule *CronSchedule
	// status of the task
	Status statemachine.NodeStatus
	// approval status of the task
	ApprovalStatus PipelineApprovalStatus
	// when condition specifies when the task should be executed
	When PipelineTaskWhenCondition
	// on error specifies the behavior of the pipeline node when a task encounters an error
	OnError statemachine.OnErrorStrategy
	// latest job associated with this task
	LatestJobID *string
	// path is the path of the task within the pipeline
	Path string
	// name of the task
	Name string
	// image is the custom image to use for the task
	Image *string
	// actions are the actions that will be executed by the task
	Actions []*PipelineAction
	// list of tags to restrict which agents can run jobs for this task
	AgentTags []string
	// Dependencies are tasks needed by this task (within the same stage)
	Dependencies []string
	// optional approval rule IDs
	ApprovalRuleIDs []string
	// any errors that have occurred during initialization.
	Errors []string
	// max attempts specifies how many times a task will be retried automatically
	MaxAttempts int
	// AttemptCount is the number of times the task has been attempted
	AttemptCount int
	// indicates if the task has a success condition
	HasSuccessCondition bool
}

// IsRetryable returns true if the task can be retried
func (p *PipelineTask) IsRetryable() bool {
	return p.AttemptCount < p.MaxAttempts
}

// ClearErrors clears the errors for the task
func (p *PipelineTask) ClearErrors() {
	p.Errors = nil
}

// ClearSchedule clears the schedule for the task
func (p *PipelineTask) ClearSchedule() {
	p.ScheduledStartTime = nil
	p.CronSchedule = nil
}

// PipelineStage represents a stage within a pipeline
type PipelineStage struct {
	NestedPipelines []*NestedPipeline
	Path            string
	Name            string
	Status          statemachine.NodeStatus
	Tasks           []*PipelineTask
}

// PipelineTimestamps includes the timestamp for each pipeline
type PipelineTimestamps struct {
	StartedTimestamp   *time.Time
	CompletedTimestamp *time.Time
}

// PipelineAnnotation represents an annotation for a pipeline
type PipelineAnnotation struct {
	Link  *string
	Key   string
	Value string
}

// Pipeline represents a Phobos pipeline.
type Pipeline struct {
	ReleaseID              *string
	ParentPipelineID       *string
	ParentPipelineNodePath *string
	EnvironmentName        *string
	CreatedBy              string
	ProjectID              string
	PipelineTemplateID     string
	ApprovalStatus         PipelineApprovalStatus
	When                   PipelineWhenCondition
	Type                   PipelineType
	Status                 statemachine.NodeStatus
	Timestamps             PipelineTimestamps
	Metadata               ResourceMetadata
	Stages                 []*PipelineStage
	Annotations            []*PipelineAnnotation
	ApprovalRuleIDs        []string
	Superseded             bool
}

// Validate validates the pipeline model
func (p *Pipeline) Validate() error {
	if p.Type == "" {
		return errors.New("pipeline type is required", errors.WithErrorCode(errors.EInvalid))
	}

	switch p.Type {
	case ReleaseLifecyclePipelineType:
		if p.ReleaseID == nil {
			return errors.New("release ID is required for release pipelines", errors.WithErrorCode(errors.EInvalid))
		}
		if p.EnvironmentName != nil {
			return errors.New("environment name is not allowed for release pipelines", errors.WithErrorCode(errors.EInvalid))
		}
	case DeploymentPipelineType:
		if p.EnvironmentName == nil {
			return errors.New("environment name is required for deployment pipelines", errors.WithErrorCode(errors.EInvalid))
		}
	case RunbookPipelineType:
		if p.EnvironmentName != nil {
			return errors.New("environment name is not allowed for runbook pipelines", errors.WithErrorCode(errors.EInvalid))
		}
	case NestedPipelineType:
		// Explicitly do nothing
	default:
		return errors.New("invalid pipeline type: %s", p.Type, errors.WithErrorCode(errors.EInvalid))
	}

	if p.Annotations != nil {
		if len(p.Annotations) > maxPipelineAnnotations {
			return errors.New("maximum of %d annotations allowed", maxPipelineAnnotations, errors.WithErrorCode(errors.EInvalid))
		}

		for _, annotation := range p.Annotations {
			if annotation.Key == "" {
				return errors.New("annotation key cannot be empty", errors.WithErrorCode(errors.EInvalid))
			}
			if annotation.Value == "" {
				return errors.New("annotation value cannot be empty", errors.WithErrorCode(errors.EInvalid))
			}
			if err := verifyValidName(annotation.Key); err != nil {
				return errors.Wrap(err, "invalid annotation key")
			}
			if len(annotation.Value) > maxPipelineAnnotationValueLength {
				return errors.New("annotation value cannot be longer than %d characters", maxPipelineAnnotationValueLength, errors.WithErrorCode(errors.EInvalid))
			}
		}
	}

	return nil
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (p *Pipeline) ResolveMetadata(key string) (string, error) {
	val, err := p.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "completed_at":
			if p.Timestamps.CompletedTimestamp != nil {
				val = p.Timestamps.CompletedTimestamp.Format(time.RFC3339Nano)
			}
		case "started_at":
			if p.Timestamps.StartedTimestamp != nil {
				val = p.Timestamps.StartedTimestamp.Format(time.RFC3339Nano)
			}
		default:
			return "", err
		}
	}

	return val, nil
}

// GetOrgName returns the org name for the pipeline
func (p *Pipeline) GetOrgName() string {
	resourcePath := strings.Split(p.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[0]
}

// GetProjectName returns the project name for the pipeline
func (p *Pipeline) GetProjectName() string {
	resourcePath := strings.Split(p.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[1]
}

// GetStage returns a pipeline stage by its path
func (p *Pipeline) GetStage(path string) (*PipelineStage, bool) {
	for _, s := range p.Stages {
		if s.Path == path {
			return s, true
		}
	}
	return nil, false
}

// GetNestedPipeline returns a nested pipeline by its path
func (p *Pipeline) GetNestedPipeline(path string) (*NestedPipeline, bool) {
	for _, s := range p.Stages {
		for _, nestedPipeline := range s.NestedPipelines {
			if nestedPipeline.Path == path {
				return nestedPipeline, true
			}
		}
	}
	return nil, false
}

// GetTask returns a pipeline task by its path
func (p *Pipeline) GetTask(path string) (*PipelineTask, bool) {
	for _, s := range p.Stages {
		for _, t := range s.Tasks {
			if t.Path == path {
				return t, true
			}
		}
	}
	return nil, false
}

// GetAction returns a pipeline action by its path
func (p *Pipeline) GetAction(path string) (*PipelineAction, bool) {
	for _, s := range p.Stages {
		for _, t := range s.Tasks {
			for _, a := range t.Actions {
				if a.Path == path {
					return a, true
				}
			}
		}
	}
	return nil, false
}

// CancelNode cancels the pipeline node
func (p *Pipeline) CancelNode(nodePath string, force bool) ([]statemachine.NodeStatusChange, error) {
	stateMachine := p.BuildStateMachine()

	node, ok := stateMachine.GetNode(nodePath)
	if !ok {
		return nil, fmt.Errorf("node with path %s not found", nodePath)
	}

	if err := node.Cancel(); err != nil {
		return nil, err
	}

	if force && node.Status() != statemachine.CanceledNodeStatus {
		// Cancel again to make sure node is in final state
		if err := node.Cancel(); err != nil {
			return nil, err
		}
	}

	p.syncStateMachine(stateMachine)

	return stateMachine.GetStatusChanges()
}

// SetStatus sets the status of the pipeline
func (p *Pipeline) SetStatus(status statemachine.NodeStatus) ([]statemachine.NodeStatusChange, error) {
	return p.SetNodeStatus("pipeline", status)
}

// SetNodeStatus sets the status of a node in the pipeline
func (p *Pipeline) SetNodeStatus(nodePath string, status statemachine.NodeStatus) ([]statemachine.NodeStatusChange, error) {
	stateMachine := p.BuildStateMachine()

	node, ok := stateMachine.GetNode(nodePath)
	if !ok {
		return nil, fmt.Errorf("node with path %s not found", nodePath)
	}

	if err := node.SetStatus(status); err != nil {
		return nil, err
	}

	p.syncStateMachine(stateMachine)

	return stateMachine.GetStatusChanges()
}

// BuildStateMachine builds a state machine based on the current state of the pipeline graph
func (p *Pipeline) BuildStateMachine() *statemachine.StateMachine {
	pipelineNode := statemachine.NewPipelineNode(&statemachine.NewPipelineNodeInput{
		Path:   "pipeline",
		Status: p.Status,
	})

	for _, stage := range p.Stages {
		stageNode := statemachine.NewStageNode(stage.Path, pipelineNode, stage.Status)

		for _, task := range stage.Tasks {
			status := task.Status
			taskNode := statemachine.NewTaskNode(&statemachine.NewTaskNodeInput{
				Path:         task.Path,
				Parent:       stageNode,
				Status:       &status,
				Dependencies: task.Dependencies,
				OnError:      task.OnError,
			})

			for _, action := range task.Actions {
				actionNode := statemachine.NewActionNode(
					action.Path,
					taskNode,
					action.Status,
				)

				taskNode.AddActionNode(actionNode)
			}

			stageNode.AddTaskNode(taskNode)
		}

		for _, nestedPipeline := range stage.NestedPipelines {
			nestedPipelineNode := statemachine.NewPipelineNode(&statemachine.NewPipelineNodeInput{
				Path:         nestedPipeline.Path,
				Status:       nestedPipeline.Status,
				Parent:       stageNode,
				Dependencies: nestedPipeline.Dependencies,
				OnError:      nestedPipeline.OnError,
			})
			stageNode.AddNestedPipelineNode(nestedPipelineNode)
		}

		pipelineNode.AddStageNode(stageNode)
	}

	return statemachine.New(pipelineNode)
}

func (p *Pipeline) syncStateMachine(stateMachine *statemachine.StateMachine) {
	pipelineNode := stateMachine.GetPipelineNode()

	// Sync state machine with pipeline
	p.Status = pipelineNode.Status()
	for stageIndex, stage := range pipelineNode.ChildrenNodes() {
		p.Stages[stageIndex].Status = stage.Status()

		var taskIndex, pipelineIndex int
		for _, node := range stage.ChildrenNodes() {
			if node.Type() == statemachine.PipelineNodeType {
				p.Stages[stageIndex].NestedPipelines[pipelineIndex].Status = node.Status()
				pipelineIndex++
			} else {
				p.Stages[stageIndex].Tasks[taskIndex].Status = node.Status()
				for actionIndex, action := range node.ChildrenNodes() {
					p.Stages[stageIndex].Tasks[taskIndex].Actions[actionIndex].Status = action.Status()
				}
				taskIndex++
			}
		}
	}
}
