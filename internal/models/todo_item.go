package models

import "slices"

// ToDoItemTargetType represents the target type of a TODO item.
type ToDoItemTargetType string

// ToDoItemTargetType constants.
const (
	PipelineApprovalTarget ToDoItemTargetType = "PIPELINE_APPROVAL"
	TaskApprovalTarget     ToDoItemTargetType = "TASK_APPROVAL"
)

// ToDoItem represents a TODO item resource.
type ToDoItem struct {
	Payload         []byte
	UserIDs         []string
	TeamIDs         []string
	ResolvedByUsers []string
	ProjectID       *string
	OrganizationID  string
	TargetID        string
	TargetType      ToDoItemTargetType
	Metadata        ResourceMetadata
	AutoResolved    bool
	Resolvable      bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (t *ToDoItem) ResolveMetadata(key string) (string, error) {
	val, err := t.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "target_type":
			val = string(t.TargetType)
		default:
			return "", err
		}
	}

	return val, nil
}

// IsResolved returns true if the item was resolved.
func (t *ToDoItem) IsResolved(userID string) bool {
	return t.AutoResolved || slices.Contains(t.ResolvedByUsers, userID)
}

// ToDoItemPipelineTaskApprovalPayload represents the payload of a pipeline task approval item.
type ToDoItemPipelineTaskApprovalPayload struct {
	TaskPath string `json:"taskPath"`
}
