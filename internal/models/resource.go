package models

import (
	"fmt"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	// PRNPrefix is the prefix for all PRNs.
	PRNPrefix = "prn:"
)

// ResourceType is an enum representing a Phobos resource type.
type ResourceType string

// ResourceType constants.
const (
	OrganizationResource         ResourceType = "organization"
	MembershipResource           ResourceType = "membership"
	ProjectResource              ResourceType = "project"
	ProjectVariableSetResource   ResourceType = "project_variable_set"
	ProjectVariableResource      ResourceType = "project_variable"
	JobResource                  ResourceType = "job"
	TeamResource                 ResourceType = "team"
	UserResource                 ResourceType = "user"
	PipelineTemplateResource     ResourceType = "pipeline_template"
	PipelineResource             ResourceType = "pipeline"
	AgentResource                ResourceType = "agent"
	AgentSessionResource         ResourceType = "agent_session"
	EnvironmentResource          ResourceType = "environment"
	ServiceAccountResource       ResourceType = "service_account"
	RoleResource                 ResourceType = "role"
	TeamMemberResource           ResourceType = "team_member"
	ResourceLimitResource        ResourceType = "resource_limit"
	SCIMTokenResource            ResourceType = "scim_token"
	LogStreamResource            ResourceType = "log_stream"
	PipelineActionOutputResource ResourceType = "pipeline_action_output"
	PipelineApprovalResource     ResourceType = "pipeline_approval"
	ApprovalRuleResource         ResourceType = "approval_rule"
	LifecycleTemplateResource    ResourceType = "lifecycle_template"
	ReleaseLifecycleResource     ResourceType = "release_lifecycle"
	ReleaseResource              ResourceType = "release"
	VariableResource             ResourceType = "variable"
	ActivityEventResource        ResourceType = "activity_event"
	DeploymentResource           ResourceType = "deployment"
	CommentResource              ResourceType = "comment"
	PluginResource               ResourceType = "plugin"
	PluginVersionResource        ResourceType = "plugin_version"
	PluginPlatformResource       ResourceType = "plugin_platform"
	ToDoItemResource             ResourceType = "todo_item"
	VCSProviderResource          ResourceType = "vcs_provider"
	ThreadResource               ResourceType = "thread"
	EnvironmentRuleResource      ResourceType = "environment_rule"
	MetricResource               ResourceType = "metric"
)

// ResourcePathFromPRN returns the resource path from a PRN.
func (r ResourceType) ResourcePathFromPRN(v string) (string, error) {
	if !strings.HasPrefix(v, PRNPrefix) {
		return "", errors.New("not a PRN", errors.WithErrorCode(errors.EInvalid))
	}

	parts := strings.Split(v[len(PRNPrefix):], ":")

	if len(parts) != 2 {
		return "", errors.New("invalid PRN format", errors.WithErrorCode(errors.EInvalid))
	}

	if ResourceType(parts[0]) != r {
		return "", errors.New("invalid PRN resource type", errors.WithErrorCode(errors.EInvalid))
	}

	resourcePath := parts[1]

	if resourcePath == "" || strings.HasPrefix(resourcePath, "/") || strings.HasSuffix(resourcePath, "/") {
		return "", errors.New("invalid PRN resource path", errors.WithErrorCode(errors.EInvalid))
	}

	return resourcePath, nil
}

// BuildPRN builds a PRN from a resource type and a list of path parts.
func (r ResourceType) BuildPRN(a ...string) string {
	return fmt.Sprintf("%s%s:%s", PRNPrefix, r, strings.Join(a, "/"))
}

// ScopeType defines an enum representing the level at which a resource is available.
type ScopeType string

// Scope constants
const (
	GlobalScope       ScopeType = "GLOBAL"
	OrganizationScope ScopeType = "ORGANIZATION"
	ProjectScope      ScopeType = "PROJECT"
)

// Equals returns true if both scopes are equal.
func (s ScopeType) Equals(o ScopeType) bool {
	return s == o
}
