package models

// Membership represents an association between a member and an organization or project.
type Membership struct {
	UserID           *string
	TeamID           *string
	ServiceAccountID *string
	ProjectID        *string
	OrganizationID   *string
	RoleID           string
	Scope            ScopeType
	Metadata         ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (m *Membership) ResolveMetadata(key string) (string, error) {
	return m.Metadata.resolveFieldValue(key)
}
