package models

import (
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// maxCommentTextLength is the maximum length for a comment's text field.
const maxCommentTextLength int = 2000

// Comment represents a Phobos comment.
type Comment struct {
	ThreadID         string
	UserID           *string
	ServiceAccountID *string
	Text             string
	Metadata         ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (c *Comment) ResolveMetadata(key string) (string, error) {
	return c.Metadata.resolveFieldValue(key)
}

// Validate validates the model.
func (c *Comment) Validate() error {

	// Verify that the text is not too long.
	if len(c.Text) > maxCommentTextLength {
		return errors.New(
			"invalid comment text, cannot be greater than %d characters",
			maxCommentTextLength,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	return nil
}

// Thread represents a collection of comments.
type Thread struct {
	PipelineID *string
	ReleaseID  *string
	Metadata   ResourceMetadata
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (t *Thread) ResolveMetadata(key string) (string, error) {
	return t.Metadata.resolveFieldValue(key)
}
