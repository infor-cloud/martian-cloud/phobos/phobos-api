package models

// PipelineActionOutput defines an output for a pipeline action
type PipelineActionOutput struct {
	Name       string
	PipelineID string
	ActionPath string
	Metadata   ResourceMetadata
	Value      []byte
	Type       []byte
}
