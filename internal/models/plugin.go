package models

import (
	"net/url"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// Plugin represents a Phobos plugin often used to interface with
// external services or extend Phobos functionality.
type Plugin struct {
	CreatedBy      string
	Name           string
	OrganizationID string
	RepositoryURL  string
	Metadata       ResourceMetadata
	Private        bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (p *Plugin) ResolveMetadata(key string) (string, error) {
	val, err := p.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "name":
			val = p.Name
		default:
			return "", err
		}
	}

	return val, nil
}

// Validate returns an error if the model is not valid
func (p *Plugin) Validate() error {
	if err := verifyValidName(p.Name); err != nil {
		return err
	}

	if p.RepositoryURL != "" {
		if _, err := url.ParseRequestURI(p.RepositoryURL); err != nil {
			return errors.Wrap(err, "Invalid repository URL", errors.WithErrorCode(errors.EInvalid))
		}
	}

	return nil
}

// GetOrgName returns the org name
func (p *Plugin) GetOrgName() string {
	resourcePath := strings.Split(p.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[0]
}
