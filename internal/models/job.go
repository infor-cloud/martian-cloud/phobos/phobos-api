package models

import (
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/dustin/go-humanize"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	// DefaultMaxJobDuration is the default max job duration
	DefaultMaxJobDuration = time.Hour * 12
	// MaxAllowedJobDuration is the max allowed job duration
	MaxAllowedJobDuration = time.Hour * 24
)

// jobForceCancelWaitPeriod is how long a job must be soft-canceled
// before it is allowed to be forcefully canceled.
const jobForceCancelWaitPeriod = 1 * time.Minute

// JobStatus type
type JobStatus string

// Job Status Constants
const (
	JobQueued    JobStatus = "queued"
	JobPending   JobStatus = "pending"
	JobRunning   JobStatus = "running"
	JobFailed    JobStatus = "failed"
	JobSucceeded JobStatus = "succeeded"
	JobCanceled  JobStatus = "canceled"
	JobCanceling JobStatus = "canceling"
)

// IsFinishedStatus returns true if the job is finished (failed, succeeded, or canceled)
func (s JobStatus) IsFinishedStatus() bool {
	return s == JobFailed || s == JobSucceeded || s == JobCanceled
}

// JobType indicates the type of job
type JobType string

// Job Types Constants
const (
	JobTaskType JobType = "task"
)

// JobTimestamps includes the timestamp for each job state change
type JobTimestamps struct {
	QueuedTimestamp   *time.Time
	PendingTimestamp  *time.Time
	RunningTimestamp  *time.Time
	FinishedTimestamp *time.Time
}

// JobTaskData is the data for a job of type task
type JobTaskData struct {
	PipelineID string `json:"pipelineId"`
	TaskPath   string `json:"taskPath"`
}

// Job represents a unit of work that is completed as part of a pipeline or lifecycle
type Job struct {
	Timestamps               JobTimestamps
	Data                     interface{}
	AgentType                *string
	ForceCanceledBy          *string
	AgentID                  *string
	Image                    *string // Custom image to use for the job
	CancelRequestedTimestamp *time.Time
	AgentName                *string
	status                   JobStatus
	Type                     JobType
	ProjectID                string
	Metadata                 ResourceMetadata
	Tags                     []string
	MaxJobDuration           int32
	ForceCanceled            bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (j *Job) ResolveMetadata(key string) (string, error) {
	val, err := j.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "cancel_requested_at":
			if j.CancelRequestedTimestamp != nil {
				val = j.CancelRequestedTimestamp.Format(time.RFC3339Nano)
			}
		default:
			return "", err
		}
	}

	return val, nil
}

// GetForceCancelAvailableAt returns the time when this job can be forcefully canceled
func (j *Job) GetForceCancelAvailableAt() time.Time {
	return j.CancelRequestedTimestamp.Add(jobForceCancelWaitPeriod)
}

// GetStatus returns the status of the job
func (j *Job) GetStatus() JobStatus {
	return j.status
}

// SetStatus sets the status of the job
func (j *Job) SetStatus(status JobStatus) error {
	if j.status == status {
		return nil
	}

	if j.status == "" {
		// This is the first time the status is being set.
		j.status = status
		return nil
	}

	transitionValid := false

	switch j.status {
	case JobQueued:
		transitionValid = (status == JobPending) || (status == JobCanceled)
	case JobPending:
		transitionValid = (status == JobRunning) || (status == JobCanceling)
	case JobRunning:
		transitionValid = (status == JobSucceeded) || (status == JobFailed) || (status == JobCanceling)
	case JobCanceling:
		transitionValid = (status == JobCanceled) || (status == JobFailed) || (status == JobSucceeded)
	}

	if !transitionValid {
		return errors.New("invalid job status transition: %s -> %s", j.status, status, errors.WithErrorCode(errors.EInvalid))
	}

	// Update the status
	j.status = status

	return nil
}

// GracefullyCancel gracefully cancels the job
func (j *Job) GracefullyCancel() error {
	if j.status == JobCanceling {
		return errors.New("job cancellation is already in progress", errors.WithErrorCode(errors.EInvalid))
	}

	now := time.Now().UTC()

	newStatus := JobCanceling
	if j.status == JobQueued {
		newStatus = JobCanceled
		j.Timestamps.FinishedTimestamp = &now
	}

	if err := j.SetStatus(newStatus); err != nil {
		return err
	}

	j.CancelRequestedTimestamp = &now

	return nil
}

// ForceCancel forcefully cancels the job
func (j *Job) ForceCancel(canceledBy string) error {
	if j.CancelRequestedTimestamp == nil {
		return errors.New(
			"a graceful cancellation attempt must be made before a forceful cancellation is available",
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	// Get the time when this job can be force canceled.
	forceCancelAvailableAt := j.GetForceCancelAvailableAt()

	if time.Now().UTC().Before(forceCancelAvailableAt) {
		return errors.New("graceful job cancellation is still in progress; force cancellation will be available %s",
			humanize.Time(forceCancelAvailableAt),
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	if err := j.SetStatus(JobCanceled); err != nil {
		return err
	}

	j.ForceCanceled = true
	j.ForceCanceledBy = &canceledBy
	j.Timestamps.FinishedTimestamp = ptr.Time(time.Now().UTC())

	return nil
}

// GetOrgName returns the org name for the job
func (j *Job) GetOrgName() string {
	resourcePath := strings.Split(j.Metadata.PRN, ":")[2]
	return strings.Split(resourcePath, "/")[0]
}
