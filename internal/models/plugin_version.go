package models

import (
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	// MaxPluginVersionDocFiles is the maximum number of documentation files allowed for a plugin version.
	MaxPluginVersionDocFiles = 100
)

// PluginVersionDocCategory represents the category of a plugin version documentation file.
type PluginVersionDocCategory string

// PluginVersionDocCategory constants
const (
	PluginVersionDocCategoryOverview PluginVersionDocCategory = "OVERVIEW"
	PluginVersionDocCategoryAction   PluginVersionDocCategory = "ACTION"
	PluginVersionDocCategoryGuide    PluginVersionDocCategory = "GUIDE"
)

// PluginVersionDocFileMetadata represents the metadata of a plugin version documentation file.
type PluginVersionDocFileMetadata struct {
	Title       string                   `json:"title"`
	Category    PluginVersionDocCategory `json:"category"`
	Subcategory string                   `json:"subcategory"`
	Name        string                   `json:"name"`
	FilePath    string                   `json:"file_path"`
}

// Validate validates the plugin version documentation file metadata.
func (p *PluginVersionDocFileMetadata) Validate() error {
	if p.Title == "" {
		return errors.New("title is required", errors.WithErrorCode(errors.EInvalid))
	}

	if p.Name == "" {
		return errors.New("name is required", errors.WithErrorCode(errors.EInvalid))
	}

	switch p.Category {
	case PluginVersionDocCategoryOverview:
		if p.Subcategory != "" {
			return errors.New("subcategory is not allowed for category OVERVIEW", errors.WithErrorCode(errors.EInvalid))
		}
		if p.Name != "index" {
			return errors.New("name must be 'index' for category OVERVIEW", errors.WithErrorCode(errors.EInvalid))
		}
	case PluginVersionDocCategoryAction, PluginVersionDocCategoryGuide:
	default:
		return errors.New("invalid category", errors.WithErrorCode(errors.EInvalid))
	}

	return verifyValidName(p.Name)
}

// PluginVersion represents a version of a Phobos plugin.
type PluginVersion struct {
	DocFiles        map[string]*PluginVersionDocFileMetadata
	CreatedBy       string
	PluginID        string
	SemanticVersion string
	Metadata        ResourceMetadata
	Protocols       []string
	SHASumsUploaded bool
	ReadmeUploaded  bool
	SchemaUploaded  bool
	Latest          bool
}

// ResolveMetadata resolves the metadata fields for cursor-based pagination
func (p *PluginVersion) ResolveMetadata(key string) (string, error) {
	val, err := p.Metadata.resolveFieldValue(key)
	if err != nil {
		switch key {
		case "semantic_version":
			val = p.SemanticVersion
		default:
			return "", err
		}
	}

	return val, nil
}

// Validate checks if the plugin version is valid.
func (p *PluginVersion) Validate() error {
	if err := plugin.AreProtocolsSupported(p.Protocols); err != nil {
		return errors.Wrap(err, "failed to validate protocol", errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}
