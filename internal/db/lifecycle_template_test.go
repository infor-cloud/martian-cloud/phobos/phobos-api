//go:build integration

package db

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (os LifecycleTemplateSortableField) getValue() string {
	return string(os)
}

func TestGetLifecycleTemplateByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	lifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode         errors.CodeType
		name                    string
		searchID                string
		expectLifecycleTemplate bool
	}

	/*
		test case template

		type testCase struct {
			name                    string
			searchID                string
			expectErrorCode         errors.CodeType
			expectLifecycleTemplate bool
		}
	*/

	testCases := []testCase{
		{
			name:                    "positive",
			searchID:                lifecycleTemplate.Metadata.ID,
			expectLifecycleTemplate: true,
		},
		{
			name:     "negative, non-existent ID",
			searchID: nonExistentID,
			// expect lifecycle template and error to be nil
		},
		{
			name:                    "defective-id",
			searchID:                invalidID,
			expectLifecycleTemplate: false,
			expectErrorCode:         errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			lifecycleTemplate, err := testClient.client.LifecycleTemplates.
				GetLifecycleTemplateByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectLifecycleTemplate {
				require.NotNil(t, lifecycleTemplate)
				assert.Equal(t, test.searchID, lifecycleTemplate.Metadata.ID)
			} else {
				assert.Nil(t, lifecycleTemplate)
			}
		})
	}
}

func TestGetLifecycleTemplateByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		OrgID: org.Metadata.ID,
		Name:  "test-proj",
	})
	require.Nil(t, err)

	orgLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
	})
	require.Nil(t, err)

	projLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &proj.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode         errors.CodeType
		name                    string
		searchPRN               string
		expectLifecycleTemplate bool
	}

	/*
		test case template

		{
			name                   string
			searchPRN              string
			expectErrorCode        errors.CodeType
			expectLifecycleTemplate bool
		}
	*/

	testCases := []testCase{
		{
			name:                    "positive-org",
			searchPRN:               orgLifecycleTemplate.Metadata.PRN,
			expectLifecycleTemplate: true,
		},
		{
			name:                    "positive-proj",
			searchPRN:               projLifecycleTemplate.Metadata.PRN,
			expectLifecycleTemplate: true,
		},
		{
			name:      "negative, lifecycle template does not exist",
			searchPRN: models.LifecycleTemplateResource.BuildPRN("non-existent-org", nonExistentGlobalID),
			// expect lifecycle template and error to be nil
		},
		{
			name:            "defective-prn",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			lifecycleTemplate, err := testClient.client.LifecycleTemplates.GetLifecycleTemplateByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectLifecycleTemplate {
				require.NotNil(t, lifecycleTemplate)
				assert.Equal(t, test.searchPRN, lifecycleTemplate.Metadata.PRN)
			} else {
				assert.Nil(t, lifecycleTemplate)
			}
		})
	}
}

func TestGetLifecycleTemplatesWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
			Scope:          models.OrganizationScope,
			OrganizationID: org.Metadata.ID,
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		LifecycleTemplateSortableFieldUpdatedAtAsc,
		LifecycleTemplateSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := LifecycleTemplateSortableField(sortByField.getValue())

		result, err := testClient.client.LifecycleTemplates.GetLifecycleTemplates(ctx, &GetLifecycleTemplatesInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.LifecycleTemplates {
			resources = append(resources, resource)
		}

		return result.PageInfo, resources, nil
	})
}

func TestGetLifecycleTemplates(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	org2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org2",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	proj1b, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	lifecycleTemplates := make([]*models.LifecycleTemplate, 10)
	for i := 0; i < len(lifecycleTemplates); i++ {
		// a mix of org-scope, proj1a, and proj1b
		var scope models.ScopeType
		var projectID *string
		switch i % 3 {
		case 0: // i=0, 3, 6, 9
			scope = models.OrganizationScope
		case 1: // i=1, 4, 7
			scope = models.ProjectScope
			projectID = &proj1a.Metadata.ID
		case 2: // i=2, 5, 8
			scope = models.ProjectScope
			projectID = &proj1b.Metadata.ID
		}

		lifecycleTemplate, aErr := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx,
			&models.LifecycleTemplate{
				Scope:          scope,
				OrganizationID: org1.Metadata.ID,
				ProjectID:      projectID,
			})
		require.Nil(t, aErr)

		lifecycleTemplates[i] = lifecycleTemplate
	}

	lifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx,
		&models.LifecycleTemplate{
			Scope:          models.OrganizationScope,
			OrganizationID: org2.Metadata.ID,
		})
	require.Nil(t, err)

	lifecycleTemplates = append(lifecycleTemplates, lifecycleTemplate)

	type testCase struct {
		filter            *LifecycleTemplateFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	/*
		test case template

		type testCase struct {
			name              string
			filter            *LifecycleTemplateFilter
			expectResultCount int
			expectErrorCode   errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name:              "filter nil",
			filter:            nil,
			expectResultCount: 11,
		},

		{
			name:              "filter empty",
			filter:            &LifecycleTemplateFilter{},
			expectResultCount: 11,
		},

		{
			name: "filter, empty slice of lifecycle template IDs",
			filter: &LifecycleTemplateFilter{
				LifecycleTemplateIDs: []string{},
			},
			expectResultCount: 0,
		},

		{
			name: "filter, lifecycle template IDs",
			filter: &LifecycleTemplateFilter{
				LifecycleTemplateIDs: []string{lifecycleTemplates[2].Metadata.ID, lifecycleTemplates[4].Metadata.ID},
			},
			expectResultCount: 2,
		},

		{
			name: "filter, lifecycle template IDs, non-existent",
			filter: &LifecycleTemplateFilter{
				LifecycleTemplateIDs: []string{nonExistentID},
			},
			expectResultCount: 0,
		},

		{
			name: "filter, lifecycle template IDs, invalid",
			filter: &LifecycleTemplateFilter{
				LifecycleTemplateIDs: []string{invalidID},
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "filter, empty organization ID",
			filter: &LifecycleTemplateFilter{
				OrganizationID: nil,
			},
			expectResultCount: 11,
		},

		{
			name: "filter, valid organization ID",
			filter: &LifecycleTemplateFilter{
				OrganizationID: &org1.Metadata.ID,
			},
			expectResultCount: 10,
		},

		{
			name: "filter, non-existent organization ID",
			filter: &LifecycleTemplateFilter{
				OrganizationID: ptr.String(nonExistentID), // & operator does not work here, because it's an untyped string
			},
			expectResultCount: 0,
		},

		{
			name: "filter, invalid organization ID",
			filter: &LifecycleTemplateFilter{
				OrganizationID: ptr.String(invalidID), // & operator does not work here, because it's an untyped string
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "filter, empty project ID",
			filter: &LifecycleTemplateFilter{
				ProjectID: nil,
			},
			expectResultCount: 11,
		},

		{
			name: "filter, valid project ID",
			filter: &LifecycleTemplateFilter{
				ProjectID: &proj1a.Metadata.ID,
			},
			expectResultCount: 7,
		},

		{
			name: "filter, non-existent project ID",
			filter: &LifecycleTemplateFilter{
				ProjectID: ptr.String(nonExistentID), // & operator does not work here, because it's an untyped string
			},
			expectResultCount: 0,
		},

		{
			name: "filter, invalid project ID",
			filter: &LifecycleTemplateFilter{
				ProjectID: ptr.String(invalidID), // & operator does not work here, because it's an untyped string
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "filter, empty scopes",
			filter: &LifecycleTemplateFilter{
				LifecycleTemplateScopes: []models.ScopeType{},
			},
			expectResultCount: 0,
		},

		{
			name: "filter, org scope, otherwise unrestricted",
			filter: &LifecycleTemplateFilter{
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectResultCount: 5,
		},

		{
			name: "filter, proj scope, otherwise unrestricted",
			filter: &LifecycleTemplateFilter{
				LifecycleTemplateScopes: []models.ScopeType{models.ProjectScope},
			},
			expectResultCount: 6,
		},

		{
			name: "filter, org1, org scope only",
			filter: &LifecycleTemplateFilter{
				OrganizationID:          &org1.Metadata.ID,
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectResultCount: 4,
		},

		{
			name: "filter, org1, proj scope only",
			filter: &LifecycleTemplateFilter{
				OrganizationID:          &org1.Metadata.ID,
				LifecycleTemplateScopes: []models.ScopeType{models.ProjectScope},
			},
			expectResultCount: 6,
		},

		{
			name: "filter, proj1a, org scope only",
			filter: &LifecycleTemplateFilter{
				ProjectID:               &proj1a.Metadata.ID,
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectResultCount: 4,
		},

		{
			name: "filter, proj1a, proj scope only",
			filter: &LifecycleTemplateFilter{
				ProjectID:               &proj1a.Metadata.ID,
				LifecycleTemplateScopes: []models.ScopeType{models.ProjectScope},
			},
			expectResultCount: 3,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			lifecycleTemplatesResult, err := testClient.client.LifecycleTemplates.
				GetLifecycleTemplates(ctx, &GetLifecycleTemplatesInput{
					Sort: ptrLifecycleTemplateSortableField(LifecycleTemplateSortableFieldUpdatedAtAsc),
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(100),
					},
					Filter: test.filter,
				})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			// If there was no error, check the results.
			if err == nil {

				// Never returns nil if error is nil.
				require.NotNil(t, lifecycleTemplatesResult.PageInfo)
				assert.NotNil(t, lifecycleTemplatesResult.LifecycleTemplates)
			}
		})
	}
}

func TestCreateLifecycleTemplate(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		toCreate        *models.LifecycleTemplate
		expectCreated   *models.LifecycleTemplate
		expectErrorCode errors.CodeType
		name            string
	}

	/*
		test case template

		type testCase struct {
			name            string
			toCreate        *models.LifecycleTemplate
			expectErrorCode errors.CodeType
			expectCreated   *models.LifecycleTemplate
		}
	*/

	testCases := []testCase{
		{
			name: "positive, org lifecycle template",
			toCreate: &models.LifecycleTemplate{
				Scope:          models.OrganizationScope,
				OrganizationID: org1.Metadata.ID,
			},
			expectCreated: &models.LifecycleTemplate{
				Scope:          models.OrganizationScope,
				OrganizationID: org1.Metadata.ID,
			},
		},
		{
			name: "positive, proj lifecycle template",
			toCreate: &models.LifecycleTemplate{
				Scope:          models.ProjectScope,
				OrganizationID: org1.Metadata.ID,
				ProjectID:      &proj1a.Metadata.ID,
			},
			expectCreated: &models.LifecycleTemplate{
				Scope:          models.ProjectScope,
				OrganizationID: org1.Metadata.ID,
				ProjectID:      &proj1a.Metadata.ID,
			},
		},

		// There is currently no negative duplicate case, because there is no name or similar unique field.

	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			// For a positive case, just use the utility function.
			actualCreated, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, test.toCreate)

			if test.expectErrorCode != "" {
				require.NotNil(t, err)
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
			} else {
				require.Nil(t, err)
			}

			if test.expectErrorCode == "" {
				compareLifecycleTemplatesCreate(t, test.expectCreated, actualCreated)
			}
		})
	}
}

func TestUpdateLifecycleTemplate(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		OrgID: org.Metadata.ID,
		Name:  "test-proj",
	})
	require.Nil(t, err)

	orgLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	projLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &proj.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	type testCase struct {
		findLifecycleTemplate *models.LifecycleTemplate
		newStatus             models.LifecycleTemplateStatus
		name                  string
		expectErrorCode       errors.CodeType
		isPositive            bool
	}

	/*
		test case template

		{
			name                  string
			isPositive            bool
			findLifecycleTemplate *models.LifecycleTemplate
			newStatus             models.LifecycleTemplateStatus
			expectErrorCode       errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name: "positive, org",
			findLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID:                   orgLifecycleTemplate.Metadata.ID,
					Version:              orgLifecycleTemplate.Metadata.Version,
					CreationTimestamp:    orgLifecycleTemplate.Metadata.CreationTimestamp,
					LastUpdatedTimestamp: orgLifecycleTemplate.Metadata.LastUpdatedTimestamp,
				},
				OrganizationID: orgLifecycleTemplate.OrganizationID,
				CreatedBy:      orgLifecycleTemplate.CreatedBy,
				Status:         orgLifecycleTemplate.Status,
			},
			newStatus:  models.LifecycleTemplateUploaded,
			isPositive: true,
		},
		{
			name: "positive, proj",
			findLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID:                   projLifecycleTemplate.Metadata.ID,
					Version:              projLifecycleTemplate.Metadata.Version,
					CreationTimestamp:    projLifecycleTemplate.Metadata.CreationTimestamp,
					LastUpdatedTimestamp: projLifecycleTemplate.Metadata.LastUpdatedTimestamp,
				},
				OrganizationID: projLifecycleTemplate.OrganizationID,
				CreatedBy:      projLifecycleTemplate.CreatedBy,
				Status:         projLifecycleTemplate.Status,
			},
			newStatus:  models.LifecycleTemplateUploaded,
			isPositive: true,
		},
		{
			name: "negative, not exist",
			findLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EOptimisticLock,
		},
		{
			name: "negative, invalid uuid",
			findLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			originalLifecycleTemplate := test.findLifecycleTemplate

			modifiedLifecycleTemplate := &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID:                   originalLifecycleTemplate.Metadata.ID,
					Version:              originalLifecycleTemplate.Metadata.Version,
					CreationTimestamp:    originalLifecycleTemplate.Metadata.CreationTimestamp,
					LastUpdatedTimestamp: originalLifecycleTemplate.Metadata.LastUpdatedTimestamp,
				},
				OrganizationID: originalLifecycleTemplate.OrganizationID,
				CreatedBy:      originalLifecycleTemplate.CreatedBy,
				Status:         test.newStatus,
			}

			claimedUpdatedLifecycleTemplate, err := testClient.client.LifecycleTemplates.
				UpdateLifecycleTemplate(ctx, modifiedLifecycleTemplate)

			assert.Equal(t, (test.expectErrorCode == ""), (err == nil))
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.isPositive {

				require.NotNil(t, claimedUpdatedLifecycleTemplate)

				compareLifecycleTemplatesUpdate(t, modifiedLifecycleTemplate, claimedUpdatedLifecycleTemplate)

				retrieved, err := testClient.client.LifecycleTemplates.GetLifecycleTemplateByID(ctx, originalLifecycleTemplate.Metadata.ID)
				require.Nil(t, err)

				require.NotNil(t, retrieved)

				compareLifecycleTemplatesUpdate(t, modifiedLifecycleTemplate, retrieved)
			}
		})
	}
}

func ptrLifecycleTemplateSortableField(arg LifecycleTemplateSortableField) *LifecycleTemplateSortableField {
	return &arg
}

// compareLifecycleTemplatesCreate compares two lifecycle templates for TestCreateLifecycleTemplate
// There are some fields that cannot be compared, or DeepEqual would work.
func compareLifecycleTemplatesCreate(t *testing.T, expected, actual *models.LifecycleTemplate) {
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
	assert.Equal(t, expected.OrganizationID, actual.OrganizationID)
	assert.Equal(t, expected.Status, actual.Status)
}

// compareLifecycleTemplatesUpdate compares two lifecycle templates for TestUpdateLifecycleTemplate
func compareLifecycleTemplatesUpdate(t *testing.T, expected, actual *models.LifecycleTemplate) {
	assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	assert.Equal(t, expected.Metadata.CreationTimestamp, actual.Metadata.CreationTimestamp)
	assert.NotEqual(t, expected.Metadata.Version, actual.Metadata.Version)
	assert.NotEqual(t, expected.Metadata.LastUpdatedTimestamp, actual.Metadata.LastUpdatedTimestamp)

	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
	assert.Equal(t, expected.OrganizationID, actual.OrganizationID)
	assert.Equal(t, expected.Status, actual.Status)
}
