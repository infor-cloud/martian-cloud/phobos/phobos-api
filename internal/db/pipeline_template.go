package db

//go:generate go tool mockery --name PipelineTemplates --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// PipelineTemplates encapsulates the logic to access PipelineTemplates from the database
type PipelineTemplates interface {
	GetPipelineTemplateByID(ctx context.Context, id string) (*models.PipelineTemplate, error)
	GetPipelineTemplateByPRN(ctx context.Context, prn string) (*models.PipelineTemplate, error)
	GetPipelineTemplates(ctx context.Context, input *GetPipelineTemplatesInput) (*PipelineTemplatesResult, error)
	CreatePipelineTemplate(ctx context.Context, pipelineTemplate *models.PipelineTemplate) (*models.PipelineTemplate, error)
	UpdatePipelineTemplate(ctx context.Context, pipelineTemplate *models.PipelineTemplate) (*models.PipelineTemplate, error)
	DeletePipelineTemplate(ctx context.Context, pipelineTemplate *models.PipelineTemplate) error
}

// PipelineTemplateFilter contains the supported fields for filtering PipelineTemplate resources
type PipelineTemplateFilter struct {
	TimeRangeStart      *time.Time
	ProjectID           *string
	Versioned           *bool
	Latest              *bool
	Name                *string
	Search              *string
	PipelineTemplateIDs []string
}

// PipelineTemplateSortableField represents the fields that a pipeline template can be sorted by
type PipelineTemplateSortableField string

// PipelineTemplateSortableField constants
const (
	PipelineTemplateSortableFieldUpdatedAtAsc  PipelineTemplateSortableField = "UPDATED_AT_ASC"
	PipelineTemplateSortableFieldUpdatedAtDesc PipelineTemplateSortableField = "UPDATED_AT_DESC"
)

func (os PipelineTemplateSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case PipelineTemplateSortableFieldUpdatedAtAsc, PipelineTemplateSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "pipeline_templates", Col: "updated_at"}
	default:
		return nil
	}
}

func (os PipelineTemplateSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetPipelineTemplatesInput is the input for listing pipeline templates
type GetPipelineTemplatesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *PipelineTemplateSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *PipelineTemplateFilter
}

// validate validates the input for GetPipelineTemplatesInput
func (i *GetPipelineTemplatesInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// PipelineTemplatesResult contains the response data and page information
type PipelineTemplatesResult struct {
	PageInfo          *pagination.PageInfo
	PipelineTemplates []models.PipelineTemplate
}

var pipelineTemplateFieldList = append(metadataFieldList, "created_by", "project_id", "status",
	"versioned", "name", "semver", "latest")

type pipelineTemplates struct {
	dbClient *Client
}

// NewPipelineTemplates returns an instance of the PipelineTemplates interface
func NewPipelineTemplates(dbClient *Client) PipelineTemplates {
	return &pipelineTemplates{dbClient: dbClient}
}

func (p *pipelineTemplates) GetPipelineTemplateByID(ctx context.Context, id string) (*models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineTemplateByID")
	defer span.End()

	return p.getPipelineTemplate(ctx, goqu.Ex{"pipeline_templates.id": id})
}

func (p *pipelineTemplates) GetPipelineTemplateByPRN(ctx context.Context, prn string) (*models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineTemplateByPRN")
	defer span.End()

	path, err := models.PipelineTemplateResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	var expr exp.Ex

	switch len(parts) {
	case 3:
		// Non-versioned:
		expr = goqu.Ex{
			"organizations.name":    parts[0],
			"projects.name":         parts[1],
			"pipeline_templates.id": gid.FromGlobalID(parts[2]),
		}
	case 4:
		// Versioned:
		expr = goqu.Ex{
			"organizations.name":        parts[0],
			"projects.name":             parts[1],
			"pipeline_templates.name":   parts[2],
			"pipeline_templates.semver": parts[3],
		}
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getPipelineTemplate(ctx, expr)
}

func (p *pipelineTemplates) GetPipelineTemplates(ctx context.Context, input *GetPipelineTemplatesInput) (*PipelineTemplatesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineTemplates")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()
	selectEx := dialect.From(goqu.T("pipeline_templates")).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipeline_templates.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")}))

	if input.Filter != nil {
		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("pipeline_templates.project_id").Eq(*input.Filter.ProjectID))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.PipelineTemplateIDs) > 0 {
			ex = ex.Append(goqu.I("pipeline_templates.id").In(input.Filter.PipelineTemplateIDs))
		}

		if input.Filter.Versioned != nil {
			ex = ex.Append(goqu.I("pipeline_templates.versioned").Eq(*input.Filter.Versioned))
		}

		if input.Filter.Name != nil && *input.Filter.Name != "" {
			ex = ex.Append(goqu.I("pipeline_templates.name").Eq(*input.Filter.Name))
		}

		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("pipeline_templates.name").ILike("%" + *input.Filter.Search + "%"))
		}

		if input.Filter.Latest != nil {
			ex = ex.Append(goqu.I("pipeline_templates.latest").Eq(*input.Filter.Latest))
		}

		if input.Filter.TimeRangeStart != nil {
			// Must use UTC here otherwise, queries will return unexpected results.
			ex = ex.Append(goqu.I("pipeline_templates.created_at").Gte(input.Filter.TimeRangeStart.UTC()))
		}
	}

	query := selectEx.Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "pipeline_templates", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.PipelineTemplate{}
	for rows.Next() {
		item, err := scanPipelineTemplate(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &PipelineTemplatesResult{
		PageInfo:          rows.GetPageInfo(),
		PipelineTemplates: results,
	}

	return result, nil
}

func (p *pipelineTemplates) CreatePipelineTemplate(ctx context.Context, pipelineTemplate *models.PipelineTemplate) (*models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.CreatePipelineTemplate")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("pipeline_templates").
		Prepared(true).
		With("pipeline_templates",
			dialect.Insert("pipeline_templates").Rows(
				goqu.Record{
					"id":         newResourceID(),
					"version":    initialResourceVersion,
					"created_at": timestamp,
					"updated_at": timestamp,
					"created_by": pipelineTemplate.CreatedBy,
					"project_id": pipelineTemplate.ProjectID,
					"status":     pipelineTemplate.Status,
					"versioned":  pipelineTemplate.Versioned,
					"name":       pipelineTemplate.Name,
					"semver":     pipelineTemplate.SemanticVersion,
					"latest":     pipelineTemplate.Latest,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipeline_templates.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdPipelineTemplate, err := scanPipelineTemplate(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("pipeline template already exists", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_pipeline_template_project_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdPipelineTemplate, nil
}

func (p *pipelineTemplates) UpdatePipelineTemplate(ctx context.Context, pipelineTemplate *models.PipelineTemplate) (*models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.UpdatePipelineTemplate")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("pipeline_templates").
		Prepared(true).
		With("pipeline_templates",
			dialect.Update("pipeline_templates").
				Set(goqu.Record{
					"version":    goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at": timestamp,
					"status":     pipelineTemplate.Status,
					"latest":     pipelineTemplate.Latest,
				}).
				Where(goqu.Ex{"id": pipelineTemplate.Metadata.ID, "version": pipelineTemplate.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipeline_templates.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedPipelineTemplate, err := scanPipelineTemplate(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedPipelineTemplate, nil
}

func (p *pipelineTemplates) DeletePipelineTemplate(ctx context.Context, pipelineTemplate *models.PipelineTemplate) error {

	ctx, span := tracer.Start(ctx, "db.DeletePipelineTemplate")
	defer span.End()

	sql, args, err := dialect.From("pipeline_templates").
		Prepared(true).
		With("pipeline_templates",
			dialect.Delete("pipeline_templates").
				Where(goqu.Ex{"id": pipelineTemplate.Metadata.ID, "version": pipelineTemplate.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipeline_templates.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanPipelineTemplate(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_pipelines_pipeline_template_id":
					return errors.New(
						"template %q cannot be deleted because it's in use by a pipeline",
						pipelineTemplate.Metadata.PRN,
						errors.WithErrorCode(errors.EConflict),
						errors.WithSpan(span),
					)
				}
			}
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *pipelineTemplates) getPipelineTemplate(ctx context.Context, exp exp.Expression) (*models.PipelineTemplate, error) {
	query := dialect.From(goqu.T("pipeline_templates")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipeline_templates.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	pipelineTemplate, err := scanPipelineTemplate(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return pipelineTemplate, nil
}

func (*pipelineTemplates) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range pipelineTemplateFieldList {
		selectFields = append(selectFields, fmt.Sprintf("pipeline_templates.%s", field))
	}

	selectFields = append(selectFields, "projects.name")
	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanPipelineTemplate(row scanner) (*models.PipelineTemplate, error) {
	var projectName, organizationName string
	pipelineTemplate := &models.PipelineTemplate{}

	fields := []interface{}{
		&pipelineTemplate.Metadata.ID,
		&pipelineTemplate.Metadata.CreationTimestamp,
		&pipelineTemplate.Metadata.LastUpdatedTimestamp,
		&pipelineTemplate.Metadata.Version,
		&pipelineTemplate.CreatedBy,
		&pipelineTemplate.ProjectID,
		&pipelineTemplate.Status,
		&pipelineTemplate.Versioned,
		&pipelineTemplate.Name,
		&pipelineTemplate.SemanticVersion,
		&pipelineTemplate.Latest,
		&projectName,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	// The PRN differs in form between versions and non-versioned pipeline templates.
	if pipelineTemplate.Versioned {
		// For versioned pipeline templates: prn:pipeline_template:<org>/<proj>/<template_name>/<semanticVersion>
		pipelineTemplate.Metadata.PRN = models.PipelineTemplateResource.BuildPRN(
			organizationName,
			projectName,
			*pipelineTemplate.Name,
			*pipelineTemplate.SemanticVersion,
		)
	} else {
		// For non-versioned pipeline templates: prn:pipeline_template:<org>/<proj>/<GID>
		pipelineTemplate.Metadata.PRN = models.PipelineTemplateResource.BuildPRN(
			organizationName,
			projectName,
			gid.ToGlobalID(gid.PipelineTemplateType, pipelineTemplate.Metadata.ID),
		)
	}

	return pipelineTemplate, nil
}
