//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (pv ProjectVariableSortableField) getValue() string {
	return string(pv)
}

func TestGetProjectVariableByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	projectVariable, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
		Key:              "foo",
		Value:            "bar",
		PipelineType:     models.DeploymentPipelineType,
		EnvironmentScope: "",
		CreatedBy:        "test-user",
		ProjectID:        project.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode       errors.CodeType
		name                  string
		id                    string
		expectProjectVariable bool
	}

	testCases := []testCase{
		{
			name:                  "get resource by id",
			id:                    projectVariable.Metadata.ID,
			expectProjectVariable: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			projectVariable, err := testClient.client.ProjectVariables.GetProjectVariableByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProjectVariable {
				require.NotNil(t, projectVariable)
				assert.Equal(t, test.id, projectVariable.Metadata.ID)
			} else {
				assert.Nil(t, projectVariable)
			}
		})
	}
}

func TestGetProjectVariableByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	projectVariable, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
		Key:              "foo",
		Value:            "bar",
		PipelineType:     models.DeploymentPipelineType,
		EnvironmentScope: "",
		CreatedBy:        "test-user",
		ProjectID:        project.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode       errors.CodeType
		name                  string
		prn                   string
		expectProjectVariable bool
	}

	testCases := []testCase{
		{
			name:                  "get resource by PRN",
			prn:                   projectVariable.Metadata.PRN,
			expectProjectVariable: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.ProjectVariableResource.BuildPRN(nonExistentID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			projectVariable, err := testClient.client.ProjectVariables.GetProjectVariableByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProjectVariable {
				require.NotNil(t, projectVariable)
				assert.Equal(t, test.prn, projectVariable.Metadata.PRN)
			} else {
				assert.Nil(t, projectVariable)
			}
		})
	}
}

func TestCreateProjectVariable(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		projectID       string
	}

	testCases := []testCase{
		{
			name:      "successfully create resource",
			projectID: project.Metadata.ID,
		},
		{
			name:            "non-existent project ID",
			projectID:       nonExistentID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			projectVariable, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
				ProjectID:        test.projectID,
				Key:              "foo",
				Value:            "bar",
				PipelineType:     models.DeploymentPipelineType,
				EnvironmentScope: "",
				CreatedBy:        "test-user",
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, projectVariable)
		})
	}
}

func TestDeleteProjectVariable(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	projectVariable, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
		Key:              "foo",
		Value:            "bar",
		PipelineType:     models.DeploymentPipelineType,
		EnvironmentScope: "",
		CreatedBy:        "test-user",
		ProjectID:        project.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              projectVariable.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      projectVariable.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.ProjectVariables.DeleteProjectVariable(ctx, &models.ProjectVariable{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestGetProjectVariables(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	deploymentPipelineType := models.DeploymentPipelineType

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	proj1, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	proj2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	variableSet, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
		ProjectID: proj1.Metadata.ID,
		Revision:  "1",
		Latest:    true,
	})
	require.Nil(t, err)

	v1, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
		Key:              "foo",
		Value:            "bar",
		PipelineType:     models.DeploymentPipelineType,
		EnvironmentScope: models.ProjectVariableEnvironmentScopeAll,
		CreatedBy:        "test-user",
		ProjectID:        proj1.Metadata.ID,
	})
	require.Nil(t, err)

	v2, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
		Key:              "foo",
		Value:            "bar",
		PipelineType:     models.DeploymentPipelineType,
		EnvironmentScope: models.ProjectVariableEnvironmentScopeAll,
		CreatedBy:        "test-user",
		ProjectID:        proj2.Metadata.ID,
	})
	require.Nil(t, err)

	err = testClient.client.ProjectVariableSets.AddProjectVariablesToSet(ctx, variableSet, []*models.ProjectVariable{v1})
	require.Nil(t, err)

	type testCase struct {
		filter          *ProjectVariableFilter
		name            string
		expectErrorCode errors.CodeType
		expectVariables []*models.ProjectVariable
	}

	testCases := []testCase{
		{
			name: "return all variables in project 1",
			filter: &ProjectVariableFilter{
				ProjectID: &proj1.Metadata.ID,
			},
			expectVariables: []*models.ProjectVariable{v1},
		},
		{
			name: "return all variables in project 2",
			filter: &ProjectVariableFilter{
				ProjectID: &proj2.Metadata.ID,
			},
			expectVariables: []*models.ProjectVariable{v2},
		},
		{
			name: "return all variables matching specific IDs",
			filter: &ProjectVariableFilter{
				ProjectVariableIDs: []string{v1.Metadata.ID, v2.Metadata.ID},
			},
			expectVariables: []*models.ProjectVariable{v1, v2},
		},
		{
			name: "return all variables matching a combination of filters",
			filter: &ProjectVariableFilter{
				ProjectVariableIDs:   []string{v1.Metadata.ID, v2.Metadata.ID},
				ProjectID:            &proj1.Metadata.ID,
				ProjectVariableSetID: &variableSet.Metadata.ID,
				PipelineType:         &deploymentPipelineType,
				VariableKey:          ptr.String("foo"),
				EnvironmentScopes:    []string{models.ProjectVariableEnvironmentScopeAll},
			},
			expectVariables: []*models.ProjectVariable{v1},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.ProjectVariables.GetProjectVariables(ctx, &GetProjectVariablesInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.Equal(t, len(test.expectVariables), len(result.ProjectVariables))

			// Build map of expected variables for easy lookup
			expectedVariables := map[string]*models.ProjectVariable{}
			for _, v := range test.expectVariables {
				expectedVariables[v.FQN()] = v
			}

			// Check that all expected variables are present
			for _, v := range result.ProjectVariables {
				expected, ok := expectedVariables[v.FQN()]
				assert.True(t, ok)
				assert.Equal(t, expected.Value, v.Value)
			}
		})
	}
}

func TestGetProjectVariablesWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
			Key:              fmt.Sprintf("foo-%d", i),
			Value:            "bar",
			PipelineType:     models.DeploymentPipelineType,
			EnvironmentScope: "",
			CreatedBy:        "test-user",
			ProjectID:        project.Metadata.ID,
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		ProjectVariableSortableFieldCreatedAtAsc,
		ProjectVariableSortableFieldCreatedAtDesc,
		ProjectVariableSortableFieldKeyAsc,
		ProjectVariableSortableFieldKeyDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := ProjectVariableSortableField(sortByField.getValue())

		result, err := testClient.client.ProjectVariables.GetProjectVariables(ctx, &GetProjectVariablesInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.ProjectVariables {
			resources = append(resources, resource)
		}

		return result.PageInfo, resources, nil
	})
}
