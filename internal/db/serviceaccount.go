package db

//go:generate go tool mockery --name ServiceAccounts --inpackage --case underscore

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ServiceAccounts encapsulates the logic to access service accounts from the database
type ServiceAccounts interface {
	GetServiceAccountByID(ctx context.Context, id string) (*models.ServiceAccount, error)
	GetServiceAccountByPRN(ctx context.Context, prn string) (*models.ServiceAccount, error)
	GetServiceAccounts(ctx context.Context, input *GetServiceAccountsInput) (*ServiceAccountsResult, error)
	CreateServiceAccount(ctx context.Context, serviceAccount *models.ServiceAccount) (*models.ServiceAccount, error)
	UpdateServiceAccount(ctx context.Context, serviceAccount *models.ServiceAccount) (*models.ServiceAccount, error)
	DeleteServiceAccount(ctx context.Context, serviceAccount *models.ServiceAccount) error
	AssignServiceAccountToAgent(ctx context.Context, serviceAccountID string, agentID string) error
	UnassignServiceAccountFromAgent(ctx context.Context, serviceAccountID string, agentID string) error
}

// ServiceAccountSortableField represents the fields that a service account can be sorted by
type ServiceAccountSortableField string

// ServiceAccountSortableField constants
const (
	ServiceAccountSortableFieldCreatedAtAsc  ServiceAccountSortableField = "CREATED_AT_ASC"
	ServiceAccountSortableFieldCreatedAtDesc ServiceAccountSortableField = "CREATED_AT_DESC"
	ServiceAccountSortableFieldUpdatedAtAsc  ServiceAccountSortableField = "UPDATED_AT_ASC"
	ServiceAccountSortableFieldUpdatedAtDesc ServiceAccountSortableField = "UPDATED_AT_DESC"
)

func (sf ServiceAccountSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case ServiceAccountSortableFieldCreatedAtAsc, ServiceAccountSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "service_accounts", Col: "created_at"}
	case ServiceAccountSortableFieldUpdatedAtAsc, ServiceAccountSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "service_accounts", Col: "updated_at"}
	default:
		return nil
	}
}

func (sf ServiceAccountSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// ServiceAccountFilter contains the supported fields for filtering ServiceAccount resources
type ServiceAccountFilter struct {
	Search               *string
	AgentID              *string
	OrganizationID       *string
	ProjectID            *string
	ServiceAccountScopes []models.ScopeType
	ServiceAccountIDs    []string
}

// oidcTrustPolicyDBType is the type used to store the trust policies in the DB table
type oidcTrustPolicyDBType struct {
	BoundClaimsType models.BoundClaimsType `json:"boundClaimsType"`
	BoundClaims     map[string]string      `json:"boundClaims"`
	Issuer          string                 `json:"issuer"`
}

// GetServiceAccountsInput is the input for listing service accounts
type GetServiceAccountsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ServiceAccountSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ServiceAccountFilter
}

// validate validates the input for GetServiceAccountsInput
func (i *GetServiceAccountsInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// ServiceAccountsResult contains the response data and page information
type ServiceAccountsResult struct {
	PageInfo        *pagination.PageInfo
	ServiceAccounts []*models.ServiceAccount
}

type serviceAccounts struct {
	dbClient *Client
}

var serviceAccountFieldList = append(metadataFieldList,
	"name", "description", "organization_id", "created_by", "oidc_trust_policies", "scope", "project_id")

// NewServiceAccounts returns an instance of the ServiceAccount interface
func NewServiceAccounts(dbClient *Client) ServiceAccounts {
	return &serviceAccounts{dbClient: dbClient}
}

func (s *serviceAccounts) GetServiceAccountByID(ctx context.Context, id string) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "db.GetServiceAccountByID")
	defer span.End()

	return s.getServiceAccount(ctx, goqu.Ex{"service_accounts.id": id})
}

func (s *serviceAccounts) GetServiceAccountByPRN(ctx context.Context, prn string) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "db.GetServiceAccountByPRN")
	defer span.End()

	path, err := models.ServiceAccountResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	ex := goqu.Ex{}

	switch len(parts) {
	case 1:
		// This is a global service account name.
		ex["service_accounts.name"] = parts[0]
		ex["service_accounts.scope"] = models.GlobalScope
	case 2:
		// This is an org/service account name.
		ex["organizations.name"] = parts[0]
		ex["service_accounts.name"] = parts[1]
		ex["service_accounts.scope"] = models.OrganizationScope
	case 3:
		// This is a project-scoped service account path.
		ex["organizations.name"] = parts[0]
		ex["projects.name"] = parts[1]
		ex["service_accounts.name"] = parts[2]
		ex["service_accounts.scope"] = models.ProjectScope
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return s.getServiceAccount(ctx, ex)
}

func (s *serviceAccounts) GetServiceAccounts(ctx context.Context, input *GetServiceAccountsInput) (*ServiceAccountsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetServiceAccounts")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if len(input.Filter.ServiceAccountIDs) > 0 {
			ex = ex.Append(goqu.I("service_accounts.id").In(input.Filter.ServiceAccountIDs))
		}

		if input.Filter.AgentID != nil {
			ex = ex.Append(goqu.I("service_account_agent_relation.agent_id").In(*input.Filter.AgentID))
		}

		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("service_accounts.organization_id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(
				// Use an OR condition to get both organization and project service accounts.
				goqu.Or(
					goqu.I("service_accounts.project_id").Eq(*input.Filter.ProjectID),
					goqu.And(
						goqu.I("service_accounts.organization_id").In(
							dialect.From("projects").
								Select("org_id").
								Where(goqu.I("id").Eq(*input.Filter.ProjectID)),
						),
						goqu.I("service_accounts.scope").Eq(models.OrganizationScope),
					),
				),
			)
		}

		if len(input.Filter.ServiceAccountScopes) > 0 {
			ex = ex.Append(goqu.I("service_accounts.scope").In(input.Filter.ServiceAccountScopes))
		}

		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("service_accounts.name").ILike("%" + *input.Filter.Search + "%"))
		}
	}

	query := dialect.From("service_accounts").
		Select(s.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"service_accounts.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"service_accounts.project_id": goqu.I("projects.id")}))

	if input.Filter != nil && input.Filter.AgentID != nil {
		// Add inner join for agent relation table
		query = query.InnerJoin(goqu.T("service_account_agent_relation"), goqu.On(goqu.Ex{"service_accounts.id": goqu.I("service_account_agent_relation.service_account_id")}))
	}

	query = query.Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "service_accounts", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, s.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.ServiceAccount{}
	for rows.Next() {
		item, err := scanServiceAccount(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := ServiceAccountsResult{
		PageInfo:        rows.GetPageInfo(),
		ServiceAccounts: results,
	}

	return &result, nil
}

func (s *serviceAccounts) CreateServiceAccount(ctx context.Context, serviceAccount *models.ServiceAccount) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "db.CreateServiceAccount")
	defer span.End()

	timestamp := currentTime()

	trustPoliciesJSON, err := s.marshalOIDCTrustPolicies(serviceAccount.OIDCTrustPolicies)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal OIDC trust policies", errors.WithSpan(span))
	}

	sql, args, err := dialect.From("service_accounts").
		Prepared(true).
		With("service_accounts",
			dialect.Insert("service_accounts").Rows(
				goqu.Record{
					"id":                  newResourceID(),
					"version":             initialResourceVersion,
					"created_at":          timestamp,
					"updated_at":          timestamp,
					"name":                serviceAccount.Name,
					"description":         serviceAccount.Description,
					"organization_id":     serviceAccount.OrganizationID,
					"created_by":          serviceAccount.CreatedBy,
					"oidc_trust_policies": trustPoliciesJSON,
					"scope":               serviceAccount.Scope,
					"project_id":          serviceAccount.ProjectID,
				}).Returning("*"),
		).Select(s.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"service_accounts.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"service_accounts.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdServiceAccount, err := scanServiceAccount(s.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("Service account already exists", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) && pgErr.ConstraintName == "fk_organization_id" {
				return nil, errors.New("organization does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) && pgErr.ConstraintName == "fk_service_account_project_id" {
				return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdServiceAccount, nil
}

func (s *serviceAccounts) UpdateServiceAccount(ctx context.Context, serviceAccount *models.ServiceAccount) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateServiceAccount")
	defer span.End()

	trustPoliciesJSON, err := s.marshalOIDCTrustPolicies(serviceAccount.OIDCTrustPolicies)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal OIDC trust policies", errors.WithSpan(span))
	}

	timestamp := currentTime()

	sql, args, err := dialect.From("service_accounts").
		Prepared(true).
		With("service_accounts",
			dialect.Update("service_accounts").
				Set(goqu.Record{
					"version":             goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":          timestamp,
					"description":         serviceAccount.Description,
					"oidc_trust_policies": trustPoliciesJSON,
				}).
				Where(goqu.Ex{"id": serviceAccount.Metadata.ID, "version": serviceAccount.Metadata.Version}).
				Returning("*"),
		).Select(s.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"service_accounts.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"service_accounts.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedServiceAccount, err := scanServiceAccount(s.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedServiceAccount, nil
}

func (s *serviceAccounts) DeleteServiceAccount(ctx context.Context, serviceAccount *models.ServiceAccount) error {
	ctx, span := tracer.Start(ctx, "db.DeleteServiceAccount")
	defer span.End()

	sql, args, err := dialect.From("service_accounts").
		Prepared(true).
		With("service_accounts",
			dialect.Delete("service_accounts").
				Where(goqu.Ex{"id": serviceAccount.Metadata.ID, "version": serviceAccount.Metadata.Version}).
				Returning("*"),
		).Select(s.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"service_accounts.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"service_accounts.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanServiceAccount(s.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				return errors.New(
					"Service account %s is assigned as a member of an organization or project", serviceAccount.Name,
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EConflict),
				)
			}
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (s *serviceAccounts) getServiceAccount(ctx context.Context, exp exp.Ex) (*models.ServiceAccount, error) {
	sql, args, err := dialect.From("service_accounts").
		Prepared(true).
		Select(s.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"service_accounts.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"service_accounts.project_id": goqu.I("projects.id")})).
		Where(exp).
		ToSQL()
	if err != nil {
		return nil, err
	}

	serviceAccount, err := scanServiceAccount(s.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return serviceAccount, nil
}

func (s *serviceAccounts) AssignServiceAccountToAgent(ctx context.Context, serviceAccountID string, agentID string) error {
	ctx, span := tracer.Start(ctx, "db.AssignServiceAccountToAgent")
	defer span.End()

	sql, args, err := dialect.Insert("service_account_agent_relation").
		Prepared(true).
		Rows(goqu.Record{
			"service_account_id": serviceAccountID,
			"agent_id":           agentID,
		}).ToSQL()

	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = s.dbClient.getConnection(ctx).Exec(ctx, sql, args...); err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return errors.New("service account is already assigned to agent", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (s *serviceAccounts) UnassignServiceAccountFromAgent(ctx context.Context, serviceAccountID string, agentID string) error {
	ctx, span := tracer.Start(ctx, "db.UnassignServiceAccountFromAgent")
	defer span.End()

	sql, args, err := dialect.Delete("service_account_agent_relation").
		Prepared(true).
		Where(
			goqu.Ex{
				"service_account_id": serviceAccountID,
				"agent_id":           agentID,
			},
		).ToSQL()

	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = s.dbClient.getConnection(ctx).Exec(ctx, sql, args...); err != nil {
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (s *serviceAccounts) marshalOIDCTrustPolicies(input []models.OIDCTrustPolicy) ([]byte, error) {
	policies := []oidcTrustPolicyDBType{}
	for _, p := range input {
		policies = append(policies, oidcTrustPolicyDBType{
			Issuer:          p.Issuer,
			BoundClaimsType: p.BoundClaimsType,
			BoundClaims:     p.BoundClaims,
		})
	}
	trustPoliciesJSON, err := json.Marshal(policies)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal trust policies to JSON %v", err)
	}

	return trustPoliciesJSON, nil
}

func (*serviceAccounts) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range serviceAccountFieldList {
		selectFields = append(selectFields, fmt.Sprintf("service_accounts.%s", field))
	}

	selectFields = append(selectFields, "organizations.name", "projects.name")

	return selectFields
}

func scanServiceAccount(row scanner) (*models.ServiceAccount, error) {
	var organizationName, projectName sql.NullString
	serviceAccount := &models.ServiceAccount{}

	policies := []oidcTrustPolicyDBType{}

	fields := []interface{}{
		&serviceAccount.Metadata.ID,
		&serviceAccount.Metadata.CreationTimestamp,
		&serviceAccount.Metadata.LastUpdatedTimestamp,
		&serviceAccount.Metadata.Version,
		&serviceAccount.Name,
		&serviceAccount.Description,
		&serviceAccount.OrganizationID,
		&serviceAccount.CreatedBy,
		&policies,
		&serviceAccount.Scope,
		&serviceAccount.ProjectID,
		&organizationName,
		&projectName,
	}

	err := row.Scan(fields...)
	if err != nil {
		return nil, err
	}

	serviceAccount.OIDCTrustPolicies = []models.OIDCTrustPolicy{}
	for _, p := range policies {
		// Default bound claims type to string if it's not defined
		boundClaimsType := p.BoundClaimsType
		if boundClaimsType == "" {
			boundClaimsType = models.BoundClaimsTypeString
		}
		serviceAccount.OIDCTrustPolicies = append(serviceAccount.OIDCTrustPolicies, models.OIDCTrustPolicy{
			Issuer:          p.Issuer,
			BoundClaimsType: boundClaimsType,
			BoundClaims:     p.BoundClaims,
		})
	}

	switch serviceAccount.Scope {
	case models.GlobalScope:
		serviceAccount.Metadata.PRN = models.ServiceAccountResource.BuildPRN(serviceAccount.Name)
	case models.OrganizationScope:
		serviceAccount.Metadata.PRN = models.ServiceAccountResource.BuildPRN(organizationName.String, serviceAccount.Name)
	case models.ProjectScope:
		serviceAccount.Metadata.PRN = models.ServiceAccountResource.BuildPRN(organizationName.String, projectName.String, serviceAccount.Name)
	default:
		return nil, fmt.Errorf("unexpected service account scope: %s", serviceAccount.Scope)
	}

	return serviceAccount, nil
}
