package db

//go:generate go tool mockery --name Transactions --inpackage --case underscore

import (
	"context"

	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// Transactions exposes DB transaction support
type Transactions interface {
	BeginTx(ctx context.Context) (context.Context, error)
	CommitTx(ctx context.Context) error
	RollbackTx(ctx context.Context) error
}

type transactions struct {
	dbClient *Client
}

// NewTransactions returns an instance of the Transactions interface
func NewTransactions(dbClient *Client) Transactions {
	return &transactions{dbClient: dbClient}
}

// BeginTx starts a new transaction which gets added to the returned context
func (t *transactions) BeginTx(ctx context.Context) (context.Context, error) {
	var tx pgx.Tx
	var err error

	ctx, span := tracer.Start(ctx, "BeginTx")
	defer span.End()

	parentTx, ok := ctx.Value(txKey).(pgx.Tx)
	if !ok {
		// Transaction doesn't exist yet so create a new one
		span.AddEvent("Creating a new transaction")
		tx, err = t.dbClient.conn.Begin(ctx)
		if err != nil {
			return nil, errors.Wrap(err, "failed to create a new transaction", errors.WithSpan(span))
		}
	} else {
		// Parent transaction already exists so create a child transaction
		span.AddEvent("Creating a child transaction")
		tx, err = parentTx.Begin(ctx)
		if err != nil {
			return nil, errors.Wrap(err, "failed to create a child transaction", errors.WithSpan(span))
		}
	}

	return context.WithValue(ctx, txKey, tx), nil
}

// CommitTx commits the transaction that is on the current context
func (t *transactions) CommitTx(ctx context.Context) error {
	ctx, span := tracer.Start(ctx, "CommitTx")
	defer span.End()

	tx, ok := ctx.Value(txKey).(pgx.Tx)
	if !ok {
		return errors.New("transaction missing from commit context", errors.WithSpan(span))
	}
	return tx.Commit(ctx)
}

// RollbackTx rolls back the transaction that is on the current context
func (t *transactions) RollbackTx(ctx context.Context) error {
	ctx, span := tracer.Start(ctx, "RollbackTx")
	defer span.End()

	tx, ok := ctx.Value(txKey).(pgx.Tx)
	if !ok {
		return errors.New("transaction missing from rollback context", errors.WithSpan(span))
	}

	err := tx.Rollback(ctx)

	// Avoid throwing unnecessary errors in caller.
	if err == pgx.ErrTxClosed {
		return nil
	}

	return err
}
