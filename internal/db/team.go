package db

//go:generate go tool mockery --name Teams --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/trace"
)

// Teams encapsulates the logic to access teams from the database
type Teams interface {
	GetTeamBySCIMExternalID(ctx context.Context, scimExternalID string) (*models.Team, error)
	GetTeamByID(ctx context.Context, id string) (*models.Team, error)
	GetTeamByPRN(ctx context.Context, prn string) (*models.Team, error)
	GetTeams(ctx context.Context, input *GetTeamsInput) (*TeamsResult, error)
	CreateTeam(ctx context.Context, team *models.Team) (*models.Team, error)
	UpdateTeam(ctx context.Context, team *models.Team) (*models.Team, error)
	DeleteTeam(ctx context.Context, team *models.Team) error
}

// TeamSortableField represents the fields that a team can be sorted by
type TeamSortableField string

// TeamSortableField constants
const (
	TeamSortableFieldNameAsc       TeamSortableField = "NAME_ASC"
	TeamSortableFieldNameDesc      TeamSortableField = "NAME_DESC"
	TeamSortableFieldUpdatedAtAsc  TeamSortableField = "UPDATED_AT_ASC"
	TeamSortableFieldUpdatedAtDesc TeamSortableField = "UPDATED_AT_DESC"
)

func (ts TeamSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch ts {
	case TeamSortableFieldNameAsc, TeamSortableFieldNameDesc:
		return &pagination.FieldDescriptor{Key: "name", Table: "teams", Col: "name"}
	case TeamSortableFieldUpdatedAtAsc, TeamSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "teams", Col: "updated_at"}
	default:
		return nil
	}
}

func (ts TeamSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(ts), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// TeamFilter contains the supported fields for filtering Team resources
type TeamFilter struct {
	Search         *string
	UserID         *string
	TeamIDs        []string
	TeamNames      []string
	SCIMExternalID bool
}

// GetTeamsInput is the input for listing teams
type GetTeamsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *TeamSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *TeamFilter
}

// validate validates the input for GetTeamsInput
func (i *GetTeamsInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// TeamsResult contains the response data and page information
type TeamsResult struct {
	PageInfo *pagination.PageInfo
	Teams    []models.Team
}

type teams struct {
	dbClient *Client
}

var teamFieldList = append(metadataFieldList, "name", "description", "scim_external_id")

// NewTeams returns an instance of the Teams interface
func NewTeams(dbClient *Client) Teams {
	return &teams{dbClient: dbClient}
}

func (t *teams) GetTeamBySCIMExternalID(ctx context.Context, scimExternalID string) (*models.Team, error) {
	ctx, span := tracer.Start(ctx, "db.GetTeamBySCIMExternalID")
	defer span.End()

	return t.getTeam(ctx, span, goqu.Ex{"teams.scim_external_id": scimExternalID})
}

func (t *teams) GetTeamByID(ctx context.Context, id string) (*models.Team, error) {
	ctx, span := tracer.Start(ctx, "db.GetTeamByID")
	defer span.End()

	return t.getTeam(ctx, span, goqu.Ex{"teams.id": id})
}

func (t *teams) GetTeamByPRN(ctx context.Context, prn string) (*models.Team, error) {
	ctx, span := tracer.Start(ctx, "db.GetTeamByPRN")
	defer span.End()

	path, err := models.TeamResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return t.getTeam(ctx, span, goqu.Ex{"teams.name": path})
}

func (t *teams) GetTeams(ctx context.Context, input *GetTeamsInput) (*TeamsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetTeams")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if len(input.Filter.TeamIDs) > 0 {
			ex = ex.Append(goqu.I("teams.id").In(input.Filter.TeamIDs))
		}
		if len(input.Filter.TeamNames) > 0 {
			ex = ex.Append(goqu.I("teams.name").In(input.Filter.TeamNames))
		}
		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("teams.name").ILike("%" + *input.Filter.Search + "%"))
		}
		if input.Filter.UserID != nil {
			ex = ex.Append(goqu.I("teams.id").In(
				dialect.From("team_members").
					Select("team_id").
					Where(goqu.I("team_members.user_id").Eq(*input.Filter.UserID)),
			))
		}
		if input.Filter.SCIMExternalID {
			ex = ex.Append(goqu.I("teams.scim_external_id").IsNotNull())
		}
	}

	query := dialect.From(goqu.T("teams")).
		Select(t.getSelectFields()...).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "teams", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, t.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Team{}
	for rows.Next() {
		item, err := scanTeam(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := TeamsResult{
		PageInfo: rows.GetPageInfo(),
		Teams:    results,
	}

	return &result, nil
}

func (t *teams) CreateTeam(ctx context.Context, team *models.Team) (*models.Team, error) {
	ctx, span := tracer.Start(ctx, "db.CreateTeam")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Insert("teams").
		Prepared(true).
		Rows(goqu.Record{
			"id":               newResourceID(),
			"version":          initialResourceVersion,
			"created_at":       timestamp,
			"updated_at":       timestamp,
			"name":             team.Name,
			"description":      team.Description,
			"scim_external_id": team.SCIMExternalID,
		}).
		Returning(teamFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdTeam, err := scanTeam(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("team with name %s already exists", team.Name, errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdTeam, nil
}

func (t *teams) UpdateTeam(ctx context.Context, team *models.Team) (*models.Team, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateTeam")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Update("teams").
		Prepared(true).
		Set(
			goqu.Record{
				"version":          goqu.L("? + ?", goqu.C("version"), 1),
				"updated_at":       timestamp,
				"description":      team.Description,
				"scim_external_id": team.SCIMExternalID,
			},
		).Where(goqu.Ex{"id": team.Metadata.ID, "version": team.Metadata.Version}).Returning(teamFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedTeam, err := scanTeam(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedTeam, nil
}

func (t *teams) DeleteTeam(ctx context.Context, team *models.Team) error {
	ctx, span := tracer.Start(ctx, "db.DeleteTeam")
	defer span.End()

	sql, args, err := dialect.Delete("teams").
		Prepared(true).
		Where(
			goqu.Ex{
				"id":      team.Metadata.ID,
				"version": team.Metadata.Version,
			},
		).Returning(teamFieldList...).ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = scanTeam(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (t *teams) getTeam(ctx context.Context, span trace.Span, exp goqu.Ex) (*models.Team, error) {
	query := dialect.From(goqu.T("teams")).
		Prepared(true).
		Select(teamFieldList...).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to prepare get team query", errors.WithSpan(span))
	}

	team, err := scanTeam(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, "invalid ID; %s", pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, errors.Wrap(err, "failed to get team", errors.WithSpan(span))
	}

	return team, nil
}

func (t *teams) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range teamFieldList {
		selectFields = append(selectFields, fmt.Sprintf("teams.%s", field))
	}

	return selectFields
}

func scanTeam(row scanner) (*models.Team, error) {
	team := &models.Team{}

	fields := []interface{}{
		&team.Metadata.ID,
		&team.Metadata.CreationTimestamp,
		&team.Metadata.LastUpdatedTimestamp,
		&team.Metadata.Version,
		&team.Name,
		&team.Description,
		&team.SCIMExternalID,
	}

	err := row.Scan(fields...)
	if err != nil {
		return nil, err
	}

	team.Metadata.PRN = models.TeamResource.BuildPRN(team.Name)

	return team, nil
}
