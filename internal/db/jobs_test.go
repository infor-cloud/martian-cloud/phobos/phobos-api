//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestAddJobToPipelineTask(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		When:               models.AutoPipeline,
		Type:               models.RunbookPipelineType,
		Status:             statemachine.RunningNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.RunningNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.RunningNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	sampleTaskPath := "pipeline.stage.test.task.test"

	jobToCreate := &models.Job{
		ProjectID: project.Metadata.ID,
		Type:      models.JobTaskType,
		Data: &models.JobTaskData{
			PipelineID: pipeline.Metadata.ID,
			TaskPath:   sampleTaskPath,
		},
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobPending))

	job, aErr := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.Nil(t, aErr)

	type testCase struct {
		name            string
		pipelineID      string
		taskPath        string
		jobID           string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "add job to pipeline task",
			pipelineID: pipeline.Metadata.ID,
			taskPath:   sampleTaskPath,
			jobID:      job.Metadata.ID,
		},
		{
			name:            "add job to pipeline task with invalid pipeline id will return an error",
			pipelineID:      invalidID,
			taskPath:        sampleTaskPath,
			jobID:           job.Metadata.ID,
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "add job to pipeline task with invalid job id will return an error",
			pipelineID:      pipeline.Metadata.ID,
			taskPath:        sampleTaskPath,
			jobID:           invalidID,
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.Jobs.AddJobToPipelineTask(ctx, test.pipelineID, test.taskPath, &models.Job{
				Metadata: models.ResourceMetadata{
					ID: test.jobID,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestGetJobByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	jobToCreate := &models.Job{
		ProjectID:      project.Metadata.ID,
		Type:           models.JobTaskType,
		MaxJobDuration: int32(time.Duration(time.Minute * 60).Minutes()),
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobQueued))

	job, err := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		id              string
		expectJob       bool
	}

	testCases := []testCase{
		{
			name:      "get resource by id",
			id:        job.Metadata.ID,
			expectJob: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			job, err := testClient.client.Jobs.GetJobByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectJob {
				require.NotNil(t, job)
				assert.Equal(t, test.id, job.Metadata.ID)
			} else {
				assert.Nil(t, job)
			}
		})
	}
}

func TestGetJobByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	jobToCreate := &models.Job{
		ProjectID:      project.Metadata.ID,
		Type:           models.JobTaskType,
		MaxJobDuration: int32(time.Duration(time.Minute * 60).Minutes()),
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobQueued))

	job, err := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		prn             string
		expectJob       bool
	}

	testCases := []testCase{
		{
			name:      "get resource by PRN",
			prn:       job.Metadata.PRN,
			expectJob: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.JobResource.BuildPRN(nonExistentID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			job, err := testClient.client.Jobs.GetJobByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectJob {
				require.NotNil(t, job)
				assert.Equal(t, test.prn, job.Metadata.PRN)
			} else {
				assert.Nil(t, job)
			}
		})
	}
}

func TestGetJobs(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test",
		Type: models.SharedAgent,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		When:               models.AutoPipeline,
		Type:               models.RunbookPipelineType,
		Status:             statemachine.RunningNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.RunningNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.RunningNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	jobs := make([]*models.Job, 10)
	for i := 0; i < len(jobs); i++ {
		taskPath := fmt.Sprintf("pipeline.stage.test.task.test-%d", i)

		jobToCreate := &models.Job{
			ProjectID:      project.Metadata.ID,
			Type:           models.JobTaskType,
			MaxJobDuration: int32(time.Duration(time.Minute * 60).Minutes()),
			AgentID:        &agent.Metadata.ID,
			AgentName:      &agent.Name,
			AgentType:      ptrType(string(agent.Type)),
			Data: &models.JobTaskData{
				PipelineID: pipeline.Metadata.ID,
				TaskPath:   taskPath,
			},
		}

		require.NoError(t, jobToCreate.SetStatus(models.JobRunning))

		job, aErr := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
		require.Nil(t, aErr)

		require.NoError(t, testClient.client.Jobs.AddJobToPipelineTask(ctx, pipeline.Metadata.ID, taskPath, job))

		jobs[i] = job
	}

	jobToCreate := &models.Job{
		ProjectID:      project.Metadata.ID,
		Type:           models.JobTaskType,
		MaxJobDuration: int32(time.Duration(time.Minute * 60).Minutes()),
		AgentID:        &agent.Metadata.ID,
		AgentName:      &agent.Name,
		AgentType:      ptrType(string(agent.Type)),
		Data: &models.JobTaskData{
			PipelineID: pipeline.Metadata.ID,
			TaskPath:   "pipeline.stage.test.task.test",
		},
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobPending))

	job, aErr := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.Nil(t, aErr)

	require.NoError(t, testClient.client.Jobs.AddJobToPipelineTask(ctx, pipeline.Metadata.ID, "pipeline.stage.test.task.test", job))

	jobs = append(jobs, job)

	type testCase struct {
		filter            *JobFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all jobs",
			filter:            &JobFilter{},
			expectResultCount: len(jobs),
		},
		{
			name: "get jobs by project id",
			filter: &JobFilter{
				ProjectID: &project.Metadata.ID,
			},
			expectResultCount: len(jobs),
		},
		{
			name: "get jobs by agent id",
			filter: &JobFilter{
				AgentID: &agent.Metadata.ID,
			},
			expectResultCount: len(jobs),
		},
		{
			name: "get jobs by type",
			filter: &JobFilter{
				JobType: ptrType(models.JobTaskType),
			},
			expectResultCount: len(jobs),
		},
		{
			name: "get jobs by status",
			filter: &JobFilter{
				JobStatuses: []models.JobStatus{
					models.JobPending,
					models.JobRunning,
				},
			},
			expectResultCount: len(jobs),
		},
		{
			name: "get jobs by id",
			filter: &JobFilter{
				JobIDs: []string{
					jobs[0].Metadata.ID,
					jobs[1].Metadata.ID,
				},
			},
			expectResultCount: 2,
		},
		{
			name: "get job by pipeline id",
			filter: &JobFilter{
				PipelineID: &pipeline.Metadata.ID,
			},
			expectResultCount: len(jobs),
		},
		{
			name: "get job by pipeline task path",
			filter: &JobFilter{
				PipelineTaskPath: ptrType("pipeline.stage.test.task.test"),
			},
			expectResultCount: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.Jobs.GetJobs(ctx, &GetJobsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.Equal(t, test.expectResultCount, len(result.Jobs))
		})
	}
}

func TestGetJobsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		jobToCreate := &models.Job{
			ProjectID:                project.Metadata.ID,
			Type:                     models.JobTaskType,
			CancelRequestedTimestamp: ptrType(time.Now().UTC().Add(time.Duration(time.Minute * 60))),
		}

		require.NoError(t, jobToCreate.SetStatus(models.JobRunning))

		_, aErr := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
		require.Nil(t, aErr)
	}

	sortableFields := []sortableField{
		JobSortableFieldCreatedAtAsc,
		JobSortableFieldCreatedAtDesc,
		JobSortableFieldUpdatedAtAsc,
		JobSortableFieldUpdatedAtDesc,
		JobSortableFieldCancelRequestedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := JobSortableField(sortByField.getValue())

		result, err := testClient.client.Jobs.GetJobs(ctx, &GetJobsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.Jobs {
			resources = append(resources, &result.Jobs[ix])
		}

		return result.PageInfo, resources, nil
	})
}

func TestUpdateJob(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	jobToCreate := &models.Job{
		ProjectID: project.Metadata.ID,
		Type:      models.JobTaskType,
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobQueued))

	job, err := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		status          models.JobStatus
		version         int
	}

	testCases := []testCase{
		{
			name:    "successfully update resource",
			status:  models.JobRunning,
			version: 1,
		},
		{
			name:            "update will fail because resource version doesn't match",
			status:          models.JobRunning,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			toUpdate := &models.Job{
				Metadata: models.ResourceMetadata{
					ID:      job.Metadata.ID,
					Version: test.version,
				},
				ProjectID: project.Metadata.ID,
				Type:      models.JobTaskType,
			}

			require.NoError(t, toUpdate.SetStatus(test.status))

			actualJob, err := testClient.client.Jobs.UpdateJob(ctx, toUpdate)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, actualJob)
			assert.Equal(t, test.status, actualJob.GetStatus())
		})
	}
}

func TestCreateJob(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		projectID       string
	}

	testCases := []testCase{
		{
			name:      "successfully create resource",
			projectID: project.Metadata.ID,
		},
		{
			name:            "create will fail because project does not exist",
			projectID:       nonExistentID,
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			job, err := testClient.client.Jobs.CreateJob(ctx, &models.Job{
				ProjectID: test.projectID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, job)
		})
	}
}

// ptrType is a helper function to return a pointer to a value of type T
func ptrType[T comparable](t T) *T {
	return &t
}
