//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// getValue returns the value of the sortable field.
func (sf CommentSortableField) getValue() string {
	return string(sf)
}

func TestGetCommentByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	comment, err := testClient.client.Comments.CreateComment(ctx, &models.Comment{
		ThreadID: thread.Metadata.ID,
		Text:     "comment for testing",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		searchID        string
		expectComment   bool
	}

	/*
		test case template

		{
			name            string
			searchID        string
			expectErrorCode errors.CodeType
			expectComment   bool
		}
	*/

	testCases := []testCase{
		{
			name:          "get a comment by id",
			searchID:      comment.Metadata.ID,
			expectComment: true,
		},
		{
			name:     "negative, non-existent ID",
			searchID: nonExistentID,
			// expect comment and error to be nil
		},
		{
			name:            "defective-id",
			searchID:        invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			comment, err := testClient.client.Comments.GetCommentByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectComment {
				require.NotNil(t, comment)
				assert.Equal(t, test.searchID, comment.Metadata.ID)
			} else {
				assert.Nil(t, comment)
			}
		})
	}
}

func TestGetCommentByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	comment, err := testClient.client.Comments.CreateComment(ctx, &models.Comment{
		ThreadID: thread.Metadata.ID,
		Text:     "pipeline comment for testing",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		searchPRN       string
		expectComment   bool
	}

	/*
		test case template

		{
			name            string
			searchPRN       string
			expectErrorCode errors.CodeType
			expectComment   bool
		}
	*/

	testCases := []testCase{
		{
			name:          "get resource by prn",
			searchPRN:     comment.Metadata.PRN,
			expectComment: true,
		},
		{
			name:      "negative, resource does not exist",
			searchPRN: models.CommentResource.BuildPRN(nonExistentGlobalID),
			// expect comment and error to be nil
		},
		{
			name:            "defective-prn",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			comment, err := testClient.client.Comments.GetCommentByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectComment {
				require.NotNil(t, comment)
				assert.Equal(t, test.searchPRN, comment.Metadata.PRN)
			} else {
				assert.Nil(t, comment)
			}
		})
	}
}

func TestGetComments(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	totalComments := 10
	comments := make([]*models.Comment, 10)
	for i := 0; i < totalComments; i++ {
		comment, cErr := testClient.client.Comments.CreateComment(ctx, &models.Comment{
			ThreadID: thread.Metadata.ID,
			Text:     fmt.Sprintf("this is comment number %d", i),
		})
		require.Nil(t, cErr)

		comments[i] = comment
	}

	type testCase struct {
		name              string
		filter            *CommentFilter
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all comments",
			filter:            &CommentFilter{},
			expectResultCount: totalComments,
		},
		{
			name: "filter by thread id",
			filter: &CommentFilter{
				ThreadID: &thread.Metadata.ID,
			},
			expectResultCount: totalComments,
		},
		{
			name: "filter by unknown thread id",
			filter: &CommentFilter{
				ThreadID: ptr.String(nonExistentID),
			},
		},
		{
			name: "filter by text search",
			filter: &CommentFilter{
				Search: &comments[0].Text,
			},
			expectResultCount: 1,
		},
		{
			name: "filter by specific comment IDs",
			filter: &CommentFilter{
				CommentIDs: []string{comments[0].Metadata.ID, comments[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualResult, err := testClient.client.Comments.GetComments(ctx, &GetCommentsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			assert.Nil(t, err)
			assert.Len(t, actualResult.Comments, test.expectResultCount)
		})
	}
}

func TestGetCommentsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.Comments.CreateComment(ctx, &models.Comment{
			ThreadID: thread.Metadata.ID,
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		CommentSortableFieldCreatedAtAsc,
		CommentSortableFieldCreatedAtDesc,
		CommentSortableFieldUpdatedAtAsc,
		CommentSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields,
		func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (
			*pagination.PageInfo, []pagination.CursorPaginatable, error,
		) {
			sortBy := CommentSortableField(sortByField.getValue())

			result, err := testClient.client.Comments.GetComments(ctx, &GetCommentsInput{
				Sort:              &sortBy,
				PaginationOptions: paginationOptions,
			})
			if err != nil {
				return nil, nil, err
			}

			resources := []pagination.CursorPaginatable{}
			for ix := range result.Comments {
				resources = append(resources, &result.Comments[ix])
			}

			return result.PageInfo, resources, nil
		})
}

func TestCreateComment(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
	})
	require.Nil(t, err)

	serviceAccount, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		OrganizationID: &org.Metadata.ID,
		Name:           "test-service-account",
		Scope:          models.OrganizationScope,
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		input           *models.Comment
		expectErrorCode errors.CodeType
	}

	/*
		test case template

		{
		name            string
		input           *models.Comment
		expectErrorCode errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name: "user creates comment",
			input: &models.Comment{
				ThreadID: thread.Metadata.ID,
				UserID:   &user.Metadata.ID,
			},
		},
		{
			name: "service account creates comment",
			input: &models.Comment{
				ThreadID:         thread.Metadata.ID,
				ServiceAccountID: &serviceAccount.Metadata.ID,
			},
		},
		{
			name: "create fails because thread does not exist",
			input: &models.Comment{
				ThreadID: nonExistentID,
				UserID:   &user.Metadata.ID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "create fails because user does not exist",
			input: &models.Comment{
				ThreadID: thread.Metadata.ID,
				UserID:   ptr.String(nonExistentID),
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "create fails because service account does not exist",
			input: &models.Comment{
				ThreadID:         thread.Metadata.ID,
				ServiceAccountID: ptr.String(nonExistentID),
			},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			created, err := testClient.client.Comments.CreateComment(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			assert.NotNil(t, created)
		})
	}
}

func TestUpdateComment(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	original, err := testClient.client.Comments.CreateComment(ctx, &models.Comment{
		ThreadID: thread.Metadata.ID,
		Text:     "pipeline thread comment for testing",
	})
	require.Nil(t, err)

	newText := "updated comment for integration test"

	type testCase struct {
		name            string
		text            string
		expectErrorCode errors.CodeType
		version         int
	}

	/*
		test case template

		{
			name            string
			text 			string
			expectErrorCode errors.CodeType
			version 		int
		}
	*/

	testCases := []testCase{
		{
			name:    "successfully update resource",
			text:    newText,
			version: 1,
		},
		{
			name:            "update will fail because resource version doesn't match",
			text:            newText,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			updated, err := testClient.client.Comments.UpdateComment(ctx, &models.Comment{
				Metadata: models.ResourceMetadata{
					ID:      original.Metadata.ID,
					Version: test.version,
				},
				Text: test.text,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, updated)
			assert.Equal(t, test.text, updated.Text)
		})
	}
}

func TestDeleteComment(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	comment, err := testClient.client.Comments.CreateComment(ctx, &models.Comment{
		ThreadID: thread.Metadata.ID,
		Text:     "pipeline thread comment for testing",
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		version         int
	}

	/*
		test case template

		{
			name            string
			expectErrorCode errors.CodeType
			version 		int
		}
	*/

	testCases := []testCase{
		{
			name:    "successfully delete resource",
			version: 1,
		},
		{
			name:            "delete will fail because resource version doesn't match",
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.Comments.DeleteComment(ctx, &models.Comment{
				Metadata: models.ResourceMetadata{
					ID:      comment.Metadata.ID,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
			assert.Nil(t, err)
		})
	}
}
