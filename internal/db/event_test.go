//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

func TestListen(t *testing.T) {

	// Context with cancel.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	// Some common/shared foundational resources.
	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org-1",
	})
	require.Nil(t, err)
	proj1, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj-1",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	listenCtx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()

	chEvent, chFatal := testClient.client.Events.Listen(listenCtx)

	// For use in expected test results, etc.
	var jobID *string
	var agentIDForAgentSessions *string
	var agentSessionID *string
	var threadIDForComments *string
	var commentID *string

	type testCase struct {
		action            func() error
		name              string
		expectedEvents    []Event
		expectedEventData []any
	}

	testCases := []testCase{
		{
			name: "create pipeline, update not feasible to do, no delete method available",
			action: func() error {
				createdPipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
					ProjectID: proj1.Metadata.ID,
					Status:    models.PipelineTemplateUploaded,
				})
				if err != nil {
					return fmt.Errorf("failed to create pipeline template for pipeline test: %w", err)
				}

				_, err = testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
					ProjectID:          proj1.Metadata.ID,
					Type:               models.RunbookPipelineType,
					PipelineTemplateID: createdPipelineTemplate.Metadata.ID,
					Status:             statemachine.CreatedNodeStatus,
					When:               models.ManualPipeline,
					Stages:             buildStages(statemachine.CreatedNodeStatus, "", []string{}),
					ApprovalRuleIDs:    []string{},
				})
				if err != nil {
					return fmt.Errorf("failed to create pipeline: %w", err)
				}

				return nil
			},
			expectedEvents: []Event{
				{
					Table:  eventTablePipelines,
					Action: "INSERT",
				},
			},
			expectedEventData: []any{
				&PipelineEventData{
					ProjectID: proj1.Metadata.ID,
				},
			},
		},
		{
			name: "create and update log stream, no delete method available",
			action: func() error {

				createdLogStream, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
					// Hope JobID and AgentSessionID are optional.
				})
				if err != nil {
					return fmt.Errorf("failed to create log stream: %w", err)
				}

				toUpdate := *createdLogStream
				toUpdate.Completed = true
				_, err = testClient.client.LogStreams.UpdateLogStream(ctx, &toUpdate)
				if err != nil {
					return fmt.Errorf("failed to update log stream: %w", err)
				}

				return nil
			},
			expectedEvents: []Event{
				{
					Table:  eventTableLogStreams,
					Action: "INSERT",
				},
				{
					Table:  eventTableLogStreams,
					Action: "UPDATE",
				},
			},
			expectedEventData: []any{
				&LogStreamEventData{
					Completed: false,
				},
				&LogStreamEventData{
					Completed: true,
				},
			},
		},
		{
			name: "create and update job, delete not available",
			action: func() error {
				originalAgentName := "original-job-agent-name"
				updatedAgentName := "updated-job-agent-name"

				createdJob, err := testClient.client.Jobs.CreateJob(ctx, &models.Job{
					ProjectID: proj1.Metadata.ID,
					AgentName: &originalAgentName,
				})
				if err != nil {
					return fmt.Errorf("failed to create job: %w", err)
				}

				toUpdate := *createdJob
				toUpdate.AgentName = &updatedAgentName
				_, err = testClient.client.Jobs.UpdateJob(ctx, &toUpdate)
				if err != nil {
					return fmt.Errorf("failed to update job: %w", err)
				}

				// Save the job ID for expected results.
				jobID = &createdJob.Metadata.ID

				return nil
			},
			expectedEvents: []Event{
				{
					Table:  eventTableJobs,
					Action: "INSERT",
				},
				{
					Table:  eventTableJobs,
					Action: "UPDATE",
				},
			},
			expectedEventData: []any{
				&JobEventData{
					// ID will be filled in later.
					ProjectID: proj1.Metadata.ID,
					Data:      JobEventTaskData{},
				},
				&JobEventData{
					// ID will be filled in later.
					ProjectID: proj1.Metadata.ID,
					Data:      JobEventTaskData{},
				},
				&JobEventData{
					// ID will be filled in later.
					ProjectID: proj1.Metadata.ID,
					Data:      JobEventTaskData{},
				},
			},
		},
		{
			name: "create, update, and delete agent session",
			action: func() error {

				// Must create an agent first, because the earlier agent was deleted.
				createdAgent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
					Name:        "agent-for-agent-sessions",
					Description: "agent description for agent sessions",
				})
				if err != nil {
					return fmt.Errorf("failed to create agent for agent session test: %w", err)
				}

				createdAgentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
					AgentID:    createdAgent.Metadata.ID,
					ErrorCount: 4,
				})
				if err != nil {
					return fmt.Errorf("failed to create agent session: %w", err)
				}

				toUpdate := *createdAgentSession
				toUpdate.ErrorCount = 76
				updatedAgentSession, err := testClient.client.AgentSessions.UpdateAgentSession(ctx, &toUpdate)
				if err != nil {
					return fmt.Errorf("failed to update agent session: %w", err)
				}

				err = testClient.client.AgentSessions.DeleteAgentSession(ctx, updatedAgentSession)
				if err != nil {
					return fmt.Errorf("failed to delete agent session: %w", err)
				}

				// Save the IDs for expected results.
				agentIDForAgentSessions = &createdAgent.Metadata.ID
				agentSessionID = &createdAgentSession.Metadata.ID

				return nil
			},
			expectedEvents: []Event{
				{
					Table:  eventTableAgents,
					Action: "INSERT",
				},
				{
					Table:  eventTableAgentSessions,
					Action: "INSERT",
				},
				{
					Table:  eventTableAgentSessions,
					Action: "UPDATE",
				},
				{
					Table:  eventTableAgentSessions,
					Action: "DELETE",
				},
			},
			expectedEventData: []any{
				&struct{}{},
				&AgentSessionEventData{
					// ID will be filled in later.
				},
				&AgentSessionEventData{
					// ID will be filled in later.
				},
				&AgentSessionEventData{
					// ID will be filled in later.
				},
			},
		},
		{
			name: "create, update, and delete comment",
			action: func() error {

				createdPipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
					ProjectID: proj1.Metadata.ID,
					Status:    models.PipelineTemplateUploaded,
				})
				if err != nil {
					return fmt.Errorf("failed to create pipeline template for comment test: %w", err)
				}

				createdPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
					ProjectID:          proj1.Metadata.ID,
					Type:               models.RunbookPipelineType,
					PipelineTemplateID: createdPipelineTemplate.Metadata.ID,
					Status:             statemachine.CreatedNodeStatus,
					When:               models.ManualPipeline,
					Stages:             buildStages(statemachine.CreatedNodeStatus, "", []string{}),
					ApprovalRuleIDs:    []string{},
				})
				if err != nil {
					return fmt.Errorf("failed to create pipeline for comment test: %w", err)
				}

				createdThread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
					PipelineID: &createdPipeline.Metadata.ID,
				})
				if err != nil {
					return fmt.Errorf("failed to create thread for comment test: %w", err)
				}

				createdComment, err := testClient.client.Comments.CreateComment(ctx, &models.Comment{
					ThreadID: createdThread.Metadata.ID,
					Text:     "original comment text",
				})
				if err != nil {
					return fmt.Errorf("failed to create comment: %w", err)
				}

				toUpdate := *createdComment
				toUpdate.Text = "updated comment text"
				updatedComment, err := testClient.client.Comments.UpdateComment(ctx, &toUpdate)
				if err != nil {
					return fmt.Errorf("failed to update comment: %w", err)
				}

				err = testClient.client.Comments.DeleteComment(ctx, updatedComment)
				if err != nil {
					return fmt.Errorf("failed to delete comment: %w", err)
				}

				// Save the IDs for expected results.
				threadIDForComments = &createdThread.Metadata.ID
				commentID = &createdComment.Metadata.ID

				return nil
			},
			expectedEvents: []Event{
				{
					Table:  eventTablePipelines,
					Action: "INSERT",
				},
				{
					Table:  eventTableComments,
					Action: "INSERT",
				},
				{
					Table:  eventTableComments,
					Action: "UPDATE",
				},
				{
					Table:  eventTableComments,
					Action: "DELETE",
				},
			},
			expectedEventData: []any{
				&PipelineEventData{
					ProjectID: proj1.Metadata.ID,
				},
				&CommentEventData{
					// The IDs will be filled in later.
				},
				&CommentEventData{
					// The IDs will be filled in later.
				},
				&CommentEventData{
					// The IDs will be filled in later.
				},
			},
		},
		{
			name: "create and update todo item, no delete method available",
			action: func() error {

				createdPipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
					ProjectID: proj1.Metadata.ID,
					Status:    models.PipelineTemplateUploaded,
				})
				if err != nil {
					return fmt.Errorf("failed to create pipeline template for comment test: %w", err)
				}

				createdPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
					ProjectID:          proj1.Metadata.ID,
					Type:               models.RunbookPipelineType,
					PipelineTemplateID: createdPipelineTemplate.Metadata.ID,
					Status:             statemachine.CreatedNodeStatus,
					When:               models.ManualPipeline,
					Stages:             buildStages(statemachine.CreatedNodeStatus, "", []string{}),
					ApprovalRuleIDs:    []string{},
				})
				if err != nil {
					return fmt.Errorf("failed to create pipeline for comment test: %w", err)
				}

				createdToDoItem, err := testClient.client.ToDoItems.CreateToDoItem(ctx, &models.ToDoItem{
					OrganizationID: org1.Metadata.ID,
					TargetType:     models.PipelineApprovalTarget,
					TargetID:       createdPipeline.Metadata.ID,
					AutoResolved:   true,
				})
				if err != nil {
					return fmt.Errorf("failed to create todo item: %w", err)
				}

				toUpdate := *createdToDoItem
				toUpdate.AutoResolved = false
				_, err = testClient.client.ToDoItems.UpdateToDoItem(ctx, &toUpdate)
				if err != nil {
					return fmt.Errorf("failed to update todo item: %w", err)
				}

				return nil
			},
			expectedEvents: []Event{
				{
					Table:  eventTablePipelines,
					Action: "INSERT",
				},
				{
					Table:  eventTableToDoItems,
					Action: "INSERT",
				},
				{
					Table:  eventTableToDoItems,
					Action: "UPDATE",
				},
			},
			expectedEventData: []any{
				&PipelineEventData{
					ProjectID: proj1.Metadata.ID,
				},
				// No typed data for todo items.
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			// Execute the action.
			gotActionError := test.action()
			require.Nil(t, gotActionError)

			// Listen for action errors, fatal errors, and events.
			expectedCount := len(test.expectedEvents)
			actualEvents := []Event{}
			fatalErrors := []error{}

			// Wait for events, errors, or timeout.
			keepWaiting := true
			for keepWaiting {
				select {
				case gotEvent := <-chEvent:
					actualEvents = append(actualEvents, gotEvent)
				case gotFatal := <-chFatal:
					fatalErrors = append(fatalErrors, gotFatal)
				case <-ctx.Done():
					keepWaiting = false
				}

				if len(actualEvents) >= expectedCount || len(fatalErrors) > 0 {
					keepWaiting = false
				}
			}

			// Check for fatal errors.
			require.Zero(t, len(fatalErrors))

			// Check errors and events.
			assert.Equal(t, expectedCount, len(actualEvents))
			for ix := range test.expectedEvents {
				assert.Equal(t, test.expectedEvents[ix].Table, actualEvents[ix].Table)
				assert.Equal(t, test.expectedEvents[ix].Action, actualEvents[ix].Action)

				switch test.expectedEvents[ix].Table {
				case eventTablePipelines:
					typedExpected, ok := test.expectedEventData[ix].(*PipelineEventData)
					assert.True(t, ok)

					actualPipelineEventData, err := actualEvents[ix].ToPipelineEventData()
					assert.Nil(t, err)

					assert.Equal(t, typedExpected, actualPipelineEventData)
				case eventTableLogStreams:
					typedExpected, ok := test.expectedEventData[ix].(*LogStreamEventData)
					assert.True(t, ok)

					actualLogStreamEventData, err := actualEvents[ix].ToLogStreamEventData()
					assert.Nil(t, err)

					assert.Equal(t, typedExpected, actualLogStreamEventData)
				case eventTableAgents:
					// No tests for this table, so nothing needs to be done here.
				case eventTableJobs:
					typedExpected, ok := test.expectedEventData[ix].(*JobEventData)
					assert.True(t, ok)
					// Must paste in the job ID after the job has been created.
					typedExpected.ID = *jobID

					actualJobEventData, err := actualEvents[ix].ToJobEventData()
					assert.Nil(t, err)

					assert.Equal(t, typedExpected, actualJobEventData)
				case eventTableAgentSessions:
					typedExpected, ok := test.expectedEventData[ix].(*AgentSessionEventData)
					assert.True(t, ok)
					// Must paste in the IDs after the resources have been created.
					typedExpected.ID = *agentSessionID
					typedExpected.AgentID = *agentIDForAgentSessions

					actualAgentSessionEventData, err := actualEvents[ix].ToAgentSessionEventData()
					assert.Nil(t, err)

					assert.Equal(t, typedExpected, actualAgentSessionEventData)
				case eventTableActivityEvents:
					// No tests for this table, so nothing needs to be done here.
				case eventTableComments:
					typedExpected, ok := test.expectedEventData[ix].(*CommentEventData)
					assert.True(t, ok)
					// Must paste in the IDs after the resources have been created.
					typedExpected.ThreadID = *threadIDForComments
					typedExpected.ID = *commentID

					actualCommentEventData, err := actualEvents[ix].ToCommentEventData()
					assert.Nil(t, err)

					assert.Equal(t, typedExpected, actualCommentEventData)
				case eventTableToDoItems:
					// No typed data for this table.
				default:
					assert.Fail(t, "Invalid actual event table: %s", actualEvents[ix].Table)
				}
			}
		})
	}
}
