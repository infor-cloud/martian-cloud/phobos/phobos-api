//go:build integration

package db

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestGetLogStreamByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	logStream, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
		AgentSessionID: &agentSession.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		id              string
		expectLogStream bool
	}

	testCases := []testCase{
		{
			name:            "get resource by id",
			id:              logStream.Metadata.ID,
			expectLogStream: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			logStream, err := testClient.client.LogStreams.GetLogStreamByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectLogStream {
				require.NotNil(t, logStream)
				assert.Equal(t, test.id, logStream.Metadata.ID)
			} else {
				assert.Nil(t, logStream)
			}
		})
	}
}

func TestGetLogStreamByJobID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	jobToCreate := &models.Job{
		ProjectID: project.Metadata.ID,
		Type:      models.JobTaskType,
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobPending))

	job, aErr := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.Nil(t, aErr)

	logStream, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
		JobID: &job.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		jobID           string
		expectLogStream bool
	}

	testCases := []testCase{
		{
			name:            "get resource by job id",
			jobID:           job.Metadata.ID,
			expectLogStream: true,
		},
		{
			name:  "resource with id not found",
			jobID: nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			jobID:           invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualLogStream, err := testClient.client.LogStreams.GetLogStreamByJobID(ctx, test.jobID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectLogStream {
				require.NotNil(t, actualLogStream)
				require.NotNil(t, actualLogStream.JobID)
				assert.Equal(t, logStream.Metadata.ID, actualLogStream.Metadata.ID)
				assert.Equal(t, test.jobID, *actualLogStream.JobID)
			} else {
				assert.Nil(t, actualLogStream)
			}
		})
	}
}

func TestGetLogStreamByAgentSessionID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	logStream, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
		AgentSessionID: &agentSession.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		sessionID       string
		expectLogStream bool
	}

	testCases := []testCase{
		{
			name:            "get resource by agent session id",
			sessionID:       agentSession.Metadata.ID,
			expectLogStream: true,
		},
		{
			name:      "resource with id not found",
			sessionID: nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			sessionID:       invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualLogStream, err := testClient.client.LogStreams.GetLogStreamByAgentSessionID(ctx, test.sessionID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectLogStream {
				require.NotNil(t, actualLogStream)
				require.NotNil(t, actualLogStream.AgentSessionID)
				assert.Equal(t, logStream.Metadata.ID, actualLogStream.Metadata.ID)
				assert.Equal(t, test.sessionID, *actualLogStream.AgentSessionID)
			} else {
				assert.Nil(t, actualLogStream)
			}
		})
	}
}

func TestGetLogStreams(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	jobToCreate := &models.Job{
		ProjectID: project.Metadata.ID,
		Type:      models.JobTaskType,
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobPending))

	job, aErr := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.Nil(t, aErr)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	logStreams := []*models.LogStream{}

	logStream, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
		AgentSessionID: &agentSession.Metadata.ID,
	})
	require.Nil(t, err)

	logStream2, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
		JobID: &job.Metadata.ID,
	})
	require.Nil(t, err)

	logStreams = append(logStreams, logStream, logStream2)

	type testCase struct {
		filter            *LogStreamFilter
		expectErrorCode   errors.CodeType
		name              string
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all log streams",
			filter:            &LogStreamFilter{},
			expectResultCount: len(logStreams),
		},
		{
			name: "get log streams by job id",
			filter: &LogStreamFilter{
				JobIDs: []string{job.Metadata.ID},
			},
			expectResultCount: 1,
		},
		{
			name: "get log streams by agent session id",
			filter: &LogStreamFilter{
				AgentSessionIDs: []string{agentSession.Metadata.ID},
			},
			expectResultCount: 1,
		},
		{
			name: "get log streams by job id and agent session id",
			filter: &LogStreamFilter{
				JobIDs:          []string{job.Metadata.ID},
				AgentSessionIDs: []string{agentSession.Metadata.ID},
			},
			expectResultCount: 0,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualLogStreams, err := testClient.client.LogStreams.GetLogStreams(ctx, &GetLogStreamsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			assert.Equal(t, test.expectResultCount, len(actualLogStreams.LogStreams))
		})
	}
}

func TestGetLogStreamsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
			AgentID: agent.Metadata.ID,
		})
		require.Nil(t, err)

		_, aErr := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
			AgentSessionID: &agentSession.Metadata.ID,
		})
		require.Nil(t, aErr)
	}

	sortableFields := []sortableField{
		LogStreamSortableFieldUpdatedAtAsc,
		LogStreamSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := LogStreamSortableField(sortByField.getValue())

		result, err := testClient.client.LogStreams.GetLogStreams(ctx, &GetLogStreamsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.LogStreams {
			resources = append(resources, &result.LogStreams[ix])
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreateLogStream(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	jobToCreate := &models.Job{
		ProjectID: project.Metadata.ID,
		Type:      models.JobTaskType,
	}

	require.NoError(t, jobToCreate.SetStatus(models.JobPending))

	job, aErr := testClient.client.Jobs.CreateJob(ctx, jobToCreate)
	require.Nil(t, aErr)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		jobID           *string
		agentSessionID  *string
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name:  "successfully create log stream with job id",
			jobID: &job.Metadata.ID,
		},
		{
			name:           "successfully create log stream with agent session id",
			agentSessionID: &agentSession.Metadata.ID,
		},
		{
			name:            "create will fail because job does not exist",
			jobID:           ptr.String(nonExistentID),
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "create will fail because agent session does not exist",
			agentSessionID:  ptr.String(nonExistentID),
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "duplicate log stream for job id will fail",
			jobID:           &job.Metadata.ID,
			expectErrorCode: errors.EConflict,
		},
		{
			name:            "duplicate log stream for agent session id will fail",
			agentSessionID:  &agentSession.Metadata.ID,
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			logStream, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
				JobID:          test.jobID,
				AgentSessionID: test.agentSessionID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			assert.NotNil(t, logStream)
		})
	}
}

func TestUpdateLogStream(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	logStream, err := testClient.client.LogStreams.CreateLogStream(ctx, &models.LogStream{
		AgentSessionID: &agentSession.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		completed       bool
		expectErrorCode errors.CodeType
		name            string
		version         int
	}

	testCases := []testCase{
		{
			name:      "successfully update log stream",
			completed: true,
			version:   1,
		},
		{
			name:            "update log stream with invalid version will fail",
			completed:       true,
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			updatedLogStream, err := testClient.client.LogStreams.UpdateLogStream(ctx, &models.LogStream{
				Metadata: models.ResourceMetadata{
					ID:      logStream.Metadata.ID,
					Version: test.version,
				},
				Completed:      test.completed,
				AgentSessionID: logStream.AgentSessionID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			assert.NotNil(t, updatedLogStream)
			assert.Equal(t, test.completed, updatedLogStream.Completed)
		})
	}
}
