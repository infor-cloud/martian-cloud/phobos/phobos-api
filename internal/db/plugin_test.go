//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// getValue implements the sortableField interface for testing.
func (sf PluginSortableField) getValue() string {
	return string(sf)
}

func TestGetPluginByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		id              string
		expectPlugin    bool
	}

	testCases := []testCase{
		{
			name:         "get resource by id",
			id:           plugin.Metadata.ID,
			expectPlugin: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPlugin, err := testClient.client.Plugins.GetPluginByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPlugin {
				require.NotNil(t, actualPlugin)
				assert.Equal(t, test.id, actualPlugin.Metadata.ID)
			} else {
				assert.Nil(t, actualPlugin)
			}
		})
	}
}

func TestGetPluginByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		prn             string
		expectPlugin    bool
	}

	testCases := []testCase{
		{
			name:         "get resource by PRN",
			prn:          plugin.Metadata.PRN,
			expectPlugin: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.PluginResource.BuildPRN(organization.Name, "invalid"),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPlugin, err := testClient.client.Plugins.GetPluginByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPlugin {
				require.NotNil(t, actualPlugin)
				assert.Equal(t, test.prn, actualPlugin.Metadata.PRN)
			} else {
				assert.Nil(t, actualPlugin)
			}
		})
	}
}

func TestGetPlugins(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	organization2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "proton",
	})
	require.NoError(t, err)

	role, err := testClient.client.Roles.CreateRole(ctx, &models.Role{
		Name: "owner",
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
	})
	require.NoError(t, err)

	serviceAccount, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-service-account",
		OrganizationID: &organization.Metadata.ID,
		Scope:          models.OrganizationScope,
	})
	require.NoError(t, err)

	// User is a member of organization 1
	_, err = testClient.client.Memberships.CreateMembership(ctx, &models.Membership{
		UserID:         &user.Metadata.ID,
		OrganizationID: &organization.Metadata.ID,
		Scope:          models.OrganizationScope,
		RoleID:         role.Metadata.ID,
	})
	require.NoError(t, err)

	// Service account is a member of organization 2
	_, err = testClient.client.Memberships.CreateMembership(ctx, &models.Membership{
		ServiceAccountID: &serviceAccount.Metadata.ID,
		OrganizationID:   &organization2.Metadata.ID,
		Scope:            models.OrganizationScope,
		RoleID:           role.Metadata.ID,
	})
	require.NoError(t, err)

	plugins := make([]*models.Plugin, 10)
	for i := 0; i < len(plugins); i++ {
		plugin, eErr := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
			Name:           fmt.Sprintf("test-plugin-%d", i),
			OrganizationID: organization.Metadata.ID,
			Private:        i%2 == 0, // half of the plugins are private
		})
		require.NoError(t, eErr)

		plugins[i] = plugin
	}

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization2.Metadata.ID,
		Private:        true,
	})
	require.NoError(t, err)

	plugins = append(plugins, plugin)

	type testCase struct {
		filter            *PluginFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all resources",
			filter:            &PluginFilter{},
			expectResultCount: len(plugins),
		},
		{
			name: "get resources for organization 1",
			filter: &PluginFilter{
				OrganizationID: &organization.Metadata.ID,
			},
			expectResultCount: len(plugins) - 1,
		},
		{
			name: "get resources for organization 2",
			filter: &PluginFilter{
				OrganizationID: &organization2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "search by plugin name prefix",
			filter: &PluginFilter{
				Search: ptr.String("test-plugin-"),
			},
			expectResultCount: len(plugins) - 1,
		},
		{
			name: "search by plugin name",
			filter: &PluginFilter{
				Search: &plugin.Name,
			},
			expectResultCount: 1,
		},
		{
			name: "search by organization name",
			filter: &PluginFilter{
				Search: &organization.Name,
			},
			expectResultCount: len(plugins) - 1,
		},
		{
			name: "search by organization name and plugin name",
			filter: &PluginFilter{
				Search: ptr.String(fmt.Sprintf("%s/%s", organization.Name, plugins[0].Name)),
			},
			expectResultCount: 1,
		},
		{
			name: "search by organization name and plugin name prefix",
			filter: &PluginFilter{
				Search: ptr.String(fmt.Sprintf("%s/test-plugin-", organization.Name)),
			},
			expectResultCount: len(plugins) - 1,
		},
		{
			name: "get resources by ids",
			filter: &PluginFilter{
				PluginIDs: []string{plugins[0].Metadata.ID, plugins[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
		{
			name: "get resources by user id",
			filter: &PluginFilter{
				// User is a member of organization 1
				UserID: &user.Metadata.ID,
			},
			// Expecting all plugins except the one in organization 2
			expectResultCount: len(plugins) - 1,
		},
		{
			name: "get resources by service account id",
			filter: &PluginFilter{
				// Service account is a member of organization 2
				ServiceAccountID: &serviceAccount.Metadata.ID,
			},
			// Expecting any public plugins and the one in organization 2
			expectResultCount: len(plugins)/2 + 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.Plugins.GetPlugins(ctx, &GetPluginsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, result)
			assert.Len(t, result.Plugins, test.expectResultCount)
		})
	}
}

func TestGetPluginsWithSortingAndPagination(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, eErr := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
			Name:           fmt.Sprintf("test-plugin-%d", i),
			OrganizationID: organization.Metadata.ID,
		})
		require.NoError(t, eErr)
	}

	sortableFields := []sortableField{
		PluginSortableFieldUpdatedAtAsc,
		PluginSortableFieldUpdatedAtDesc,
		PluginSortableFieldNameAsc,
		PluginSortableFieldNameDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := PluginSortableField(sortByField.getValue())

		result, err := testClient.client.Plugins.GetPlugins(ctx, &GetPluginsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.Plugins {
			resources = append(resources, &result.Plugins[ix])
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreatePlugin(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		organizationID  string
	}

	testCases := []testCase{
		{
			name:           "create resource",
			organizationID: organization.Metadata.ID,
		},
		{
			name:            "create resource with non-existent organization id will return an error",
			organizationID:  nonExistentID,
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "duplicate resource will return an error",
			organizationID:  organization.Metadata.ID,
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPlugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
				Name:           "gitlab",
				OrganizationID: test.organizationID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, actualPlugin)
		})
	}
}

func TestUpdatePlugin(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	updatedRepoURL := "http://sample.tld"

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		version         int
	}

	testCases := []testCase{
		{
			name:    "update resource",
			version: plugin.Metadata.Version,
		},
		{
			name:            "update resource with invalid version will return an error",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPlugin, err := testClient.client.Plugins.UpdatePlugin(ctx, &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID:      plugin.Metadata.ID,
					Version: plugin.Metadata.Version,
				},
				Private:       true,
				RepositoryURL: updatedRepoURL,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, actualPlugin)
			assert.True(t, actualPlugin.Private)
			assert.Equal(t, updatedRepoURL, actualPlugin.RepositoryURL)
		})
	}
}

func TestDeletePlugin(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "delete resource",
			version: plugin.Metadata.Version,
		},
		{
			name:            "delete resource with invalid version will return an error",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.Plugins.DeletePlugin(ctx, &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID:      plugin.Metadata.ID,
					Version: plugin.Metadata.Version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
