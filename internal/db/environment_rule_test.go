//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (os EnvironmentRuleSortableField) getValue() string {
	return string(os)
}

func TestGetEnvironmentRuleByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	orgRule, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:           models.OrganizationScope,
		OrgID:           org.Metadata.ID,
		EnvironmentName: "environment-1",
	})
	require.Nil(t, err)

	projRule, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:           models.ProjectScope,
		OrgID:           org.Metadata.ID,
		ProjectID:       &proj.Metadata.ID,
		EnvironmentName: "environment-1",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode       errors.CodeType
		name                  string
		id                    string
		expectEnvironmentRule bool
	}

	testCases := []testCase{
		{
			name:                  "get org rule by id",
			id:                    orgRule.Metadata.ID,
			expectEnvironmentRule: true,
		},
		{
			name:                  "get proj rule by id",
			id:                    projRule.Metadata.ID,
			expectEnvironmentRule: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			environmentRule, err := testClient.client.EnvironmentRules.GetEnvironmentRuleByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectEnvironmentRule {
				require.NotNil(t, environmentRule)
				assert.Equal(t, test.id, environmentRule.Metadata.ID)
			} else {
				assert.Nil(t, environmentRule)
			}
		})
	}
}

func TestGetEnvironmentRuleByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	orgRule, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:           models.OrganizationScope,
		OrgID:           org.Metadata.ID,
		EnvironmentName: "environment-1",
	})
	require.Nil(t, err)

	projRule, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:           models.ProjectScope,
		OrgID:           org.Metadata.ID,
		ProjectID:       &proj.Metadata.ID,
		EnvironmentName: "environment-1",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode       errors.CodeType
		name                  string
		prn                   string
		expectEnvironmentRule bool
	}

	testCases := []testCase{
		{
			name:                  "get org rule by PRN",
			prn:                   orgRule.Metadata.PRN,
			expectEnvironmentRule: true,
		},
		{
			name:                  "get proj rule by PRN",
			prn:                   projRule.Metadata.PRN,
			expectEnvironmentRule: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.EnvironmentRuleResource.BuildPRN(nonExistentID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			environmentRule, err := testClient.client.EnvironmentRules.GetEnvironmentRuleByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectEnvironmentRule {
				require.NotNil(t, environmentRule)
				assert.Equal(t, test.prn, environmentRule.Metadata.PRN)
			} else {
				assert.Nil(t, environmentRule)
			}
		})
	}
}

func TestCreateEnvironmentRule(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	user1, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user-1",
		Email:    "user-1@example.invalid",
	})
	require.Nil(t, err)

	user2, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user-2",
		Email:    "user-2@example.invalid",
	})
	require.Nil(t, err)

	serviceAccount1, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-sa-1",
		Scope:          models.OrganizationScope,
		OrganizationID: &org.Metadata.ID,
	})
	require.Nil(t, err)

	serviceAccount2, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-sa-2",
		Scope:          models.OrganizationScope,
		OrganizationID: &org.Metadata.ID,
	})
	require.Nil(t, err)

	team1, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team-1",
	})
	require.Nil(t, err)

	team2, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team-2",
	})
	require.Nil(t, err)

	role1, err := testClient.client.Roles.CreateRole(ctx, &models.Role{
		Name:        "test-role-1",
		Description: "test role 1",
	})
	require.Nil(t, err)

	role2, err := testClient.client.Roles.CreateRole(ctx, &models.Role{
		Name:        "test-role-2",
		Description: "test role 2",
	})
	require.Nil(t, err)

	type testCase struct {
		name              string
		expectErrorCode   errors.CodeType
		scope             models.ScopeType
		orgID             string
		projID            *string
		environmentName   string
		userIDs           []string
		serviceAccountIDs []string
		teamIDs           []string
		roleIDs           []string
	}

	testCases := []testCase{
		{
			name:              "successfully create org rule 1",
			scope:             models.OrganizationScope,
			orgID:             org.Metadata.ID,
			environmentName:   "environment-1",
			userIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
			serviceAccountIDs: []string{serviceAccount1.Metadata.ID, serviceAccount2.Metadata.ID},
			teamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
			roleIDs:           []string{role1.Metadata.ID, role2.Metadata.ID},
		},
		{
			name:              "successfully create org rule 2",
			scope:             models.OrganizationScope,
			orgID:             org.Metadata.ID,
			environmentName:   "environment-2",
			userIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
			serviceAccountIDs: []string{serviceAccount1.Metadata.ID, serviceAccount2.Metadata.ID},
			teamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
			roleIDs:           []string{role1.Metadata.ID, role2.Metadata.ID},
		},
		{
			name:              "successfully create proj rule 1",
			scope:             models.ProjectScope,
			orgID:             org.Metadata.ID,
			projID:            &proj.Metadata.ID,
			environmentName:   "environment-1",
			userIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
			serviceAccountIDs: []string{serviceAccount1.Metadata.ID, serviceAccount2.Metadata.ID},
			teamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
			roleIDs:           []string{role1.Metadata.ID, role2.Metadata.ID},
		},
		{
			name:              "successfully create proj rule 2",
			scope:             models.ProjectScope,
			orgID:             org.Metadata.ID,
			projID:            &proj.Metadata.ID,
			environmentName:   "environment-2",
			userIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
			serviceAccountIDs: []string{serviceAccount1.Metadata.ID, serviceAccount2.Metadata.ID},
			teamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
			roleIDs:           []string{role1.Metadata.ID, role2.Metadata.ID},
		},
		{
			name:            "org rule, duplicate environment",
			scope:           models.OrganizationScope,
			orgID:           org.Metadata.ID,
			projID:          &proj.Metadata.ID,
			environmentName: "environment-1",
			expectErrorCode: errors.EConflict,
		},
		{
			name:            "proj rule, duplicate environment",
			scope:           models.ProjectScope,
			orgID:           org.Metadata.ID,
			projID:          &proj.Metadata.ID,
			environmentName: "environment-1",
			expectErrorCode: errors.EConflict,
		},
		{
			name:            "create will fail because org does not exist",
			scope:           models.OrganizationScope,
			orgID:           nonExistentID,
			environmentName: "environment-org-org",
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "create will fail because proj does not exist",
			scope:           models.ProjectScope,
			orgID:           nonExistentID,
			environmentName: "environment-no-proj",
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "create will fail because user does not exist",
			scope:           models.OrganizationScope,
			orgID:           org.Metadata.ID,
			environmentName: "environment-no-user",
			userIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:              "create will fail because service account does not exist",
			scope:             models.OrganizationScope,
			orgID:             org.Metadata.ID,
			environmentName:   "environment-no-sa",
			serviceAccountIDs: []string{nonExistentID},
			expectErrorCode:   errors.EInvalid,
		},
		{
			name:            "create will fail because team does not exist",
			scope:           models.OrganizationScope,
			orgID:           org.Metadata.ID,
			environmentName: "environment-no-team",
			teamIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "create will fail because role does not exist",
			scope:           models.OrganizationScope,
			orgID:           org.Metadata.ID,
			environmentName: "environment-no-role",
			roleIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			environmentRule, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
				Scope:             test.scope,
				OrgID:             test.orgID,
				ProjectID:         test.projID,
				EnvironmentName:   test.environmentName,
				UserIDs:           test.userIDs,
				ServiceAccountIDs: test.serviceAccountIDs,
				TeamIDs:           test.teamIDs,
				RoleIDs:           test.roleIDs,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, environmentRule)
			assert.Equal(t, test.userIDs, environmentRule.UserIDs)
			assert.Equal(t, test.serviceAccountIDs, environmentRule.ServiceAccountIDs)
			assert.Equal(t, test.teamIDs, environmentRule.TeamIDs)
		})
	}
}

func TestUpdateEnvironmentRule(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	user1, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user-1",
		Email:    "user-1@example.invalid",
	})
	require.Nil(t, err)

	user2, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user-2",
		Email:    "user-2@example.invalid",
	})
	require.Nil(t, err)

	serviceAccount1, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-sa-1",
		Scope:          models.OrganizationScope,
		OrganizationID: &org.Metadata.ID,
	})
	require.Nil(t, err)

	serviceAccount2, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-sa-2",
		Scope:          models.OrganizationScope,
		OrganizationID: &org.Metadata.ID,
	})
	require.Nil(t, err)

	team1, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team-1",
	})
	require.Nil(t, err)

	team2, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team-2",
	})
	require.Nil(t, err)

	role1, err := testClient.client.Roles.CreateRole(ctx, &models.Role{
		Name:        "test-role-1",
		Description: "test role 1",
	})
	require.Nil(t, err)

	role2, err := testClient.client.Roles.CreateRole(ctx, &models.Role{
		Name:        "test-role-2",
		Description: "test role 2",
	})
	require.Nil(t, err)

	environmentRule1, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:             models.OrganizationScope,
		OrgID:             org.Metadata.ID,
		EnvironmentName:   "environment-1",
		UserIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
		ServiceAccountIDs: []string{serviceAccount1.Metadata.ID, serviceAccount2.Metadata.ID},
		TeamIDs:           []string{},
		RoleIDs:           []string{},
	})
	require.Nil(t, err)

	environmentRule2, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:             models.ProjectScope,
		OrgID:             org.Metadata.ID,
		ProjectID:         &proj.Metadata.ID,
		EnvironmentName:   "environment-1",
		UserIDs:           []string{},
		ServiceAccountIDs: []string{},
		TeamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
		RoleIDs:           []string{role1.Metadata.ID, role2.Metadata.ID},
	})
	require.Nil(t, err)

	type testCase struct {
		name              string
		ruleID            string
		version           int
		userIDs           []string
		serviceAccountIDs []string
		teamIDs           []string
		roleIDs           []string
		expectUpdatedRule *models.EnvironmentRule
		expectErrorCode   errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully update rule 1",
			ruleID:  environmentRule1.Metadata.ID,
			version: 1,
			teamIDs: []string{team1.Metadata.ID, team2.Metadata.ID},
			roleIDs: []string{role1.Metadata.ID, role2.Metadata.ID},
			expectUpdatedRule: &models.EnvironmentRule{
				Scope:             models.OrganizationScope,
				OrgID:             org.Metadata.ID,
				EnvironmentName:   "environment-1",
				UserIDs:           []string{},
				ServiceAccountIDs: []string{},
				TeamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
				RoleIDs:           []string{role1.Metadata.ID, role2.Metadata.ID},
			},
		},
		{
			name:              "successfully update rule 2",
			ruleID:            environmentRule2.Metadata.ID,
			version:           1,
			userIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
			serviceAccountIDs: []string{serviceAccount1.Metadata.ID, serviceAccount2.Metadata.ID},
			expectUpdatedRule: &models.EnvironmentRule{
				Scope:             models.OrganizationScope,
				OrgID:             org.Metadata.ID,
				EnvironmentName:   "environment-1",
				UserIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
				ServiceAccountIDs: []string{serviceAccount1.Metadata.ID, serviceAccount2.Metadata.ID},
				TeamIDs:           []string{},
				RoleIDs:           []string{},
			},
		},
		{
			name:            "update will fail because resource version doesn't match",
			ruleID:          environmentRule1.Metadata.ID,
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
		{
			name:            "update will fail because user does not exist",
			ruleID:          environmentRule1.Metadata.ID,
			version:         2,
			userIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:              "update will fail because service account does not exist",
			ruleID:            environmentRule1.Metadata.ID,
			version:           2,
			serviceAccountIDs: []string{nonExistentID},
			expectErrorCode:   errors.EInvalid,
		},
		{
			name:            "update will fail because team does not exist",
			ruleID:          environmentRule1.Metadata.ID,
			version:         2,
			teamIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "update will fail because role does not exist",
			ruleID:          environmentRule1.Metadata.ID,
			version:         2,
			roleIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualEnvironmentRule, err := testClient.client.EnvironmentRules.UpdateEnvironmentRule(ctx, &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      test.ruleID,
					Version: test.version,
				},
				UserIDs:           test.userIDs,
				ServiceAccountIDs: test.serviceAccountIDs,
				TeamIDs:           test.teamIDs,
				RoleIDs:           test.roleIDs,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, actualEnvironmentRule)
			assert.ElementsMatch(t, test.userIDs, actualEnvironmentRule.UserIDs)
			assert.ElementsMatch(t, test.serviceAccountIDs, actualEnvironmentRule.ServiceAccountIDs)
			assert.ElementsMatch(t, test.teamIDs, actualEnvironmentRule.TeamIDs)
			assert.ElementsMatch(t, test.roleIDs, actualEnvironmentRule.RoleIDs)
		})
	}
}

func TestDeleteEnvironmentRule(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.Nil(t, err)

	environmentRule, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:           models.OrganizationScope,
		OrgID:           org.Metadata.ID,
		EnvironmentName: "environment-1",
		TeamIDs:         []string{team.Metadata.ID},
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              environmentRule.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      environmentRule.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.EnvironmentRules.DeleteEnvironmentRule(ctx, &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestGetEnvironmentRules(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	org2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org2",
	})
	require.Nil(t, err)

	proj1, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj-1",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	proj2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj-2",
		OrgID: org2.Metadata.ID,
	})
	require.Nil(t, err)

	// Make some org rules, indices 0 through 4.
	rules := []*models.EnvironmentRule{}
	for i := 0; i < 5; i++ {
		rule, aErr := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
			Scope:           models.OrganizationScope,
			OrgID:           org1.Metadata.ID,
			EnvironmentName: fmt.Sprintf("environment-org1-%d", i),
		})
		require.Nil(t, aErr)

		rules = append(rules, rule)
	}

	// Make another org rule, index 5.
	rule, err := testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:           models.OrganizationScope,
		OrgID:           org2.Metadata.ID,
		EnvironmentName: "environment-org2",
	})
	require.Nil(t, err)

	rules = append(rules, rule)

	// Make some project rules, indices 6 through 10.
	for i := 0; i < 5; i++ {
		rule, err = testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
			Scope:           models.ProjectScope,
			OrgID:           org1.Metadata.ID,
			ProjectID:       &proj1.Metadata.ID,
			EnvironmentName: fmt.Sprintf("environment-proj1-%d", i),
		})
		require.Nil(t, err)

		rules = append(rules, rule)
	}

	// Make another proj rule, index 11.
	rule, err = testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
		Scope:           models.ProjectScope,
		OrgID:           org2.Metadata.ID,
		ProjectID:       &proj2.Metadata.ID,
		EnvironmentName: "environment-proj2",
	})
	require.Nil(t, err)

	rules = append(rules, rule)

	type testCase struct {
		filter          *EnvironmentRuleFilter
		name            string
		expectRules     []*models.EnvironmentRule
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "return all rules in org 1",
			filter: &EnvironmentRuleFilter{
				OrgID:                 &org1.Metadata.ID,
				EnvironmentRuleScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectRules: rules[:5],
		},
		{
			name: "return all rules in org 2",
			filter: &EnvironmentRuleFilter{
				OrgID:                 &org2.Metadata.ID,
				EnvironmentRuleScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectRules: rules[5:6],
		},
		{
			name: "return all rules in project 1",
			filter: &EnvironmentRuleFilter{
				ProjectID:             &proj1.Metadata.ID,
				EnvironmentRuleScopes: []models.ScopeType{models.ProjectScope},
			},
			expectRules: rules[6:11],
		},
		{
			name: "return all rules in project 2",
			filter: &EnvironmentRuleFilter{
				ProjectID:             &proj2.Metadata.ID,
				EnvironmentRuleScopes: []models.ScopeType{models.ProjectScope},
			},
			expectRules: rules[11:],
		},
		{
			name: "return all rules for a specific environment",
			filter: &EnvironmentRuleFilter{
				EnvironmentName: &rules[3].EnvironmentName,
			},
			expectRules: rules[3:4],
		},
		{
			name: "return all rules with organization scope",
			filter: &EnvironmentRuleFilter{
				EnvironmentRuleScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectRules: rules[:6],
		},
		{
			name: "return all rules with project scope",
			filter: &EnvironmentRuleFilter{
				EnvironmentRuleScopes: []models.ScopeType{models.ProjectScope},
			},
			expectRules: rules[6:],
		},
		{
			name: "return all rules matching specific IDs",
			filter: &EnvironmentRuleFilter{
				EnvironmentRuleIDs: []string{rules[4].Metadata.ID, rules[7].Metadata.ID},
			},
			expectRules: []*models.EnvironmentRule{rules[4], rules[7]},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.EnvironmentRules.GetEnvironmentRules(ctx, &GetEnvironmentRulesInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.Equal(t, len(test.expectRules), len(result.EnvironmentRules))
			assert.ElementsMatch(t, test.expectRules, result.EnvironmentRules)
		})
	}
}

func TestGetEnvironmentRulesWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.EnvironmentRules.CreateEnvironmentRule(ctx, &models.EnvironmentRule{
			Scope:           models.OrganizationScope,
			OrgID:           org.Metadata.ID,
			EnvironmentName: fmt.Sprintf("environment-org-%d", i),
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		EnvironmentRuleSortableFieldUpdatedAtAsc,
		EnvironmentRuleSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := EnvironmentRuleSortableField(sortByField.getValue())

		result, err := testClient.client.EnvironmentRules.GetEnvironmentRules(ctx, &GetEnvironmentRulesInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.EnvironmentRules {
			resources = append(resources, resource)
		}

		return result.PageInfo, resources, nil
	})
}
