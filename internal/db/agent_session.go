package db

//go:generate go tool mockery --name AgentSessions --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// AgentSessions encapsulates the logic to access sessions from the database
type AgentSessions interface {
	GetAgentSessionByID(ctx context.Context, id string) (*models.AgentSession, error)
	GetAgentSessionByPRN(ctx context.Context, prn string) (*models.AgentSession, error)
	GetAgentSessions(ctx context.Context, input *GetAgentSessionsInput) (*AgentSessionsResult, error)
	CreateAgentSession(ctx context.Context, session *models.AgentSession) (*models.AgentSession, error)
	UpdateAgentSession(ctx context.Context, session *models.AgentSession) (*models.AgentSession, error)
	DeleteAgentSession(ctx context.Context, session *models.AgentSession) error
}

// AgentSessionSortableField represents the fields that sessions can be sorted by
type AgentSessionSortableField string

// AgentSessionSortableField constants
const (
	AgentSessionSortableFieldCreatedAtAsc        AgentSessionSortableField = "CREATED_AT_ASC"
	AgentSessionSortableFieldCreatedAtDesc       AgentSessionSortableField = "CREATED_AT_DESC"
	AgentSessionSortableFieldLastContactedAtAsc  AgentSessionSortableField = "LAST_CONTACTED_AT_ASC"
	AgentSessionSortableFieldLastContactedAtDesc AgentSessionSortableField = "LAST_CONTACTED_AT_DESC"
)

func (as AgentSessionSortableField) getValue() string {
	return string(as)
}

func (as AgentSessionSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch as {
	case AgentSessionSortableFieldCreatedAtAsc, AgentSessionSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "agent_sessions", Col: "created_at"}
	case AgentSessionSortableFieldLastContactedAtAsc, AgentSessionSortableFieldLastContactedAtDesc:
		return &pagination.FieldDescriptor{Key: "last_contacted_at", Table: "agent_sessions", Col: "last_contacted_at"}
	default:
		return nil
	}
}

func (as AgentSessionSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(as), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// AgentSessionFilter contains the supported fields for filtering AgentSession resources
type AgentSessionFilter struct {
	AgentID         *string
	AgentSessionIDs []string
}

// GetAgentSessionsInput is the input for listing sessions
type GetAgentSessionsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *AgentSessionSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *AgentSessionFilter
}

// AgentSessionsResult contains the response data and page information
type AgentSessionsResult struct {
	PageInfo      *pagination.PageInfo
	AgentSessions []models.AgentSession
}

type sessions struct {
	dbClient *Client
}

var sessionFieldList = append(metadataFieldList, "agent_id", "last_contacted_at", "error_count", "internal")

// NewAgentSessions returns an instance of the AgentSessions interface
func NewAgentSessions(dbClient *Client) AgentSessions {
	return &sessions{dbClient: dbClient}
}

func (a *sessions) GetAgentSessionByID(ctx context.Context, id string) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "db.GetAgentSessionByID")
	defer span.End()

	return a.getAgentSession(ctx, goqu.Ex{"agent_sessions.id": id})
}

func (a *sessions) GetAgentSessionByPRN(ctx context.Context, prn string) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "db.GetAgentSessionByPRN")
	defer span.End()

	path, err := models.AgentSessionResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return a.GetAgentSessionByID(ctx, gid.FromGlobalID(path))
}

func (a *sessions) GetAgentSessions(ctx context.Context, input *GetAgentSessionsInput) (*AgentSessionsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetAgentSessions")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.AgentID != nil {
			ex = ex.Append(goqu.I("agent_sessions.agent_id").Eq(*input.Filter.AgentID))
		}

		if len(input.Filter.AgentSessionIDs) > 0 {
			ex = ex.Append(goqu.I("agent_sessions.id").In(input.Filter.AgentSessionIDs))
		}
	}

	query := dialect.From(goqu.T("agent_sessions")).
		Select(a.getSelectFields()...).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "agent_sessions", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)

	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, a.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.AgentSession{}
	for rows.Next() {
		item, err := scanAgentSession(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := AgentSessionsResult{
		PageInfo:      rows.GetPageInfo(),
		AgentSessions: results,
	}

	return &result, nil
}

func (a *sessions) CreateAgentSession(ctx context.Context, session *models.AgentSession) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "db.CreateAgentSession")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("agent_sessions").
		Prepared(true).
		With("agent_sessions",
			dialect.Insert("agent_sessions").Rows(
				goqu.Record{
					"id":                newResourceID(),
					"version":           initialResourceVersion,
					"created_at":        timestamp,
					"updated_at":        timestamp,
					"agent_id":          session.AgentID,
					"last_contacted_at": session.LastContactTimestamp,
					"error_count":       session.ErrorCount,
					"internal":          session.Internal,
				}).Returning("*"),
		).Select(a.getSelectFields()...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdAgentSession, err := scanAgentSession(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				return nil, errors.New("invalid agent ID", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdAgentSession, nil
}

func (a *sessions) UpdateAgentSession(ctx context.Context, session *models.AgentSession) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateAgentSession")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("agent_sessions").
		Prepared(true).
		With("agent_sessions",
			dialect.Update("agent_sessions").
				Set(goqu.Record{
					"version":           goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":        timestamp,
					"last_contacted_at": session.LastContactTimestamp,
					"error_count":       session.ErrorCount,
				}).Where(goqu.Ex{"id": session.Metadata.ID, "version": session.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedAgentSession, err := scanAgentSession(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedAgentSession, nil
}

func (a *sessions) DeleteAgentSession(ctx context.Context, session *models.AgentSession) error {
	ctx, span := tracer.Start(ctx, "db.DeleteAgentSession")
	defer span.End()

	sql, args, err := dialect.From("agent_sessions").
		Prepared(true).
		With("agent_sessions",
			dialect.Delete("agent_sessions").
				Where(goqu.Ex{"id": session.Metadata.ID, "version": session.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	_, err = scanAgentSession(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (a *sessions) getAgentSession(ctx context.Context, exp goqu.Ex) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "db.getAgentSession")
	defer span.End()

	query := dialect.From(goqu.T("agent_sessions")).
		Prepared(true).
		Select(a.getSelectFields()...).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	session, err := scanAgentSession(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, "invalid ID; %s", pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return session, nil
}

func (*sessions) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range sessionFieldList {
		selectFields = append(selectFields, fmt.Sprintf("agent_sessions.%s", field))
	}

	return selectFields
}

func scanAgentSession(row scanner) (*models.AgentSession, error) {
	session := &models.AgentSession{}

	fields := []interface{}{
		&session.Metadata.ID,
		&session.Metadata.CreationTimestamp,
		&session.Metadata.LastUpdatedTimestamp,
		&session.Metadata.Version,
		&session.AgentID,
		&session.LastContactTimestamp,
		&session.ErrorCount,
		&session.Internal,
	}

	err := row.Scan(fields...)
	if err != nil {
		return nil, err
	}

	session.Metadata.PRN = models.AgentSessionResource.BuildPRN(gid.ToGlobalID(gid.AgentSessionType, session.Metadata.ID))

	return session, nil
}
