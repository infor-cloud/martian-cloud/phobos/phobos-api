package db

//go:generate go tool mockery --name VCSProviders --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
)

// VCSProviders encapsulates the logic to access VCS providers from the database.
type VCSProviders interface {
	GetProviderByID(ctx context.Context, id string) (*models.VCSProvider, error)
	GetProviderByPRN(ctx context.Context, prn string) (*models.VCSProvider, error)
	GetProviderByOAuthState(ctx context.Context, state string) (*models.VCSProvider, error)
	GetProviders(ctx context.Context, input *GetVCSProvidersInput) (*VCSProvidersResult, error)
	CreateProvider(ctx context.Context, provider *models.VCSProvider) (*models.VCSProvider, error)
	UpdateProvider(ctx context.Context, provider *models.VCSProvider) (*models.VCSProvider, error)
	DeleteProvider(ctx context.Context, provider *models.VCSProvider) error
}

// VCSProviderSortableField represents the field that a VCS provider can be sorted by.
type VCSProviderSortableField string

// VCSProviderSortableField constants
const (
	VCSProviderSortableFieldCreatedAtAsc  VCSProviderSortableField = "CREATED_AT_ASC"
	VCSProviderSortableFieldCreatedAtDesc VCSProviderSortableField = "CREATED_AT_DESC"
	VCSProviderSortableFieldUpdatedAtAsc  VCSProviderSortableField = "UPDATED_AT_ASC"
	VCSProviderSortableFieldUpdatedAtDesc VCSProviderSortableField = "UPDATED_AT_DESC"
)

func (sf VCSProviderSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case VCSProviderSortableFieldCreatedAtAsc, VCSProviderSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "vcs_providers", Col: "created_at"}
	case VCSProviderSortableFieldUpdatedAtAsc, VCSProviderSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "vcs_providers", Col: "updated_at"}
	default:
		return nil
	}
}

func (sf VCSProviderSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// VCSProviderFilter contains the supported fields for filtering VCSProvider resources.
type VCSProviderFilter struct {
	Search            *string
	OrganizationID    *string
	ProjectID         *string
	VCSProviderScopes []models.ScopeType
	VCSProviderIDs    []string
}

// GetVCSProvidersInput is the input for listing VCS providers.
type GetVCSProvidersInput struct {
	// Sort specifies the field to sort on and direction
	Sort *VCSProviderSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *VCSProviderFilter
}

// validate validates the input for GetVCSProvidersInput
func (i *GetVCSProvidersInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// VCSProvidersResult contains the response data and page information.
type VCSProvidersResult struct {
	PageInfo     *pagination.PageInfo
	VCSProviders []models.VCSProvider
}

type vcsProviders struct {
	dbClient *Client
}

var vcsProvidersFieldList = append(
	metadataFieldList,
	"created_by",
	"name",
	"description",
	"organization_id",
	"project_id",
	"auth_type",
	"type",
	"scope",
	"url",
	"oauth_client_id",
	"oauth_client_secret",
	"oauth_state",
	"oauth_access_token",
	"oauth_refresh_token",
	"oauth_access_token_expires_at",
	"extra_oauth_scopes",
	"personal_access_token",
)

// NewVCSProviders returns an instance of the VCSProviders interface.
func NewVCSProviders(dbClient *Client) VCSProviders {
	return &vcsProviders{dbClient: dbClient}
}

func (vp *vcsProviders) GetProviderByID(ctx context.Context, id string) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "db.GetProviderByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	return vp.getProvider(ctx, goqu.Ex{"vcs_providers.id": id})
}

func (vp *vcsProviders) GetProviderByPRN(ctx context.Context, prn string) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "db.GetProviderByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	path, err := models.VCSProviderResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, err
	}

	ex := goqu.Ex{}
	parts := strings.Split(path, "/")

	switch len(parts) {
	case 2:
		// Organization VCS provider.
		ex["organizations.name"] = parts[0]
		ex["vcs_providers.name"] = parts[1]
		ex["vcs_providers.scope"] = models.OrganizationScope
	case 3:
		// Project VCS provider.
		ex["organizations.name"] = parts[0]
		ex["projects.name"] = parts[1]
		ex["vcs_providers.name"] = parts[2]
		ex["vcs_providers.scope"] = models.ProjectScope
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return vp.getProvider(ctx, ex)
}

func (vp *vcsProviders) GetProviderByOAuthState(ctx context.Context, state string) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "db.GetProviderByOAuthState")
	defer span.End()

	return vp.getProvider(ctx, goqu.Ex{"vcs_providers.oauth_state": state})
}

func (vp *vcsProviders) GetProviders(ctx context.Context, input *GetVCSProvidersInput) (*VCSProvidersResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetProviders")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("vcs_providers.name").ILike("%" + *input.Filter.Search + "%"))
		}

		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("vcs_providers.organization_id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(
				// Use an OR condition to get both organization and project VCS providers.
				goqu.Or(
					goqu.I("vcs_providers.project_id").Eq(*input.Filter.ProjectID),
					goqu.And(
						goqu.I("vcs_providers.organization_id").In(
							dialect.From("projects").
								Select("org_id").
								Where(goqu.I("id").Eq(*input.Filter.ProjectID)),
						),
						goqu.I("vcs_providers.scope").Eq(models.OrganizationScope),
					),
				),
			)
		}

		if len(input.Filter.VCSProviderScopes) > 0 {
			ex = ex.Append(goqu.I("vcs_providers.scope").In(input.Filter.VCSProviderScopes))
		}

		if len(input.Filter.VCSProviderIDs) > 0 {
			ex = ex.Append(goqu.I("vcs_providers.id").In(input.Filter.VCSProviderIDs))
		}
	}

	query := dialect.From("vcs_providers").
		Select(vp.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("vcs_providers.organization_id")))).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("vcs_providers.project_id")))).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "vcs_providers", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, vp.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.VCSProvider{}
	for rows.Next() {
		item, err := scanVCSProvider(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &VCSProvidersResult{
		PageInfo:     rows.GetPageInfo(),
		VCSProviders: results,
	}

	return result, nil
}

func (vp *vcsProviders) CreateProvider(ctx context.Context, provider *models.VCSProvider) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "db.CreateProvider")
	defer span.End()

	timestamp := currentTime()

	extraOAuthScopes, err := json.Marshal(provider.ExtraOAuthScopes)
	if err != nil {
		return nil, err
	}

	sql, args, err := dialect.From("vcs_providers").
		Prepared(true).
		With("vcs_providers",
			dialect.Insert("vcs_providers").Rows(
				goqu.Record{
					"id":                            newResourceID(),
					"version":                       initialResourceVersion,
					"created_at":                    timestamp,
					"updated_at":                    timestamp,
					"created_by":                    provider.CreatedBy,
					"name":                          provider.Name,
					"description":                   provider.Description,
					"organization_id":               provider.OrganizationID,
					"project_id":                    provider.ProjectID,
					"auth_type":                     provider.AuthType,
					"type":                          provider.Type,
					"scope":                         provider.Scope,
					"url":                           provider.URL.String(),
					"oauth_client_id":               provider.OAuthClientID,
					"oauth_client_secret":           provider.OAuthClientSecret,
					"oauth_state":                   provider.OAuthState,
					"oauth_access_token":            provider.OAuthAccessToken,
					"oauth_refresh_token":           provider.OAuthRefreshToken,
					"oauth_access_token_expires_at": provider.OAuthAccessTokenExpiresAt,
					"extra_oauth_scopes":            extraOAuthScopes,
					"personal_access_token":         provider.PersonalAccessToken,
				}).Returning("*"),
		).Select(vp.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("vcs_providers.organization_id")))).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("vcs_providers.project_id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdProvider, err := scanVCSProvider(vp.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("vcs provider already exists", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_vcs_providers_organization_id":
					return nil, errors.New("organization does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_vcs_providers_project_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdProvider, nil
}

func (vp *vcsProviders) UpdateProvider(ctx context.Context, provider *models.VCSProvider) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateProvider")
	defer span.End()

	timestamp := currentTime()

	extraOAuthScopes, err := json.Marshal(provider.ExtraOAuthScopes)
	if err != nil {
		return nil, err
	}

	sql, args, err := dialect.From("vcs_providers").
		Prepared(true).
		With("vcs_providers",
			dialect.Update("vcs_providers").Set(
				goqu.Record{
					"version":                       goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":                    timestamp,
					"description":                   provider.Description,
					"oauth_client_id":               provider.OAuthClientID,
					"oauth_client_secret":           provider.OAuthClientSecret,
					"oauth_state":                   provider.OAuthState,
					"oauth_access_token":            provider.OAuthAccessToken,
					"oauth_refresh_token":           provider.OAuthRefreshToken,
					"oauth_access_token_expires_at": provider.OAuthAccessTokenExpiresAt,
					"extra_oauth_scopes":            extraOAuthScopes,
					"personal_access_token":         provider.PersonalAccessToken,
				}).Where(goqu.Ex{"id": provider.Metadata.ID, "version": provider.Metadata.Version}).
				Returning("*"),
		).Select(vp.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("vcs_providers.organization_id")))).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("vcs_providers.project_id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedProvider, err := scanVCSProvider(vp.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}

		return nil, errors.Wrap(err, "failed to execute query")
	}

	return updatedProvider, nil
}

func (vp *vcsProviders) DeleteProvider(ctx context.Context, provider *models.VCSProvider) error {
	ctx, span := tracer.Start(ctx, "db.DeleteProvider")
	defer span.End()

	sql, args, err := dialect.From("vcs_providers").
		Prepared(true).
		With("vcs_providers",
			dialect.Delete("vcs_providers").
				Where(goqu.Ex{"id": provider.Metadata.ID, "version": provider.Metadata.Version}).
				Returning("*"),
		).Select(vp.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("vcs_providers.organization_id")))).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("vcs_providers.project_id")))).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanVCSProvider(vp.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query")
	}

	return nil
}

func (vp *vcsProviders) getProvider(ctx context.Context, exp goqu.Ex) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "db.getProvider")
	defer span.End()

	sql, args, err := dialect.From(goqu.T("vcs_providers")).
		Prepared(true).
		Select(vp.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("vcs_providers.organization_id")))).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("vcs_providers.project_id")))).
		Where(exp).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	provider, err := scanVCSProvider(vp.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, "invalid ID; %s", pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return provider, nil
}

func (vp *vcsProviders) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range vcsProvidersFieldList {
		selectFields = append(selectFields, fmt.Sprintf("vcs_providers.%s", field))
	}

	selectFields = append(selectFields, "organizations.name")
	selectFields = append(selectFields, "projects.name")

	return selectFields
}

func scanVCSProvider(row scanner) (*models.VCSProvider, error) {
	var providerURL, organizationName string
	var projectName *string
	vp := &models.VCSProvider{}

	fields := []interface{}{
		&vp.Metadata.ID,
		&vp.Metadata.CreationTimestamp,
		&vp.Metadata.LastUpdatedTimestamp,
		&vp.Metadata.Version,
		&vp.CreatedBy,
		&vp.Name,
		&vp.Description,
		&vp.OrganizationID,
		&vp.ProjectID,
		&vp.AuthType,
		&vp.Type,
		&vp.Scope,
		&providerURL,
		&vp.OAuthClientID,
		&vp.OAuthClientSecret,
		&vp.OAuthState,
		&vp.OAuthAccessToken,
		&vp.OAuthRefreshToken,
		&vp.OAuthAccessTokenExpiresAt,
		&vp.ExtraOAuthScopes,
		&vp.PersonalAccessToken,
		&organizationName,
		&projectName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	parsedURL, err := url.Parse(providerURL)
	if err != nil {
		return nil, err
	}
	vp.URL = *parsedURL

	switch vp.Scope {
	case models.OrganizationScope:
		vp.Metadata.PRN = models.VCSProviderResource.BuildPRN(organizationName, vp.Name)
	case models.ProjectScope:
		vp.Metadata.PRN = models.VCSProviderResource.BuildPRN(organizationName, *projectName, vp.Name)
	default:
		return nil, errors.New("unexpected vcs provider scope %s", vp.Scope)
	}

	return vp, nil
}
