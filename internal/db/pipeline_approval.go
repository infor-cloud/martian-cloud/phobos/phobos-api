package db

//go:generate go tool mockery --name PipelineApprovals --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// PipelineApprovalFilter contains the supported fields for filtering PipelineApproval resources
type PipelineApprovalFilter struct {
	PipelineID       *string
	TaskPath         *string
	UserID           *string
	ServiceAccountID *string
	Type             *models.PipelineApprovalType
}

// PipelineApprovalSortableField represents the fields that a pipeline approval can be sorted by
type PipelineApprovalSortableField string

// PipelineApprovalSortableField constants
const (
	PipelineApprovalSortableFieldUpdatedAtAsc  PipelineApprovalSortableField = "UPDATED_AT_ASC"
	PipelineApprovalSortableFieldUpdatedAtDesc PipelineApprovalSortableField = "UPDATED_AT_DESC"
)

func (os PipelineApprovalSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case PipelineApprovalSortableFieldUpdatedAtAsc, PipelineApprovalSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "pipeline_approvals", Col: "updated_at"}
	default:
		return nil
	}
}

func (os PipelineApprovalSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetPipelineApprovalsInput is the input for listing pipeline approvals
type GetPipelineApprovalsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *PipelineApprovalSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *PipelineApprovalFilter
}

// PipelineApprovalsResult contains the response data and page information
type PipelineApprovalsResult struct {
	PageInfo          *pagination.PageInfo
	PipelineApprovals []*models.PipelineApproval
}

// PipelineApprovals encapsulates the logic to access pipeline approvals from the DB.
type PipelineApprovals interface {
	CreatePipelineApproval(ctx context.Context, req *models.PipelineApproval) (*models.PipelineApproval, error)
	DeletePipelineApproval(ctx context.Context, req *models.PipelineApproval) error
	GetPipelineApprovals(ctx context.Context, input *GetPipelineApprovalsInput) (*PipelineApprovalsResult, error)
}

type pipelineApprovals struct {
	dbClient *Client
}

var pipelineApprovalFieldList = append(metadataFieldList,
	"pipeline_id",
	"task_path",
	"user_id",
	"service_account_id",
	"type",
	"approval_rule_ids",
)

// NewPipelineApprovals returns an instance of the PipelineApprovals interface
func NewPipelineApprovals(dbClient *Client) PipelineApprovals {
	return &pipelineApprovals{dbClient: dbClient}
}

func (p *pipelineApprovals) CreatePipelineApproval(ctx context.Context, approval *models.PipelineApproval) (*models.PipelineApproval, error) {
	ctx, span := tracer.Start(ctx, "db.CreatePipelineApproval")
	defer span.End()

	timestamp := currentTime()

	ruleIDs, err := json.Marshal(approval.ApprovalRuleIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal approval rule IDs: %w", err)
	}

	sql, args, err := dialect.Insert("pipeline_approvals").
		Prepared(true).
		Rows(goqu.Record{
			"id":                 newResourceID(),
			"version":            initialResourceVersion,
			"created_at":         timestamp,
			"updated_at":         timestamp,
			"pipeline_id":        approval.PipelineID,
			"task_path":          approval.TaskPath,
			"user_id":            approval.UserID,
			"service_account_id": approval.ServiceAccountID,
			"type":               approval.Type,
			"approval_rule_ids":  ruleIDs,
		}).
		Returning(pipelineApprovalFieldList...).ToSQL()

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdOutput, err := scanPipelineApproval(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("approval already exists", errors.WithErrorCode(errors.EConflict))
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdOutput, nil
}

func (p *pipelineApprovals) DeletePipelineApproval(ctx context.Context, req *models.PipelineApproval) error {
	ctx, span := tracer.Start(ctx, "db.DeletePipelineApproval")
	defer span.End()

	sql, args, err := dialect.Delete("pipeline_approvals").
		Prepared(true).
		Where(goqu.Ex{
			"id":      req.Metadata.ID,
			"version": req.Metadata.Version,
		}).
		Returning(pipelineApprovalFieldList...).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = scanPipelineApproval(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *pipelineApprovals) GetPipelineApprovals(ctx context.Context, input *GetPipelineApprovalsInput) (*PipelineApprovalsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineApprovals")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.UserID != nil {
			ex = ex.Append(goqu.I("pipeline_approvals.user_id").Eq(*input.Filter.UserID))
		}

		if input.Filter.ServiceAccountID != nil {
			ex = ex.Append(goqu.I("pipeline_approvals.service_account_id").Eq(*input.Filter.ServiceAccountID))
		}

		if input.Filter.PipelineID != nil {
			ex = ex.Append(goqu.I("pipeline_approvals.pipeline_id").Eq(*input.Filter.PipelineID))
		}

		if input.Filter.TaskPath != nil {
			ex = ex.Append(goqu.I("pipeline_approvals.task_path").Eq(*input.Filter.TaskPath))
		}

		if input.Filter.Type != nil {
			ex = ex.Append(goqu.I("pipeline_approvals.type").Eq(*input.Filter.Type))
		}
	}

	query := dialect.From(goqu.T("pipeline_approvals")).Select(p.getSelectFields()...).Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "pipeline_approvals", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.PipelineApproval{}
	for rows.Next() {
		item, err := scanPipelineApproval(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &PipelineApprovalsResult{
		PageInfo:          rows.GetPageInfo(),
		PipelineApprovals: results,
	}

	return result, nil
}

func (*pipelineApprovals) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range pipelineApprovalFieldList {
		selectFields = append(selectFields, fmt.Sprintf("pipeline_approvals.%s", field))
	}

	return selectFields
}

func scanPipelineApproval(row scanner) (*models.PipelineApproval, error) {
	approval := &models.PipelineApproval{}

	err := row.Scan(
		&approval.Metadata.ID,
		&approval.Metadata.CreationTimestamp,
		&approval.Metadata.LastUpdatedTimestamp,
		&approval.Metadata.Version,
		&approval.PipelineID,
		&approval.TaskPath,
		&approval.UserID,
		&approval.ServiceAccountID,
		&approval.Type,
		&approval.ApprovalRuleIDs,
	)
	if err != nil {
		return nil, err
	}

	approval.Metadata.PRN = models.PipelineApprovalResource.BuildPRN(gid.ToGlobalID(gid.PipelineApprovalType, approval.Metadata.ID))

	return approval, nil
}
