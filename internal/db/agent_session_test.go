//go:build integration

package db

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestGetAgentSessionByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode    errors.CodeType
		name               string
		id                 string
		expectAgentSession bool
	}

	testCases := []testCase{
		{
			name:               "get resource by id",
			id:                 agentSession.Metadata.ID,
			expectAgentSession: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			agentSession, err := testClient.client.AgentSessions.GetAgentSessionByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectAgentSession {
				require.NotNil(t, agentSession)
				assert.Equal(t, test.id, agentSession.Metadata.ID)
			} else {
				assert.Nil(t, agentSession)
			}
		})
	}
}

func TestGetAgentSessionByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode    errors.CodeType
		name               string
		prn                string
		expectAgentSession bool
	}

	testCases := []testCase{
		{
			name:               "get resource by PRN",
			prn:                agentSession.Metadata.PRN,
			expectAgentSession: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.AgentSessionResource.BuildPRN(nonExistentID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			agentSession, err := testClient.client.AgentSessions.GetAgentSessionByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectAgentSession {
				require.NotNil(t, agentSession)
				assert.Equal(t, test.prn, agentSession.Metadata.PRN)
			} else {
				assert.Nil(t, agentSession)
			}
		})
	}
}

func TestCreateAgentSession(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		agentID         string
	}

	testCases := []testCase{
		{
			name:    "successfully create resource",
			agentID: agent.Metadata.ID,
		},
		{
			name:            "create will fail because agent does not exist",
			agentID:         nonExistentID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
				AgentID: test.agentID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, agentSession)
		})
	}
}

func TestUpdateAgentSession(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	currentTime := time.Now().UTC()

	type testCase struct {
		lastContactTimestamp time.Time
		name                 string
		expectErrorCode      errors.CodeType
		version              int
	}

	testCases := []testCase{
		{
			name:                 "successfully update resource",
			lastContactTimestamp: currentTime,
			version:              1,
		},
		{
			name:                 "update will fail because resource version doesn't match",
			lastContactTimestamp: currentTime,
			expectErrorCode:      errors.EOptimisticLock,
			version:              -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualAgentSession, err := testClient.client.AgentSessions.UpdateAgentSession(ctx, &models.AgentSession{
				Metadata: models.ResourceMetadata{
					ID:      agentSession.Metadata.ID,
					Version: test.version,
				},
				LastContactTimestamp: test.lastContactTimestamp,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, actualAgentSession)
			assert.Equal(t, test.lastContactTimestamp.Format(time.RFC3339), actualAgentSession.LastContactTimestamp.Format(time.RFC3339))
		})
	}
}

func TestDeleteAgentSession(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent",
	})
	require.Nil(t, err)

	agentSession, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              agentSession.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      agentSession.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.AgentSessions.DeleteAgentSession(ctx, &models.AgentSession{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestGetAgentSessions(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent1, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent-1",
	})
	require.Nil(t, err)

	agent2, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent-2",
	})
	require.Nil(t, err)

	sessions := make([]*models.AgentSession, 10)
	for i := 0; i < len(sessions); i++ {
		session, aErr := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
			AgentID: agent1.Metadata.ID,
		})
		require.Nil(t, aErr)

		sessions[i] = session
	}

	session, err := testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
		AgentID: agent2.Metadata.ID,
	})
	require.Nil(t, err)

	sessions = append(sessions, session)

	type testCase struct {
		filter            *AgentSessionFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name: "return all sessions for agent 1",
			filter: &AgentSessionFilter{
				AgentID: &agent1.Metadata.ID,
			},
			expectResultCount: len(sessions) - 1,
		},
		{
			name: "return all sessions for agent 2",
			filter: &AgentSessionFilter{
				AgentID: &agent2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "return all sessions matching specific IDs",
			filter: &AgentSessionFilter{
				AgentSessionIDs: []string{sessions[0].Metadata.ID, sessions[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.AgentSessions.GetAgentSessions(ctx, &GetAgentSessionsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.Equal(t, test.expectResultCount, len(result.AgentSessions))
		})
	}
}

func TestGetAgentSessionsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	agent1, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name: "test-agent-1",
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.AgentSessions.CreateAgentSession(ctx, &models.AgentSession{
			AgentID: agent1.Metadata.ID,
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		AgentSessionSortableFieldCreatedAtAsc,
		AgentSessionSortableFieldCreatedAtDesc,
		AgentSessionSortableFieldLastContactedAtAsc,
		AgentSessionSortableFieldLastContactedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := AgentSessionSortableField(sortByField.getValue())

		result, err := testClient.client.AgentSessions.GetAgentSessions(ctx, &GetAgentSessionsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.AgentSessions {
			resourceCopy := resource
			resources = append(resources, &resourceCopy)
		}

		return result.PageInfo, resources, nil
	})
}
