//go:build integration

package db

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (r ToDoItemSortableField) getValue() string {
	return string(r)
}

func TestGetToDoItemByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ProjectID:          project.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CanceledNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	todoItem, err := testClient.client.ToDoItems.CreateToDoItem(ctx, &models.ToDoItem{
		OrganizationID: org.Metadata.ID,
		TargetID:       pipeline.Metadata.ID,
		TargetType:     models.PipelineApprovalTarget,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		prn             string
		expectToDoItem  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by PRN",
			prn:            todoItem.Metadata.PRN,
			expectToDoItem: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.ToDoItemResource.BuildPRN(nonExistentGlobalID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			todoItem, err := testClient.client.ToDoItems.GetToDoItemByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectToDoItem {
				require.NotNil(t, todoItem)
				assert.Equal(t, test.prn, todoItem.Metadata.PRN)
			} else {
				assert.Nil(t, todoItem)
			}
		})
	}
}

func TestGetToDoItems(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org-1",
	})
	require.NoError(t, err)

	project1, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	org2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org-2",
	})
	require.NoError(t, err)

	project2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-2",
		OrgID: org2.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project2.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	// Create a user, team and add it as a member so we can test out whether a user
	// can view todo items via its team.
	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
		Email:    "test-user@phobos.tld",
	})
	require.NoError(t, err)

	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.NoError(t, err)

	_, err = testClient.client.TeamMembers.AddUserToTeam(ctx, &models.TeamMember{
		UserID: user.Metadata.ID,
		TeamID: team.Metadata.ID,
	})
	require.NoError(t, err)

	role, err := testClient.client.Roles.CreateRole(ctx, &models.Role{
		Name: "owner",
	})
	require.NoError(t, err)

	_, err = testClient.client.Memberships.CreateMembership(ctx, &models.Membership{
		UserID:    &user.Metadata.ID,
		ProjectID: &project2.Metadata.ID,
		Scope:     models.ProjectScope,
		RoleID:    role.Metadata.ID,
	})
	require.NoError(t, err)

	taskPath := "pipeline.stage.s1.task.t1"
	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ProjectID:          project2.Metadata.ID,
		Stages: []*models.PipelineStage{
			{
				Name: "s1",
				Path: "pipeline.stage.s1",
				Tasks: []*models.PipelineTask{
					{
						Name: "t1",
						Path: taskPath,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	todoItems := make([]*models.ToDoItem, 10)
	for i := 0; i < len(todoItems); i++ {
		sampleTemplate, cErr := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
			ProjectID: project1.Metadata.ID,
			Status:    models.PipelineTemplateUploaded,
		})
		require.NoError(t, cErr)

		samplePipeline, cErr := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
			PipelineTemplateID: sampleTemplate.Metadata.ID,
			ProjectID:          project1.Metadata.ID,
			Status:             statemachine.CreatedNodeStatus,
			Stages: []*models.PipelineStage{
				{
					Name:   "s1",
					Path:   "pipeline.stage.s1",
					Status: statemachine.CanceledNodeStatus,
					Tasks: []*models.PipelineTask{
						{
							Name:   "t1",
							Path:   "pipeline.stage.s1.task.t1",
							Status: statemachine.CreatedNodeStatus,
						},
					},
				},
			},
		})
		require.NoError(t, cErr)

		todoItem, cErr := testClient.client.ToDoItems.CreateToDoItem(ctx, &models.ToDoItem{
			OrganizationID: org.Metadata.ID,
			ProjectID:      &project1.Metadata.ID,
			TargetID:       samplePipeline.Metadata.ID,
			TargetType:     models.PipelineApprovalTarget,
			TeamIDs:        []string{team.Metadata.ID},
			AutoResolved:   i%2 == 0, // Mark half items resolved.
		})
		require.NoError(t, cErr)

		todoItems[i] = todoItem
	}

	payload, err := json.Marshal(&models.ToDoItemPipelineTaskApprovalPayload{
		TaskPath: taskPath,
	})
	require.NoError(t, err)

	todoItem, aErr := testClient.client.ToDoItems.CreateToDoItem(ctx, &models.ToDoItem{
		OrganizationID:  org2.Metadata.ID,
		ProjectID:       &project2.Metadata.ID,
		TargetID:        pipeline.Metadata.ID,
		UserIDs:         []string{user.Metadata.ID},
		TeamIDs:         []string{team.Metadata.ID},
		ResolvedByUsers: []string{user.Metadata.ID},
		TargetType:      models.PipelineApprovalTarget,
		Payload:         payload,
	})
	require.NoError(t, aErr)

	todoItems = append(todoItems, todoItem)

	type testCase struct {
		filter            *ToDoItemFilter
		name              string
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "all todo items",
			expectResultCount: len(todoItems),
		},
		{
			name: "filter by user id",
			filter: &ToDoItemFilter{
				UserID: &user.Metadata.ID,
			},
			expectResultCount: len(todoItems),
		},
		{
			name: "filter by project",
			filter: &ToDoItemFilter{
				ProjectID: &project2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "filter by organization",
			filter: &ToDoItemFilter{
				OrganizationID: &org.Metadata.ID,
			},
			expectResultCount: len(todoItems) - 1,
		},
		{
			name: "filter by target types",
			filter: &ToDoItemFilter{
				TargetTypes: []models.ToDoItemTargetType{
					models.PipelineApprovalTarget,
				},
			},
			expectResultCount: len(todoItems),
		},
		{
			name: "filter by pipeline target id",
			filter: &ToDoItemFilter{
				PipelineTargetID: &pipeline.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "filter by resolved",
			filter: &ToDoItemFilter{
				Resolved: ptr.Bool(true),
			},
			expectResultCount: (len(todoItems)) / 2,
		},
		{
			name: "filter by todo item ids",
			filter: &ToDoItemFilter{
				ToDoItemIDs: []string{
					todoItems[0].Metadata.ID,
					todoItems[1].Metadata.ID,
					todoItems[2].Metadata.ID,
				},
			},
			expectResultCount: 3,
		},
		{
			name: "filter by payload task path",
			filter: &ToDoItemFilter{
				PayloadFilter: &ToDoItemPayloadFilter{
					PipelineTaskPath: &taskPath,
				},
			},
			expectResultCount: 1,
		},
		{
			name: "filter by membership requirement",
			filter: &ToDoItemFilter{
				MembershipRequirement: &ToDoItemMembershipRequirement{
					UserID: &user.Metadata.ID,
				},
			},
			expectResultCount: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			resp, err := testClient.client.ToDoItems.GetToDoItems(ctx, &GetToDoItemsInput{
				Filter: test.filter,
			})
			require.NoError(t, err)

			assert.Len(t, resp.ToDoItems, test.expectResultCount)
		})
	}
}

func TestGetToDoItemsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org-1",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		pipeline, aErr := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
			PipelineTemplateID: pipelineTemplate.Metadata.ID,
			ProjectID:          project.Metadata.ID,
			Status:             statemachine.CreatedNodeStatus,
			Stages: []*models.PipelineStage{
				{
					Name:   "s1",
					Path:   "pipeline.stage.s1",
					Status: statemachine.CanceledNodeStatus,
					Tasks: []*models.PipelineTask{
						{
							Name:   "t1",
							Path:   "pipeline.stage.s1.task.t1",
							Status: statemachine.CreatedNodeStatus,
						},
					},
				},
			},
		})
		require.NoError(t, aErr)

		_, aErr = testClient.client.ToDoItems.CreateToDoItem(ctx, &models.ToDoItem{
			OrganizationID: org.Metadata.ID,
			ProjectID:      &project.Metadata.ID,
			TargetID:       pipeline.Metadata.ID,
			TargetType:     models.PipelineApprovalTarget,
		})
		require.Nil(t, aErr)
	}

	sortableFields := []sortableField{
		ToDoItemSortableFieldCreatedAtAsc,
		ToDoItemSortableFieldCreatedAtDesc,
		ToDoItemSortableFieldUpdatedAtAsc,
		ToDoItemSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := ToDoItemSortableField(sortByField.getValue())

		result, err := testClient.client.ToDoItems.GetToDoItems(ctx, &GetToDoItemsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.ToDoItems {
			resources = append(resources, resource)
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreateToDoItem(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org-1",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
		Email:    "test-user@phobos.tld",
	})
	require.NoError(t, err)

	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	taskPath := "pipeline.stage.s1.task.t1"
	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ProjectID:          project.Metadata.ID,
		Stages: []*models.PipelineStage{
			{
				Name: "s1",
				Path: "pipeline.stage.s1",
				Tasks: []*models.PipelineTask{
					{
						Name: "t1",
						Path: taskPath,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	createPayload := func(p any) []byte {
		payloadBuf, jErr := json.Marshal(p)
		require.NoError(t, jErr)
		return payloadBuf
	}

	type testCase struct {
		name            string
		item            *models.ToDoItem
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create resource",
			item: &models.ToDoItem{
				OrganizationID: org.Metadata.ID,
				ProjectID:      &project.Metadata.ID,
				TargetID:       pipeline.Metadata.ID,
				TargetType:     models.PipelineApprovalTarget,
				UserIDs:        []string{user.Metadata.ID},
				TeamIDs:        []string{team.Metadata.ID},
				Resolvable:     true,
				Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
					TaskPath: taskPath,
				}),
			},
		},
		{
			name: "create will fail because organization does not exist",
			item: &models.ToDoItem{
				OrganizationID: nonExistentID,
				ProjectID:      &project.Metadata.ID,
				TargetID:       pipeline.Metadata.ID,
				TargetType:     models.PipelineApprovalTarget,
				UserIDs:        []string{user.Metadata.ID},
				TeamIDs:        []string{team.Metadata.ID},
				Resolvable:     true,
				Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
					TaskPath: taskPath,
				}),
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "create will fail because project does not exist",
			item: &models.ToDoItem{
				OrganizationID: org.Metadata.ID,
				ProjectID:      ptr.String(nonExistentID),
				TargetID:       pipeline.Metadata.ID,
				TargetType:     models.PipelineApprovalTarget,
				UserIDs:        []string{user.Metadata.ID},
				TeamIDs:        []string{team.Metadata.ID},
				Resolvable:     true,
				Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
					TaskPath: taskPath,
				}),
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "create will fail because target id does not exist",
			item: &models.ToDoItem{
				OrganizationID: org.Metadata.ID,
				ProjectID:      &project.Metadata.ID,
				TargetID:       nonExistentID,
				TargetType:     models.PipelineApprovalTarget,
				UserIDs:        []string{user.Metadata.ID},
				TeamIDs:        []string{team.Metadata.ID},
				Resolvable:     true,
				Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
					TaskPath: taskPath,
				}),
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "create will fail because target type is unknown",
			item: &models.ToDoItem{
				OrganizationID: org.Metadata.ID,
				ProjectID:      &project.Metadata.ID,
				TargetID:       pipeline.Metadata.ID,
				TargetType:     models.ToDoItemTargetType("unknown"),
				UserIDs:        []string{user.Metadata.ID},
				TeamIDs:        []string{team.Metadata.ID},
				Resolvable:     true,
				Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
					TaskPath: taskPath,
				}),
			},
			expectErrorCode: errors.EInternal,
		},
		{
			name: "create will fail because user does not exist",
			item: &models.ToDoItem{
				OrganizationID: org.Metadata.ID,
				ProjectID:      &project.Metadata.ID,
				TargetID:       pipeline.Metadata.ID,
				TargetType:     models.PipelineApprovalTarget,
				UserIDs:        []string{nonExistentID},
				TeamIDs:        []string{team.Metadata.ID},
				Resolvable:     true,
				Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
					TaskPath: taskPath,
				}),
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "create will fail because team does not exist",
			item: &models.ToDoItem{
				OrganizationID: org.Metadata.ID,
				ProjectID:      &project.Metadata.ID,
				TargetID:       pipeline.Metadata.ID,
				TargetType:     models.PipelineApprovalTarget,
				UserIDs:        []string{user.Metadata.ID},
				TeamIDs:        []string{nonExistentID},
				Resolvable:     true,
				Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
					TaskPath: taskPath,
				}),
			},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			toDoItem, err := testClient.client.ToDoItems.CreateToDoItem(ctx, test.item)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			assert.NotNil(t, toDoItem)
		})
	}
}

func TestUpdateToDoItem(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org-1",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
		Email:    "test-user@phobos.tld",
	})
	require.NoError(t, err)

	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	taskPath := "pipeline.stage.s1.task.t1"
	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ProjectID:          project.Metadata.ID,
		Stages: []*models.PipelineStage{
			{
				Name: "s1",
				Path: "pipeline.stage.s1",
				Tasks: []*models.PipelineTask{
					{
						Name: "t1",
						Path: taskPath,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	payloadBuf, jErr := json.Marshal(&models.ToDoItemPipelineTaskApprovalPayload{
		TaskPath: taskPath,
	})
	require.NoError(t, jErr)

	todoItem, err := testClient.client.ToDoItems.CreateToDoItem(ctx, &models.ToDoItem{
		OrganizationID: org.Metadata.ID,
		ProjectID:      &project.Metadata.ID,
		TargetID:       pipeline.Metadata.ID,
		TargetType:     models.PipelineApprovalTarget,
		UserIDs:        []string{user.Metadata.ID},
		TeamIDs:        []string{team.Metadata.ID},
		Resolvable:     true,
		Payload:        payloadBuf,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "successfully update resource",
			version: todoItem.Metadata.Version,
		},
		{
			name:            "update will fail because of resource version mismatch",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualToDoItem, aErr := testClient.client.ToDoItems.UpdateToDoItem(ctx, &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID:      todoItem.Metadata.ID,
					Version: test.version,
				},
				AutoResolved: true,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(aErr))
				return
			}

			require.NoError(t, aErr)
			require.NotNil(t, actualToDoItem)
			assert.True(t, actualToDoItem.AutoResolved)
		})
	}
}
