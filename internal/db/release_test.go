//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (r ReleaseSortableField) getValue() string {
	return string(r)
}

func TestGetReleaseByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID:       proj.Metadata.ID,
		SemanticVersion: "0.1.0",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		id              string
		expectRelease   bool
	}

	testCases := []testCase{
		{
			name:          "get resource by id",
			id:            release.Metadata.ID,
			expectRelease: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualRelease, err := testClient.client.Releases.GetReleaseByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectRelease {
				require.NotNil(t, actualRelease)
				assert.Equal(t, test.id, actualRelease.Metadata.ID)
			} else {
				assert.Nil(t, actualRelease)
			}
		})
	}
}

func TestGetReleaseByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID:       proj.Metadata.ID,
		SemanticVersion: "0.1.0",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		prn             string
		expectRelease   bool
	}

	testCases := []testCase{
		{
			name:          "get resource by PRN",
			prn:           release.Metadata.PRN,
			expectRelease: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.ReleaseResource.BuildPRN(nonExistentID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			release, err := testClient.client.Releases.GetReleaseByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectRelease {
				require.NotNil(t, release)
				assert.Equal(t, test.prn, release.Metadata.PRN)
			} else {
				assert.Nil(t, release)
			}
		})
	}
}

func TestGetReleases(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project1, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	project2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-2",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	releases := make([]*models.Release, 10)
	for i := 0; i < len(releases); i++ {
		release, aErr := testClient.client.Releases.CreateRelease(ctx, &models.Release{
			SemanticVersion: fmt.Sprintf("0.0.%d", i),
			ProjectID:       project1.Metadata.ID,
		})
		require.Nil(t, aErr)

		releases[i] = release
	}

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		SemanticVersion: fmt.Sprintf("0.0.%d", len(releases)),
		ProjectID:       project2.Metadata.ID,
	})
	require.Nil(t, err)

	releases = append(releases, release)

	type testCase struct {
		filter            *ReleaseFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name: "return all releases in project 1",
			filter: &ReleaseFilter{
				ProjectID: &project1.Metadata.ID,
			},
			expectResultCount: len(releases) - 1,
		},
		{
			name: "return all releases in project 2",
			filter: &ReleaseFilter{
				ProjectID: &project2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "return all releases matching specific IDs",
			filter: &ReleaseFilter{
				ReleaseIDs: []string{releases[0].Metadata.ID, releases[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.Releases.GetReleases(ctx, &GetReleasesInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.Equal(t, test.expectResultCount, len(result.Releases))
		})
	}
}

func TestGetReleasesWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.Releases.CreateRelease(ctx, &models.Release{
			SemanticVersion: fmt.Sprintf("0.0.%d", i),
			ProjectID:       project.Metadata.ID,
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		ReleaseSortableFieldUpdatedAtAsc,
		ReleaseSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := ReleaseSortableField(sortByField.getValue())

		result, err := testClient.client.Releases.GetReleases(ctx, &GetReleasesInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.Releases {
			resources = append(resources, result.Releases[ix])
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreateRelease(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
		Email:    "test-user@email.tld",
	})
	require.Nil(t, err)

	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.Nil(t, err)

	type testCase struct {
		name                   string
		expectErrorCode        errors.CodeType
		releaseSemanticVersion string
		projectID              string
	}

	testCases := []testCase{
		{
			name:                   "successfully create resource",
			projectID:              project.Metadata.ID,
			releaseSemanticVersion: "0.1.0",
		},
		{
			name:                   "duplicate release version",
			projectID:              project.Metadata.ID,
			releaseSemanticVersion: "0.1.0",
			expectErrorCode:        errors.EConflict,
		},
		{
			name:                   "create will fail because project does not exist",
			projectID:              nonExistentID,
			releaseSemanticVersion: "0.2.0",
			expectErrorCode:        errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
				SemanticVersion:    test.releaseSemanticVersion,
				ProjectID:          test.projectID,
				UserParticipantIDs: []string{user.Metadata.ID},
				TeamParticipantIDs: []string{team.Metadata.ID},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, release)
			assert.Equal(t, test.releaseSemanticVersion, release.SemanticVersion)
			assert.Equal(t, test.projectID, release.ProjectID)
			assert.Len(t, release.UserParticipantIDs, 1)
		})
	}
}

func TestUpdateRelease(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID:       project.Metadata.ID,
		SemanticVersion: "0.1.0",
	})
	require.Nil(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
		Email:    "test-user@tld",
	})
	require.Nil(t, err)

	dueDate := currentTime()

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		dueDate         *time.Time
		users           []string
		version         int
	}

	testCases := []testCase{
		{
			name:    "successfully update resource",
			dueDate: &dueDate,
			users:   []string{user.Metadata.ID},
			version: 1,
		},
		{
			name:            "update will fail because resource version doesn't match",
			dueDate:         &dueDate,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualRelease, err := testClient.client.Releases.UpdateRelease(ctx, &models.Release{
				Metadata: models.ResourceMetadata{
					ID:      release.Metadata.ID,
					Version: test.version,
				},
				DueDate:            test.dueDate,
				UserParticipantIDs: test.users,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, actualRelease)
			assert.Equal(t, test.dueDate, actualRelease.DueDate)
			assert.Equal(t, test.users, actualRelease.UserParticipantIDs)
		})
	}
}

func TestDeleteRelease(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID:       project.Metadata.ID,
		SemanticVersion: "0.1.0",
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              release.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      release.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.Releases.DeleteRelease(ctx, &models.Release{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}
