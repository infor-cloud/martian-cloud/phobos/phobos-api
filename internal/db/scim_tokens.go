package db

//go:generate go tool mockery --name SCIMTokens --inpackage --case underscore

import (
	"context"
	"fmt"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// SCIMTokens encapsulates the logic to access SCIM tokens from the database
type SCIMTokens interface {
	GetTokenByNonce(ctx context.Context, nonce string) (*models.SCIMToken, error)
	GetTokens(ctx context.Context) ([]models.SCIMToken, error)
	CreateToken(ctx context.Context, token *models.SCIMToken) (*models.SCIMToken, error)
	DeleteToken(ctx context.Context, token *models.SCIMToken) error
}

type scimTokens struct {
	dbClient *Client
}

var scimTokensFieldList = append(metadataFieldList, "created_by", "nonce")

// NewSCIMTokens returns an instance of the SCIMTokens interface.
func NewSCIMTokens(dbClient *Client) SCIMTokens {
	return &scimTokens{dbClient: dbClient}
}

func (s *scimTokens) GetTokenByNonce(ctx context.Context, nonce string) (*models.SCIMToken, error) {
	ctx, span := tracer.Start(ctx, "db.GetTokenByNonce")
	defer span.End()

	return s.getToken(ctx, goqu.Ex{"scim_tokens.nonce": nonce})
}

func (s *scimTokens) GetTokens(ctx context.Context) ([]models.SCIMToken, error) {
	ctx, span := tracer.Start(ctx, "db.GetTokens")
	defer span.End()

	sql, args, err := dialect.From("scim_tokens").
		Prepared(true).
		Select(s.getSelectFields()...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	rows, err := s.dbClient.getConnection(ctx).Query(ctx, sql, args...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	results := []models.SCIMToken{}
	for rows.Next() {
		item, err := scanSCIMToken(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	return results, nil
}

func (s *scimTokens) CreateToken(ctx context.Context, token *models.SCIMToken) (*models.SCIMToken, error) {
	ctx, span := tracer.Start(ctx, "db.CreateToken")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Insert("scim_tokens").
		Prepared(true).
		Rows(goqu.Record{
			"id":         newResourceID(),
			"version":    initialResourceVersion,
			"created_at": timestamp,
			"updated_at": timestamp,
			"created_by": token.CreatedBy,
			"nonce":      token.Nonce,
		}).
		Returning(scimTokensFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdToken, err := scanSCIMToken(s.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("SCIM token already exists", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdToken, nil
}

func (s *scimTokens) DeleteToken(ctx context.Context, token *models.SCIMToken) error {
	ctx, span := tracer.Start(ctx, "db.DeleteToken")
	defer span.End()

	sql, args, err := dialect.Delete("scim_tokens").
		Prepared(true).
		Where(
			goqu.Ex{
				"id":      token.Metadata.ID,
				"version": token.Metadata.Version,
			},
		).Returning(scimTokensFieldList...).ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = scanSCIMToken(s.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (s *scimTokens) getToken(ctx context.Context, exp exp.Ex) (*models.SCIMToken, error) {
	sql, args, err := dialect.From("scim_tokens").
		Prepared(true).
		Select(s.getSelectFields()...).
		Where(exp).
		ToSQL()

	if err != nil {
		return nil, err
	}

	token, err := scanSCIMToken(s.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))

	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}
	return token, nil
}

func (s *scimTokens) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range scimTokensFieldList {
		selectFields = append(selectFields, fmt.Sprintf("scim_tokens.%s", field))
	}

	return selectFields
}

func scanSCIMToken(row scanner) (*models.SCIMToken, error) {
	token := &models.SCIMToken{}

	fields := []interface{}{
		&token.Metadata.ID,
		&token.Metadata.CreationTimestamp,
		&token.Metadata.LastUpdatedTimestamp,
		&token.Metadata.Version,
		&token.CreatedBy,
		&token.Nonce,
	}

	err := row.Scan(fields...)
	if err != nil {
		return nil, err
	}

	token.Metadata.PRN = models.SCIMTokenResource.BuildPRN(gid.ToGlobalID(gid.SCIMTokenType, token.Metadata.ID))

	return token, nil
}
