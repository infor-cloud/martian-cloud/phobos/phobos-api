//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (sf PipelineApprovalSortableField) getValue() string {
	return string(sf)
}

func TestCreatePipelineApproval(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		Status:             statemachine.RunningNodeStatus,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.RunningNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:           "t1",
						Path:           "pipeline.stage.s1.task.t1",
						Status:         statemachine.ReadyNodeStatus,
						ApprovalStatus: models.ApprovalApproved,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		pipelineApproval *models.PipelineApproval
	}

	testCases := []testCase{
		{
			name: "successfully create task approval",
			pipelineApproval: &models.PipelineApproval{
				UserID:     &user.Metadata.ID,
				PipelineID: pipeline.Metadata.ID,
				Type:       models.ApprovalTypeTask,
				TaskPath:   ptr.String("pipeline.stage.s1.task.t1"),
			},
		},
		{
			name: "successfully create pipeline approval",
			pipelineApproval: &models.PipelineApproval{
				UserID:     &user.Metadata.ID,
				PipelineID: pipeline.Metadata.ID,
				Type:       models.ApprovalTypePipeline,
			},
		},
		{
			name: "create will fail because pipeline does not exist",
			pipelineApproval: &models.PipelineApproval{
				UserID:     &user.Metadata.ID,
				PipelineID: nonExistentID,
				Type:       models.ApprovalTypeTask,
				TaskPath:   ptr.String("pipeline.stage.s1.task.t1"),
			},
			expectErrorCode: errors.EInternal,
		},
		{
			name: "create will fail because user does not exist",
			pipelineApproval: &models.PipelineApproval{
				UserID:     ptr.String(user.Metadata.ID),
				PipelineID: pipeline.Metadata.ID,
				Type:       models.ApprovalTypeTask,
				TaskPath:   ptr.String("pipeline.stage.s1.task.t1"),
			},
			expectErrorCode: errors.EConflict,
		},
		{
			name: "duplicate task approval will fail",
			pipelineApproval: &models.PipelineApproval{
				UserID:     &user.Metadata.ID,
				PipelineID: pipeline.Metadata.ID,
				Type:       models.ApprovalTypeTask,
				TaskPath:   ptr.String("pipeline.stage.s1.task.t1"),
			},
			expectErrorCode: errors.EConflict,
		},
		{
			name: "duplicate pipeline approval will fail",
			pipelineApproval: &models.PipelineApproval{
				UserID:     &user.Metadata.ID,
				PipelineID: pipeline.Metadata.ID,
				Type:       models.ApprovalTypePipeline,
			},
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualApproval, err := testClient.client.PipelineApprovals.CreatePipelineApproval(ctx, test.pipelineApproval)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, actualApproval)
		})
	}
}

func TestDeletePipelineApproval(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		Status:             statemachine.RunningNodeStatus,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.RunningNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:           "t1",
						Path:           "pipeline.stage.s1.task.t1",
						Status:         statemachine.ReadyNodeStatus,
						ApprovalStatus: models.ApprovalApproved,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
	})
	require.NoError(t, err)

	pipelineApproval, err := testClient.client.PipelineApprovals.CreatePipelineApproval(ctx, &models.PipelineApproval{
		PipelineID: pipeline.Metadata.ID,
		UserID:     &user.Metadata.ID,
		TaskPath:   ptr.String("pipeline.stage.s1.task.t1"),
		Type:       models.ApprovalTypeTask,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              pipelineApproval.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      pipelineApproval.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.PipelineApprovals.DeletePipelineApproval(ctx, &models.PipelineApproval{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestGetPipelineApprovals(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		Status:             statemachine.RunningNodeStatus,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		// Generate a stage with some tasks since we can't approve same task twice.s
		Stages: func() []*models.PipelineStage {
			stage := &models.PipelineStage{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.RunningNodeStatus,
				Tasks:  []*models.PipelineTask{},
			}

			for i := 0; i < 10; i++ {
				stage.Tasks = append(stage.Tasks, &models.PipelineTask{
					Name:           fmt.Sprintf("t%d", i),
					Path:           fmt.Sprintf("pipeline.stage.s1.task.t%d", i),
					ApprovalStatus: models.ApprovalApproved,
					Status:         statemachine.ReadyNodeStatus,
				})
			}

			return []*models.PipelineStage{stage}
		}(),
	})
	require.NoError(t, err)

	pipeline2, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		Status:             statemachine.RunningNodeStatus,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		Stages: []*models.PipelineStage{
			{
				Name:   "s2",
				Path:   "pipeline.stage.s2",
				Status: statemachine.RunningNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:           "sample",
						Path:           "pipeline.stage.s2.task.sample",
						ApprovalStatus: models.ApprovalApproved,
						Status:         statemachine.ReadyNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
	})
	require.NoError(t, err)

	serviceAccount, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-service-account",
		OrganizationID: &organization.Metadata.ID,
		Scope:          models.OrganizationScope,
	})
	require.NoError(t, err)

	approvals := make([]*models.PipelineApproval, 10)
	for i := 0; i < len(approvals); i++ {
		approval, aErr := testClient.client.PipelineApprovals.CreatePipelineApproval(ctx, &models.PipelineApproval{
			UserID:     &user.Metadata.ID,
			PipelineID: pipeline.Metadata.ID,
			TaskPath:   &pipeline.Stages[0].Tasks[i].Path, // Substitute the path from the tasks we created above.
			Type:       models.ApprovalTypeTask,
		})
		require.NoError(t, aErr)

		approvals[i] = approval
	}

	// Do one with service account and a different pipeline.
	approval, err := testClient.client.PipelineApprovals.CreatePipelineApproval(ctx, &models.PipelineApproval{
		ServiceAccountID: &serviceAccount.Metadata.ID,
		PipelineID:       pipeline2.Metadata.ID,
		TaskPath:         &pipeline2.Stages[0].Tasks[0].Path,
		Type:             models.ApprovalTypeTask,
	})
	require.NoError(t, err)

	approvals = append(approvals, approval)

	approvalTypePipeline := models.ApprovalTypePipeline

	type testCase struct {
		filter            *PipelineApprovalFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all approvals",
			filter:            &PipelineApprovalFilter{},
			expectResultCount: len(approvals),
		},
		{
			name: "filter by pipeline 1 id",
			filter: &PipelineApprovalFilter{
				PipelineID: &pipeline.Metadata.ID,
			},
			expectResultCount: len(approvals) - 1,
		},
		{
			name: "filter by pipeline 2 id",
			filter: &PipelineApprovalFilter{
				PipelineID: &pipeline2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "filter by node path",
			filter: &PipelineApprovalFilter{
				TaskPath: &pipeline.Stages[0].Tasks[0].Path,
			},
			expectResultCount: 1,
		},
		{
			name: "filter by user id",
			filter: &PipelineApprovalFilter{
				UserID: &user.Metadata.ID,
			},
			expectResultCount: len(approvals) - 1,
		},
		{
			name: "filter by service account id",
			filter: &PipelineApprovalFilter{
				ServiceAccountID: &serviceAccount.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "filter by task type",
			filter: &PipelineApprovalFilter{
				Type: &approval.Type,
			},
			expectResultCount: len(approvals),
		},
		{
			name: "filter by nested pipeline type",
			filter: &PipelineApprovalFilter{
				Type: &approvalTypePipeline,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.PipelineApprovals.GetPipelineApprovals(ctx, &GetPipelineApprovalsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.Equal(t, test.expectResultCount, len(result.PipelineApprovals))
		})
	}
}

func TestGetPipelineApprovalsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		Status:             statemachine.RunningNodeStatus,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		// Generate a stage with some tasks since we can't approve same task twice.s
		Stages: func() []*models.PipelineStage {
			stage := &models.PipelineStage{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.RunningNodeStatus,
				Tasks:  []*models.PipelineTask{},
			}

			for i := 0; i < 10; i++ {
				stage.Tasks = append(stage.Tasks, &models.PipelineTask{
					Name:           fmt.Sprintf("t%d", i),
					Path:           fmt.Sprintf("pipeline.stage.s1.task.t%d", i),
					ApprovalStatus: models.ApprovalApproved,
					Status:         statemachine.ReadyNodeStatus,
				})
			}

			return []*models.PipelineStage{stage}
		}(),
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, aErr := testClient.client.PipelineApprovals.CreatePipelineApproval(ctx, &models.PipelineApproval{
			UserID:     &user.Metadata.ID,
			PipelineID: pipeline.Metadata.ID,
			TaskPath:   &pipeline.Stages[0].Tasks[i].Path, // Substitute the path from the tasks we created above.
			Type:       models.ApprovalTypeTask,
		})
		require.NoError(t, aErr)
	}

	sortableFields := []sortableField{
		PipelineApprovalSortableFieldUpdatedAtAsc,
		PipelineApprovalSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := PipelineApprovalSortableField(sortByField.getValue())

		result, err := testClient.client.PipelineApprovals.GetPipelineApprovals(ctx, &GetPipelineApprovalsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.PipelineApprovals {
			resources = append(resources, result.PipelineApprovals[ix])
		}

		return result.PageInfo, resources, nil
	})
}
