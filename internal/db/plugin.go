package db

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

//go:generate go tool mockery --name Plugins --inpackage --case underscore

// Plugins encapsulates the logic to access the plugins from the database.
type Plugins interface {
	GetPluginByID(ctx context.Context, id string) (*models.Plugin, error)
	GetPluginByPRN(ctx context.Context, prn string) (*models.Plugin, error)
	GetPlugins(ctx context.Context, input *GetPluginsInput) (*PluginsResult, error)
	CreatePlugin(ctx context.Context, plugin *models.Plugin) (*models.Plugin, error)
	UpdatePlugin(ctx context.Context, plugin *models.Plugin) (*models.Plugin, error)
	DeletePlugin(ctx context.Context, plugin *models.Plugin) error
}

// PluginSortableField represents a sortable field for the Plugin entity.
type PluginSortableField string

// PluginSortableField constants.
const (
	PluginSortableFieldNameAsc       PluginSortableField = "NAME_ASC"
	PluginSortableFieldNameDesc      PluginSortableField = "NAME_DESC"
	PluginSortableFieldUpdatedAtAsc  PluginSortableField = "UPDATED_AT_ASC"
	PluginSortableFieldUpdatedAtDesc PluginSortableField = "UPDATED_AT_DESC"
)

func (sf PluginSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case PluginSortableFieldNameAsc, PluginSortableFieldNameDesc:
		return &pagination.FieldDescriptor{Key: "name", Table: "plugins", Col: "name"}
	case PluginSortableFieldUpdatedAtAsc, PluginSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "plugins", Col: "updated_at"}
	default:
		return nil
	}
}

func (sf PluginSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// PluginFilter represents a filter for the Plugin entity.
type PluginFilter struct {
	Search           *string
	OrganizationID   *string
	UserID           *string
	ServiceAccountID *string
	PluginIDs        []string
}

// GetPluginsInput represents the input of the GetPlugins method.
type GetPluginsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *PluginSortableField
	// PaginationOptions supports cursor-based pagination.
	PaginationOptions *pagination.Options
	// Filter is the list of filters for the query.
	Filter *PluginFilter
}

// validate validates the input for GetPluginsInput
func (i *GetPluginsInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// PluginsResult is the result of the GetPlugins method.
type PluginsResult struct {
	PageInfo *pagination.PageInfo
	Plugins  []models.Plugin
}

type plugins struct {
	dbClient *Client
}

var pluginFieldList = append(metadataFieldList,
	"created_by",
	"organization_id",
	"name",
	"private",
	"repository_url",
)

// NewPlugins returns a new Plugins.
func NewPlugins(dbClient *Client) Plugins {
	return &plugins{dbClient: dbClient}
}

func (p *plugins) GetPluginByID(ctx context.Context, id string) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginByID")
	defer span.End()

	return p.getPlugin(ctx, goqu.Ex{"plugins.id": id})
}

func (p *plugins) GetPluginByPRN(ctx context.Context, prn string) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginByPRN")
	defer span.End()

	path, err := models.PluginResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, err
	}

	parts := strings.Split(path, "/")

	if len(parts) != 2 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getPlugin(ctx, goqu.Ex{
		"organizations.name": parts[0],
		"plugins.name":       parts[1],
	})
}

func (p *plugins) GetPlugins(ctx context.Context, input *GetPluginsInput) (*PluginsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetPlugins")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.Search != nil && *input.Filter.Search != "" {
			search := *input.Filter.Search
			lastDelimiterIndex := strings.LastIndex(search, "/")

			if lastDelimiterIndex != -1 {
				organizationName := search[:lastDelimiterIndex]
				pluginName := search[lastDelimiterIndex+1:]

				if pluginName != "" {
					// Use an AND to ensure that the organization name is matched
					ex = ex.Append(
						goqu.And(
							goqu.I("organizations.name").ILike(organizationName+"%"),
							goqu.I("plugins.name").ILike(pluginName+"%"),
						),
					)
				} else {
					// If the plugin name is empty, then only match the organization name
					ex = ex.Append(goqu.I("organizations.name").ILike(organizationName + "%"))
				}
			} else {
				// We don't know if the search string is an organization name or a plugin name, so we need to match both
				ex = ex.Append(
					goqu.Or(
						goqu.I("organizations.name").ILike(search+"%"),
						goqu.I("plugins.name").ILike(search+"%"),
					),
				)
			}
		}

		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("organizations.id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.UserID != nil {
			ex = ex.Append(
				goqu.Or(
					goqu.I("plugins.private").Eq(false),
					organizationMembershipExpressionBuilder{
						userID: input.Filter.UserID,
					}.build(),
				),
			)
		}

		if input.Filter.ServiceAccountID != nil {
			ex = ex.Append(
				goqu.Or(
					goqu.I("plugins.private").Eq(false),
					organizationMembershipExpressionBuilder{
						serviceAccountID: input.Filter.ServiceAccountID,
					}.build(),
				),
			)
		}

		if len(input.Filter.PluginIDs) > 0 {
			ex = ex.Append(goqu.I("plugins.id").In(input.Filter.PluginIDs))
		}
	}

	query := dialect.From(goqu.T("plugins")).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("plugins.organization_id")))).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "plugins", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Plugin{}
	for rows.Next() {
		item, err := scanPlugin(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &PluginsResult{
		PageInfo: rows.GetPageInfo(),
		Plugins:  results,
	}

	return result, nil
}

func (p *plugins) CreatePlugin(ctx context.Context, plugin *models.Plugin) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "db.CreatePlugin")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("plugins").
		Prepared(true).
		With("plugins",
			dialect.Insert("plugins").Rows(
				goqu.Record{
					"id":              newResourceID(),
					"version":         initialResourceVersion,
					"created_at":      timestamp,
					"updated_at":      timestamp,
					"created_by":      plugin.CreatedBy,
					"organization_id": plugin.OrganizationID,
					"name":            plugin.Name,
					"private":         plugin.Private,
					"repository_url":  plugin.RepositoryURL,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("plugins.organization_id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdPlugin, err := scanPlugin(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("plugin with name %s already exists", plugin.Name, errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdPlugin, nil
}

func (p *plugins) UpdatePlugin(ctx context.Context, plugin *models.Plugin) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "db.UpdatePlugin")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("plugins").
		Prepared(true).
		With("plugins",
			dialect.Update("plugins").
				Set(goqu.Record{
					"version":        goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":     timestamp,
					"private":        plugin.Private,
					"repository_url": plugin.RepositoryURL,
				}).Where(goqu.Ex{"id": plugin.Metadata.ID, "version": plugin.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("plugins.organization_id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedPlugin, err := scanPlugin(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedPlugin, nil
}

func (p *plugins) DeletePlugin(ctx context.Context, plugin *models.Plugin) error {
	ctx, span := tracer.Start(ctx, "db.DeletePlugin")
	defer span.End()

	sql, args, err := dialect.From("plugins").
		Prepared(true).
		With("plugins",
			dialect.Delete("plugins").
				Where(goqu.Ex{"id": plugin.Metadata.ID, "version": plugin.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("plugins.organization_id")))).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanPlugin(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *plugins) getPlugin(ctx context.Context, exp goqu.Ex) (*models.Plugin, error) {
	query := dialect.From(goqu.T("plugins")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("plugins.organization_id")))).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	plugin, err := scanPlugin(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return plugin, nil
}

func (p *plugins) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range pluginFieldList {
		selectFields = append(selectFields, fmt.Sprintf("plugins.%s", field))
	}

	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanPlugin(row scanner) (*models.Plugin, error) {
	var organizationName string
	plugin := &models.Plugin{}

	fields := []interface{}{
		&plugin.Metadata.ID,
		&plugin.Metadata.CreationTimestamp,
		&plugin.Metadata.LastUpdatedTimestamp,
		&plugin.Metadata.Version,
		&plugin.CreatedBy,
		&plugin.OrganizationID,
		&plugin.Name,
		&plugin.Private,
		&plugin.RepositoryURL,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	plugin.Metadata.PRN = models.PluginResource.BuildPRN(organizationName, plugin.Name)

	return plugin, nil
}
