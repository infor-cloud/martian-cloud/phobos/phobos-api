package db

//go:generate go tool mockery --name LifecycleTemplates --inpackage --case underscore

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// LifecycleTemplates encapsulates the logic to access LifecycleTemplates from the database
type LifecycleTemplates interface {
	GetLifecycleTemplateByID(ctx context.Context, id string) (*models.LifecycleTemplate, error)
	GetLifecycleTemplateByPRN(ctx context.Context, prn string) (*models.LifecycleTemplate, error)
	GetLifecycleTemplates(ctx context.Context, input *GetLifecycleTemplatesInput) (*LifecycleTemplatesResult, error)
	CreateLifecycleTemplate(ctx context.Context, lifecycleTemplate *models.LifecycleTemplate) (*models.LifecycleTemplate, error)
	UpdateLifecycleTemplate(ctx context.Context, releaseLifecycleTemplate *models.LifecycleTemplate) (*models.LifecycleTemplate, error)
}

// LifecycleTemplateFilter contains the supported fields for filtering LifecycleTemplate resources
type LifecycleTemplateFilter struct {
	TimeRangeStart          *time.Time
	OrganizationID          *string
	ProjectID               *string
	LifecycleTemplateScopes []models.ScopeType
	LifecycleTemplateIDs    []string
}

// LifecycleTemplateSortableField represents the fields that a lifecycle template can be sorted by
type LifecycleTemplateSortableField string

// LifecycleTemplateSortableField constants
const (
	LifecycleTemplateSortableFieldUpdatedAtAsc  LifecycleTemplateSortableField = "UPDATED_AT_ASC"
	LifecycleTemplateSortableFieldUpdatedAtDesc LifecycleTemplateSortableField = "UPDATED_AT_DESC"
)

func (os LifecycleTemplateSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case LifecycleTemplateSortableFieldUpdatedAtAsc, LifecycleTemplateSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "lifecycle_templates", Col: "updated_at"}
	default:
		return nil
	}
}

func (os LifecycleTemplateSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetLifecycleTemplatesInput is the input for listing lifecycle templates
type GetLifecycleTemplatesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *LifecycleTemplateSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *LifecycleTemplateFilter
}

// LifecycleTemplatesResult contains the response data and page information
type LifecycleTemplatesResult struct {
	PageInfo           *pagination.PageInfo
	LifecycleTemplates []*models.LifecycleTemplate
}

var lifecycleTemplateFieldList = append(metadataFieldList,
	"created_by", "organization_id", "status", "scope", "project_id")

type lifecycleTemplates struct {
	dbClient *Client
}

// NewLifecycleTemplates returns an instance of the LifecycleTemplates interface
func NewLifecycleTemplates(dbClient *Client) LifecycleTemplates {
	return &lifecycleTemplates{dbClient: dbClient}
}

func (p *lifecycleTemplates) GetLifecycleTemplateByID(ctx context.Context, id string) (*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.GetLifecycleTemplateByID")
	defer span.End()

	return p.getLifecycleTemplate(ctx, goqu.Ex{"lifecycle_templates.id": id})
}

func (p *lifecycleTemplates) GetLifecycleTemplateByPRN(ctx context.Context, prn string) (*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.GetLifecycleTemplateByPRN")
	defer span.End()

	path, err := models.LifecycleTemplateResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	ex := goqu.Ex{}

	switch len(parts) {
	case 2:
		// This is an org lifecycle template name.
		ex["organizations.name"] = parts[0]
		ex["lifecycle_templates.id"] = gid.FromGlobalID(parts[1])
		ex["lifecycle_templates.scope"] = models.OrganizationScope
	case 3:
		// This is a project-scoped lifecycle template path.
		ex["organizations.name"] = parts[0]
		ex["projects.name"] = parts[1]
		ex["lifecycle_templates.id"] = gid.FromGlobalID(parts[2])
		ex["lifecycle_templates.scope"] = models.ProjectScope
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getLifecycleTemplate(ctx, ex)
}

func (p *lifecycleTemplates) GetLifecycleTemplates(ctx context.Context, input *GetLifecycleTemplatesInput) (*LifecycleTemplatesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetLifecycleTemplates")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("lifecycle_templates.organization_id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(
				// Use an OR condition to get both organization and project lifecycle templates.
				goqu.Or(
					goqu.I("lifecycle_templates.project_id").Eq(*input.Filter.ProjectID),
					goqu.And(
						goqu.I("lifecycle_templates.organization_id").In(
							dialect.From("projects").
								Select("org_id").
								Where(goqu.I("id").Eq(*input.Filter.ProjectID)),
						),
						goqu.I("lifecycle_templates.scope").Eq(models.OrganizationScope),
					),
				),
			)
		}

		if len(input.Filter.LifecycleTemplateScopes) > 0 {
			ex = ex.Append(goqu.I("lifecycle_templates.scope").In(input.Filter.LifecycleTemplateScopes))
		}

		if input.Filter.TimeRangeStart != nil {
			// Must use UTC here otherwise, queries will return unexpected results.
			ex = ex.Append(goqu.I("lifecycle_templates.created_at").Gte(input.Filter.TimeRangeStart.UTC()))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.LifecycleTemplateIDs) > 0 {
			ex = ex.Append(goqu.I("lifecycle_templates.id").In(input.Filter.LifecycleTemplateIDs))
		}
	}

	query := dialect.From(goqu.T("lifecycle_templates")).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"lifecycle_templates.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"lifecycle_templates.project_id": goqu.I("projects.id")})).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "lifecycle_templates", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.LifecycleTemplate{}
	for rows.Next() {
		item, err := scanLifecycleTemplate(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &LifecycleTemplatesResult{
		PageInfo:           rows.GetPageInfo(),
		LifecycleTemplates: results,
	}

	return result, nil
}

func (p *lifecycleTemplates) CreateLifecycleTemplate(ctx context.Context,
	lifecycleTemplate *models.LifecycleTemplate) (*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.CreateLifecycleTemplate")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("lifecycle_templates").
		Prepared(true).
		With("lifecycle_templates",
			dialect.Insert("lifecycle_templates").
				Rows(goqu.Record{
					"id":              newResourceID(),
					"version":         initialResourceVersion,
					"created_at":      timestamp,
					"updated_at":      timestamp,
					"created_by":      lifecycleTemplate.CreatedBy,
					"organization_id": lifecycleTemplate.OrganizationID,
					"status":          lifecycleTemplate.Status,
					"scope":           lifecycleTemplate.Scope,
					"project_id":      lifecycleTemplate.ProjectID,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"lifecycle_templates.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"lifecycle_templates.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdLifecycleTemplate, err := scanLifecycleTemplate(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_lifecycle_template_organization_id":
					return nil, errors.New("organization does not exist",
						errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_lifecycle_template_project_id":
					return nil, errors.New("project does not exist",
						errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdLifecycleTemplate, nil
}

func (p *lifecycleTemplates) UpdateLifecycleTemplate(ctx context.Context,
	lifecycleTemplate *models.LifecycleTemplate) (*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateLifecycleTemplate")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("lifecycle_templates").
		Prepared(true).
		With("lifecycle_templates",
			dialect.Update("lifecycle_templates").
				Set(
					goqu.Record{
						"version":    goqu.L("? + ?", goqu.C("version"), 1),
						"updated_at": timestamp,
						"status":     lifecycleTemplate.Status,
					}).
				Where(goqu.Ex{"id": lifecycleTemplate.Metadata.ID, "version": lifecycleTemplate.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"lifecycle_templates.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"lifecycle_templates.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedLifecycleTemplate, err := scanLifecycleTemplate(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedLifecycleTemplate, nil
}

func (p *lifecycleTemplates) getLifecycleTemplate(ctx context.Context, exp exp.Expression) (*models.LifecycleTemplate, error) {
	query := dialect.From(goqu.T("lifecycle_templates")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"lifecycle_templates.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"lifecycle_templates.project_id": goqu.I("projects.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	lifecycleTemplate, err := scanLifecycleTemplate(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return lifecycleTemplate, nil
}

func (*lifecycleTemplates) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range lifecycleTemplateFieldList {
		selectFields = append(selectFields, fmt.Sprintf("lifecycle_templates.%s", field))
	}

	selectFields = append(selectFields, "organizations.name", "projects.name")

	return selectFields
}

func scanLifecycleTemplate(row scanner) (*models.LifecycleTemplate, error) {
	var organizationName string
	var projectName sql.NullString
	lifecycleTemplate := &models.LifecycleTemplate{}

	fields := []interface{}{
		&lifecycleTemplate.Metadata.ID,
		&lifecycleTemplate.Metadata.CreationTimestamp,
		&lifecycleTemplate.Metadata.LastUpdatedTimestamp,
		&lifecycleTemplate.Metadata.Version,
		&lifecycleTemplate.CreatedBy,
		&lifecycleTemplate.OrganizationID,
		&lifecycleTemplate.Status,
		&lifecycleTemplate.Scope,
		&lifecycleTemplate.ProjectID,
		&organizationName,
		&projectName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	switch lifecycleTemplate.Scope {
	case models.OrganizationScope:
		lifecycleTemplate.Metadata.PRN = models.LifecycleTemplateResource.BuildPRN(
			organizationName,
			gid.ToGlobalID(gid.LifecycleTemplateType, lifecycleTemplate.Metadata.ID),
		)
	case models.ProjectScope:
		lifecycleTemplate.Metadata.PRN = models.LifecycleTemplateResource.BuildPRN(
			organizationName,
			projectName.String,
			gid.ToGlobalID(gid.LifecycleTemplateType, lifecycleTemplate.Metadata.ID),
		)
	default:
		return nil, fmt.Errorf("unexpected lifecycle template scope: %s", lifecycleTemplate.Scope)
	}

	return lifecycleTemplate, nil
}
