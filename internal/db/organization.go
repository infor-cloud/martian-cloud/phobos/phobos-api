package db

//go:generate go tool mockery --name Organizations --inpackage --case underscore

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Organizations encapsulates the logic to access Organizations from the database
type Organizations interface {
	GetOrganizationByID(ctx context.Context, id string) (*models.Organization, error)
	GetOrganizationByName(ctx context.Context, name string) (*models.Organization, error)
	GetOrganizationByPRN(ctx context.Context, prn string) (*models.Organization, error)
	GetOrganizations(ctx context.Context, input *GetOrganizationsInput) (*OrganizationsResult, error)
	CreateOrganization(ctx context.Context, org *models.Organization) (*models.Organization, error)
	UpdateOrganization(ctx context.Context, org *models.Organization) (*models.Organization, error)
	DeleteOrganization(ctx context.Context, org *models.Organization) error
}

// OrganizationFilter contains the supported fields for filtering Organization resources
type OrganizationFilter struct {
	UserMemberID           *string
	ServiceAccountMemberID *string
	Search                 *string
	OrganizationIDs        []string
}

// OrganizationSortableField represents the fields that an organization can be sorted by
type OrganizationSortableField string

// OrganizationSortableField constants
const (
	OrganizationSortableFieldUpdatedAtAsc  OrganizationSortableField = "UPDATED_AT_ASC"
	OrganizationSortableFieldUpdatedAtDesc OrganizationSortableField = "UPDATED_AT_DESC"
)

func (os OrganizationSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case OrganizationSortableFieldUpdatedAtAsc, OrganizationSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "organizations", Col: "updated_at"}
	default:
		return nil
	}
}

func (os OrganizationSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetOrganizationsInput is the input for listing organizations
type GetOrganizationsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *OrganizationSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *OrganizationFilter
}

// validate validates the input for GetOrganizationsInput
func (i *GetOrganizationsInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// OrganizationsResult contains the response data and page information
type OrganizationsResult struct {
	PageInfo      *pagination.PageInfo
	Organizations []models.Organization
}

var organizationFieldList = append(metadataFieldList, "name", "description", "created_by")

type organizations struct {
	dbClient *Client
}

// NewOrganizations returns an instance of the Organizations interface
func NewOrganizations(dbClient *Client) Organizations {
	return &organizations{dbClient: dbClient}
}

func (o *organizations) GetOrganizationByID(ctx context.Context, id string) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "db.GetOrganizationByID")
	defer span.End()

	return o.getOrganization(ctx, goqu.Ex{"organizations.id": id})
}

func (o *organizations) GetOrganizationByName(ctx context.Context, name string) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "db.GetOrganizationByName")
	defer span.End()

	return o.getOrganization(ctx, goqu.Ex{"organizations.name": name})
}

func (o *organizations) GetOrganizationByPRN(ctx context.Context, prn string) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "db.GetOrganizationByPRN")
	defer span.End()

	path, err := models.OrganizationResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return o.getOrganization(ctx, goqu.Ex{"organizations.name": path})
}

func (o *organizations) GetOrganizations(ctx context.Context, input *GetOrganizationsInput) (*OrganizationsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetOrganizations")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.OrganizationIDs) > 0 {
			ex = ex.Append(goqu.I("organizations.id").In(input.Filter.OrganizationIDs))
		}

		if input.Filter.UserMemberID != nil {
			ex = ex.Append(organizationMembershipExpressionBuilder{
				userID: input.Filter.UserMemberID,
			}.build())
		}

		if input.Filter.ServiceAccountMemberID != nil {
			ex = ex.Append(organizationMembershipExpressionBuilder{
				serviceAccountID: input.Filter.ServiceAccountMemberID,
			}.build())
		}

		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("organizations.name").ILike("%" + *input.Filter.Search + "%"))
		}
	}

	query := dialect.From(goqu.T("organizations")).Select(o.getSelectFields()...).Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "organizations", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, o.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Organization{}
	for rows.Next() {
		item, err := scanOrganization(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &OrganizationsResult{
		PageInfo:      rows.GetPageInfo(),
		Organizations: results,
	}

	return result, nil
}

func (o *organizations) CreateOrganization(ctx context.Context, org *models.Organization) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "db.CreateOrganization")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Insert("organizations").
		Prepared(true).
		Rows(goqu.Record{
			"id":          newResourceID(),
			"version":     initialResourceVersion,
			"created_at":  timestamp,
			"updated_at":  timestamp,
			"name":        org.Name,
			"description": org.Description,
			"created_by":  org.CreatedBy,
		}).
		Returning(organizationFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdOrg, err := scanOrganization(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("organization with name %s already exists", org.Name, errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdOrg, nil
}

func (o *organizations) UpdateOrganization(ctx context.Context, org *models.Organization) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateOrganization")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Update("organizations").
		Prepared(true).
		Set(
			goqu.Record{
				"version":     goqu.L("? + ?", goqu.C("version"), 1),
				"updated_at":  timestamp,
				"description": org.Description,
			},
		).Where(goqu.Ex{"id": org.Metadata.ID, "version": org.Metadata.Version}).Returning(organizationFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedOrg, err := scanOrganization(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedOrg, nil
}

func (o *organizations) DeleteOrganization(ctx context.Context, org *models.Organization) error {
	ctx, span := tracer.Start(ctx, "db.DeleteOrganization")
	defer span.End()

	sql, args, err := dialect.Delete("organizations").
		Prepared(true).
		Where(
			goqu.Ex{
				"id":      org.Metadata.ID,
				"version": org.Metadata.Version,
			},
		).Returning(organizationFieldList...).ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanOrganization(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (o *organizations) getOrganization(ctx context.Context, exp exp.Expression) (*models.Organization, error) {
	query := dialect.From(goqu.T("organizations")).
		Prepared(true).
		Select(o.getSelectFields()...).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	organization, err := scanOrganization(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return organization, nil
}

func (*organizations) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range organizationFieldList {
		selectFields = append(selectFields, fmt.Sprintf("organizations.%s", field))
	}

	return selectFields
}

func scanOrganization(row scanner) (*models.Organization, error) {
	var description sql.NullString

	organization := &models.Organization{}

	fields := []interface{}{
		&organization.Metadata.ID,
		&organization.Metadata.CreationTimestamp,
		&organization.Metadata.LastUpdatedTimestamp,
		&organization.Metadata.Version,
		&organization.Name,
		&description,
		&organization.CreatedBy,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	if description.Valid {
		organization.Description = description.String
	}

	organization.Metadata.PRN = models.OrganizationResource.BuildPRN(organization.Name)

	return organization, nil
}

type organizationMembershipExpressionBuilder struct {
	userID           *string
	serviceAccountID *string
}

func (m organizationMembershipExpressionBuilder) build() exp.Expression {
	var whereEx exp.Expression

	if m.userID != nil {
		// If dealing with a user ID, must also check team member relationships.
		whereEx = goqu.Or().
			Append(goqu.I("memberships.user_id").Eq(*m.userID)).
			Append(
				goqu.I("memberships.team_id").In(
					dialect.From("team_members").
						Select("team_id").
						Where(goqu.I("team_members.user_id").Eq(*m.userID))))
	} else {
		whereEx = goqu.I("memberships.service_account_id").Eq(*m.serviceAccountID)
	}

	// Use an OR expression to check for both organization and global memberships.
	return goqu.Or(
		goqu.I("organizations.id").In(
			dialect.From("memberships").
				Select(goqu.L("organization_id")).
				Where(whereEx),
		),
		goqu.V(1).Eq(
			dialect.From("memberships").
				Select(goqu.COUNT("*")).
				Where(goqu.And(whereEx, goqu.I("memberships.scope").Eq(models.GlobalScope))),
		),
	)
}
