package db

//go:generate go tool mockery --name PluginVersions --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// PluginVersions encapsulates the data for a plugin version.
type PluginVersions interface {
	GetPluginVersionByID(ctx context.Context, id string) (*models.PluginVersion, error)
	GetPluginVersionByPRN(ctx context.Context, prn string) (*models.PluginVersion, error)
	GetPluginVersions(ctx context.Context, input *GetPluginVersionsInput) (*PluginVersionsResult, error)
	CreatePluginVersion(ctx context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error)
	UpdatePluginVersion(ctx context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error)
	DeletePluginVersion(ctx context.Context, pluginVersion *models.PluginVersion) error
}

// PluginVersionSortableField is a field on the PluginVersion object that can be sorted.
type PluginVersionSortableField string

// PluginVersionSortableFields constants.
const (
	PluginVersionSortableFieldUpdatedAtAsc  PluginVersionSortableField = "UPDATED_AT_ASC"
	PluginVersionSortableFieldUpdatedAtDesc PluginVersionSortableField = "UPDATED_AT_DESC"
	PluginVersionSortableFieldCreatedAtAsc  PluginVersionSortableField = "CREATED_AT_ASC"
	PluginVersionSortableFieldCreatedAtDesc PluginVersionSortableField = "CREATED_AT_DESC"
)

func (sf PluginVersionSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case PluginVersionSortableFieldUpdatedAtAsc, PluginVersionSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "plugin_versions", Col: "updated_at"}
	case PluginVersionSortableFieldCreatedAtAsc, PluginVersionSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "plugin_versions", Col: "created_at"}
	default:
		return nil
	}
}

func (sf PluginVersionSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// PluginVersionFilter is a filter on the PluginVersion object.
type PluginVersionFilter struct {
	PluginID         *string
	SemanticVersion  *string
	SHASumsUploaded  *bool
	Latest           *bool
	PluginVersionIDs []string
}

// GetPluginVersionsInput is the input for the GetPluginVersions method.
type GetPluginVersionsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *PluginVersionSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *PluginVersionFilter
}

// PluginVersionsResult is the result of the GetPluginVersions method.
type PluginVersionsResult struct {
	PageInfo       *pagination.PageInfo
	PluginVersions []models.PluginVersion
}

type pluginVersions struct {
	dbClient *Client
}

var pluginVersionFieldList = append(metadataFieldList,
	"created_by",
	"plugin_id",
	"plugin_semantic_version",
	"protocols",
	"sha_sums_uploaded",
	"readme_uploaded",
	"schema_uploaded",
	"latest",
	"docs",
)

// NewPluginVersions returns a new PluginVersions instance.
func NewPluginVersions(dbClient *Client) PluginVersions {
	return &pluginVersions{dbClient: dbClient}
}

func (p *pluginVersions) GetPluginVersionByID(ctx context.Context, id string) (*models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginByID")
	defer span.End()

	return p.getPluginVersion(ctx, goqu.Ex{"plugin_versions.id": id})
}

func (p *pluginVersions) GetPluginVersionByPRN(ctx context.Context, prn string) (*models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginVersionByPRN")
	defer span.End()

	path, err := models.PluginVersionResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, err
	}

	parts := strings.Split(path, "/")

	if len(parts) != 3 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getPluginVersion(ctx, goqu.Ex{
		"organizations.name":                      parts[0],
		"plugins.name":                            parts[1],
		"plugin_versions.plugin_semantic_version": parts[2],
	})
}

func (p *pluginVersions) GetPluginVersions(ctx context.Context, input *GetPluginVersionsInput) (*PluginVersionsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginVersions")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.PluginID != nil {
			ex = ex.Append(goqu.I("plugin_versions.plugin_id").Eq(*input.Filter.PluginID))
		}

		if input.Filter.SemanticVersion != nil {
			ex = ex.Append(goqu.I("plugin_versions.plugin_semantic_version").Eq(*input.Filter.SemanticVersion))
		}

		if input.Filter.SHASumsUploaded != nil {
			ex = ex.Append(goqu.I("plugin_versions.sha_sums_uploaded").Eq(*input.Filter.SHASumsUploaded))
		}

		if input.Filter.Latest != nil {
			ex = ex.Append(goqu.I("plugin_versions.latest").Eq(*input.Filter.Latest))
		}

		if len(input.Filter.PluginVersionIDs) > 0 {
			ex = ex.Append(goqu.I("plugin_versions.id").In(input.Filter.PluginVersionIDs))
		}
	}

	query := dialect.From(goqu.T("plugin_versions")).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "plugin_versions", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.PluginVersion{}
	for rows.Next() {
		item, err := scanPluginVersion(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &PluginVersionsResult{
		PageInfo:       rows.GetPageInfo(),
		PluginVersions: results,
	}

	return result, nil
}

func (p *pluginVersions) CreatePluginVersion(ctx context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "db.CreatePluginVersion")
	defer span.End()

	protocolsJSON, err := json.Marshal(pluginVersion.Protocols)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal protocols", errors.WithSpan(span))
	}

	docsJSON, err := json.Marshal(pluginVersion.DocFiles)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal plugin version docs to JSON: %w", err)
	}

	timestamp := currentTime()

	sql, args, err := dialect.From("plugin_versions").
		Prepared(true).
		With("plugin_versions",
			dialect.Insert("plugin_versions").Rows(
				goqu.Record{
					"id":                      newResourceID(),
					"version":                 initialResourceVersion,
					"created_at":              timestamp,
					"updated_at":              timestamp,
					"created_by":              pluginVersion.CreatedBy,
					"plugin_id":               pluginVersion.PluginID,
					"plugin_semantic_version": pluginVersion.SemanticVersion,
					"protocols":               protocolsJSON,
					"sha_sums_uploaded":       pluginVersion.SHASumsUploaded,
					"readme_uploaded":         pluginVersion.ReadmeUploaded,
					"schema_uploaded":         pluginVersion.SchemaUploaded,
					"latest":                  pluginVersion.Latest,
					"docs":                    docsJSON,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdVersion, err := scanPluginVersion(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("plugin version %s already exists", pluginVersion.SemanticVersion,
					errors.WithErrorCode(errors.EConflict),
					errors.WithSpan(span),
				)
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdVersion, nil
}

func (p *pluginVersions) UpdatePluginVersion(ctx context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "db.UpdatePluginVersion")
	defer span.End()

	protocolsJSON, err := json.Marshal(pluginVersion.Protocols)
	if err != nil {
		tracing.RecordError(span, err, "failed to marshal provider version protocols")
		return nil, err
	}

	docsJSON, err := json.Marshal(pluginVersion.DocFiles)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal plugin version docs to JSON: %w", err)
	}

	timestamp := currentTime()

	sql, args, err := dialect.From("plugin_versions").
		Prepared(true).
		With("plugin_versions",
			dialect.Update("plugin_versions").
				Set(goqu.Record{
					"version":           goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":        timestamp,
					"sha_sums_uploaded": pluginVersion.SHASumsUploaded,
					"protocols":         protocolsJSON,
					"readme_uploaded":   pluginVersion.ReadmeUploaded,
					"schema_uploaded":   pluginVersion.SchemaUploaded,
					"latest":            pluginVersion.Latest,
					"docs":              docsJSON,
				}).
				Where(goqu.Ex{"id": pluginVersion.Metadata.ID, "version": pluginVersion.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedVersion, err := scanPluginVersion(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedVersion, nil
}

func (p *pluginVersions) DeletePluginVersion(ctx context.Context, pluginVersion *models.PluginVersion) error {
	ctx, span := tracer.Start(ctx, "db.DeletePluginVersion")
	defer span.End()

	sql, args, err := dialect.From("plugin_versions").
		Prepared(true).
		With("plugin_versions",
			dialect.Delete("plugin_versions").
				Where(goqu.Ex{"id": pluginVersion.Metadata.ID, "version": pluginVersion.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanPluginVersion(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *pluginVersions) getPluginVersion(ctx context.Context, exp goqu.Ex) (*models.PluginVersion, error) {
	query := dialect.From(goqu.T("plugin_versions")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	pluginVersion, err := scanPluginVersion(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return pluginVersion, nil
}

func (p *pluginVersions) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range pluginVersionFieldList {
		selectFields = append(selectFields, fmt.Sprintf("plugin_versions.%s", field))
	}

	selectFields = append(selectFields, "organizations.name")
	selectFields = append(selectFields, "plugins.name")

	return selectFields
}

func scanPluginVersion(row scanner) (*models.PluginVersion, error) {
	var organizationName, pluginName string
	pluginVersion := &models.PluginVersion{}

	fields := []interface{}{
		&pluginVersion.Metadata.ID,
		&pluginVersion.Metadata.CreationTimestamp,
		&pluginVersion.Metadata.LastUpdatedTimestamp,
		&pluginVersion.Metadata.Version,
		&pluginVersion.CreatedBy,
		&pluginVersion.PluginID,
		&pluginVersion.SemanticVersion,
		&pluginVersion.Protocols,
		&pluginVersion.SHASumsUploaded,
		&pluginVersion.ReadmeUploaded,
		&pluginVersion.SchemaUploaded,
		&pluginVersion.Latest,
		&pluginVersion.DocFiles,
		&organizationName,
		&pluginName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	pluginVersion.Metadata.PRN = models.PluginVersionResource.BuildPRN(
		organizationName,
		pluginName,
		pluginVersion.SemanticVersion,
	)

	return pluginVersion, nil
}
