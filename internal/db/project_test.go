//go:build integration

package db

import (
	"context"
	"fmt"
	"sort"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// projectInfo aids convenience in accessing the information TestGetProjects needs about the warmup projects.
type projectInfo struct {
	name      string
	projectID string
}

// projectInfoSlice makes a slice of projectInfo sortable
type projectInfoSlice []projectInfo

func TestGetProjectByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForProjects)
	require.Nil(t, err)

	createdWarmupProjs, _, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjects)
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		searchID        string
		expectProject   bool
	}

	positiveProj := createdWarmupProjs[0]

	/*
		test case template

		{
			name            string
			searchID        string
			expectErrorCode errors.CodeType
			expectProject   bool
		}
	*/

	testCases := []testCase{
		{
			name:          "positive-" + positiveProj.Name,
			searchID:      positiveProj.Metadata.ID,
			expectProject: true,
		},
		{
			name:     "negative, non-existent ID",
			searchID: nonExistentID,
			// expect proj and error to be nil
		},
		{
			name:            "defective-id",
			searchID:        invalidID,
			expectProject:   false,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			project, err := testClient.client.Projects.GetProjectByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProject {
				require.NotNil(t, project)
				assert.Equal(t, test.searchID, project.Metadata.ID)
			} else {
				assert.Nil(t, project)
			}
		})
	}
}

func TestGetProjectByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForProjects)
	require.Nil(t, err)

	createdWarmupProjs, _, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjects)
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		searchPRN       string
		expectProject   bool
	}

	positiveProj := createdWarmupProjs[0]

	/*
		test case template

		{
			name            string
			searchPRN       string
			expectErrorCode errors.CodeType
			expectProject   bool
		}
	*/

	testCases := []testCase{
		{
			name:          "positive-" + positiveProj.Name,
			searchPRN:     positiveProj.Metadata.PRN,
			expectProject: true,
		},
		{
			name:      "negative, non-existent PRN",
			searchPRN: models.ProjectResource.BuildPRN("non-existent-org", "non-existent-project"),
			// expect proj and error to be nil
		},
		{
			name:            "defective-prn",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			project, err := testClient.client.Projects.GetProjectByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectProject {
				require.NotNil(t, project)
				assert.Equal(t, test.searchPRN, project.Metadata.PRN)
			} else {
				assert.Nil(t, project)
			}
		})
	}
}

func TestGetProjects(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOrganizations, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForProjects)
	require.Nil(t, err)

	createdWarmupProjects, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjects)
	require.Nil(t, err)

	// Create a role, two users, and membership for one user.
	_, roleName2ID, err := createInitialRoles(ctx, testClient, standardWarmupRolesForProjects)
	require.Nil(t, err)

	createdWarmupUsers, username2ID, err := createInitialUsers(ctx, testClient, standardWarmupUsersForProjects)
	require.Nil(t, err)

	createdWarmupSAs, saName2ID, err := createInitialServiceAccounts(ctx, testClient, orgMap, standardWarmupServiceAccountsForProjects)
	require.Nil(t, err)

	_, teamName2ID, err := createInitialTeams(ctx, testClient, standardWarmupTeamsForProjects)
	require.Nil(t, err)

	_, err = createInitialTeamMembers(ctx, testClient, teamName2ID, username2ID, standardWarmupTeamMembersForProjects)
	require.Nil(t, err)

	_, err = createInitialMemberships(ctx, testClient,
		username2ID, teamName2ID, saName2ID, orgMap, projMap, roleName2ID, standardWarmupMembershipsForProjects)
	require.Nil(t, err)

	allProjectInfos := projectInfoFromProjects(ctx, testClient.client.getConnection(ctx), createdWarmupProjects)

	allNames := namesFromProjectInfo(allProjectInfos)
	reverseNames := reverseStringSlice(allNames)
	allProjectIDs := projectIDsFromProjectInfos(allProjectInfos)

	dummyCursorFunc := func(cp pagination.CursorPaginatable) (*string, error) { return ptr.String("dummy-cursor-value"), nil }

	type testCase struct {
		expectPageInfo              pagination.PageInfo
		expectStartCursorError      error
		expectEndCursorError        error
		expectErrorCode             errors.CodeType
		input                       *GetProjectsInput
		name                        string
		expectProjectNames          []string
		getAfterCursorFromPrevious  bool
		expectHasStartCursor        bool
		getBeforeCursorFromPrevious bool
		expectHasEndCursor          bool
	}

	/*
		test case template

		{
		name                        string
		input                       *GetProjectsInput
		getBeforeCursorFromPrevious bool
		getAfterCursorFromPrevious  bool
		expectErrorCode             errors.CodeType
		expectProjectNames          []string
		expectPageInfo              pagination.PageInfo
		expectStartCursorError      error
		expectHasStartCursor        bool
		expectEndCursorError        error
		expectHasEndCursor          bool
		}
	*/

	testCases := []testCase{
		{
			name: "non-nil but mostly empty input",
			input: &GetProjectsInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "populated sort and pagination, nil filter",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
				Filter: nil,
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "sort in ascending order",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "sort in descending order",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtDesc),
			},
			expectProjectNames:   reverseNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(reverseNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: everything at once",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: first two",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(2),
				},
			},
			expectProjectNames: allNames[:2],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: middle two",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(2),
				},
			},
			getAfterCursorFromPrevious: true,
			expectProjectNames:         allNames[2:4],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: final two",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			getAfterCursorFromPrevious: true,
			expectProjectNames:         allNames[4:],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		// When Last is supplied, the sort order is intended to be reversed.
		{
			name: "pagination: last three",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					Last: ptr.Int32(3),
				},
			},
			expectProjectNames: reverseNames[:3],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(reverseNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination, before and after, expect error",
			input: &GetProjectsInput{
				Sort:              ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{},
			},
			getAfterCursorFromPrevious:  true,
			getBeforeCursorFromPrevious: true,
			expectErrorCode:             errors.EInternal,
			expectProjectNames:          []string{},
			expectPageInfo:              pagination.PageInfo{},
		},

		{
			name: "pagination, first one and last two, expect error",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
					Last:  ptr.Int32(2),
				},
			},
			expectErrorCode:    errors.EInternal,
			expectProjectNames: allNames[5:],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, empty slice of project IDs",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					ProjectIDs: []string{},
				},
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, project IDs",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					ProjectIDs: []string{allProjectIDs[0], allProjectIDs[2], allProjectIDs[4]},
				},
			},
			expectProjectNames:   []string{allNames[0], allNames[2], allNames[4]},
			expectPageInfo:       pagination.PageInfo{TotalCount: 3, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, project IDs, non-existent",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					ProjectIDs: []string{nonExistentID},
				},
			},
			expectProjectNames:   []string{},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, project IDs, invalid",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					ProjectIDs: []string{invalidID},
				},
			},
			expectErrorCode:      errors.EInternal,
			expectProjectNames:   []string{allNames[0], allNames[2], allNames[4]},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, empty org ID",
			input: &GetProjectsInput{
				Sort:   ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{},
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, valid org ID",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					OrgID: &createdWarmupOrganizations[0].Metadata.ID,
				},
			},
			expectProjectNames:   allNames[0:4],
			expectPageInfo:       pagination.PageInfo{TotalCount: 4, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, non-existent org ID",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					OrgID: ptr.String(nonExistentID), // nonExistentID is an untyped string, so cannot use ampersand operator
				},
			},
			expectProjectNames:   []string{},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, invalid org ID",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					OrgID: ptr.String(invalidID), // invalidID is an untyped string, so cannot use ampersand operator
				},
			},
			expectErrorCode:      errors.EInternal,
			expectProjectNames:   []string{allNames[0], allNames[2], allNames[4]},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, user member ID, matches",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					// User is a member of an organization which has 4 projects.
					UserMemberID: &createdWarmupUsers[0].Metadata.ID,
				},
			},
			expectProjectNames:   allNames[0:4],
			expectPageInfo:       pagination.PageInfo{TotalCount: 4, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		// A global membership should return all projects.
		{
			name: "filter, user member ID, user has a global membership",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					UserMemberID: &createdWarmupUsers[2].Metadata.ID,
				},
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, user member ID, not a match",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					UserMemberID: ptr.String(nonExistentID),
				},
			},
			expectProjectNames:   []string{},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, user member id with team membership, matches",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					// User's team is a member of only one project.
					UserMemberID: &createdWarmupUsers[1].Metadata.ID,
				},
			},
			expectProjectNames:   []string{allNames[5]},
			expectPageInfo:       pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, service account member id, matches",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					// Service account is a member of only one project.
					ServiceAccountMemberID: &createdWarmupSAs[0].Metadata.ID,
				},
			},
			expectProjectNames:   []string{allNames[0]},
			expectPageInfo:       pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		// A global membership should return all projects.
		{
			name: "filter, service account member id, service account has a global membership",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					ServiceAccountMemberID: &createdWarmupSAs[1].Metadata.ID,
				},
			},
			expectProjectNames:   allNames,
			expectPageInfo:       pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, service account member id, not a match",
			input: &GetProjectsInput{
				Sort: ptrProjectSortableField(ProjectSortableFieldUpdatedAtAsc),
				Filter: &ProjectFilter{
					ServiceAccountMemberID: ptr.String(nonExistentID),
				},
			},
			expectProjectNames:   []string{},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "search, plain search, empty string, no other restrictions",
			input: &GetProjectsInput{
				Filter: &ProjectFilter{
					Search: ptr.String(""),
				},
			},
			expectProjectNames:   allNames, // should find all 6 of them
			expectPageInfo:       pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "search, plain search, project, no other restrictions",
			input: &GetProjectsInput{
				Filter: &ProjectFilter{
					Search: ptr.String("project"),
				},
			},
			expectProjectNames:   allNames, // should find all 6 of them
			expectPageInfo:       pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "search, plain search, project-1, no other restrictions",
			input: &GetProjectsInput{
				Filter: &ProjectFilter{
					Search: ptr.String("project-1"),
				},
			},
			expectProjectNames:   allNames[0:1],
			expectPageInfo:       pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "search, plain search, project, ProjectIDs",
			input: &GetProjectsInput{
				Filter: &ProjectFilter{
					Search: ptr.String("project"),
					OrgID:  &createdWarmupOrganizations[0].Metadata.ID,
				},
			},
			expectProjectNames:   allNames[0:4],
			expectPageInfo:       pagination.PageInfo{TotalCount: 4, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "search, plain search, project, UserMemberID",
			input: &GetProjectsInput{
				Filter: &ProjectFilter{
					// Should only return 4 projects since the organization which user is a
					// member of only has 4 projects.
					Search:       ptr.String("project"),
					UserMemberID: &createdWarmupUsers[0].Metadata.ID,
				},
			},
			expectProjectNames:   allNames[0:4],
			expectPageInfo:       pagination.PageInfo{TotalCount: 4, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "search, plain search, project, ServiceAccountMemberID",
			input: &GetProjectsInput{
				Filter: &ProjectFilter{
					// Should only return 1 project since the service account is only a
					// member of one project.
					Search:                 ptr.String("project"),
					ServiceAccountMemberID: &createdWarmupSAs[0].Metadata.ID,
				},
			},
			expectProjectNames:   allNames[0:1],
			expectPageInfo:       pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
	}

	var (
		previousEndCursorValue   *string
		previousStartCursorValue *string
	)
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// For some pagination tests, a previous case's cursor value gets piped into the next case.
			if test.getAfterCursorFromPrevious || test.getBeforeCursorFromPrevious {

				// Make sure there's a place to put it.
				require.NotNil(t, test.input.PaginationOptions)

				if test.getAfterCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousEndCursorValue)
					test.input.PaginationOptions.After = previousEndCursorValue
				}

				if test.getBeforeCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousStartCursorValue)
					test.input.PaginationOptions.Before = previousStartCursorValue
				}

				// Clear the values so they won't be used twice.
				previousEndCursorValue = nil
				previousStartCursorValue = nil
			}

			projectsResult, err := testClient.client.Projects.GetProjects(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			// If there was no error, check the results.
			if err == nil {

				// Never returns nil if error is nil.
				require.NotNil(t, projectsResult.PageInfo)
				assert.NotNil(t, projectsResult.Projects)
				pageInfo := projectsResult.PageInfo
				projects := projectsResult.Projects

				// Check the projects result by comparing a list of the names.
				resultNames := []string{}
				for _, proj := range projects {
					resultNames = append(resultNames, proj.Name)
				}

				// If no sort direction was specified, sort the results here for repeatability.
				if test.input.Sort == nil {
					sort.Strings(resultNames)
				}

				assert.Equal(t, len(test.expectProjectNames), len(resultNames))
				assert.Equal(t, test.expectProjectNames, resultNames)

				assert.Equal(t, test.expectPageInfo.HasNextPage, pageInfo.HasNextPage)
				assert.Equal(t, test.expectPageInfo.HasPreviousPage, pageInfo.HasPreviousPage)
				assert.Equal(t, test.expectPageInfo.TotalCount, pageInfo.TotalCount)
				assert.Equal(t, test.expectPageInfo.Cursor != nil, pageInfo.Cursor != nil)

				// Compare the cursor function results only if there is at least one project returned.
				// If there are no projects returned, there is no argument to pass to the cursor function.
				// Also, don't try to reverse engineer to compare the cursor string values.
				if len(projects) > 0 {
					resultStartCursor, resultStartCursorError := pageInfo.Cursor(&projects[0])
					resultEndCursor, resultEndCursorError := pageInfo.Cursor(&projects[len(projects)-1])
					assert.Equal(t, test.expectStartCursorError, resultStartCursorError)
					assert.Equal(t, test.expectHasStartCursor, resultStartCursor != nil)
					assert.Equal(t, test.expectEndCursorError, resultEndCursorError)
					assert.Equal(t, test.expectHasEndCursor, resultEndCursor != nil)

					// Capture the ending cursor values for the next case.
					previousEndCursorValue = resultEndCursor
					previousStartCursorValue = resultStartCursor
				}
			}
		})
	}
}

func TestCreateProject(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForProjects)
	require.Nil(t, err)

	type testCase struct {
		expectMsg      *string
		name           string
		toCreate       []models.Project
		isPositiveCase bool
	}

	testCases := []testCase{
		{
			name:           "positive, standard warmup projects",
			isPositiveCase: true,
			toCreate:       standardWarmupProjects,
			// expect message to be nil
		},
		{
			name:      "negative, duplicate project",
			toCreate:  []models.Project{standardWarmupProjects[2]},
			expectMsg: ptr.String("project with name " + standardWarmupProjects[2].Name + " already exists"),
		},
	}

	cumulativeToCreate := []models.Project{}
	cumulativeClaimedCreated := []models.Project{}
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			if test.isPositiveCase {

				// Capture any that should be created.
				cumulativeToCreate = append(cumulativeToCreate, test.toCreate...)

				// For a positive case, just use the utility function.
				claimedCreated, _, err := createInitialProjects(ctx, testClient, orgMap, test.toCreate)
				require.Nil(t, err)

				// Capture any new claimed to be created.
				cumulativeClaimedCreated = append(cumulativeClaimedCreated, claimedCreated...)
			} else {
				// For a negative case, do each project one at a time.
				for _, input := range test.toCreate {

					// Must translate the org name to org ID.
					newID, ok := orgMap[input.OrgID]
					require.True(t, ok)
					input.OrgID = newID

					_, err := testClient.client.Projects.CreateProject(ctx, &input)
					require.NotNil(t, err)
					assert.Equal(t, *test.expectMsg, err.Error())
				}
			}

			// Get the projects for comparison.
			gotResult, err := testClient.client.Projects.GetProjects(ctx, &GetProjectsInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			})
			require.Nil(t, err)
			retrievedProjects := gotResult.Projects

			// Compare lengths.
			assert.Equal(t, len(cumulativeToCreate), len(cumulativeClaimedCreated))
			assert.Equal(t, len(cumulativeToCreate), len(retrievedProjects))

			// Compare the contents of the cumulative projects to create, the projects reportedly created,
			// and the projects retrieved.  The three slices aren't guaranteed to be in the same order,
			// so it is necessary to look them up. Name is used for that purpose.
			copyCumulativeClaimedCreated := cumulativeClaimedCreated
			for _, toldToCreate := range cumulativeToCreate {
				toldToCreate.Metadata.PRN = models.ProjectResource.BuildPRN(toldToCreate.OrgID, toldToCreate.Name)

				// Must translate the org name to org ID in the test cases.
				newID, ok := orgMap[toldToCreate.OrgID]
				require.True(t, ok)
				toldToCreate.OrgID = newID

				var claimedCreated, retrieved models.Project
				claimedCreated, copyCumulativeClaimedCreated, err = removeMatchingProject(toldToCreate.Name, copyCumulativeClaimedCreated)
				assert.Nil(t, err)
				if err != nil {
					break
				}

				retrieved, retrievedProjects, err = removeMatchingProject(toldToCreate.Name, retrievedProjects)
				assert.Nil(t, err)
				if err != nil {
					break
				}

				compareProjectsCreate(t, toldToCreate, claimedCreated)
				compareProjectsCreate(t, toldToCreate, retrieved)
			}

			// Must not have any leftovers.
			assert.Equal(t, 0, len(copyCumulativeClaimedCreated))
			assert.Equal(t, 0, len(retrievedProjects))
		})
	}
}

func TestUpdateProject(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForProjects)
	require.Nil(t, err)

	createdWarmupProjects, _, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjects)
	require.Nil(t, err)

	type testCase struct {
		findName           *string
		findOrganizationID string
		findProject        *models.Project
		newDescription     *string
		expectErrorCode    errors.CodeType
		name               string
		isPositive         bool
	}

	/*
		test case template

		{
			name            string
			isPositive      bool
			findName        *string
			findProject     *models.Project
			newDescription  *string
			expectErrorCode errors.CodeType
		}
	*/

	testCases := []testCase{}
	for ix := range createdWarmupProjects {
		testCases = append(testCases, testCase{
			name:               "positive-" + createdWarmupProjects[ix].Name,
			findOrganizationID: createdWarmupProjects[ix].OrgID,
			findName:           &createdWarmupProjects[ix].Name,
			newDescription:     updateProjectDescription(createdWarmupProjects[ix].Description),
			isPositive:         true,
		})
	}

	testCases = append(testCases,
		testCase{
			name: "negative, not exist",
			findProject: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: 1,
				},
				Name:        "project-that-does-not-exist",
				Description: "This is the project that does not exist.",
				CreatedBy:   "someone",
			},
			newDescription:  ptr.String("Update description for a project that does not exist."),
			expectErrorCode: errors.EOptimisticLock,
		},
		testCase{
			name: "negative, invalid uuid",
			findProject: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				Name:        "project-that-does-not-exist",
				Description: "This is the project that does not exist.",
				CreatedBy:   "someone",
			},
			newDescription:  ptr.String("Update description for a project that does not exist."),
			expectErrorCode: errors.EInternal,
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			var originalProject *models.Project

			// Must find organizationName since there are multiple.
			organizationName := ""
			for name, id := range orgMap {
				if id == test.findOrganizationID {
					organizationName = name
					break
				}
			}

			switch {
			case (test.findName != nil) && (test.findProject == nil):

				originalProject, err = testClient.client.Projects.GetProjectByPRN(ctx,
					models.ProjectResource.BuildPRN(organizationName, *test.findName))
				require.Nil(t, err)
				require.NotNil(t, originalProject)

			case (test.findName == nil) && (test.findProject != nil):
				originalProject = test.findProject

			default:
				assert.Equal(t, "test case should use exactly one of findName, findProject",
					"test case violated that rule")
				// No point in going forward with this test case.
				return
			}

			expectUpdatedDescription := updateProjectDescription(originalProject.Description)
			copyOriginalProject := originalProject
			copyOriginalProject.Description = *expectUpdatedDescription

			claimedUpdatedProject, err := testClient.client.Projects.UpdateProject(ctx, copyOriginalProject)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.isPositive {

				require.NotNil(t, claimedUpdatedProject)

				compareProjectsUpdate(t, *expectUpdatedDescription, *originalProject, *claimedUpdatedProject)

				retrieved, err := testClient.client.Projects.GetProjectByPRN(ctx, models.ProjectResource.BuildPRN(
					organizationName, originalProject.Name))
				require.Nil(t, err)

				require.NotNil(t, retrieved)

				compareProjectsUpdate(t, *expectUpdatedDescription, *originalProject, *retrieved)
			}
		})
	}
}

func TestDeleteProject(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForProjects)
	require.Nil(t, err)

	initialWarmupProjects, _, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjects)
	require.Nil(t, err)

	allNames := []string{}
	for _, proj := range initialWarmupProjects {
		allNames = append(allNames, proj.Name)
	}

	// Must close manually, because more test clients will be created inside the loop.
	testClient.close(ctx)

	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		projNames        []string
		ids              []string
		expectFinalCount int
	}

	/*
		test case template

		{
		name             string
		projNames        []string
		ids              []string
		expectErrorCode  errors.CodeType
		expectFinalCount int
		}
	*/

	testCases := []testCase{
		{
			name:             "positive",
			projNames:        allNames,
			expectFinalCount: 0,
		},
		{
			name:             "negative, non-existent ID",
			ids:              []string{nonExistentID},
			expectErrorCode:  errors.EOptimisticLock,
			expectFinalCount: 6,
		},
		{
			name:             "negative, invalid ID",
			ids:              []string{invalidID},
			expectErrorCode:  errors.EInternal,
			expectFinalCount: 6,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// Because deleting a project is destructive, this test must create/delete
			// the warmup projects inside the loop.
			// Creating the new client deletes the projects from the previous run.
			testClient := newTestClient(ctx, t)
			// Must close manually, because later test cases will create their own test clients.

			_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForProjects)
			require.Nil(t, err)

			createdWarmupProjects, _, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjects)
			require.Nil(t, err)

			projectsToDelete := []*models.Project{}
			switch {
			case test.projNames != nil:
				for _, name := range test.projNames {
					// Must find the project object in the current generation of warmup projects.
					// Using the initial warmup projects would cause metadata version mismatches.
					var foundProject *models.Project
					for _, candidate := range createdWarmupProjects {
						if candidate.Name == name {
							foundProject = &candidate
							break
						}
					}
					require.NotNil(t, foundProject)
					projectsToDelete = append(projectsToDelete, foundProject)
				}
			case test.ids != nil:
				for _, id := range test.ids {
					projectToDelete := models.Project{
						Metadata: models.ResourceMetadata{
							ID:      id,
							Version: 1,
						},
					}
					projectsToDelete = append(projectsToDelete, &projectToDelete)
				}
			default:
				assert.Nil(t, "each test case must have either projNames or ids but not both")
				return
			}

			// Now, try to delete the projects in sequence.
			for _, projectToDelete := range projectsToDelete {
				err = testClient.client.Projects.DeleteProject(ctx, projectToDelete)
				if test.expectErrorCode != "" {
					assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
					return
				}
			}

			// Check that the final count is right.
			stillAround, err := testClient.client.Projects.GetProjects(ctx, &GetProjectsInput{})
			assert.Nil(t, err)
			assert.Equal(t, test.expectFinalCount, len(stillAround.Projects))

			// Must close manually, because later test cases will create their own test clients.
			testClient.close(ctx)
		})
	}
}

// standardWarmupOrganizations for tests in this module.
// One should suffice for this module.
var standardWarmupOrganizationsForProjects = []models.Organization{
	{
		Description: "organization-1 for testing organization functions",
		Name:        "organization-1",
		CreatedBy:   "someone",
	},
	{
		Description: "organization-2 for testing organization functions",
		Name:        "organization-2",
		CreatedBy:   "someone",
	},
}

// standardWarmupUsers for tests in this module: 2 users
var standardWarmupUsersForProjects = []models.User{
	{
		Username: "user-0",
		Email:    "user-0@example.com",
	},
	{
		Username: "user-1",
		Email:    "user-1@example.com",
	},
	{
		Username: "global-user",
		Email:    "global-user@example.com",
	},
}

// standardWarmupTeams for tests in this module: 2 teams
var standardWarmupTeamsForProjects = []models.Team{
	{
		Name:        "team-0",
		Description: "team 0 for testing team functions",
	},
}

// standardWarmupTeamMembers for tests in this module: 1 team member
var standardWarmupTeamMembersForProjects = []models.TeamMember{
	{
		TeamID: "team-0",
		UserID: "user-1",
	},
}

// standardWarmupServiceAccounts for tests in this module: 1 service account
var standardWarmupServiceAccountsForProjects = []models.ServiceAccount{
	{
		Name:           "service-account-0",
		Description:    "service account 0 for testing service account functions",
		OrganizationID: ptr.String("organization-1"),
		Scope:          models.OrganizationScope,
		CreatedBy:      "someone",
	},
	{
		Name:        "global-service-account-0",
		Description: "global service account 0 for testing service account functions",
		Scope:       models.GlobalScope,
		CreatedBy:   "someone",
	},
}

// Standard warmup roles for tests in this module:
var standardWarmupRolesForProjects = []models.Role{
	{
		Name:        "role-a",
		Description: "role a for organization membership tests",
		CreatedBy:   "someone-a",
	},
}

// standardWarmupMembershipsForProjects for tests in this module:
// Contains memberships for a user in an org, a service account and team in a project.
var standardWarmupMembershipsForProjects = []models.Membership{
	{
		OrganizationID: ptr.String("organization-1"),
		UserID:         ptr.String("user-0"),
		RoleID:         "role-a",
		Scope:          models.OrganizationScope,
	},
	{
		ProjectID: ptr.String("project-6"),
		TeamID:    ptr.String("team-0"),
		RoleID:    "role-a",
		Scope:     models.ProjectScope,
	},
	{
		ProjectID:        ptr.String("project-1"),
		ServiceAccountID: ptr.String("service-account-0"),
		RoleID:           "role-a",
		Scope:            models.ProjectScope,
	},
	{
		UserID: ptr.String("global-user"),
		RoleID: "role-a",
		Scope:  models.GlobalScope,
	},
	{
		ServiceAccountID: ptr.String("global-service-account-0"),
		RoleID:           "role-a",
		Scope:            models.GlobalScope,
	},
}

// standardWarmupProjects for tests in this module.
var standardWarmupProjects = []models.Project{
	{
		Description: "project-1 for testing project functions",
		Name:        "project-1",
		CreatedBy:   "someone",
		OrgID:       "organization-1",
	},
	{
		Description: "project-2 for testing project functions",
		Name:        "project-2",
		CreatedBy:   "someone",
		OrgID:       "organization-1",
	},
	{
		Description: "project-3 for testing project functions",
		Name:        "project-3",
		CreatedBy:   "someone",
		OrgID:       "organization-1",
	},
	{
		Description: "project-4 for testing project functions",
		Name:        "project-4",
		CreatedBy:   "someone",
		OrgID:       "organization-1",
	},
	{
		Description: "project-5 for testing project functions",
		Name:        "project-5",
		CreatedBy:   "someone",
		OrgID:       "organization-2",
	},
	{
		Description: "project-6 for testing project functions",
		Name:        "project-6",
		CreatedBy:   "someone",
		OrgID:       "organization-2",
	},
}

func ptrProjectSortableField(arg ProjectSortableField) *ProjectSortableField {
	return &arg
}

func (ois projectInfoSlice) Len() int {
	return len(ois)
}

func (ois projectInfoSlice) Swap(i, j int) {
	ois[i], ois[j] = ois[j], ois[i]
}

func (ois projectInfoSlice) Less(i, j int) bool {
	return ois[i].name < ois[j].name
}

// projectInfoFromProjects returns a slice of projectInfo, sorted by name.
func projectInfoFromProjects(ctx context.Context, conn connection, projects []models.Project) []projectInfo {
	result := []projectInfo{}
	for _, project := range projects {
		result = append(result, projectInfo{
			name:      project.Name,
			projectID: project.Metadata.ID,
		})
	}

	sort.Sort(projectInfoSlice(result))

	return result
}

// namesFromProjectInfo preserves order.
func namesFromProjectInfo(projectInfos []projectInfo) []string {
	result := []string{}
	for _, projectInfo := range projectInfos {
		result = append(result, projectInfo.name)
	}
	return result
}

// projectIDsFromProjectInfos preserves order
func projectIDsFromProjectInfos(projectInfos []projectInfo) []string {
	result := []string{}
	for _, projectInfo := range projectInfos {
		result = append(result, projectInfo.projectID)
	}
	return result
}

// removeMatchingProject finds a project in a slice with a specified name.
// It returns the found project, a shortened slice, and an error.
func removeMatchingProject(lookFor string, oldSlice []models.Project) (models.Project, []models.Project, error) {
	found := models.Project{}
	foundOne := false
	newSlice := []models.Project{}

	for _, candidate := range oldSlice {
		if candidate.Name == lookFor {
			found = candidate
			foundOne = true
		} else {
			newSlice = append(newSlice, candidate)
		}
	}

	if !foundOne {
		return models.Project{}, nil, fmt.Errorf("Failed to find project with name: %s", lookFor)
	}

	return found, newSlice, nil
}

// compareProjectsCreate compares two projects for TestCreateProject
// There are some fields that cannot be compared, or DeepEqual would work.
func compareProjectsCreate(t *testing.T, expected, actual models.Project) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.Description, actual.Description)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)

	assert.Equal(t, expected.Metadata.PRN, actual.Metadata.PRN)
	assert.Equal(t, expected.OrgID, actual.OrgID)
}

// updateProjectDescription takes an original description and returns a modified version for TestUpdateProject
func updateProjectDescription(input string) *string {
	return ptr.String(fmt.Sprintf("Updated description: %s", input))
}

// compareProjectsUpdate compares two projects for TestUpdateProject
func compareProjectsUpdate(t *testing.T, expectedDescription string, expected, actual models.Project) {
	assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	assert.Equal(t, expected.Metadata.CreationTimestamp, actual.Metadata.CreationTimestamp)
	assert.NotEqual(t, expected.Metadata.Version, actual.Metadata.Version)
	assert.NotEqual(t, expected.Metadata.LastUpdatedTimestamp, actual.Metadata.LastUpdatedTimestamp)
	assert.Equal(t, expected.Metadata.PRN, actual.Metadata.PRN)

	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expectedDescription, actual.Description)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)

	assert.Equal(t, expected.OrgID, actual.OrgID)
}

// createInitialProjects creates some Projects for a test.
func createInitialProjects(ctx context.Context, testClient *testClient, orgMap map[string]string,
	toCreate []models.Project) ([]models.Project, map[string]string, error) {
	result := []models.Project{}
	name2ID := make(map[string]string)

	for _, proj := range toCreate {
		orgID, ok := orgMap[proj.OrgID]
		if !ok {
			return nil, nil, fmt.Errorf("Failed to look up parent org ID in createInitialProjects: %s", proj.OrgID)
		}
		proj.OrgID = orgID

		created, err := testClient.client.Projects.CreateProject(ctx, &proj)
		if err != nil {
			return nil, nil, err
		}

		result = append(result, *created)
		name2ID[created.Name] = created.Metadata.ID
	}

	return result, name2ID, nil
}
