// Package db encapsulates all the logic needed to access
// information from the DB.
package db

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/avast/retry-go"
	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/postgres" // Register Postgres dialect
	"github.com/golang-migrate/migrate/v4"
	"github.com/google/uuid"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	te "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const (
	// maxSearchFilterLength is the maximum length of a search filter to prevent the DB from being overloaded.
	maxSearchFilterLength int = 512
	// initialResourceVersion is the initial version of a resource.
	initialResourceVersion int = 1
)

// Key type is used for attaching state to the context
type key string

func (k key) String() string {
	return fmt.Sprintf("gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db/dbclient %s", string(k))
}

const (
	txKey key = "tx"
)

var (
	// ErrOptimisticLockError is used for optimistic lock exceptions
	ErrOptimisticLockError = te.New(
		"resource version does not match specified version",
		te.WithErrorCode(te.EOptimisticLock),
	)
	// ErrInvalidID is used for invalid resource UUIDs
	ErrInvalidID = te.New(
		"invalid id: the id must be a valid uuid",
		te.WithErrorCode(te.EInvalid),
	)
)

var (
	metadataFieldList = []interface{}{"id", "created_at", "updated_at", "version"}
	dialect           = goqu.Dialect("postgres")
	// Assigning time.Now to a variable to allow for mocking in tests
	now = time.Now
)

// connection is used to represent a DB connection
type connection interface {
	Begin(ctx context.Context) (pgx.Tx, error)
	Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)
	Query(ctx context.Context, sql string, optionsAndArgs ...any) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, optionsAndArgs ...any) pgx.Row
}

// Client acts as a facade for the database
type Client struct {
	conn                  *pgxpool.Pool
	logger                logger.Logger
	Events                Events
	Users                 Users
	Roles                 Roles
	Organizations         Organizations
	Memberships           Memberships
	Transactions          Transactions
	ResourceLimits        ResourceLimits
	Projects              Projects
	ProjectVariableSets   ProjectVariableSets
	ProjectVariables      ProjectVariables
	LogStreams            LogStreams
	Teams                 Teams
	TeamMembers           TeamMembers
	SCIMTokens            SCIMTokens
	PipelineTemplates     PipelineTemplates
	Pipelines             Pipelines
	PipelineActionOutputs PipelineActionOutputs
	PipelineApprovals     PipelineApprovals
	Jobs                  Jobs
	Agents                Agents
	AgentSessions         AgentSessions
	Environments          Environments
	ServiceAccounts       ServiceAccounts
	ApprovalRules         ApprovalRules
	LifecycleTemplates    LifecycleTemplates
	ReleaseLifecycles     ReleaseLifecycles
	Releases              Releases
	ActivityEvents        ActivityEvents
	Comments              Comments
	Plugins               Plugins
	PluginVersions        PluginVersions
	PluginPlatforms       PluginPlatforms
	ToDoItems             ToDoItems
	VCSProviders          VCSProviders
	Threads               Threads
	Metrics               Metrics
	EnvironmentRules      EnvironmentRules
	SchemaMigrations      SchemaMigrations
}

// NewClient creates a new Client
func NewClient(
	ctx context.Context,
	dbHost string,
	dbPort int,
	dbName string,
	dbSslMode string,
	dbUsername string,
	dbPassword string,
	dbMaxConnections int,
	dbAutoMigrateEnabled bool,
	logger logger.Logger,
) (*Client, error) {
	dbURI := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=%s", dbUsername, dbPassword, dbHost, dbPort, dbName, dbSslMode)

	ctx, span := tracer.Start(ctx, "db.NewClient")
	defer span.End()

	cfg, err := pgxpool.ParseConfig(dbURI)
	if err != nil {
		return nil, te.Wrap(err, "failed to parse db connection URI: %w", err, te.WithSpan(span))
	}

	if dbMaxConnections != 0 {
		cfg.MaxConns = int32(dbMaxConnections)
	}

	logger.Infof("Connecting to DB (host=%s, maxConnections=%d)", dbHost, cfg.MaxConns)
	span.AddEvent("Connecting to DB",
		trace.WithAttributes(attribute.String("host", dbHost), attribute.Int("maxConnections", int(cfg.MaxConns))))

	pool, err := pgxpool.ConnectConfig(ctx, cfg)
	if err != nil {
		logger.Errorf("Unable to connect to DB: %v", err)
		return nil, te.Wrap(err, "Unable to connect to DB", te.WithSpan(span))
	}

	logger.Infof("Successfully connected to DB %s", dbHost)
	span.AddEvent("Successfully connected to DB", trace.WithAttributes((attribute.String("db-host", dbHost))))

	// Auto migrate-up the DB if enabled.
	if dbAutoMigrateEnabled {
		logger.Info("Starting DB migrate")
		span.AddEvent("Starting DB migrate")

		migrations, err := newMigrations(logger, cfg.ConnString())
		if err != nil {
			return nil, te.Wrap(err, "newMigrations failed", te.WithSpan(span))
		}

		err = migrations.migrateUp(ctx)
		if err == migrate.ErrNoChange {
			logger.Info("No migration necessary since DB is already on latest version")
			span.AddEvent("No migration necessary since DB is already on latest version")
		} else if err != nil {
			logger.Errorf("Unable to migrate DB: %v", err)
			return nil, te.Wrap(err, "Unable to migrate DB", te.WithSpan(span))
		} else {
			logger.Info("Successfully migrated DB to latest version")
			span.AddEvent("Successfully migrated DB to latest version")
		}
	}

	dbClient := &Client{
		conn:   pool,
		logger: logger,
	}

	dbClient.Events = NewEvents(dbClient)
	dbClient.Transactions = NewTransactions(dbClient)
	dbClient.Users = NewUsers(dbClient)
	dbClient.Roles = NewRoles(dbClient)
	dbClient.Organizations = NewOrganizations(dbClient)
	dbClient.Memberships = NewMemberships(dbClient)
	dbClient.ResourceLimits = NewResourceLimits(dbClient)
	dbClient.Projects = NewProjects(dbClient)
	dbClient.ProjectVariableSets = NewProjectVariableSets(dbClient)
	dbClient.ProjectVariables = NewProjectVariables(dbClient)
	dbClient.LogStreams = NewLogStreams(dbClient)
	dbClient.Teams = NewTeams(dbClient)
	dbClient.TeamMembers = NewTeamMembers(dbClient)
	dbClient.SCIMTokens = NewSCIMTokens(dbClient)
	dbClient.PipelineTemplates = NewPipelineTemplates(dbClient)
	dbClient.Pipelines = NewPipelines(dbClient)
	dbClient.PipelineActionOutputs = NewPipelineActionOutputs(dbClient)
	dbClient.PipelineApprovals = NewPipelineApprovals(dbClient)
	dbClient.Jobs = NewJobs(dbClient)
	dbClient.Agents = NewAgents(dbClient)
	dbClient.AgentSessions = NewAgentSessions(dbClient)
	dbClient.Environments = NewEnvironments(dbClient)
	dbClient.ServiceAccounts = NewServiceAccounts(dbClient)
	dbClient.ApprovalRules = NewApprovalRules(dbClient)
	dbClient.LifecycleTemplates = NewLifecycleTemplates(dbClient)
	dbClient.ReleaseLifecycles = NewReleaseLifecycles(dbClient)
	dbClient.Releases = NewReleases(dbClient)
	dbClient.ActivityEvents = NewActivityEvents(dbClient)
	dbClient.Comments = NewComments(dbClient)
	dbClient.Plugins = NewPlugins(dbClient)
	dbClient.PluginVersions = NewPluginVersions(dbClient)
	dbClient.PluginPlatforms = NewPluginPlatforms(dbClient)
	dbClient.ToDoItems = NewToDoItems(dbClient)
	dbClient.VCSProviders = NewVCSProviders(dbClient)
	dbClient.Threads = NewThreads(dbClient)
	dbClient.Metrics = NewMetrics(dbClient)
	dbClient.EnvironmentRules = NewEnvironmentRules(dbClient)
	dbClient.SchemaMigrations = NewSchemaMigrations(dbClient)

	return dbClient, nil
}

// Close will close the database connections
func (db *Client) Close(_ context.Context) {
	db.conn.Close()
}

func (db *Client) getConnection(ctx context.Context) connection {
	trx, ok := ctx.Value(txKey).(pgx.Tx)
	if !ok {
		// Return a normal DB connection if no transaction exists
		return db.conn
	}
	// Return transaction if it exists on the context
	return trx
}

// RetryOnOLE will retry the given function if an optimistic lock error occurs
func (db *Client) RetryOnOLE(ctx context.Context, fn func() error) error {
	return retry.Do(
		func() error {
			return fn()
		},
		retry.Attempts(1000),
		retry.DelayType(retry.FixedDelay),
		retry.Delay(10*time.Millisecond),
		retry.RetryIf(func(err error) bool {
			// Only retry on optimistic lock errors
			return te.ErrorCode(err) == te.EOptimisticLock
		}),
		retry.OnRetry(func(n uint, _ error) {
			// Explicitly do nothing
			if n > 100 {
				db.logger.Errorf("Warning, OLE retry function has reached %d attempts", n)
			}
		}),
		retry.LastErrorOnly(true),
		retry.Context(ctx),
	)
}

type scanner interface {
	Scan(dest ...interface{}) error
}

func isUniqueViolation(pgErr *pgconn.PgError) bool {
	if pgErr == nil {
		return false
	}

	return pgErr.Code == pgerrcode.UniqueViolation
}

func isForeignKeyViolation(pgErr *pgconn.PgError) bool {
	if pgErr == nil {
		return false
	}

	return pgErr.Code == pgerrcode.ForeignKeyViolation
}

func isInvalidIDViolation(pgErr *pgconn.PgError) bool {
	if pgErr == nil {
		return false
	}

	return pgErr.Code == pgerrcode.InvalidTextRepresentation && pgErr.Routine == "string_to_uuid"
}

func asPgError(err error) *pgconn.PgError {
	var pgErr *pgconn.PgError
	ok := errors.As(err, &pgErr)
	if ok {
		return pgErr
	}
	return nil
}

func newResourceID() string {
	return uuid.New().String()
}

// Produce a rounded version of current time suitable for storing in the DB.
// Because time.Now().UTC() returns nanosecond precision but the DB stores only
// microseconds, it is necessary to round the time to the nearest microsecond
// before storing it.
func currentTime() time.Time {
	return now().UTC().Round(time.Microsecond)
}
