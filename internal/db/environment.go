package db

//go:generate go tool mockery --name Environments --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Environments encapsulates the logic to access environments from the database
type Environments interface {
	GetEnvironmentByID(ctx context.Context, id string) (*models.Environment, error)
	GetEnvironmentByPRN(ctx context.Context, prn string) (*models.Environment, error)
	GetEnvironments(ctx context.Context, input *GetEnvironmentsInput) (*EnvironmentsResult, error)
	CreateEnvironment(ctx context.Context, environment *models.Environment) (*models.Environment, error)
	UpdateEnvironment(ctx context.Context, environment *models.Environment) (*models.Environment, error)
	DeleteEnvironment(ctx context.Context, environment *models.Environment) error
}

// EnvironmentSortableField represents the fields that environments can be sorted by
type EnvironmentSortableField string

// EnvironmentSortableField constants
const (
	EnvironmentSortableFieldUpdatedAtAsc  EnvironmentSortableField = "UPDATED_AT_ASC"
	EnvironmentSortableFieldUpdatedAtDesc EnvironmentSortableField = "UPDATED_AT_DESC"
	EnvironmentSortableFieldNameAsc       EnvironmentSortableField = "NAME_ASC"
	EnvironmentSortableFieldNameDesc      EnvironmentSortableField = "NAME_DESC"
)

// getValue implements the sortableField interface for testing
func (sf EnvironmentSortableField) getValue() string {
	return string(sf)
}

func (sf EnvironmentSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case EnvironmentSortableFieldUpdatedAtAsc, EnvironmentSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "environments", Col: "updated_at"}
	case EnvironmentSortableFieldNameAsc, EnvironmentSortableFieldNameDesc:
		return &pagination.FieldDescriptor{Key: "name", Table: "environments", Col: "name"}
	default:
		return nil
	}
}

func (sf EnvironmentSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// EnvironmentFilter contains the supported fields for filtering Environment resources
type EnvironmentFilter struct {
	OrganizationID  *string
	ProjectID       *string
	EnvironmentName *string
	EnvironmentIDs  []string
}

// GetEnvironmentsInput is the input for listing environments
type GetEnvironmentsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *EnvironmentSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *EnvironmentFilter
}

// EnvironmentsResult contains the response data and page information
type EnvironmentsResult struct {
	PageInfo     *pagination.PageInfo
	Environments []models.Environment
}

type environments struct {
	dbClient *Client
}

var environmentFieldList = append(metadataFieldList, "name", "description", "project_id", "created_by")

// NewEnvironments returns an instance of the Environments interface
func NewEnvironments(dbClient *Client) Environments {
	return &environments{dbClient: dbClient}
}

func (a *environments) GetEnvironmentByID(ctx context.Context, id string) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "db.GetEnvironmentByID")
	defer span.End()

	return a.getEnvironment(ctx, goqu.Ex{"environments.id": id})
}

func (a *environments) GetEnvironmentByPRN(ctx context.Context, prn string) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "db.GetEnvironmentByPRN")
	defer span.End()

	path, err := models.EnvironmentResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	if len(parts) != 3 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return a.getEnvironment(ctx, goqu.Ex{
		"organizations.name": parts[0],
		"projects.name":      parts[1],
		"environments.name":  parts[2],
	})
}

func (a *environments) GetEnvironments(ctx context.Context, input *GetEnvironmentsInput) (*EnvironmentsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetEnvironments")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if len(input.Filter.EnvironmentIDs) > 0 {
			ex = ex.Append(goqu.I("environments.id").In(input.Filter.EnvironmentIDs))
		}

		if input.Filter.EnvironmentName != nil {
			ex = ex.Append(goqu.I("environments.name").Eq(*input.Filter.EnvironmentName))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("environments.project_id").Eq(*input.Filter.ProjectID))
		}

		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("projects.org_id").Eq(*input.Filter.OrganizationID))
		}
	}

	query := dialect.From(goqu.T("environments")).
		Select(a.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"environments.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "environments", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)

	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, a.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Environment{}
	for rows.Next() {
		item, err := scanEnvironment(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := EnvironmentsResult{
		PageInfo:     rows.GetPageInfo(),
		Environments: results,
	}

	return &result, nil
}

func (a *environments) CreateEnvironment(ctx context.Context, environment *models.Environment) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "db.CreateEnvironment")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("environments").
		Prepared(true).
		With("environments",
			dialect.Insert("environments").Rows(
				goqu.Record{
					"id":          newResourceID(),
					"version":     initialResourceVersion,
					"created_at":  timestamp,
					"updated_at":  timestamp,
					"project_id":  environment.ProjectID,
					"name":        environment.Name,
					"description": environment.Description,
					"created_by":  environment.CreatedBy,
				}).Returning("*"),
		).Select(a.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"environments.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdEnvironment, err := scanEnvironment(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New(
					"environment with name %s already exists in project", environment.Name,
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EConflict),
				)
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdEnvironment, nil
}

func (a *environments) UpdateEnvironment(ctx context.Context, environment *models.Environment) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateEnvironment")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("environments").
		Prepared(true).
		With("environments",
			dialect.Update("environments").
				Set(goqu.Record{
					"version":     goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":  timestamp,
					"description": environment.Description,
				}).
				Where(goqu.Ex{"id": environment.Metadata.ID, "version": environment.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"environments.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedEnvironment, err := scanEnvironment(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedEnvironment, nil
}

func (a *environments) DeleteEnvironment(ctx context.Context, environment *models.Environment) error {
	ctx, span := tracer.Start(ctx, "db.DeleteEnvironment")
	defer span.End()

	sql, args, err := dialect.From("environments").
		Prepared(true).
		With("environments",
			dialect.Delete("environments").
				Where(goqu.Ex{"id": environment.Metadata.ID, "version": environment.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"environments.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	_, err = scanEnvironment(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (a *environments) getEnvironment(ctx context.Context, exp goqu.Ex) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "db.getEnvironment")
	defer span.End()

	query := dialect.From(goqu.T("environments")).
		Prepared(true).
		Select(a.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"environments.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	environment, err := scanEnvironment(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return environment, nil
}

func (*environments) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range environmentFieldList {
		selectFields = append(selectFields, fmt.Sprintf("environments.%s", field))
	}

	selectFields = append(selectFields, "projects.name")
	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanEnvironment(row scanner) (*models.Environment, error) {
	var organizationName, projectName string

	environment := &models.Environment{}

	fields := []interface{}{
		&environment.Metadata.ID,
		&environment.Metadata.CreationTimestamp,
		&environment.Metadata.LastUpdatedTimestamp,
		&environment.Metadata.Version,
		&environment.Name,
		&environment.Description,
		&environment.ProjectID,
		&environment.CreatedBy,
		&projectName,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	environment.Metadata.PRN = models.EnvironmentResource.BuildPRN(organizationName, projectName, environment.Name)

	return environment, nil
}
