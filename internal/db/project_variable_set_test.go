//go:build integration

package db

import (
	"context"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (pvs ProjectVariableSetSortableField) getValue() string {
	return string(pvs)
}

func TestGetProjectVariableSetByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	variableSet, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
		ProjectID: project.Metadata.ID,
		Latest:    true,
		Revision:  "1",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode          errors.CodeType
		name                     string
		id                       string
		expectProjectVariableSet bool
	}

	testCases := []testCase{
		{
			name:                     "get resource by id",
			id:                       variableSet.Metadata.ID,
			expectProjectVariableSet: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			variableSet, err := testClient.client.ProjectVariableSets.GetProjectVariableSetByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProjectVariableSet {
				require.NotNil(t, variableSet)
				assert.Equal(t, test.id, variableSet.Metadata.ID)
			} else {
				assert.Nil(t, variableSet)
			}
		})
	}
}

func TestGetProjectVariableSetByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	variableSet, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
		ProjectID: project.Metadata.ID,
		Latest:    true,
		Revision:  "1",
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode          errors.CodeType
		name                     string
		prn                      string
		expectProjectVariableSet bool
	}

	testCases := []testCase{
		{
			name:                     "get resource by PRN",
			prn:                      variableSet.Metadata.PRN,
			expectProjectVariableSet: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.ProjectVariableSetResource.BuildPRN(nonExistentID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			variableSet, err := testClient.client.ProjectVariableSets.GetProjectVariableSetByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProjectVariableSet {
				require.NotNil(t, variableSet)
				assert.Equal(t, test.prn, variableSet.Metadata.PRN)
			} else {
				assert.Nil(t, variableSet)
			}
		})
	}
}

func TestCreateProjectVariableSet(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		projectID       string
		latest          bool
		revision        string
	}

	testCases := []testCase{
		{
			name:      "successfully create resource",
			projectID: project.Metadata.ID,
			latest:    true,
			revision:  "1",
		},
		{
			name:            "fail to create resource because latest already exists",
			projectID:       project.Metadata.ID,
			latest:          true,
			revision:        "2",
			expectErrorCode: errors.EConflict,
		},
		{
			name:            "fail to create resource because revision already exists",
			projectID:       project.Metadata.ID,
			latest:          false,
			revision:        "1",
			expectErrorCode: errors.EConflict,
		},
		{
			name:            "non-existent project ID",
			projectID:       nonExistentID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			variableSet, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
				ProjectID: test.projectID,
				Latest:    test.latest,
				Revision:  test.revision,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, variableSet)
		})
	}
}

func TestDeleteProjectVariableSet(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	variableSet, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
		ProjectID: project.Metadata.ID,
		Latest:    true,
		Revision:  "1",
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              variableSet.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      variableSet.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.ProjectVariableSets.DeleteProjectVariableSet(ctx, &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestAddProjectVariablesToSet(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	variableSet, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
		ProjectID: project.Metadata.ID,
		Latest:    true,
		Revision:  "1",
	})
	require.Nil(t, err)

	v1, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
		Key:              "foo",
		Value:            "bar",
		PipelineType:     models.DeploymentPipelineType,
		EnvironmentScope: "",
		CreatedBy:        "test-user",
		ProjectID:        project.Metadata.ID,
	})
	require.Nil(t, err)

	v2, err := testClient.client.ProjectVariables.CreateProjectVariable(ctx, &models.ProjectVariable{
		Key:              "foo",
		Value:            "bar",
		PipelineType:     models.DeploymentPipelineType,
		EnvironmentScope: "",
		CreatedBy:        "test-user",
		ProjectID:        project.Metadata.ID,
	})
	require.Nil(t, err)

	err = testClient.client.ProjectVariableSets.AddProjectVariablesToSet(ctx, variableSet, []*models.ProjectVariable{v1, v2})
	require.Nil(t, err)

	// Get the variable set to verify the variables were added
	variables, err := testClient.client.ProjectVariables.GetProjectVariables(ctx, &GetProjectVariablesInput{
		Filter: &ProjectVariableFilter{
			ProjectVariableSetID: &variableSet.Metadata.ID,
		},
	})
	require.Nil(t, err)

	assert.ElementsMatch(t, []*models.ProjectVariable{v1, v2}, variables.ProjectVariables)
}

func TestGetProjectVariableSets(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	proj1, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	proj2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	vs1, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
		ProjectID: proj1.Metadata.ID,
		Latest:    true,
		Revision:  "1",
	})
	require.Nil(t, err)

	vs2, err := testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
		ProjectID: proj2.Metadata.ID,
		Latest:    true,
		Revision:  "1",
	})
	require.Nil(t, err)

	type testCase struct {
		filter             *ProjectVariableSetFilter
		name               string
		expectErrorCode    errors.CodeType
		expectVariableSets []*models.ProjectVariableSet
	}

	testCases := []testCase{
		{
			name: "return all variable sets in project 1",
			filter: &ProjectVariableSetFilter{
				ProjectID: &proj1.Metadata.ID,
			},
			expectVariableSets: []*models.ProjectVariableSet{vs1},
		},
		{
			name: "return all variable sets in project 2",
			filter: &ProjectVariableSetFilter{
				ProjectID: &proj2.Metadata.ID,
			},
			expectVariableSets: []*models.ProjectVariableSet{vs2},
		},
		{
			name: "return all variable sets matching specific IDs",
			filter: &ProjectVariableSetFilter{
				VariableSetIDs: []string{vs1.Metadata.ID, vs2.Metadata.ID},
			},
			expectVariableSets: []*models.ProjectVariableSet{vs1, vs2},
		},
		{
			name: "return all variables matching a combination of filters",
			filter: &ProjectVariableSetFilter{
				VariableSetIDs: []string{vs1.Metadata.ID, vs2.Metadata.ID},
				ProjectID:      &proj1.Metadata.ID,
			},
			expectVariableSets: []*models.ProjectVariableSet{vs1},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.ProjectVariableSets.GetProjectVariableSets(ctx, &GetProjectVariableSetsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.ElementsMatch(t, test.expectVariableSets, result.ProjectVariableSets)
		})
	}
}

func TestGetProjectVariableSetsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.ProjectVariableSets.CreateProjectVariableSet(ctx, &models.ProjectVariableSet{
			ProjectID: project.Metadata.ID,
			Latest:    false,
			Revision:  strconv.Itoa(i),
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		ProjectVariableSetSortableFieldCreatedAtAsc,
		ProjectVariableSetSortableFieldCreatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := ProjectVariableSetSortableField(sortByField.getValue())

		result, err := testClient.client.ProjectVariableSets.GetProjectVariableSets(ctx, &GetProjectVariableSetsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.ProjectVariableSets {
			resources = append(resources, resource)
		}

		return result.PageInfo, resources, nil
	})
}
