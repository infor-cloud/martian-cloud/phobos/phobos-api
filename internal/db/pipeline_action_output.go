package db

//go:generate go tool mockery --name PipelineActionOutputs --inpackage --case underscore

import (
	"context"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// PipelineActionOutputs encapsulates the logic to access state version outputs from the database
type PipelineActionOutputs interface {
	CreatePipelineActionOutput(ctx context.Context, output *models.PipelineActionOutput) (*models.PipelineActionOutput, error)
	GetPipelineActionOutputs(ctx context.Context, pipelineID string, actions []string) ([]*models.PipelineActionOutput, error)
	DeletePipelineActionOutput(ctx context.Context, output *models.PipelineActionOutput) error
}

type pipelineActionOutputs struct {
	dbClient *Client
}

var pipelineActionOutputFieldList = append(metadataFieldList,
	"name", "value", "type", "pipeline_id", "action_path")

// NewPipelineActionOutputs returns an instance of the PipelineActionOutput interface
func NewPipelineActionOutputs(dbClient *Client) PipelineActionOutputs {
	return &pipelineActionOutputs{dbClient: dbClient}
}

// CreatePipelineActionOutput creates a new action output
func (o *pipelineActionOutputs) CreatePipelineActionOutput(ctx context.Context, output *models.PipelineActionOutput) (*models.PipelineActionOutput, error) {
	ctx, span := tracer.Start(ctx, "db.CreatePipelineActionOutput")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Insert("pipeline_action_outputs").
		Prepared(true).
		Rows(goqu.Record{
			"id":          newResourceID(),
			"version":     initialResourceVersion,
			"created_at":  timestamp,
			"updated_at":  timestamp,
			"name":        output.Name,
			"value":       output.Value,
			"type":        output.Type,
			"pipeline_id": output.PipelineID,
			"action_path": output.ActionPath,
		}).
		Returning(pipelineActionOutputFieldList...).ToSQL()

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdOutput, err := scanPipelineActionOutput(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		o.dbClient.logger.Error(err)
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdOutput, nil
}

// GetPipelineActionOutputs returns a slice of state version outputs.  It does _NOT_ do pagination.
func (o *pipelineActionOutputs) GetPipelineActionOutputs(ctx context.Context,
	pipelineID string, actionPaths []string) ([]*models.PipelineActionOutput, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineActionOutputs")
	defer span.End()

	ex := goqu.And()
	ex = ex.Append(goqu.Ex{"pipeline_id": pipelineID})
	ex = ex.Append(goqu.I("action_path").In(actionPaths))

	sql, args, err := dialect.From("pipeline_action_outputs").
		Prepared(true).
		Select(pipelineActionOutputFieldList...).
		Where(ex).
		ToSQL()

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	rows, err := o.dbClient.getConnection(ctx).Query(ctx, sql, args...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}
	defer rows.Close()

	// Scan rows
	results := []*models.PipelineActionOutput{}
	for rows.Next() {
		item, err := scanPipelineActionOutput(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}
		results = append(results, item)
	}

	return results, nil
}

func (o *pipelineActionOutputs) DeletePipelineActionOutput(ctx context.Context, output *models.PipelineActionOutput) error {
	ctx, span := tracer.Start(ctx, "db.DeletePipelineActionOutput")
	defer span.End()

	sql, args, err := dialect.Delete("pipeline_action_outputs").
		Prepared(true).
		Where(goqu.Ex{"id": output.Metadata.ID, "version": output.Metadata.Version}).
		Returning(pipelineActionOutputFieldList...).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanPipelineActionOutput(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func scanPipelineActionOutput(row scanner) (*models.PipelineActionOutput, error) {

	pipelineActionOutput := &models.PipelineActionOutput{}

	err := row.Scan(
		&pipelineActionOutput.Metadata.ID,
		&pipelineActionOutput.Metadata.CreationTimestamp,
		&pipelineActionOutput.Metadata.LastUpdatedTimestamp,
		&pipelineActionOutput.Metadata.Version,
		&pipelineActionOutput.Name,
		&pipelineActionOutput.Value,
		&pipelineActionOutput.Type,
		&pipelineActionOutput.PipelineID,
		&pipelineActionOutput.ActionPath,
	)
	if err != nil {
		return nil, err
	}

	pipelineActionOutput.Metadata.PRN = models.PipelineActionOutputResource.BuildPRN(
		gid.ToGlobalID(gid.PipelineActionOutputType, pipelineActionOutput.Metadata.ID),
	)

	return pipelineActionOutput, nil
}
