//go:build integration

package db

import (
	"context"
	"fmt"
	"sort"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// sortableTemplateSlice makes a slice of pipeline templates sortable
type sortableTemplateSlice []models.PipelineTemplate

func TestGetPipelineTemplateByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForPipelineTemplates)
	require.Nil(t, err)

	_, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjectsForPipelineTemplates)
	require.Nil(t, err)

	createdWarmupPTs, _, err := createInitialPipelineTemplates(ctx, testClient, projMap, standardWarmupPipelineTemplates)
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode        errors.CodeType
		name                   string
		searchID               string
		expectPipelineTemplate bool
	}

	positivePT := createdWarmupPTs[0]

	/*
		test case template

		{
			name                   string
			searchID               string
			expectErrorCode        errors.CodeType
			expectPipelineTemplate bool
		}
	*/

	testCases := []testCase{
		{
			name:                   "positive-" + positivePT.Metadata.ID,
			searchID:               positivePT.Metadata.ID,
			expectPipelineTemplate: true,
		},
		{
			name:     "negative, non-existent ID",
			searchID: nonExistentID,
			// expect pipeline template and error to be nil
		},
		{
			name:                   "defective-id",
			searchID:               invalidID,
			expectPipelineTemplate: false,
			expectErrorCode:        errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipelineTemplate, err := testClient.client.PipelineTemplates.GetPipelineTemplateByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectPipelineTemplate {
				require.NotNil(t, pipelineTemplate)
				assert.Equal(t, test.searchID, pipelineTemplate.Metadata.ID)
			} else {
				assert.Nil(t, pipelineTemplate)
			}
		})
	}
}

func TestGetPipelineTemplateByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForPipelineTemplates)
	require.Nil(t, err)

	_, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjectsForPipelineTemplates)
	require.Nil(t, err)

	createdWarmupPTs, _, err := createInitialPipelineTemplates(ctx, testClient, projMap, standardWarmupPipelineTemplates)
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode        errors.CodeType
		name                   string
		searchPRN              string
		expectPipelineTemplate bool
	}

	positivePT := createdWarmupPTs[0]

	/*
		test case template

		{
			name                   string
			searchPRN              string
			expectErrorCode        errors.CodeType
			expectPipelineTemplate bool
		}
	*/

	testCases := []testCase{
		{
			name:                   "positive-" + positivePT.Metadata.ID,
			searchPRN:              positivePT.Metadata.PRN,
			expectPipelineTemplate: true,
		},
		{
			name:      "negative, pipeline template does not exist",
			searchPRN: models.PipelineTemplateResource.BuildPRN("non-existent-org", "non-existent-project", nonExistentGlobalID),
			// expect pipeline template and error to be nil
		},
		{
			name:            "defective-prn",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipelineTemplate, err := testClient.client.PipelineTemplates.GetPipelineTemplateByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectPipelineTemplate {
				require.NotNil(t, pipelineTemplate)
				assert.Equal(t, test.searchPRN, pipelineTemplate.Metadata.PRN)
			} else {
				assert.Nil(t, pipelineTemplate)
			}
		})
	}
}

func TestGetPipelineTemplates(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForPipelineTemplates)
	require.Nil(t, err)

	createdWarmupProjects, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjectsForPipelineTemplates)
	require.Nil(t, err)

	createdWarmupPipelineTemplates, _, err := createInitialPipelineTemplates(ctx, testClient, projMap, standardWarmupPipelineTemplates)
	require.Nil(t, err)

	reversePipelineTemplates := reversePipelineTemplates(createdWarmupPipelineTemplates)

	dummyCursorFunc := func(cp pagination.CursorPaginatable) (*string, error) { return ptr.String("dummy-cursor-value"), nil }

	type testCase struct {
		expectPageInfo              pagination.PageInfo
		expectStartCursorError      error
		expectEndCursorError        error
		expectErrorCode             errors.CodeType
		input                       *GetPipelineTemplatesInput
		name                        string
		expectPipelineTemplates     []models.PipelineTemplate
		getAfterCursorFromPrevious  bool
		expectHasStartCursor        bool
		getBeforeCursorFromPrevious bool
		expectHasEndCursor          bool
	}

	/*
		test case template

		{
			name                        string
			input                       *GetPipelineTemplatesInput
			getBeforeCursorFromPrevious bool
			getAfterCursorFromPrevious  bool
			expectErrorCode             errors.CodeType
			expectPageInfo              pagination.PageInfo
			expectStartCursorError      error
			expectHasStartCursor        bool
			expectEndCursorError        error
			expectHasEndCursor          bool
			expectPipelineTemplates     []models.PipelineTemplate
		}
	*/

	testCases := []testCase{
		{
			name: "non-nil but mostly empty input",
			input: &GetPipelineTemplatesInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(createdWarmupPipelineTemplates)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "populated sort and pagination, nil filter",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
				Filter: nil,
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(createdWarmupPipelineTemplates)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "sort in ascending order",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(createdWarmupPipelineTemplates)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "sort in descending order",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtDesc),
			},
			expectPipelineTemplates: reversePipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(reversePipelineTemplates)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "pagination: everything at once",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(createdWarmupPipelineTemplates)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "pagination: first two",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(2),
				},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates[:2],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(createdWarmupPipelineTemplates)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: middle two",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(2),
				},
			},
			getAfterCursorFromPrevious: true,
			expectPipelineTemplates:    createdWarmupPipelineTemplates[2:4],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(createdWarmupPipelineTemplates)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: final two",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			getAfterCursorFromPrevious: true,
			expectPipelineTemplates:    createdWarmupPipelineTemplates[4:],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(createdWarmupPipelineTemplates)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		// When Last is supplied, the sort order is intended to be reversed.
		{
			name: "pagination: last three",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					Last: ptr.Int32(3),
				},
			},
			expectPipelineTemplates: reversePipelineTemplates[:3],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(reversePipelineTemplates)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination, before and after, expect error",
			input: &GetPipelineTemplatesInput{
				Sort:              ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{},
			},
			getAfterCursorFromPrevious:  true,
			getBeforeCursorFromPrevious: true,
			expectErrorCode:             errors.EInternal,
			expectPipelineTemplates:     []models.PipelineTemplate{},
			expectPageInfo:              pagination.PageInfo{},
		},

		{
			name: "pagination, first one and last two, expect error",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
					Last:  ptr.Int32(2),
				},
			},
			expectErrorCode:         errors.EInternal,
			expectPipelineTemplates: createdWarmupPipelineTemplates[5:],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(createdWarmupPipelineTemplates)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, empty slice of pipeline template IDs",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					PipelineTemplateIDs: []string{},
				},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, pipeline template IDs",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					PipelineTemplateIDs: []string{
						createdWarmupPipelineTemplates[0].Metadata.ID,
						createdWarmupPipelineTemplates[2].Metadata.ID,
						createdWarmupPipelineTemplates[4].Metadata.ID,
					},
				},
			},
			expectPipelineTemplates: []models.PipelineTemplate{
				createdWarmupPipelineTemplates[0],
				createdWarmupPipelineTemplates[2],
				createdWarmupPipelineTemplates[4],
			},
			expectPageInfo:       pagination.PageInfo{TotalCount: 3, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, pipeline template IDs, non-existent",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					PipelineTemplateIDs: []string{nonExistentID},
				},
			},
			expectPipelineTemplates: []models.PipelineTemplate{},
			expectPageInfo:          pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, pipeline template IDs, invalid",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					PipelineTemplateIDs: []string{invalidID},
				},
			},
			expectErrorCode: errors.EInternal,
			expectPipelineTemplates: []models.PipelineTemplate{
				createdWarmupPipelineTemplates[0],
				createdWarmupPipelineTemplates[2],
				createdWarmupPipelineTemplates[4],
			},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, empty project ID",
			input: &GetPipelineTemplatesInput{
				Sort:   ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, valid project ID",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					ProjectID: &createdWarmupProjects[0].Metadata.ID,
				},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates,
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, non-existent project ID",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					ProjectID: ptr.String(nonExistentID), // nonExistentID is an untyped string, so cannot use ampersand operator
				},
			},
			expectPipelineTemplates: []models.PipelineTemplate{},
			expectPageInfo:          pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, invalid project ID",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					ProjectID: ptr.String(invalidID), // invalidID is an untyped string, so cannot use ampersand operator
				},
			},
			expectErrorCode: errors.EInternal,
			expectPipelineTemplates: []models.PipelineTemplate{
				createdWarmupPipelineTemplates[0],
				createdWarmupPipelineTemplates[2],
				createdWarmupPipelineTemplates[4],
			},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, versioned true",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					Versioned: ptr.Bool(true),
				},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates[3:],
			expectPageInfo:          pagination.PageInfo{TotalCount: 3, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, versioned false",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					Versioned: ptr.Bool(false),
				},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates[:3],
			expectPageInfo:          pagination.PageInfo{TotalCount: 3, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, search by name, which implies versioned",
			input: &GetPipelineTemplatesInput{
				Sort: ptrPipelineTemplateSortableField(PipelineTemplateSortableFieldUpdatedAtAsc),
				Filter: &PipelineTemplateFilter{
					Search: ptr.String("aa"),
				},
			},
			expectPipelineTemplates: createdWarmupPipelineTemplates[3:4],
			expectPageInfo:          pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},
	}

	var (
		previousEndCursorValue   *string
		previousStartCursorValue *string
	)
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// For some pagination tests, a previous case's cursor value gets piped into the next case.
			if test.getAfterCursorFromPrevious || test.getBeforeCursorFromPrevious {

				// Make sure there's a place to put it.
				require.NotNil(t, test.input.PaginationOptions)

				if test.getAfterCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousEndCursorValue)
					test.input.PaginationOptions.After = previousEndCursorValue
				}

				if test.getBeforeCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousStartCursorValue)
					test.input.PaginationOptions.Before = previousStartCursorValue
				}

				// Clear the values so they won't be used twice.
				previousEndCursorValue = nil
				previousStartCursorValue = nil
			}

			pipelineTemplatesResult, err := testClient.client.PipelineTemplates.GetPipelineTemplates(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			// If there was no error, check the results.
			if err == nil {

				// Never returns nil if error is nil.
				require.NotNil(t, pipelineTemplatesResult.PageInfo)
				assert.NotNil(t, pipelineTemplatesResult.PipelineTemplates)
				pageInfo := pipelineTemplatesResult.PageInfo
				pipelineTemplates := sortableTemplateSlice(pipelineTemplatesResult.PipelineTemplates)

				// If no sort direction was specified, sort the results here for repeatability.
				if test.input.Sort == nil {
					sort.Sort(sortableTemplateSlice(test.expectPipelineTemplates))
					sort.Sort(sortableTemplateSlice(pipelineTemplates))
				}

				assert.Equal(t, len(test.expectPipelineTemplates), len(pipelineTemplates))
				for i := 0; i < len(test.expectPipelineTemplates); i++ {
					assert.Equal(t, test.expectPipelineTemplates[i], pipelineTemplates[i])
				}

				assert.Equal(t, test.expectPageInfo.HasNextPage, pageInfo.HasNextPage)
				assert.Equal(t, test.expectPageInfo.HasPreviousPage, pageInfo.HasPreviousPage)
				assert.Equal(t, test.expectPageInfo.TotalCount, pageInfo.TotalCount)
				assert.Equal(t, test.expectPageInfo.Cursor != nil, pageInfo.Cursor != nil)

				// Compare the cursor function results only if there is at least one pipeline template returned.
				// If there are no pipeline templates returned, there is no argument to pass to the cursor function.
				// Also, don't try to reverse engineer to compare the cursor string values.
				if len(pipelineTemplates) > 0 {
					resultStartCursor, resultStartCursorError := pageInfo.Cursor(&pipelineTemplates[0])
					resultEndCursor, resultEndCursorError := pageInfo.Cursor(&pipelineTemplates[len(pipelineTemplates)-1])
					assert.Equal(t, test.expectStartCursorError, resultStartCursorError)
					assert.Equal(t, test.expectHasStartCursor, resultStartCursor != nil)
					assert.Equal(t, test.expectEndCursorError, resultEndCursorError)
					assert.Equal(t, test.expectHasEndCursor, resultEndCursor != nil)

					// Capture the ending cursor values for the next case.
					previousEndCursorValue = resultEndCursor
					previousStartCursorValue = resultStartCursor
				}
			}
		})
	}
}

func TestCreatePipelineTemplate(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForPipelineTemplates)
	require.Nil(t, err)

	_, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjectsForPipelineTemplates)
	require.Nil(t, err)

	type testCase struct {
		expectMsg      *string
		name           string
		toCreate       []models.PipelineTemplate
		isPositiveCase bool
	}

	testCases := []testCase{
		{
			name:           "positive, standard warmup pipeline templates",
			isPositiveCase: true,
			toCreate:       standardWarmupPipelineTemplates,
			// expect message to be nil
		},

		// There is currently no negative duplicate case, because there is no name or similar unique field.

	}

	cumulativeToCreate := []models.PipelineTemplate{}
	cumulativeClaimedCreated := []models.PipelineTemplate{}
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			if test.isPositiveCase {

				// Capture any that should be created.
				cumulativeToCreate = append(cumulativeToCreate, test.toCreate...)

				// For a positive case, just use the utility function.
				claimedCreated, _, err := createInitialPipelineTemplates(ctx, testClient, projMap, test.toCreate)
				require.Nil(t, err)

				// Capture any new claimed to be created.
				cumulativeClaimedCreated = append(cumulativeClaimedCreated, claimedCreated...)
			} else {
				// For a negative case, do each pipeline template one at a time.
				for _, input := range test.toCreate {
					_, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
						ProjectID: input.ProjectID,
						Status:    input.Status,
					})
					require.NotNil(t, err)
					assert.Equal(t, *test.expectMsg, err.Error())
				}
			}

			// Get the pipeline templates for comparison.
			gotResult, err := testClient.client.PipelineTemplates.GetPipelineTemplates(ctx, &GetPipelineTemplatesInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			})
			require.Nil(t, err)
			retrievedPipelineTemplates := gotResult.PipelineTemplates

			// Compare lengths.
			assert.Equal(t, len(cumulativeToCreate), len(cumulativeClaimedCreated))
			assert.Equal(t, len(cumulativeToCreate), len(retrievedPipelineTemplates))

			// Compare the contents of the cumulative pipeline templates to create, the pipeline templates reportedly created,
			// and the pipeline templates retrieved.  The three slices aren't guaranteed to be in the same order,
			// so it is necessary to look them up. Metadata ID is used for that purpose.
			copyCumulativeClaimedCreated := cumulativeClaimedCreated
			for _, toldToCreate := range cumulativeToCreate {
				var claimedCreated, retrieved models.PipelineTemplate
				claimedCreated, copyCumulativeClaimedCreated, err = removeMatchingPipelineTemplate(toldToCreate.CreatedBy,
					copyCumulativeClaimedCreated)
				assert.Nil(t, err)
				if err != nil {
					break
				}

				retrieved, retrievedPipelineTemplates, err = removeMatchingPipelineTemplate(toldToCreate.CreatedBy,
					retrievedPipelineTemplates)
				assert.Nil(t, err)
				if err != nil {
					break
				}

				// Because claimedCreated and retrieved have a real UUID but toldToCreate has the project name as project ID,
				// must convert project name to project ID.
				var ok bool
				toldToCreate.ProjectID, ok = projMap[toldToCreate.ProjectID]
				assert.True(t, ok)

				comparePipelineTemplatesCreate(t, toldToCreate, claimedCreated)
				comparePipelineTemplatesCreate(t, toldToCreate, retrieved)
			}

			// Must not have any leftovers.
			assert.Equal(t, 0, len(copyCumulativeClaimedCreated))
			assert.Equal(t, 0, len(retrievedPipelineTemplates))
		})
	}
}

func TestUpdatePipelineTemplate(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForPipelineTemplates)
	require.Nil(t, err)

	_, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjectsForPipelineTemplates)
	require.Nil(t, err)

	createdWarmupPipelineTemplates, _, err := createInitialPipelineTemplates(ctx, testClient, projMap, standardWarmupPipelineTemplates)
	require.Nil(t, err)

	type testCase struct {
		findID               *string
		findPipelineTemplate *models.PipelineTemplate
		expectErrorCode      errors.CodeType
		name                 string
		isPositive           bool
	}

	/*
		test case template

		{
			name                 string
			isPositive           bool
			findID               *string
			findPipelineTemplate *models.PipelineTemplate
			expectErrorCode      errors.CodeType
		}
	*/

	testCases := []testCase{}
	for _, positivePipelineTemplate := range createdWarmupPipelineTemplates {
		testCases = append(testCases, testCase{
			name:       "positive-" + positivePipelineTemplate.Metadata.ID,
			findID:     &positivePipelineTemplate.Metadata.ID,
			isPositive: true,
		})
	}

	testCases = append(testCases,
		testCase{
			name: "negative, not exist",
			findPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EOptimisticLock,
		},
		testCase{
			name: "negative, invalid uuid",
			findPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EInternal,
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			var originalPipelineTemplate *models.PipelineTemplate
			switch {

			case (test.findID != nil) && (test.findPipelineTemplate == nil):
				originalPipelineTemplate, err = testClient.client.PipelineTemplates.GetPipelineTemplateByID(ctx, *test.findID)
				require.Nil(t, err)
				require.NotNil(t, originalPipelineTemplate)

			case (test.findID == nil) && (test.findPipelineTemplate != nil):
				originalPipelineTemplate = test.findPipelineTemplate

			default:
				assert.Equal(t, "test case should use exactly one of findName, findPipelineTemplate",
					"test case violated that rule")
				// No point in going forward with this test case.
				return
			}

			expectUpdatedStatus := rotatePipelineTemplateStatus(originalPipelineTemplate.Status)
			copyOriginalPipelineTemplate := originalPipelineTemplate
			copyOriginalPipelineTemplate.Status = *expectUpdatedStatus

			claimedUpdatedPipelineTemplate, err := testClient.client.PipelineTemplates.UpdatePipelineTemplate(ctx, copyOriginalPipelineTemplate)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.isPositive {

				require.NotNil(t, claimedUpdatedPipelineTemplate)

				comparePipelineTemplatesUpdate(t, *expectUpdatedStatus, *originalPipelineTemplate, *claimedUpdatedPipelineTemplate)

				retrieved, err := testClient.client.PipelineTemplates.GetPipelineTemplateByID(ctx, originalPipelineTemplate.Metadata.ID)
				require.Nil(t, err)

				require.NotNil(t, retrieved)

				comparePipelineTemplatesUpdate(t, *expectUpdatedStatus, *originalPipelineTemplate, *retrieved)
			}
		})
	}
}

func TestDeletePipelineTemplate(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	_, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizationsForPipelineTemplates)
	require.Nil(t, err)

	_, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjectsForPipelineTemplates)
	require.Nil(t, err)

	createdWarmupPipelineTemplates, _, err := createInitialPipelineTemplates(ctx, testClient, projMap, standardWarmupPipelineTemplates)
	require.Nil(t, err)

	type testCase struct {
		findPipelineTemplate *models.PipelineTemplate
		expectErrorCode      errors.CodeType
		name                 string
		isPositive           bool
	}

	/*
		test case template

		{
			name                 string
			isPositive           bool
			findPipelineTemplate *models.PipelineTemplate
			expectErrorCode      errors.CodeType
		}
	*/

	testCases := []testCase{}
	for ix := range createdWarmupPipelineTemplates {
		pt := &createdWarmupPipelineTemplates[ix]
		testCases = append(testCases, testCase{
			name:                 "positive-" + pt.Metadata.ID,
			findPipelineTemplate: pt,
			isPositive:           true,
		})
	}

	testCases = append(testCases,
		testCase{
			name: "negative, not exist",
			findPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EOptimisticLock,
		},
		testCase{
			name: "negative, invalid uuid",
			findPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EInternal,
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			err := testClient.client.PipelineTemplates.DeletePipelineTemplate(ctx, test.findPipelineTemplate)

			assert.Equal(t, (test.expectErrorCode != ""), (err != nil))
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

		})
	}
}

// standardWarmupOrganizations for tests in this module.
// One should suffice for this module.
var standardWarmupOrganizationsForPipelineTemplates = []models.Organization{
	{
		Description: "organization-1 for testing organization functions",
		Name:        "organization-1",
		CreatedBy:   "someone",
	},
}

// standardWarmupProjectsForPipelineTemplates for tests in this module.
var standardWarmupProjectsForPipelineTemplates = []models.Project{
	{
		Description: "project-1 for testing pipeline template functions",
		Name:        "project-1",
		CreatedBy:   "someone",
		OrgID:       "organization-1", // will be fixed later
	},
}

// standardWarmupPipelineTemplates for tests in this module.
var standardWarmupPipelineTemplates = []models.PipelineTemplate{
	{
		CreatedBy: "someone-0",
		ProjectID: "project-1", // will be fixed later
		Versioned: false,
	},
	{
		CreatedBy: "someone-1",
		ProjectID: "project-1", // will be fixed later
		Versioned: false,
	},
	{
		CreatedBy: "someone-2",
		ProjectID: "project-1", // will be fixed later
		Versioned: false,
	},
	{
		CreatedBy:       "someone-3",
		ProjectID:       "project-1", // will be fixed later
		Versioned:       true,
		Name:            ptr.String("aa-pipeline-template-3"),
		SemanticVersion: ptr.String("1.2.3"),
	},
	{
		CreatedBy:       "someone-4",
		ProjectID:       "project-1", // will be fixed later
		Versioned:       true,
		Name:            ptr.String("ab-pipeline-template-4"),
		SemanticVersion: ptr.String("1.2.4"),
	},
	{
		CreatedBy:       "someone-5",
		ProjectID:       "project-1", // will be fixed later
		Versioned:       true,
		Name:            ptr.String("bc-pipeline-template-5"),
		SemanticVersion: ptr.String("1.2.5"),
	},
}

func reversePipelineTemplates(arg []models.PipelineTemplate) []models.PipelineTemplate {
	result := []models.PipelineTemplate{}

	for i := len(arg) - 1; i >= 0; i-- {
		result = append(result, arg[i])
	}

	return result
}

func ptrPipelineTemplateSortableField(arg PipelineTemplateSortableField) *PipelineTemplateSortableField {
	return &arg
}

func (s sortableTemplateSlice) Len() int {
	return len(s)
}

func (s sortableTemplateSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortableTemplateSlice) Less(i, j int) bool {
	return s[i].CreatedBy < s[j].CreatedBy
}

// removeMatchingPipelineTemplate finds a pipeline template in a slice with a specified CreatedBy field.
// It returns the found pipeline template, a shortened slice, and an error.
func removeMatchingPipelineTemplate(lookFor string, oldSlice []models.PipelineTemplate) (models.PipelineTemplate, []models.PipelineTemplate, error) {
	found := models.PipelineTemplate{}
	foundOne := false
	newSlice := []models.PipelineTemplate{}

	for _, candidate := range oldSlice {
		if candidate.CreatedBy == lookFor {
			found = candidate
			foundOne = true
		} else {
			newSlice = append(newSlice, candidate)
		}
	}

	if !foundOne {
		return models.PipelineTemplate{}, nil, fmt.Errorf("Failed to find pipeline template with CreatedBy: %s", lookFor)
	}

	return found, newSlice, nil
}

// comparePipelineTemplatesCreate compares two pipeline templates for TestCreatePipelineTemplate
// There are some fields that cannot be compared, or DeepEqual would work.
func comparePipelineTemplatesCreate(t *testing.T, expected, actual models.PipelineTemplate) {
	assert.Equal(t, expected.Status, actual.Status)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)

	assert.Equal(t, expected.ProjectID, actual.ProjectID)
}

// rotatePipelineTemplateStatus takes an original description and returns a modified version for TestUpdatePipelineTemplate
func rotatePipelineTemplateStatus(input models.PipelineTemplateStatus) *models.PipelineTemplateStatus {
	var result models.PipelineTemplateStatus
	switch input {
	case models.PipelineTemplatePending:
		result = models.PipelineTemplateUploaded
	case models.PipelineTemplateUploaded:
		result = models.PipelineTemplatePending
	}

	return &result
}

// comparePipelineTemplatesUpdate compares two pipeline templates for TestUpdatePipelineTemplate
func comparePipelineTemplatesUpdate(t *testing.T, expectedStatus models.PipelineTemplateStatus,
	expected, actual models.PipelineTemplate) {
	assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	assert.Equal(t, expected.Metadata.CreationTimestamp, actual.Metadata.CreationTimestamp)
	assert.NotEqual(t, expected.Metadata.Version, actual.Metadata.Version)
	assert.NotEqual(t, expected.Metadata.LastUpdatedTimestamp, actual.Metadata.LastUpdatedTimestamp)

	// Not possible to know the PRN in advance since it requires an ID.
	assert.NotEmpty(t, actual.Metadata.PRN)

	assert.Equal(t, expectedStatus, actual.Status)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)

	assert.Equal(t, expected.ProjectID, actual.ProjectID)
}

// createInitialPipelineTemplates creates some PipelineTemplates for a test.
// Because there is no name field, this function returns a map from CreatedBy to ID.
func createInitialPipelineTemplates(ctx context.Context, testClient *testClient, projMap map[string]string,
	toCreate []models.PipelineTemplate) ([]models.PipelineTemplate, map[string]string, error) {
	result := []models.PipelineTemplate{}
	createdBy2ID := make(map[string]string)

	for _, pt := range toCreate {
		projID, ok := projMap[pt.ProjectID]
		if !ok {
			return nil, nil, fmt.Errorf("Failed to look up parent project ID in createInitialPipelineTemplates: %s", pt.ProjectID)
		}
		pt.ProjectID = projID

		created, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &pt)
		if err != nil {
			return nil, nil, err
		}

		result = append(result, *created)
		createdBy2ID[created.CreatedBy] = created.Metadata.ID
	}

	return result, createdBy2ID, nil
}
