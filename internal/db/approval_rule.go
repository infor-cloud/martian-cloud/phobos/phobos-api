package db

//go:generate go tool mockery --name ApprovalRules --inpackage --case underscore

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ApprovalRuleSortableField represents the fields that an organization can be sorted by
type ApprovalRuleSortableField string

// ApprovalRuleSortableField constants
const (
	ApprovalRuleSortableFieldUpdatedAtAsc  ApprovalRuleSortableField = "UPDATED_AT_ASC"
	ApprovalRuleSortableFieldUpdatedAtDesc ApprovalRuleSortableField = "UPDATED_AT_DESC"
)

func (os ApprovalRuleSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case ApprovalRuleSortableFieldUpdatedAtAsc, ApprovalRuleSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "approval_rules", Col: "updated_at"}
	default:
		return nil
	}
}

func (os ApprovalRuleSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// ApprovalRuleFilter contains the supported fields for filtering ApprovalRule resources
type ApprovalRuleFilter struct {
	OrgID              *string
	ProjectID          *string
	ApprovalRuleScopes []models.ScopeType
	ApprovalRuleIDs    []string
}

// GetApprovalRulesInput is the input for listing organizations
type GetApprovalRulesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ApprovalRuleSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ApprovalRuleFilter
}

// ApprovalRulesResult contains the response data and page information
type ApprovalRulesResult struct {
	PageInfo      *pagination.PageInfo
	ApprovalRules []*models.ApprovalRule
}

// ApprovalRules encapsulates the logic to access state version reqs from the database
type ApprovalRules interface {
	GetApprovalRuleByPRN(ctx context.Context, prn string) (*models.ApprovalRule, error)
	GetApprovalRuleByID(ctx context.Context, id string) (*models.ApprovalRule, error)
	CreateApprovalRule(ctx context.Context, req *models.ApprovalRule) (*models.ApprovalRule, error)
	UpdateApprovalRule(ctx context.Context, rule *models.ApprovalRule) (*models.ApprovalRule, error)
	GetApprovalRules(ctx context.Context, input *GetApprovalRulesInput) (*ApprovalRulesResult, error)
	DeleteApprovalRule(ctx context.Context, rule *models.ApprovalRule) error
}

type approvalRuleEligibleApprover struct {
	UserID           *string
	ServiceAccountID *string
	TeamID           *string
	RuleID           string
}

type approvalRules struct {
	dbClient *Client
}

var approvalRuleFieldList = append(metadataFieldList,
	"created_by", "name", "description", "organization_id", "approvals_required", "scope", "project_id")

// NewApprovalRules returns an instance of the ApprovalRules interface
func NewApprovalRules(dbClient *Client) ApprovalRules {
	return &approvalRules{dbClient: dbClient}
}

func (a *approvalRules) GetApprovalRuleByPRN(ctx context.Context, prn string) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "db.GetApprovalRuleByPR")
	defer span.End()

	path, err := models.ApprovalRuleResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	ex := goqu.Ex{}

	switch len(parts) {
	case 2:
		// This is an org/approval rule name.
		ex["organizations.name"] = parts[0]
		ex["approval_rules.name"] = parts[1]
		ex["approval_rules.scope"] = models.OrganizationScope
	case 3:
		// This is a project-scoped approval rule path.
		ex["organizations.name"] = parts[0]
		ex["projects.name"] = parts[1]
		ex["approval_rules.name"] = parts[2]
		ex["approval_rules.scope"] = models.ProjectScope
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return a.getApprovalRule(ctx, ex)
}

func (a *approvalRules) GetApprovalRuleByID(ctx context.Context, id string) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "db.GetApprovalRuleByID")
	defer span.End()

	return a.getApprovalRule(ctx, goqu.Ex{
		"approval_rules.id": id,
	})
}

// CreateApprovalRule creates a new approval rule
func (a *approvalRules) CreateApprovalRule(ctx context.Context,
	req *models.ApprovalRule) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "db.CreateApprovalRule")
	defer span.End()

	timestamp := currentTime()

	tx, err := a.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			a.dbClient.logger.Errorf("failed to rollback tx: %v", txErr)
		}
	}()

	sql, args, err := dialect.From("approval_rules").
		Prepared(true).
		With("approval_rules",
			dialect.Insert("approval_rules").Rows(goqu.Record{
				"id":                 newResourceID(),
				"version":            initialResourceVersion,
				"created_at":         timestamp,
				"updated_at":         timestamp,
				"created_by":         req.CreatedBy,
				"name":               req.Name,
				"description":        req.Description,
				"organization_id":    req.OrgID,
				"approvals_required": req.ApprovalsRequired,
				"scope":              req.Scope,
				"project_id":         req.ProjectID,
			}).Returning("*"),
		).Select(a.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"approval_rules.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"approval_rules.project_id": goqu.I("projects.id")})).
		ToSQL()

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdRule, err := scanApprovalRule(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New(
					"rule with name %s already exists in organization", req.Name,
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EConflict),
				)
			}
			if isForeignKeyViolation(pgErr) && pgErr.ConstraintName == "fk_organization_id" {
				return nil, errors.New("organization does not exist",
					errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) && pgErr.ConstraintName == "fk_approval_rule_project_id" {
				return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	for _, userID := range req.UserIDs {
		userIDCopy := userID
		if err := a.insertEligibleApprover(ctx, tx, &approvalRuleEligibleApprover{
			RuleID: createdRule.Metadata.ID,
			UserID: &userIDCopy,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert eligible approver", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	for _, serviceAccountID := range req.ServiceAccountIDs {
		serviceAccountIDCopy := serviceAccountID
		if err := a.insertEligibleApprover(ctx, tx, &approvalRuleEligibleApprover{
			RuleID:           createdRule.Metadata.ID,
			ServiceAccountID: &serviceAccountIDCopy,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert eligible approver", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	for _, teamID := range req.TeamIDs {
		teamIDCopy := teamID
		if err := a.insertEligibleApprover(ctx, tx, &approvalRuleEligibleApprover{
			RuleID: createdRule.Metadata.ID,
			TeamID: &teamIDCopy,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert eligible approver", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	if err := a.hydrateRules(ctx, tx, []*models.ApprovalRule{createdRule}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate rules", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit tx", errors.WithSpan(span))
	}

	return createdRule, nil
}

// UpdateApprovalRule updates an existing approval rule
func (a *approvalRules) UpdateApprovalRule(ctx context.Context, rule *models.ApprovalRule) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateApprovalRule")
	defer span.End()

	timestamp := currentTime()

	tx, err := a.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			a.dbClient.logger.Errorf("failed to rollback tx: %v", txErr)
		}
	}()

	sql, args, err := dialect.From("approval_rules").
		Prepared(true).
		With("approval_rules",
			dialect.Update("approval_rules").
				Set(goqu.Record{
					"version":            goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":         timestamp,
					"description":        rule.Description,
					"approvals_required": rule.ApprovalsRequired,
				}).
				Where(goqu.Ex{"id": rule.Metadata.ID, "version": rule.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"approval_rules.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"approval_rules.project_id": goqu.I("projects.id")})).
		ToSQL()

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedRule, err := scanApprovalRule(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		a.dbClient.logger.Error(err)
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	deleteEligibleApproversSQL, args, err := dialect.Delete("approval_rule_eligible_approvers").
		Prepared(true).
		Where(
			goqu.Ex{
				"approval_rule_id": rule.Metadata.ID,
			},
		).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = tx.Exec(ctx, deleteEligibleApproversSQL, args...); err != nil {
		return nil, errors.Wrap(err, "failed to execute DB query", errors.WithSpan(span))
	}

	for _, userID := range rule.UserIDs {
		userIDCopy := userID
		if err := a.insertEligibleApprover(ctx, tx, &approvalRuleEligibleApprover{
			RuleID: updatedRule.Metadata.ID,
			UserID: &userIDCopy,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert eligible approver", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	for _, serviceAccountID := range rule.ServiceAccountIDs {
		serviceAccountIDCopy := serviceAccountID
		if err := a.insertEligibleApprover(ctx, tx, &approvalRuleEligibleApprover{
			RuleID:           updatedRule.Metadata.ID,
			ServiceAccountID: &serviceAccountIDCopy,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert eligible approver", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	for _, teamID := range rule.TeamIDs {
		teamIDCopy := teamID
		if err := a.insertEligibleApprover(ctx, tx, &approvalRuleEligibleApprover{
			RuleID: updatedRule.Metadata.ID,
			TeamID: &teamIDCopy,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert eligible approver", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	if err := a.hydrateRules(ctx, tx, []*models.ApprovalRule{updatedRule}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate rules", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit tx", errors.WithSpan(span))
	}

	return updatedRule, nil
}

func (a *approvalRules) DeleteApprovalRule(ctx context.Context, rule *models.ApprovalRule) error {
	ctx, span := tracer.Start(ctx, "db.DeleteApprovalRule")
	defer span.End()

	sql, args, err := dialect.From("approval_rules").
		Prepared(true).
		With("approval_rules",
			dialect.Delete("approval_rules").
				Where(goqu.Ex{"id": rule.Metadata.ID, "version": rule.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"approval_rules.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"approval_rules.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	_, err = scanApprovalRule(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (a *approvalRules) GetApprovalRules(ctx context.Context, input *GetApprovalRulesInput) (*ApprovalRulesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetApprovalRules")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.OrgID != nil {
			ex = ex.Append(goqu.I("approval_rules.organization_id").Eq(*input.Filter.OrgID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(
				// Use an OR condition to get both organization and project approval rules.
				goqu.Or(
					goqu.I("approval_rules.project_id").Eq(*input.Filter.ProjectID),
					goqu.And(
						goqu.I("approval_rules.organization_id").In(
							dialect.From("projects").
								Select("org_id").
								Where(goqu.I("id").Eq(*input.Filter.ProjectID)),
						),
						goqu.I("approval_rules.scope").Eq(models.OrganizationScope),
					),
				),
			)
		}

		if len(input.Filter.ApprovalRuleScopes) > 0 {
			ex = ex.Append(goqu.I("approval_rules.scope").In(input.Filter.ApprovalRuleScopes))
		}

		if input.Filter.ApprovalRuleIDs != nil {
			ex = ex.Append(goqu.I("approval_rules.id").In(input.Filter.ApprovalRuleIDs))
		}
	}

	query := dialect.From(goqu.T("approval_rules")).Select(a.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"approval_rules.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"approval_rules.project_id": goqu.I("projects.id")})).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "approval_rules", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, a.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.ApprovalRule{}
	for rows.Next() {
		item, err := scanApprovalRule(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	if err := a.hydrateRules(ctx, a.dbClient.getConnection(ctx), results); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate rules", errors.WithSpan(span))
	}

	result := &ApprovalRulesResult{
		PageInfo:      rows.GetPageInfo(),
		ApprovalRules: results,
	}

	return result, nil
}

func (a *approvalRules) hydrateRules(ctx context.Context, conn connection, rules []*models.ApprovalRule) error {
	if len(rules) == 0 {
		return nil
	}

	ruleIDs := []string{}
	for _, rule := range rules {
		ruleIDs = append(ruleIDs, rule.Metadata.ID)
	}

	sql, args, err := dialect.From("approval_rule_eligible_approvers").
		Prepared(true).
		Select("approval_rule_id", "user_id", "service_account_id", "team_id").
		Where(goqu.I("approval_rule_id").In(ruleIDs)).ToSQL()
	if err != nil {
		return err
	}

	rows, err := conn.Query(ctx, sql, args...)
	if err != nil {
		return err
	}

	defer rows.Close()

	ruleMap := map[string]*models.ApprovalRule{}
	for _, rule := range rules {
		rule.UserIDs = []string{}
		rule.ServiceAccountIDs = []string{}
		rule.TeamIDs = []string{}

		ruleMap[rule.Metadata.ID] = rule
	}

	// Scan rows
	for rows.Next() {
		approver, err := scanApprovalRuleEligibleApprover(rows)
		if err != nil {
			return err
		}

		rule, ok := ruleMap[approver.RuleID]
		if !ok {
			return fmt.Errorf("rule not found in rule map: %s", approver.RuleID)
		}

		if approver.UserID != nil {
			rule.UserIDs = append(rule.UserIDs, *approver.UserID)
		}

		if approver.ServiceAccountID != nil {
			rule.ServiceAccountIDs = append(rule.ServiceAccountIDs, *approver.ServiceAccountID)
		}

		if approver.TeamID != nil {
			rule.TeamIDs = append(rule.TeamIDs, *approver.TeamID)
		}
	}

	return nil
}

func (a *approvalRules) insertEligibleApprover(ctx context.Context, con connection, approver *approvalRuleEligibleApprover) error {
	sql, args, err := dialect.Insert("approval_rule_eligible_approvers").
		Prepared(true).
		Rows(goqu.Record{
			"id":                 newResourceID(),
			"approval_rule_id":   approver.RuleID,
			"user_id":            approver.UserID,
			"service_account_id": approver.ServiceAccountID,
			"team_id":            approver.TeamID,
		}).ToSQL()
	if err != nil {
		return err
	}

	_, err = con.Exec(ctx, sql, args...)
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				return errors.New("invalid approver ID", errors.WithErrorCode(errors.EInvalid))
			}
		}
		return err
	}

	return nil
}

func (a *approvalRules) getApprovalRule(ctx context.Context, exp exp.Expression) (*models.ApprovalRule, error) {
	query := dialect.From(goqu.T("approval_rules")).
		Prepared(true).
		Select(a.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"approval_rules.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"approval_rules.project_id": goqu.I("projects.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	rule, err := scanApprovalRule(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	if err := a.hydrateRules(ctx, a.dbClient.getConnection(ctx), []*models.ApprovalRule{rule}); err != nil {
		return nil, err
	}

	return rule, nil
}

func (*approvalRules) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range approvalRuleFieldList {
		selectFields = append(selectFields, fmt.Sprintf("approval_rules.%s", field))
	}

	selectFields = append(selectFields, "organizations.name", "projects.name")

	return selectFields
}

func scanApprovalRule(row scanner) (*models.ApprovalRule, error) {
	var organizationName string
	var projectName sql.NullString

	approvalRule := &models.ApprovalRule{}

	err := row.Scan(
		&approvalRule.Metadata.ID,
		&approvalRule.Metadata.CreationTimestamp,
		&approvalRule.Metadata.LastUpdatedTimestamp,
		&approvalRule.Metadata.Version,
		&approvalRule.CreatedBy,
		&approvalRule.Name,
		&approvalRule.Description,
		&approvalRule.OrgID,
		&approvalRule.ApprovalsRequired,
		&approvalRule.Scope,
		&approvalRule.ProjectID,
		&organizationName,
		&projectName,
	)
	if err != nil {
		return nil, err
	}

	switch approvalRule.Scope {
	case models.OrganizationScope:
		approvalRule.Metadata.PRN = models.ApprovalRuleResource.BuildPRN(organizationName, approvalRule.Name)
	case models.ProjectScope:
		approvalRule.Metadata.PRN = models.ApprovalRuleResource.BuildPRN(organizationName, projectName.String, approvalRule.Name)
	default:
		return nil, fmt.Errorf("unexpected approval rule scope: %s", approvalRule.Scope)
	}

	return approvalRule, nil
}

func scanApprovalRuleEligibleApprover(row scanner) (*approvalRuleEligibleApprover, error) {

	approver := &approvalRuleEligibleApprover{}

	err := row.Scan(
		&approver.RuleID,
		&approver.UserID,
		&approver.ServiceAccountID,
		&approver.TeamID,
	)
	if err != nil {
		return nil, err
	}

	return approver, nil
}
