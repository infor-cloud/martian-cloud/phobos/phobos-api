//go:build integration

package db

import (
	"context"
	"fmt"
	"sort"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// organizationInfo aids convenience in accessing the information TestGetOrganizations needs about the warmup organizations.
type organizationInfo struct {
	name           string
	organizationID string
}

// organizationInfoSlice makes a slice of organizationInfo sortable
type organizationInfoSlice []organizationInfo

func TestGetOrganizationByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOrgs, _, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizations)
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode    errors.CodeType
		name               string
		searchID           string
		expectOrganization bool
	}

	positiveOrg := createdWarmupOrgs[0]

	/*
		test case template

		{
			name               string
			searchID           string
			expectErrorCode    errors.CodeType
			expectOrganization bool
		}
	*/

	testCases := []testCase{
		{
			name:               "positive-" + positiveOrg.Name,
			searchID:           positiveOrg.Metadata.ID,
			expectOrganization: true,
		},
		{
			name:     "negative, non-existent ID",
			searchID: nonExistentID,
			// expect org and error to be nil
		},
		{
			name:               "defective-id",
			searchID:           invalidID,
			expectOrganization: false,
			expectErrorCode:    errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			organization, err := testClient.client.Organizations.GetOrganizationByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectOrganization {
				require.NotNil(t, organization)
				assert.Equal(t, test.searchID, organization.Metadata.ID)
			} else {
				assert.Nil(t, organization)
			}
		})
	}
}

func TestGetOrganizationByName(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOrgs, _, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizations)
	require.Nil(t, err)

	type testCase struct {
		name               string
		searchName         string
		expectOrganization bool
		// No test cases require an expected error code.
	}

	testCases := []testCase{}
	for _, positiveOrganization := range createdWarmupOrgs {
		testCases = append(testCases, testCase{
			name:               "positive-" + positiveOrganization.Name,
			searchName:         positiveOrganization.Name,
			expectOrganization: true,
		})
	}

	testCases = append(testCases,
		testCase{
			name:       "negative, non-existent organization",
			searchName: "non-existent-organization",
			// expect organization and error to be nil
		},
		testCase{
			name:       "defective-name",
			searchName: "this*is*a*not*a*valid*name",
			// expect organization and error to be nil
			// At the DB layer, the search name is just looked up, with no organization returned.
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			organization, err := testClient.client.Organizations.GetOrganizationByName(ctx, test.searchName)
			assert.Nil(t, err)

			if test.expectOrganization {
				require.NotNil(t, organization)
				assert.Equal(t, test.searchName, organization.Name)
			} else {
				assert.Nil(t, organization)
			}
		})
	}
}

func TestGetOrganizationByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOrgs, _, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizations)
	require.Nil(t, err)

	type testCase struct {
		name               string
		searchPRN          string
		expectErrorCode    errors.CodeType
		expectOrganization bool
	}

	positiveOrg := createdWarmupOrgs[0]

	/*
		test case template

		{
			name               string
			searchPRN          string
			expectErrorCode    errors.CodeType
			expectOrganization bool
		}
	*/

	testCases := []testCase{
		{
			name:               "positive-" + positiveOrg.Name,
			searchPRN:          positiveOrg.Metadata.PRN,
			expectOrganization: true,
		},
		{
			name:               "negative, organization does not exist",
			searchPRN:          models.OrganizationResource.BuildPRN("non-existent-org"),
			expectOrganization: false,
			// expect error to be nil
		},
		{
			name:               "defective-prn",
			searchPRN:          "this*is*a*not*a*valid*prn",
			expectOrganization: false,
			expectErrorCode:    errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			organization, err := testClient.client.Organizations.GetOrganizationByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectOrganization {
				require.NotNil(t, organization)
				assert.Equal(t, test.searchPRN, organization.Metadata.PRN)
			} else {
				assert.Nil(t, organization)
			}
		})
	}
}

func TestGetOrganizations(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOrganizations, orgMap, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizations)
	require.Nil(t, err)

	_, projMap, err := createInitialProjects(ctx, testClient, orgMap, standardWarmupProjects)
	require.Nil(t, err)

	createdWarmupUsers, userMap, err := createInitialUsers(ctx, testClient, warmupUsersForSearch)
	require.Nil(t, err)

	_, rolesMap, err := createInitialRoles(ctx, testClient, warmupRolesForSearch)
	require.Nil(t, err)

	createdWarmupSAs, serviceAccountMap, err := createInitialServiceAccounts(ctx, testClient, orgMap, warmupServiceAccountsForSearch)
	require.Nil(t, err)

	_, teamMap, err := createInitialTeams(ctx, testClient, warmupTeamsForSearch)
	require.Nil(t, err)

	_, err = createInitialTeamMembers(ctx, testClient, teamMap, userMap, warmupTeamMembersForSearch)
	require.Nil(t, err)

	_, err = createInitialMemberships(ctx, testClient,
		userMap, teamMap, serviceAccountMap, orgMap, projMap, rolesMap, warmupMembershipsForSearch)
	require.Nil(t, err)

	allOrganizationInfos := organizationInfoFromOrganizations(ctx, testClient.client.getConnection(ctx), createdWarmupOrganizations)

	allNames := namesFromOrganizationInfo(allOrganizationInfos)
	reverseNames := reverseStringSlice(allNames)
	allOrganizationIDs := organizationIDsFromOrganizationInfos(allOrganizationInfos)

	dummyCursorFunc := func(cp pagination.CursorPaginatable) (*string, error) { return ptr.String("dummy-cursor-value"), nil }

	type testCase struct {
		expectPageInfo              pagination.PageInfo
		expectStartCursorError      error
		expectEndCursorError        error
		expectErrorCode             errors.CodeType
		input                       *GetOrganizationsInput
		name                        string
		expectOrganizationNames     []string
		getAfterCursorFromPrevious  bool
		expectHasStartCursor        bool
		getBeforeCursorFromPrevious bool
		expectHasEndCursor          bool
	}

	/*
		test case template

		{
			name                        string
			input                       *GetOrganizationsInput
			getBeforeCursorFromPrevious bool
			getAfterCursorFromPrevious  bool
			expectErrorCode             errors.CodeType
			expectOrganizationNames     []string
			expectPageInfo              pagination.PageInfo
			expectStartCursorError      error
			expectHasStartCursor        bool
			expectEndCursorError        error
			expectHasEndCursor          bool
		}
	*/

	testCases := []testCase{
		{
			name: "non-nil but mostly empty input",
			input: &GetOrganizationsInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			},
			expectOrganizationNames: allNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "populated sort and pagination, nil filter",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
				Filter: nil,
			},
			expectOrganizationNames: allNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "sort in ascending order",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
			},
			expectOrganizationNames: allNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "sort in descending order",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtDesc),
			},
			expectOrganizationNames: reverseNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(reverseNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "pagination: everything at once",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			expectOrganizationNames: allNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: int32(len(allNames)), Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "pagination: first two",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(2),
				},
			},
			expectOrganizationNames: allNames[:2],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: middle two",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(2),
				},
			},
			getAfterCursorFromPrevious: true,
			expectOrganizationNames:    allNames[2:4],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination: final two",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			getAfterCursorFromPrevious: true,
			expectOrganizationNames:    allNames[4:],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		// When Last is supplied, the sort order is intended to be reversed.
		{
			name: "pagination: last three",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					Last: ptr.Int32(3),
				},
			},
			expectOrganizationNames: reverseNames[:3],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(reverseNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "pagination, before and after, expect error",
			input: &GetOrganizationsInput{
				Sort:              ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{},
			},
			getAfterCursorFromPrevious:  true,
			getBeforeCursorFromPrevious: true,
			expectErrorCode:             errors.EInternal,
			expectOrganizationNames:     []string{},
			expectPageInfo:              pagination.PageInfo{},
		},

		{
			name: "pagination, first one and last two, expect error",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
					Last:  ptr.Int32(2),
				},
			},
			expectErrorCode:         errors.EInternal,
			expectOrganizationNames: allNames[5:],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allNames)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, empty slice of organization IDs",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				Filter: &OrganizationFilter{
					OrganizationIDs: []string{},
				},
			},
			expectOrganizationNames: allNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, organization IDs",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				Filter: &OrganizationFilter{
					OrganizationIDs: []string{allOrganizationIDs[0], allOrganizationIDs[2], allOrganizationIDs[4]},
				},
			},
			expectOrganizationNames: []string{allNames[0], allNames[2], allNames[4]},
			expectPageInfo:          pagination.PageInfo{TotalCount: 3, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, organization IDs, non-existent",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				Filter: &OrganizationFilter{
					OrganizationIDs: []string{nonExistentID},
				},
			},
			expectOrganizationNames: []string{},
			expectPageInfo:          pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "filter, organization IDs, invalid",
			input: &GetOrganizationsInput{
				Sort: ptrOrganizationSortableField(OrganizationSortableFieldUpdatedAtAsc),
				Filter: &OrganizationFilter{
					OrganizationIDs: []string{invalidID},
				},
			},
			expectErrorCode:         errors.EInternal,
			expectOrganizationNames: []string{allNames[0], allNames[2], allNames[4]},
			expectPageInfo:          pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, empty string, no other restrictions",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search: ptr.String(""),
				},
			},
			expectOrganizationNames: allNames, // should find all 6 of them
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, organization, no other restrictions",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search: ptr.String("organization"),
				},
			},
			expectOrganizationNames: allNames, // should find all 6 of them
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, organization-1, no other restrictions",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search: ptr.String("organization-1"),
				},
			},
			expectOrganizationNames: allNames[0:1],
			expectPageInfo:          pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, organization, with UserMemberID",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search: ptr.String("organization"),
					// User is only a member in organization-1 but indirectly a team member with a membership in organization-3.
					UserMemberID: &createdWarmupUsers[0].Metadata.ID,
				},
			},
			expectOrganizationNames: []string{allNames[0], allNames[2]},
			expectPageInfo:          pagination.PageInfo{TotalCount: 2, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, organization, with UserMemberID, non-existent",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search:       ptr.String("organization"),
					UserMemberID: ptr.String(nonExistentID),
				},
			},
			expectOrganizationNames: []string{},
			expectPageInfo:          pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},

		{
			name: "search, plain search, organization-3, with UserMemberID, team is only a member in organization-3",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search:       ptr.String("organization-3"),
					UserMemberID: &createdWarmupUsers[0].Metadata.ID,
				},
			},
			expectOrganizationNames: allNames[2:3],
			expectPageInfo:          pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		// A global membership should be able to see all organizations.
		{
			name: "search, plain search, organization, with UserMemberID, user has a global membership",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search:       ptr.String("organization"),
					UserMemberID: &createdWarmupUsers[1].Metadata.ID,
				},
			},
			expectOrganizationNames: allNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, organization, with ServiceAccountMemberID, service account has a global membership",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search:                 ptr.String("organization"),
					ServiceAccountMemberID: &createdWarmupSAs[2].Metadata.ID,
				},
			},
			expectOrganizationNames: allNames,
			expectPageInfo:          pagination.PageInfo{TotalCount: 6, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, organization, with ServiceAccountMemberID",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search:                 ptr.String("organization"),
					ServiceAccountMemberID: &createdWarmupSAs[0].Metadata.ID,
				},
			},
			expectOrganizationNames: []string{allNames[1]},
			expectPageInfo:          pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},

		{
			name: "search, plain search, organization, with ServiceAccountMemberID, service account only has a membership in project-2",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search:                 ptr.String("organization"),
					ServiceAccountMemberID: &createdWarmupSAs[1].Metadata.ID,
				},
			},
			// Project level memberships shouldn't return any organization results.
			expectOrganizationNames: []string{},
			expectPageInfo:          pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},

		{
			name: "search, plain search, organization, with ServiceAccountMemberID, non-existent",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search:                 ptr.String("organization"),
					ServiceAccountMemberID: ptr.String(nonExistentID),
				},
			},
			expectOrganizationNames: []string{},
			expectPageInfo:          pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},

		{
			name: "search, plain search, organization, OrganizationIDs",
			input: &GetOrganizationsInput{
				Filter: &OrganizationFilter{
					Search: ptr.String("organization"),
					OrganizationIDs: []string{
						allOrganizationIDs[1],
						allOrganizationIDs[4],
					},
				},
			},
			expectOrganizationNames: []string{allNames[1], allNames[4]},
			expectPageInfo:          pagination.PageInfo{TotalCount: 2, Cursor: dummyCursorFunc},
			expectHasStartCursor:    true,
			expectHasEndCursor:      true,
		},
	}

	var (
		previousEndCursorValue   *string
		previousStartCursorValue *string
	)
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// For some pagination tests, a previous case's cursor value gets piped into the next case.
			if test.getAfterCursorFromPrevious || test.getBeforeCursorFromPrevious {

				// Make sure there's a place to put it.
				require.NotNil(t, test.input.PaginationOptions)

				if test.getAfterCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousEndCursorValue)
					test.input.PaginationOptions.After = previousEndCursorValue
				}

				if test.getBeforeCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousStartCursorValue)
					test.input.PaginationOptions.Before = previousStartCursorValue
				}

				// Clear the values so they won't be used twice.
				previousEndCursorValue = nil
				previousStartCursorValue = nil
			}

			organizationsResult, err := testClient.client.Organizations.GetOrganizations(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			// If there was no error, check the results.
			if err == nil {

				// Never returns nil if error is nil.
				require.NotNil(t, organizationsResult.PageInfo)
				assert.NotNil(t, organizationsResult.Organizations)
				pageInfo := organizationsResult.PageInfo
				organizations := organizationsResult.Organizations

				// Check the organizations result by comparing a list of the names.
				resultNames := []string{}
				for _, org := range organizations {
					resultNames = append(resultNames, org.Name)
				}

				// If no sort direction was specified, sort the results here for repeatability.
				if test.input.Sort == nil {
					sort.Strings(resultNames)
				}

				assert.Equal(t, len(test.expectOrganizationNames), len(resultNames))
				assert.Equal(t, test.expectOrganizationNames, resultNames)

				assert.Equal(t, test.expectPageInfo.HasNextPage, pageInfo.HasNextPage)
				assert.Equal(t, test.expectPageInfo.HasPreviousPage, pageInfo.HasPreviousPage)
				assert.Equal(t, test.expectPageInfo.TotalCount, pageInfo.TotalCount)
				assert.Equal(t, test.expectPageInfo.Cursor != nil, pageInfo.Cursor != nil)

				// Compare the cursor function results only if there is at least one organization returned.
				// If there are no organizations returned, there is no argument to pass to the cursor function.
				// Also, don't try to reverse engineer to compare the cursor string values.
				if len(organizations) > 0 {
					resultStartCursor, resultStartCursorError := pageInfo.Cursor(&organizations[0])
					resultEndCursor, resultEndCursorError := pageInfo.Cursor(&organizations[len(organizations)-1])
					assert.Equal(t, test.expectStartCursorError, resultStartCursorError)
					assert.Equal(t, test.expectHasStartCursor, resultStartCursor != nil)
					assert.Equal(t, test.expectEndCursorError, resultEndCursorError)
					assert.Equal(t, test.expectHasEndCursor, resultEndCursor != nil)

					// Capture the ending cursor values for the next case.
					previousEndCursorValue = resultEndCursor
					previousStartCursorValue = resultStartCursor
				}
			}
		})
	}
}

func TestCreateOrganization(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	type testCase struct {
		expectMsg      *string
		name           string
		toCreate       []models.Organization
		isPositiveCase bool
	}

	testCases := []testCase{
		{
			name:           "positive, standard warmup organizations",
			isPositiveCase: true,
			toCreate:       standardWarmupOrganizations,
			// expect message to be nil
		},
		{
			name:      "negative, duplicate organization",
			toCreate:  []models.Organization{standardWarmupOrganizations[2]},
			expectMsg: ptr.String("organization with name " + standardWarmupOrganizations[2].Name + " already exists"),
		},
	}

	cumulativeToCreate := []models.Organization{}
	cumulativeClaimedCreated := []models.Organization{}
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			if test.isPositiveCase {

				// Capture any that should be created.
				cumulativeToCreate = append(cumulativeToCreate, test.toCreate...)

				// For a positive case, just use the utility function.
				claimedCreated, _, err := createInitialOrganizations(ctx, testClient, test.toCreate)
				require.Nil(t, err)

				// Capture any new claimed to be created.
				cumulativeClaimedCreated = append(cumulativeClaimedCreated, claimedCreated...)
			} else {
				// For a negative case, do each organization one at a time.
				for _, input := range test.toCreate {
					_, err := testClient.client.Organizations.CreateOrganization(ctx, &input)
					require.NotNil(t, err)
					assert.Equal(t, *test.expectMsg, err.Error())
				}
			}

			// Get the organizations for comparison.
			gotResult, err := testClient.client.Organizations.GetOrganizations(ctx, &GetOrganizationsInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			})
			require.Nil(t, err)
			retrievedOrganizations := gotResult.Organizations

			// Compare lengths.
			assert.Equal(t, len(cumulativeToCreate), len(cumulativeClaimedCreated))
			assert.Equal(t, len(cumulativeToCreate), len(retrievedOrganizations))

			// Compare the contents of the cumulative organizations to create, the organizations reportedly created,
			// and the organizations retrieved.  The three slices aren't guaranteed to be in the same order,
			// so it is necessary to look them up. Name is used for that purpose.
			copyCumulativeClaimedCreated := cumulativeClaimedCreated
			for _, toldToCreate := range cumulativeToCreate {
				toldToCreate.Metadata.PRN = models.OrganizationResource.BuildPRN(toldToCreate.Name)

				var claimedCreated, retrieved models.Organization

				claimedCreated, copyCumulativeClaimedCreated, err = removeMatching(toldToCreate.Name, copyCumulativeClaimedCreated)
				assert.Nil(t, err)
				if err != nil {
					break
				}

				retrieved, retrievedOrganizations, err = removeMatching(toldToCreate.Name, retrievedOrganizations)
				assert.Nil(t, err)
				if err != nil {
					break
				}

				compareOrganizationsCreate(t, toldToCreate, claimedCreated)
				compareOrganizationsCreate(t, toldToCreate, retrieved)
			}

			// Must not have any leftovers.
			assert.Equal(t, 0, len(copyCumulativeClaimedCreated))
			assert.Equal(t, 0, len(retrievedOrganizations))
		})
	}
}

func TestUpdateOrganization(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOrganizations, _, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizations)
	require.Nil(t, err)

	type testCase struct {
		findName         *string
		findOrganization *models.Organization
		newDescription   *string
		expectErrorCode  errors.CodeType
		name             string
		isPositive       bool
	}

	/*
		test case template

		{
			name             string
			isPositive       bool
			findName         *string
			findOrganization *models.Organization
			newDescription   *string
			expectErrorCode  errors.CodeType
		}
	*/

	testCases := []testCase{}
	for _, positiveOrganization := range createdWarmupOrganizations {
		testCases = append(testCases, testCase{
			name:           "positive-" + positiveOrganization.Name,
			findName:       &positiveOrganization.Name,
			newDescription: updateDescription(positiveOrganization.Description),
			isPositive:     true,
		})
	}

	testCases = append(testCases,
		testCase{
			name: "negative, not exist",
			findOrganization: &models.Organization{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: 1,
				},
				Name:        "organization-that-does-not-exist",
				Description: "This is the organization that does not exist.",
				CreatedBy:   "someone",
			},
			newDescription:  ptr.String("Update description for an organization that does not exist."),
			expectErrorCode: errors.EOptimisticLock,
		},
		testCase{
			name: "negative, invalid uuid",
			findOrganization: &models.Organization{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				Name:        "organization-that-does-not-exist",
				Description: "This is the organization that does not exist.",
				CreatedBy:   "someone",
			},
			newDescription:  ptr.String("Update description for an organization that does not exist."),
			expectErrorCode: errors.EInternal,
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			var originalOrganization *models.Organization
			switch {

			case (test.findName != nil) && (test.findOrganization == nil):
				originalOrganization, err = testClient.client.Organizations.GetOrganizationByName(ctx, *test.findName)
				require.Nil(t, err)
				require.NotNil(t, originalOrganization)

			case (test.findName == nil) && (test.findOrganization != nil):
				originalOrganization = test.findOrganization

			default:
				assert.Equal(t, "test case should use exactly one of findName, findOrganization",
					"test case violated that rule")
				// No point in going forward with this test case.
				return
			}

			expectUpdatedDescription := updateDescription(originalOrganization.Description)
			copyOriginalOrganization := originalOrganization
			copyOriginalOrganization.Description = *expectUpdatedDescription

			claimedUpdatedOrganization, err := testClient.client.Organizations.UpdateOrganization(ctx, copyOriginalOrganization)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.isPositive {

				require.NotNil(t, claimedUpdatedOrganization)

				compareOrganizationsUpdate(t, *expectUpdatedDescription, *originalOrganization, *claimedUpdatedOrganization)

				retrieved, err := testClient.client.Organizations.GetOrganizationByName(ctx, originalOrganization.Name)
				require.Nil(t, err)

				require.NotNil(t, retrieved)

				compareOrganizationsUpdate(t, *expectUpdatedDescription, *originalOrganization, *retrieved)
			}
		})
	}
}

func TestDeleteOrganization(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	initialWarmupOrganizations, _, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizations)
	require.Nil(t, err)

	allNames := []string{}
	for _, org := range initialWarmupOrganizations {
		allNames = append(allNames, org.Name)
	}

	// Must close manually, because more test clients will be created inside the loop.
	testClient.close(ctx)

	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		orgNames         []string
		ids              []string
		expectFinalCount int
	}

	// Please note: An attempt to delete an organization at the DB layer produces a message that the
	// "resource version does not match specified version".  However, that is not a bug, because
	// the GraphQL resolver does a GetOrganizationByName before making the actual attempt to delete
	// the organization, and that GetOrganizationByName will allow the GraphQL resolver to return an error
	// indicating that the organization does not exist.

	/*
		test case template

		{
			name             string
			orgNames         []string
			ids              []string
			expectErrorCode  errors.CodeType
			expectFinalCount int
		}
	*/

	testCases := []testCase{
		{
			name:             "positive",
			orgNames:         allNames,
			expectFinalCount: 0,
		},
		{
			name:             "negative, non-existent ID",
			ids:              []string{nonExistentID},
			expectErrorCode:  errors.EOptimisticLock,
			expectFinalCount: 6,
		},
		{
			name:             "negative, invalid ID",
			ids:              []string{invalidID},
			expectErrorCode:  errors.EInternal,
			expectFinalCount: 6,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// Because deleting a organization is destructive, this test must create/delete
			// the warmup organizations inside the loop.
			// Creating the new client deletes the organizations from the previous run.
			testClient := newTestClient(ctx, t)
			// Must close manually, because later test cases will create their own test clients.

			createdWarmupOrganizations, _, err := createInitialOrganizations(ctx, testClient, standardWarmupOrganizations)
			require.Nil(t, err)

			organizationsToDelete := []*models.Organization{}
			switch {
			case test.orgNames != nil:
				for _, name := range test.orgNames {
					// Must find the organization object in the current generation of warmup organizations.
					// Using the initial warmup organizations would cause metadata version mismatches.
					var foundOrganization *models.Organization
					for _, candidate := range createdWarmupOrganizations {
						if candidate.Name == name {
							foundOrganization = &candidate
							break
						}
					}
					require.NotNil(t, foundOrganization)
					organizationsToDelete = append(organizationsToDelete, foundOrganization)
				}
			case test.ids != nil:
				for _, id := range test.ids {
					organizationToDelete := models.Organization{
						Metadata: models.ResourceMetadata{
							ID:      id,
							Version: 1,
						},
					}
					organizationsToDelete = append(organizationsToDelete, &organizationToDelete)
				}
			default:
				assert.Nil(t, "each test case must have either orgNames or ids but not both")
				return
			}

			// Now, try to delete the organizations in sequence.
			for _, organizationToDelete := range organizationsToDelete {
				err = testClient.client.Organizations.DeleteOrganization(ctx, organizationToDelete)
				if test.expectErrorCode != "" {
					assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
					return
				}
			}

			// Check that the final count is right.
			stillAround, err := testClient.client.Organizations.GetOrganizations(ctx, &GetOrganizationsInput{})
			assert.Nil(t, err)
			assert.Equal(t, test.expectFinalCount, len(stillAround.Organizations))

			// Must close manually, because later test cases will create their own test clients.
			testClient.close(ctx)
		})
	}
}

// standardWarmupOrganizations for tests in this module.
var standardWarmupOrganizations = []models.Organization{
	{
		Description: "organization-1 for testing organization functions",
		Name:        "organization-1",
		CreatedBy:   "someone",
	},
	{
		Description: "organization-2 for testing organization functions",
		Name:        "organization-2",
		CreatedBy:   "someone",
	},
	{
		Description: "organization-3 for testing organization functions",
		Name:        "organization-3",
		CreatedBy:   "someone",
	},
	{
		Description: "organization-4 for testing organization functions",
		Name:        "organization-4",
		CreatedBy:   "someone",
	},
	{
		Description: "organization-5 for testing organization functions",
		Name:        "organization-5",
		CreatedBy:   "someone",
	},
	{
		Description: "organization-6 for testing organization functions",
		Name:        "organization-6",
		CreatedBy:   "someone",
	},
}

// Warmup projects for GetOrganizations search.
var warmupProjectsForSearch = []models.Project{
	{
		Name:        "project-1",
		OrgID:       "organization-1",
		Description: "project-1 for testing organization functions",
		CreatedBy:   "someone",
	},
	{
		Name:        "project-2",
		OrgID:       "organization-2",
		Description: "project-2 for testing organization functions",
		CreatedBy:   "someone",
	},
}

// Warmup users for GetOrganizations search.
var warmupUsersForSearch = []models.User{
	{
		Username: "plain-user",
		Email:    "plain-user@invalid.example",
	},
	{
		Username: "global-user",
		Email:    "global-user@invalid.example",
	},
}

// A role is a prerequisite for the organization memberships.
var warmupRolesForSearch = []models.Role{
	{
		Name:        "role-a",
		Description: "role a for organization membership tests",
		CreatedBy:   "someone-a",
	},
}

// warmupServiceAccountsForSearch for testing in this module.
var warmupServiceAccountsForSearch = []models.ServiceAccount{
	{
		Name:              "service-account-a",
		Description:       "service account a for organization membership tests",
		OrganizationID:    &standardWarmupOrganizations[1].Name,
		Scope:             models.OrganizationScope,
		CreatedBy:         "someone-a",
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
	{
		Name:              "service-account-b",
		Description:       "service account b for organization membership tests",
		OrganizationID:    &standardWarmupOrganizations[1].Name,
		Scope:             models.OrganizationScope,
		CreatedBy:         "someone-b",
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
	{
		Name:              "global-service-account",
		Description:       "global service account for organization membership tests",
		Scope:             models.GlobalScope,
		CreatedBy:         "someone-c",
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
}

// warmupTeamsForSearch for testing in this module.
var warmupTeamsForSearch = []models.Team{
	{
		Name:        "team-a",
		Description: "team a for organization membership tests",
	},
}

// warmupTeamMembersForSearch for testing in this module.
var warmupTeamMembersForSearch = []models.TeamMember{
	{
		TeamID: "team-a",
		UserID: warmupUsersForSearch[0].Username,
	},
}

// warmupMembershipsForSearch for testing memberships in this module.
var warmupMembershipsForSearch = []models.Membership{
	{
		UserID:         &warmupUsersForSearch[0].Username,
		RoleID:         warmupRolesForSearch[0].Name,
		OrganizationID: &standardWarmupOrganizations[0].Name,
		Scope:          models.OrganizationScope,
	},
	{
		UserID: &warmupUsersForSearch[1].Username,
		RoleID: warmupRolesForSearch[0].Name,
		Scope:  models.GlobalScope,
	},
	{
		ServiceAccountID: &warmupServiceAccountsForSearch[0].Name,
		RoleID:           warmupRolesForSearch[0].Name,
		OrganizationID:   &standardWarmupOrganizations[1].Name,
		Scope:            models.OrganizationScope,
	},
	{
		ServiceAccountID: &warmupServiceAccountsForSearch[1].Name,
		RoleID:           warmupRolesForSearch[0].Name,
		ProjectID:        &warmupProjectsForSearch[1].Name,
		Scope:            models.ProjectScope,
	},
	{
		ServiceAccountID: &warmupServiceAccountsForSearch[2].Name,
		RoleID:           warmupRolesForSearch[0].Name,
		Scope:            models.GlobalScope,
	},
	{
		TeamID:         &warmupTeamsForSearch[0].Name,
		RoleID:         warmupRolesForSearch[0].Name,
		OrganizationID: &standardWarmupOrganizations[2].Name,
		Scope:          models.OrganizationScope,
	},
}

// createInitialOrganizations creates some Organizations for a test.
func createInitialOrganizations(ctx context.Context, testClient *testClient,
	toCreate []models.Organization) ([]models.Organization, map[string]string, error) {
	result := []models.Organization{}
	name2ID := make(map[string]string)

	for _, org := range toCreate {
		created, err := testClient.client.Organizations.CreateOrganization(ctx, &org)
		if err != nil {
			return nil, nil, err
		}

		result = append(result, *created)
		name2ID[created.Name] = created.Metadata.ID
	}

	return result, name2ID, nil
}

func ptrOrganizationSortableField(arg OrganizationSortableField) *OrganizationSortableField {
	return &arg
}

func (ois organizationInfoSlice) Len() int {
	return len(ois)
}

func (ois organizationInfoSlice) Swap(i, j int) {
	ois[i], ois[j] = ois[j], ois[i]
}

func (ois organizationInfoSlice) Less(i, j int) bool {
	return ois[i].name < ois[j].name
}

// organizationInfoFromOrganizations returns a slice of organizationInfo, sorted by name.
func organizationInfoFromOrganizations(ctx context.Context, conn connection, organizations []models.Organization) []organizationInfo {
	result := []organizationInfo{}
	for _, organization := range organizations {
		result = append(result, organizationInfo{
			name:           organization.Name,
			organizationID: organization.Metadata.ID,
		})
	}

	sort.Sort(organizationInfoSlice(result))

	return result
}

// namesFromOrganizationInfo preserves order.
func namesFromOrganizationInfo(organizationInfos []organizationInfo) []string {
	result := []string{}
	for _, organizationInfo := range organizationInfos {
		result = append(result, organizationInfo.name)
	}
	return result
}

// organizationIDsFromOrganizationInfos preserves order
func organizationIDsFromOrganizationInfos(organizationInfos []organizationInfo) []string {
	result := []string{}
	for _, organizationInfo := range organizationInfos {
		result = append(result, organizationInfo.organizationID)
	}
	return result
}

// removeMatching finds an organization in a slice with a specified name.
// It returns the found organization, a shortened slice, and an error.
func removeMatching(lookFor string, oldSlice []models.Organization) (models.Organization, []models.Organization, error) {
	found := models.Organization{}
	foundOne := false
	newSlice := []models.Organization{}

	for _, candidate := range oldSlice {
		if candidate.Name == lookFor {
			found = candidate
			foundOne = true
		} else {
			newSlice = append(newSlice, candidate)
		}
	}

	if !foundOne {
		return models.Organization{}, nil, fmt.Errorf("Failed to find organization with name: %s", lookFor)
	}

	return found, newSlice, nil
}

// compareOrganizationsCreate compares two organizations for TestCreateOrganization
// There are some fields that cannot be compared, or DeepEqual would work.
func compareOrganizationsCreate(t *testing.T, expected, actual models.Organization) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.Description, actual.Description)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
	assert.Equal(t, expected.Metadata.PRN, actual.Metadata.PRN)
}

// updateDescription takes an original description and returns a modified version for TestUpdateOrganization
func updateDescription(input string) *string {
	return ptr.String(fmt.Sprintf("Updated description: %s", input))
}

// compareOrganizationsUpdate compares two organizations for TestUpdateOrganization
func compareOrganizationsUpdate(t *testing.T, expectedDescription string, expected, actual models.Organization) {
	assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	assert.Equal(t, expected.Metadata.CreationTimestamp, actual.Metadata.CreationTimestamp)
	assert.NotEqual(t, expected.Metadata.Version, actual.Metadata.Version)
	assert.NotEqual(t, expected.Metadata.LastUpdatedTimestamp, actual.Metadata.LastUpdatedTimestamp)

	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expectedDescription, actual.Description)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
}
