//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// getValue implements the sortableField interface for testing.
func (sf PluginVersionSortableField) getValue() string {
	return string(sf)
}

func TestGetPluginVersionByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode     errors.CodeType
		name                string
		id                  string
		expectPluginVersion bool
	}

	testCases := []testCase{
		{
			name:                "get resource by id",
			id:                  pluginVersion.Metadata.ID,
			expectPluginVersion: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPluginVersion, err := testClient.client.PluginVersions.GetPluginVersionByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPluginVersion {
				require.NotNil(t, actualPluginVersion)
				assert.Equal(t, test.id, actualPluginVersion.Metadata.ID)
			} else {
				assert.Nil(t, actualPluginVersion)
			}
		})
	}
}

func TestGetPluginVersionByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode     errors.CodeType
		name                string
		prn                 string
		expectPluginVersion bool
	}

	testCases := []testCase{
		{
			name:                "get resource by PRN",
			prn:                 pluginVersion.Metadata.PRN,
			expectPluginVersion: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.PluginVersionResource.BuildPRN(organization.Name, plugin.Name, "2.0.0"),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPluginVersion, err := testClient.client.PluginVersions.GetPluginVersionByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPluginVersion {
				require.NotNil(t, actualPluginVersion)
				assert.Equal(t, test.prn, actualPluginVersion.Metadata.PRN)
			} else {
				assert.Nil(t, actualPluginVersion)
			}
		})
	}
}

func TestGetPluginVersions(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	plugin2, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "github",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	versions := make([]*models.PluginVersion, 10)
	for i := 0; i < len(versions); i++ {
		version, eErr := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
			SemanticVersion: fmt.Sprintf("1.0.%d", i),
			PluginID:        plugin.Metadata.ID,
		})
		require.NoError(t, eErr)

		versions[i] = version
	}

	pluginVersion, eErr := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "2.0.0",
		PluginID:        plugin2.Metadata.ID,
		SHASumsUploaded: true,
		Latest:          true,
	})
	require.NoError(t, eErr)

	versions = append(versions, pluginVersion)

	type testCase struct {
		filter            *PluginVersionFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all resources",
			filter:            &PluginVersionFilter{},
			expectResultCount: len(versions),
		},
		{
			name: "get resources by plugin id",
			filter: &PluginVersionFilter{
				PluginID: &plugin.Metadata.ID,
			},
			expectResultCount: len(versions) - 1,
		},
		{
			name: "get resources by plugin id and semantic version",
			filter: &PluginVersionFilter{
				PluginID:        &plugin.Metadata.ID,
				SemanticVersion: &versions[0].SemanticVersion,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by semantic version",
			filter: &PluginVersionFilter{
				SemanticVersion: &versions[0].SemanticVersion,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by sha sums uploaded",
			filter: &PluginVersionFilter{
				SHASumsUploaded: &pluginVersion.SHASumsUploaded,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by latest",
			filter: &PluginVersionFilter{
				Latest: &pluginVersion.Latest,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by plugin version ids",
			filter: &PluginVersionFilter{
				PluginVersionIDs: []string{versions[0].Metadata.ID, versions[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.PluginVersions.GetPluginVersions(ctx, &GetPluginVersionsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, result)
			assert.Len(t, result.PluginVersions, test.expectResultCount)
		})
	}
}

func TestGetPluginVersionsWithSortingAndPagination(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, eErr := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
			SemanticVersion: fmt.Sprintf("1.0.%d", i),
			PluginID:        plugin.Metadata.ID,
		})
		require.NoError(t, eErr)
	}

	sortableFields := []sortableField{
		PluginVersionSortableFieldUpdatedAtAsc,
		PluginVersionSortableFieldUpdatedAtDesc,
		PluginVersionSortableFieldCreatedAtAsc,
		PluginVersionSortableFieldCreatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := PluginVersionSortableField(sortByField.getValue())

		result, err := testClient.client.PluginVersions.GetPluginVersions(ctx, &GetPluginVersionsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.PluginVersions {
			resources = append(resources, &result.PluginVersions[ix])
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreatePluginVersion(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		pluginID        string
	}

	testCases := []testCase{
		{
			name:     "create resource",
			pluginID: plugin.Metadata.ID,
		},
		{
			name:            "create resource with non-existent plugin id will return an error",
			pluginID:        nonExistentID,
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "duplicate resource will return an error",
			pluginID:        plugin.Metadata.ID,
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
				SemanticVersion: "1.0.0",
				PluginID:        test.pluginID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, pluginVersion)
		})
	}
}

func TestUpdatePluginVersion(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		version         int
	}

	testCases := []testCase{
		{
			name:    "update resource",
			version: pluginVersion.Metadata.Version,
		},
		{
			name:            "update resource with invalid version will return an error",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPluginVersion, err := testClient.client.PluginVersions.UpdatePluginVersion(ctx, &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID:      pluginVersion.Metadata.ID,
					Version: pluginVersion.Metadata.Version,
				},
				Protocols:       []string{"2.0"},
				SHASumsUploaded: true,
				ReadmeUploaded:  true,
				Latest:          true,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, actualPluginVersion)
			assert.Len(t, actualPluginVersion.Protocols, 1)
			assert.True(t, actualPluginVersion.SHASumsUploaded)
			assert.True(t, actualPluginVersion.ReadmeUploaded)
			assert.True(t, actualPluginVersion.Latest)
		})
	}
}

func TestDeletePluginVersion(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "delete resource",
			version: pluginVersion.Metadata.Version,
		},
		{
			name:            "delete resource with invalid version will return an error",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.PluginVersions.DeletePluginVersion(ctx, &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID:      pluginVersion.Metadata.ID,
					Version: pluginVersion.Metadata.Version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
