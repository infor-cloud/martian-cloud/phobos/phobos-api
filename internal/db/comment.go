package db

//go:generate go tool mockery --name Comments --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Comments encapsulates the logic to access Comments from the database
type Comments interface {
	GetCommentByID(ctx context.Context, id string) (*models.Comment, error)
	GetCommentByPRN(ctx context.Context, prn string) (*models.Comment, error)
	GetComments(ctx context.Context, input *GetCommentsInput) (*CommentsResult, error)
	CreateComment(ctx context.Context, comment *models.Comment) (*models.Comment, error)
	UpdateComment(ctx context.Context, comment *models.Comment) (*models.Comment, error)
	DeleteComment(ctx context.Context, comment *models.Comment) error
}

// CommentFilter contains the supported fields for filtering Comment resources
type CommentFilter struct {
	ThreadID   *string
	Search     *string
	CommentIDs []string
}

// CommentSortableField represents the fields that a comment can be sorted by
type CommentSortableField string

// CommentSortableField constants
const (
	CommentSortableFieldCreatedAtAsc  CommentSortableField = "CREATED_AT_ASC"
	CommentSortableFieldCreatedAtDesc CommentSortableField = "CREATED_AT_DESC"
	CommentSortableFieldUpdatedAtAsc  CommentSortableField = "UPDATED_AT_ASC"
	CommentSortableFieldUpdatedAtDesc CommentSortableField = "UPDATED_AT_DESC"
)

func (sf CommentSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case CommentSortableFieldCreatedAtAsc, CommentSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "comments", Col: "created_at"}
	case CommentSortableFieldUpdatedAtAsc, CommentSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "comments", Col: "updated_at"}
	default:
		return nil
	}
}

func (sf CommentSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetCommentsInput is the input for listing comments
type GetCommentsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *CommentSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *CommentFilter
}

// validate validates the input for GetCommentsInput
func (i *GetCommentsInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// CommentsResult contains the response data and page information
type CommentsResult struct {
	PageInfo *pagination.PageInfo
	Comments []models.Comment
}

var commentFieldList = append(metadataFieldList,
	"thread_id",
	"text",
	"user_id",
	"service_account_id",
)

type comments struct {
	dbClient *Client
}

// NewComments returns an instance of the Comments interface
func NewComments(dbClient *Client) Comments {
	return &comments{dbClient: dbClient}
}

func (c *comments) GetCommentByID(ctx context.Context, id string) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "db.GetCommentByID")
	defer span.End()

	return c.getComment(ctx, goqu.Ex{"comments.id": id})
}

func (c *comments) GetCommentByPRN(ctx context.Context, prn string) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "db.GetCommentByPRN")
	defer span.End()

	// prn:comment:<commentId>
	path, err := models.CommentResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return c.getComment(ctx, goqu.Ex{"comments.id": gid.FromGlobalID(path)})
}

func (c *comments) GetComments(ctx context.Context, input *GetCommentsInput) (*CommentsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetComments")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.ThreadID != nil {
			ex = ex.Append(goqu.I("comments.thread_id").Eq(*input.Filter.ThreadID))
		}

		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("comments.text").ILike("%" + *input.Filter.Search + "%"))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.CommentIDs) > 0 {
			ex = ex.Append(goqu.I("comments.id").In(input.Filter.CommentIDs))
		}
	}

	query := dialect.From(goqu.T("comments")).
		Select(c.getSelectFields()...).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "comments", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, c.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Comment{}
	for rows.Next() {
		item, err := scanComment(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &CommentsResult{
		PageInfo: rows.GetPageInfo(),
		Comments: results,
	}

	return result, nil
}

func (c *comments) CreateComment(ctx context.Context, comment *models.Comment) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "db.CreateComment")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Insert("comments").
		Prepared(true).
		Rows(
			goqu.Record{
				"id":                 newResourceID(),
				"version":            initialResourceVersion,
				"created_at":         timestamp,
				"updated_at":         timestamp,
				"thread_id":          comment.ThreadID,
				"text":               comment.Text,
				"user_id":            comment.UserID,
				"service_account_id": comment.ServiceAccountID,
			}).Returning(commentFieldList...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdComment, err := scanComment(c.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			// There is no unique violation, because comments are not guaranteed to be unique.
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_comment_user_id":
					return nil, errors.New("user does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_comment_service_account_id":
					return nil, errors.New("service account does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_comment_thread_id":
					return nil, errors.New("thread does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdComment, nil
}

func (c *comments) UpdateComment(ctx context.Context, comment *models.Comment) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateComment")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Update("comments").
		Prepared(true).
		Set(goqu.Record{
			"version":    goqu.L("? + ?", goqu.C("version"), 1),
			"updated_at": timestamp,
			"text":       comment.Text,
		}).
		Where(goqu.Ex{"id": comment.Metadata.ID, "version": comment.Metadata.Version}).
		Returning(c.getSelectFields()...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedComment, err := scanComment(c.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedComment, nil
}

func (c *comments) DeleteComment(ctx context.Context, comment *models.Comment) error {
	ctx, span := tracer.Start(ctx, "db.DeleteComment")
	defer span.End()

	sql, args, err := dialect.Delete("comments").
		Prepared(true).
		Where(goqu.Ex{"id": comment.Metadata.ID, "version": comment.Metadata.Version}).
		Returning(c.getSelectFields()...).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanComment(c.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (c *comments) getComment(ctx context.Context, exp exp.Expression) (*models.Comment, error) {
	query := dialect.From(goqu.T("comments")).
		Prepared(true).
		Select(c.getSelectFields()...).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	comment, err := scanComment(c.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return comment, nil
}

func (*comments) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range commentFieldList {
		selectFields = append(selectFields, fmt.Sprintf("comments.%s", field))
	}

	return selectFields
}

func scanComment(row scanner) (*models.Comment, error) {
	comment := &models.Comment{}

	fields := []interface{}{
		&comment.Metadata.ID,
		&comment.Metadata.CreationTimestamp,
		&comment.Metadata.LastUpdatedTimestamp,
		&comment.Metadata.Version,
		&comment.ThreadID,
		&comment.Text,
		&comment.UserID,
		&comment.ServiceAccountID,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	// prn:comment:<commentId>
	comment.Metadata.PRN = models.CommentResource.BuildPRN(gid.ToGlobalID(gid.CommentType, comment.Metadata.ID))

	return comment, nil
}
