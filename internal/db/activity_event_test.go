//go:build integration

package db

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// getValue returns the value of the sortable field.
func (sf ActivityEventSortableField) getValue() string {
	return string(sf)
}

func TestGetActivityEventByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Email:    "someone@domain.com",
		Username: "someone",
	})
	require.NoError(t, err)

	activityEvent, err := testClient.client.ActivityEvents.CreateActivityEvent(ctx, &models.ActivityEvent{
		OrganizationID: &organization.Metadata.ID,
		UserID:         &user.Metadata.ID,
		Action:         models.ActionUpdate,
		TargetType:     models.TargetOrganization,
		TargetID:       &organization.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode     errors.CodeType
		name                string
		id                  string
		expectActivityEvent bool
	}

	testCases := []testCase{
		{
			name:                "get resource by id",
			id:                  activityEvent.Metadata.ID,
			expectActivityEvent: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			activityEvent, err := testClient.client.ActivityEvents.GetActivityEventByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectActivityEvent {
				require.NotNil(t, activityEvent)
				assert.Equal(t, test.id, activityEvent.Metadata.ID)
			} else {
				assert.Nil(t, activityEvent)
			}
		})
	}
}

func TestGetActivityEvents(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	role, err := testClient.client.Roles.CreateRole(ctx, &models.Role{
		Name: "owner",
	})
	require.NoError(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	sampleUsers := []models.User{
		{
			Email:    "org-user@domain.com",
			Username: "org-user",
		},
		{
			Email:    "project-user@domain.com",
			Username: "project-user",
		},
		{
			Email:    "global-user@domain.com",
			Username: "global-user",
		},
	}

	users := make([]*models.User, 0, len(sampleUsers))
	for _, user := range sampleUsers {
		u, cErr := testClient.client.Users.CreateUser(ctx, &user)
		require.NoError(t, cErr)
		users = append(users, u)
	}

	sampleMemberships := []models.Membership{
		{
			UserID:         &users[0].Metadata.ID,
			RoleID:         role.Metadata.ID,
			OrganizationID: &organization.Metadata.ID,
			Scope:          models.OrganizationScope,
		},
		{
			UserID:    &users[1].Metadata.ID,
			RoleID:    role.Metadata.ID,
			ProjectID: &project.Metadata.ID,
			Scope:     models.ProjectScope,
		},
		{
			UserID: &users[2].Metadata.ID,
			RoleID: role.Metadata.ID,
			Scope:  models.GlobalScope,
		},
	}

	for _, membership := range sampleMemberships {
		_, err = testClient.client.Memberships.CreateMembership(ctx, &membership)
		require.NoError(t, err)
	}

	sampleActivityEvents := []models.ActivityEvent{
		{
			UserID:         &users[0].Metadata.ID,
			OrganizationID: &organization.Metadata.ID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetOrganization,
			TargetID:       &organization.Metadata.ID,
		},
		{
			UserID:     &users[0].Metadata.ID,
			ProjectID:  &project.Metadata.ID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetProject,
			TargetID:   &project.Metadata.ID,
		},
		{
			UserID:         &users[1].Metadata.ID,
			OrganizationID: &organization.Metadata.ID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetOrganization,
			TargetID:       &organization.Metadata.ID,
		},
		{
			UserID:     &users[1].Metadata.ID,
			ProjectID:  &project.Metadata.ID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetProject,
			TargetID:   &project.Metadata.ID,
		},
		{
			UserID:         &users[2].Metadata.ID,
			OrganizationID: &organization.Metadata.ID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetOrganization,
			TargetID:       &organization.Metadata.ID,
		},
		{
			UserID:     &users[2].Metadata.ID,
			ProjectID:  &project.Metadata.ID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetProject,
			TargetID:   &project.Metadata.ID,
		},
		{
			UserID:     &users[0].Metadata.ID,
			ProjectID:  &project.Metadata.ID,
			Action:     models.ActionCreate,
			TargetType: models.TargetRelease,
			TargetID:   &release.Metadata.ID,
		},
	}

	activityEvents := make([]*models.ActivityEvent, 0, len(sampleActivityEvents))
	for _, activityEvent := range sampleActivityEvents {
		a, cErr := testClient.client.ActivityEvents.CreateActivityEvent(ctx, &activityEvent)
		require.NoError(t, cErr)
		activityEvents = append(activityEvents, a)
	}

	type testCase struct {
		filter            *ActivityEventFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name: "filter by organization",
			filter: &ActivityEventFilter{
				OrganizationID: &organization.Metadata.ID,
			},
			// should be able to see all activity events for the organization
			expectResultCount: len(activityEvents),
		},
		{
			name: "filter by project",
			filter: &ActivityEventFilter{
				ProjectID: &project.Metadata.ID,
			},
			expectResultCount: 4,
		},
		{
			name: "filter by user",
			filter: &ActivityEventFilter{
				UserID: &users[0].Metadata.ID,
			},
			expectResultCount: 3,
		},
		{
			name: "filter by user and organization",
			filter: &ActivityEventFilter{
				MembershipRequirement: &ActivityEventMembershipRequirement{
					UserID: &users[0].Metadata.ID,
				},
				UserID:         &users[0].Metadata.ID,
				OrganizationID: &organization.Metadata.ID,
			},
			expectResultCount: 3,
		},
		{
			name: "user is only a member of the organization",
			filter: &ActivityEventFilter{
				MembershipRequirement: &ActivityEventMembershipRequirement{
					UserID: &users[0].Metadata.ID,
				},
				UserID:    &users[0].Metadata.ID,
				ProjectID: &project.Metadata.ID,
			},
			// should still be able to see the activity event for the project
			expectResultCount: 2,
		},
		{
			name: "user with global membership queries for organization activity events",
			filter: &ActivityEventFilter{
				MembershipRequirement: &ActivityEventMembershipRequirement{
					UserID: &users[2].Metadata.ID,
				},
				UserID:         &users[2].Metadata.ID,
				OrganizationID: &organization.Metadata.ID,
			},
			// should still be able to see all their activity events for the organization
			expectResultCount: 2,
		},
		{
			name: "user with global membership queries for project activity events",
			filter: &ActivityEventFilter{
				MembershipRequirement: &ActivityEventMembershipRequirement{
					UserID: &users[2].Metadata.ID,
				},
				UserID:    &users[2].Metadata.ID,
				ProjectID: &project.Metadata.ID,
			},
			// should still be able to see the activity event for the project
			expectResultCount: 1,
		},
		{
			name: "filter by user and project",
			filter: &ActivityEventFilter{
				MembershipRequirement: &ActivityEventMembershipRequirement{
					UserID: &users[1].Metadata.ID,
				},
				UserID:    &users[1].Metadata.ID,
				ProjectID: &project.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "user cannot see organization activity events since they are not a member of the organization",
			filter: &ActivityEventFilter{
				MembershipRequirement: &ActivityEventMembershipRequirement{
					UserID: &users[1].Metadata.ID,
				},
				UserID:         &users[1].Metadata.ID,
				OrganizationID: &organization.Metadata.ID,
			},
			// user can only see activity events for the project
			expectResultCount: 1,
		},
		{
			name: "filter by time range start",
			filter: &ActivityEventFilter{
				TimeRangeStart: activityEvents[0].Metadata.CreationTimestamp,
			},
			expectResultCount: len(activityEvents),
		},
		{
			name: "filter by time range end",
			filter: &ActivityEventFilter{
				TimeRangeEnd: activityEvents[len(activityEvents)-1].Metadata.CreationTimestamp,
			},
			expectResultCount: len(activityEvents),
		},
		{
			name: "filter by time range start and end",
			filter: &ActivityEventFilter{
				TimeRangeStart: activityEvents[0].Metadata.CreationTimestamp,
				TimeRangeEnd:   activityEvents[len(activityEvents)-1].Metadata.CreationTimestamp,
			},
			expectResultCount: len(activityEvents),
		},
		{
			name: "filter by create action",
			filter: &ActivityEventFilter{
				Actions: []models.ActivityEventAction{models.ActionCreate},
			},
			expectResultCount: 1,
		},
		{
			name: "filter by create and update actions",
			filter: &ActivityEventFilter{
				Actions: []models.ActivityEventAction{models.ActionCreate, models.ActionUpdate},
			},
			expectResultCount: len(activityEvents),
		},
		{
			name: "filter by organization target type",
			filter: &ActivityEventFilter{
				TargetTypes: []models.ActivityEventTargetType{models.TargetOrganization},
			},
			expectResultCount: 3,
		},
		{
			name: "filter by organization and project target types",
			filter: &ActivityEventFilter{
				TargetTypes: []models.ActivityEventTargetType{models.TargetOrganization, models.TargetProject},
			},
			expectResultCount: len(activityEvents) - 1,
		},
		{
			name: "filter by release target id",
			filter: &ActivityEventFilter{
				ReleaseTargetID: &release.Metadata.ID,
			},
			expectResultCount: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.ActivityEvents.GetActivityEvents(ctx, &GetActivityEventsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			assert.Len(t, result.ActivityEvents, test.expectResultCount)
		})
	}
}

func TestGetActivityEventsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test",
	})
	require.NoError(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Email:    "someone@domain.com",
		Username: "someone",
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.ActivityEvents.CreateActivityEvent(ctx, &models.ActivityEvent{
			OrganizationID: &organization.Metadata.ID,
			UserID:         &user.Metadata.ID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetOrganization,
			TargetID:       &organization.Metadata.ID,
		})
		require.NoError(t, err)
	}

	sortableFields := []sortableField{
		ActivityEventSortableFieldActionAsc,
		ActivityEventSortableFieldActionDesc,
		ActivityEventSortableFieldCreatedAtAsc,
		ActivityEventSortableFieldCreatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := ActivityEventSortableField(sortByField.getValue())

		result, err := testClient.client.ActivityEvents.GetActivityEvents(ctx, &GetActivityEventsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.ActivityEvents {
			resources = append(resources, &result.ActivityEvents[ix])
		}

		return result.PageInfo, resources, nil
	})
}
