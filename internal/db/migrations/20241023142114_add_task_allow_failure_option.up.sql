ALTER TABLE pipeline_nodes
    ADD COLUMN IF NOT EXISTS task_node_on_error VARCHAR DEFAULT 'FAIL';

ALTER TABLE pipeline_nodes
    ADD COLUMN IF NOT EXISTS nested_pipeline_node_on_error VARCHAR DEFAULT 'FAIL';
