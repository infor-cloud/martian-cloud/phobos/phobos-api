ALTER TABLE pipeline_approvals ADD COLUMN IF NOT EXISTS approval_rule_ids JSONB DEFAULT '[]';
