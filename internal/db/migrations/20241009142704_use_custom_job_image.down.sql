ALTER TABLE jobs
    DROP COLUMN IF EXISTS image;

ALTER TABLE pipeline_nodes
    DROP COLUMN IF EXISTS task_node_image;
