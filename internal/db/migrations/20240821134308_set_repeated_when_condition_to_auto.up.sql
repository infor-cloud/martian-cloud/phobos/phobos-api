UPDATE pipeline_nodes
SET task_node_max_attempts = 1
WHERE task_node_max_attempts is NULL;

ALTER TABLE pipeline_nodes
ALTER COLUMN task_node_max_attempts
SET NOT NULL;

UPDATE pipeline_nodes
SET task_node_when = 'auto'
WHERE task_node_when = 'repeated';

ALTER TABLE pipeline_nodes
ADD COLUMN IF NOT EXISTS task_node_attempt_count INTEGER DEFAULT 0;

UPDATE pipeline_nodes
SET task_node_attempt_count = 1
WHERE task_node_latest_job_id IS NOT NULL;

ALTER TABLE pipeline_nodes
ADD COLUMN task_node_last_attempt_finished_time TIMESTAMP;
