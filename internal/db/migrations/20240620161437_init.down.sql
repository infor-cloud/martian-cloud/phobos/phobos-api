DROP TRIGGER comments_notify_event ON comments;
DROP TRIGGER agents_notify_event ON agents;
DROP TRIGGER log_streams_notify_event ON log_streams;
DROP TRIGGER pipelines_notify_event ON pipelines;
DROP TRIGGER jobs_notify_event ON jobs;
DROP TRIGGER agent_sessions_notify_event ON agent_sessions;
DROP TRIGGER activity_events_notify_event ON activity_events;
DROP TRIGGER todo_items_notify_event ON todo_items;
DROP FUNCTION notify_event;

DROP TABLE activity_events;
DROP TABLE vcs_providers;
DROP TABLE environment_rule_eligible_deployers;
DROP TABLE environment_rules;
DROP TABLE metrics;
DROP TABLE todo_item_resolved_for_users;
DROP TABLE todo_item_assignees;
DROP TABLE todo_items;
DROP TABLE release_participants;
DROP TABLE plugin_platforms;
DROP TABLE plugin_versions;
DROP TABLE plugins;
DROP TABLE comments;
DROP TABLE threads;
DROP TABLE release_lifecycles;
DROP TABLE lifecycle_templates;
DROP TABLE task_jobs_relation;
DROP TABLE service_account_agent_relation;
DROP TABLE scim_tokens;
DROP TABLE resource_limits;
DROP TABLE memberships;
DROP TABLE roles;
DROP TABLE pipeline_approval_rule_relation;
DROP TABLE approval_rule_eligible_approvers;
DROP TABLE pipeline_approvals;
DROP TABLE pipeline_action_outputs;
DROP TABLE pipeline_nodes;
DROP TABLE approval_rules;
DROP TABLE pipelines;
DROP TABLE pipeline_templates;
DROP TABLE log_streams;
DROP TABLE agent_sessions;
DROP TABLE jobs;
DROP TABLE agents;
DROP TABLE releases;
DROP TABLE environments;
DROP TABLE team_members;
DROP TABLE teams;
DROP TABlE user_external_identities;
DROP TABLE users;
DROP TABLE service_accounts;
DROP TABLE projects;
DROP TABLE organizations;
