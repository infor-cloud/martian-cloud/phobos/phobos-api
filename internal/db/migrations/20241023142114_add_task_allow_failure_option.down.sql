ALTER TABLE pipeline_nodes
    DROP COLUMN IF EXISTS task_node_on_error;

ALTER TABLE pipeline_nodes
    DROP COLUMN IF EXISTS nested_pipeline_node_on_error;
