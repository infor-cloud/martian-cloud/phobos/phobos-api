UPDATE pipeline_nodes
SET task_node_when = 'repeated'
WHERE task_node_max_attempts > 1;

ALTER TABLE pipeline_nodes
ALTER COLUMN task_node_max_attempts DROP NOT NULL;

UPDATE pipeline_nodes
SET task_node_max_attempts = NULL
WHERE task_node_max_attempts = 1;

ALTER TABLE pipeline_nodes DROP COLUMN IF EXISTS task_node_attempt_count;

ALTER TABLE pipeline_nodes DROP COLUMN IF EXISTS task_node_last_attempt_finished_time;
