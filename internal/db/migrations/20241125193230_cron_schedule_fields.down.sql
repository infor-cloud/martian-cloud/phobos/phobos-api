ALTER TABLE pipeline_nodes DROP COLUMN IF EXISTS task_node_cron_expression,
    DROP COLUMN IF EXISTS task_node_cron_timezone,
    DROP COLUMN IF EXISTS nested_pipeline_node_cron_expression,
    DROP COLUMN IF EXISTS nested_pipeline_node_cron_timezone;
