INSERT INTO resource_limits
    (id, version, created_at, updated_at, name, value)
VALUES
    ('8b0bec39-584a-45ae-bf2e-cd65f2b05469', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'VariableSetsPerProject', 10000)
ON CONFLICT DO NOTHING;
