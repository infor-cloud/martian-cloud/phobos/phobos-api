CREATE TABLE IF NOT EXISTS project_variable_sets (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    project_id UUID NOT NULL,
    revision VARCHAR NOT NULL,
    latest BOOLEAN NOT NULL,
    CONSTRAINT fk_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_project_variable_sets_on_project_id_revision ON project_variable_sets(project_id, revision);
CREATE UNIQUE INDEX index_project_variable_sets_on_project_id_latest ON project_variable_sets(project_id, latest) WHERE latest = true;

CREATE TABLE IF NOT EXISTS project_variables (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    project_id UUID NOT NULL,
    key VARCHAR NOT NULL,
    value VARCHAR NOT NULL,
    environment_scope VARCHAR NOT NULL,
    pipeline_type VARCHAR NOT NULL,
    created_by VARCHAR NOT NULL,
    CONSTRAINT fk_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);

CREATE TABLE project_variable_set_variable_relation (
    variable_set_id UUID NOT NULL,
    variable_id UUID NOT NULL,
    CONSTRAINT fk_project_variable_set_variable_relation_variable_set_id FOREIGN KEY(variable_set_id) REFERENCES project_variable_sets(id) ON DELETE CASCADE,
    CONSTRAINT fk_project_variable_set_variable_relation_variable_id FOREIGN KEY(variable_id) REFERENCES project_variables(id) ON DELETE CASCADE,
    PRIMARY KEY(variable_set_id, variable_id)
);

ALTER TABLE activity_events
    ADD COLUMN IF NOT EXISTS project_variable_set_target_id UUID,
    ADD CONSTRAINT fk_activity_events_project_variable_set_target_id FOREIGN KEY(project_variable_set_target_id) REFERENCES project_variable_sets(id) ON DELETE CASCADE;
