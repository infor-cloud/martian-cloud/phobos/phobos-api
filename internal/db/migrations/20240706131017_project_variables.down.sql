DELETE FROM activity_events WHERE target_type = 'PROJECT_VARIABLE_SET';

ALTER TABLE activity_events
    DROP COLUMN IF EXISTS project_variable_set_target_id;

DROP TABLE IF EXISTS project_variable_set_variable_relation;
DROP TABLE IF EXISTS project_variables;
DROP TABLE IF EXISTS project_variable_sets;
