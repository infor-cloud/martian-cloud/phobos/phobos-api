CREATE TABLE organizations (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR,
    created_by VARCHAR NOT NULL
);
CREATE UNIQUE INDEX index_orgs_on_name ON organizations(name);

CREATE TABLE projects (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR,
    created_by VARCHAR NOT NULL,
    org_id UUID NOT NULL,
    CONSTRAINT fk_projects_org_id FOREIGN KEY(org_id) REFERENCES organizations(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_projects_on_name ON projects(name, org_id);

CREATE TABLE users (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    username VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    admin BOOLEAN NOT NULL,
    scim_external_id UUID,
    active BOOLEAN NOT NULL
);
CREATE UNIQUE INDEX index_users_on_username ON users(username);
CREATE UNIQUE INDEX index_users_on_email ON users(email);
CREATE UNIQUE INDEX index_users_on_scim_external_id ON users(scim_external_id);

CREATE TABLE user_external_identities (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    issuer VARCHAR NOT NULL,
    external_id VARCHAR NOT NULL,
    user_id UUID NOT NULL,
    CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_user_external_identities_on_issuer_external_id ON user_external_identities(issuer, external_id);

CREATE TABLE teams (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR,
    scim_external_id UUID
);
CREATE UNIQUE INDEX index_teams_on_name ON teams(name);
CREATE UNIQUE INDEX index_teams_on_scim_external_id ON teams(scim_external_id);

CREATE TABLE team_members (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    user_id UUID NOT NULL,
    team_id UUID NOT NULL,
    is_maintainer BOOLEAN NOT NULL,
    CONSTRAINT fk_team_members_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT fk_team_members_team_id FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_team_members_on_user_id_team_id ON team_members(user_id, team_id);

CREATE TABLE roles (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR,
    permissions JSONB NOT NULL
);
CREATE UNIQUE INDEX index_roles_on_name ON roles(name);

CREATE TABLE service_accounts (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    organization_id UUID,
    project_id UUID,
    created_by VARCHAR NOT NULL,
    oidc_trust_policies JSON NOT NULL,
    scope VARCHAR NOT NULL,
    CONSTRAINT fk_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_service_account_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS index_service_accounts_on_name_global
ON service_accounts(name)
WHERE scope = 'GLOBAL';
CREATE UNIQUE INDEX IF NOT EXISTS index_service_accounts_on_organization_id_name
ON service_accounts(organization_id, name)
WHERE scope = 'ORGANIZATION';
CREATE UNIQUE INDEX IF NOT EXISTS index_service_accounts_on_project_id_name
ON service_accounts(project_id, name)
WHERE scope = 'PROJECT';

INSERT INTO roles (
        id,
        version,
        created_at,
        updated_at,
        created_by,
        name,
        description,
        permissions
    )
VALUES (
        '3e8eb414-4504-4dc5-ad81-2f0d20db6ee4',
        1,
        CURRENT_TIMESTAMP(7),
        CURRENT_TIMESTAMP(7),
        'system',
        'owner',
        'Default owner role.',
        '[]'
    ),
    (
        'c78c6d31-465f-4d16-a365-51f10a4ca37d',
        1,
        CURRENT_TIMESTAMP(7),
        CURRENT_TIMESTAMP(7),
        'system',
        'release_manager',
        'Default release manager role.',
        '[]'
    ),
    (
        'd50e782f-c82a-4e18-b68b-c7eb176f4e5e',
        1,
        CURRENT_TIMESTAMP(7),
        CURRENT_TIMESTAMP(7),
        'system',
        'developer',
        'Default developer role.',
        '[]'
    ),
    (
        '2e240dba-ee81-4f0b-bd97-b8d8b15b9119',
        1,
        CURRENT_TIMESTAMP(7),
        CURRENT_TIMESTAMP(7),
        'system',
        'viewer',
        'Default viewer role.',
        '[]'
    ) ON CONFLICT DO NOTHING;

CREATE TABLE memberships (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    organization_id UUID,
    project_id UUID,
    user_id UUID,
    team_id UUID,
    service_account_id UUID,
    role_id UUID NOT NULL,
    scope VARCHAR NOT NULL,
    CONSTRAINT fk_memberships_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_memberships_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE,
    CONSTRAINT fk_memberships_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT fk_memberships_team_id FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE CASCADE,
    CONSTRAINT fk_memberships_service_account_id FOREIGN KEY(service_account_id) REFERENCES service_accounts(id) ON DELETE CASCADE,
    CONSTRAINT fk_memberships_role_id FOREIGN KEY(role_id) REFERENCES roles(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_memberships_on_user_id_organization_id ON memberships(user_id, organization_id)
WHERE scope = 'ORGANIZATION';
CREATE UNIQUE INDEX index_memberships_on_user_id_project_id ON memberships(user_id, project_id)
WHERE scope = 'PROJECT';
CREATE UNIQUE INDEX index_memberships_on_team_id_organization_id ON memberships(team_id, organization_id)
WHERE scope = 'ORGANIZATION';
CREATE UNIQUE INDEX index_memberships_on_team_id_project_id ON memberships(team_id, project_id)
WHERE scope = 'PROJECT';
CREATE UNIQUE INDEX index_memberships_on_service_account_id_organization_id ON memberships(service_account_id, organization_id)
WHERE scope = 'ORGANIZATION';
CREATE UNIQUE INDEX index_memberships_on_service_account_id_project_id ON memberships(service_account_id, project_id)
WHERE scope = 'PROJECT';
CREATE UNIQUE INDEX index_memberships_on_user_id_scope ON memberships(user_id, scope)
WHERE scope = 'GLOBAL';
CREATE UNIQUE INDEX index_memberships_on_team_id_scope ON memberships(team_id, scope)
WHERE scope = 'GLOBAL';
CREATE UNIQUE INDEX index_memberships_on_service_account_id_scope ON memberships(service_account_id, scope)
WHERE scope = 'GLOBAL';

CREATE TABLE resource_limits (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    value INTEGER NOT NULL
);
CREATE UNIQUE INDEX index_resource_limits_on_name ON resource_limits(name);

INSERT INTO resource_limits
    (id, version, created_at, updated_at, name, value)
VALUES
('c9b800cd-0b28-4188-aa55-dd977ccf035c', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'TotalRoles', 1000), -- total number of roles
('9eabc3e1-85a8-4335-8faa-2c39d8c638e2', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'PipelineTemplatesPerProjectPerTimePeriod', 1000), -- number of pipeline templates per project
('d7ee8540-bad8-413b-8d80-126b931f7df0', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'PipelinesPerProjectPerTimePeriod', 1000), -- number of pipelines per project
('02d5375e-b4e7-4976-8259-5ad997ed97c2', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'AgentsPerOrganization', 1000), -- number of agents per organization
('c5989b9c-45fb-420e-be96-acf4d74be9ad', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'EnvironmentsPerProject', 100), -- number of environments per project
('addfa030-11f7-4f6a-8a34-547723b2612d', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'EnvironmentRulesPerOrganization', 1000), -- number of environment protection rules per organization
('10bc1c5b-154c-4c4b-9ef1-730b898ee235', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ServiceAccountsPerOrganization', 1000), -- number of service accounts per organization
('0443837f-95aa-4b52-8da5-9c33ab251f10', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'LifecycleTemplatesPerOrganizationPerTimePeriod', 10000), -- number of lifecycle templates per organization
('b9fa2ed0-ebac-4afb-8f3e-7f78c594027b', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ReleaseLifecyclesPerOrganization', 1000), -- number of release lifecycles per organization
('9f296690-d398-4f64-b02c-7a51ef360912', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ApprovalRulesPerOrganization', 1000), -- number of approval rules per organization
('2868bb8a-e2cc-4c1d-a684-7545b8669a96', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ReleasesPerProjectPerTimePeriod', 1000), -- number of releases per project
('5877f8d4-40ff-4229-9c43-c5366c63b655', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'SessionsPerAgent', 100), -- number of active sessions per agent
('1bbf66c5-9178-4b87-93b3-802739bef65b', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ThreadsPerPipeline', 100), -- number of total threads per pipeline
('8adaca18-7a94-4aaa-877b-9c51cd8a1e92', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ThreadsPerRelease', 100), -- number of total threads per release
('894160c1-cf81-4cf6-ad17-8c867572b230', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'CommentsPerThread', 100), -- number of comments per thread
('5d89a0a0-09f5-44d3-81d2-18e9dfcd4256', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'PluginsPerOrganization', 1000), -- number of plugins per organization
('09939c55-d50e-486d-bf2b-ebd6e2036ed9', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'VersionsPerPlugin', 1000), -- number of plugin versions per plugin
('b2f6b8a9-9b0a-4b0e-8e9a-9e0b8b8b8b8b', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'PlatformsPerPluginVersion', 1000), -- number of platforms per plugin version
('888348f5-7b91-462e-963e-128be11f82da', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'VCSProvidersPerOrganization', 1000), -- number of VCS providers per organization
('1b15dfa7-0990-4f47-8140-3737999386bd', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'VCSProvidersPerProject', 1000), -- number of VCS providers per project
('9e117d58-d029-4958-b10f-af18bf34a227', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ServiceAccountsPerProject', 100), -- number of service accounts per project
('455426b5-c12b-45ba-a3a2-ab1fcdfb13d5', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ReleaseLifecyclesPerProject', 100), -- number of release lifecycles per project
('2c2f4934-4f88-4932-ab41-9465ba9db019', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'LifecycleTemplatesPerProjectPerTimePeriod', 100), -- number of lifecycle templates per project
('ebd60717-bfa6-4902-a558-bc68dbf7df7c', 1, CURRENT_TIMESTAMP(7), CURRENT_TIMESTAMP(7), 'ApprovalRulesPerProject', 100) -- number of approval rules per project
ON CONFLICT DO NOTHING;

CREATE TABLE scim_tokens (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    nonce UUID NOT NULL
);
CREATE UNIQUE INDEX index_scim_tokens_on_nonce ON scim_tokens(nonce);

CREATE TABLE environments (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    project_id UUID NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    created_by VARCHAR NOT NULL,
    CONSTRAINT fk_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_environments_on_project_id_name ON environments(project_id, name);

CREATE TABLE environment_rules (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    scope VARCHAR NOT NULL,
    organization_id UUID NOT NULL,
    project_id UUID,
    environment_name VARCHAR NOT NULL,
    CONSTRAINT fk_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX index_environment_rules_on_organization_id_environment_name
ON environment_rules(organization_id, environment_name)
WHERE scope = 'ORGANIZATION';

CREATE UNIQUE INDEX index_environment_rules_on_project_id_environment_name
ON environment_rules(project_id, environment_name)
WHERE scope = 'PROJECT';


CREATE TABLE approval_rules (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR,
    organization_id UUID NOT NULL,
    approvals_required INTEGER NOT NULL,
    created_by VARCHAR NOT NULL,
    scope VARCHAR NOT NULL,
    project_id UUID,
    CONSTRAINT fk_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_approval_rule_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX IF NOT EXISTS index_approval_rules_organization_id_name
ON approval_rules(organization_id, name)
WHERE scope = 'ORGANIZATION';
CREATE UNIQUE INDEX IF NOT EXISTS index_approval_rules_project_id_name
ON approval_rules(project_id, name)
WHERE scope = 'PROJECT';

CREATE TABLE environment_rule_eligible_deployers (
    id UUID PRIMARY KEY,
    environment_rule_id UUID NOT NULL,
    user_id UUID,
    team_id UUID,
    service_account_id UUID,
    role_id UUID,
    CONSTRAINT fk_environment_rule_eligible_deployers_environment_rule_id FOREIGN KEY(environment_rule_id) REFERENCES environment_rules(id) ON DELETE CASCADE,
    CONSTRAINT fk_environment_rule_eligible_deployers_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT fk_environment_rule_eligible_deployers_team_id FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE CASCADE,
    CONSTRAINT fk_environment_rule_eligible_deployers_service_account_id FOREIGN KEY(service_account_id) REFERENCES service_accounts(id) ON DELETE CASCADE,
    CONSTRAINT fk_environment_rule_eligible_deployers_role_id FOREIGN KEY(role_id) REFERENCES roles(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_environment_rule_eligible_deployers_rule_id_user_id ON environment_rule_eligible_deployers(
    environment_rule_id,
    user_id
);
CREATE UNIQUE INDEX index_environment_rule_eligible_deployers_rule_id_service_account_id ON environment_rule_eligible_deployers(
    environment_rule_id,
    service_account_id
);
CREATE UNIQUE INDEX index_environment_rule_eligible_deployers_rule_id_team_id ON environment_rule_eligible_deployers(
    environment_rule_id,
    team_id
);
CREATE UNIQUE INDEX index_environment_rule_eligible_deployers_rule_id_role_id ON environment_rule_eligible_deployers(
    environment_rule_id,
    role_id
);

CREATE TABLE approval_rule_eligible_approvers (
    id UUID PRIMARY KEY,
    approval_rule_id UUID NOT NULL,
    user_id UUID,
    team_id UUID,
    service_account_id UUID,
    CONSTRAINT fk_approval_rule_eligible_approvers_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT fk_approval_rule_eligible_approvers_team_id FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE CASCADE,
    CONSTRAINT fk_approval_rule_eligible_approvers_service_account_id FOREIGN KEY(service_account_id) REFERENCES service_accounts(id) ON DELETE CASCADE,
    CONSTRAINT fk_approval_rule_eligible_approvers_approval_rule_id FOREIGN KEY(approval_rule_id) REFERENCES approval_rules(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_approval_rule_eligible_approvers_rule_id_user_id ON approval_rule_eligible_approvers(
    approval_rule_id,
    user_id
);
CREATE UNIQUE INDEX index_approval_rule_eligible_approvers_rule_id_service_account_id ON approval_rule_eligible_approvers(
    approval_rule_id,
    service_account_id
);
CREATE UNIQUE INDEX index_approval_rule_eligible_approvers_rule_id_team_id ON approval_rule_eligible_approvers(
    approval_rule_id,
    team_id
);

CREATE TABLE pipeline_templates (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    project_id UUID NOT NULL,
    status VARCHAR NOT NULL,
    versioned BOOLEAN NOT NULL,
    name VARCHAR,
    semver VARCHAR,
    latest BOOLEAN NOT NULL,
    CONSTRAINT fk_pipeline_template_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);
CREATE UNIQUE index IF NOT EXISTS index_pipeline_templates_project_id_name_semver ON pipeline_templates(
    project_id,
    name,
    semver
);
CREATE UNIQUE INDEX IF NOT EXISTS index_pipeline_templates_project_id_name_latest ON pipeline_templates(
    project_id,
    name,
    latest
) WHERE latest = true;

CREATE TABLE IF NOT EXISTS agents (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    type VARCHAR NOT NULL,
    organization_id UUID,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    created_by VARCHAR NOT NULL,
    disabled BOOLEAN NOT NULL,
    tags JSONB NOT NULL,
    run_untagged_jobs BOOLEAN NOT NULL,
    CONSTRAINT fk_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_agents_on_name ON agents(name)
WHERE type = 'SHARED';
CREATE UNIQUE INDEX index_agents_on_organization_id_name ON agents(organization_id, name)
WHERE type = 'ORGANIZATION';

CREATE TABLE IF NOT EXISTS agent_sessions (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    agent_id UUID NOT NULL,
    last_contacted_at TIMESTAMP NOT NULL,
    error_count INTEGER NOT NULL,
    internal BOOLEAN NOT NULL,
    CONSTRAINT fk_agent_id FOREIGN KEY(agent_id) REFERENCES agents(id) ON DELETE CASCADE
);
CREATE INDEX index_agent_sessions_on_agent_id ON agent_sessions(agent_id);

CREATE TABLE jobs (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    status VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    project_id UUID NOT NULL,
    agent_id UUID,
    cancel_requested_at TIMESTAMP,
    force_canceled BOOLEAN NOT NULL,
    force_canceled_by VARCHAR,
    queued_at TIMESTAMP,
    pending_at TIMESTAMP,
    running_at TIMESTAMP,
    finished_at TIMESTAMP,
    max_job_duration INTEGER NOT NULL,
    -- in minutes
    agent_name VARCHAR,
    agent_type VARCHAR,
    data JSONB NOT NULL,
    tags JSONB NOT NULL,
    CONSTRAINT fk_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE,
    CONSTRAINT fk_agent_id FOREIGN KEY(agent_id) REFERENCES agents(id) ON DELETE SET NULL
);
CREATE INDEX index_jobs_on_project_id ON jobs(project_id);
CREATE INDEX index_jobs_on_agent_id ON jobs(agent_id);
CREATE INDEX IF NOT EXISTS index_jobs_on_tags ON jobs USING GIN (tags jsonb_path_ops);

CREATE TABLE IF NOT EXISTS releases (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    semantic_version VARCHAR NOT NULL,
    project_id UUID NOT NULL,
    due_date TIMESTAMP,
    pre_release BOOLEAN NOT NULL,
    lifecycle_prn VARCHAR NOT NULL,
    latest BOOLEAN NOT NULL,
    CONSTRAINT fk_release_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_releases_on_semantic_version_project_id ON releases(project_id, semantic_version);
CREATE UNIQUE INDEX IF NOT EXISTS index_releases_project_id_latest ON releases(
    project_id,
    latest
) WHERE latest = true;

CREATE TABLE IF NOT EXISTS release_participants (
    id UUID PRIMARY KEY,
    release_id UUID NOT NULL,
    user_id UUID,
    team_id UUID,
    CONSTRAINT fk_release_participants_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT fk_release_participants_team_id FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE CASCADE,
    CONSTRAINT fk_release_participants_release_id FOREIGN KEY(release_id) REFERENCES releases(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_release_participants_release_id_user_id ON release_participants(
    release_id,
    user_id
);
CREATE UNIQUE INDEX index_release_participants_release_id_team_id ON release_participants(
    release_id,
    team_id
);

CREATE TABLE pipelines (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    project_id UUID NOT NULL,
    pipeline_template_id UUID NOT NULL,
    status VARCHAR NOT NULL,
    release_id UUID,
    parent_pipeline_id UUID,
    parent_pipeline_node_path VARCHAR,
    type VARCHAR NOT NULL,
    when_condition VARCHAR NOT NULL,
    superseded BOOLEAN NOT NULL,
    environment_name VARCHAR,
    started_at TIMESTAMP,
    completed_at TIMESTAMP,
    annotations JSONB NOT NULL DEFAULT '{}',
    approval_status VARCHAR,
    CONSTRAINT fk_pipelines_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE,
    CONSTRAINT fk_pipelines_pipeline_template_id FOREIGN KEY(pipeline_template_id) REFERENCES pipeline_templates(id),
    CONSTRAINT fk_pipelines_release_id FOREIGN KEY(release_id) REFERENCES releases(id) ON DELETE CASCADE,
    CONSTRAINT fk_pipelines_parent_pipeline_id FOREIGN KEY(parent_pipeline_id) REFERENCES pipelines(id) ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS index_pipelines_on_annotations ON pipelines USING GIN (annotations jsonb_ops);

CREATE TABLE pipeline_nodes (
    id UUID PRIMARY KEY,
    pipeline_id UUID NOT NULL,
    parent_id UUID,
    path VARCHAR NOT NULL,
    node_type VARCHAR NOT NULL,
    pos INTEGER NOT NULL,
    name VARCHAR,
    status VARCHAR NOT NULL,
    action_node_plugin_name VARCHAR,
    action_node_plugin_action_name VARCHAR,
    task_node_approval_status VARCHAR,
    task_node_when VARCHAR,
    task_node_interval_seconds INTEGER,
    task_node_latest_job_id UUID,
    task_node_has_success_condition BOOLEAN,
    task_node_max_attempts INTEGER,
    task_node_agent_tags JSONB,
    task_node_scheduled_start_time TIMESTAMP,
    task_node_dependencies JSONB DEFAULT '[]',
    nested_pipeline_node_environment_name VARCHAR,
    nested_pipeline_node_latest_pipeline_id UUID,
    nested_pipeline_node_scheduled_start_time TIMESTAMP,
    nested_pipeline_node_approval_status VARCHAR,
    nested_pipeline_node_dependencies JSONB DEFAULT '[]',
    CONSTRAINT fk_pipeline_nodes_parent_id FOREIGN KEY(parent_id) REFERENCES pipeline_nodes(id) ON DELETE CASCADE,
    CONSTRAINT fk_pipeline_nodes_pipeline_id FOREIGN KEY(pipeline_id) REFERENCES pipelines(id) ON DELETE CASCADE,
    CONSTRAINT fk_pipeline_nodes_task_node_latest_job_id FOREIGN KEY(task_node_latest_job_id) REFERENCES jobs(id) ON DELETE SET NULL,
    CONSTRAINT fk_pipeline_nodes_nested_pipeline_node_latest_pipeline_id FOREIGN KEY(nested_pipeline_node_latest_pipeline_id) REFERENCES pipelines(id) ON DELETE SET NULL
);
CREATE UNIQUE INDEX index_pipeline_nodes_pipeline_id_path ON pipeline_nodes(pipeline_id, path);
CREATE INDEX index_pipeline_nodes_pipeline_id_pos ON pipeline_nodes(pipeline_id, pos);
CREATE INDEX index_pipeline_nodes_node_type_status ON pipeline_nodes(node_type, status);

CREATE TABLE pipeline_approvals (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    pipeline_id UUID NOT NULL,
    task_path VARCHAR,
    type VARCHAR NOT NULL,
    user_id UUID,
    service_account_id UUID,
    CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE SET NULL,
    CONSTRAINT fk_service_account_id FOREIGN KEY(service_account_id) REFERENCES service_accounts(id) ON DELETE SET NULL,
    CONSTRAINT fk_pipeline_id_task_path FOREIGN KEY(pipeline_id, task_path) REFERENCES pipeline_nodes(pipeline_id, path) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_pipeline_approvals_pipeline_id_task_path_user_id ON pipeline_approvals(
    pipeline_id,
    task_path,
    user_id
);
CREATE UNIQUE INDEX index_pipeline_approvals_pipeline_id_task_path_service_account_id ON pipeline_approvals(
    pipeline_id,
    task_path,
    service_account_id
);

CREATE UNIQUE INDEX index_pipeline_approvals_pipeline_id_user_id ON pipeline_approvals(
    pipeline_id,
    user_id
) WHERE type = 'PIPELINE';

CREATE UNIQUE INDEX index_pipeline_approvals_pipeline_id_service_account_id ON pipeline_approvals(
    pipeline_id,
    service_account_id
) WHERE type = 'PIPELINE';

CREATE TABLE pipeline_approval_rule_relation (
    approval_rule_id UUID NOT NULL,
    pipeline_id UUID,
    pipeline_node_id UUID,
    approval_type VARCHAR NOT NULL,
    CONSTRAINT fk_pipeline_approval_rule_relation_approval_rule_id FOREIGN KEY(approval_rule_id) REFERENCES approval_rules(id) ON DELETE CASCADE,
    CONSTRAINT fk_pipeline_approval_rule_relation_pipeline_id FOREIGN KEY(pipeline_id) REFERENCES pipelines(id) ON DELETE CASCADE,
    CONSTRAINT fk_pipeline_approval_rule_relation_pipeline_node_id FOREIGN KEY(pipeline_node_id) REFERENCES pipeline_nodes(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX index_pipeline_approval_rule_relation_pipeline_id ON pipeline_approval_rule_relation (
    approval_rule_id,
    pipeline_id
) WHERE approval_type = 'PIPELINE';

CREATE UNIQUE INDEX index_pipeline_approval_rule_relation_pipeline_node_id ON pipeline_approval_rule_relation (
    approval_rule_id,
    pipeline_node_id
) WHERE approval_type = 'TASK';

CREATE TABLE pipeline_action_outputs (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    value VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    pipeline_id UUID NOT NULL,
    action_path VARCHAR NOT NULL,
    CONSTRAINT fk_pipeline_id_action_path FOREIGN KEY(pipeline_id, action_path) REFERENCES pipeline_nodes(pipeline_id, path) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_pipeline_action_outputs_pipeline_id_action_path_name ON pipeline_action_outputs(pipeline_id, action_path, name);

CREATE TABLE service_account_agent_relation (
    agent_id UUID NOT NULL,
    service_account_id UUID NOT NULL,
    CONSTRAINT fk_agent_id FOREIGN KEY(agent_id) REFERENCES agents(id) ON DELETE CASCADE,
    CONSTRAINT fk_service_account_id FOREIGN KEY(service_account_id) REFERENCES service_accounts(id) ON DELETE CASCADE,
    PRIMARY KEY(agent_id, service_account_id)
);

CREATE TABLE IF NOT EXISTS metrics (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    name VARCHAR NOT NULL,
    value REAL NOT NULL,
    tags JSONB NOT NULL DEFAULT '{}',
    pipeline_target_id UUID,
    release_target_id UUID,
    project_target_id UUID,
    organization_target_id UUID NOT NULL,
    environment_name VARCHAR,
    CONSTRAINT fk_metrics_pipeline_target_id FOREIGN KEY(pipeline_target_id) REFERENCES pipelines(id) ON DELETE CASCADE,
    CONSTRAINT fk_metrics_release_target_id FOREIGN KEY(release_target_id) REFERENCES releases(id) ON DELETE CASCADE,
    CONSTRAINT fk_metrics_project_target_id FOREIGN KEY(project_target_id) REFERENCES projects(id) ON DELETE CASCADE,
    CONSTRAINT fk_metrics_organization_target_id FOREIGN KEY(organization_target_id) REFERENCES organizations(id) ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS index_metrics_on_organization_target_id ON metrics(name, organization_target_id);
CREATE INDEX IF NOT EXISTS index_metrics_on_project_target_id ON metrics(name, project_target_id);
CREATE INDEX IF NOT EXISTS index_metrics_on_release_target_id ON metrics(name, release_target_id);
CREATE INDEX IF NOT EXISTS index_metrics_on_pipeline_target_id ON metrics(name, pipeline_target_id);
CREATE INDEX IF NOT EXISTS index_metrics_on_environment_name ON metrics(name, environment_name);
CREATE INDEX IF NOT EXISTS index_metrics_on_created_at ON metrics USING BRIN (created_at) WITH (pages_per_range = 128);
CREATE INDEX IF NOT EXISTS index_metrics_on_tags ON metrics USING GIN (tags jsonb_ops);

CREATE TABLE log_streams (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    job_id UUID,
    agent_session_id UUID,
    size INTEGER NOT NULL,
    completed BOOLEAN NOT NULL,
    CONSTRAINT fk_log_streams_job_id FOREIGN KEY(job_id) REFERENCES jobs(id) ON DELETE CASCADE,
    CONSTRAINT fk_log_streams_agent_session_id FOREIGN KEY(agent_session_id) REFERENCES agent_sessions(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_log_streams_on_job_id ON log_streams(job_id);
CREATE UNIQUE INDEX index_log_streams_on_agent_session_id ON log_streams(agent_session_id);

CREATE TABLE task_jobs_relation (
    pipeline_id UUID NOT NULL,
    task_path VARCHAR NOT NULL,
    job_id UUID NOT NULL,
    CONSTRAINT fk_task_jobs_relation_pipeline_id FOREIGN KEY(pipeline_id) REFERENCES pipelines(id) ON DELETE CASCADE,
    CONSTRAINT fk_task_jobs_relation_job_id FOREIGN KEY(job_id) REFERENCES jobs(id) ON DELETE CASCADE,
    PRIMARY KEY(pipeline_id, task_path, job_id)
);

CREATE TABLE IF NOT EXISTS lifecycle_templates (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    organization_id UUID NOT NULL,
    project_id UUID,
    status VARCHAR NOT NULL,
    scope VARCHAR NOT NULL,
    CONSTRAINT fk_lifecycle_template_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_lifecycle_template_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS release_lifecycles (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    organization_id UUID NOT NULL,
    project_id UUID,
    lifecycle_template_id UUID NOT NULL,
    environments JSONB NOT NULL,
    scope VARCHAR NOT NULL,
    CONSTRAINT fk_release_lifecycle_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_release_lifecycle_lifecycle_template_id FOREIGN KEY(lifecycle_template_id) REFERENCES lifecycle_templates(id) ON DELETE CASCADE,
    CONSTRAINT fk_release_lifecycle_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS index_release_lifecycles_on_name_organization_id
ON release_lifecycles(name, organization_id)
WHERE scope = 'ORGANIZATION';
CREATE UNIQUE INDEX IF NOT EXISTS index_release_lifecycles_on_name_project_id
ON release_lifecycles(name, project_id)
WHERE scope = 'PROJECT';

CREATE TABLE IF NOT EXISTS threads (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    pipeline_id UUID,
    release_id UUID,
    CONSTRAINT fk_threads_pipeline_id FOREIGN KEY(pipeline_id) REFERENCES pipelines(id) ON DELETE CASCADE,
    CONSTRAINT fk_threads_release_id FOREIGN KEY(release_id) REFERENCES releases(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS comments (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    text VARCHAR NOT NULL,
    user_id UUID,
    service_account_id UUID,
    thread_id UUID NOT NULL,
    CONSTRAINT fk_comment_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE SET NULL,
    CONSTRAINT fk_comment_service_account_id FOREIGN KEY(service_account_id) REFERENCES service_accounts(id) ON DELETE SET NULL,
     CONSTRAINT fk_comment_thread_id FOREIGN KEY(thread_id) REFERENCES threads(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS plugins (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    organization_id UUID NOT NULL,
    private BOOLEAN NOT NULL,
    repository_url VARCHAR NOT NULL DEFAULT '',
    CONSTRAINT fk_plugin_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_plugins_on_name_organization_id ON plugins(name, organization_id);

CREATE TABLE IF NOT EXISTS plugin_versions (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    plugin_id UUID NOT NULL,
    plugin_semantic_version VARCHAR NOT NULL,
    readme_uploaded BOOLEAN NOT NULL,
    schema_uploaded BOOLEAN NOT NULL,
    sha_sums_uploaded BOOLEAN NOT NULL,
    latest BOOLEAN NOT NULL,
    protocols JSON NOT NULL,
    docs JSONB NOT NULL,
    CONSTRAINT fk_plugin_version_plugin_id FOREIGN KEY(plugin_id) REFERENCES plugins(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_plugin_versions_on_plugin_id_plugin_semver ON plugin_versions(plugin_id, plugin_semantic_version);
CREATE UNIQUE INDEX index_plugin_versions_on_plugin_id_latest ON plugin_versions(plugin_id, latest) WHERE latest = true;

CREATE TABLE IF NOT EXISTS plugin_platforms (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    plugin_version_id UUID NOT NULL,
    os VARCHAR NOT NULL,
    arch VARCHAR NOT NULL,
    sha_sum VARCHAR NOT NULL,
    filename VARCHAR NOT NULL,
    binary_uploaded BOOLEAN NOT NULL,
    CONSTRAINT fk_plugin_platforms_plugin_version_id FOREIGN KEY(plugin_version_id) REFERENCES plugin_versions(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_plugin_platforms_on_plugin_version_id_os_arch ON plugin_platforms(plugin_version_id, os, arch);

CREATE TABLE IF NOT EXISTS todo_items (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    organization_id UUID NOT NULL,
    project_id UUID,
    auto_resolved BOOLEAN NOT NULL,
    resolvable BOOLEAN NOT NULL,
    target_type VARCHAR NOT NULL,
    pipeline_target_id UUID,
    payload JSONB,
    CONSTRAINT fk_todo_items_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_todo_items_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE,
    CONSTRAINT fk_todo_items_pipeline_target_id FOREIGN KEY(pipeline_target_id) REFERENCES pipelines(id) ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS index_todo_items_on_organization_id ON todo_items(organization_id);
CREATE INDEX IF NOT EXISTS index_todo_items_on_project_id ON todo_items(project_id)
WHERE project_id IS NOT NULL;

CREATE TABLE IF NOT EXISTS todo_item_assignees (
    id UUID PRIMARY KEY,
    todo_item_id UUID NOT NULL,
    user_id UUID,
    team_id UUID,
    CONSTRAINT fk_todo_item_assignees_todo_item_id FOREIGN KEY(todo_item_id) REFERENCES todo_items(id) ON DELETE CASCADE,
    CONSTRAINT fk_todo_item_assignees_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT fk_todo_item_assignees_team_id FOREIGN KEY(team_id) REFERENCES teams(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX IF NOT EXISTS index_todo_item_assignees_on_todo_item_id_user_id ON todo_item_assignees(todo_item_id, user_id);
CREATE UNIQUE INDEX IF NOT EXISTS index_todo_item_assignees_on_todo_item_id_team_id ON todo_item_assignees(todo_item_id, team_id);

CREATE TABLE IF NOT EXISTS todo_item_resolved_for_users (
    todo_item_id UUID NOT NULL,
    user_id UUID NOT NULL,
    CONSTRAINT fk_todo_item_resolved_for_users_todo_item_id FOREIGN KEY(todo_item_id) REFERENCES todo_items(id) ON DELETE CASCADE,
    CONSTRAINT fk_todo_item_resolved_for_users_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    PRIMARY KEY(todo_item_id, user_id)
);

CREATE TABLE IF NOT EXISTS vcs_providers (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    created_by VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    scope VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    auth_type VARCHAR NOT NULL,
    url VARCHAR NOT NULL,
    organization_id UUID NOT NULL,
    project_id UUID,
    oauth_client_id VARCHAR,
    oauth_client_secret VARCHAR,
    oauth_state UUID,
    oauth_access_token VARCHAR,
    oauth_refresh_token VARCHAR,
    oauth_access_token_expires_at TIMESTAMP,
    personal_access_token VARCHAR,
    CONSTRAINT fk_vcs_providers_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_vcs_providers_project_id FOREIGN KEY (project_id) REFERENCES projects(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX index_vcs_providers_on_organization_id ON vcs_providers(name, organization_id)
WHERE scope = 'ORGANIZATION';

CREATE UNIQUE INDEX index_vcs_providers_on_project_id ON vcs_providers(name, project_id)
WHERE scope = 'PROJECT';

CREATE TABLE IF NOT EXISTS activity_events (
    id UUID PRIMARY KEY,
    version INTEGER NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    user_id UUID,
    organization_id UUID,
    project_id UUID,
    service_account_id UUID,
    action VARCHAR NOT NULL,
    target_type VARCHAR NOT NULL,
    payload JSONB,
    role_target_id UUID,
    organization_target_id UUID,
    project_target_id UUID,
    membership_target_id UUID,
    agent_target_id UUID,
    pipeline_template_target_id UUID,
    pipeline_target_id UUID,
    team_target_id UUID,
    environment_target_id UUID,
    service_account_target_id UUID,
    approval_rule_target_id UUID,
    lifecycle_template_target_id UUID,
    release_lifecycle_target_id UUID,
    release_target_id UUID,
    plugin_target_id UUID,
    plugin_version_target_id UUID,
    comment_target_id UUID,
    vcs_provider_target_id UUID,
    thread_target_id UUID,
    environment_rule_target_id UUID,
    CONSTRAINT fk_activity_events_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_service_account_id FOREIGN KEY(service_account_id) REFERENCES service_accounts(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_organization_id FOREIGN KEY(organization_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_project_id FOREIGN KEY(project_id) REFERENCES projects(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_role_target_id FOREIGN KEY(role_target_id) REFERENCES roles(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_organization_target_id FOREIGN KEY(organization_target_id) REFERENCES organizations(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_project_target_id FOREIGN KEY(project_target_id) REFERENCES projects(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_membership_target_id FOREIGN KEY(membership_target_id) REFERENCES memberships(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_agent_target_id FOREIGN KEY(agent_target_id) REFERENCES agents(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_pipeline_template_target_id FOREIGN KEY(pipeline_template_target_id) REFERENCES pipeline_templates(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_pipeline_target_id FOREIGN KEY(pipeline_target_id) REFERENCES pipelines(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_team_target_id FOREIGN KEY(team_target_id) REFERENCES teams(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_environment_target_id FOREIGN KEY(environment_target_id) REFERENCES environments(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_service_account_target_id FOREIGN KEY(service_account_target_id) REFERENCES service_accounts(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_approval_rule_target_id FOREIGN KEY(approval_rule_target_id) REFERENCES approval_rules(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_lifecycle_template_target_id FOREIGN KEY(lifecycle_template_target_id) REFERENCES lifecycle_templates(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_release_lifecycle_target_id FOREIGN KEY(release_lifecycle_target_id) REFERENCES release_lifecycles(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_release_target_id FOREIGN KEY(release_target_id) REFERENCES releases(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_plugin_target_id FOREIGN KEY(plugin_target_id) REFERENCES plugins(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_plugin_version_target_id FOREIGN KEY(plugin_version_target_id) REFERENCES plugin_versions(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_comment_target_id FOREIGN KEY(comment_target_id) REFERENCES comments(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_vcs_provider_target_id FOREIGN KEY(vcs_provider_target_id) REFERENCES vcs_providers(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_thread_target_id FOREIGN KEY(thread_target_id) REFERENCES threads(id) ON DELETE CASCADE,
    CONSTRAINT fk_activity_events_environment_rule_target_id FOREIGN KEY(environment_rule_target_id) REFERENCES environment_rules(id) ON DELETE CASCADE
);
CREATE INDEX IF NOT EXISTS index_activity_events_on_user_id ON activity_events(user_id);
CREATE INDEX IF NOT EXISTS index_activity_events_on_service_account_id ON activity_events(service_account_id);
CREATE INDEX IF NOT EXISTS index_activity_events_on_organization_id ON activity_events(organization_id)
WHERE organization_id IS NOT NULL;
CREATE INDEX IF NOT EXISTS index_activity_events_on_project_id ON activity_events(project_id)
WHERE project_id IS NOT NULL;

CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS $$
DECLARE row RECORD;
notification json;

    BEGIN -- Convert the old or new row to JSON, based on the kind of action.
-- Action = DELETE?             -> OLD row
-- Action = INSERT or UPDATE?   -> NEW row
IF (TG_OP = 'DELETE') THEN row = OLD;
ELSE row = NEW;
END IF;

        -- Construct the notification as a JSON string.
notification = json_build_object(
    'table',
    TG_TABLE_NAME,
    'action',
    TG_OP,
    'id',
    row.id
);


        -- Execute pg_notify(channel, notification)
PERFORM pg_notify('events', notification::text);

        -- Result is ignored since this is an AFTER trigger
RETURN NULL;
END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER pipelines_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON pipelines FOR EACH ROW EXECUTE PROCEDURE notify_event();

CREATE TRIGGER log_streams_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON log_streams FOR EACH ROW EXECUTE PROCEDURE notify_event();

CREATE TRIGGER agents_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON agents FOR EACH ROW EXECUTE PROCEDURE notify_event();

CREATE TRIGGER jobs_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON jobs FOR EACH ROW EXECUTE PROCEDURE notify_event();

CREATE TRIGGER agent_sessions_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON agent_sessions FOR EACH ROW EXECUTE PROCEDURE notify_event();

CREATE TRIGGER activity_events_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON activity_events FOR EACH ROW EXECUTE PROCEDURE notify_event();

CREATE TRIGGER comments_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON comments FOR EACH ROW EXECUTE PROCEDURE notify_event();

CREATE TRIGGER todo_items_notify_event
AFTER
INSERT
    OR
UPDATE
    OR DELETE ON todo_items FOR EACH ROW EXECUTE PROCEDURE notify_event();
