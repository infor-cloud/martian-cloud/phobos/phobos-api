ALTER TABLE pipeline_nodes ADD COLUMN IF NOT EXISTS task_node_cron_expression VARCHAR,
    ADD COLUMN IF NOT EXISTS task_node_cron_timezone VARCHAR,
    ADD COLUMN IF NOT EXISTS nested_pipeline_node_cron_expression VARCHAR,
    ADD COLUMN IF NOT EXISTS nested_pipeline_node_cron_timezone VARCHAR;
