ALTER TABLE pipeline_nodes
    ADD COLUMN IF NOT EXISTS node_errors JSONB DEFAULT '[]';
