//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (os ServiceAccountSortableField) getValue() string {
	return string(os)
}

// Some constants and pseudo-constants are declared/defined in dbclient_test.go.

type serviceAccountWarmupInput struct {
	organizations     []models.Organization
	serviceAccounts   []models.ServiceAccount
	oidcTrustPolicies []models.OIDCTrustPolicy
}

type serviceAccountWarmupOutput struct {
	serviceAccounts []models.ServiceAccount
	organizations   []models.Organization
}

// serviceAccountInfo aids convenience in accessing the information TestGetServiceAccounts
// needs about the warmup service accounts.
type serviceAccountInfo struct {
	createTime       time.Time
	updateTime       time.Time
	serviceAccountID string
	name             string
}

// serviceAccountInfoIDSlice makes a slice of serviceAccountInfo sortable by ID string
type serviceAccountInfoIDSlice []serviceAccountInfo

// serviceAccountInfoCreateSlice makes a slice of serviceAccountInfo sortable by creation time
type serviceAccountInfoCreateSlice []serviceAccountInfo

// serviceAccountInfoUpdateSlice makes a slice of serviceAccountInfo sortable by last updated time
type serviceAccountInfoUpdateSlice []serviceAccountInfo

// serviceAccountInfoNameSlice makes a slice of serviceAccountInfo sortable by name
type serviceAccountInfoNameSlice []serviceAccountInfo

func TestGetServiceAccountByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdLow := time.Now()
	warmupOutput, err := createWarmupServiceAccounts(ctx, testClient, serviceAccountWarmupInput{
		organizations:     standardWarmupOrganizationsForServiceAccounts,
		serviceAccounts:   standardWarmupServiceAccounts,
		oidcTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
	})
	require.Nil(t, err)
	createdHigh := time.Now()

	type testCase struct {
		expectErrorCode      errors.CodeType
		expectServiceAccount *models.ServiceAccount
		name                 string
		searchID             string
	}

	positiveServiceAccount := warmupOutput.serviceAccounts[0]
	now := time.Now()
	testCases := []testCase{
		{
			name:     "positive",
			searchID: positiveServiceAccount.Metadata.ID,
			expectServiceAccount: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID:                positiveServiceAccount.Metadata.ID,
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
					PRN:               positiveServiceAccount.Metadata.PRN,
				},
				Name:              positiveServiceAccount.Name,
				Description:       positiveServiceAccount.Description,
				OrganizationID:    positiveServiceAccount.OrganizationID,
				CreatedBy:         positiveServiceAccount.CreatedBy,
				OIDCTrustPolicies: positiveServiceAccount.OIDCTrustPolicies,
				Scope:             positiveServiceAccount.Scope,
			},
		},

		{
			name:     "negative, non-existent service account ID",
			searchID: nonExistentID,
			// expect service account and error to be nil
		},

		{
			name:            "defective-ID",
			searchID:        invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualServiceAccount, err := testClient.client.ServiceAccounts.GetServiceAccountByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectServiceAccount != nil {
				require.NotNil(t, actualServiceAccount)
				compareServiceAccounts(t, test.expectServiceAccount, actualServiceAccount, false, &timeBounds{
					createLow:  &createdLow,
					createHigh: &createdHigh,
					updateLow:  &createdLow,
					updateHigh: &createdHigh,
				})
			} else {
				assert.Nil(t, actualServiceAccount)
			}
		})
	}
}

func TestGetServiceAccountByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdLow := time.Now()
	warmupOutput, err := createWarmupServiceAccounts(ctx, testClient, serviceAccountWarmupInput{
		organizations:     standardWarmupOrganizationsForServiceAccounts,
		serviceAccounts:   standardWarmupServiceAccounts,
		oidcTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
	})
	require.Nil(t, err)

	createdHigh := time.Now()

	type testCase struct {
		expectErrorCode      errors.CodeType
		expectServiceAccount *models.ServiceAccount
		name                 string
		searchPRN            string
	}

	testCases := []testCase{}

	for ix := range warmupOutput.serviceAccounts {
		testCases = append(testCases, testCase{
			name:                 fmt.Sprintf("positive %d", ix),
			searchPRN:            warmupOutput.serviceAccounts[ix].Metadata.PRN,
			expectServiceAccount: &warmupOutput.serviceAccounts[ix],
		})
	}

	testCases = append(testCases, []testCase{
		{
			name:      "negative, non-existent service account PRN",
			searchPRN: models.ServiceAccountResource.BuildPRN("non-existent-service-account"),
			// expect service account and error to be nil
		},
		{
			name:            "defective-PRN",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	}...)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualServiceAccount, err := testClient.client.ServiceAccounts.GetServiceAccountByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectServiceAccount != nil {
				require.NotNil(t, actualServiceAccount)
				compareServiceAccounts(t, test.expectServiceAccount, actualServiceAccount, false, &timeBounds{
					createLow:  &createdLow,
					createHigh: &createdHigh,
					updateLow:  &createdLow,
					updateHigh: &createdHigh,
				})
			} else {
				assert.Nil(t, actualServiceAccount)
			}
		})
	}
}

func TestGetServiceAccounts(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	org2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org2",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	proj1b, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	trustPolicies := []models.OIDCTrustPolicy{
		{
			Issuer:      "issuer-0",
			BoundClaims: map[string]string{"bc1-k1": "bc1-v1", "bc1-k2": "bc1-v2"},
		},
		{
			Issuer:      "issuer-1",
			BoundClaims: map[string]string{"bc2-k1": "bc2-v1", "bc2-k2": "bc2-v2"},
		},
	}

	// 10 service accounts in org1
	serviceAccounts := make([]*models.ServiceAccount, 10)
	for i := 0; i < len(serviceAccounts); i++ {
		// a mix of org-scope, proj1a, and proj1b
		var scope models.ScopeType
		var projectID *string
		switch i % 3 {
		case 0: // i=0, 3, 6, 9
			scope = models.OrganizationScope
		case 1: // i=1, 4, 7
			scope = models.ProjectScope
			projectID = &proj1a.Metadata.ID
		case 2: // i=2, 5, 8
			scope = models.ProjectScope
			projectID = &proj1b.Metadata.ID
		}

		sa, aErr := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
			Name:              fmt.Sprintf("test-sa-%d", i),
			Description:       fmt.Sprintf("test service account %d", i),
			Scope:             scope,
			OrganizationID:    &org1.Metadata.ID,
			ProjectID:         projectID,
			CreatedBy:         "test user",
			OIDCTrustPolicies: trustPolicies,
		})
		require.Nil(t, aErr)

		serviceAccounts[i] = sa
	}

	// 1 service account in org2
	sa, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:              fmt.Sprintf("test-sa-%d", len(serviceAccounts)),
		Description:       fmt.Sprintf("test service account %d", len(serviceAccounts)),
		Scope:             models.OrganizationScope,
		OrganizationID:    &org2.Metadata.ID,
		CreatedBy:         "test user",
		OIDCTrustPolicies: trustPolicies,
	})
	require.Nil(t, err)
	serviceAccounts = append(serviceAccounts, sa)

	// 1 global service account
	sa, err = testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:              fmt.Sprintf("test-sa-%d", len(serviceAccounts)),
		Description:       fmt.Sprintf("test service account %d", len(serviceAccounts)),
		Scope:             models.GlobalScope,
		CreatedBy:         "test user",
		OIDCTrustPolicies: trustPolicies,
	})
	require.Nil(t, err)
	serviceAccounts = append(serviceAccounts, sa)

	// Arbitrarily create an agent so we can assign a service account to it.
	// This is only needed to test the agent ID filter hence why it's created here.
	createdAgent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		OrganizationID: &org1.Metadata.ID,
		Type:           models.OrganizationAgent,
		Name:           "test-agent",
		Description:    "test agent for service account test",
		CreatedBy:      "someone",
	})
	require.Nil(t, err)

	// Assign a service account to the agent.
	err = testClient.client.ServiceAccounts.AssignServiceAccountToAgent(ctx,
		serviceAccounts[0].Metadata.ID, createdAgent.Metadata.ID)
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode       errors.CodeType
		input                 *GetServiceAccountsInput
		name                  string
		expectServiceAccounts int
	}

	/*
		template test case:

		{
		name                        string
		input                       *GetServiceAccountsInput
		expectErrorCode             errors.CodeType
		expectServiceAccounts       int
		}
	*/

	testCases := []testCase{
		// nil input likely causes a nil pointer dereference in GetServiceAccounts, so don't try it.

		{
			name: "non-nil but mostly empty input",
			input: &GetServiceAccountsInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			},
			expectServiceAccounts: len(serviceAccounts),
		},

		{
			// If there were more filter fields, this would allow nothing through the filters.
			name: "fully-populated types, everything allowed through filters",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
				Filter: &ServiceAccountFilter{
					Search:            ptr.String(""),
					ServiceAccountIDs: []string{},
				},
			},
			expectServiceAccounts: len(serviceAccounts),
		},

		{
			name: "filter, search field, empty string",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					Search: ptr.String(""),
				},
			},
			expectServiceAccounts: len(serviceAccounts),
		},

		{
			name: "filter, search field, t",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					Search: ptr.String("t"),
				},
			},
			expectServiceAccounts: len(serviceAccounts),
		},

		{
			name: "filter, search field, test-sa-1",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					Search: ptr.String("test-sa-1"),
				},
			},
			expectServiceAccounts: 3,
		},

		{
			name: "filter, search field, test-sa-5",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					Search: ptr.String("test-sa-5"),
				},
			},
			expectServiceAccounts: 1,
		},

		{
			name: "filter, search field, bogus",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					Search: ptr.String("bogus"),
				},
			},
			expectServiceAccounts: 0,
		},

		{
			name: "filter, service account IDs, positive",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					ServiceAccountIDs: []string{
						serviceAccounts[0].Metadata.ID, serviceAccounts[1].Metadata.ID, serviceAccounts[3].Metadata.ID,
					},
				},
			},
			expectServiceAccounts: 3,
		},

		{
			name: "filter, service account IDs, non-existent",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					ServiceAccountIDs: []string{nonExistentID},
				},
			},
			expectServiceAccounts: 0,
		},

		{
			name: "filter, service account IDs, invalid ID",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					ServiceAccountIDs: []string{invalidID},
				},
			},
			expectErrorCode:       errors.EInternal,
			expectServiceAccounts: 0,
		},

		{
			name: "filter, organization ID, org scope only, positive",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					OrganizationID:       ptr.String(org1.Metadata.ID),
					ServiceAccountScopes: []models.ScopeType{models.OrganizationScope},
				},
			},
			expectServiceAccounts: 4,
		},

		{
			name: "filter, organization ID, any scope, positive",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					OrganizationID: ptr.String(org1.Metadata.ID),
				},
			},
			expectServiceAccounts: 10,
		},

		{
			name: "filter, organization ID, non-existent",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					OrganizationID: ptr.String(nonExistentID),
				},
			},
			expectServiceAccounts: 0,
		},

		{
			name: "filter, organization ID, invalid ID",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					OrganizationID: ptr.String(invalidID),
				},
			},
			expectErrorCode:       errors.EInternal,
			expectServiceAccounts: 0,
		},

		{
			name: "filter, project ID, project scope only, positive",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					ProjectID:            ptr.String(proj1a.Metadata.ID),
					ServiceAccountScopes: []models.ScopeType{models.ProjectScope},
				},
			},
			expectServiceAccounts: 3,
		},

		{
			name: "filter, project ID, any scope, positive",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					ProjectID: ptr.String(proj1a.Metadata.ID),
				},
			},
			expectServiceAccounts: 7,
		},

		{
			name: "filter, project ID, non-existent",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					ProjectID: ptr.String(nonExistentID),
				},
			},
			expectServiceAccounts: 0,
		},

		{
			name: "filter, project ID, invalid ID",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					ProjectID: ptr.String(invalidID),
				},
			},
			expectErrorCode:       errors.EInternal,
			expectServiceAccounts: 0,
		},

		{
			name: "filter, agent ID, positive",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					AgentID: ptr.String(createdAgent.Metadata.ID),
				},
			},
			expectServiceAccounts: 1,
		},

		{
			name: "filter, agent ID, non-existent",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					AgentID: ptr.String(nonExistentID),
				},
			},
			expectServiceAccounts: 0,
		},

		{
			name: "filter, agent ID, invalid ID",
			input: &GetServiceAccountsInput{
				Sort: ptrServiceAccountSortableField(ServiceAccountSortableFieldCreatedAtAsc),
				Filter: &ServiceAccountFilter{
					AgentID: ptr.String(invalidID),
				},
			},
			expectErrorCode:       errors.EInternal,
			expectServiceAccounts: 0,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualResult, err := testClient.client.ServiceAccounts.GetServiceAccounts(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			// If there was no error, check the results.
			if err == nil {

				// Never returns nil if error is nil.
				require.NotNil(t, actualResult.PageInfo)
				assert.NotNil(t, actualResult.ServiceAccounts)
				actualServiceAccounts := actualResult.ServiceAccounts

				// Check the service accounts result by comparing a list of the service account IDs.
				actualServiceAccountIDs := []string{}
				for _, serviceAccount := range actualServiceAccounts {
					actualServiceAccountIDs = append(actualServiceAccountIDs, serviceAccount.Metadata.ID)
				}

				assert.Equal(t, test.expectServiceAccounts, len(actualServiceAccountIDs))
			}
		})
	}
}

func TestGetServiceAccountsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	proj1b, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	trustPolicies := []models.OIDCTrustPolicy{
		{
			Issuer:      "issuer-0",
			BoundClaims: map[string]string{"bc1-k1": "bc1-v1", "bc1-k2": "bc1-v2"},
		},
		{
			Issuer:      "issuer-1",
			BoundClaims: map[string]string{"bc2-k1": "bc2-v1", "bc2-k2": "bc2-v2"},
		},
	}

	resourceCount := 10
	serviceAccounts := make([]*models.ServiceAccount, resourceCount)
	for i := 0; i < len(serviceAccounts); i++ {
		// a mix of org-scope, proj1a, and proj1b
		var scope models.ScopeType
		var projectID *string
		switch i % 3 {
		case 0: // i=0, 3, 6, 9
			scope = models.OrganizationScope
		case 1: // i=1, 4, 7
			scope = models.ProjectScope
			projectID = &proj1a.Metadata.ID
		case 2: // i=2, 5, 8
			scope = models.ProjectScope
			projectID = &proj1b.Metadata.ID
		}

		sa, aErr := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
			Name:              fmt.Sprintf("test-sa-%d", i),
			Description:       fmt.Sprintf("test service account %d", i),
			Scope:             scope,
			OrganizationID:    &org1.Metadata.ID,
			ProjectID:         projectID,
			CreatedBy:         "test user",
			OIDCTrustPolicies: trustPolicies,
		})
		require.Nil(t, aErr)

		serviceAccounts[i] = sa
	}

	sortableFields := []sortableField{
		ServiceAccountSortableFieldCreatedAtAsc,
		ServiceAccountSortableFieldCreatedAtDesc,
		ServiceAccountSortableFieldUpdatedAtAsc,
		ServiceAccountSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields,
		func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo,
			[]pagination.CursorPaginatable, error) {
			sortBy := ServiceAccountSortableField(sortByField.getValue())

			result, err := testClient.client.ServiceAccounts.GetServiceAccounts(ctx, &GetServiceAccountsInput{
				Sort:              &sortBy,
				PaginationOptions: paginationOptions,
			})
			if err != nil {
				return nil, nil, err
			}

			resources := []pagination.CursorPaginatable{}
			for _, resource := range result.ServiceAccounts {
				resources = append(resources, resource)
			}

			return result.PageInfo, resources, nil
		},
	)
}

func TestCreateServiceAccount(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	proj1, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		toCreate        *models.ServiceAccount
		expectCreated   *models.ServiceAccount
		expectErrorCode errors.CodeType
		name            string
	}

	now := currentTime()
	testCases := []testCase{
		{
			name: "positive project service account nearly empty",
			toCreate: &models.ServiceAccount{
				Name:           "positive-create-project-service-account-nearly-empty",
				OrganizationID: &org1.Metadata.ID,
				ProjectID:      &proj1.Metadata.ID,
				Scope:          models.ProjectScope,
			},
			expectCreated: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
					PRN: models.ServiceAccountResource.BuildPRN(org1.Name, proj1.Name,
						"positive-create-project-service-account-nearly-empty"),
				},
				Name:              "positive-create-project-service-account-nearly-empty",
				OrganizationID:    &org1.Metadata.ID,
				ProjectID:         &proj1.Metadata.ID,
				Scope:             models.ProjectScope,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{},
			},
		},

		{
			name: "positive project service account full",
			toCreate: &models.ServiceAccount{
				Name:              "positive-create-project-service-account-full",
				Description:       "positive create service account",
				OrganizationID:    &org1.Metadata.ID,
				ProjectID:         &proj1.Metadata.ID,
				CreatedBy:         "creator-of-service-accounts",
				Scope:             models.ProjectScope,
				OIDCTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
			},
			expectCreated: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
					PRN: models.ServiceAccountResource.BuildPRN(org1.Name, proj1.Name,
						"positive-create-project-service-account-full"),
				},
				Name:              "positive-create-project-service-account-full",
				Description:       "positive create service account",
				OrganizationID:    &org1.Metadata.ID,
				ProjectID:         &proj1.Metadata.ID,
				CreatedBy:         "creator-of-service-accounts",
				Scope:             models.ProjectScope,
				OIDCTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
			},
		},

		{
			name: "positive organization service account nearly empty",
			toCreate: &models.ServiceAccount{
				Name:           "positive-create-organization-service-account-nearly-empty",
				OrganizationID: &org1.Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectCreated: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
					PRN: models.ServiceAccountResource.BuildPRN(org1.Name,
						"positive-create-organization-service-account-nearly-empty"),
				},
				Name:              "positive-create-organization-service-account-nearly-empty",
				OrganizationID:    &org1.Metadata.ID,
				Scope:             models.OrganizationScope,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{},
			},
		},

		{
			name: "positive organization service account full",
			toCreate: &models.ServiceAccount{
				Name:              "positive-create-organization-service-account-full",
				Description:       "positive create service account",
				OrganizationID:    &org1.Metadata.ID,
				CreatedBy:         "creator-of-service-accounts",
				Scope:             models.OrganizationScope,
				OIDCTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
			},
			expectCreated: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
					PRN: models.ServiceAccountResource.BuildPRN(org1.Name,
						"positive-create-organization-service-account-full"),
				},
				Name:              "positive-create-organization-service-account-full",
				Description:       "positive create service account",
				OrganizationID:    &org1.Metadata.ID,
				CreatedBy:         "creator-of-service-accounts",
				Scope:             models.OrganizationScope,
				OIDCTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
			},
		},

		{
			name: "positive global service account full",
			toCreate: &models.ServiceAccount{
				Name:              "positive-create-global-service-account-full",
				Description:       "positive create global service account",
				CreatedBy:         "creator-of-service-accounts",
				Scope:             models.GlobalScope,
				OIDCTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
			},
			expectCreated: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
					PRN:               models.ServiceAccountResource.BuildPRN("positive-create-global-service-account-full"),
				},
				Name:              "positive-create-global-service-account-full",
				Description:       "positive create global service account",
				CreatedBy:         "creator-of-service-accounts",
				Scope:             models.GlobalScope,
				OIDCTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
			},
		},

		{
			name: "duplicate global service account",
			toCreate: &models.ServiceAccount{
				Name:              "positive-create-global-service-account-full", // duplicates earlier test case
				Description:       "duplicate global service account",
				CreatedBy:         "creator-of-service-accounts",
				Scope:             models.GlobalScope,
				OIDCTrustPolicies: standardWarmupOIDCTrustPoliciesForServiceAccounts,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "duplicate name in same project",
			toCreate: &models.ServiceAccount{
				Name:           "positive-create-project-service-account-nearly-empty", // duplicates earlier test case
				OrganizationID: &org1.Metadata.ID,
				ProjectID:      &proj1.Metadata.ID,
				Scope:          models.ProjectScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "duplicate name in same organization",
			toCreate: &models.ServiceAccount{
				Name:           "positive-create-organization-service-account-nearly-empty", // duplicates earlier test case
				OrganizationID: &org1.Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "non-existent project ID",
			toCreate: &models.ServiceAccount{
				Name:           "non-existent-project-id",
				OrganizationID: &org1.Metadata.ID,
				ProjectID:      ptr.String(nonExistentID),
				Scope:          models.ProjectScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "non-existent organization ID",
			toCreate: &models.ServiceAccount{
				Name:           "non-existent-organization-id",
				OrganizationID: ptr.String(nonExistentID),
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "defective project ID",
			toCreate: &models.ServiceAccount{
				Name:           "defective-project-id",
				OrganizationID: &org1.Metadata.ID,
				ProjectID:      ptr.String(invalidID),
				Scope:          models.ProjectScope,
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "defective organization ID",
			toCreate: &models.ServiceAccount{
				Name:           "defective-organization-id",
				OrganizationID: ptr.String(invalidID),
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualCreated, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, test.toCreate)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectCreated != nil {
				// the positive case
				require.NotNil(t, actualCreated)

				// The creation process must set the creation and last updated timestamps
				// between when the test case was created and when it the result is checked.
				whenCreated := test.expectCreated.Metadata.CreationTimestamp
				now := time.Now()

				compareServiceAccounts(t, test.expectCreated, actualCreated, false, &timeBounds{
					createLow:  whenCreated,
					createHigh: &now,
					updateLow:  whenCreated,
					updateHigh: &now,
				})
			} else {
				// the negative and defective cases
				assert.Nil(t, actualCreated)
			}
		})
	}
}

func TestUpdateServiceAccount(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdLow := time.Now()
	warmupOutput, err := createWarmupServiceAccounts(ctx, testClient, serviceAccountWarmupInput{
		standardWarmupOrganizationsForServiceAccounts,
		standardWarmupServiceAccounts,
		standardWarmupOIDCTrustPoliciesForServiceAccounts,
	})
	require.Nil(t, err)
	createdHigh := time.Now()

	type testCase struct {
		expectErrorCode      errors.CodeType
		expectServiceAccount *models.ServiceAccount
		toUpdate             *models.ServiceAccount
		name                 string
	}

	positiveServiceAccount := warmupOutput.serviceAccounts[0]
	now := time.Now()
	testCases := []testCase{
		{
			name: "positive",
			toUpdate: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID:      positiveServiceAccount.Metadata.ID,
					Version: positiveServiceAccount.Metadata.Version,
				},
				Description: "updated description",
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:      "new-issuer",
						BoundClaims: map[string]string{"new-key": "new-value"},
					},
				},
			},
			// Only the description and trust policies get updated.
			expectServiceAccount: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID:                   positiveServiceAccount.Metadata.ID,
					Version:              positiveServiceAccount.Metadata.Version + 1,
					CreationTimestamp:    positiveServiceAccount.Metadata.CreationTimestamp,
					LastUpdatedTimestamp: &now,
					PRN:                  positiveServiceAccount.Metadata.PRN,
				},
				Name:           positiveServiceAccount.Name,
				Description:    "updated description",
				OrganizationID: positiveServiceAccount.OrganizationID,
				CreatedBy:      positiveServiceAccount.CreatedBy,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:      "new-issuer",
						BoundClaims: map[string]string{"new-key": "new-value"},
					},
				},
				Scope: positiveServiceAccount.Scope,
			},
		},

		{
			name: "negative, non-existent service account ID",
			toUpdate: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: positiveServiceAccount.Metadata.Version,
				},
			},
			expectErrorCode: errors.EOptimisticLock,
		},

		{
			name: "defective-id",
			toUpdate: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: positiveServiceAccount.Metadata.Version,
				},
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualServiceAccount, err := testClient.client.ServiceAccounts.UpdateServiceAccount(ctx, test.toUpdate)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			now := time.Now()
			if test.expectServiceAccount != nil {
				require.NotNil(t, actualServiceAccount)
				compareServiceAccounts(t, test.expectServiceAccount, actualServiceAccount, false, &timeBounds{
					createLow:  &createdLow,
					createHigh: &createdHigh,
					updateLow:  &createdLow,
					updateHigh: &now,
				})
			} else {
				assert.Nil(t, actualServiceAccount)
			}
		})
	}
}

func TestDeleteServiceAccount(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupServiceAccounts(ctx, testClient, serviceAccountWarmupInput{
		standardWarmupOrganizationsForServiceAccounts,
		standardWarmupServiceAccounts,
		standardWarmupOIDCTrustPoliciesForServiceAccounts,
	})
	require.Nil(t, err)

	type testCase struct {
		toDelete        *models.ServiceAccount
		expectErrorCode errors.CodeType
		name            string
	}

	positiveServiceAccount := warmupItems.serviceAccounts[0]

	testCases := []testCase{
		{
			name: "positive",
			toDelete: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID:      positiveServiceAccount.Metadata.ID,
					Version: positiveServiceAccount.Metadata.Version,
				},
			},
		},

		{
			name: "negative, non-existent ID",
			toDelete: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID: nonExistentID,
				},
				Description: "looking for a non-existent ID",
			},
			expectErrorCode: errors.EOptimisticLock,
		},

		{
			name: "defective-id",
			toDelete: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID: invalidID,
				},
				Description: "looking for a defective ID",
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.ServiceAccounts.DeleteServiceAccount(ctx, test.toDelete)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestAssignServiceAccountToAgent(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupServiceAccounts(ctx, testClient, serviceAccountWarmupInput{
		standardWarmupOrganizationsForServiceAccounts,
		standardWarmupServiceAccounts,
		standardWarmupOIDCTrustPoliciesForServiceAccounts,
	})
	require.Nil(t, err)

	createdAgent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name:        "agent-for-assigning-to-service-account",
		Description: "agent for assigning to service account",
		CreatedBy:   "someone",
		Type:        models.OrganizationAgent,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		serviceAccountID string
		agentID          string
	}

	positiveServiceAccount := warmupItems.serviceAccounts[0]

	testCases := []testCase{
		{
			name:             "assign service account to agent, positive",
			serviceAccountID: positiveServiceAccount.Metadata.ID,
			agentID:          createdAgent.Metadata.ID,
		},
		{
			name:             "duplicate assignment, negative",
			serviceAccountID: positiveServiceAccount.Metadata.ID,
			agentID:          createdAgent.Metadata.ID,
			expectErrorCode:  errors.EConflict,
		},
		{
			name:             "non-existent service account ID, negative",
			serviceAccountID: nonExistentID,
			agentID:          createdAgent.Metadata.ID,
			expectErrorCode:  errors.EInternal,
		},
		{
			name:             "defective service account ID, negative",
			serviceAccountID: invalidID,
			agentID:          createdAgent.Metadata.ID,
			expectErrorCode:  errors.EInternal,
		},
		{
			name:             "defective agent ID, negative",
			serviceAccountID: positiveServiceAccount.Metadata.ID,
			agentID:          invalidID,
			expectErrorCode:  errors.EInternal,
		},
		{
			name:             "assign service account to agent, defective",
			serviceAccountID: positiveServiceAccount.Metadata.ID,
			agentID:          invalidID,
			expectErrorCode:  errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.ServiceAccounts.AssignServiceAccountToAgent(ctx, test.serviceAccountID, test.agentID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestUnassignServiceAccountFromAgent(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupServiceAccounts(ctx, testClient, serviceAccountWarmupInput{
		standardWarmupOrganizationsForServiceAccounts,
		standardWarmupServiceAccounts,
		standardWarmupOIDCTrustPoliciesForServiceAccounts,
	})
	require.Nil(t, err)

	createdAgent, err := testClient.client.Agents.CreateAgent(ctx, &models.Agent{
		Name:        "agent-for-assigning-to-service-account",
		Description: "agent for assigning to service account",
		CreatedBy:   "someone",
		Type:        models.OrganizationAgent,
	})
	require.Nil(t, err)

	err = testClient.client.ServiceAccounts.AssignServiceAccountToAgent(ctx, warmupItems.serviceAccounts[0].Metadata.ID, createdAgent.Metadata.ID)
	require.Nil(t, err)

	positiveServiceAccount := warmupItems.serviceAccounts[0]

	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		serviceAccountID string
		agentID          string
	}

	testCases := []testCase{
		{
			name:             "unassign service account from agent, positive",
			serviceAccountID: positiveServiceAccount.Metadata.ID,
			agentID:          createdAgent.Metadata.ID,
		},
		{
			name:             "unassign service account from agent, defective",
			serviceAccountID: invalidID,
			agentID:          createdAgent.Metadata.ID,
			expectErrorCode:  errors.EInternal,
		},
		{
			name:             "unassign service account from agent, defective",
			serviceAccountID: positiveServiceAccount.Metadata.ID,
			agentID:          invalidID,
			expectErrorCode:  errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.ServiceAccounts.UnassignServiceAccountFromAgent(ctx, test.serviceAccountID, test.agentID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

//////////////////////////////////////////////////////////////////////////////

// Common utility structures and functions:

// Standard warmup organization(s) for tests in this module:
var standardWarmupOrganizationsForServiceAccounts = []models.Organization{
	{
		Name:        "top-level-organization-0-for-service-accounts",
		Description: "top level organization 0 for testing service account functions",
		CreatedBy:   "someone-g0",
	},
}

// Standard service account(s) for tests in this module:
// The create function will convert the organization name to organization ID.
var standardWarmupServiceAccounts = []models.ServiceAccount{
	{
		Name:              "1-service-account-0",
		Description:       "service account 0",
		OrganizationID:    ptr.String("top-level-organization-0-for-service-accounts"),
		CreatedBy:         "someone-sa0",
		Scope:             models.OrganizationScope,
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
	{
		Name:              "1-service-account-1",
		Description:       "service account 1",
		OrganizationID:    ptr.String("top-level-organization-0-for-service-accounts"),
		CreatedBy:         "someone-sa1",
		Scope:             models.OrganizationScope,
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
	{
		Name:              "2-service-account-2",
		Description:       "service account 2",
		OrganizationID:    ptr.String("top-level-organization-0-for-service-accounts"),
		CreatedBy:         "someone-sa2",
		Scope:             models.OrganizationScope,
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
	{
		Name:              "2-service-account-3",
		Description:       "service account 3",
		CreatedBy:         "someone-sa3",
		Scope:             models.GlobalScope,
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
	{
		Name:              "5-service-account-4",
		Description:       "service account 4",
		CreatedBy:         "someone-sa4",
		Scope:             models.GlobalScope,
		OIDCTrustPolicies: []models.OIDCTrustPolicy{},
	},
}

// Standard OIDC trust policy/policies for tests in this module:
var standardWarmupOIDCTrustPoliciesForServiceAccounts = []models.OIDCTrustPolicy{
	{
		Issuer:      "issuer-0",
		BoundClaims: map[string]string{"bc1-k1": "bc1-v1", "bc1-k2": "bc1-v2"},
	},
	{
		Issuer:      "issuer-1",
		BoundClaims: map[string]string{"bc2-k1": "bc2-v1", "bc2-k2": "bc2-v2"},
	},
}

// createWarmupServiceAccounts creates some warmup service accounts for a test
// The warmup service accounts to create can be standard or otherwise.
func createWarmupServiceAccounts(
	ctx context.Context,
	testClient *testClient,
	input serviceAccountWarmupInput,
) (*serviceAccountWarmupOutput, error,
) {
	// It is necessary to create at least one organization
	// in order to provide the necessary IDs for the service accounts.

	resultOrganizations, organizationName2ID, err := createInitialOrganizations(ctx, testClient, input.organizations)
	if err != nil {
		return nil, err
	}

	// It is necessary to add the OIDC trust policies to the service account before
	// creating the service account in the DB.
	for _, sa := range input.serviceAccounts {
		for _, tp := range input.oidcTrustPolicies {
			sa.OIDCTrustPolicies = append(sa.OIDCTrustPolicies, models.OIDCTrustPolicy{
				Issuer:      tp.Issuer + "-for-" + sa.Name,
				BoundClaims: tp.BoundClaims,
			})
		}
	}

	resultServiceAccounts, _, err := createInitialServiceAccounts(ctx, testClient, organizationName2ID, input.serviceAccounts)
	if err != nil {
		return nil, err
	}

	return &serviceAccountWarmupOutput{
		organizations:   resultOrganizations,
		serviceAccounts: resultServiceAccounts,
	}, nil
}

// createInitialServiceAccounts creates some service accounts for a test.
func createInitialServiceAccounts(ctx context.Context, testClient *testClient, orgMap map[string]string,
	toCreate []models.ServiceAccount) ([]models.ServiceAccount, map[string]string, error) {
	result := []models.ServiceAccount{}
	serviceAccountName2ID := make(map[string]string)

	for _, input := range toCreate {
		createCopy := &models.ServiceAccount{
			Name:              input.Name,
			Description:       input.Description,
			Scope:             input.Scope,
			CreatedBy:         input.CreatedBy,
			OIDCTrustPolicies: input.OIDCTrustPolicies,
		}

		if input.OrganizationID != nil {
			createCopy.OrganizationID = ptr.String(orgMap[*input.OrganizationID])
		}

		created, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, createCopy)

		if err != nil {
			return nil, nil, err
		}

		result = append(result, *created)
		serviceAccountName2ID[created.Name] = created.Metadata.ID
	}

	return result, serviceAccountName2ID, nil
}

func ptrServiceAccountSortableField(arg ServiceAccountSortableField) *ServiceAccountSortableField {
	return &arg
}

func (sais serviceAccountInfoIDSlice) Len() int {
	return len(sais)
}

func (sais serviceAccountInfoIDSlice) Swap(i, j int) {
	sais[i], sais[j] = sais[j], sais[i]
}

func (sais serviceAccountInfoIDSlice) Less(i, j int) bool {
	return sais[i].serviceAccountID < sais[j].serviceAccountID
}

func (sais serviceAccountInfoCreateSlice) Len() int {
	return len(sais)
}

func (sais serviceAccountInfoCreateSlice) Swap(i, j int) {
	sais[i], sais[j] = sais[j], sais[i]
}

func (sais serviceAccountInfoCreateSlice) Less(i, j int) bool {
	return sais[i].createTime.Before(sais[j].createTime)
}

func (sais serviceAccountInfoUpdateSlice) Len() int {
	return len(sais)
}

func (sais serviceAccountInfoUpdateSlice) Swap(i, j int) {
	sais[i], sais[j] = sais[j], sais[i]
}

func (sais serviceAccountInfoUpdateSlice) Less(i, j int) bool {
	return sais[i].updateTime.Before(sais[j].updateTime)
}

func (sais serviceAccountInfoNameSlice) Len() int {
	return len(sais)
}

func (sais serviceAccountInfoNameSlice) Swap(i, j int) {
	sais[i], sais[j] = sais[j], sais[i]
}

func (sais serviceAccountInfoNameSlice) Less(i, j int) bool {
	return sais[i].name < sais[j].name
}

// serviceAccountInfoFromServiceAccounts returns a slice of serviceAccountInfo, not necessarily sorted in any order.
func serviceAccountInfoFromServiceAccounts(serviceAccounts []models.ServiceAccount) []serviceAccountInfo {
	result := []serviceAccountInfo{}

	for _, serviceAccount := range serviceAccounts {
		result = append(result, serviceAccountInfo{
			createTime:       *serviceAccount.Metadata.CreationTimestamp,
			updateTime:       *serviceAccount.Metadata.LastUpdatedTimestamp,
			serviceAccountID: serviceAccount.Metadata.ID,
			name:             serviceAccount.Name,
		})
	}

	return result
}

// serviceAccountIDsFromServiceAccountInfos preserves order
func serviceAccountIDsFromServiceAccountInfos(serviceAccountInfos []serviceAccountInfo) []string {
	result := []string{}
	for _, serviceAccountInfo := range serviceAccountInfos {
		result = append(result, serviceAccountInfo.serviceAccountID)
	}
	return result
}

// compareServiceAccounts compares two service account objects, including bounds for creation and updated times.
// If times is nil, it compares the exact metadata timestamps.
func compareServiceAccounts(t *testing.T, expected, actual *models.ServiceAccount,
	checkID bool, times *timeBounds,
) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.Description, actual.Description)
	assert.Equal(t, expected.OrganizationID, actual.OrganizationID)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
	assert.Equal(t, expected.Scope, actual.Scope)
	compareOIDCTrustPolicies(t, expected.OIDCTrustPolicies, actual.OIDCTrustPolicies)

	if checkID {
		assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	}
	assert.Equal(t, expected.Metadata.Version, actual.Metadata.Version)
	assert.Equal(t, expected.Metadata.PRN, actual.Metadata.PRN)

	// Compare timestamps.
	if times != nil {
		compareTime(t, times.createLow, times.createHigh, actual.Metadata.CreationTimestamp)
		compareTime(t, times.updateLow, times.updateHigh, actual.Metadata.LastUpdatedTimestamp)
	} else {
		assert.Equal(t, expected.Metadata.CreationTimestamp, actual.Metadata.CreationTimestamp)
		assert.Equal(t, expected.Metadata.LastUpdatedTimestamp, actual.Metadata.LastUpdatedTimestamp)
	}
}

// compareOIDCTrustPolicies compares two slices of OIDC trust policies,
func compareOIDCTrustPolicies(t *testing.T, expected, actual []models.OIDCTrustPolicy) {
	require.Equal(t, len(expected), len(actual))

	for ix := range expected {
		assert.Equal(t, expected[ix].Issuer, actual[ix].Issuer)
		assert.Equal(t, expected[ix].BoundClaims, actual[ix].BoundClaims)
	}
}
