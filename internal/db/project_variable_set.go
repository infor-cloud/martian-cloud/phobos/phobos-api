package db

//go:generate go tool mockery --name ProjectVariableSets --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ProjectVariableSets encapsulates the logic to access ProjectVariableSets from the database
type ProjectVariableSets interface {
	GetProjectVariableSetByID(ctx context.Context, id string) (*models.ProjectVariableSet, error)
	GetProjectVariableSetByPRN(ctx context.Context, prn string) (*models.ProjectVariableSet, error)
	GetProjectVariableSets(ctx context.Context, input *GetProjectVariableSetsInput) (*ProjectVariableSetsResult, error)
	CreateProjectVariableSet(ctx context.Context, variableSet *models.ProjectVariableSet) (*models.ProjectVariableSet, error)
	UpdateProjectVariableSet(ctx context.Context, variableSet *models.ProjectVariableSet) (*models.ProjectVariableSet, error)
	DeleteProjectVariableSet(ctx context.Context, variableSet *models.ProjectVariableSet) error
	AddProjectVariablesToSet(ctx context.Context, variableSet *models.ProjectVariableSet, variables []*models.ProjectVariable) error
}

// ProjectVariableSetFilter contains the supported fields for filtering ProjectVariableSet resources
type ProjectVariableSetFilter struct {
	ProjectID                 *string
	Latest                    *bool
	VariableID                *string
	VariableSetRevisionSearch *string
	VariableSetIDs            []string
}

// ProjectVariableSetSortableField represents the fields that a project can be sorted by
type ProjectVariableSetSortableField string

// ProjectVariableSetSortableField constants
const (
	ProjectVariableSetSortableFieldCreatedAtAsc  ProjectVariableSetSortableField = "CREATED_AT_ASC"
	ProjectVariableSetSortableFieldCreatedAtDesc ProjectVariableSetSortableField = "CREATED_AT_DESC"
)

func (pvs ProjectVariableSetSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch pvs {
	case ProjectVariableSetSortableFieldCreatedAtAsc, ProjectVariableSetSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "project_variable_sets", Col: "created_at"}
	default:
		return nil
	}
}

func (pvs ProjectVariableSetSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(pvs), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetProjectVariableSetsInput is the input for listing projectVariableSets
type GetProjectVariableSetsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ProjectVariableSetSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ProjectVariableSetFilter
}

// validate validates the input for GetProjectVariableSetsInput
func (i *GetProjectVariableSetsInput) validate() error {
	if i.Filter != nil {
		if i.Filter.VariableSetRevisionSearch != nil && len(*i.Filter.VariableSetRevisionSearch) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// ProjectVariableSetsResult contains the response data and page information
type ProjectVariableSetsResult struct {
	PageInfo            *pagination.PageInfo
	ProjectVariableSets []*models.ProjectVariableSet
}

var projectVariableSetFieldList = append(metadataFieldList, "project_id", "revision", "latest")

type projectVariableSets struct {
	dbClient *Client
}

// NewProjectVariableSets returns an instance of the ProjectVariableSets interface
func NewProjectVariableSets(dbClient *Client) ProjectVariableSets {
	return &projectVariableSets{dbClient: dbClient}
}

func (p *projectVariableSets) GetProjectVariableSetByID(ctx context.Context, id string) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectVariableSetByID")
	defer span.End()

	return p.getProjectVariableSet(ctx, goqu.Ex{"project_variable_sets.id": id})
}

func (p *projectVariableSets) GetProjectVariableSetByPRN(ctx context.Context, prn string) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectVariableSetByPRN")
	defer span.End()

	path, err := models.ProjectVariableSetResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	if len(parts) != 3 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getProjectVariableSet(ctx, goqu.Ex{
		"organizations.name":             parts[0],
		"projects.name":                  parts[1],
		"project_variable_sets.revision": parts[2],
	})
}

func (p *projectVariableSets) GetProjectVariableSets(ctx context.Context, input *GetProjectVariableSetsInput) (*ProjectVariableSetsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectVariableSets")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("project_variable_sets.project_id").Eq(*input.Filter.ProjectID))
		}

		if input.Filter.Latest != nil {
			ex = ex.Append(goqu.I("project_variable_sets.latest").Eq(*input.Filter.Latest))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.VariableSetIDs) > 0 {
			ex = ex.Append(goqu.I("project_variable_sets.id").In(input.Filter.VariableSetIDs))
		}

		if input.Filter.VariableID != nil {
			ex = ex.Append(goqu.I("project_variable_set_variable_relation.variable_id").Eq(*input.Filter.VariableID))
		}

		if input.Filter.VariableSetRevisionSearch != nil && *input.Filter.VariableSetRevisionSearch != "" {
			ex = ex.Append(goqu.I("project_variable_sets.revision").Like(*input.Filter.VariableSetRevisionSearch + "%"))
		}
	}

	query := dialect.From(goqu.T("project_variable_sets")).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variable_sets.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(ex)

	if input.Filter != nil && input.Filter.VariableID != nil {
		query = query.LeftJoin(goqu.T("project_variable_set_variable_relation"), goqu.On(goqu.Ex{"project_variable_sets.id": goqu.I("project_variable_set_variable_relation.variable_set_id")}))
	}

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "project_variable_sets", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.ProjectVariableSet{}
	for rows.Next() {
		item, err := scanProjectVariableSet(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &ProjectVariableSetsResult{
		PageInfo:            rows.GetPageInfo(),
		ProjectVariableSets: results,
	}

	return result, nil
}

func (p *projectVariableSets) AddProjectVariablesToSet(ctx context.Context, variableSet *models.ProjectVariableSet, variables []*models.ProjectVariable) error {
	ctx, span := tracer.Start(ctx, "db.AddProjectVariablesToSet")
	defer span.End()

	records := []goqu.Record{}
	for _, variable := range variables {
		records = append(records, goqu.Record{
			"variable_set_id": variableSet.Metadata.ID,
			"variable_id":     variable.Metadata.ID,
		})
	}

	sql, args, err := dialect.Insert("project_variable_set_variable_relation").
		Prepared(true).
		Rows(records).ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL")
	}

	if _, err = p.dbClient.getConnection(ctx).Exec(ctx, sql, args...); err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_project_variable_set_variable_relation_variable_set_id":
					return errors.New("variable set does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_project_variable_set_variable_relation_variable_id":
					return errors.New("variable does not exist", errors.WithErrorCode(errors.ENotFound))
				}
			}
			return errors.Wrap(err, "failed to add variable to project variable set")
		}
	}

	return nil
}

func (p *projectVariableSets) CreateProjectVariableSet(ctx context.Context, variableSet *models.ProjectVariableSet) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "db.CreateProjectVariableSet")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("project_variable_sets").
		Prepared(true).
		With("project_variable_sets",
			dialect.Insert("project_variable_sets").Rows(
				goqu.Record{
					"id":         newResourceID(),
					"version":    initialResourceVersion,
					"created_at": timestamp,
					"updated_at": timestamp,
					"project_id": variableSet.ProjectID,
					"revision":   variableSet.Revision,
					"latest":     variableSet.Latest,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variable_sets.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdVariableSet, err := scanProjectVariableSet(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("project variable set with revision %s already exists", variableSet.Revision, errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_project_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdVariableSet, nil
}

func (p *projectVariableSets) UpdateProjectVariableSet(ctx context.Context, variableSet *models.ProjectVariableSet) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateProjectVariableSet")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("project_variable_sets").
		Prepared(true).
		With("project_variable_sets",
			dialect.Update("project_variable_sets").
				Set(goqu.Record{
					"version":    goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at": timestamp,
					"latest":     variableSet.Latest,
				}).
				Where(goqu.Ex{"id": variableSet.Metadata.ID, "version": variableSet.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variable_sets.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedVariableSet, err := scanProjectVariableSet(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedVariableSet, nil
}

func (p *projectVariableSets) DeleteProjectVariableSet(ctx context.Context, variableSet *models.ProjectVariableSet) error {
	ctx, span := tracer.Start(ctx, "db.DeleteProjectVariableSet")
	defer span.End()

	sql, args, err := dialect.From("project_variable_sets").
		Prepared(true).
		With("project_variable_sets",
			dialect.Delete("project_variable_sets").
				Where(goqu.Ex{"id": variableSet.Metadata.ID, "version": variableSet.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variable_sets.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanProjectVariableSet(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *projectVariableSets) getProjectVariableSet(ctx context.Context, exp exp.Expression) (*models.ProjectVariableSet, error) {
	query := dialect.From(goqu.T("project_variable_sets")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variable_sets.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	project, err := scanProjectVariableSet(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return project, nil
}

func (*projectVariableSets) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range projectVariableSetFieldList {
		selectFields = append(selectFields, fmt.Sprintf("project_variable_sets.%s", field))
	}

	selectFields = append(selectFields, "projects.name")
	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanProjectVariableSet(row scanner) (*models.ProjectVariableSet, error) {
	var organizationName string
	var projectName string

	variableSet := &models.ProjectVariableSet{}

	fields := []interface{}{
		&variableSet.Metadata.ID,
		&variableSet.Metadata.CreationTimestamp,
		&variableSet.Metadata.LastUpdatedTimestamp,
		&variableSet.Metadata.Version,
		&variableSet.ProjectID,
		&variableSet.Revision,
		&variableSet.Latest,
		&projectName,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	variableSet.Metadata.PRN = models.ProjectVariableSetResource.BuildPRN(organizationName, projectName, variableSet.Revision)

	return variableSet, nil
}
