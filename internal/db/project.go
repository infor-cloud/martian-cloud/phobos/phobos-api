package db

//go:generate go tool mockery --name Projects --inpackage --case underscore

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Projects encapsulates the logic to access Projects from the database
type Projects interface {
	GetProjectByID(ctx context.Context, id string) (*models.Project, error)
	GetProjectByPRN(ctx context.Context, prn string) (*models.Project, error)
	GetProjects(ctx context.Context, input *GetProjectsInput) (*ProjectsResult, error)
	CreateProject(ctx context.Context, proj *models.Project) (*models.Project, error)
	UpdateProject(ctx context.Context, proj *models.Project) (*models.Project, error)
	DeleteProject(ctx context.Context, proj *models.Project) error
}

// ProjectFilter contains the supported fields for filtering Project resources
type ProjectFilter struct {
	UserMemberID           *string
	ServiceAccountMemberID *string
	OrgID                  *string
	Search                 *string
	ProjectIDs             []string
}

// ProjectSortableField represents the fields that a project can be sorted by
type ProjectSortableField string

// ProjectSortableField constants
const (
	ProjectSortableFieldUpdatedAtAsc  ProjectSortableField = "UPDATED_AT_ASC"
	ProjectSortableFieldUpdatedAtDesc ProjectSortableField = "UPDATED_AT_DESC"
)

func (os ProjectSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case ProjectSortableFieldUpdatedAtAsc, ProjectSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "projects", Col: "updated_at"}
	default:
		return nil
	}
}

func (os ProjectSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetProjectsInput is the input for listing projects
type GetProjectsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ProjectSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ProjectFilter
}

// validate checks the input for GetProjectsInput
func (i *GetProjectsInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// ProjectsResult contains the response data and page information
type ProjectsResult struct {
	PageInfo *pagination.PageInfo
	Projects []models.Project
}

var projectFieldList = append(metadataFieldList, "name", "description", "created_by", "org_id")

type projects struct {
	dbClient *Client
}

// NewProjects returns an instance of the Projects interface
func NewProjects(dbClient *Client) Projects {
	return &projects{dbClient: dbClient}
}

func (o *projects) GetProjectByID(ctx context.Context, id string) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectByID")
	defer span.End()

	return o.getProject(ctx, goqu.Ex{"projects.id": id})
}

func (o *projects) GetProjectByPRN(ctx context.Context, prn string) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectByPRN")
	defer span.End()

	path, err := models.ProjectResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	if len(parts) != 2 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return o.getProject(ctx, goqu.Ex{
		"organizations.name": parts[0],
		"projects.name":      parts[1],
	})
}

func (o *projects) GetProjects(ctx context.Context, input *GetProjectsInput) (*ProjectsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjects")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.OrgID != nil {
			ex = ex.Append(goqu.I("projects.org_id").Eq(*input.Filter.OrgID))
		}

		if input.Filter.UserMemberID != nil {
			ex = ex.Append(projectMembershipExpressionBuilder{
				userID: input.Filter.UserMemberID,
			}.build())
		}

		if input.Filter.ServiceAccountMemberID != nil {
			ex = ex.Append(projectMembershipExpressionBuilder{
				serviceAccountID: input.Filter.ServiceAccountMemberID,
			}.build())
		}

		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("projects.name").ILike("%" + *input.Filter.Search + "%"))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.ProjectIDs) > 0 {
			ex = ex.Append(goqu.I("projects.id").In(input.Filter.ProjectIDs))
		}
	}

	query := dialect.From(goqu.T("projects")).
		Select(o.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "projects", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, o.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Project{}
	for rows.Next() {
		item, err := scanProject(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &ProjectsResult{
		PageInfo: rows.GetPageInfo(),
		Projects: results,
	}

	return result, nil
}

func (o *projects) CreateProject(ctx context.Context, proj *models.Project) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "db.CreateProject")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("projects").
		Prepared(true).
		With("projects",
			dialect.Insert("projects").Rows(
				goqu.Record{
					"id":          newResourceID(),
					"version":     initialResourceVersion,
					"created_at":  timestamp,
					"updated_at":  timestamp,
					"name":        proj.Name,
					"description": proj.Description,
					"created_by":  proj.CreatedBy,
					"org_id":      proj.OrgID,
				}).Returning("*"),
		).Select(o.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdProj, err := scanProject(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("project with name %s already exists", proj.Name, errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_projects_org_id":
					return nil, errors.New("organization does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdProj, nil
}

func (o *projects) UpdateProject(ctx context.Context, proj *models.Project) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateProject")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("projects").
		Prepared(true).
		With("projects",
			dialect.Update("projects").
				Set(goqu.Record{
					"version":     goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":  timestamp,
					"description": proj.Description,
				}).
				Where(goqu.Ex{"id": proj.Metadata.ID, "version": proj.Metadata.Version}).
				Returning("*"),
		).Select(o.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedProj, err := scanProject(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedProj, nil
}

func (o *projects) DeleteProject(ctx context.Context, proj *models.Project) error {
	ctx, span := tracer.Start(ctx, "db.DeleteProject")
	defer span.End()

	sql, args, err := dialect.From("projects").
		Prepared(true).
		With("projects",
			dialect.Delete("projects").
				Where(goqu.Ex{"id": proj.Metadata.ID, "version": proj.Metadata.Version}).
				Returning("*"),
		).Select(o.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanProject(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (o *projects) getProject(ctx context.Context, exp exp.Expression) (*models.Project, error) {
	query := dialect.From(goqu.T("projects")).
		Prepared(true).
		Select(o.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	project, err := scanProject(o.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return project, nil
}

func (*projects) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range projectFieldList {
		selectFields = append(selectFields, fmt.Sprintf("projects.%s", field))
	}

	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanProject(row scanner) (*models.Project, error) {
	var description sql.NullString
	var organizationName string

	project := &models.Project{}

	fields := []interface{}{
		&project.Metadata.ID,
		&project.Metadata.CreationTimestamp,
		&project.Metadata.LastUpdatedTimestamp,
		&project.Metadata.Version,
		&project.Name,
		&description,
		&project.CreatedBy,
		&project.OrgID,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	if description.Valid {
		project.Description = description.String
	}

	project.Metadata.PRN = models.ProjectResource.BuildPRN(organizationName, project.Name)

	return project, nil
}

type projectMembershipExpressionBuilder struct {
	userID           *string
	serviceAccountID *string
}

func (m projectMembershipExpressionBuilder) build() exp.Expression {
	var whereEx exp.Expression

	if m.userID != nil {
		// If dealing with a user ID, must also check team member relationships.
		whereEx = goqu.Or().
			Append(goqu.I("memberships.user_id").Eq(*m.userID)).
			Append(
				goqu.I("memberships.team_id").In(
					dialect.From("team_members").
						Select("team_id").
						Where(goqu.I("team_members.user_id").Eq(*m.userID))))
	} else {
		whereEx = goqu.I("memberships.service_account_id").Eq(*m.serviceAccountID)
	}

	// Check for global membership, org membership, or project membership.
	return goqu.Or(
		goqu.I("projects.org_id").In(
			dialect.From("memberships").
				Select(goqu.L("organization_id")).
				Where(whereEx),
		),
		goqu.I("projects.id").In(
			dialect.From("memberships").
				Select(goqu.L("project_id")).
				Where(whereEx),
		),
		goqu.V(1).Eq(
			dialect.From("memberships").
				Select(goqu.COUNT("*")).
				Where(goqu.And(whereEx, goqu.I("memberships.scope").Eq(models.GlobalScope))),
		),
	)
}
