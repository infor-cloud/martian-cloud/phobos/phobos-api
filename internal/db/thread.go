package db

//go:generate go tool mockery --name Threads --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Threads encapsulates the logic to access Thread resources from the DB.
type Threads interface {
	GetThreadByID(ctx context.Context, id string) (*models.Thread, error)
	GetThreadByPRN(ctx context.Context, prn string) (*models.Thread, error)
	GetThreads(ctx context.Context, input *GetThreadsInput) (*ThreadsResult, error)
	CreateThread(ctx context.Context, thread *models.Thread) (*models.Thread, error)
	DeleteThread(ctx context.Context, thread *models.Thread) error
}

// ThreadFilter contains the supported fields for filtering Thread resources.
type ThreadFilter struct {
	PipelineID *string
	ReleaseID  *string
	ThreadIDs  []string
}

// ThreadSortableField represents the fields that a Thread can be sorted by.
type ThreadSortableField string

// ThreadSortableField constants.
const (
	ThreadSortableFieldCreatedAtAsc  ThreadSortableField = "CREATED_AT_ASC"
	ThreadSortableFieldCreatedAtDesc ThreadSortableField = "CREATED_AT_DESC"
)

func (sf ThreadSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case ThreadSortableFieldCreatedAtAsc, ThreadSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "threads", Col: "created_at"}
	default:
		return nil
	}
}

func (sf ThreadSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetThreadsInput is the input for listing Threads.
type GetThreadsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ThreadSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ThreadFilter
}

// ThreadsResult contains the response data and page information.
type ThreadsResult struct {
	PageInfo *pagination.PageInfo
	Threads  []models.Thread
}

var threadFieldList = append(metadataFieldList,
	"pipeline_id",
	"release_id",
)

type threads struct {
	dbClient *Client
}

// NewThreads returns an instance of Threads interface.
func NewThreads(dbClient *Client) Threads {
	return &threads{dbClient: dbClient}
}

func (t *threads) GetThreadByID(ctx context.Context, id string) (*models.Thread, error) {
	ctx, span := tracer.Start(ctx, "db.GetThreadByID")
	defer span.End()

	return t.getThread(ctx, goqu.Ex{"threads.id": id})
}

func (t *threads) GetThreadByPRN(ctx context.Context, prn string) (*models.Thread, error) {
	ctx, span := tracer.Start(ctx, "db.GetThreadByPRN")
	defer span.End()

	path, err := models.ThreadResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return t.getThread(ctx, goqu.Ex{"threads.id": gid.FromGlobalID(path)})
}

func (t *threads) GetThreads(ctx context.Context, input *GetThreadsInput) (*ThreadsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetThreads")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.PipelineID != nil {
			ex = ex.Append(goqu.I("threads.pipeline_id").Eq(*input.Filter.PipelineID))
		}

		if input.Filter.ReleaseID != nil {
			ex = ex.Append(goqu.I("threads.release_id").Eq(*input.Filter.ReleaseID))
		}

		if len(input.Filter.ThreadIDs) > 0 {
			ex = ex.Append(goqu.I("threads.id").In(input.Filter.ThreadIDs))
		}
	}

	query := dialect.From(goqu.T("threads")).
		Select(t.getSelectFields()...).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "threads", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, t.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Thread{}
	for rows.Next() {
		item, err := scanThread(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &ThreadsResult{
		PageInfo: rows.GetPageInfo(),
		Threads:  results,
	}

	return result, nil
}

func (t *threads) CreateThread(ctx context.Context, thread *models.Thread) (*models.Thread, error) {
	ctx, span := tracer.Start(ctx, "db.CreateThread")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Insert("threads").
		Prepared(true).
		Rows(
			goqu.Record{
				"id":          newResourceID(),
				"version":     initialResourceVersion,
				"created_at":  timestamp,
				"updated_at":  timestamp,
				"pipeline_id": thread.PipelineID,
				"release_id":  thread.ReleaseID,
			}).Returning(t.getSelectFields()...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdThread, err := scanThread(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_threads_pipeline_id":
					return nil, errors.New("pipeline does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_threads_release_id":
					return nil, errors.New("release does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdThread, nil
}

func (t *threads) DeleteThread(ctx context.Context, thread *models.Thread) error {
	ctx, span := tracer.Start(ctx, "db.DeleteThread")
	defer span.End()

	sql, args, err := dialect.Delete("threads").
		Prepared(true).
		Where(goqu.Ex{"id": thread.Metadata.ID, "version": thread.Metadata.Version}).
		Returning(t.getSelectFields()...).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanThread(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (t *threads) getThread(ctx context.Context, exp exp.Expression) (*models.Thread, error) {
	query := dialect.From(goqu.T("threads")).
		Prepared(true).
		Select(t.getSelectFields()...).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	thread, err := scanThread(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return thread, nil
}

func (*threads) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range threadFieldList {
		selectFields = append(selectFields, fmt.Sprintf("threads.%s", field))
	}

	return selectFields
}

func scanThread(row scanner) (*models.Thread, error) {
	thread := &models.Thread{}

	fields := []interface{}{
		&thread.Metadata.ID,
		&thread.Metadata.CreationTimestamp,
		&thread.Metadata.LastUpdatedTimestamp,
		&thread.Metadata.Version,
		&thread.PipelineID,
		&thread.ReleaseID,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	thread.Metadata.PRN = models.ThreadResource.BuildPRN(gid.ToGlobalID(gid.ThreadType, thread.Metadata.ID))

	return thread, nil
}
