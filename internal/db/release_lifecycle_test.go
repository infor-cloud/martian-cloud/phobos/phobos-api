//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (os ReleaseLifecycleSortableField) getValue() string {
	return string(os)
}

// sortableReleaseLifecycleSlice makes a slice of release lifecycles sortable
type sortableReleaseLifecycleSlice []*models.ReleaseLifecycle

func TestGetReleaseLifecycleByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	orgLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	projLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &proj.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	orgReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "org-release-lifecycle",
		Scope:               models.OrganizationScope,
		OrganizationID:      org.Metadata.ID,
		LifecycleTemplateID: orgLifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	projReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "proj-release-lifecycle",
		Scope:               models.ProjectScope,
		OrganizationID:      org.Metadata.ID,
		ProjectID:           &proj.Metadata.ID,
		LifecycleTemplateID: projLifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode        errors.CodeType
		name                   string
		searchID               string
		expectReleaseLifecycle bool
	}

	/*
		test case template

		type testCase struct {
			name                           string
			searchID                       string
			expectErrorCode                errors.CodeType
			expectReleaseLifecycle bool
		}
	*/

	testCases := []testCase{
		{
			name:                   "positive-org",
			searchID:               orgReleaseLifecycle.Metadata.ID,
			expectReleaseLifecycle: true,
		},
		{
			name:                   "positive-proj",
			searchID:               projReleaseLifecycle.Metadata.ID,
			expectReleaseLifecycle: true,
		},
		{
			name:     "negative, non-existent ID",
			searchID: nonExistentID,
			// expect release lifecycle and error to be nil
		},
		{
			name:                   "defective-id",
			searchID:               invalidID,
			expectReleaseLifecycle: false,
			expectErrorCode:        errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			releaseLifecycle, err := testClient.client.ReleaseLifecycles.
				GetReleaseLifecycleByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectReleaseLifecycle {
				require.NotNil(t, releaseLifecycle)
				assert.Equal(t, test.searchID, releaseLifecycle.Metadata.ID)
			} else {
				assert.Nil(t, releaseLifecycle)
			}
		})
	}
}

func TestGetReleaseLifecycleByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	orgLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	projLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &proj.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	orgReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "org-release-lifecycle",
		Scope:               models.OrganizationScope,
		OrganizationID:      org.Metadata.ID,
		LifecycleTemplateID: orgLifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	projReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "proj-release-lifecycle",
		Scope:               models.ProjectScope,
		OrganizationID:      org.Metadata.ID,
		ProjectID:           &proj.Metadata.ID,
		LifecycleTemplateID: projLifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode        errors.CodeType
		name                   string
		searchPRN              string
		expectReleaseLifecycle bool
	}

	/*
		test case template

		{
			name                   string
			searchPRN              string
			expectErrorCode        errors.CodeType
			expectReleaseLifecycle bool
		}
	*/

	testCases := []testCase{
		{
			name:                   "positive-org",
			searchPRN:              orgReleaseLifecycle.Metadata.PRN,
			expectReleaseLifecycle: true,
		},
		{
			name:                   "positive-proj",
			searchPRN:              projReleaseLifecycle.Metadata.PRN,
			expectReleaseLifecycle: true,
		},
		{
			name:      "negative, release lifecycle does not exist",
			searchPRN: models.ReleaseLifecycleResource.BuildPRN("non-existent-org", nonExistentGlobalID),
			// expect release lifecycle and error to be nil
		},
		{
			name:            "defective-prn",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			releaseLifecycle, err := testClient.client.ReleaseLifecycles.GetReleaseLifecycleByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectReleaseLifecycle {
				require.NotNil(t, releaseLifecycle)
				assert.Equal(t, test.searchPRN, releaseLifecycle.Metadata.PRN)
			} else {
				assert.Nil(t, releaseLifecycle)
			}
		})
	}
}

func TestGetReleaseLifecycles(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	org2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org2",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	proj1b, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	org1LifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org1.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	org2LifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org2.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	proj1aLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org1.Metadata.ID,
		ProjectID:      &proj1a.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	proj1bLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org1.Metadata.ID,
		ProjectID:      &proj1b.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	lifecycles := make([]*models.ReleaseLifecycle, 10)
	for i := 0; i < len(lifecycles); i++ {
		// a mix of org-scope, proj1a, and proj1b
		var scope models.ScopeType
		var projectID *string
		var templateID string
		switch i % 3 {
		case 0: // i=0, 3, 6, 9
			scope = models.OrganizationScope
			templateID = org1LifecycleTemplate.Metadata.ID
		case 1: // i=1, 4, 7
			scope = models.ProjectScope
			projectID = &proj1a.Metadata.ID
			templateID = proj1aLifecycleTemplate.Metadata.ID
		case 2: // i=2, 5, 8
			scope = models.ProjectScope
			projectID = &proj1b.Metadata.ID
			templateID = proj1bLifecycleTemplate.Metadata.ID
		}

		lifecycle, aErr := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
			Name:                fmt.Sprintf("test-lifecycle-%d", i),
			Scope:               scope,
			OrganizationID:      org1.Metadata.ID,
			ProjectID:           projectID,
			LifecycleTemplateID: templateID,
		})
		require.Nil(t, aErr)

		lifecycles[i] = lifecycle
	}

	lifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                fmt.Sprintf("test-lifecycle-%d", len(lifecycles)),
		Scope:               models.OrganizationScope,
		OrganizationID:      org2.Metadata.ID,
		LifecycleTemplateID: org2LifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	lifecycles = append(lifecycles, lifecycle)

	type testCase struct {
		expectErrorCode errors.CodeType
		input           *GetReleaseLifecyclesInput
		name            string
		expectCount     int
	}

	/*
		test case template

		type testCase struct {
		name            string
		input           *GetReleaseLifecyclesInput
		expectErrorCode errors.CodeType
		expectCount     int
		}
	*/

	testCases := []testCase{
		{
			name: "non-nil but mostly empty input",
			input: &GetReleaseLifecyclesInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			},
			expectCount: len(lifecycles),
		},

		{
			name: "populated sort and pagination, nil filter",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
				Filter: nil,
			},
			expectCount: len(lifecycles),
		},

		{
			name: "filter, empty slice of release lifecycle IDs, same as no filter",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ReleaseLifecycleIDs: []string{},
				},
			},
			expectCount: len(lifecycles),
		},

		{
			name: "filter, release lifecycle IDs",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ReleaseLifecycleIDs: []string{
						lifecycles[0].Metadata.ID,
						lifecycles[2].Metadata.ID,
						lifecycles[4].Metadata.ID,
					},
				},
			},
			expectCount: 3,
		},

		{
			name: "filter, release lifecycle IDs, non-existent",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ReleaseLifecycleIDs: []string{nonExistentID},
				},
			},
			expectCount: 0,
		},

		{
			name: "filter, release lifecycle IDs, invalid",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ReleaseLifecycleIDs: []string{invalidID},
				},
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "filter, empty organization ID",
			input: &GetReleaseLifecyclesInput{
				Sort:   ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{},
			},
			expectCount: len(lifecycles),
		},

		{
			name: "filter, valid organization ID, scopes not filtered",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					OrganizationID: &org1.Metadata.ID,
				},
			},
			expectCount: len(lifecycles) - 1,
		},

		{
			name: "filter, valid organization ID, org scope only",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					OrganizationID:         &org1.Metadata.ID,
					ReleaseLifecycleScopes: []models.ScopeType{models.OrganizationScope},
				},
			},
			expectCount: 4,
		},

		{
			name: "filter, non-existent organization ID",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					OrganizationID: ptr.String(nonExistentID), // nonExistentID is an untyped string, so cannot use ampersand operator
				},
			},
			expectCount: 0,
		},

		{
			name: "filter, invalid organization ID",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					OrganizationID: ptr.String(invalidID), // invalidID is an untyped string, so cannot use ampersand operator
				},
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "filter, empty project ID",
			input: &GetReleaseLifecyclesInput{
				Sort:   ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{},
			},
			expectCount: len(lifecycles),
		},

		{
			name: "filter, valid project ID, all scopes",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ProjectID: &proj1a.Metadata.ID,
				},
			},
			expectCount: 7,
		},

		{
			name: "filter, valid project ID, project scope only",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ProjectID:              &proj1a.Metadata.ID,
					ReleaseLifecycleScopes: []models.ScopeType{models.ProjectScope},
				},
			},
			expectCount: 3,
		},

		{
			name: "filter, non-existent project ID",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ProjectID: ptr.String(nonExistentID), // nonExistentID is an untyped string, so cannot use ampersand operator
				},
			},
			expectCount: 0,
		},

		{
			name: "filter, invalid project ID",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					ProjectID: ptr.String(invalidID),
				},
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "filter, valid release lifecycle search prefix",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					Search: ptr.String("test-lifecycle-1"),
				},
			},
			expectCount: 2,
		},

		{
			name: "filter, non-existent release lifecycle search prefix",
			input: &GetReleaseLifecyclesInput{
				Sort: ptrReleaseLifecycleSortableField(ReleaseLifecycleSortableFieldUpdatedAtAsc),
				Filter: &ReleaseLifecycleFilter{
					Search: ptr.String(nonExistentName), // untyped string, so cannot use ampersand operator
				},
			},
			expectCount: 0,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			releaseLifecyclesResult, err := testClient.client.ReleaseLifecycles.
				GetReleaseLifecycles(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			// If there was no error, check the results.
			if err == nil {

				// Never returns nil if error is nil.
				require.NotNil(t, releaseLifecyclesResult.PageInfo)
				assert.NotNil(t, releaseLifecyclesResult.ReleaseLifecycles)
				assert.Equal(t, test.expectCount, len(releaseLifecyclesResult.ReleaseLifecycles))

			}
		})
	}
}

func TestGetReleaseLifecyclesWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	org2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org2",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	proj1b, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	org1LifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org1.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	org2LifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org2.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	proj1aLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org1.Metadata.ID,
		ProjectID:      &proj1a.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	proj1bLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org1.Metadata.ID,
		ProjectID:      &proj1b.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	lifecycles := make([]*models.ReleaseLifecycle, 10)
	for i := 0; i < len(lifecycles); i++ {
		// a mix of org-scope, proj1a, and proj1b
		var scope models.ScopeType
		var projectID *string
		var templateID string
		switch i % 3 {
		case 0: // i=0, 3, 6, 9
			scope = models.OrganizationScope
			templateID = org1LifecycleTemplate.Metadata.ID
		case 1: // i=1, 4, 7
			scope = models.ProjectScope
			projectID = &proj1a.Metadata.ID
			templateID = proj1aLifecycleTemplate.Metadata.ID
		case 2: // i=2, 5, 8
			scope = models.ProjectScope
			projectID = &proj1b.Metadata.ID
			templateID = proj1bLifecycleTemplate.Metadata.ID
		}

		lifecycle, aErr := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
			Name:                fmt.Sprintf("test-lifecycle-%d", i),
			Scope:               scope,
			OrganizationID:      org1.Metadata.ID,
			ProjectID:           projectID,
			LifecycleTemplateID: templateID,
		})
		require.Nil(t, aErr)

		lifecycles[i] = lifecycle
	}

	lifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                fmt.Sprintf("test-lifecycle-%d", len(lifecycles)),
		Scope:               models.OrganizationScope,
		OrganizationID:      org2.Metadata.ID,
		LifecycleTemplateID: org2LifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	lifecycles = append(lifecycles, lifecycle)
	resourceCount := len(lifecycles)

	sortableFields := []sortableField{
		ReleaseLifecycleSortableFieldUpdatedAtAsc,
		ReleaseLifecycleSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields,
		func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
			sortBy := ReleaseLifecycleSortableField(sortByField.getValue())

			result, err := testClient.client.ReleaseLifecycles.GetReleaseLifecycles(ctx, &GetReleaseLifecyclesInput{
				Sort:              &sortBy,
				PaginationOptions: paginationOptions,
			})
			if err != nil {
				return nil, nil, err
			}

			resources := []pagination.CursorPaginatable{}
			for _, resource := range result.ReleaseLifecycles {
				resources = append(resources, resource)
			}

			return result.PageInfo, resources, nil
		})
}

func TestCreateReleaseLifecycle(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	orgLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org1.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	projLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org1.Metadata.ID,
		ProjectID:      &proj1a.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	type testCase struct {
		toCreate        *models.ReleaseLifecycle
		expectErrorCode errors.CodeType
		name            string
	}

	/*
		test case template

		type testCase struct {
			name            string
			toCreate        *models.ReleaseLifecycle
			expectErrorCode errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name: "positive, organization scoped release lifecycle",
			toCreate: &models.ReleaseLifecycle{
				Name:                "org-scope-release-lifecycle-1",
				CreatedBy:           "someone",
				Scope:               models.OrganizationScope,
				OrganizationID:      org1.Metadata.ID,
				LifecycleTemplateID: orgLifecycleTemplate.Metadata.ID,
				EnvironmentNames:    []string{"dev", "staging", "prod"},
			},
			// no error expected
		},
		{
			name: "positive, project scoped release lifecycle",
			toCreate: &models.ReleaseLifecycle{
				Name:                "project-scope-release-lifecycle-1",
				CreatedBy:           "someone",
				Scope:               models.OrganizationScope,
				OrganizationID:      org1.Metadata.ID,
				ProjectID:           &proj1a.Metadata.ID,
				LifecycleTemplateID: projLifecycleTemplate.Metadata.ID,
				EnvironmentNames:    []string{"dev", "prod"},
			},
			// no error expected
		},
		{
			name: "negative, duplicate entry",
			toCreate: &models.ReleaseLifecycle{
				Name:                "org-scope-release-lifecycle-1", // intentionally duplicated from above
				CreatedBy:           "someone",
				Scope:               models.OrganizationScope,
				OrganizationID:      org1.Metadata.ID,
				LifecycleTemplateID: orgLifecycleTemplate.Metadata.ID,
				EnvironmentNames:    []string{"dev", "staging", "prod"},
			},
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			created, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
				Name:                test.toCreate.Name,
				CreatedBy:           test.toCreate.CreatedBy,
				Scope:               test.toCreate.Scope,
				OrganizationID:      test.toCreate.OrganizationID,
				ProjectID:           test.toCreate.ProjectID,
				LifecycleTemplateID: test.toCreate.LifecycleTemplateID,
				EnvironmentNames:    test.toCreate.EnvironmentNames,
			})

			if test.expectErrorCode != "" {
				require.NotNil(t, err)
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
			} else {
				require.Nil(t, err)
			}

			if test.expectErrorCode == "" {
				compareReleaseLifecyclesCreate(t, test.toCreate, created)
			}
		})
	}
}

func TestUpdateReleaseLifecycle(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	orgLifecycleTemplate1, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	projLifecycleTemplate1, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &proj.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	orgLifecycleTemplate2, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	projLifecycleTemplate2, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &proj.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	orgReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "org-release-lifecycle",
		Scope:               models.OrganizationScope,
		OrganizationID:      org.Metadata.ID,
		LifecycleTemplateID: orgLifecycleTemplate1.Metadata.ID,
	})
	require.Nil(t, err)

	projReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "proj-release-lifecycle",
		Scope:               models.ProjectScope,
		OrganizationID:      org.Metadata.ID,
		ProjectID:           &proj.Metadata.ID,
		LifecycleTemplateID: projLifecycleTemplate1.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		findReleaseLifecycle   *models.ReleaseLifecycle
		newLifecycleTemplateID string
		expectErrorCode        errors.CodeType
		name                   string
		isPositive             bool
	}

	/*
		test case template

		type testCase struct {
			name                   string
			findReleaseLifecycle   *models.ReleaseLifecycle
			newLifecycleTemplateID string
			isPositive             bool
			expectErrorCode        errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name:                   "positive-org",
			findReleaseLifecycle:   orgReleaseLifecycle,
			newLifecycleTemplateID: orgLifecycleTemplate2.Metadata.ID,
			isPositive:             true,
		},
		{
			name:                   "positive-proj",
			findReleaseLifecycle:   projReleaseLifecycle,
			newLifecycleTemplateID: projLifecycleTemplate2.Metadata.ID,
			isPositive:             true,
		},
		{
			name: "negative, not exist",
			findReleaseLifecycle: &models.ReleaseLifecycle{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			newLifecycleTemplateID: orgLifecycleTemplate2.Metadata.ID,
			expectErrorCode:        errors.EOptimisticLock,
		},
		{
			name: "negative, invalid uuid",
			findReleaseLifecycle: &models.ReleaseLifecycle{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			newLifecycleTemplateID: projLifecycleTemplate2.Metadata.ID,
			expectErrorCode:        errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			originalReleaseLifecycle := test.findReleaseLifecycle

			modifiedReleaseLifecycle := &models.ReleaseLifecycle{
				Metadata: models.ResourceMetadata{
					ID:                   originalReleaseLifecycle.Metadata.ID,
					Version:              originalReleaseLifecycle.Metadata.Version,
					CreationTimestamp:    originalReleaseLifecycle.Metadata.CreationTimestamp,
					LastUpdatedTimestamp: originalReleaseLifecycle.Metadata.LastUpdatedTimestamp,
				},
				CreatedBy:           originalReleaseLifecycle.CreatedBy,
				Name:                originalReleaseLifecycle.Name,
				OrganizationID:      originalReleaseLifecycle.OrganizationID,
				LifecycleTemplateID: test.newLifecycleTemplateID,
			}

			claimedUpdatedReleaseLifecycle, err := testClient.client.ReleaseLifecycles.
				UpdateReleaseLifecycle(ctx, modifiedReleaseLifecycle)

			assert.Equal(t, (test.expectErrorCode == ""), (err == nil))
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.isPositive {
				require.NotNil(t, claimedUpdatedReleaseLifecycle)
				compareReleaseLifecyclesUpdate(t, modifiedReleaseLifecycle, claimedUpdatedReleaseLifecycle)

				retrieved, err := testClient.client.ReleaseLifecycles.GetReleaseLifecycleByID(ctx, modifiedReleaseLifecycle.Metadata.ID)
				require.Nil(t, err)

				require.NotNil(t, retrieved)
				compareReleaseLifecyclesUpdate(t, modifiedReleaseLifecycle, retrieved)
			}
		})
	}
}

func TestDeleteReleaseLifecycle(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-proj",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	orgLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.OrganizationScope,
		OrganizationID: org.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	projLifecycleTemplate, err := testClient.client.LifecycleTemplates.CreateLifecycleTemplate(ctx, &models.LifecycleTemplate{
		Scope:          models.ProjectScope,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &proj.Metadata.ID,
		Status:         models.LifecycleTemplatePending,
	})
	require.Nil(t, err)

	orgReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "org-release-lifecycle",
		Scope:               models.OrganizationScope,
		OrganizationID:      org.Metadata.ID,
		LifecycleTemplateID: orgLifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	projReleaseLifecycle, err := testClient.client.ReleaseLifecycles.CreateReleaseLifecycle(ctx, &models.ReleaseLifecycle{
		Name:                "proj-release-lifecycle",
		Scope:               models.ProjectScope,
		OrganizationID:      org.Metadata.ID,
		ProjectID:           &proj.Metadata.ID,
		LifecycleTemplateID: projLifecycleTemplate.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		findReleaseLifecycle *models.ReleaseLifecycle
		expectErrorCode      errors.CodeType
		name                 string
		isPositive           bool
	}

	/*
		test case template

		type testCase struct {
			name                   string
			findReleaseLifecycle   *models.ReleaseLifecycle
			isPositive             bool
			expectErrorCode        errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name: "positive, org",
			findReleaseLifecycle: &models.ReleaseLifecycle{
				Metadata: models.ResourceMetadata{
					ID:      orgReleaseLifecycle.Metadata.ID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			isPositive: true,
		},
		{
			name: "positive, proj",
			findReleaseLifecycle: &models.ReleaseLifecycle{
				Metadata: models.ResourceMetadata{
					ID:      projReleaseLifecycle.Metadata.ID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			isPositive: true,
		},
		{
			name: "negative, not exist",
			findReleaseLifecycle: &models.ReleaseLifecycle{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EOptimisticLock,
		},
		{
			name: "negative, invalid uuid",
			findReleaseLifecycle: &models.ReleaseLifecycle{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				CreatedBy: "someone",
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.ReleaseLifecycles.DeleteReleaseLifecycle(ctx, test.findReleaseLifecycle)

			assert.Equal(t, (test.expectErrorCode == ""), (err == nil))
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
		})
	}
}

func ptrReleaseLifecycleSortableField(arg ReleaseLifecycleSortableField) *ReleaseLifecycleSortableField {
	return &arg
}

func (s sortableReleaseLifecycleSlice) Len() int {
	return len(s)
}

func (s sortableReleaseLifecycleSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s sortableReleaseLifecycleSlice) Less(i, j int) bool {
	return s[i].Name < s[j].Name
}

// compareReleaseLifecyclesCreate compares two release lifecycles for TestCreateReleaseLifecycle
// There are some fields that cannot be compared, or DeepEqual would work.
func compareReleaseLifecyclesCreate(t *testing.T, expected, actual *models.ReleaseLifecycle) {
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
	assert.Equal(t, expected.Scope, actual.Scope)
	assert.Equal(t, expected.OrganizationID, actual.OrganizationID)
	assert.Equal(t, expected.ProjectID, actual.ProjectID)
	assert.Equal(t, expected.LifecycleTemplateID, actual.LifecycleTemplateID)
	assert.ElementsMatch(t, expected.EnvironmentNames, actual.EnvironmentNames)
}

// compareReleaseLifecyclesUpdate compares two release lifecycles for TestUpdateReleaseLifecycle
func compareReleaseLifecyclesUpdate(t *testing.T, expected, actual *models.ReleaseLifecycle) {
	assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	assert.Equal(t, expected.Metadata.CreationTimestamp, actual.Metadata.CreationTimestamp)
	assert.NotEqual(t, expected.Metadata.Version, actual.Metadata.Version)
	assert.NotEqual(t, expected.Metadata.LastUpdatedTimestamp, actual.Metadata.LastUpdatedTimestamp)

	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
	assert.Equal(t, expected.OrganizationID, actual.OrganizationID)
	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.LifecycleTemplateID, actual.LifecycleTemplateID)
}
