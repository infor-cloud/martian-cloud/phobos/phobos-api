//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestGetEnvironmentByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	environment, err := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
		Name:      "account-1",
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode   errors.CodeType
		name              string
		id                string
		expectEnvironment bool
	}

	testCases := []testCase{
		{
			name:              "get resource by id",
			id:                environment.Metadata.ID,
			expectEnvironment: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			environment, err := testClient.client.Environments.GetEnvironmentByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectEnvironment {
				require.NotNil(t, environment)
				assert.Equal(t, test.id, environment.Metadata.ID)
			} else {
				assert.Nil(t, environment)
			}
		})
	}
}

func TestGetEnvironmentByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	environment, err := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
		Name:      "production",
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode   errors.CodeType
		name              string
		prn               string
		expectEnvironment bool
	}

	testCases := []testCase{
		{
			name:              "get resource by PRN",
			prn:               environment.Metadata.PRN,
			expectEnvironment: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.EnvironmentResource.BuildPRN(organization.Name, project.Name, "non-existent"),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			environment, err := testClient.client.Environments.GetEnvironmentByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectEnvironment {
				require.NotNil(t, environment)
				assert.Equal(t, test.prn, environment.Metadata.PRN)
			} else {
				assert.Nil(t, environment)
			}
		})
	}
}

func TestGetEnvironments(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	project2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "electron",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	environments := make([]*models.Environment, 10)
	for i := 0; i < len(environments); i++ {
		environment, eErr := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
			Name:      fmt.Sprintf("account-%d", i),
			ProjectID: project.Metadata.ID,
		})
		require.NoError(t, eErr)

		environments[i] = environment
	}

	environment, err := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
		Name:      "production",
		ProjectID: project2.Metadata.ID,
	})
	require.NoError(t, err)

	environments = append(environments, environment)

	type testCase struct {
		filter            *EnvironmentFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all resources",
			filter:            &EnvironmentFilter{},
			expectResultCount: len(environments),
		},
		{
			name: "get environments for project 1",
			filter: &EnvironmentFilter{
				ProjectID: &project.Metadata.ID,
			},
			expectResultCount: len(environments) - 1,
		},
		{
			name: "get environments for project 2",
			filter: &EnvironmentFilter{
				ProjectID: &project2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "get environment by name",
			filter: &EnvironmentFilter{
				EnvironmentName: &environment.Name,
			},
			expectResultCount: 1,
		},
		{
			name: "return resources by ids",
			filter: &EnvironmentFilter{
				EnvironmentIDs: []string{environments[0].Metadata.ID, environments[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.Environments.GetEnvironments(ctx, &GetEnvironmentsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, result)
			assert.Len(t, result.Environments, test.expectResultCount)
		})
	}
}

func TestGetEnvironmentsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, eErr := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
			Name:      fmt.Sprintf("account-%d", i),
			ProjectID: project.Metadata.ID,
		})
		require.NoError(t, eErr)
	}

	sortableFields := []sortableField{
		EnvironmentSortableFieldUpdatedAtAsc,
		EnvironmentSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := EnvironmentSortableField(sortByField.getValue())

		result, err := testClient.client.Environments.GetEnvironments(ctx, &GetEnvironmentsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.Environments {
			resourceCopy := resource
			resources = append(resources, &resourceCopy)
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreateEnvironment(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	environmentName := "production"

	type testCase struct {
		name            string
		projectID       string
		environmentName string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:            "create resource",
			projectID:       project.Metadata.ID,
			environmentName: environmentName,
		},
		{
			name:            "create resource with invalid project id will return an error",
			projectID:       nonExistentID,
			environmentName: environmentName,
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "duplicate resource will return an error",
			projectID:       project.Metadata.ID,
			environmentName: environmentName,
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			environment, err := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
				Name:      test.environmentName,
				ProjectID: test.projectID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, environment)
			assert.Equal(t, test.environmentName, environment.Name)
		})
	}
}

func TestUpdateEnvironment(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	environment, err := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
		Name:      "production",
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	description := "updated description"

	type testCase struct {
		name            string
		description     string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:        "update resource",
			description: description,
			version:     environment.Metadata.Version,
		},
		{
			name:            "update resource with invalid version will return an error",
			description:     description,
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			updatedEnvironment, err := testClient.client.Environments.UpdateEnvironment(ctx, &models.Environment{
				Metadata: models.ResourceMetadata{
					ID:      environment.Metadata.ID,
					Version: test.version,
				},
				Description: description,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedEnvironment)
			assert.Equal(t, description, updatedEnvironment.Description)
		})
	}
}

func TestDeleteEnvironment(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	environment, err := testClient.client.Environments.CreateEnvironment(ctx, &models.Environment{
		Name:      "production",
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		id              string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "delete resource",
			id:      environment.Metadata.ID,
			version: environment.Metadata.Version,
		},
		{
			name:            "delete resource with invalid version will return an error",
			id:              environment.Metadata.ID,
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.Environments.DeleteEnvironment(ctx, &models.Environment{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
