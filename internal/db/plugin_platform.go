package db

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

//go:generate go tool mockery --name PluginPlatforms --inpackage --case underscore

// PluginPlatforms is an interface that represents the db plugin_platforms table.
type PluginPlatforms interface {
	GetPluginPlatformByID(ctx context.Context, id string) (*models.PluginPlatform, error)
	GetPluginPlatformByPRN(ctx context.Context, prn string) (*models.PluginPlatform, error)
	GetPluginPlatforms(ctx context.Context, input *GetPluginPlatformsInput) (*PluginPlatformsResult, error)
	CreatePluginPlatform(ctx context.Context, pluginPlatform *models.PluginPlatform) (*models.PluginPlatform, error)
	UpdatePluginPlatform(ctx context.Context, pluginPlatform *models.PluginPlatform) (*models.PluginPlatform, error)
	DeletePluginPlatform(ctx context.Context, pluginPlatform *models.PluginPlatform) error
}

// PluginPlatformSortableField represents the fields that can be used to sort a collection of plugin_platforms.
type PluginPlatformSortableField string

// PluginPlatformSortableField constants
const (
	PluginPlatformSortableFieldUpdatedAtAsc  PluginPlatformSortableField = "UPDATED_AT_ASC"
	PluginPlatformSortableFieldUpdatedAtDesc PluginPlatformSortableField = "UPDATED_AT_DESC"
)

func (sf PluginPlatformSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case PluginPlatformSortableFieldUpdatedAtAsc, PluginPlatformSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "plugin_platforms", Col: "updated_at"}
	default:
		return nil
	}
}

func (sf PluginPlatformSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// PluginPlatformFilter represents the filters that can be used to filter a collection of plugin_platforms.
type PluginPlatformFilter struct {
	PluginID        *string
	PluginVersionID *string
	BinaryUploaded  *bool
	OperatingSystem *string
	Architecture    *string
}

// GetPluginPlatformsInput represents the input for the GetPluginPlatforms method.
type GetPluginPlatformsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *PluginPlatformSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *PluginPlatformFilter
}

// PluginPlatformsResult represents the result from the GetPluginPlatforms method.
type PluginPlatformsResult struct {
	PageInfo        *pagination.PageInfo
	PluginPlatforms []models.PluginPlatform
}

type pluginPlatforms struct {
	dbClient *Client
}

var pluginPlatformFieldList = append(metadataFieldList,
	"created_by",
	"plugin_version_id",
	"os",
	"arch",
	"sha_sum",
	"filename",
	"binary_uploaded",
)

// NewPluginPlatforms returns a new instance of PluginPlatforms.
func NewPluginPlatforms(dbClient *Client) PluginPlatforms {
	return &pluginPlatforms{dbClient: dbClient}
}

func (p *pluginPlatforms) GetPluginPlatformByID(ctx context.Context, id string) (*models.PluginPlatform, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginPlatformByID")
	defer span.End()

	return p.getPluginPlatform(ctx, goqu.Ex{"plugin_platforms.id": id})
}

func (p *pluginPlatforms) GetPluginPlatformByPRN(ctx context.Context, prn string) (*models.PluginPlatform, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginPlatformByPRN")
	defer span.End()

	path, err := models.PluginPlatformResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, err
	}

	parts := strings.Split(path, "/")

	if len(parts) != 5 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getPluginPlatform(ctx, goqu.Ex{
		"organizations.name":                      parts[0],
		"plugins.name":                            parts[1],
		"plugin_versions.plugin_semantic_version": parts[2],
		"plugin_platforms.os":                     parts[3],
		"plugin_platforms.arch":                   parts[4],
	})
}

func (p *pluginPlatforms) GetPluginPlatforms(ctx context.Context, input *GetPluginPlatformsInput) (*PluginPlatformsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetPluginPlatforms")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.PluginID != nil {
			ex = ex.Append(goqu.I("plugin_versions.plugin_id").Eq(*input.Filter.PluginID))
		}

		if input.Filter.PluginVersionID != nil {
			ex = ex.Append(goqu.I("plugin_platforms.plugin_version_id").Eq(*input.Filter.PluginVersionID))
		}

		if input.Filter.BinaryUploaded != nil {
			ex = ex.Append(goqu.I("plugin_platforms.binary_uploaded").Eq(*input.Filter.BinaryUploaded))
		}

		if input.Filter.OperatingSystem != nil {
			ex = ex.Append(goqu.I("plugin_platforms.os").Eq(*input.Filter.OperatingSystem))
		}

		if input.Filter.Architecture != nil {
			ex = ex.Append(goqu.I("plugin_platforms.arch").Eq(*input.Filter.Architecture))
		}
	}

	query := dialect.From(goqu.T("plugin_platforms")).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugin_versions"), goqu.On(goqu.I("plugin_platforms.plugin_version_id").Eq(goqu.I("plugin_versions.id")))).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "plugin_platforms", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.PluginPlatform{}
	for rows.Next() {
		item, err := scanPluginPlatform(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &PluginPlatformsResult{
		PageInfo:        rows.GetPageInfo(),
		PluginPlatforms: results,
	}

	return result, nil
}

func (p *pluginPlatforms) CreatePluginPlatform(ctx context.Context, pluginPlatform *models.PluginPlatform) (*models.PluginPlatform, error) {
	ctx, span := tracer.Start(ctx, "db.CreatePluginPlatform")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("plugin_platforms").
		Prepared(true).
		With("plugin_platforms",
			dialect.Insert("plugin_platforms").Rows(
				goqu.Record{
					"id":                newResourceID(),
					"version":           initialResourceVersion,
					"created_at":        timestamp,
					"updated_at":        timestamp,
					"created_by":        pluginPlatform.CreatedBy,
					"plugin_version_id": pluginPlatform.PluginVersionID,
					"os":                pluginPlatform.OperatingSystem,
					"arch":              pluginPlatform.Architecture,
					"sha_sum":           pluginPlatform.SHASum,
					"filename":          pluginPlatform.Filename,
					"binary_uploaded":   pluginPlatform.BinaryUploaded,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugin_versions"), goqu.On(goqu.I("plugin_platforms.plugin_version_id").Eq(goqu.I("plugin_versions.id")))).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdPlatform, err := scanPluginPlatform(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("plugin platform %s_%s already exists",
					pluginPlatform.OperatingSystem,
					pluginPlatform.Architecture,
					errors.WithErrorCode(errors.EConflict),
					errors.WithSpan(span),
				)
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdPlatform, nil
}

func (p *pluginPlatforms) UpdatePluginPlatform(ctx context.Context, pluginPlatform *models.PluginPlatform) (*models.PluginPlatform, error) {
	ctx, span := tracer.Start(ctx, "db.UpdatePluginPlatform")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("plugin_platforms").
		Prepared(true).
		With("plugin_platforms",
			dialect.Update("plugin_platforms").
				Set(goqu.Record{
					"version":         goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":      timestamp,
					"binary_uploaded": pluginPlatform.BinaryUploaded,
				}).
				Where(goqu.Ex{"id": pluginPlatform.Metadata.ID, "version": pluginPlatform.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugin_versions"), goqu.On(goqu.I("plugin_platforms.plugin_version_id").Eq(goqu.I("plugin_versions.id")))).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedPlatform, err := scanPluginPlatform(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedPlatform, nil
}

func (p *pluginPlatforms) DeletePluginPlatform(ctx context.Context, pluginPlatform *models.PluginPlatform) error {
	ctx, span := tracer.Start(ctx, "db.DeletePluginPlatform")
	defer span.End()

	sql, args, err := dialect.From("plugin_platforms").
		Prepared(true).
		With("plugin_platforms",
			dialect.Delete("plugin_platforms").
				Where(goqu.Ex{"id": pluginPlatform.Metadata.ID, "version": pluginPlatform.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugin_versions"), goqu.On(goqu.I("plugin_platforms.plugin_version_id").Eq(goqu.I("plugin_versions.id")))).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanPluginPlatform(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *pluginPlatforms) getPluginPlatform(ctx context.Context, exp goqu.Ex) (*models.PluginPlatform, error) {
	query := dialect.From(goqu.T("plugin_platforms")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("plugin_versions"), goqu.On(goqu.I("plugin_platforms.plugin_version_id").Eq(goqu.I("plugin_versions.id")))).
		InnerJoin(goqu.T("plugins"), goqu.On(goqu.I("plugin_versions.plugin_id").Eq(goqu.I("plugins.id")))).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("plugins.organization_id").Eq(goqu.I("organizations.id")))).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	pluginPlatform, err := scanPluginPlatform(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return pluginPlatform, nil
}

func (p *pluginPlatforms) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range pluginPlatformFieldList {
		selectFields = append(selectFields, fmt.Sprintf("plugin_platforms.%s", field))
	}

	selectFields = append(selectFields, "organizations.name")
	selectFields = append(selectFields, "plugins.name")
	selectFields = append(selectFields, "plugin_versions.plugin_semantic_version")

	return selectFields
}

func scanPluginPlatform(row scanner) (*models.PluginPlatform, error) {
	var organizationName, pluginName, pluginSemanticVersion string
	pluginPlatform := &models.PluginPlatform{}

	fields := []interface{}{
		&pluginPlatform.Metadata.ID,
		&pluginPlatform.Metadata.CreationTimestamp,
		&pluginPlatform.Metadata.LastUpdatedTimestamp,
		&pluginPlatform.Metadata.Version,
		&pluginPlatform.CreatedBy,
		&pluginPlatform.PluginVersionID,
		&pluginPlatform.OperatingSystem,
		&pluginPlatform.Architecture,
		&pluginPlatform.SHASum,
		&pluginPlatform.Filename,
		&pluginPlatform.BinaryUploaded,
		&organizationName,
		&pluginName,
		&pluginSemanticVersion,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	pluginPlatform.Metadata.PRN = models.PluginPlatformResource.BuildPRN(
		organizationName,
		pluginName,
		pluginSemanticVersion,
		pluginPlatform.OperatingSystem,
		pluginPlatform.Architecture,
	)

	return pluginPlatform, nil
}
