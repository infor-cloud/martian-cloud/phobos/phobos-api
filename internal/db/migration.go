package db

import (
	"context"
	"embed"
	"net/url"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/pgx" // Instantiating migrate command
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// migrationSchema contains the embedded .sql files that are
// needed to migrate the DB automatically.
//
//go:embed migrations/*.sql
var migrationSchema embed.FS

// migrations implements methods necessary to migrate the DB automatically.
type migrations struct {
	logger      logger.Logger
	databaseURL string
}

// newMigrations returns an instance of migrations struct.
func newMigrations(logger logger.Logger, databaseURL string) (*migrations, error) {
	dbURL, err := url.Parse(databaseURL)
	if err != nil {
		return nil, err
	}
	dbURL.Scheme = "pgx" // Change the scheme.

	return &migrations{
		logger:      logger,
		databaseURL: dbURL.String(),
	}, nil
}

// migrateUp migrates the DB to the latest version.
func (m *migrations) migrateUp(ctx context.Context) error {
	_, span := tracer.Start(ctx, "db.migrateUp")
	defer span.End()

	fsDriver, err := iofs.New(migrationSchema, "migrations")
	if err != nil {
		return errors.Wrap(err, "failed to get iofs driver", errors.WithSpan(span))
	}
	defer fsDriver.Close()

	migrateCmd, err := migrate.NewWithSourceInstance("iofs", fsDriver, m.databaseURL)
	if err != nil {
		return errors.Wrap(err, "failed to generate migrate command", errors.WithSpan(span))
	}

	defer func() {
		sourceErr, dbDriverErr := migrateCmd.Close()
		if sourceErr != nil {
			m.logger.Errorf("failed to close migrate command source driver: %v", err)
			_ = errors.Wrap(err, "failed to close migrate command source driver", errors.WithSpan(span))
		}
		if dbDriverErr != nil {
			m.logger.Errorf("failed to close migrate command DB driver: %v", err)
			_ = errors.Wrap(err, "failed to close migrate command DB driver", errors.WithSpan(span))
		}
	}()

	// Perform the migration.
	return migrateCmd.Up()
}
