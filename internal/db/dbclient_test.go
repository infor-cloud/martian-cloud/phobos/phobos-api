//go:build integration

package db

import (
	"cmp"
	"context"
	"fmt"
	"slices"
	"strconv"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/doug-martin/goqu/v9"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

const (

	// a contrived bogus ID
	nonExistentID = "12345678-1234-1234-1234-123456789abc"

	// a contrived bogus global ID used for testing PRN functions
	nonExistentGlobalID = "GEZDGNBVGY3TQLJRGIZTILJRGIZTILJRGIZTILJRGIZTINJWG44DSYLCMNPUC"

	// an invalid ID
	invalidID = "not-a-valid-uuid"

	// a non-existent resource name
	nonExistentName = "this-resource-name-does-not-exist"

	// Maximum number of DB connections--intended to mirror the CI environment
	maxConns = 4
)

var (

	// Used by NewClient to build the DB URI:
	// The script that runs the tests should set these via build flags.
	TestDBHost string
	TestDBPort string // contents of string must be numeric
	TestDBName string
	TestDBMode string
	TestDBUser string
	TestDBPass string

	// constants that cannot be constants

	// Map of names of tables that are excluded from being truncated.
	nonTruncateTables = map[string]interface{}{
		"schema_migrations": nil,
		"resource_limits":   nil,
	}
)

type testClient struct {
	logger logger.Logger
	client *Client
}

// time bounds for comparing object metadata
type timeBounds struct {
	createLow  *time.Time
	createHigh *time.Time
	updateLow  *time.Time
	updateHigh *time.Time
}

// newTestClient creates a new DB client to use for the DB integration tests.
// It also wipes all tables empty.
// Based on environment variables, the client could be for a local standalone DB server
// or one created inside the CI/CD pipeline.
func newTestClient(ctx context.Context, t *testing.T) *testClient {
	portNum, err := strconv.Atoi(TestDBPort)
	if err != nil {
		t.Fatal(err)
	}

	logger, _ := logger.NewForTest()

	client, err := NewClient(ctx, TestDBHost, portNum, TestDBName, TestDBMode, TestDBUser, TestDBPass, maxConns, true, logger)
	if err != nil {
		t.Fatal(err)
	}

	result := testClient{
		client: client,
		logger: logger,
	}

	err = result.wipeAllTables(ctx)
	if err != nil {
		t.Fatal(err)
	}

	return &result
}

// close closes the test client but does not terminate the local server
func (tc *testClient) close(ctx context.Context) {
	tc.client.Close(ctx)
}

func (tc *testClient) wipeAllTables(ctx context.Context) error {
	conn := tc.client.getConnection(ctx)

	// Get the names of all tables to wipe.  Sort them to ensure deterministic behavior.
	query := dialect.From(goqu.T("pg_tables")).
		Select("tablename").
		Where(goqu.I("schemaname").Eq("public")).
		Order(goqu.I("tablename").Asc())

	sql, _, err := query.ToSQL()
	if err != nil {
		return err
	}

	rows, err := conn.Query(ctx, sql)
	if err != nil {
		return err
	}
	defer rows.Close()

	model := struct {
		tableName string
	}{}
	fields := []interface{}{
		&model.tableName,
	}
	tableNames := []interface{}{}
	for rows.Next() {
		err = rows.Scan(fields...)
		if err != nil {
			return err
		}
		// Exclude special tables from being wiped.
		if _, ok := nonTruncateTables[model.tableName]; !ok {
			tableNames = append(tableNames, model.tableName)
		}
	}

	if len(tableNames) == 0 {
		return fmt.Errorf("function wipeAllTables found no tables to truncate")
	}

	// Wipe all the tables.
	query2 := dialect.Truncate(tableNames...)

	sql2, _, err := query2.ToSQL()
	if err != nil {
		return err
	}

	_, err = conn.Exec(ctx, sql2)
	if err != nil {
		return err
	}

	return nil
}

//////////////////////////////////////////////////////////////////////////////

// Create initial objects to prepare to run tests.  These functions are called
// by several test modules.

// createInitialUsers creates some users for a test.
func createInitialUsers(ctx context.Context, testClient *testClient,
	toCreate []models.User) ([]models.User, map[string]string, error) {
	result := []models.User{}
	username2ID := make(map[string]string)

	for _, user := range toCreate {

		created, err := testClient.client.Users.CreateUser(ctx, &user)
		if err != nil {
			return nil, nil, err
		}

		result = append(result, *created)
		username2ID[created.Username] = created.Metadata.ID
	}

	return result, username2ID, nil
}

type sortableField interface {
	getFieldDescriptor() *pagination.FieldDescriptor
	getSortDirection() pagination.SortDirection
	getValue() string
}

func testResourcePaginationAndSorting(
	ctx context.Context,
	t *testing.T,
	totalCount int,
	sortableFields []sortableField,
	getResourcesFunc func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error),
) {
	/* Test pagination in forward direction */
	defaultSortBy := sortableFields[0]

	middleIndex := totalCount / 2
	pageInfo, resources, err := getResourcesFunc(ctx, defaultSortBy, &pagination.Options{
		First: ptr.Int32(int32(middleIndex)),
	})
	require.Nil(t, err)

	assert.Equal(t, middleIndex, len(resources))
	assert.True(t, pageInfo.HasNextPage)
	assert.False(t, pageInfo.HasPreviousPage)

	cursor, err := pageInfo.Cursor(resources[len(resources)-1])
	require.Nil(t, err)

	remaining := totalCount - middleIndex
	pageInfo, resources, err = getResourcesFunc(ctx, defaultSortBy, &pagination.Options{
		First: ptr.Int32(int32(remaining)),
		After: cursor,
	})
	require.Nil(t, err)

	assert.Equal(t, remaining, len(resources))
	assert.True(t, pageInfo.HasPreviousPage)
	assert.False(t, pageInfo.HasNextPage)

	/* Test pagination in reverse direction */

	pageInfo, resources, err = getResourcesFunc(ctx, defaultSortBy, &pagination.Options{
		Last: ptr.Int32(int32(middleIndex)),
	})
	require.Nil(t, err)

	assert.Equal(t, middleIndex, len(resources))
	assert.False(t, pageInfo.HasNextPage)
	assert.True(t, pageInfo.HasPreviousPage)

	cursor, err = pageInfo.Cursor(resources[len(resources)-1])
	require.Nil(t, err)

	remaining = totalCount - middleIndex
	pageInfo, resources, err = getResourcesFunc(ctx, defaultSortBy, &pagination.Options{
		Last:   ptr.Int32(int32(remaining)),
		Before: cursor,
	})
	require.Nil(t, err)

	assert.Equal(t, remaining, len(resources))
	assert.False(t, pageInfo.HasPreviousPage)
	assert.True(t, pageInfo.HasNextPage)

	/* Test sorting */
	for _, sortByField := range sortableFields {
		_, resources, err = getResourcesFunc(ctx, sortByField, &pagination.Options{})
		require.Nil(t, err)

		values := []string{}
		for _, resource := range resources {
			value, err := resource.ResolveMetadata(sortByField.getFieldDescriptor().Key)
			require.Nil(t, err)
			values = append(values, value)
		}

		// Must detect whether values are iso8601 timestamps, which are similar enough to RFC 3339 to use that layout.
		// If they are, must convert them and sort as time.Time rather than as strings.
		// That is because truncated trailing zeros cause string comparison to be different vs. time value comparison.
		areAllTimestamps := true
		timeValues := []time.Time{}
		for _, value := range values {
			tv, err := time.Parse(time.RFC3339, value)
			if err != nil {
				areAllTimestamps = false
				break
			}
			timeValues = append(timeValues, tv)
		}

		if areAllTimestamps {
			// Time value sort/comparison.
			expectedTimes := []time.Time{}
			expectedTimes = append(expectedTimes, timeValues...)

			slices.SortFunc(expectedTimes, func(a, b time.Time) int {
				if sortByField.getSortDirection() == pagination.AscSort {
					return int(a.Sub(b)) // positive if a is later/greater than g
				}
				return int(b.Sub(a)) // positive if b is later/greater than a
			})

			assert.Equal(t, expectedTimes, timeValues, "resources are not sorted correctly when using sort by %s", sortByField.getValue())
		} else {
			// Ordinary string sort/comparison.
			expectedValues := []string{}
			expectedValues = append(expectedValues, values...)

			slices.SortFunc(expectedValues, func(a, b string) int {
				if sortByField.getSortDirection() == pagination.AscSort {
					return cmp.Compare(a, b)
				}
				return cmp.Compare(b, a)
			})

			assert.Equal(t, expectedValues, values, "resources are not sorted correctly when using sort by %s", sortByField.getValue())
		}
	}
}

//////////////////////////////////////////////////////////////////////////////

// Other utility function(s):

// reverseStringSlice returns a new []string in the reverse order of the original.
// The original is not modified.
// This is not optimized for speed.
// It is used by multiple test modules.
func reverseStringSlice(input []string) []string {
	result := []string{}

	for i := len(input) - 1; i >= 0; i-- {
		result = append(result, input[i])
	}

	return result
}

// Compare one actual time vs. an expect interval.
// Use the negative sense, because we want >= and <=, while time gives us > and <.
func compareTime(t *testing.T, expectedLow, expectedHigh, actual *time.Time) {
	assert.False(t, actual.Before(*expectedLow))
	assert.False(t, actual.After(*expectedHigh))
}
