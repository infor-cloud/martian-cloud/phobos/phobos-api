package db

//go:generate go tool mockery --name Releases --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// releaseParticipant represents a participant in a release
type releaseParticipant struct {
	UserID    *string
	TeamID    *string
	ReleaseID string
}

// Releases is the interface for interacting with the releases table.
type Releases interface {
	GetReleaseByID(ctx context.Context, id string) (*models.Release, error)
	GetReleaseByPRN(ctx context.Context, prn string) (*models.Release, error)
	GetReleases(ctx context.Context, input *GetReleasesInput) (*ReleasesResult, error)
	CreateRelease(ctx context.Context, release *models.Release) (*models.Release, error)
	UpdateRelease(ctx context.Context, release *models.Release) (*models.Release, error)
	DeleteRelease(ctx context.Context, release *models.Release) error
}

// ReleaseFilter contains the supported fields for filtering Release resources
type ReleaseFilter struct {
	TimeRangeStart         *time.Time
	ProjectID              *string
	UserParticipantID      *string
	UserMemberID           *string
	ServiceAccountMemberID *string
	Latest                 *bool
	ReleaseIDs             []string
}

// ReleaseSortableField represents the fields that a release can be sorted by
type ReleaseSortableField string

// ReleaseSortableField constants
const (
	ReleaseSortableFieldUpdatedAtAsc  ReleaseSortableField = "UPDATED_AT_ASC"
	ReleaseSortableFieldUpdatedAtDesc ReleaseSortableField = "UPDATED_AT_DESC"
	ReleaseSortableFieldCreatedAtAsc  ReleaseSortableField = "CREATED_AT_ASC"
	ReleaseSortableFieldCreatedAtDesc ReleaseSortableField = "CREATED_AT_DESC"
)

func (r ReleaseSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch r {
	case ReleaseSortableFieldUpdatedAtAsc, ReleaseSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "releases", Col: "updated_at"}
	case ReleaseSortableFieldCreatedAtAsc, ReleaseSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "releases", Col: "created_at"}
	default:
		return nil
	}
}

func (r ReleaseSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(r), "_DESC") {
		return pagination.DescSort
	}

	return pagination.AscSort
}

// GetReleasesInput is the input for listing releases
type GetReleasesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ReleaseSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ReleaseFilter
}

// ReleasesResult contains the response data and page information
type ReleasesResult struct {
	PageInfo *pagination.PageInfo
	Releases []*models.Release
}

var releaseFieldList = append(
	metadataFieldList,
	"created_by",
	"semantic_version",
	"project_id",
	"due_date",
	"pre_release",
	"lifecycle_prn",
	"latest",
)

type releases struct {
	dbClient *Client
}

// NewReleases returns a new Releases instance.
func NewReleases(dbClient *Client) Releases {
	return &releases{
		dbClient: dbClient,
	}
}

func (r *releases) GetReleaseByID(ctx context.Context, id string) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "db.GetReleaseByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	return r.getRelease(ctx, span, goqu.Ex{"releases.id": id})
}

func (r *releases) GetReleaseByPRN(ctx context.Context, prn string) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "db.GetReleaseByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	path, err := models.ReleaseResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, err
	}

	parts := strings.Split(path, "/")

	if len(parts) != 3 {
		return nil, errors.New("invalid PRN format", errors.WithErrorCode(errors.EInvalid))
	}

	return r.getRelease(ctx, span, goqu.Ex{
		"organizations.name":        parts[0],
		"projects.name":             parts[1],
		"releases.semantic_version": parts[2],
	})
}

func (r *releases) GetReleases(ctx context.Context, input *GetReleasesInput) (*ReleasesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetReleases")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("releases.project_id").Eq(*input.Filter.ProjectID))
		}

		if len(input.Filter.ReleaseIDs) > 0 {
			ex = ex.Append(goqu.I("releases.id").In(input.Filter.ReleaseIDs))
		}

		if input.Filter.Latest != nil {
			ex = ex.Append(goqu.I("releases.latest").Eq(*input.Filter.Latest))
		}

		if input.Filter.UserMemberID != nil {
			ex = ex.Append(projectMembershipExpressionBuilder{
				userID: input.Filter.UserMemberID,
			}.build())
		}

		if input.Filter.ServiceAccountMemberID != nil {
			ex = ex.Append(organizationMembershipExpressionBuilder{
				serviceAccountID: input.Filter.ServiceAccountMemberID,
			}.build())
		}

		if input.Filter.TimeRangeStart != nil {
			// Must use UTC here otherwise, queries will return unexpected results.
			ex = ex.Append(goqu.I("releases.created_at").Gte(input.Filter.TimeRangeStart.UTC()))
		}

		if input.Filter.UserParticipantID != nil {
			ex = ex.Append(goqu.I("releases.id").In(
				dialect.From("release_participants").
					Select("release_id").
					Where(
						goqu.Or(
							goqu.I("release_participants.user_id").Eq(*input.Filter.UserParticipantID),
							goqu.I("release_participants.team_id").In(
								dialect.From("team_members").
									Select("team_id").
									Where(goqu.I("user_id").Eq(*input.Filter.UserParticipantID)),
							),
						),
					),
			))
		}
	}

	query := dialect.From(goqu.T("releases")).
		Select(r.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"releases.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "releases", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, r.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	results := []*models.Release{}
	for rows.Next() {
		item, err := scanRelease(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	if err := r.hydrateRelease(ctx, r.dbClient.getConnection(ctx), results); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate release", errors.WithSpan(span))
	}

	result := &ReleasesResult{
		PageInfo: rows.GetPageInfo(),
		Releases: results,
	}

	return result, nil
}

func (r *releases) CreateRelease(ctx context.Context, release *models.Release) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "db.CreateRelease")
	defer span.End()

	timestamp := currentTime()

	tx, err := r.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			r.dbClient.logger.Errorf("failed to rollback tx in DB CreateRelease: %v", txErr)
		}
	}()

	sql, args, err := dialect.From("releases").
		Prepared(true).
		With("releases",
			dialect.Insert("releases").
				Rows(goqu.Record{
					"id":               newResourceID(),
					"version":          initialResourceVersion,
					"created_at":       timestamp,
					"updated_at":       timestamp,
					"created_by":       release.CreatedBy,
					"semantic_version": release.SemanticVersion,
					"project_id":       release.ProjectID,
					"due_date":         release.DueDate,
					"pre_release":      release.PreRelease,
					"lifecycle_prn":    release.LifecyclePRN,
					"latest":           release.Latest,
				}).Returning("*"),
		).Select(r.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"releases.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdRelease, err := scanRelease(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_release_project_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}

			if isUniqueViolation(pgErr) {
				return nil, errors.New("release already exists", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	for ix := range release.UserParticipantIDs {
		if err = r.insertReleaseParticipant(ctx, tx, &releaseParticipant{
			ReleaseID: createdRelease.Metadata.ID,
			UserID:    &release.UserParticipantIDs[ix],
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert release participant", errors.WithSpan(span))
		}
	}

	for ix := range release.TeamParticipantIDs {
		if err = r.insertReleaseParticipant(ctx, tx, &releaseParticipant{
			ReleaseID: createdRelease.Metadata.ID,
			TeamID:    &release.TeamParticipantIDs[ix],
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert release participant", errors.WithSpan(span))
		}
	}

	if err = r.hydrateRelease(ctx, tx, []*models.Release{createdRelease}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate release", errors.WithSpan(span))
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	return createdRelease, nil
}

func (r *releases) UpdateRelease(ctx context.Context, release *models.Release) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateRelease")
	defer span.End()

	timestamp := currentTime()

	tx, err := r.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			r.dbClient.logger.Errorf("failed to rollback tx in DB UpdateRelease: %v", txErr)
		}
	}()

	sql, args, err := dialect.From("releases").
		Prepared(true).
		With("releases",
			dialect.Update("releases").
				Set(goqu.Record{
					"version":    goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at": timestamp,
					"due_date":   release.DueDate,
					"latest":     release.Latest,
				}).
				Where(goqu.Ex{"id": release.Metadata.ID, "version": release.Metadata.Version}).
				Returning("*"),
		).Select(r.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"releases.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedRelease, err := scanRelease(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	deleteReleaseParticipantSQL, args, err := dialect.Delete("release_participants").
		Prepared(true).
		Where(
			goqu.Ex{
				"release_id": release.Metadata.ID,
			},
		).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = tx.Exec(ctx, deleteReleaseParticipantSQL, args...); err != nil {
		return nil, errors.Wrap(err, "failed to execute DB query", errors.WithSpan(span))
	}

	for ix := range release.UserParticipantIDs {
		if err = r.insertReleaseParticipant(ctx, tx, &releaseParticipant{
			ReleaseID: updatedRelease.Metadata.ID,
			UserID:    &release.UserParticipantIDs[ix],
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert release participant", errors.WithSpan(span))
		}
	}

	for ix := range release.TeamParticipantIDs {
		if err = r.insertReleaseParticipant(ctx, tx, &releaseParticipant{
			ReleaseID: updatedRelease.Metadata.ID,
			TeamID:    &release.TeamParticipantIDs[ix],
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert release participant", errors.WithSpan(span))
		}
	}

	if err = r.hydrateRelease(ctx, tx, []*models.Release{updatedRelease}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate release", errors.WithSpan(span))
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	return updatedRelease, nil
}

func (r *releases) DeleteRelease(ctx context.Context, release *models.Release) error {
	ctx, span := tracer.Start(ctx, "db.DeleteRelease")
	defer span.End()

	sql, args, err := dialect.From("releases").
		Prepared(true).
		With("releases",
			dialect.Delete("releases").
				Where(goqu.Ex{"id": release.Metadata.ID, "version": release.Metadata.Version}).
				Returning("*"),
		).Select(r.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"releases.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanRelease(r.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (r *releases) insertReleaseParticipant(ctx context.Context, con connection, participant *releaseParticipant) error {
	sql, args, err := dialect.Insert("release_participants").
		Prepared(true).
		Rows(goqu.Record{
			"id":         newResourceID(),
			"release_id": participant.ReleaseID,
			"user_id":    participant.UserID,
			"team_id":    participant.TeamID,
		}).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL")
	}

	if _, err := con.Exec(ctx, sql, args...); err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_release_participants_release_id":
					return errors.New("release does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_release_participants_user_id":
					return errors.New("user does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_release_participants_team_id":
					return errors.New("team does not exist", errors.WithErrorCode(errors.ENotFound))
				}
			}

			if isUniqueViolation(pgErr) {
				return errors.New("release participant already exists", errors.WithErrorCode(errors.EConflict))
			}
		}

		return errors.Wrap(err, "failed to execute query")
	}

	return nil
}

func (r *releases) hydrateRelease(ctx context.Context, conn connection, releases []*models.Release) error {
	if len(releases) == 0 {
		return nil
	}

	releaseMap := map[string]*models.Release{}
	releaseIDs := []string{}
	for _, release := range releases {
		release.UserParticipantIDs = []string{}
		release.TeamParticipantIDs = []string{}

		releaseMap[release.Metadata.ID] = release
		releaseIDs = append(releaseIDs, release.Metadata.ID)
	}

	sql, args, err := dialect.From("release_participants").
		Prepared(true).
		Select("release_id", "user_id", "team_id").
		Where(goqu.I("release_id").In(releaseIDs)).ToSQL()
	if err != nil {
		return err
	}

	rows, err := conn.Query(ctx, sql, args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	// Scan rows
	for rows.Next() {
		participant, err := scanReleaseParticipant(rows)
		if err != nil {
			return err
		}

		release, ok := releaseMap[participant.ReleaseID]
		if !ok {
			return fmt.Errorf("release not found in release map: %s", participant.ReleaseID)
		}

		if participant.UserID != nil {
			release.UserParticipantIDs = append(release.UserParticipantIDs, *participant.UserID)
		}

		if participant.TeamID != nil {
			release.TeamParticipantIDs = append(release.TeamParticipantIDs, *participant.TeamID)
		}
	}

	return nil
}

func (r *releases) getRelease(ctx context.Context, span trace.Span, exp exp.Expression) (*models.Release, error) {
	query := dialect.From(goqu.T("releases")).
		Prepared(true).
		Select(r.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"releases.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	release, err := scanRelease(r.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, "invalid ID; %s", pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	if err := r.hydrateRelease(ctx, r.dbClient.getConnection(ctx), []*models.Release{release}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate release", errors.WithSpan(span))
	}

	return release, nil
}

func (*releases) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range releaseFieldList {
		selectFields = append(selectFields, fmt.Sprintf("releases.%s", field))
	}

	selectFields = append(selectFields, "organizations.name")
	selectFields = append(selectFields, "projects.name")

	return selectFields
}

func scanRelease(row scanner) (*models.Release, error) {
	var organizationName string
	var projectName string
	r := &models.Release{}

	fields := []interface{}{
		&r.Metadata.ID,
		&r.Metadata.CreationTimestamp,
		&r.Metadata.LastUpdatedTimestamp,
		&r.Metadata.Version,
		&r.CreatedBy,
		&r.SemanticVersion,
		&r.ProjectID,
		&r.DueDate,
		&r.PreRelease,
		&r.LifecyclePRN,
		&r.Latest,
		&organizationName,
		&projectName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	r.Metadata.PRN = models.ReleaseResource.BuildPRN(
		organizationName,
		projectName,
		r.SemanticVersion,
	)

	return r, nil
}

func scanReleaseParticipant(row scanner) (*releaseParticipant, error) {
	participant := &releaseParticipant{}

	if err := row.Scan(
		&participant.ReleaseID,
		&participant.UserID,
		&participant.TeamID,
	); err != nil {
		return nil, err
	}

	return participant, nil
}
