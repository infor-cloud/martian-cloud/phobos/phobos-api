package db

//go:generate go tool mockery --name TeamMembers --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// TeamMembers encapsulates the logic to access team members from the database
type TeamMembers interface {
	GetTeamMember(ctx context.Context, userID, teamID string) (*models.TeamMember, error)
	GetTeamMembers(ctx context.Context, input *GetTeamMembersInput) (*TeamMembersResult, error)
	AddUserToTeam(ctx context.Context, teamMember *models.TeamMember) (*models.TeamMember, error)
	UpdateTeamMember(ctx context.Context, teamMember *models.TeamMember) (*models.TeamMember, error)
	RemoveUserFromTeam(ctx context.Context, teamMember *models.TeamMember) error
}

// TeamMemberFilter contains the supported fields for filtering TeamMember resources
type TeamMemberFilter struct {
	UserID         *string
	TeamIDs        []string
	MaintainerOnly bool
}

// TeamMemberSortableField represents the fields that a team member can be sorted by
type TeamMemberSortableField string

// TeamMemberSortableField constants
const (
	UsernameAsc  TeamMemberSortableField = "USERNAME_ASC"
	UsernameDesc TeamMemberSortableField = "USERNAME_DESC"
)

func (tms TeamMemberSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch tms {
	// Placeholder for any future sorting field assignments.
	default:
		return nil
	}
}

func (tms TeamMemberSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(tms), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetTeamMembersInput is the input for listing team members
type GetTeamMembersInput struct {
	// Sort specifies the field to sort on and direction
	Sort *TeamMemberSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *TeamMemberFilter
}

// TeamMembersResult contains the response data and page information
type TeamMembersResult struct {
	PageInfo    *pagination.PageInfo
	TeamMembers []models.TeamMember
}

var teamMemberFieldList = append(metadataFieldList, "user_id", "team_id", "is_maintainer")

type teamMembers struct {
	dbClient *Client
}

// NewTeamMembers returns an instance of the TeamMembers interface
func NewTeamMembers(dbClient *Client) TeamMembers {
	return &teamMembers{dbClient: dbClient}
}

func (tm *teamMembers) GetTeamMember(ctx context.Context,
	userID, teamID string) (*models.TeamMember, error) {
	ctx, span := tracer.Start(ctx, "db.GetTeamMember")
	defer span.End()

	return tm.getTeamMember(ctx, goqu.Ex{"team_members.user_id": userID, "team_members.team_id": teamID})
}

func (tm *teamMembers) GetTeamMembers(ctx context.Context, input *GetTeamMembersInput) (*TeamMembersResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetTeamMembers")
	defer span.End()

	ex := goqu.Ex{}

	if input.Filter != nil {
		if input.Filter.UserID != nil {
			ex["team_members.user_id"] = *input.Filter.UserID
		}

		if input.Filter.TeamIDs != nil {
			ex["team_members.team_id"] = input.Filter.TeamIDs
		}

		if input.Filter.MaintainerOnly {
			ex["team_members.maintainer_only"] = true
		}
	}

	query := dialect.From(goqu.T("team_members")).
		Select(teamMemberFieldList...).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "team_members", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, tm.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.TeamMember{}
	for rows.Next() {
		item, err := scanTeamMember(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := TeamMembersResult{
		PageInfo:    rows.GetPageInfo(),
		TeamMembers: results,
	}

	return &result, nil
}

func (tm *teamMembers) AddUserToTeam(ctx context.Context, teamMember *models.TeamMember) (*models.TeamMember, error) {
	ctx, span := tracer.Start(ctx, "db.AddUserToTeam")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Insert("team_members").
		Prepared(true).
		Rows(goqu.Record{
			"id":            newResourceID(),
			"version":       initialResourceVersion,
			"created_at":    timestamp,
			"updated_at":    timestamp,
			"user_id":       teamMember.UserID,
			"team_id":       teamMember.TeamID,
			"is_maintainer": teamMember.IsMaintainer,
		}).
		Returning(teamMemberFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdTeamMember, err := scanTeamMember(tm.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {

				// Don't have the username or team name, so need to fetch them for printing the error message.
				username := ""
				userRecord, uErr := tm.dbClient.Users.GetUserByID(ctx, teamMember.UserID)
				if uErr != nil {
					// Return original error but with user ID.
					username = fmt.Sprintf("ID %s", teamMember.UserID)
				} else {
					username = userRecord.Username
				}

				teamName := ""
				teamRecord, tErr := tm.dbClient.Teams.GetTeamByID(ctx, teamMember.TeamID)
				if tErr != nil {
					// Return the original error but with the team ID.
					teamName = fmt.Sprintf("ID %s", teamMember.TeamID)
				} else {
					teamName = teamRecord.Name
				}

				return nil, errors.New(
					"team member of user %s in team %s already exists",
					username, teamName, errors.WithSpan(span), errors.WithErrorCode(errors.EConflict))
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdTeamMember, nil
}

func (tm *teamMembers) UpdateTeamMember(ctx context.Context, teamMember *models.TeamMember) (*models.TeamMember, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateTeamMember")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.Update("team_members").
		Prepared(true).
		Set(
			goqu.Record{
				"version":       goqu.L("? + ?", goqu.C("version"), 1),
				"updated_at":    timestamp,
				"is_maintainer": teamMember.IsMaintainer,
			},
		).Where(goqu.Ex{"id": teamMember.Metadata.ID, "version": teamMember.Metadata.Version}).
		Returning(teamMemberFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedTeamMember, err := scanTeamMember(tm.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))

	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedTeamMember, nil
}

func (tm *teamMembers) RemoveUserFromTeam(ctx context.Context, teamMember *models.TeamMember) error {
	ctx, span := tracer.Start(ctx, "db.RemoveUserFromTeam")
	defer span.End()

	sql, args, err := dialect.Delete("team_members").
		Prepared(true).
		Where(
			goqu.Ex{
				"id":      teamMember.Metadata.ID,
				"version": teamMember.Metadata.Version,
			},
		).Returning(teamMemberFieldList...).ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	_, err = scanTeamMember(tm.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (tm *teamMembers) getTeamMember(ctx context.Context, exp exp.Expression) (*models.TeamMember, error) {
	query := dialect.From(goqu.T("team_members")).
		Prepared(true).
		Select(teamMemberFieldList...).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	teamMember, err := scanTeamMember(tm.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return teamMember, nil
}

func scanTeamMember(row scanner) (*models.TeamMember, error) {
	teamMember := &models.TeamMember{}

	fields := []interface{}{
		&teamMember.Metadata.ID,
		&teamMember.Metadata.CreationTimestamp,
		&teamMember.Metadata.LastUpdatedTimestamp,
		&teamMember.Metadata.Version,
		&teamMember.UserID,
		&teamMember.TeamID,
		&teamMember.IsMaintainer,
	}

	err := row.Scan(fields...)
	if err != nil {
		return nil, err
	}

	teamMember.Metadata.PRN = models.TeamMemberResource.BuildPRN(gid.ToGlobalID(gid.TeamMemberType, teamMember.Metadata.ID))

	return teamMember, nil
}
