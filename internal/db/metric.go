package db

//go:generate go tool mockery --name Metrics --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Metrics encapsulates the logic to access metrics from the DB.
type Metrics interface {
	GetMetrics(ctx context.Context, input *GetMetricsInput) (*MetricsResult, error)
	GetMetricStatistics(ctx context.Context, input *MetricFilter) (*MetricStatistics, error)
	GetAggregatedMetricStatistics(ctx context.Context, input *GetAggregatedMetricsInput) ([]*AggregatedMetric, error)
	CreateMetric(ctx context.Context, metric *models.Metric) (*models.Metric, error)
}

// MetricsResult contains the response data and page information
type MetricsResult struct {
	PageInfo *pagination.PageInfo
	Metrics  []*models.Metric
}

// MetricSortableField represents the fields that a metric can be sorted by
type MetricSortableField string

// MetricSortableField constants
const (
	MetricSortableFieldCreatedAtAsc  MetricSortableField = "CREATED_AT_ASC"
	MetricSortableFieldCreatedAtDesc MetricSortableField = "CREATED_AT_DESC"
)

func (os MetricSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case MetricSortableFieldCreatedAtAsc, MetricSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "metrics", Col: "created_at"}
	default:
		return nil
	}
}

func (os MetricSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetMetricsInput is the input for listing metrics
type GetMetricsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *MetricSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *MetricFilter
}

// MetricStatistics represents the statistics for a metric.
type MetricStatistics struct {
	Minimum float64
	Maximum float64
	Average float64
	Sum     float64
}

// MetricBucketPeriod represents the period to aggregate the metrics by
type MetricBucketPeriod string

// MetricBucketPeriodField constants
const (
	MetricBucketPeriodHour  MetricBucketPeriod = "hour"
	MetricBucketPeriodDay   MetricBucketPeriod = "day"
	MetricBucketPeriodMonth MetricBucketPeriod = "month"
)

// GetAggregatedMetricsInput is the input for querying aggregated metrics
type GetAggregatedMetricsInput struct {
	// Filter is used to filter the results
	Filter *MetricFilter
	// BucketPeriod is the period to aggregate the metrics by
	BucketPeriod MetricBucketPeriod
	// BucketCount is the number of buckets to return
	BucketCount int
}

// AggregatedMetric represents the aggregated metric statistics
type AggregatedMetric struct {
	Bucket time.Time
	Stats  MetricStatistics
}

// MetricFilter is the filter to apply when querying metrics.
type MetricFilter struct {
	TimeRangeStart  *time.Time
	TimeRangeEnd    *time.Time
	OrganizationID  *string
	ProjectID       *string
	ReleaseID       *string
	PipelineID      *string
	EnvironmentName *string
	MetricName      *models.MetricName
	Tags            map[models.MetricTagName]string
}

type metrics struct {
	dbClient *Client
}

var metricFieldList = []any{
	"id",
	"created_at",
	"name",
	"value",
	"tags",
	"pipeline_target_id",
	"release_target_id",
	"project_target_id",
	"organization_target_id",
	"environment_name",
}

// NewMetrics returns an instance of Metrics interface.
func NewMetrics(dbClient *Client) Metrics {
	return &metrics{dbClient: dbClient}
}

func (m *metrics) GetMetrics(ctx context.Context, input *GetMetricsInput) (*MetricsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetMetrics")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.MetricName != nil {
			ex = ex.Append(goqu.I("metrics.name").Eq(*input.Filter.MetricName))
		}

		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.C("organization_target_id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.C("project_target_id").Eq(*input.Filter.ProjectID))
		}

		if input.Filter.ReleaseID != nil {
			ex = ex.Append(goqu.C("release_target_id").Eq(*input.Filter.ReleaseID))
		}

		if input.Filter.PipelineID != nil {
			ex = ex.Append(goqu.C("pipeline_target_id").Eq(*input.Filter.PipelineID))
		}

		if input.Filter.EnvironmentName != nil {
			ex = ex.Append(goqu.C("environment_name").Eq(*input.Filter.EnvironmentName))
		}

		for key, value := range input.Filter.Tags {
			ex = ex.Append(goqu.L(fmt.Sprintf("tags->>'%s'", key)).Eq(value))
		}

		if input.Filter.TimeRangeStart != nil {
			ex = ex.Append(goqu.C("created_at").Gte(input.Filter.TimeRangeStart.UTC()))
		}

		if input.Filter.TimeRangeEnd != nil {
			ex = ex.Append(goqu.C("created_at").Lt(input.Filter.TimeRangeEnd.UTC()))
		}
	}

	query := dialect.From(goqu.T("metrics")).
		Select(m.getSelectFields()...).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "metrics", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, m.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.Metric{}
	for rows.Next() {
		item, err := scanMetric(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &MetricsResult{
		PageInfo: rows.GetPageInfo(),
		Metrics:  results,
	}

	return result, nil
}

func (m *metrics) GetAggregatedMetricStatistics(ctx context.Context, input *GetAggregatedMetricsInput) ([]*AggregatedMetric, error) {
	ctx, span := tracer.Start(ctx, "db.GetMetricStatistics")
	defer span.End()

	ex := goqu.And()

	if input.Filter.MetricName != nil {
		ex = ex.Append(goqu.I("metrics.name").Eq(*input.Filter.MetricName))
	}

	if input.Filter.OrganizationID != nil {
		ex = ex.Append(goqu.C("organization_target_id").Eq(*input.Filter.OrganizationID))
	}

	if input.Filter.ProjectID != nil {
		ex = ex.Append(goqu.C("project_target_id").Eq(*input.Filter.ProjectID))
	}

	if input.Filter.ReleaseID != nil {
		ex = ex.Append(goqu.C("release_target_id").Eq(*input.Filter.ReleaseID))
	}

	if input.Filter.PipelineID != nil {
		ex = ex.Append(goqu.C("pipeline_target_id").Eq(*input.Filter.PipelineID))
	}

	if input.Filter.EnvironmentName != nil {
		ex = ex.Append(goqu.C("environment_name").Eq(*input.Filter.EnvironmentName))
	}

	for key, value := range input.Filter.Tags {
		ex = ex.Append(goqu.L(fmt.Sprintf("tags->>'%s'", key)).Eq(value))
	}

	if input.Filter.TimeRangeStart != nil {
		ex = ex.Append(goqu.C("created_at").Gte(input.Filter.TimeRangeStart.UTC()))
	}

	if input.Filter.TimeRangeEnd != nil {
		ex = ex.Append(goqu.C("created_at").Lt(input.Filter.TimeRangeEnd.UTC()))
	}

	sql, args, err := dialect.From("buckets").
		Prepared(true).
		With("buckets", goqu.Select(
			goqu.Func(
				"generate_series",
				goqu.L(fmt.Sprintf("date_trunc('%[1]s', now()) - '%[2]d %[1]s'::interval", input.BucketPeriod, input.BucketCount-1)),
				goqu.L(fmt.Sprintf("date_trunc('%s', now())", input.BucketPeriod)),
				goqu.L(fmt.Sprintf("'1 %s'::interval", input.BucketPeriod))).
				As("bucket"),
		)).
		Select(
			goqu.I("buckets.bucket"),
			goqu.SUM("value").As("sum"),
			goqu.MIN("value").As("minimum"),
			goqu.MAX("value").As("maximum"),
			goqu.AVG("value").As("average"),
		).
		LeftJoin(goqu.T("metrics"), goqu.On(goqu.And(goqu.L(fmt.Sprintf("date_trunc('%s', metrics.created_at)", input.BucketPeriod)).Eq(goqu.I("buckets.bucket")), ex))).
		GroupBy(goqu.L("1")).
		Order(goqu.I("buckets.bucket").Asc()).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := m.dbClient.getConnection(ctx).Query(ctx, sql, args...) // nosemgrep: gosec.G202-1
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	results := []*AggregatedMetric{}

	for rows.Next() {
		var bucket time.Time
		var sum, min, max, avg *float64

		if err := rows.Scan(
			&bucket,
			&sum,
			&min,
			&max,
			&avg,
		); err != nil {
			return nil, errors.Wrap(err, "failed to scan metric db result", errors.WithSpan(span))
		}

		result := MetricStatistics{}
		if sum != nil {
			result.Sum = *sum
		}
		if min != nil {
			result.Minimum = *min
		}
		if max != nil {
			result.Maximum = *max
		}
		if avg != nil {
			result.Average = *avg
		}

		results = append(results, &AggregatedMetric{
			Bucket: bucket.UTC(),
			Stats:  result,
		})
	}

	return results, nil
}

func (m *metrics) GetMetricStatistics(ctx context.Context, filter *MetricFilter) (*MetricStatistics, error) {
	ctx, span := tracer.Start(ctx, "db.GetMetricStatistics")
	defer span.End()

	ex := goqu.And()

	if filter.MetricName != nil {
		ex = ex.Append(goqu.I("metrics.name").Eq(*filter.MetricName))
	}

	if filter.OrganizationID != nil {
		ex = ex.Append(goqu.C("organization_target_id").Eq(*filter.OrganizationID))
	}

	if filter.ProjectID != nil {
		ex = ex.Append(goqu.C("project_target_id").Eq(*filter.ProjectID))
	}

	if filter.ReleaseID != nil {
		ex = ex.Append(goqu.C("release_target_id").Eq(*filter.ReleaseID))
	}

	if filter.PipelineID != nil {
		ex = ex.Append(goqu.C("pipeline_target_id").Eq(*filter.PipelineID))
	}

	if filter.EnvironmentName != nil {
		ex = ex.Append(goqu.C("environment_name").Eq(*filter.EnvironmentName))
	}

	for key, value := range filter.Tags {
		ex = ex.Append(goqu.L(fmt.Sprintf("tags->>'%s'", key)).Eq(value))
	}

	if filter.TimeRangeStart != nil {
		ex = ex.Append(goqu.C("created_at").Gte(filter.TimeRangeStart.UTC()))
	}

	if filter.TimeRangeEnd != nil {
		ex = ex.Append(goqu.C("created_at").Lt(filter.TimeRangeEnd.UTC()))
	}

	sql, args, err := dialect.From("metrics").
		Prepared(true).
		Select(
			goqu.SUM("value").As("sum"),
			goqu.MIN("value").As("minimum"),
			goqu.MAX("value").As("maximum"),
			goqu.AVG("value").As("average"),
		).
		Where(ex).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	row := m.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...) // nosemgrep: gosec.G202-1

	result := &MetricStatistics{}

	if err := row.Scan(
		&result.Sum,
		&result.Minimum,
		&result.Maximum,
		&result.Average,
	); err != nil {
		if _, ok := err.(pgx.ScanArgError); ok {
			return result, nil
		}
		return nil, errors.Wrap(err, "failed to scan result", errors.WithSpan(span))
	}

	return result, nil
}

func (m *metrics) CreateMetric(ctx context.Context, metric *models.Metric) (*models.Metric, error) {
	ctx, span := tracer.Start(ctx, "db.CreateMetric")
	defer span.End()

	var marshalledTags []byte
	if metric.Tags != nil {
		buf, err := json.Marshal(metric.Tags)
		if err != nil {
			return nil, errors.Wrap(err, "failed to marshal metric tags")
		}
		marshalledTags = buf
	}

	timestamp := currentTime()

	sql, args, err := dialect.Insert("metrics").
		Prepared(true).
		Rows(
			goqu.Record{
				"id":                     newResourceID(),
				"created_at":             timestamp,
				"name":                   metric.Name,
				"value":                  metric.Value,
				"tags":                   marshalledTags,
				"pipeline_target_id":     metric.PipelineID,
				"release_target_id":      metric.ReleaseID,
				"project_target_id":      metric.ProjectID,
				"organization_target_id": metric.OrganizationID,
				"environment_name":       metric.EnvironmentName,
			}).Returning(m.getSelectFields()...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	createdMetric, err := scanMetric(m.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_metrics_pipeline_target_id":
					return nil, errors.New("pipeline does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_metrics_release_target_id":
					return nil, errors.New("release does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_metrics_project_target_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_metrics_organization_target_id":
					return nil, errors.New("organization does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdMetric, nil
}

func (*metrics) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range metricFieldList {
		selectFields = append(selectFields, fmt.Sprintf("metrics.%s", field))
	}

	return selectFields
}

func scanMetric(row scanner) (*models.Metric, error) {
	metric := &models.Metric{}

	fields := []interface{}{
		&metric.Metadata.ID,
		&metric.Metadata.CreationTimestamp,
		&metric.Name,
		&metric.Value,
		&metric.Tags,
		&metric.PipelineID,
		&metric.ReleaseID,
		&metric.ProjectID,
		&metric.OrganizationID,
		&metric.EnvironmentName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	metric.Metadata.Version = initialResourceVersion
	metric.Metadata.LastUpdatedTimestamp = metric.Metadata.CreationTimestamp

	metric.Metadata.PRN = models.MetricResource.BuildPRN(gid.ToGlobalID(gid.MetricType, metric.Metadata.ID))

	return metric, nil
}
