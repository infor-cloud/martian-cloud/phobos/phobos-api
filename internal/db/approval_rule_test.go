//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (os ApprovalRuleSortableField) getValue() string {
	return string(os)
}

func TestGetApprovalRuleByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	approvalRule, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Name:  "test-rule",
		Scope: models.OrganizationScope,
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode    errors.CodeType
		name               string
		id                 string
		expectApprovalRule bool
	}

	testCases := []testCase{
		{
			name:               "get resource by id",
			id:                 approvalRule.Metadata.ID,
			expectApprovalRule: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			approvalRule, err := testClient.client.ApprovalRules.GetApprovalRuleByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectApprovalRule {
				require.NotNil(t, approvalRule)
				assert.Equal(t, test.id, approvalRule.Metadata.ID)
			} else {
				assert.Nil(t, approvalRule)
			}
		})
	}
}

func TestGetApprovalRuleByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	proj, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		OrgID: org.Metadata.ID,
		Name:  "test-proj",
	})

	orgApprovalRule, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Name:  "test-rule",
		Scope: models.OrganizationScope,
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	projApprovalRule, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Name:      "test-rule",
		Scope:     models.ProjectScope,
		OrgID:     org.Metadata.ID,
		ProjectID: &proj.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode    errors.CodeType
		name               string
		prn                string
		expectApprovalRule bool
	}

	testCases := []testCase{
		{
			name:               "get org resource by PRN",
			prn:                orgApprovalRule.Metadata.PRN,
			expectApprovalRule: true,
		},
		{
			name:               "get proj resource by PRN",
			prn:                projApprovalRule.Metadata.PRN,
			expectApprovalRule: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.ApprovalRuleResource.BuildPRN(nonExistentID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			approvalRule, err := testClient.client.ApprovalRules.GetApprovalRuleByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectApprovalRule {
				require.NotNil(t, approvalRule)
				assert.Equal(t, test.prn, approvalRule.Metadata.PRN)
			} else {
				assert.Nil(t, approvalRule)
			}
		})
	}
}

func TestCreateApprovalRule(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	user, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user",
	})
	require.Nil(t, err)

	serviceAccount, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-sa",
		Scope:          models.OrganizationScope,
		OrganizationID: &org.Metadata.ID,
	})
	require.Nil(t, err)
	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.Nil(t, err)

	type testCase struct {
		name              string
		expectErrorCode   errors.CodeType
		ruleName          string
		scope             models.ScopeType
		orgID             string
		projectID         *string
		userIDs           []string
		serviceAccountIDs []string
		teamIDs           []string
	}

	testCases := []testCase{
		{
			name:              "successfully create organization-scope resource",
			ruleName:          "rule1",
			scope:             models.OrganizationScope,
			orgID:             org.Metadata.ID,
			userIDs:           []string{user.Metadata.ID},
			serviceAccountIDs: []string{serviceAccount.Metadata.ID},
			teamIDs:           []string{team.Metadata.ID},
		},
		{
			name:              "successfully create project-scope resource",
			ruleName:          "rule1-proj-scope",
			scope:             models.ProjectScope,
			orgID:             org.Metadata.ID,
			projectID:         &project.Metadata.ID,
			userIDs:           []string{user.Metadata.ID},
			serviceAccountIDs: []string{serviceAccount.Metadata.ID},
			teamIDs:           []string{team.Metadata.ID},
		},
		{
			name:            "duplicate rule name",
			ruleName:        "rule1",
			scope:           models.OrganizationScope,
			orgID:           org.Metadata.ID,
			expectErrorCode: errors.EConflict,
		},
		{
			name:            "create will fail because user does not exist",
			ruleName:        "rule2",
			scope:           models.OrganizationScope,
			orgID:           org.Metadata.ID,
			userIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "non-existent organization ID",
			ruleName:        "rule3",
			scope:           models.OrganizationScope,
			orgID:           nonExistentID,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "non-existent project ID",
			ruleName:        "rule4",
			scope:           models.ProjectScope,
			orgID:           org.Metadata.ID,
			projectID:       ptr.String(nonExistentID),
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			approvalRule, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
				Name:              test.ruleName,
				Scope:             test.scope,
				OrgID:             test.orgID,
				ProjectID:         test.projectID,
				UserIDs:           test.userIDs,
				ServiceAccountIDs: test.serviceAccountIDs,
				TeamIDs:           test.teamIDs,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, approvalRule)
			assert.Equal(t, test.userIDs, approvalRule.UserIDs)
			assert.Equal(t, test.serviceAccountIDs, approvalRule.ServiceAccountIDs)
			assert.Equal(t, test.teamIDs, approvalRule.TeamIDs)
		})
	}
}

func TestUpdateApprovalRule(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	user1, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user1",
		Email:    "user1@test.com",
	})
	require.Nil(t, err)

	user2, err := testClient.client.Users.CreateUser(ctx, &models.User{
		Username: "test-user2",
		Email:    "user2@test.com",
	})
	require.Nil(t, err)

	serviceAccount, err := testClient.client.ServiceAccounts.CreateServiceAccount(ctx, &models.ServiceAccount{
		Name:           "test-sa",
		Scope:          models.OrganizationScope,
		OrganizationID: &org.Metadata.ID,
	})
	require.Nil(t, err)

	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.Nil(t, err)

	approvalRule, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Name:              "test-rule",
		Scope:             models.OrganizationScope,
		OrgID:             org.Metadata.ID,
		UserIDs:           []string{user1.Metadata.ID},
		ServiceAccountIDs: []string{serviceAccount.Metadata.ID},
		TeamIDs:           []string{},
	})
	require.Nil(t, err)

	type testCase struct {
		name              string
		expectErrorCode   errors.CodeType
		userIDs           []string
		serviceAccountIDs []string
		teamIDs           []string
		version           int
	}

	testCases := []testCase{
		{
			name:              "successfully update resource",
			userIDs:           []string{user1.Metadata.ID, user2.Metadata.ID},
			serviceAccountIDs: []string{},
			teamIDs:           []string{team.Metadata.ID},
			version:           1,
		},
		{
			name:            "update will fail because user does not exist",
			userIDs:         []string{nonExistentID},
			expectErrorCode: errors.EInvalid,
			version:         2,
		},
		{
			name:            "update will fail because resource version doesn't match",
			userIDs:         []string{nonExistentID},
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualApprovalRule, err := testClient.client.ApprovalRules.UpdateApprovalRule(ctx, &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID:      approvalRule.Metadata.ID,
					Version: test.version,
				},
				UserIDs:           test.userIDs,
				ServiceAccountIDs: test.serviceAccountIDs,
				TeamIDs:           test.teamIDs,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			require.NotNil(t, actualApprovalRule)
			assert.Equal(t, test.userIDs, actualApprovalRule.UserIDs)
			assert.Equal(t, test.serviceAccountIDs, actualApprovalRule.ServiceAccountIDs)
			assert.Equal(t, test.teamIDs, actualApprovalRule.TeamIDs)
		})
	}
}

func TestDeleteApprovalRule(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	team, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name: "test-team",
	})
	require.Nil(t, err)

	approvalRule, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Name:    "test-rule",
		Scope:   models.OrganizationScope,
		OrgID:   org.Metadata.ID,
		TeamIDs: []string{team.Metadata.ID},
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              approvalRule.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      approvalRule.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.ApprovalRules.DeleteApprovalRule(ctx, &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
		})
	}
}

func TestGetApprovalRules(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org1, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org1",
	})
	require.Nil(t, err)

	org2, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org2",
	})
	require.Nil(t, err)

	proj1a, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1a",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	proj1b, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project-1b",
		OrgID: org1.Metadata.ID,
	})
	require.Nil(t, err)

	rules := make([]*models.ApprovalRule, 10)
	for i := 0; i < len(rules); i++ {
		// a mix of org-scope, proj1a, and proj1b
		var scope models.ScopeType
		var projectID *string
		switch i % 3 {
		case 0: // i=0, 3, 6, 9
			scope = models.OrganizationScope
		case 1: // i=1, 4, 7
			scope = models.ProjectScope
			projectID = &proj1a.Metadata.ID
		case 2: // i=2, 5, 8
			scope = models.ProjectScope
			projectID = &proj1b.Metadata.ID
		}

		rule, aErr := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
			Name:      fmt.Sprintf("test-rule-%d", i),
			Scope:     scope,
			OrgID:     org1.Metadata.ID,
			ProjectID: projectID,
		})
		require.Nil(t, aErr)

		rules[i] = rule
	}

	rule, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Name:  fmt.Sprintf("test-rule-%d", len(rules)),
		Scope: models.OrganizationScope,
		OrgID: org2.Metadata.ID,
	})
	require.Nil(t, err)

	rules = append(rules, rule)

	type testCase struct {
		filter            *ApprovalRuleFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name: "return all rules in org 1",
			filter: &ApprovalRuleFilter{
				OrgID: &org1.Metadata.ID,
			},
			expectResultCount: len(rules) - 1,
		},
		{
			name: "return all rules in org 2",
			filter: &ApprovalRuleFilter{
				OrgID: &org2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "return all rules in org 1 but ONLY at organization scope",
			filter: &ApprovalRuleFilter{
				OrgID:              &org1.Metadata.ID,
				ApprovalRuleScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectResultCount: 4,
		},
		{
			name: "return all rules in org 1 but ONLY at project scope",
			filter: &ApprovalRuleFilter{
				OrgID:              &org1.Metadata.ID,
				ApprovalRuleScopes: []models.ScopeType{models.ProjectScope},
			},
			expectResultCount: 6,
		},
		{
			name: "return all rules in proj 1a, including inherited organization-scope rules",
			filter: &ApprovalRuleFilter{
				ProjectID: &proj1a.Metadata.ID,
			},
			expectResultCount: 7,
		},
		{
			name: "return all rules in proj 1a but ONLY at project scope",
			filter: &ApprovalRuleFilter{
				ProjectID:          &proj1a.Metadata.ID,
				ApprovalRuleScopes: []models.ScopeType{models.ProjectScope},
			},
			expectResultCount: 3,
		},
		{
			name: "return all rules matching specific IDs",
			filter: &ApprovalRuleFilter{
				ApprovalRuleIDs: []string{rules[0].Metadata.ID, rules[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.ApprovalRules.GetApprovalRules(ctx, &GetApprovalRulesInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			assert.Equal(t, test.expectResultCount, len(result.ApprovalRules))
		})
	}
}

func TestGetApprovalRulesWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
			Name:  fmt.Sprintf("test-rule-%d", i),
			Scope: models.OrganizationScope,
			OrgID: org.Metadata.ID,
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		ApprovalRuleSortableFieldUpdatedAtAsc,
		ApprovalRuleSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := ApprovalRuleSortableField(sortByField.getValue())

		result, err := testClient.client.ApprovalRules.GetApprovalRules(ctx, &GetApprovalRulesInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.ApprovalRules {
			resources = append(resources, resource)
		}

		return result.PageInfo, resources, nil
	})
}
