//go:build integration

package db

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// getValue returns the value of the sortable field.
func (sf ThreadSortableField) getValue() string {
	return string(sf)
}

func TestGetThreadByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineThread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	releaseThread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		ReleaseID: &release.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		searchID        string
		expectThread    bool
	}

	testCases := []testCase{
		{
			name:         "get a pipeline thread",
			searchID:     pipelineThread.Metadata.ID,
			expectThread: true,
		},
		{
			name:         "get a release thread",
			searchID:     releaseThread.Metadata.ID,
			expectThread: true,
		},
		{
			name:     "negative, non-existent ID",
			searchID: nonExistentID,
			// expect thread and error to be nil
		},
		{
			name:            "defective-id",
			searchID:        invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			thread, err := testClient.client.Threads.GetThreadByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectThread {
				require.NotNil(t, thread)
				assert.Equal(t, test.searchID, thread.Metadata.ID)
			} else {
				assert.Nil(t, thread)
			}
		})
	}
}

func TestGetThreadByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		searchPRN       string
		expectThread    bool
	}

	testCases := []testCase{
		{
			name:         "get resource by prn",
			searchPRN:    thread.Metadata.PRN,
			expectThread: true,
		},
		{
			name:      "negative, resource does not exist",
			searchPRN: models.ThreadResource.BuildPRN(nonExistentGlobalID),
			// expect thread and error to be nil
		},
		{
			name:            "defective-prn",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualThread, err := testClient.client.Threads.GetThreadByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectThread {
				require.NotNil(t, actualThread)
				assert.Equal(t, test.searchPRN, actualThread.Metadata.PRN)
			} else {
				assert.Nil(t, actualThread)
			}
		})
	}
}

func TestGetThreads(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID:       project.Metadata.ID,
		SemanticVersion: "0.0.1",
	})
	require.Nil(t, err)

	totalThreads := 10
	threads := make([]*models.Thread, 10)
	for i := 0; i < totalThreads; i++ {
		toCreate := &models.Thread{}

		// Alternate between pipeline / release ids for testing both.
		if i%2 == 0 {
			toCreate.PipelineID = &pipeline.Metadata.ID
		} else {
			toCreate.ReleaseID = &release.Metadata.ID
		}

		thread, cErr := testClient.client.Threads.CreateThread(ctx, toCreate)
		require.Nil(t, cErr)

		threads[i] = thread
	}

	type testCase struct {
		name              string
		filter            *ThreadFilter
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all threads",
			filter:            &ThreadFilter{},
			expectResultCount: totalThreads,
		},
		{
			name: "filter by pipeline id",
			filter: &ThreadFilter{
				PipelineID: &pipeline.Metadata.ID,
			},
			expectResultCount: totalThreads / 2,
		},
		{
			name: "filter by release id",
			filter: &ThreadFilter{
				ReleaseID: &release.Metadata.ID,
			},
			expectResultCount: totalThreads / 2,
		},
		{
			name: "filter by unknown pipeline id",
			filter: &ThreadFilter{
				PipelineID: ptr.String(nonExistentID),
			},
		},
		{
			name: "filter by unknown release id",
			filter: &ThreadFilter{
				ReleaseID: ptr.String(nonExistentID),
			},
		},
		{
			name: "filter by specific thread IDs",
			filter: &ThreadFilter{
				ThreadIDs: []string{threads[0].Metadata.ID, threads[1].Metadata.ID},
			},
			expectResultCount: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualResult, err := testClient.client.Threads.GetThreads(ctx, &GetThreadsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			assert.Nil(t, err)
			assert.Len(t, actualResult.Threads, test.expectResultCount)
		})
	}
}

func TestGetThreadsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.Threads.CreateThread(ctx, &models.Thread{
			PipelineID: &pipeline.Metadata.ID,
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		ThreadSortableFieldCreatedAtAsc,
		ThreadSortableFieldCreatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields,
		func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (
			*pagination.PageInfo, []pagination.CursorPaginatable, error,
		) {
			sortBy := ThreadSortableField(sortByField.getValue())

			result, err := testClient.client.Threads.GetThreads(ctx, &GetThreadsInput{
				Sort:              &sortBy,
				PaginationOptions: paginationOptions,
			})
			if err != nil {
				return nil, nil, err
			}

			resources := []pagination.CursorPaginatable{}
			for ix := range result.Threads {
				resources = append(resources, &result.Threads[ix])
			}

			return result.PageInfo, resources, nil
		})
}

func TestCreateThread(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		ProjectID:       project.Metadata.ID,
		SemanticVersion: "0.0.1",
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		pipelineID      *string
		releaseID       *string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "create a pipeline thread",
			pipelineID: &pipeline.Metadata.ID,
		},
		{
			name:      "create a release thread",
			releaseID: &release.Metadata.ID,
		},
		{
			name:            "create fails because pipeline does not exist",
			pipelineID:      ptr.String(nonExistentID),
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "create fails because release does not exist",
			releaseID:       ptr.String(nonExistentID),
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			created, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
				PipelineID: test.pipelineID,
				ReleaseID:  test.releaseID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			assert.NotNil(t, created)
		})
	}
}

func TestDeleteThread(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "organization for testing",
	})
	require.Nil(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "project for testing",
		OrgID: org.Metadata.ID,
	})
	require.Nil(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.Nil(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.Nil(t, err)

	thread, err := testClient.client.Threads.CreateThread(ctx, &models.Thread{
		PipelineID: &pipeline.Metadata.ID,
	})
	require.Nil(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "successfully delete resource",
			version: 1,
		},
		{
			name:            "delete will fail because resource version doesn't match",
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.Threads.DeleteThread(ctx, &models.Thread{
				Metadata: models.ResourceMetadata{
					ID:      thread.Metadata.ID,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
			assert.Nil(t, err)
		})
	}
}
