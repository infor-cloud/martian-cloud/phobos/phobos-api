//go:build integration

package db

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func (os PipelineSortableField) getValue() string {
	return string(os)
}

func TestGetPipelineByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "bit",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "sample",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		When:               models.AutoPipeline,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		id              string
		expectErrorCode errors.CodeType
		expectPipeline  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by id",
			id:             pipeline.Metadata.ID,
			expectPipeline: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPipeline, err := testClient.client.Pipelines.GetPipelineByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPipeline {
				require.NotNil(t, actualPipeline)
				require.Equal(t, test.id, actualPipeline.Metadata.ID)
			} else {
				require.Nil(t, actualPipeline)
			}
		})
	}
}

func TestGetPipelineByReleaseID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "bit",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "sample",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		SemanticVersion: "1.0.0",
		ProjectID:       project.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.ReleaseLifecyclePipelineType,
		ReleaseID:          &release.Metadata.ID,
		Status:             statemachine.CreatedNodeStatus,
		When:               models.AutoPipeline,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		releaseID       string
		expectErrorCode errors.CodeType
		expectPipeline  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by release id",
			releaseID:      release.Metadata.ID,
			expectPipeline: true,
		},
		{
			name:      "resource with release id not found",
			releaseID: nonExistentID,
		},
		{
			name:            "get resource with invalid release id will return an error",
			releaseID:       invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPipeline, err := testClient.client.Pipelines.GetPipelineByReleaseID(ctx, test.releaseID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPipeline {
				require.NotNil(t, actualPipeline)
				require.NotNil(t, actualPipeline.ReleaseID)
				require.Equal(t, test.releaseID, *actualPipeline.ReleaseID)
				require.Equal(t, pipeline.Metadata.ID, actualPipeline.Metadata.ID)
			} else {
				require.Nil(t, actualPipeline)
			}
		})
	}
}

func TestGetPipelineByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "bit",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "sample",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		When:               models.AutoPipeline,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		prn             string
		expectErrorCode errors.CodeType
		expectPipeline  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by PRN",
			prn:            pipeline.Metadata.PRN,
			expectPipeline: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.PipelineResource.BuildPRN(org.Name, project.Name, nonExistentGlobalID),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPipeline, err := testClient.client.Pipelines.GetPipelineByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPipeline {
				require.NotNil(t, actualPipeline)
				require.Equal(t, test.prn, actualPipeline.Metadata.PRN)
			} else {
				require.Nil(t, actualPipeline)
			}
		})
	}
}

func TestGetPipelines(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	var nestedPipelinePath string

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "bit",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "sample",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	project2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "cli",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	team1, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name:        "team-1",
		Description: "this is team 1",
	})
	require.NoError(t, err)

	team2, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name:        "team-2",
		Description: "this is team 2",
	})
	require.NoError(t, err)

	approvalRule1, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Scope:             models.OrganizationScope,
		OrgID:             org.Metadata.ID,
		Name:              "approval-rule-1",
		Description:       "this is approval rule 1",
		TeamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
		ApprovalsRequired: 2,
	})
	require.NoError(t, err)

	approvalRule2, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Scope:             models.OrganizationScope,
		OrgID:             org.Metadata.ID,
		Name:              "approval-rule-2",
		Description:       "this is approval rule 2",
		TeamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
		ApprovalsRequired: 2,
	})
	require.NoError(t, err)

	approvalRuleIDs := []string{approvalRule1.Metadata.ID, approvalRule2.Metadata.ID}

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	pipelineTemplate2, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project2.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	parentPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project2.Metadata.ID,
		PipelineTemplateID: pipelineTemplate2.Metadata.ID,
		Type:               models.RunbookPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		When:               models.AutoPipeline,
		Stages:             buildStages(statemachine.CreatedNodeStatus, "", approvalRuleIDs),
		ApprovalRuleIDs:    approvalRuleIDs,
	})
	require.NoError(t, err)

	nestedPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:              project2.Metadata.ID,
		ParentPipelineID:       &parentPipeline.Metadata.ID,
		ParentPipelineNodePath: &nestedPipelinePath,
		PipelineTemplateID:     pipelineTemplate2.Metadata.ID,
		Type:                   models.NestedPipelineType,
		Status:                 statemachine.CreatedNodeStatus,
		When:                   models.AutoPipeline,
		ApprovalRuleIDs:        approvalRuleIDs,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	// This pipeline only exists to test the superseded by nested pipeline filter.
	supersededNestedPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:              project2.Metadata.ID,
		ParentPipelineID:       &parentPipeline.Metadata.ID,
		ParentPipelineNodePath: &nestedPipelinePath,
		PipelineTemplateID:     pipelineTemplate2.Metadata.ID,
		Type:                   models.NestedPipelineType,
		Status:                 statemachine.CreatedNodeStatus,
		When:                   models.AutoPipeline,
		Superseded:             true,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	// Set the parent pipeline's nested pipeline ID.
	parentPipeline.Stages = buildStages(statemachine.CreatedNodeStatus, nestedPipeline.Metadata.ID, approvalRuleIDs)
	parentPipeline, err = testClient.client.Pipelines.UpdatePipeline(ctx, parentPipeline)
	require.NoError(t, err)

	pipelines := []*models.Pipeline{parentPipeline, nestedPipeline, supersededNestedPipeline}
	for i := 0; i < 20; i++ {
		// alternate between failed and running stages, so we can test the node filter
		var stageStatus statemachine.NodeStatus
		if i%2 == 0 {
			stageStatus = statemachine.SucceededNodeStatus
		} else {
			stageStatus = statemachine.RunningNodeStatus
		}

		pipeline, aErr := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
			ProjectID:          project.Metadata.ID,
			PipelineTemplateID: pipelineTemplate.Metadata.ID,
			Type:               models.RunbookPipelineType,
			Status:             statemachine.CreatedNodeStatus,
			When:               models.AutoPipeline,
			Stages:             buildStages(stageStatus, "", approvalRuleIDs),
			ApprovalRuleIDs:    approvalRuleIDs,
		})
		require.NoError(t, aErr)

		nested, nErr := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
			ProjectID:              project.Metadata.ID,
			ParentPipelineID:       &pipeline.Metadata.ID,
			ParentPipelineNodePath: &nestedPipelinePath,
			PipelineTemplateID:     pipelineTemplate.Metadata.ID,
			Type:                   models.NestedPipelineType,
			Status:                 statemachine.CreatedNodeStatus,
			When:                   models.AutoPipeline,
			ApprovalRuleIDs:        approvalRuleIDs,
			Stages: []*models.PipelineStage{
				{
					Name:   "s1",
					Path:   "pipeline.stage.s1",
					Status: statemachine.CreatedNodeStatus,
					Tasks: []*models.PipelineTask{
						{
							Name:   "t1",
							Path:   "pipeline.stage.s1.task.t1",
							Status: statemachine.CreatedNodeStatus,
						},
					},
				},
			},
		})
		require.NoError(t, nErr)

		pipeline.Stages = buildStages(stageStatus, nested.Metadata.ID, approvalRuleIDs)
		pipeline, err = testClient.client.Pipelines.UpdatePipeline(ctx, pipeline)
		require.NoError(t, err)

		pipelines = append(pipelines, pipeline, nested)
	}

	type testCase struct {
		filter            *PipelineFilter
		name              string
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all pipelines",
			filter:            &PipelineFilter{},
			expectResultCount: len(pipelines),
		},
		{
			name: "get pipelines by project id",
			filter: &PipelineFilter{
				ProjectID: &project.Metadata.ID,
			},
			expectResultCount: len(pipelines) - 3,
		},
		{
			name: "get pipelines by pipeline template id",
			filter: &PipelineFilter{
				PipelineTemplateID: &pipelineTemplate.Metadata.ID,
			},
			expectResultCount: len(pipelines) - 3,
		},
		{
			name: "get all pipelines for project 2",
			filter: &PipelineFilter{
				ProjectID: &project2.Metadata.ID,
			},
			expectResultCount: 3,
		},
		{
			name: "get all pipelines for pipeline template 2",
			filter: &PipelineFilter{
				PipelineTemplateID: &pipelineTemplate2.Metadata.ID,
			},
			expectResultCount: 3,
		},
		{
			name: "get pipelines by specific pipeline id",
			filter: &PipelineFilter{
				PipelineIDs: []string{
					pipelines[0].Metadata.ID,
					pipelines[1].Metadata.ID,
					pipelines[2].Metadata.ID,
				},
			},
			expectResultCount: 3,
		},
		{
			name: "get pipelines by failed node filter",
			filter: &PipelineFilter{
				NodeTypes:    []statemachine.NodeType{statemachine.StageNodeType},
				NodeStatuses: []statemachine.NodeStatus{statemachine.FailedNodeStatus},
			},
			expectResultCount: 0,
		},
		{
			name: "get pipelines by running node filter",
			filter: &PipelineFilter{
				NodeTypes:    []statemachine.NodeType{statemachine.ActionNodeType, statemachine.PipelineNodeType},
				NodeStatuses: []statemachine.NodeStatus{statemachine.RunningNodeStatus},
			},
			expectResultCount: 10,
		},
		{
			name: "get pipelines by succeeded node filter",
			filter: &PipelineFilter{
				NodeTypes:    []statemachine.NodeType{statemachine.TaskNodeType},
				NodeStatuses: []statemachine.NodeStatus{statemachine.SucceededNodeStatus},
			},
			expectResultCount: 10,
		},
		{
			name: "get pipelines by ready node filter",
			filter: &PipelineFilter{
				NodeTypes:    []statemachine.NodeType{statemachine.TaskNodeType},
				NodeStatuses: []statemachine.NodeStatus{statemachine.ReadyNodeStatus},
			},
		},
		{
			name: "get pipelines by superseded filter",
			filter: &PipelineFilter{
				Superseded: ptr.Bool(true),
			},
			expectResultCount: 1,
		},
		{
			name: "get pipelines by parent pipeline id",
			filter: &PipelineFilter{
				ParentPipelineID: &parentPipeline.Metadata.ID,
			},
			expectResultCount: 2,
		},
		{
			name: "get pipelines by parent nested pipeline node path",
			filter: &PipelineFilter{
				ParentNestedPipelineNodePath: &nestedPipelinePath,
			},
			expectResultCount: 22,
		},
		{
			name: "get pipelines superseded by nested pipeline",
			filter: &PipelineFilter{
				Superseded:                   ptr.Bool(true),
				ParentPipelineID:             nestedPipeline.ParentPipelineID,
				ParentNestedPipelineNodePath: nestedPipeline.ParentPipelineNodePath,
			},
			expectResultCount: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.Pipelines.GetPipelines(ctx, &GetPipelinesInput{
				Filter: test.filter,
			})

			require.Nil(t, err)

			assert.Equal(t, test.expectResultCount, len(result.Pipelines))
		})
	}
}

func TestGetPipelinesWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "bit",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "sample",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
			ProjectID:          project.Metadata.ID,
			PipelineTemplateID: pipelineTemplate.Metadata.ID,
			Type:               models.RunbookPipelineType,
			Status:             statemachine.CreatedNodeStatus,
			When:               models.AutoPipeline,
			Stages: []*models.PipelineStage{
				{
					Name:   "s1",
					Path:   "pipeline.stage.s1",
					Status: statemachine.CreatedNodeStatus,
					Tasks: []*models.PipelineTask{
						{
							Name:   "t1",
							Path:   "pipeline.stage.s1.task.t1",
							Status: statemachine.CreatedNodeStatus,
						},
					},
				},
			},
		})
		require.Nil(t, err)
	}

	sortableFields := []sortableField{
		PipelineSortableFieldCreatedAtAsc,
		PipelineSortableFieldCreatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := PipelineSortableField(sortByField.getValue())

		result, err := testClient.client.Pipelines.GetPipelines(ctx, &GetPipelinesInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.Pipelines {
			resources = append(resources, &result.Pipelines[ix])
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreatePipeline(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "bit",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "sample",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		SemanticVersion: "1.0.0",
		ProjectID:       project.Metadata.ID,
	})
	require.NoError(t, err)

	parentPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ReleaseID:          &release.Metadata.ID,
		Type:               models.ReleaseLifecyclePipelineType,
		Status:             statemachine.CreatedNodeStatus,
		When:               models.AutoPipeline,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	nestedPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.NestedPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		When:               models.AutoPipeline,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	type testCase struct {
		releaseID          *string
		parentPipelineID   *string
		name               string
		expectErrorCode    errors.CodeType
		pipelineTemplateID string
		projectID          string
	}

	testCases := []testCase{
		{
			name:               "successfully create resource",
			pipelineTemplateID: pipelineTemplate.Metadata.ID,
			projectID:          project.Metadata.ID,
			releaseID:          &release.Metadata.ID,
			parentPipelineID:   &parentPipeline.Metadata.ID,
		},
		{
			name:               "create will fail because pipeline template does not exist",
			pipelineTemplateID: nonExistentID,
			projectID:          project.Metadata.ID,
			expectErrorCode:    errors.ENotFound,
		},
		{
			name:               "create will fail because project does not exist",
			pipelineTemplateID: pipelineTemplate.Metadata.ID,
			projectID:          nonExistentID,
			expectErrorCode:    errors.ENotFound,
		},
		{
			name:               "create will fail because release does not exist",
			pipelineTemplateID: pipelineTemplate.Metadata.ID,
			projectID:          project.Metadata.ID,
			releaseID:          ptr.String(nonExistentID),
			expectErrorCode:    errors.ENotFound,
		},
		{
			name:               "create will fail because parent pipeline does not exist",
			pipelineTemplateID: pipelineTemplate.Metadata.ID,
			projectID:          project.Metadata.ID,
			parentPipelineID:   ptr.String(nonExistentID),
			expectErrorCode:    errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
				ProjectID:          test.projectID,
				PipelineTemplateID: test.pipelineTemplateID,
				ReleaseID:          test.releaseID,
				ParentPipelineID:   test.parentPipelineID,
				Type:               models.RunbookPipelineType,
				Status:             statemachine.CreatedNodeStatus,
				When:               models.AutoPipeline,
				CreatedBy:          "system",
				Stages:             buildStages(statemachine.SucceededNodeStatus, nestedPipeline.Metadata.ID, []string{}),
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, pipeline)
		})
	}
}

func TestUpdatePipeline(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "bit",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "sample",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	team1, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name:        "team-1",
		Description: "this is team 1",
	})
	require.NoError(t, err)

	team2, err := testClient.client.Teams.CreateTeam(ctx, &models.Team{
		Name:        "team-2",
		Description: "this is team 2",
	})
	require.NoError(t, err)

	approvalRule1, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Scope:             models.OrganizationScope,
		OrgID:             org.Metadata.ID,
		Name:              "approval-rule-1",
		Description:       "this is approval rule 1",
		TeamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
		ApprovalsRequired: 2,
	})
	require.NoError(t, err)

	approvalRule2, err := testClient.client.ApprovalRules.CreateApprovalRule(ctx, &models.ApprovalRule{
		Scope:             models.OrganizationScope,
		OrgID:             org.Metadata.ID,
		Name:              "approval-rule-2",
		Description:       "this is approval rule 2",
		TeamIDs:           []string{team1.Metadata.ID, team2.Metadata.ID},
		ApprovalsRequired: 2,
	})
	require.NoError(t, err)

	approvalRuleIDs := []string{approvalRule1.Metadata.ID, approvalRule2.Metadata.ID}

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
		Status:    models.PipelineTemplateUploaded,
	})
	require.NoError(t, err)

	nestedPipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.NestedPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		When:               models.AutoPipeline,
		ApprovalRuleIDs:    approvalRuleIDs,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		ProjectID:          project.Metadata.ID,
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		Type:               models.RunbookPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		When:               models.AutoPipeline,
		Stages:             buildStages(statemachine.SucceededNodeStatus, nestedPipeline.Metadata.ID, approvalRuleIDs),
		ApprovalRuleIDs:    approvalRuleIDs,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		status          statemachine.NodeStatus
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "successfully update resource",
			version: 1,
			status:  statemachine.SucceededNodeStatus,
		},
		{
			name:            "update will fail because resource version doesn't match",
			status:          statemachine.FailedNodeStatus,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipeline.Status = test.status
			pipeline.Metadata.Version = test.version
			_, err := pipeline.SetStatus(test.status)
			require.NoError(t, err)

			updatedPipeline, err := testClient.client.Pipelines.UpdatePipeline(ctx, pipeline)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.Equal(t, test.status, updatedPipeline.Status)

			// check that all nested resources have been updated
			for _, stage := range updatedPipeline.Stages {
				assert.Equal(t, test.status, stage.Status)

				for _, task := range stage.Tasks {
					assert.Equal(t, test.status, task.Status)

					for _, action := range task.Actions {
						assert.Equal(t, test.status, action.Status)
					}
				}

				for _, nestedPipeline := range stage.NestedPipelines {
					assert.Equal(t, test.status, nestedPipeline.Status)
				}
			}
		})
	}
}

// buildStages builds sample stages for testing.
func buildStages(
	status statemachine.NodeStatus,
	nestedPipelineID string,
	approvalRuleIDs []string,
) []*models.PipelineStage {
	return []*models.PipelineStage{
		{
			Name:   "s1",
			Path:   "pipeline.stage.s1",
			Status: status,
			Tasks: []*models.PipelineTask{
				{
					Name:   "t1",
					Path:   "pipeline.stage.s1.task.t1",
					Status: status,
					Actions: []*models.PipelineAction{
						{
							Name:   "a1",
							Path:   "pipeline.stage.s1.task.t1.action.a1",
							Status: status,
						},
					},
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			NestedPipelines: []*models.NestedPipeline{
				{
					Name:             "np1",
					Path:             "pipeline.stage.s1.pipeline.np1",
					LatestPipelineID: nestedPipelineID,
					Status:           status,
				},
			},
		},
	}
}
