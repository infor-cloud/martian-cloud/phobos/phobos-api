package db

//go:generate go tool mockery --name Agents --inpackage --case underscore

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Agents encapsulates the logic to access agents from the database
type Agents interface {
	GetAgentByID(ctx context.Context, id string) (*models.Agent, error)
	GetAgentByPRN(ctx context.Context, prn string) (*models.Agent, error)
	GetAgents(ctx context.Context, input *GetAgentsInput) (*AgentsResult, error)
	CreateAgent(ctx context.Context, agent *models.Agent) (*models.Agent, error)
	UpdateAgent(ctx context.Context, agent *models.Agent) (*models.Agent, error)
	DeleteAgent(ctx context.Context, agent *models.Agent) error
}

// AgentSortableField represents the fields that agents can be sorted by
type AgentSortableField string

// AgentSortableField constants
const (
	AgentSortableFieldUpdatedAtAsc  AgentSortableField = "UPDATED_AT_ASC"
	AgentSortableFieldUpdatedAtDesc AgentSortableField = "UPDATED_AT_DESC"
)

func (as AgentSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch as {
	case AgentSortableFieldUpdatedAtAsc, AgentSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "agents", Col: "updated_at"}
	default:
		return nil
	}
}

func (as AgentSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(as), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// AgentFilter contains the supported fields for filtering Agent resources
type AgentFilter struct {
	OrganizationID *string
	AgentName      *string
	AgentID        *string
	AgentType      *models.AgentType
	Enabled        *bool
	AgentIDs       []string
}

// GetAgentsInput is the input for listing agents
type GetAgentsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *AgentSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *AgentFilter
}

// AgentsResult contains the response data and page information
type AgentsResult struct {
	PageInfo *pagination.PageInfo
	Agents   []models.Agent
}

type agents struct {
	dbClient *Client
}

var agentFieldList = append(metadataFieldList, "type", "name", "description", "organization_id", "created_by", "disabled", "tags", "run_untagged_jobs")

// NewAgents returns an instance of the Agents interface
func NewAgents(dbClient *Client) Agents {
	return &agents{dbClient: dbClient}
}

func (a *agents) GetAgentByID(ctx context.Context, id string) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "db.GetAgentByID")
	defer span.End()

	return a.getAgent(ctx, goqu.Ex{"agents.id": id})
}

func (a *agents) GetAgentByPRN(ctx context.Context, prn string) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "db.GetAgentByPRN")
	defer span.End()

	path, err := models.AgentResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.Ex{}
	parts := strings.Split(path, "/")

	switch len(parts) {
	case 1:
		// Shared agent
		ex["agents.name"] = parts[0]
		ex["agents.type"] = models.SharedAgent
	case 2:
		// Organization agent
		ex["organizations.name"] = parts[0]
		ex["agents.name"] = parts[1]
		ex["agents.type"] = models.OrganizationAgent
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return a.getAgent(ctx, ex)
}

func (a *agents) GetAgents(ctx context.Context, input *GetAgentsInput) (*AgentsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetAgents")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.AgentID != nil {
			ex = ex.Append(goqu.I("agents.id").Eq(*input.Filter.AgentID))
		}

		if len(input.Filter.AgentIDs) > 0 {
			ex = ex.Append(goqu.I("agents.id").In(input.Filter.AgentIDs))
		}

		if input.Filter.AgentName != nil {
			ex = ex.Append(goqu.I("agents.name").Eq(*input.Filter.AgentName))
		}

		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("agents.organization_id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.AgentType != nil {
			ex = ex.Append(goqu.I("agents.type").Eq(*input.Filter.AgentType))
		}

		if input.Filter.Enabled != nil {
			ex = ex.Append(goqu.I("agents.disabled").Eq(!(*input.Filter.Enabled)))
		}
	}

	query := dialect.From(goqu.T("agents")).
		Select(a.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.I("agents.organization_id").Eq(goqu.I("organizations.id")))).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "agents", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)

	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, a.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Agent{}
	for rows.Next() {
		item, err := scanAgent(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := AgentsResult{
		PageInfo: rows.GetPageInfo(),
		Agents:   results,
	}

	return &result, nil
}

func (a *agents) CreateAgent(ctx context.Context, agent *models.Agent) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "db.CreateAgent")
	defer span.End()

	timestamp := currentTime()

	tags, err := json.Marshal(agent.Tags)
	if err != nil {
		return nil, err
	}

	sql, args, err := dialect.From("agents").
		Prepared(true).
		With("agents",
			dialect.Insert("agents").Rows(
				goqu.Record{
					"id":                newResourceID(),
					"version":           initialResourceVersion,
					"created_at":        timestamp,
					"updated_at":        timestamp,
					"type":              agent.Type,
					"organization_id":   agent.OrganizationID,
					"name":              agent.Name,
					"description":       agent.Description,
					"created_by":        agent.CreatedBy,
					"disabled":          agent.Disabled,
					"tags":              tags,
					"run_untagged_jobs": agent.RunUntaggedJobs,
				}).Returning("*"),
		).Select(a.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"agents.organization_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdAgent, err := scanAgent(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New(
					"agent with name %s already exists in organization", agent.Name,
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EConflict),
				)
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdAgent, nil
}

func (a *agents) UpdateAgent(ctx context.Context, agent *models.Agent) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateAgent")
	defer span.End()

	timestamp := currentTime()

	tags, err := json.Marshal(agent.Tags)
	if err != nil {
		return nil, err
	}

	sql, args, err := dialect.From("agents").
		Prepared(true).
		With("agents",
			dialect.Update("agents").
				Set(goqu.Record{
					"version":           goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":        timestamp,
					"description":       agent.Description,
					"disabled":          agent.Disabled,
					"tags":              tags,
					"run_untagged_jobs": agent.RunUntaggedJobs,
				}).Where(goqu.Ex{"id": agent.Metadata.ID, "version": agent.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"agents.organization_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedAgent, err := scanAgent(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedAgent, nil
}

func (a *agents) DeleteAgent(ctx context.Context, agent *models.Agent) error {
	ctx, span := tracer.Start(ctx, "db.DeleteAgent")
	defer span.End()

	sql, args, err := dialect.From("agents").
		Prepared(true).
		With("agents",
			dialect.Delete("agents").
				Where(goqu.Ex{"id": agent.Metadata.ID, "version": agent.Metadata.Version}).
				Returning("*"),
		).Select(a.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"agents.organization_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	_, err = scanAgent(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (a *agents) getAgent(ctx context.Context, exp goqu.Ex) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "db.getAgent")
	defer span.End()

	query := dialect.From(goqu.T("agents")).
		Prepared(true).
		Select(a.getSelectFields()...).
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.I("agents.organization_id").Eq(goqu.I("organizations.id")))).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	agent, err := scanAgent(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, "invalid ID; %s", pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return agent, nil
}

func (*agents) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range agentFieldList {
		selectFields = append(selectFields, fmt.Sprintf("agents.%s", field))
	}

	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanAgent(row scanner) (*models.Agent, error) {
	var organizationName sql.NullString

	agent := &models.Agent{}

	fields := []interface{}{
		&agent.Metadata.ID,
		&agent.Metadata.CreationTimestamp,
		&agent.Metadata.LastUpdatedTimestamp,
		&agent.Metadata.Version,
		&agent.Type,
		&agent.Name,
		&agent.Description,
		&agent.OrganizationID,
		&agent.CreatedBy,
		&agent.Disabled,
		&agent.Tags,
		&agent.RunUntaggedJobs,
		&organizationName,
	}

	err := row.Scan(fields...)
	if err != nil {
		return nil, err
	}

	if organizationName.Valid {
		agent.Metadata.PRN = models.AgentResource.BuildPRN(organizationName.String, agent.Name)
	} else {
		agent.Metadata.PRN = models.AgentResource.BuildPRN(agent.Name)
	}

	return agent, nil
}
