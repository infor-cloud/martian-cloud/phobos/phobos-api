//go:build integration

package db

import (
	"context"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// getValue implements the sortableField interface for testing
func (sf MetricSortableField) getValue() string {
	return string(sf)
}

func TestGetMetrics(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	project2, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "electron",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	metrics := make([]*models.Metric, 10)
	for i := 0; i < len(metrics); i++ {
		metric, cErr := testClient.client.Metrics.CreateMetric(ctx, &models.Metric{
			Name:           models.MetricDeploymentRecoveryTime,
			ProjectID:      &project.Metadata.ID,
			OrganizationID: &organization.Metadata.ID,
			Tags:           map[models.MetricTagName]string{},
			Value:          10.0 * float64(i),
		})
		require.NoError(t, cErr)

		metrics[i] = metric
	}

	metric, err := testClient.client.Metrics.CreateMetric(ctx, &models.Metric{
		Name:           models.MetricPipelineCompleted,
		ProjectID:      &project2.Metadata.ID,
		OrganizationID: &organization.Metadata.ID,
		Tags: map[models.MetricTagName]string{
			models.MetricTagNamePipelineType: string(models.DeploymentPipelineType),
		},
		Value: 10.0,
	})
	require.NoError(t, err)

	metrics = append(metrics, metric)

	pipelinesCompletedMetric := models.MetricPipelineCompleted

	type testCase struct {
		filter            *MetricFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all resources",
			filter:            &MetricFilter{},
			expectResultCount: len(metrics),
		},
		{
			name: "get metrics for project 1",
			filter: &MetricFilter{
				ProjectID: &project.Metadata.ID,
			},
			expectResultCount: len(metrics) - 1,
		},
		{
			name: "get metrics for project 2",
			filter: &MetricFilter{
				ProjectID: &project2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "get metrics by name",
			filter: &MetricFilter{
				MetricName: &pipelinesCompletedMetric,
			},
			expectResultCount: 1,
		},
		{
			name: "get metrics by tag",
			filter: &MetricFilter{
				Tags: map[models.MetricTagName]string{
					models.MetricTagNamePipelineType: string(models.DeploymentPipelineType),
				},
			},
			expectResultCount: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.Metrics.GetMetrics(ctx, &GetMetricsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, result)
			assert.Len(t, result.Metrics, test.expectResultCount)
		})
	}
}

func TestGetMetricsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "proton",
		OrgID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 1; i <= resourceCount; i++ {
		_, cErr := testClient.client.Metrics.CreateMetric(ctx, &models.Metric{
			Name:           models.MetricDeploymentRecoveryTime,
			ProjectID:      &project.Metadata.ID,
			OrganizationID: &organization.Metadata.ID,
			Tags:           map[models.MetricTagName]string{},
			Value:          10.0 * float64(i),
		})
		require.NoError(t, cErr)
	}

	sortableFields := []sortableField{
		MetricSortableFieldCreatedAtAsc,
		MetricSortableFieldCreatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := MetricSortableField(sortByField.getValue())

		result, err := testClient.client.Metrics.GetMetrics(ctx, &GetMetricsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.Metrics {
			resourceCopy := resource
			resources = append(resources, resourceCopy)
		}

		return result.PageInfo, resources, nil
	})
}

func TestGetAggregatedMetricStatistics(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-org",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	environmentName := "test-environment"

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ProjectID:          project.Metadata.ID,
		EnvironmentName:    &environmentName,
		Type:               models.DeploymentPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	bucketCount := 24

	for i := 0; i < bucketCount; i++ {
		// Stub out time functions to create metrics with different timestamps
		now = func() time.Time { return time.Now().UTC().Add(-time.Hour * time.Duration(i)) }
		for j := 0; j <= i; j++ {
			_, cErr := testClient.client.Metrics.CreateMetric(ctx, &models.Metric{
				Name:            models.MetricPipelineCompleted,
				PipelineID:      &pipeline.Metadata.ID,
				ProjectID:       &project.Metadata.ID,
				OrganizationID:  &org.Metadata.ID,
				EnvironmentName: &environmentName,
				Tags: map[models.MetricTagName]string{
					models.MetricTagNamePipelineType: string(models.DeploymentPipelineType),
				},
				Value: 1.0,
			})
			require.NoError(t, cErr)
		}
	}
	// Reset time function
	now = time.Now

	metricName := models.MetricPipelineCompleted
	actualStatistics, err := testClient.client.Metrics.GetAggregatedMetricStatistics(ctx, &GetAggregatedMetricsInput{
		BucketPeriod: MetricBucketPeriodHour,
		BucketCount:  bucketCount,
		Filter: &MetricFilter{
			OrganizationID:  &org.Metadata.ID,
			PipelineID:      &pipeline.Metadata.ID,
			ProjectID:       &project.Metadata.ID,
			EnvironmentName: &environmentName,
			Tags: map[models.MetricTagName]string{
				models.MetricTagNamePipelineType: string(models.DeploymentPipelineType),
			},
			MetricName: &metricName,
		},
	})
	require.NoError(t, err)
	require.Equal(t, bucketCount, len(actualStatistics))

	// Verify that the statistics are correct for each bucket
	for i := 0; i < bucketCount; i++ {
		assert.Equal(t, MetricStatistics{
			Minimum: 1,
			Maximum: 1,
			Average: 1,
			Sum:     1.0 * float64(bucketCount-i),
		}, actualStatistics[i].Stats)
	}
}

func TestGetMetricStatistics(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-org",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	environmentName := "test-environment"

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ProjectID:          project.Metadata.ID,
		EnvironmentName:    &environmentName,
		Type:               models.DeploymentPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	timeRangeStart := time.Now().UTC()

	metricsCount := 10
	for i := 1; i <= metricsCount; i++ {
		_, cErr := testClient.client.Metrics.CreateMetric(ctx, &models.Metric{
			Name:            models.MetricDeploymentRecoveryTime,
			PipelineID:      &pipeline.Metadata.ID,
			ProjectID:       &project.Metadata.ID,
			OrganizationID:  &org.Metadata.ID,
			EnvironmentName: &environmentName,
			Tags:            map[models.MetricTagName]string{},
			Value:           10.0 * float64(i),
		})
		require.NoError(t, cErr)
	}

	timeRangeEnd := time.Now().UTC()

	expectStatistics := &MetricStatistics{
		Minimum: 10,
		Maximum: 100,
		Average: 55,
		Sum:     550,
	}

	type testCase struct {
		name            string
		input           *MetricFilter
		expectErrorCode errors.CodeType
	}

	metricName := models.MetricDeploymentRecoveryTime
	testCases := []testCase{
		{
			name: "get metric statistics by organization",
			input: &MetricFilter{
				OrganizationID: &org.Metadata.ID,
				MetricName:     &metricName,
			},
		},
		{
			name: "get metric statistics by project",
			input: &MetricFilter{
				ProjectID:  &project.Metadata.ID,
				MetricName: &metricName,
			},
		},
		{
			name: "get metric statistics by pipeline id",
			input: &MetricFilter{
				PipelineID: &pipeline.Metadata.ID,
				MetricName: &metricName,
			},
		},
		{
			name: "get metric statistics by environment name",
			input: &MetricFilter{
				EnvironmentName: &environmentName,
				MetricName:      &metricName,
			},
		},
		{
			name: "get metric statistics by time range start",
			input: &MetricFilter{
				TimeRangeStart: &timeRangeStart,
				MetricName:     &metricName,
			},
		},
		{
			name: "get metric statistics by time range end",
			input: &MetricFilter{
				TimeRangeEnd: &timeRangeEnd,
				MetricName:   &metricName,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualStatistics, err := testClient.client.Metrics.GetMetricStatistics(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.Equal(t, expectStatistics, actualStatistics)
		})
	}
}

func TestCreateMetric(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-org",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	release, err := testClient.client.Releases.CreateRelease(ctx, &models.Release{
		SemanticVersion: "0.0.1",
		ProjectID:       project.Metadata.ID,
	})
	require.NoError(t, err)

	pipelineTemplate, err := testClient.client.PipelineTemplates.CreatePipelineTemplate(ctx, &models.PipelineTemplate{
		ProjectID: project.Metadata.ID,
	})
	require.NoError(t, err)

	pipeline, err := testClient.client.Pipelines.CreatePipeline(ctx, &models.Pipeline{
		PipelineTemplateID: pipelineTemplate.Metadata.ID,
		ProjectID:          project.Metadata.ID,
		ReleaseID:          &release.Metadata.ID,
		EnvironmentName:    ptr.String("test-environment"),
		Type:               models.DeploymentPipelineType,
		Status:             statemachine.CreatedNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.CreatedNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.CreatedNodeStatus,
					},
				},
			},
		},
	})
	require.NoError(t, err)

	type testCase struct {
		name             string
		metricName       models.MetricName
		pipelineTargetID *string
		releaseTargetID  *string
		expectErrorCode  errors.CodeType
	}

	testCases := []testCase{
		{
			name:             "successfully create metric for deployment pipeline",
			metricName:       models.MetricPipelineCompleted,
			pipelineTargetID: &pipeline.Metadata.ID,
		},
		{
			name:            "successfully create metric for release",
			metricName:      models.MetricPipelineCompleted,
			releaseTargetID: &release.Metadata.ID,
		},
		{
			name:             "create will fail since pipeline does not exist",
			metricName:       models.MetricPipelineCompleted,
			pipelineTargetID: ptr.String(nonExistentID),
			expectErrorCode:  errors.ENotFound,
		},
		{
			name:            "create will fail since release does not exist",
			metricName:      models.MetricPipelineCompleted,
			releaseTargetID: ptr.String(nonExistentID),
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualMetric, err := testClient.client.Metrics.CreateMetric(ctx, &models.Metric{
				Name:           test.metricName,
				PipelineID:     test.pipelineTargetID,
				ReleaseID:      test.releaseTargetID,
				OrganizationID: &org.Metadata.ID,
				Value:          1,
				Tags:           map[models.MetricTagName]string{},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, actualMetric)
		})
	}
}
