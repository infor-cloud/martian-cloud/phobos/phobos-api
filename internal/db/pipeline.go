package db

//go:generate go tool mockery --name Pipelines --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Pipelines encapsulates the logic to access Pipelines from the database
type Pipelines interface {
	GetPipelineByID(ctx context.Context, id string) (*models.Pipeline, error)
	GetPipelineByReleaseID(ctx context.Context, releaseID string) (*models.Pipeline, error)
	GetPipelineByPRN(ctx context.Context, prn string) (*models.Pipeline, error)
	GetPipelines(ctx context.Context, input *GetPipelinesInput) (*PipelinesResult, error)
	CreatePipeline(ctx context.Context, pipeline *models.Pipeline) (*models.Pipeline, error)
	UpdatePipeline(ctx context.Context, pipeline *models.Pipeline) (*models.Pipeline, error)
}

// PipelineFilter contains the supported fields for filtering Pipeline resources
type PipelineFilter struct {
	TimeRangeStart               *time.Time
	Completed                    *bool
	ParentPipelineID             *string
	ParentNestedPipelineNodePath *string
	ProjectID                    *string
	PipelineTemplateID           *string
	Started                      *bool
	Superseded                   *bool
	EnvironmentName              *string
	ReleaseID                    *string
	PipelineIDs                  []string
	PipelineStatuses             []statemachine.NodeStatus
	NodeTypes                    []statemachine.NodeType
	NodeStatuses                 []statemachine.NodeStatus
	PipelineTypes                []models.PipelineType
}

// PipelineSortableField represents the fields that a pipeline can be sorted by
type PipelineSortableField string

// PipelineSortableField constants
const (
	PipelineSortableFieldCreatedAtAsc    PipelineSortableField = "CREATED_AT_ASC"
	PipelineSortableFieldCreatedAtDesc   PipelineSortableField = "CREATED_AT_DESC"
	PipelineSortableFieldCompletedAtAsc  PipelineSortableField = "COMPLETED_AT_ASC"
	PipelineSortableFieldCompletedAtDesc PipelineSortableField = "COMPLETED_AT_DESC"
	PipelineSortableFieldStartedAtAsc    PipelineSortableField = "STARTED_AT_ASC"
	PipelineSortableFieldStartedAtDesc   PipelineSortableField = "STARTED_AT_DESC"
)

func (os PipelineSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case PipelineSortableFieldCreatedAtAsc, PipelineSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "pipelines", Col: "created_at"}
	case PipelineSortableFieldCompletedAtAsc, PipelineSortableFieldCompletedAtDesc:
		return &pagination.FieldDescriptor{Key: "completed_at", Table: "pipelines", Col: "completed_at"}
	case PipelineSortableFieldStartedAtAsc, PipelineSortableFieldStartedAtDesc:
		return &pagination.FieldDescriptor{Key: "started_at", Table: "pipelines", Col: "started_at"}
	default:
		return nil
	}
}

func (os PipelineSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetPipelinesInput is the input for listing pipelines
type GetPipelinesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *PipelineSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *PipelineFilter
}

// PipelinesResult contains the response data and page information
type PipelinesResult struct {
	PageInfo  *pagination.PageInfo
	Pipelines []models.Pipeline
}

type pipelineAnnotation struct {
	Link  *string `json:"l,omitempty"`
	Value string  `json:"v"`
}

type pipelineNode struct {
	ParentID                             *string
	TaskNodeLastAttemptFinishedAt        *time.Time
	NestedPipelineNodeEnvironmentName    *string
	NestedPipelineNodeLatestPipelineID   *string
	NestedPipelineNodeScheduledStartTime *time.Time
	NestedPipelineNodeCronExpression     *string
	TaskNodeCronExpression               *string
	NestedPipelineNodeCronTimezone       *string
	TaskNodeCronTimezone                 *string
	TaskNodeScheduledStartTime           *time.Time
	TaskNodeLatestJobID                  *string
	TaskNodeImage                        *string
	TaskNodeIntervalSeconds              *int
	Name                                 string
	ID                                   string
	NestedPipelineNodeApprovalStatus     string
	Path                                 string
	TaskNodeWhen                         string
	TaskNodeOnError                      statemachine.OnErrorStrategy
	NestedPipelineNodeOnError            statemachine.OnErrorStrategy
	Status                               statemachine.NodeStatus
	PipelineID                           string
	NodeType                             statemachine.NodeType
	ActionNodePluginActionName           string
	TaskNodeApprovalStatus               string
	ActionNodePluginName                 string
	TaskNodeDependencies                 []string
	NestedPipelineNodeDependencies       []string
	TaskNodeApprovalRuleIDs              []string
	Errors                               []string
	TaskNodeAgentTags                    []string
	TaskNodeMaxAttempts                  int
	Position                             int
	TaskNodeAttemptCount                 int
	TaskNodeHasSuccessCondition          bool
}

func (pn *pipelineNode) toModel() (interface{}, error) {
	switch pn.NodeType {
	case statemachine.StageNodeType:
		return &models.PipelineStage{
			Path:   pn.Path,
			Status: pn.Status,
			Name:   pn.Name,
		}, nil
	case statemachine.PipelineNodeType:
		var cronSchedule *models.CronSchedule
		if pn.NestedPipelineNodeCronExpression != nil {
			cronSchedule = &models.CronSchedule{
				Expression: *pn.NestedPipelineNodeCronExpression,
				Timezone:   pn.NestedPipelineNodeCronTimezone,
			}
		}

		return &models.NestedPipeline{
			Path:               pn.Path,
			Status:             pn.Status,
			Name:               pn.Name,
			ApprovalStatus:     models.PipelineApprovalStatus(pn.NestedPipelineNodeApprovalStatus),
			EnvironmentName:    pn.NestedPipelineNodeEnvironmentName,
			LatestPipelineID:   ptr.ToString(pn.NestedPipelineNodeLatestPipelineID),
			ScheduledStartTime: pn.NestedPipelineNodeScheduledStartTime,
			CronSchedule:       cronSchedule,
			Dependencies:       pn.NestedPipelineNodeDependencies,
			Errors:             pn.Errors,
			OnError:            pn.NestedPipelineNodeOnError,
		}, nil
	case statemachine.TaskNodeType:
		var interval *time.Duration
		if pn.TaskNodeIntervalSeconds != nil {
			interval = ptr.Duration(time.Duration(*pn.TaskNodeIntervalSeconds) * time.Second)
		}

		var cronSchedule *models.CronSchedule
		if pn.TaskNodeCronExpression != nil {
			cronSchedule = &models.CronSchedule{
				Expression: *pn.TaskNodeCronExpression,
				Timezone:   pn.TaskNodeCronTimezone,
			}
		}

		return &models.PipelineTask{
			Path:                  pn.Path,
			Status:                pn.Status,
			Name:                  pn.Name,
			ApprovalStatus:        models.PipelineApprovalStatus(pn.TaskNodeApprovalStatus),
			ApprovalRuleIDs:       pn.TaskNodeApprovalRuleIDs,
			When:                  models.PipelineTaskWhenCondition(pn.TaskNodeWhen),
			Interval:              interval,
			LatestJobID:           pn.TaskNodeLatestJobID,
			Image:                 pn.TaskNodeImage,
			HasSuccessCondition:   pn.TaskNodeHasSuccessCondition,
			MaxAttempts:           pn.TaskNodeMaxAttempts,
			AttemptCount:          pn.TaskNodeAttemptCount,
			LastAttemptFinishedAt: pn.TaskNodeLastAttemptFinishedAt,
			AgentTags:             pn.TaskNodeAgentTags,
			ScheduledStartTime:    pn.TaskNodeScheduledStartTime,
			Dependencies:          pn.TaskNodeDependencies,
			Errors:                pn.Errors,
			OnError:               pn.TaskNodeOnError,
			CronSchedule:          cronSchedule,
		}, nil
	case statemachine.ActionNodeType:
		return &models.PipelineAction{
			Path:       pn.Path,
			Status:     pn.Status,
			Name:       pn.Name,
			PluginName: pn.ActionNodePluginName,
			ActionName: pn.ActionNodePluginActionName,
		}, nil
	}
	return nil, fmt.Errorf("unexpected node type %s", pn.NodeType)
}

type pipelineApprovalRule struct {
	PipelineID     *string
	PipelineNodeID *string
	ApprovalRuleID string
	ApprovalType   models.PipelineApprovalType
}

var (
	pipelineFieldList = append(metadataFieldList,
		"created_by",
		"project_id",
		"pipeline_template_id",
		"status",
		"release_id",
		"parent_pipeline_id",
		"parent_pipeline_node_path",
		"type",
		"when_condition",
		"superseded",
		"started_at",
		"completed_at",
		"environment_name",
		"annotations",
		"approval_status",
	)
	pipelineNodeFieldList = []interface{}{
		"id",
		"path",
		"node_type",
		"pipeline_id",
		"parent_id",
		"pos",
		"status",
		"name",
		"action_node_plugin_name",
		"action_node_plugin_action_name",
		"task_node_approval_status",
		"task_node_when",
		"task_node_interval_seconds",
		"task_node_latest_job_id",
		"task_node_image",
		"task_node_has_success_condition",
		"task_node_max_attempts",
		"task_node_attempt_count",
		"task_node_last_attempt_finished_time",
		"task_node_agent_tags",
		"task_node_scheduled_start_time",
		"task_node_cron_expression",
		"task_node_cron_timezone",
		"task_node_dependencies",
		"task_node_on_error",
		"nested_pipeline_node_environment_name",
		"nested_pipeline_node_latest_pipeline_id",
		"nested_pipeline_node_scheduled_start_time",
		"nested_pipeline_node_cron_expression",
		"nested_pipeline_node_cron_timezone",
		"nested_pipeline_node_approval_status",
		"nested_pipeline_node_dependencies",
		"nested_pipeline_node_on_error",
		"node_errors",
	}
	pipelineApprovalRuleFieldList = []interface{}{
		"approval_rule_id",
		"pipeline_id",
		"pipeline_node_id",
		"approval_type",
	}
)

type pipelines struct {
	dbClient *Client
}

// NewPipelines returns an instance of the Pipelines interface
func NewPipelines(dbClient *Client) Pipelines {
	return &pipelines{dbClient: dbClient}
}

func (p *pipelines) GetPipelineByID(ctx context.Context, id string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineByID")
	defer span.End()

	return p.getPipeline(ctx, goqu.Ex{"pipelines.id": id})
}

func (p *pipelines) GetPipelineByReleaseID(ctx context.Context, releaseID string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineByReleaseID")
	defer span.End()

	return p.getPipeline(ctx, goqu.Ex{
		"pipelines.release_id": releaseID,
		"pipelines.type":       models.ReleaseLifecyclePipelineType,
	})
}

func (p *pipelines) GetPipelineByPRN(ctx context.Context, prn string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelineByPRN")
	defer span.End()

	path, err := models.PipelineResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	if len(parts) != 3 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getPipeline(ctx, goqu.Ex{
		"organizations.name": parts[0],
		"projects.name":      parts[1],
		"pipelines.id":       gid.FromGlobalID(parts[2]),
	})
}

func (p *pipelines) GetPipelines(ctx context.Context, input *GetPipelinesInput) (*PipelinesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetPipelines")
	defer span.End()

	ex := goqu.And()
	selectEx := dialect.From(goqu.T("pipelines")).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipelines.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")}))

	if input.Filter != nil {
		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("pipelines.project_id").Eq(*input.Filter.ProjectID))
		}

		if input.Filter.PipelineTemplateID != nil {
			ex = ex.Append(goqu.I("pipelines.pipeline_template_id").Eq(*input.Filter.PipelineTemplateID))
		}

		if input.Filter.Superseded != nil {
			ex = ex.Append(goqu.I("pipelines.superseded").Eq(*input.Filter.Superseded))
		}

		if input.Filter.ParentPipelineID != nil {
			ex = ex.Append(goqu.I("pipelines.parent_pipeline_id").Eq(*input.Filter.ParentPipelineID))
		}

		if input.Filter.ParentNestedPipelineNodePath != nil {
			ex = ex.Append(goqu.I("pipelines.parent_pipeline_node_path").Eq(*input.Filter.ParentNestedPipelineNodePath))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.PipelineIDs) > 0 {
			ex = ex.Append(goqu.I("pipelines.id").In(input.Filter.PipelineIDs))
		}

		if input.Filter.Started != nil {
			if *input.Filter.Started {
				ex = ex.Append(goqu.I("pipelines.started_at").IsNotNull())
			} else {
				ex = ex.Append(goqu.I("pipelines.started_at").IsNull())
			}
		}

		if input.Filter.Completed != nil {
			if *input.Filter.Completed {
				ex = ex.Append(goqu.I("pipelines.completed_at").IsNotNull())
			} else {
				ex = ex.Append(goqu.I("pipelines.completed_at").IsNull())
			}
		}

		if input.Filter.EnvironmentName != nil {
			ex = ex.Append(goqu.I("pipelines.environment_name").Eq(*input.Filter.EnvironmentName))
		}

		if len(input.Filter.PipelineTypes) > 0 {
			ex = ex.Append(goqu.I("pipelines.type").In(input.Filter.PipelineTypes))
		}

		if input.Filter.ReleaseID != nil {
			ex = ex.Append(goqu.I("pipelines.release_id").Eq(*input.Filter.ReleaseID))
		}

		if input.Filter.TimeRangeStart != nil {
			// Must use UTC here otherwise, queries will return unexpected results.
			ex = ex.Append(goqu.I("pipelines.created_at").Gte(input.Filter.TimeRangeStart.UTC()))
		}

		if len(input.Filter.PipelineStatuses) > 0 {
			ex = ex.Append(goqu.I("pipelines.status").In(input.Filter.PipelineStatuses))
		}

		// Check if any node filters are provided
		if len(input.Filter.NodeTypes) > 0 || len(input.Filter.NodeStatuses) > 0 {
			nodeWhereExp := goqu.And()
			if len(input.Filter.NodeTypes) > 0 {
				nodeWhereExp = nodeWhereExp.Append(goqu.I("node_type").In(input.Filter.NodeTypes))
			}
			if len(input.Filter.NodeStatuses) > 0 {
				nodeWhereExp = nodeWhereExp.Append(goqu.I("status").In(input.Filter.NodeStatuses))
			}
			// Use a subquery to get the pipeline IDs that match the node filters
			ex = ex.Append(goqu.I("pipelines.id").In(dialect.From("pipeline_nodes").
				Select("pipeline_id").
				Where(nodeWhereExp)))
		}
	}

	query := selectEx.Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "pipelines", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Pipeline{}
	for rows.Next() {
		item, err := scanPipeline(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	pipelinesToHydrate := []*models.Pipeline{}
	for _, pipeline := range results {
		pipelineCopy := pipeline
		pipelinesToHydrate = append(pipelinesToHydrate, &pipelineCopy)
	}

	if err := p.hydratePipelines(ctx, p.dbClient.getConnection(ctx), pipelinesToHydrate); err != nil {
		return nil, err
	}

	hydratedPipelines := []models.Pipeline{}
	for _, pipeline := range pipelinesToHydrate {
		hydratedPipelines = append(hydratedPipelines, *pipeline)
	}

	result := &PipelinesResult{
		PageInfo:  rows.GetPageInfo(),
		Pipelines: hydratedPipelines,
	}

	return result, nil
}

func (p *pipelines) UpdatePipeline(ctx context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "db.UpdatePipeline")
	defer span.End()

	tx, err := p.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			p.dbClient.logger.Errorf("failed to rollback tx for UpdatePipeline: %v", txErr)
		}
	}()

	timestamp := currentTime()

	sql, args, err := dialect.From("pipelines").
		Prepared(true).
		With("pipelines",
			dialect.Update("pipelines").
				Set(goqu.Record{
					"version":         goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":      timestamp,
					"status":          pipeline.Status,
					"superseded":      pipeline.Superseded,
					"started_at":      pipeline.Timestamps.StartedTimestamp,
					"completed_at":    pipeline.Timestamps.CompletedTimestamp,
					"approval_status": pipeline.ApprovalStatus,
				}).
				Where(goqu.Ex{"id": pipeline.Metadata.ID, "version": pipeline.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipelines.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedPipeline, err := scanPipeline(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	// Must delete previous approval rules.
	deleteApprovalRulesSQL, deleteArgs, err := dialect.Delete("pipeline_approval_rule_relation").
		Prepared(true).
		Where(
			goqu.Ex{
				"pipeline_id": updatedPipeline.Metadata.ID,
			},
		).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL to delete pipeline approval rules", errors.WithSpan(span))
	}

	if _, err := tx.Exec(ctx, deleteApprovalRulesSQL, deleteArgs...); err != nil {
		return nil, errors.Wrap(err, "failed to delete pipeline approval rules", errors.WithSpan(span))
	}

	if err := p.insertApprovalRules(ctx, tx, pipeline.ApprovalRuleIDs, &updatedPipeline.Metadata.ID, nil, models.ApprovalTypePipeline); err != nil {
		return nil, errors.Wrap(err, "failed to insert approval rules while updating pipeline", errors.WithSpan(span))
	}

	if err := p.updatePipelineNodes(ctx, tx, pipeline); err != nil {
		return nil, err
	}

	if err := p.hydratePipelines(ctx, tx, []*models.Pipeline{updatedPipeline}); err != nil {
		return nil, err
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return updatedPipeline, nil
}

func (p *pipelines) CreatePipeline(ctx context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "db.CreatePipeline")
	defer span.End()

	annotations := map[string]*pipelineAnnotation{}
	for _, annotation := range pipeline.Annotations {
		annotations[annotation.Key] = &pipelineAnnotation{
			Value: annotation.Value,
			Link:  annotation.Link,
		}
	}

	annotationsJSON, err := json.Marshal(annotations)
	if err != nil {
		return nil, err
	}

	tx, err := p.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			p.dbClient.logger.Errorf("failed to rollback tx for CreatePipeline: %v", txErr)
		}
	}()

	timestamp := currentTime()

	sql, args, err := dialect.From("pipelines").
		Prepared(true).
		With("pipelines",
			dialect.Insert("pipelines").Rows(
				goqu.Record{
					"id":                        newResourceID(),
					"version":                   initialResourceVersion,
					"created_at":                timestamp,
					"updated_at":                timestamp,
					"created_by":                pipeline.CreatedBy,
					"project_id":                pipeline.ProjectID,
					"pipeline_template_id":      pipeline.PipelineTemplateID,
					"status":                    pipeline.Status,
					"release_id":                pipeline.ReleaseID,
					"parent_pipeline_id":        pipeline.ParentPipelineID,
					"type":                      pipeline.Type,
					"when_condition":            pipeline.When,
					"parent_pipeline_node_path": pipeline.ParentPipelineNodePath,
					"superseded":                pipeline.Superseded,
					"started_at":                pipeline.Timestamps.StartedTimestamp,
					"completed_at":              pipeline.Timestamps.CompletedTimestamp,
					"environment_name":          pipeline.EnvironmentName,
					"annotations":               annotationsJSON,
					"approval_status":           pipeline.ApprovalStatus,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipelines.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdPipeline, err := scanPipeline(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_pipelines_project_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_pipelines_pipeline_template_id":
					return nil, errors.New("pipeline template does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_pipelines_release_id":
					return nil, errors.New("release does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_pipelines_parent_pipeline_id":
					return nil, errors.New("parent pipeline does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	if err := p.insertApprovalRules(ctx, tx, pipeline.ApprovalRuleIDs, &createdPipeline.Metadata.ID, nil, models.ApprovalTypePipeline); err != nil {
		return nil, err
	}

	if err := p.insertPipelineNodes(ctx, tx, createdPipeline.Metadata.ID, pipeline.Stages); err != nil {
		return nil, err
	}

	if err := p.hydratePipelines(ctx, tx, []*models.Pipeline{createdPipeline}); err != nil {
		return nil, err
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return createdPipeline, nil
}

func (p *pipelines) getPipeline(ctx context.Context, exp exp.Expression) (*models.Pipeline, error) {
	query := dialect.From(goqu.T("pipelines")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"pipelines.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	pipeline, err := scanPipeline(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	if err := p.hydratePipelines(ctx, p.dbClient.getConnection(ctx), []*models.Pipeline{pipeline}); err != nil {
		return nil, err
	}

	return pipeline, nil
}

func (p *pipelines) hydratePipelines(ctx context.Context, con connection, pipelines []*models.Pipeline) error {
	if len(pipelines) == 0 {
		return nil
	}

	idList := []string{}
	for _, pipeline := range pipelines {
		idList = append(idList, pipeline.Metadata.ID)
	}

	query := dialect.From(goqu.T("pipeline_nodes")).
		Prepared(true).
		Select(pipelineNodeFieldList...).
		Where(goqu.I("pipeline_id").In(idList)).
		Order(goqu.I("pos").Asc())

	sql, args, err := query.ToSQL()
	if err != nil {
		return err
	}

	rows, err := con.Query(ctx, sql, args...)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil
		}
		return err
	}
	defer rows.Close()

	nodeMap := map[string]interface{}{}
	pipelineMap := map[string]*models.Pipeline{}

	for _, pipeline := range pipelines {
		pipelineMap[pipeline.Metadata.ID] = pipeline
	}

	// per pipeline node (nested pipeline or task)
	for rows.Next() {
		node, rErr := scanPipelineNode(rows)
		if rErr != nil {
			return rErr
		}

		model, mErr := node.toModel()
		if mErr != nil {
			return mErr
		}
		nodeMap[node.ID] = model

		if node.ParentID != nil {
			// Get parent node if it has one
			parent, ok := nodeMap[*node.ParentID]
			if !ok {
				return fmt.Errorf("failed to find parent node for node %s in pipeline %s", node.Path, node.PipelineID)
			}

			// Add end node to start node
			switch typed := parent.(type) {
			case *models.PipelineStage:
				switch childModel := model.(type) {
				case *models.PipelineTask:
					if node.Path == fmt.Sprintf("pipeline.stage.%s.task.%s", typed.Name, node.Name) {
						typed.Tasks = append(typed.Tasks, childModel)
					} else {
						return fmt.Errorf("unexpected stage task node type %s", node.NodeType)
					}
				case *models.NestedPipeline:
					typed.NestedPipelines = append(typed.NestedPipelines, childModel)
				default:
					return fmt.Errorf("unexpected stage child type %T", childModel)
				}
			case *models.PipelineTask:
				action, ok := model.(*models.PipelineAction)
				if !ok {
					return fmt.Errorf("expected action type but received: %T", model)
				}
				typed.Actions = append(typed.Actions, action)
			case *models.PipelineAction:
				return fmt.Errorf("action node is not expected as a start node: %s", node.ID)
			}
		} else {
			pipeline, ok := pipelineMap[node.PipelineID]
			if !ok {
				return fmt.Errorf("failed to find pipeline for node %s while hydrating pipeline", node.Path)
			}

			stage, ok := model.(*models.PipelineStage)
			if !ok {
				return fmt.Errorf("expected stage type but received: %T", model)
			}
			pipeline.Stages = append(pipeline.Stages, stage)
		}
	}

	if err = p.hydratePipelineApprovalRules(ctx, con, idList, pipelineMap, nodeMap); err != nil {
		return err
	}

	return nil
}

// hydratePipelineApprovalRules queries for approval rules associated with the
// pipeline / its nodes and assigns the result to the appropriate model.
func (p *pipelines) hydratePipelineApprovalRules(
	ctx context.Context,
	con connection,
	pipelineIDList []string,
	pipelineMap map[string]*models.Pipeline,
	nodeMap map[string]any,
) error {
	nodeIDList := []string{}
	for nodeID := range nodeMap {
		nodeIDList = append(nodeIDList, nodeID)
	}

	query := dialect.From(goqu.T("pipeline_approval_rule_relation")).
		Prepared(true).
		Select(pipelineApprovalRuleFieldList...).
		Where(
			goqu.Or(
				goqu.I("pipeline_id").In(pipelineIDList),
				goqu.I("pipeline_node_id").In(nodeIDList),
			),
		)

	sql, args, err := query.ToSQL()
	if err != nil {
		return err
	}

	rows, err := con.Query(ctx, sql, args...)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil
		}
		return err
	}
	defer rows.Close()

	// per pipeline node (nested pipeline or task)
	for rows.Next() {
		ruleRow, rErr := scanPipelineApprovalRule(rows)
		if rErr != nil {
			return rErr
		}

		switch ruleRow.ApprovalType {
		case models.ApprovalTypePipeline:
			// queried for pipeline IDs in the list, so pipeline ID cannot be nil
			pipelineModel, ok := pipelineMap[*ruleRow.PipelineID]
			if !ok {
				return errors.New("failed to find pipeline %d in pipeline map while hydrating pipeline approval rules", *ruleRow.PipelineID)
			}

			pipelineModel.ApprovalRuleIDs = append(pipelineModel.ApprovalRuleIDs, ruleRow.ApprovalRuleID)
		case models.ApprovalTypeTask:
			task, ok := nodeMap[*ruleRow.PipelineNodeID].(*models.PipelineTask)
			if !ok {
				return errors.New("failed to find task node %s in map while hydrating pipeline approval rules", *ruleRow.PipelineNodeID)
			}

			task.ApprovalRuleIDs = append(task.ApprovalRuleIDs, ruleRow.ApprovalRuleID)
		default:
			return errors.New("received unexpected rule type %s while hydrating pipeline approval rules", ruleRow.ApprovalType)
		}
	}

	return nil
}

func (p *pipelines) insertNode(ctx context.Context, con connection, node *pipelineNode) (string, error) {
	var tags, taskDependencies, nestedPipelineDependencies []byte
	var err error

	if node.TaskNodeAgentTags != nil {
		tags, err = json.Marshal(node.TaskNodeAgentTags)
		if err != nil {
			return "", fmt.Errorf("failed to marshal task node agent tags: %w", err)
		}
	}

	if node.TaskNodeDependencies != nil {
		taskDependencies, err = json.Marshal(node.TaskNodeDependencies)
		if err != nil {
			return "", fmt.Errorf("failed to marshal task node dependencies: %w", err)
		}
	}

	if node.NestedPipelineNodeDependencies != nil {
		nestedPipelineDependencies, err = json.Marshal(node.NestedPipelineNodeDependencies)
		if err != nil {
			return "", fmt.Errorf("failed to marshal nested pipeline dependencies: %w", err)
		}
	}

	sql, args, err := dialect.Insert("pipeline_nodes").
		Prepared(true).
		Rows(goqu.Record{
			"id":                                        newResourceID(),
			"path":                                      node.Path,
			"node_type":                                 node.NodeType,
			"pipeline_id":                               node.PipelineID,
			"parent_id":                                 node.ParentID,
			"pos":                                       node.Position,
			"status":                                    node.Status,
			"name":                                      node.Name,
			"action_node_plugin_name":                   node.ActionNodePluginName,
			"action_node_plugin_action_name":            node.ActionNodePluginActionName,
			"task_node_approval_status":                 node.TaskNodeApprovalStatus,
			"task_node_when":                            node.TaskNodeWhen,
			"task_node_image":                           node.TaskNodeImage,
			"task_node_interval_seconds":                node.TaskNodeIntervalSeconds,
			"task_node_latest_job_id":                   node.TaskNodeLatestJobID,
			"task_node_has_success_condition":           node.TaskNodeHasSuccessCondition,
			"task_node_max_attempts":                    node.TaskNodeMaxAttempts,
			"task_node_attempt_count":                   node.TaskNodeAttemptCount,
			"task_node_last_attempt_finished_time":      node.TaskNodeLastAttemptFinishedAt,
			"task_node_agent_tags":                      tags,
			"task_node_scheduled_start_time":            node.TaskNodeScheduledStartTime,
			"task_node_cron_expression":                 node.TaskNodeCronExpression,
			"task_node_cron_timezone":                   node.TaskNodeCronTimezone,
			"task_node_dependencies":                    taskDependencies,
			"task_node_on_error":                        node.TaskNodeOnError,
			"nested_pipeline_node_environment_name":     node.NestedPipelineNodeEnvironmentName,
			"nested_pipeline_node_latest_pipeline_id":   node.NestedPipelineNodeLatestPipelineID,
			"nested_pipeline_node_scheduled_start_time": node.NestedPipelineNodeScheduledStartTime,
			"nested_pipeline_node_cron_expression":      node.NestedPipelineNodeCronExpression,
			"nested_pipeline_node_cron_timezone":        node.NestedPipelineNodeCronTimezone,
			"nested_pipeline_node_approval_status":      node.NestedPipelineNodeApprovalStatus,
			"nested_pipeline_node_dependencies":         nestedPipelineDependencies,
			"nested_pipeline_node_on_error":             node.NestedPipelineNodeOnError,
		}).
		Returning(pipelineNodeFieldList...).ToSQL()
	if err != nil {
		return "", err
	}

	createdNode, err := scanPipelineNode(con.QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_pipeline_nodes_pipeline_id":
					return "", errors.New("pipeline does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_pipeline_nodes_nested_pipeline_node_template_id":
					return "", errors.New("nested pipeline template does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_pipeline_nodes_nested_pipeline_node_latest_pipeline_id":
					return "", errors.New("nested pipeline does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_pipeline_nodes_task_node_latest_job_id":
					return "", errors.New("latest job does not exist", errors.WithErrorCode(errors.ENotFound))
				}
			}
		}
		return "", err
	}

	return createdNode.ID, nil
}

// insertApprovalRules inserts approval rule bridging rows for a root pipeline
// or a pipeline node (a nested pipeline or task)
func (p *pipelines) insertApprovalRules(
	ctx context.Context,
	con connection,
	approvalRuleIDs []string,
	pipelineID *string, // for root pipelines
	pipelineNodeID *string, // for pipeline nodes (tasks)
	approvalType models.PipelineApprovalType,
) error {
	for _, approvalRuleID := range approvalRuleIDs {
		sql, args, err := dialect.Insert("pipeline_approval_rule_relation").
			Prepared(true).
			Rows(goqu.Record{
				"approval_rule_id": approvalRuleID,
				"pipeline_id":      pipelineID,
				"pipeline_node_id": pipelineNodeID,
				"approval_type":    approvalType,
			}).ToSQL()
		if err != nil {
			return errors.Wrap(err, "failed to generate SQL")
		}

		if _, err = con.Exec(ctx, sql, args...); err != nil {
			if pgErr := asPgError(err); pgErr != nil {
				if isForeignKeyViolation(pgErr) {
					switch pgErr.ConstraintName {
					case "fk_pipeline_approval_rule_relation_approval_rule_id":
						return errors.New("approval rule does not exist", errors.WithErrorCode(errors.ENotFound))
					case "fk_pipeline_approval_rule_relation_pipeline_node_id":
						return errors.New("pipeline node does not exist", errors.WithErrorCode(errors.ENotFound))
					}
				}
				return errors.Wrap(err, "failed to create pipeline approval rule relation")
			}
		}
	}

	return nil
}

func (p *pipelines) updateNode(ctx context.Context, con connection, node *pipelineNode) error {
	var nodeErrors []byte
	var err error

	if node.Errors != nil {
		nodeErrors, err = json.Marshal(node.Errors)
		if err != nil {
			return fmt.Errorf("failed to marshal node errors: %w", err)
		}
	}

	sql, args, err := dialect.Update("pipeline_nodes").
		Prepared(true).
		Set(
			goqu.Record{
				"status":                                    node.Status,
				"task_node_approval_status":                 node.TaskNodeApprovalStatus,
				"task_node_latest_job_id":                   node.TaskNodeLatestJobID,
				"task_node_attempt_count":                   node.TaskNodeAttemptCount,
				"task_node_last_attempt_finished_time":      node.TaskNodeLastAttemptFinishedAt,
				"task_node_scheduled_start_time":            node.TaskNodeScheduledStartTime,
				"task_node_cron_expression":                 node.TaskNodeCronExpression,
				"task_node_cron_timezone":                   node.TaskNodeCronTimezone,
				"nested_pipeline_node_latest_pipeline_id":   node.NestedPipelineNodeLatestPipelineID,
				"nested_pipeline_node_scheduled_start_time": node.NestedPipelineNodeScheduledStartTime,
				"nested_pipeline_node_cron_expression":      node.NestedPipelineNodeCronExpression,
				"nested_pipeline_node_cron_timezone":        node.NestedPipelineNodeCronTimezone,
				"nested_pipeline_node_approval_status":      node.NestedPipelineNodeApprovalStatus,
				"node_errors":                               nodeErrors,
			},
		).Where(goqu.Ex{"pipeline_id": node.PipelineID, "path": node.Path}).
		Returning(pipelineNodeFieldList...).
		ToSQL()
	if err != nil {
		return err
	}

	updatedNode, err := scanPipelineNode(con.QueryRow(ctx, sql, args...))
	if err != nil {
		return err
	}

	// Create bridging rows from node.TaskNodeApprovalRuleIDs.
	var (
		approvalRuleIDs = []string{}
		approvalType    models.PipelineApprovalType
	)

	switch node.NodeType {
	case statemachine.TaskNodeType:
		approvalRuleIDs = node.TaskNodeApprovalRuleIDs
		approvalType = models.ApprovalTypeTask
		// More types can be added here...
	}

	// Must delete previous approval rules.
	deleteApprovalRulesSQL, deleteArgs, err := dialect.Delete("pipeline_approval_rule_relation").
		Prepared(true).
		Where(
			goqu.Ex{
				"pipeline_node_id": updatedNode.ID,
			},
		).ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL to delete approval rules")
	}

	if _, err := con.Exec(ctx, deleteApprovalRulesSQL, deleteArgs...); err != nil {
		return err
	}

	if err := p.insertApprovalRules(ctx, con, approvalRuleIDs, nil, &updatedNode.ID, approvalType); err != nil {
		return err
	}

	return nil
}

func (p *pipelines) insertPipelineNodes(ctx context.Context, con connection,
	pipelineID string, stages []*models.PipelineStage) error {
	ctx, span := tracer.Start(ctx, "db.insertPipelineNodes")
	defer span.End()

	pos := 0

	// Create stages
	for _, stage := range stages {
		stageNodeID, err := p.insertNode(ctx, con, &pipelineNode{
			NodeType:   statemachine.StageNodeType,
			Path:       stage.Path,
			Status:     stage.Status,
			PipelineID: pipelineID,
			Name:       stage.Name,
			Position:   pos,
		})
		if err != nil {
			return errors.Wrap(err, "failed to insert stage node", errors.WithSpan(span))
		}

		pos++

		// Create nested pipelines
		for _, nestedPipeline := range stage.NestedPipelines {
			nestedPipelineCopy := nestedPipeline

			var cronExpression *string
			var cronTimezone *string

			if nestedPipeline.CronSchedule != nil {
				cronExpression = ptr.String(nestedPipeline.CronSchedule.Expression)
				cronTimezone = nestedPipeline.CronSchedule.Timezone
			}

			node := &pipelineNode{
				NodeType:                             statemachine.PipelineNodeType,
				Path:                                 nestedPipeline.Path,
				Status:                               nestedPipeline.Status,
				PipelineID:                           pipelineID,
				Name:                                 nestedPipeline.Name,
				NestedPipelineNodeEnvironmentName:    nestedPipeline.EnvironmentName,
				NestedPipelineNodeScheduledStartTime: nestedPipeline.ScheduledStartTime,
				NestedPipelineNodeCronExpression:     cronExpression,
				NestedPipelineNodeCronTimezone:       cronTimezone,
				NestedPipelineNodeApprovalStatus:     string(nestedPipeline.ApprovalStatus),
				NestedPipelineNodeDependencies:       nestedPipeline.Dependencies,
				NestedPipelineNodeOnError:            nestedPipeline.OnError,
				ParentID:                             &stageNodeID,
				Position:                             pos,
			}
			if nestedPipeline.LatestPipelineID != "" {
				node.NestedPipelineNodeLatestPipelineID = &nestedPipelineCopy.LatestPipelineID
			}
			if _, err := p.insertNode(ctx, con, node); err != nil {
				return errors.Wrap(err, "failed to insert nested pipeline node", errors.WithSpan(span))
			}

			pos++
		}

		// Create tasks and actions
		if err := p.insertTasks(ctx, con, pipelineID, stageNodeID, &pos, stage.Tasks); err != nil {
			return errors.Wrap(err, "failed to insert task nodes", errors.WithSpan(span))
		}
	}

	return nil
}

// insertTasks inserts tasks and actions into the database
// and updates the position pointer.
func (p *pipelines) insertTasks(
	ctx context.Context,
	con connection,
	pipelineID string,
	parentID string,
	pos *int,
	tasks []*models.PipelineTask,
) error {
	// Create tasks
	for _, task := range tasks {
		var interval *int
		if task.Interval != nil {
			interval = ptr.Int(int(task.Interval.Seconds()))
		}

		var cronExpression *string
		var cronTimezone *string

		if task.CronSchedule != nil {
			cronExpression = ptr.String(task.CronSchedule.Expression)
			cronTimezone = task.CronSchedule.Timezone
		}

		taskNodeID, err := p.insertNode(ctx, con, &pipelineNode{
			NodeType:                      statemachine.TaskNodeType,
			Path:                          task.Path,
			Status:                        task.Status,
			PipelineID:                    pipelineID,
			Name:                          task.Name,
			Position:                      *pos,
			ParentID:                      &parentID,
			TaskNodeApprovalStatus:        string(task.ApprovalStatus),
			TaskNodeApprovalRuleIDs:       task.ApprovalRuleIDs,
			TaskNodeWhen:                  string(task.When),
			TaskNodeIntervalSeconds:       interval,
			TaskNodeLatestJobID:           task.LatestJobID,
			TaskNodeHasSuccessCondition:   task.HasSuccessCondition,
			TaskNodeMaxAttempts:           task.MaxAttempts,
			TaskNodeAttemptCount:          task.AttemptCount,
			TaskNodeLastAttemptFinishedAt: task.LastAttemptFinishedAt,
			TaskNodeAgentTags:             task.AgentTags,
			TaskNodeScheduledStartTime:    task.ScheduledStartTime,
			TaskNodeCronExpression:        cronExpression,
			TaskNodeCronTimezone:          cronTimezone,
			TaskNodeDependencies:          task.Dependencies,
			TaskNodeImage:                 task.Image,
			TaskNodeOnError:               task.OnError,
		})
		if err != nil {
			return err
		}

		*pos++

		// Create actions
		for _, action := range task.Actions {
			_, err := p.insertNode(ctx, con, &pipelineNode{
				NodeType:                   statemachine.ActionNodeType,
				Path:                       action.Path,
				Status:                     action.Status,
				PipelineID:                 pipelineID,
				Position:                   *pos,
				ParentID:                   &taskNodeID,
				Name:                       action.Name,
				ActionNodePluginName:       action.PluginName,
				ActionNodePluginActionName: action.ActionName,
			})
			if err != nil {
				return err
			}

			*pos++
		}
	}

	return nil
}

func (p *pipelines) updatePipelineNodes(ctx context.Context, con connection, pipeline *models.Pipeline) error {
	ctx, span := tracer.Start(ctx, "db.updatePipelineNodes")
	defer span.End()

	pipelineID := pipeline.Metadata.ID

	// Update stages
	for _, stage := range pipeline.Stages {
		if err := p.updateNode(ctx, con, &pipelineNode{
			PipelineID: pipelineID,
			Path:       stage.Path,
			Status:     stage.Status,
			NodeType:   statemachine.StageNodeType,
		}); err != nil {
			return errors.Wrap(err, "failed to update stage node", errors.WithSpan(span))
		}

		// Update nested pipelines
		for _, nestedPipeline := range stage.NestedPipelines {
			var cronExpression *string
			var cronTimezone *string

			if nestedPipeline.CronSchedule != nil {
				cronExpression = ptr.String(nestedPipeline.CronSchedule.Expression)
				cronTimezone = nestedPipeline.CronSchedule.Timezone
			}

			if err := p.updateNode(ctx, con, &pipelineNode{
				PipelineID:                           pipelineID,
				Path:                                 nestedPipeline.Path,
				Status:                               nestedPipeline.Status,
				NodeType:                             statemachine.PipelineNodeType,
				NestedPipelineNodeLatestPipelineID:   &nestedPipeline.LatestPipelineID,
				NestedPipelineNodeScheduledStartTime: nestedPipeline.ScheduledStartTime,
				NestedPipelineNodeCronExpression:     cronExpression,
				NestedPipelineNodeCronTimezone:       cronTimezone,
				NestedPipelineNodeApprovalStatus:     string(nestedPipeline.ApprovalStatus),
				Errors:                               nestedPipeline.Errors,
			}); err != nil {
				return errors.Wrap(err, "failed to update nested pipeline node", errors.WithSpan(span))
			}
		}

		// Update tasks
		if err := p.updateTasks(ctx, con, pipelineID, stage.Tasks); err != nil {
			return errors.Wrap(err, "failed to update task nodes", errors.WithSpan(span))
		}
	}

	return nil
}

// updateTasks updates tasks and actions in the database
func (p *pipelines) updateTasks(
	ctx context.Context,
	con connection,
	pipelineID string,
	tasks []*models.PipelineTask,
) error {
	// Update tasks
	for _, task := range tasks {

		var cronExpression *string
		var cronTimezone *string

		if task.CronSchedule != nil {
			cronExpression = ptr.String(task.CronSchedule.Expression)
			cronTimezone = task.CronSchedule.Timezone
		}

		if err := p.updateNode(ctx, con, &pipelineNode{
			PipelineID:                    pipelineID,
			Path:                          task.Path,
			Status:                        task.Status,
			NodeType:                      statemachine.TaskNodeType,
			TaskNodeApprovalStatus:        string(task.ApprovalStatus),
			TaskNodeApprovalRuleIDs:       task.ApprovalRuleIDs,
			TaskNodeLatestJobID:           task.LatestJobID,
			TaskNodeScheduledStartTime:    task.ScheduledStartTime,
			TaskNodeCronExpression:        cronExpression,
			TaskNodeCronTimezone:          cronTimezone,
			TaskNodeAttemptCount:          task.AttemptCount,
			TaskNodeLastAttemptFinishedAt: task.LastAttemptFinishedAt,
			Errors:                        task.Errors,
		}); err != nil {
			return err
		}

		// Update actions
		for _, action := range task.Actions {
			if err := p.updateNode(ctx, con, &pipelineNode{
				PipelineID: pipelineID,
				Path:       action.Path,
				Status:     action.Status,
				NodeType:   statemachine.ActionNodeType,
			}); err != nil {
				return err
			}
		}
	}

	return nil
}

func (*pipelines) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range pipelineFieldList {
		selectFields = append(selectFields, fmt.Sprintf("pipelines.%s", field))
	}

	selectFields = append(selectFields, "projects.name")
	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanPipeline(row scanner) (*models.Pipeline, error) {
	var projectName, organizationName string
	pipeline := &models.Pipeline{}

	annotations := map[string]*pipelineAnnotation{}

	fields := []interface{}{
		&pipeline.Metadata.ID,
		&pipeline.Metadata.CreationTimestamp,
		&pipeline.Metadata.LastUpdatedTimestamp,
		&pipeline.Metadata.Version,
		&pipeline.CreatedBy,
		&pipeline.ProjectID,
		&pipeline.PipelineTemplateID,
		&pipeline.Status,
		&pipeline.ReleaseID,
		&pipeline.ParentPipelineID,
		&pipeline.ParentPipelineNodePath,
		&pipeline.Type,
		&pipeline.When,
		&pipeline.Superseded,
		&pipeline.Timestamps.StartedTimestamp,
		&pipeline.Timestamps.CompletedTimestamp,
		&pipeline.EnvironmentName,
		&annotations,
		&pipeline.ApprovalStatus,
		&projectName,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	pipeline.Annotations = []*models.PipelineAnnotation{}
	for key, annotation := range annotations {
		pipeline.Annotations = append(pipeline.Annotations, &models.PipelineAnnotation{
			Key:   key,
			Value: annotation.Value,
			Link:  annotation.Link,
		})
	}

	pipeline.Metadata.PRN = models.PipelineResource.BuildPRN(
		organizationName,
		projectName,
		gid.ToGlobalID(gid.PipelineType, pipeline.Metadata.ID),
	)

	return pipeline, nil
}

func scanPipelineNode(row scanner) (*pipelineNode, error) {
	node := &pipelineNode{}

	fields := []interface{}{
		&node.ID,
		&node.Path,
		&node.NodeType,
		&node.PipelineID,
		&node.ParentID,
		&node.Position,
		&node.Status,
		&node.Name,
		&node.ActionNodePluginName,
		&node.ActionNodePluginActionName,
		&node.TaskNodeApprovalStatus,
		&node.TaskNodeWhen,
		&node.TaskNodeIntervalSeconds,
		&node.TaskNodeLatestJobID,
		&node.TaskNodeImage,
		&node.TaskNodeHasSuccessCondition,
		&node.TaskNodeMaxAttempts,
		&node.TaskNodeAttemptCount,
		&node.TaskNodeLastAttemptFinishedAt,
		&node.TaskNodeAgentTags,
		&node.TaskNodeScheduledStartTime,
		&node.TaskNodeCronExpression,
		&node.TaskNodeCronTimezone,
		&node.TaskNodeDependencies,
		&node.TaskNodeOnError,
		&node.NestedPipelineNodeEnvironmentName,
		&node.NestedPipelineNodeLatestPipelineID,
		&node.NestedPipelineNodeScheduledStartTime,
		&node.NestedPipelineNodeCronExpression,
		&node.NestedPipelineNodeCronTimezone,
		&node.NestedPipelineNodeApprovalStatus,
		&node.NestedPipelineNodeDependencies,
		&node.NestedPipelineNodeOnError,
		&node.Errors,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	return node, nil
}

func scanPipelineApprovalRule(row scanner) (*pipelineApprovalRule, error) {
	elem := &pipelineApprovalRule{}

	fields := []interface{}{
		&elem.ApprovalRuleID,
		&elem.PipelineID,
		&elem.PipelineNodeID,
		&elem.ApprovalType,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	return elem, nil
}
