package db

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

//go:generate go tool mockery --name ToDoItems --inpackage --case underscore

// todoItemAssignees are the subjects a ToDo item is assigned to.
type todoItemAssignees struct {
	UserID     *string
	TeamID     *string
	ToDoItemID string
}

// todoItemResolvedByUser represent a user that has resolved a todo item.
type todoItemResolvedByUser struct {
	UserID     string
	ToDoItemID string
}

// ToDoItems encapsulates the logic to access todo items from the data source.
type ToDoItems interface {
	GetToDoItemByPRN(ctx context.Context, prn string) (*models.ToDoItem, error)
	GetToDoItems(ctx context.Context, input *GetToDoItemsInput) (*ToDoItemsResult, error)
	CreateToDoItem(ctx context.Context, item *models.ToDoItem) (*models.ToDoItem, error)
	UpdateToDoItem(ctx context.Context, item *models.ToDoItem) (*models.ToDoItem, error)
}

// ToDoItemSortableField represents a sortable field for a ToDo item.
type ToDoItemSortableField string

// ToDoSortableField constants.
const (
	ToDoItemSortableFieldCreatedAtAsc  ToDoItemSortableField = "CREATED_AT_ASC"
	ToDoItemSortableFieldCreatedAtDesc ToDoItemSortableField = "CREATED_AT_DESC"
	ToDoItemSortableFieldUpdatedAtAsc  ToDoItemSortableField = "UPDATED_AT_ASC"
	ToDoItemSortableFieldUpdatedAtDesc ToDoItemSortableField = "UPDATED_AT_DESC"
)

func (sf ToDoItemSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case ToDoItemSortableFieldCreatedAtAsc, ToDoItemSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "todo_items", Col: "created_at"}
	case ToDoItemSortableFieldUpdatedAtAsc, ToDoItemSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "todo_items", Col: "updated_at"}
	default:
		return nil
	}
}

func (sf ToDoItemSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// ToDoItemMembershipRequirement represents the membership requirement for a ToDo item.
type ToDoItemMembershipRequirement struct {
	UserID *string
}

// ToDoItemPayloadFilter filters for specific fields on a payload.
type ToDoItemPayloadFilter struct {
	PipelineTaskPath *string
}

// ToDoItemFilter represents a filter for a ToDo item.
type ToDoItemFilter struct {
	UserID                *string
	OrganizationID        *string
	ProjectID             *string
	PipelineTargetID      *string
	Resolved              *bool
	MembershipRequirement *ToDoItemMembershipRequirement
	PayloadFilter         *ToDoItemPayloadFilter
	TargetTypes           []models.ToDoItemTargetType
	ToDoItemIDs           []string
}

// GetToDoItemsInput represents the input for the GetToDoItems method.
type GetToDoItemsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ToDoItemSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter contains the supported fields for filtering the result set
	Filter *ToDoItemFilter
}

// ToDoItemsResult contains the response data and page information
type ToDoItemsResult struct {
	PageInfo  *pagination.PageInfo
	ToDoItems []*models.ToDoItem
}

type todoItems struct {
	dbClient *Client
}

var todoItemsFieldList = append(metadataFieldList,
	"organization_id",
	"project_id",
	"auto_resolved",
	"resolvable",
	"target_type",
	"payload",
	"pipeline_target_id",
)

// NewToDoItems returns a new ToDoItems instance.
func NewToDoItems(dbClient *Client) ToDoItems {
	return &todoItems{dbClient: dbClient}
}

func (t *todoItems) GetToDoItemByPRN(ctx context.Context, prn string) (*models.ToDoItem, error) {
	ctx, span := tracer.Start(ctx, "db.GetToDoItemByPRN")
	defer span.End()

	path, err := models.ToDoItemResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return t.getToDoItem(ctx, goqu.Ex{"todo_items.id": gid.FromGlobalID(path)})
}

func (t *todoItems) GetToDoItems(ctx context.Context, input *GetToDoItemsInput) (*ToDoItemsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetToDoItems")
	defer span.End()

	ex := goqu.And()

	selectEx := dialect.From("todo_items").Select(t.getSelectFields()...)

	if input.Filter != nil {
		if input.Filter.UserID != nil {
			ex = ex.Append(
				goqu.I("todo_items.id").In(
					dialect.From("todo_item_assignees").
						Select("todo_item_id").
						Where(
							// Use an OR condition here to filter for both todo items assigned
							// directly to the user and also their team.
							goqu.Or(
								goqu.I("todo_item_assignees.user_id").Eq(*input.Filter.UserID),
								goqu.I("todo_item_assignees.team_id").In(
									dialect.From("team_members").
										Select("team_id").
										Where(goqu.I("user_id").Eq(*input.Filter.UserID)),
								),
							),
						),
				),
			)
		}

		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("todo_items.organization_id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("todo_items.project_id").Eq(*input.Filter.ProjectID))
		}

		if len(input.Filter.TargetTypes) > 0 {
			ex = ex.Append(goqu.I("todo_items.target_type").In(input.Filter.TargetTypes))
		}

		if input.Filter.PipelineTargetID != nil {
			ex = ex.Append(goqu.I("todo_items.pipeline_target_id").Eq(*input.Filter.PipelineTargetID))
		}

		if input.Filter.Resolved != nil {
			if input.Filter.UserID != nil {
				// For a user we must check for the presence of the user id in the "todo_item_resolved_for_users" table
				// which indicates that they've completed the item or vice-versa along with the value of the "auto_resolved" column.
				subQuery := dialect.From("todo_item_resolved_for_users").
					Select("todo_item_id").
					Where(goqu.I("user_id").Eq(*input.Filter.UserID))

				if *input.Filter.Resolved {
					// Use an OR filter here to get both items the user has
					// completed or were marked as "AutoResolved" otherwise.
					ex = ex.Append(goqu.Or(
						goqu.I("todo_items.id").In(subQuery),
						goqu.I("todo_items.auto_resolved").IsTrue(),
					))
				} else {
					// Otherwise get only those items which aren't marked as AutoResolved and haven't been completed by the user.
					ex = ex.Append(
						goqu.I("todo_items.id").NotIn(subQuery),
						goqu.I("todo_items.auto_resolved").IsFalse(),
					)
				}
			} else {
				// By default, only filter for value of "auto_resolved" column.
				ex = ex.Append(goqu.I("todo_items.auto_resolved").Eq(*input.Filter.Resolved))
			}
		}

		if len(input.Filter.ToDoItemIDs) > 0 {
			ex = ex.Append(goqu.I("todo_items.id").In(input.Filter.ToDoItemIDs))
		}

		if input.Filter.MembershipRequirement != nil {
			selectEx = selectEx.InnerJoin(goqu.T("organizations"), goqu.On(goqu.I("organizations.id").Eq(goqu.I("todo_items.organization_id"))))
			selectEx = selectEx.LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("todo_items.project_id"))))

			ex = ex.Append(
				goqu.Or(
					organizationMembershipExpressionBuilder{
						userID: input.Filter.MembershipRequirement.UserID,
					}.build(),
					projectMembershipExpressionBuilder{
						userID: input.Filter.MembershipRequirement.UserID,
					}.build(),
				),
			)
		}

		if input.Filter.PayloadFilter != nil {
			if input.Filter.PayloadFilter.PipelineTaskPath != nil {
				ex = ex.Append(goqu.L("payload->>'taskPath'").Eq(*input.Filter.PayloadFilter.PipelineTaskPath))
			}
		}
	}

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	query := selectEx.Where(ex)

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "todo_items", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, t.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}
	defer rows.Close()

	results := []*models.ToDoItem{}
	for rows.Next() {
		item, sErr := scanToDoItem(rows)
		if sErr != nil {
			return nil, errors.Wrap(sErr, "failed to scan rows", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err = rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	if err = t.hydrateToDoItems(ctx, t.dbClient.getConnection(ctx), results); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate todo item", errors.WithSpan(span))
	}

	return &ToDoItemsResult{
		PageInfo:  rows.GetPageInfo(),
		ToDoItems: results,
	}, nil
}

func (t *todoItems) CreateToDoItem(ctx context.Context, item *models.ToDoItem) (*models.ToDoItem, error) {
	ctx, span := tracer.Start(ctx, "db.CreateToDoItem")
	defer span.End()

	// Use target type to fan out target ID to the various columns.
	var (
		pipelineTargetID *string
	)

	switch item.TargetType {
	case models.PipelineApprovalTarget,
		models.TaskApprovalTarget:
		pipelineTargetID = &item.TargetID
	default:
		return nil, errors.New("invalid todo item target type %s", item.TargetType, errors.WithSpan(span))
	}

	timestamp := currentTime()

	tx, err := t.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			t.dbClient.logger.Errorf("failed to rollback tx in DB CreateToDoItem: %v", txErr)
		}
	}()

	sql, args, err := dialect.Insert("todo_items").
		Prepared(true).
		Rows(
			goqu.Record{
				"id":                 newResourceID(),
				"version":            initialResourceVersion,
				"created_at":         timestamp,
				"updated_at":         timestamp,
				"organization_id":    item.OrganizationID,
				"project_id":         item.ProjectID,
				"auto_resolved":      item.AutoResolved,
				"resolvable":         item.Resolvable,
				"target_type":        item.TargetType,
				"payload":            item.Payload,
				"pipeline_target_id": pipelineTargetID,
			}).
		Returning(todoItemsFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	createdItem, err := scanToDoItem(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_todo_items_organization_id":
					return nil, errors.Wrap(pgErr, "organization does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_todo_items_project_id":
					return nil, errors.Wrap(pgErr, "project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_todo_items_pipeline_target_id":
					return nil, errors.Wrap(pgErr, "pipeline does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	for ix := range item.UserIDs {
		if err = t.insertToDoItemAssignees(ctx, tx, &todoItemAssignees{
			ToDoItemID: createdItem.Metadata.ID,
			UserID:     &item.UserIDs[ix],
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert todo item user assignee", errors.WithSpan(span))
		}
	}

	for ix := range item.TeamIDs {
		if err = t.insertToDoItemAssignees(ctx, tx, &todoItemAssignees{
			ToDoItemID: createdItem.Metadata.ID,
			TeamID:     &item.TeamIDs[ix],
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert todo item team assignee", errors.WithSpan(span))
		}
	}

	for _, resolvedByUserID := range item.ResolvedByUsers {
		if err = t.insertToDoItemResolvedByUser(ctx, tx, &todoItemResolvedByUser{
			UserID:     resolvedByUserID,
			ToDoItemID: createdItem.Metadata.ID,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert todo item resolved by", errors.WithSpan(span))
		}
	}

	if err = t.hydrateToDoItems(ctx, tx, []*models.ToDoItem{createdItem}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate todo item", errors.WithSpan(span))
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	return createdItem, nil
}

func (t *todoItems) UpdateToDoItem(ctx context.Context, item *models.ToDoItem) (*models.ToDoItem, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateToDoItem")
	defer span.End()

	timestamp := currentTime()

	tx, err := t.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			t.dbClient.logger.Errorf("failed to rollback tx in DB UpdateToDoItem: %v", txErr)
		}
	}()

	sql, args, err := dialect.Update("todo_items").
		Prepared(true).
		Set(
			goqu.Record{
				"version":       goqu.L("? + ?", goqu.C("version"), 1),
				"updated_at":    timestamp,
				"auto_resolved": item.AutoResolved,
			},
		).Where(goqu.Ex{"id": item.Metadata.ID, "version": item.Metadata.Version}).
		Returning(todoItemsFieldList...).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	updatedItem, err := scanToDoItem(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	// Delete all previous entries.
	deleteToDoItemResolvedBySQL, args, err := dialect.Delete("todo_item_resolved_for_users").
		Prepared(true).
		Where(
			goqu.Ex{
				"todo_item_id": updatedItem.Metadata.ID,
			},
		).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = tx.Exec(ctx, deleteToDoItemResolvedBySQL, args...); err != nil {
		return nil, errors.Wrap(err, "failed to execute DB query", errors.WithSpan(span))
	}

	for _, resolvedByUserID := range item.ResolvedByUsers {
		if err = t.insertToDoItemResolvedByUser(ctx, tx, &todoItemResolvedByUser{
			UserID:     resolvedByUserID,
			ToDoItemID: updatedItem.Metadata.ID,
		}); err != nil {
			return nil, errors.Wrap(err, "failed to insert todo item resolved by", errors.WithSpan(span))
		}
	}

	if err = t.hydrateToDoItems(ctx, tx, []*models.ToDoItem{updatedItem}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate todo item", errors.WithSpan(span))
	}

	if err = tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	return updatedItem, nil
}

func (t *todoItems) getToDoItem(ctx context.Context, exp exp.Ex) (*models.ToDoItem, error) {
	sql, args, err := dialect.From("todo_items").
		Prepared(true).
		Select(todoItemsFieldList...).
		Where(exp).
		ToSQL()

	if err != nil {
		return nil, err
	}

	todoItem, err := scanToDoItem(t.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	if err := t.hydrateToDoItems(ctx, t.dbClient.getConnection(ctx), []*models.ToDoItem{todoItem}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate todo item")
	}

	return todoItem, nil
}

func (t *todoItems) insertToDoItemAssignees(ctx context.Context, con connection, assignee *todoItemAssignees) error {
	sql, args, err := dialect.Insert("todo_item_assignees").
		Prepared(true).
		Rows(goqu.Record{
			"id":           newResourceID(),
			"todo_item_id": assignee.ToDoItemID,
			"user_id":      assignee.UserID,
			"team_id":      assignee.TeamID,
		}).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL")
	}

	if _, err := con.Exec(ctx, sql, args...); err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_todo_item_assignees_user_id":
					return errors.New("user does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_todo_item_assignees_team_id":
					return errors.New("team does not exist", errors.WithErrorCode(errors.ENotFound))
				}
			}

			if isUniqueViolation(pgErr) {
				return errors.New("todo item assignee already exists", errors.WithErrorCode(errors.EConflict))
			}
		}

		return errors.Wrap(err, "failed to execute query")
	}

	return nil
}

func (t *todoItems) insertToDoItemResolvedByUser(ctx context.Context, con connection, resolver *todoItemResolvedByUser) error {
	sql, args, err := dialect.Insert("todo_item_resolved_for_users").
		Prepared(true).
		Rows(goqu.Record{
			"todo_item_id": resolver.ToDoItemID,
			"user_id":      resolver.UserID,
		}).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL")
	}

	if _, err := con.Exec(ctx, sql, args...); err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_todo_item_resolved_for_users_user_id":
					return errors.New("user does not exist", errors.WithErrorCode(errors.ENotFound))
				case "fk_todo_item_resolved_for_users_team_id":
					return errors.New("team does not exist", errors.WithErrorCode(errors.ENotFound))
				}
			}

			if isUniqueViolation(pgErr) {
				return errors.New("user has already resolved the todo item", errors.WithErrorCode(errors.EConflict))
			}
		}

		return errors.Wrap(err, "failed to execute query")
	}

	return nil
}

func (t *todoItems) hydrateToDoItems(ctx context.Context, conn connection, items []*models.ToDoItem) error {
	if len(items) == 0 {
		return nil
	}

	todoItemMap := map[string]*models.ToDoItem{}
	todoItemIDs := []string{}
	for _, item := range items {
		item.UserIDs = []string{}
		item.TeamIDs = []string{}

		todoItemMap[item.Metadata.ID] = item
		todoItemIDs = append(todoItemIDs, item.Metadata.ID)
	}

	// Get the todo item resolved by.
	resolvedBySQL, resolvedByArgs, err := dialect.From("todo_item_resolved_for_users").
		Prepared(true).
		Select("todo_item_id", "user_id").
		Where(goqu.I("todo_item_id").In(todoItemIDs)).ToSQL()
	if err != nil {
		return err
	}

	resolvedByRows, err := conn.Query(ctx, resolvedBySQL, resolvedByArgs...)
	if err != nil {
		return err
	}
	defer resolvedByRows.Close()

	for resolvedByRows.Next() {
		resolvedBy, rErr := scanToDoItemResolvedByUser(resolvedByRows)
		if rErr != nil {
			return rErr
		}

		todoItem, ok := todoItemMap[resolvedBy.ToDoItemID]
		if !ok {
			return fmt.Errorf("todo item not found in todo item map: %s", resolvedBy.ToDoItemID)
		}

		todoItem.ResolvedByUsers = append(todoItem.ResolvedByUsers, resolvedBy.UserID)
	}

	// Get the todo item assignees.
	assigneesSQL, assigneesArgs, err := dialect.From("todo_item_assignees").
		Prepared(true).
		Select("todo_item_id", "user_id", "team_id").
		Where(goqu.I("todo_item_id").In(todoItemIDs)).ToSQL()
	if err != nil {
		return err
	}

	assigneeRows, err := conn.Query(ctx, assigneesSQL, assigneesArgs...)
	if err != nil {
		return err
	}
	defer assigneeRows.Close()

	// Scan rows
	for assigneeRows.Next() {
		assignee, err := scanToDoItemAssignee(assigneeRows)
		if err != nil {
			return err
		}

		todoItem, ok := todoItemMap[assignee.ToDoItemID]
		if !ok {
			return fmt.Errorf("todo item not found in todo item map: %s", assignee.ToDoItemID)
		}

		if assignee.UserID != nil {
			todoItem.UserIDs = append(todoItem.UserIDs, *assignee.UserID)
		}

		if assignee.TeamID != nil {
			todoItem.TeamIDs = append(todoItem.TeamIDs, *assignee.TeamID)
		}
	}

	return nil
}

func (t *todoItems) getSelectFields() []interface{} {
	selectFields := []interface{}{}

	for _, field := range todoItemsFieldList {
		selectFields = append(selectFields, fmt.Sprintf("todo_items.%s", field))
	}

	return selectFields
}

func scanToDoItem(row scanner) (*models.ToDoItem, error) {
	todoItem := &models.ToDoItem{}

	// Use the target type to fan out to the correct target id.
	var (
		pipelineTargetID *string
	)

	fields := []interface{}{
		&todoItem.Metadata.ID,
		&todoItem.Metadata.CreationTimestamp,
		&todoItem.Metadata.LastUpdatedTimestamp,
		&todoItem.Metadata.Version,
		&todoItem.OrganizationID,
		&todoItem.ProjectID,
		&todoItem.AutoResolved,
		&todoItem.Resolvable,
		&todoItem.TargetType,
		&todoItem.Payload,
		&pipelineTargetID,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	todoItem.Metadata.PRN = models.ToDoItemResource.BuildPRN(gid.ToGlobalID(gid.ToDoItemType, todoItem.Metadata.ID))

	switch todoItem.TargetType {
	case models.PipelineApprovalTarget,
		models.TaskApprovalTarget:
		todoItem.TargetID = *pipelineTargetID
	default:
		return nil, errors.New("invalid todo item target type %s", todoItem.TargetType)
	}

	return todoItem, nil
}

func scanToDoItemAssignee(row scanner) (*todoItemAssignees, error) {
	assignees := &todoItemAssignees{}

	if err := row.Scan(
		&assignees.ToDoItemID,
		&assignees.UserID,
		&assignees.TeamID,
	); err != nil {
		return nil, err
	}

	return assignees, nil
}

func scanToDoItemResolvedByUser(row scanner) (*todoItemResolvedByUser, error) {
	resolvedBy := &todoItemResolvedByUser{}

	if err := row.Scan(
		&resolvedBy.ToDoItemID,
		&resolvedBy.UserID,
	); err != nil {
		return nil, err
	}

	return resolvedBy, nil
}
