package db

//go:generate go tool mockery --name Events --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/metric"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	eventTablePipelines      = "pipelines"
	eventTableLogStreams     = "log_streams"
	eventTableAgents         = "agents"
	eventTableJobs           = "jobs"
	eventTableAgentSessions  = "agent_sessions"
	eventTableActivityEvents = "activity_events"
	eventTableComments       = "comments"
	eventTableToDoItems      = "todo_items"
)

// Events provides the ability to listen for async events from the database
type Events interface {
	// Listen for async events from the database
	Listen(ctx context.Context) (<-chan Event, <-chan error)
}

// Event contains processed information related to the database row that was changed
// The ID field is needed for triage independent of the type of the event data.
type Event struct {
	Table  string          `json:"table"`
	Action string          `json:"action"`
	ID     string          `json:"id"`
	Data   json.RawMessage `json:"data"`
}

// PipelineEventData contains the event response data for a row from the pipelines table.
type PipelineEventData struct {
	ProjectID string `json:"project_id"`
}

// LogStreamEventData contains the event response data for a row from the log_streams table.
type LogStreamEventData struct {
	Size      int  `json:"size"`
	Completed bool `json:"completed"`
}

// JobEventTaskData is the data for a job of type task.  See models.JobTaskData.
type JobEventTaskData struct {
	PipelineID string `json:"pipelineId"`
}

// JobEventData contains the event response data for a row from the jobs table.
type JobEventData struct {
	AgentID   *string          `json:"agent_id"`
	ID        string           `json:"id"`
	Type      models.JobType   `json:"type"`
	ProjectID string           `json:"project_id"`
	Data      JobEventTaskData `json:"data"`
}

// AgentSessionEventData contains the event response data for a row from the agent_sessions table.
type AgentSessionEventData struct {
	ID      string `json:"id"`
	AgentID string `json:"agent_id"`
}

// CommentEventData contains the event response data for a row from the comments table.
type CommentEventData struct {
	ID       string `json:"id"`
	ThreadID string `json:"thread_id"`
}

type events struct {
	dbClient *Client
}

var dbEventCount = metric.NewCounter("db_event_count", "Amount of database events.")

// NewEvents returns an instance of the Events interface
func NewEvents(dbClient *Client) Events {
	return &events{dbClient: dbClient}
}

func (e *events) Listen(ctx context.Context) (<-chan Event, <-chan error) {
	ch := make(chan Event)
	fatalErrors := make(chan error, 1)
	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer close(ch)
		defer close(fatalErrors)
		// Don't try to do 'defer wg.Done()' here, because it will try to make it negative.

		conn, err := e.dbClient.conn.Acquire(ctx)
		if err != nil {
			e.dbClient.logger.Errorf("failed to acquire db connection in db events module: %v", err)
			fatalErrors <- errors.Wrap(err, "failed to acquire db connection from pool")
			wg.Done()
			return
		}
		defer conn.Release()

		_, err = conn.Exec(ctx, "listen events")
		if err != nil {
			e.dbClient.logger.Errorf("failed to start listening for db events: %v", err)
			fatalErrors <- errors.Wrap(err, "error when listening on events channel")
			wg.Done()
			return
		}

		// Let Listen return to its caller, now that listening is fully active.
		wg.Done()

		for {
			notification, err := conn.Conn().WaitForNotification(ctx)

			// Check if context has been cancelled
			if ctx.Err() != nil {
				return
			}

			if err != nil {
				e.dbClient.logger.Errorf("received error when listening for db event: %v", err)
				fatalErrors <- errors.Wrap(err, "error waiting for db notification")
				return
			}

			var event Event
			if err := json.Unmarshal([]byte(notification.Payload), &event); err != nil {
				e.dbClient.logger.Errorf("failed to unmarshal db event %v", err)
				continue
			}

			dbEventCount.Inc()
			select {
			case ch <- event:
			case <-ctx.Done():
				return
			}
		}
	}()

	wg.Wait()
	return ch, fatalErrors
}

/////////////////////////////////////////////////////////////////////////////////
// Shorthand methods to convert an event to specific event data references.

// ToPipelineEventData is a shorthand method to return type-checked event data.
func (e *Event) ToPipelineEventData() (*PipelineEventData, error) {
	if e.Table != eventTablePipelines {
		return nil, fmt.Errorf("invalid event table, expected 'pipelines': %s", e.Table)
	}

	d := PipelineEventData{}
	if err := json.Unmarshal(e.Data, &d); err != nil {
		return nil, fmt.Errorf("failed to unmarshal db pipelines event data, %v", err)
	}

	return &d, nil
}

// ToLogStreamEventData is a shorthand method to return type-checked event data.
func (e *Event) ToLogStreamEventData() (*LogStreamEventData, error) {
	if e.Table != eventTableLogStreams {
		return nil, fmt.Errorf("invalid event table, expected 'log_streams': %s", e.Table)
	}

	d := LogStreamEventData{}
	if err := json.Unmarshal(e.Data, &d); err != nil {
		return nil, fmt.Errorf("failed to unmarshal db log_streams event data, %v", err)
	}

	return &d, nil
}

// ToJobEventData is a shorthand method to return type-checked event data.
func (e *Event) ToJobEventData() (*JobEventData, error) {
	if e.Table != eventTableJobs {
		return nil, fmt.Errorf("invalid event table, expected 'jobs': %s", e.Table)
	}

	d := JobEventData{}
	if err := json.Unmarshal(e.Data, &d); err != nil {
		return nil, fmt.Errorf("failed to unmarshal db jobs event data, %v", err)
	}

	return &d, nil
}

// ToAgentSessionEventData is a shorthand method to return type-checked event data.
func (e *Event) ToAgentSessionEventData() (*AgentSessionEventData, error) {
	if e.Table != eventTableAgentSessions {
		return nil, fmt.Errorf("invalid event table, expected 'agent_sessions': %s", e.Table)
	}

	d := AgentSessionEventData{}
	if err := json.Unmarshal(e.Data, &d); err != nil {
		return nil, fmt.Errorf("failed to unmarshal db agent_sessions event data, %v", err)
	}

	return &d, nil
}

// ToCommentEventData is a shorthand method to return type-checked event data.
func (e *Event) ToCommentEventData() (*CommentEventData, error) {
	if e.Table != eventTableComments {
		return nil, fmt.Errorf("invalid event table, expected 'comments': %s", e.Table)
	}

	d := CommentEventData{}
	if err := json.Unmarshal(e.Data, &d); err != nil {
		return nil, fmt.Errorf("failed to unmarshal db comments event data, %v", err)
	}

	return &d, nil
}
