package db

//go:generate go tool mockery --name EnvironmentRules --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/trace"
)

// EnvironmentRuleSortableField represents the fields that an organization can be sorted by
type EnvironmentRuleSortableField string

// EnvironmentRuleSortableField constants
const (
	EnvironmentRuleSortableFieldCreatedAtAsc        EnvironmentRuleSortableField = "CREATED_AT_ASC"
	EnvironmentRuleSortableFieldCreatedAtDesc       EnvironmentRuleSortableField = "CREATED_AT_DESC"
	EnvironmentRuleSortableFieldUpdatedAtAsc        EnvironmentRuleSortableField = "UPDATED_AT_ASC"
	EnvironmentRuleSortableFieldUpdatedAtDesc       EnvironmentRuleSortableField = "UPDATED_AT_DESC"
	EnvironmentRuleSortableFieldEnvironmentNameAsc  EnvironmentRuleSortableField = "ENVIRONMENT_NAME_ASC"
	EnvironmentRuleSortableFieldEnvironmentNameDesc EnvironmentRuleSortableField = "ENVIRONMENT_NAME_DESC"
)

func (os EnvironmentRuleSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case EnvironmentRuleSortableFieldCreatedAtAsc, EnvironmentRuleSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "environment_rules", Col: "created_at"}
	case EnvironmentRuleSortableFieldUpdatedAtAsc, EnvironmentRuleSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "environment_rules", Col: "updated_at"}
	case EnvironmentRuleSortableFieldEnvironmentNameAsc, EnvironmentRuleSortableFieldEnvironmentNameDesc:
		return &pagination.FieldDescriptor{Key: "environment_name", Table: "environment_rules", Col: "environment_name"}
	default:
		return nil
	}
}

func (os EnvironmentRuleSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// EnvironmentRuleFilter contains the supported fields for filtering EnvironmentRule resources
type EnvironmentRuleFilter struct {
	OrgID                 *string
	ProjectID             *string
	EnvironmentName       *string
	EnvironmentRuleScopes []models.ScopeType
	EnvironmentRuleIDs    []string
}

// GetEnvironmentRulesInput is the input for listing organizations
type GetEnvironmentRulesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *EnvironmentRuleSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *EnvironmentRuleFilter
}

// EnvironmentRulesResult contains the response data and page information
type EnvironmentRulesResult struct {
	PageInfo         *pagination.PageInfo
	EnvironmentRules []*models.EnvironmentRule
}

// EnvironmentRules encapsulates the logic to access state version reqs from the database
type EnvironmentRules interface {
	GetEnvironmentRuleByPRN(ctx context.Context, prn string) (*models.EnvironmentRule, error)
	GetEnvironmentRuleByID(ctx context.Context, id string) (*models.EnvironmentRule, error)
	CreateEnvironmentRule(ctx context.Context, req *models.EnvironmentRule) (*models.EnvironmentRule, error)
	UpdateEnvironmentRule(ctx context.Context, rule *models.EnvironmentRule) (*models.EnvironmentRule, error)
	GetEnvironmentRules(ctx context.Context, input *GetEnvironmentRulesInput) (*EnvironmentRulesResult, error)
	DeleteEnvironmentRule(ctx context.Context, rule *models.EnvironmentRule) error
}

type environmentRuleEligibleDeployer struct {
	UserID           *string
	TeamID           *string
	ServiceAccountID *string
	RoleID           *string
	RuleID           string
}

type environmentRules struct {
	dbClient *Client
}

var environmentRuleFieldList = append(metadataFieldList,
	"created_by", "scope", "organization_id", "project_id", "environment_name")

// NewEnvironmentRules returns an instance of the EnvironmentRules interface
func NewEnvironmentRules(dbClient *Client) EnvironmentRules {
	return &environmentRules{dbClient: dbClient}
}

func (r *environmentRules) GetEnvironmentRuleByPRN(ctx context.Context, prn string) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "db.GetEnvironmentRuleByPRN")
	defer span.End()

	path, err := models.EnvironmentRuleResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.Ex{}
	parts := strings.Split(path, "/")

	switch len(parts) {
	case 2:
		// This is an organization rule.
		ex["organizations.name"] = parts[0]
		ex["environment_rules.id"] = gid.FromGlobalID(parts[1])
	case 3:
		// This is a project rule.
		ex["organizations.name"] = parts[0]
		ex["projects.name"] = parts[1]
		ex["environment_rules.id"] = gid.FromGlobalID(parts[2])
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return r.getEnvironmentRule(ctx, ex)
}

func (r *environmentRules) GetEnvironmentRuleByID(ctx context.Context, id string) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "db.GetEnvironmentRuleByID")
	defer span.End()

	return r.getEnvironmentRule(ctx, goqu.Ex{
		"environment_rules.id": id,
	})
}

// CreateEnvironmentRule creates a new environment protection rule
func (r *environmentRules) CreateEnvironmentRule(ctx context.Context,
	req *models.EnvironmentRule) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "db.CreateEnvironmentRule")
	defer span.End()

	timestamp := currentTime()

	tx, err := r.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			r.dbClient.logger.Errorf("failed to rollback tx: %v", txErr)
		}
	}()

	sql, args, err := dialect.From("environment_rules").
		Prepared(true).
		With("environment_rules",
			dialect.Insert("environment_rules").Rows(goqu.Record{
				"id":               newResourceID(),
				"version":          initialResourceVersion,
				"created_at":       timestamp,
				"updated_at":       timestamp,
				"created_by":       req.CreatedBy,
				"scope":            req.Scope,
				"organization_id":  req.OrgID,
				"project_id":       req.ProjectID,
				"environment_name": req.EnvironmentName,
			}).Returning("*"),
		).Select(r.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"organizations.id": goqu.I("environment_rules.organization_id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("environment_rules.project_id")))).
		ToSQL()

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdRule, err := scanEnvironmentRule(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "index_environment_rules_on_organization_id_environment_name":
					return nil, errors.New("only one rule per environment allowed",
						errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
				case "index_environment_rules_on_project_id_environment_name":
					return nil, errors.New("only one rule per environment allowed",
						errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
				default:
					return nil, errors.New("environment protection rule already exists",
						errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
				}
			}
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_organization_id":
					return nil, errors.Wrap(err, "invalid organization ID",
						errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
				case "fk_project_id":
					return nil, errors.Wrap(err, "invalid project ID",
						errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
				}
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	// Insert the lists of eligible deployers.
	if err := r.insertEligibleDeployerLists(ctx, span, tx, req, createdRule); err != nil {
		return nil, err
	}

	if err := r.hydrateRules(ctx, tx, []*models.EnvironmentRule{createdRule}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate rules", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit tx", errors.WithSpan(span))
	}

	return createdRule, nil
}

// UpdateEnvironmentRule updates an existing environment rule
// It deletes all pre-existing eligible deployers and then updates to the supplied lists of eligible deployers.
func (r *environmentRules) UpdateEnvironmentRule(ctx context.Context, rule *models.EnvironmentRule) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateEnvironmentRule")
	defer span.End()

	timestamp := currentTime()

	tx, err := r.dbClient.getConnection(ctx).Begin(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	// Rollback is safe to call even if the tx is already closed, so if
	// the tx commits successfully, this is a no-op
	defer func() {
		if txErr := tx.Rollback(ctx); txErr != nil && txErr != pgx.ErrTxClosed {
			r.dbClient.logger.Errorf("failed to rollback tx: %v", txErr)
		}
	}()

	sql, args, err := dialect.From("environment_rules").
		Prepared(true).
		With("environment_rules",
			dialect.Update("environment_rules").
				Set(goqu.Record{
					"version":    goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at": timestamp,
					// Don't update anything else in this table.
				}).
				Where(goqu.Ex{"id": rule.Metadata.ID, "version": rule.Metadata.Version}).
				Returning("*"),
		).Select(r.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"environment_rules.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("environment_rules.project_id")))).
		ToSQL()

	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedRule, err := scanEnvironmentRule(tx.QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		r.dbClient.logger.Error(err)
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	deleteEligibleDeployersSQL, args, err := dialect.Delete("environment_rule_eligible_deployers").
		Prepared(true).
		Where(
			goqu.Ex{
				"environment_rule_id": rule.Metadata.ID,
			},
		).ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err = tx.Exec(ctx, deleteEligibleDeployersSQL, args...); err != nil {
		return nil, errors.Wrap(err, "failed to execute DB query", errors.WithSpan(span))
	}

	// Insert the lists of eligible deployers.
	if err := r.insertEligibleDeployerLists(ctx, span, tx, rule, updatedRule); err != nil {
		return nil, err
	}

	if err := r.hydrateRules(ctx, tx, []*models.EnvironmentRule{updatedRule}); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate rules", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to commit tx", errors.WithSpan(span))
	}

	return updatedRule, nil
}

// insertEligibleDeployerLists inserts the lists of eligible deployers.
// It is called from CreateEnvironmentRule and UpdateEnvironmentRule
func (r *environmentRules) insertEligibleDeployerLists(
	ctx context.Context,
	span trace.Span,
	tx pgx.Tx,
	input *models.EnvironmentRule,
	newRule *models.EnvironmentRule,
) error {

	for _, userID := range input.UserIDs {
		userIDCopy := userID
		if err := r.insertEligibleDeployer(ctx, tx, &environmentRuleEligibleDeployer{
			RuleID: newRule.Metadata.ID,
			UserID: &userIDCopy,
		}); err != nil {
			return errors.Wrap(err, "failed to insert eligible user deployer",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	for _, serviceAccountID := range input.ServiceAccountIDs {
		serviceAccountIDCopy := serviceAccountID
		if err := r.insertEligibleDeployer(ctx, tx, &environmentRuleEligibleDeployer{
			RuleID:           newRule.Metadata.ID,
			ServiceAccountID: &serviceAccountIDCopy,
		}); err != nil {
			return errors.Wrap(err, "failed to insert eligible service account deployer",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	for _, teamID := range input.TeamIDs {
		teamIDCopy := teamID
		if err := r.insertEligibleDeployer(ctx, tx, &environmentRuleEligibleDeployer{
			RuleID: newRule.Metadata.ID,
			TeamID: &teamIDCopy,
		}); err != nil {
			return errors.Wrap(err, "failed to insert eligible team deployer",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	for _, roleID := range input.RoleIDs {
		roleIDCopy := roleID
		if err := r.insertEligibleDeployer(ctx, tx, &environmentRuleEligibleDeployer{
			RuleID: newRule.Metadata.ID,
			RoleID: &roleIDCopy,
		}); err != nil {
			return errors.Wrap(err, "failed to insert eligible role deployer",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	return nil
}

func (r *environmentRules) DeleteEnvironmentRule(ctx context.Context, rule *models.EnvironmentRule) error {
	ctx, span := tracer.Start(ctx, "db.DeleteEnvironmentRule")
	defer span.End()

	sql, args, err := dialect.From("environment_rules").
		Prepared(true).
		With("environment_rules",
			dialect.Delete("environment_rules").
				Where(goqu.Ex{"id": rule.Metadata.ID, "version": rule.Metadata.Version}).
				Returning("*"),
		).Select(r.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"environment_rules.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("environment_rules.project_id")))).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	_, err = scanEnvironmentRule(r.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}
		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (r *environmentRules) GetEnvironmentRules(ctx context.Context, input *GetEnvironmentRulesInput) (*EnvironmentRulesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetEnvironmentRules")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.OrgID != nil {
			ex = ex.Append(goqu.I("environment_rules.organization_id").Eq(*input.Filter.OrgID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(
				// Use an OR condition to get both organization and project environment protection rules.
				goqu.Or(
					goqu.I("environment_rules.project_id").Eq(*input.Filter.ProjectID),
					goqu.And(
						goqu.I("environment_rules.organization_id").In(
							dialect.From("projects").
								Select("org_id").
								Where(goqu.I("id").Eq(*input.Filter.ProjectID)),
						),
						goqu.I("environment_rules.scope").Eq(models.OrganizationScope),
					),
				),
			)
		}

		if input.Filter.EnvironmentName != nil {
			ex = ex.Append(goqu.I("environment_rules.environment_name").Eq(*input.Filter.EnvironmentName))
		}

		if len(input.Filter.EnvironmentRuleScopes) > 0 {
			ex = ex.Append(goqu.I("environment_rules.scope").In(input.Filter.EnvironmentRuleScopes))
		}

		if input.Filter.EnvironmentRuleIDs != nil {
			ex = ex.Append(goqu.I("environment_rules.id").In(input.Filter.EnvironmentRuleIDs))
		}
	}

	query := dialect.From(goqu.T("environment_rules")).Select(r.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"organizations.id": goqu.I("environment_rules.organization_id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("environment_rules.project_id")))).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "environment_rules", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, r.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.EnvironmentRule{}
	for rows.Next() {
		item, err := scanEnvironmentRule(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	if err := r.hydrateRules(ctx, r.dbClient.getConnection(ctx), results); err != nil {
		return nil, errors.Wrap(err, "failed to hydrate rules", errors.WithSpan(span))
	}

	result := &EnvironmentRulesResult{
		PageInfo:         rows.GetPageInfo(),
		EnvironmentRules: results,
	}

	return result, nil
}

func (r *environmentRules) hydrateRules(ctx context.Context, conn connection, rules []*models.EnvironmentRule) error {
	if len(rules) == 0 {
		return nil
	}

	ruleIDs := []string{}
	for _, rule := range rules {
		ruleIDs = append(ruleIDs, rule.Metadata.ID)
	}

	sql, args, err := dialect.From("environment_rule_eligible_deployers").
		Prepared(true).
		Select("environment_rule_id", "user_id", "service_account_id", "team_id", "role_id").
		Where(goqu.I("environment_rule_id").In(ruleIDs)).ToSQL()
	if err != nil {
		return err
	}

	rows, err := conn.Query(ctx, sql, args...)
	if err != nil {
		return err
	}

	defer rows.Close()

	ruleMap := map[string]*models.EnvironmentRule{}
	for _, rule := range rules {
		rule.UserIDs = []string{}
		rule.ServiceAccountIDs = []string{}
		rule.TeamIDs = []string{}
		rule.RoleIDs = []string{}

		ruleMap[rule.Metadata.ID] = rule
	}

	// Scan rows
	for rows.Next() {
		deployer, err := scanEnvironmentRuleEligibleDeployer(rows)
		if err != nil {
			return err
		}

		rule, ok := ruleMap[deployer.RuleID]
		if !ok {
			return fmt.Errorf("rule not found in rule map: %s", deployer.RuleID)
		}

		if deployer.UserID != nil {
			rule.UserIDs = append(rule.UserIDs, *deployer.UserID)
		}

		if deployer.ServiceAccountID != nil {
			rule.ServiceAccountIDs = append(rule.ServiceAccountIDs, *deployer.ServiceAccountID)
		}

		if deployer.TeamID != nil {
			rule.TeamIDs = append(rule.TeamIDs, *deployer.TeamID)
		}

		if deployer.RoleID != nil {
			rule.RoleIDs = append(rule.RoleIDs, *deployer.RoleID)
		}
	}

	return nil
}

func (r *environmentRules) insertEligibleDeployer(ctx context.Context, con connection, deployer *environmentRuleEligibleDeployer) error {
	sql, args, err := dialect.Insert("environment_rule_eligible_deployers").
		Prepared(true).
		Rows(goqu.Record{
			"id":                  newResourceID(),
			"environment_rule_id": deployer.RuleID,
			"user_id":             deployer.UserID,
			"service_account_id":  deployer.ServiceAccountID,
			"team_id":             deployer.TeamID,
			"role_id":             deployer.RoleID,
		}).ToSQL()
	if err != nil {
		return err
	}

	_, err = con.Exec(ctx, sql, args...)
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				return errors.New("invalid deployer ID", errors.WithErrorCode(errors.EInvalid))
			}
		}
		return err
	}

	return nil
}

func (r *environmentRules) getEnvironmentRule(ctx context.Context, exp exp.Expression) (*models.EnvironmentRule, error) {
	query := dialect.From(goqu.T("environment_rules")).
		Prepared(true).
		Select(r.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"organizations.id": goqu.I("environment_rules.organization_id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("projects.id").Eq(goqu.I("environment_rules.project_id")))).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	rule, err := scanEnvironmentRule(r.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	if err := r.hydrateRules(ctx, r.dbClient.getConnection(ctx), []*models.EnvironmentRule{rule}); err != nil {
		return nil, err
	}

	return rule, nil
}

func (*environmentRules) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range environmentRuleFieldList {
		selectFields = append(selectFields, fmt.Sprintf("environment_rules.%s", field))
	}

	selectFields = append(selectFields, "organizations.name", "projects.name")

	return selectFields
}

func scanEnvironmentRule(row scanner) (*models.EnvironmentRule, error) {
	var organizationName string
	var projectName *string

	environmentRule := &models.EnvironmentRule{}

	err := row.Scan(
		&environmentRule.Metadata.ID,
		&environmentRule.Metadata.CreationTimestamp,
		&environmentRule.Metadata.LastUpdatedTimestamp,
		&environmentRule.Metadata.Version,
		&environmentRule.CreatedBy,
		&environmentRule.Scope,
		&environmentRule.OrgID,
		&environmentRule.ProjectID,
		&environmentRule.EnvironmentName,
		&organizationName,
		&projectName,
	)
	if err != nil {
		return nil, err
	}

	globalID := gid.ToGlobalID(gid.EnvironmentRuleType, environmentRule.Metadata.ID)

	switch environmentRule.Scope {
	case models.OrganizationScope:
		environmentRule.Metadata.PRN = models.EnvironmentRuleResource.BuildPRN(organizationName, globalID)
	case models.ProjectScope:
		if projectName == nil {
			return nil, errors.New("unexpected nil project name")
		}
		environmentRule.Metadata.PRN = models.EnvironmentRuleResource.BuildPRN(organizationName, *projectName, globalID)
	default:
		return nil, errors.New("unexpected environment protection rule scope %s", environmentRule.Scope)
	}

	return environmentRule, nil
}

func scanEnvironmentRuleEligibleDeployer(row scanner) (*environmentRuleEligibleDeployer, error) {

	deployer := &environmentRuleEligibleDeployer{}

	err := row.Scan(
		&deployer.RuleID,
		&deployer.UserID,
		&deployer.ServiceAccountID,
		&deployer.TeamID,
		&deployer.RoleID,
	)
	if err != nil {
		return nil, err
	}

	return deployer, nil
}
