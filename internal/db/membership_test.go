//go:build integration

package db

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

type holderIDs2Name struct {
	userIDs2Name           map[string]string
	teamIDs2Name           map[string]string
	serviceAccountIDs2Name map[string]string
}

type membershipWarmupsInput struct {
	teams           []models.Team
	teamMembers     []models.TeamMember
	projects        []models.Project
	organizations   []models.Organization
	memberships     []models.Membership
	roles           []models.Role
	users           []models.User
	serviceAccounts []models.ServiceAccount
}

type membershipWarmupsOutput struct {
	holderIDs2Name  holderIDs2Name
	teams           []models.Team
	teamMembers     []models.TeamMember
	organizations   []models.Organization
	projects        []models.Project
	memberships     []models.Membership
	users           []models.User
	roles           []models.Role
	serviceAccounts []models.ServiceAccount
}

// membershipInfo aids convenience in accessing the information TestGetMemberships
// needs about the warmup memberships.
type membershipInfo struct {
	updateTime   time.Time
	holder       string
	role         string
	name         string // project or organization name or just "global".
	membershipID string
}

// membershipInfoTimeSlice makes a slice of membershipInfo sortable by last updated time
type membershipInfoTimeSlice []membershipInfo

// membershipInfoNameSlice makes a slice of membershipInfo sortable by organization name
type membershipInfoNameSlice []membershipInfo

func TestGetMembershipByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOutput, err := createWarmupMemberships(ctx, testClient, membershipWarmupsInput{
		teams:           standardWarmupTeamsForMemberships,
		teamMembers:     standardWarmupTeamMembersForMemberships,
		users:           standardWarmupUsersForMemberships,
		organizations:   standardWarmupOrganizationsForMemberships,
		projects:        standardWarmupProjectsForMemberships,
		memberships:     standardWarmupMemberships,
		roles:           standardWarmupRolesForMemberships,
		serviceAccounts: standardWarmupServiceAccountsForMemberships,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		searchID         string
		expectMembership bool
	}

	/*
		test case template

		{
			name             string
			searchID         string
			expectErrorCode  errors.CodeType
			expectMembership bool
		}
	*/

	testCases := []testCase{}
	for _, positiveMembership := range createdWarmupOutput.memberships {
		testCases = append(testCases, testCase{
			name:             positiveMembership.Metadata.ID,
			searchID:         positiveMembership.Metadata.ID,
			expectMembership: true,
		})
	}

	testCases = append(testCases,
		testCase{
			name:     "negative, does not exist",
			searchID: nonExistentID,
		},
		testCase{
			name:            "negative, invalid",
			searchID:        invalidID,
			expectErrorCode: errors.EInvalid,
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			membership, err := testClient.client.Memberships.GetMembershipByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectMembership {
				// the positive case
				require.NotNil(t, membership)
				assert.Equal(t, test.name, membership.Metadata.ID)
			} else {
				// the negative and defective cases
				assert.Nil(t, membership)
			}
		})
	}
}

func TestGetMembershipByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOutput, err := createWarmupMemberships(ctx, testClient, membershipWarmupsInput{
		teams:           standardWarmupTeamsForMemberships,
		teamMembers:     standardWarmupTeamMembersForMemberships,
		users:           standardWarmupUsersForMemberships,
		organizations:   standardWarmupOrganizationsForMemberships,
		projects:        standardWarmupProjectsForMemberships,
		memberships:     standardWarmupMemberships,
		roles:           standardWarmupRolesForMemberships,
		serviceAccounts: standardWarmupServiceAccountsForMemberships,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		searchPRN        string
		expectMembership bool
	}

	/*
		test case template

		{
			name             string
			searchPRN        string
			expectErrorCode  errors.CodeType
			expectMembership bool
		}
	*/

	testCases := []testCase{}

	for ix := range createdWarmupOutput.memberships {
		testCases = append(testCases, testCase{
			name:             createdWarmupOutput.memberships[ix].Metadata.ID,
			searchPRN:        createdWarmupOutput.memberships[ix].Metadata.PRN,
			expectMembership: true,
		})
	}

	testCases = append(testCases,
		testCase{
			name:      "negative, does not exist",
			searchPRN: models.MembershipResource.BuildPRN(nonExistentGlobalID),
		},
		testCase{
			name:            "negative, invalid",
			searchPRN:       "invalid-prn",
			expectErrorCode: errors.EInvalid,
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			membership, err := testClient.client.Memberships.GetMembershipByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectMembership {
				// the positive case
				require.NotNil(t, membership)
				assert.Equal(t, test.name, membership.Metadata.ID)
			} else {
				// the negative and defective cases
				assert.Nil(t, membership)
			}
		})
	}
}

func TestGetMemberships(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOutput, err := createWarmupMemberships(ctx, testClient, membershipWarmupsInput{
		teams:           standardWarmupTeamsForMemberships,
		teamMembers:     standardWarmupTeamMembersForMemberships,
		users:           standardWarmupUsersForMemberships,
		organizations:   standardWarmupOrganizationsForMemberships,
		projects:        standardWarmupProjectsForMemberships,
		memberships:     standardWarmupMemberships,
		roles:           standardWarmupRolesForMemberships,
		serviceAccounts: standardWarmupServiceAccountsForMemberships,
	})
	require.Nil(t, err)

	allMembershipInfos := membershipInfoFromMemberships(
		createdWarmupOutput.holderIDs2Name,
		createdWarmupOutput.organizations,
		createdWarmupOutput.projects,
		createdWarmupOutput.memberships,
	)

	sort.Sort(membershipInfoNameSlice(allMembershipInfos))
	allTrails := trailsFromMembershipInfo(allMembershipInfos, false)

	// Sort by last update times.
	sort.Sort(membershipInfoTimeSlice(allMembershipInfos))
	allTrailsByUpdateTime := trailsFromMembershipInfo(allMembershipInfos, false)
	reverseTrailsByUpdateTime := reverseStringSlice(allTrailsByUpdateTime)

	dummyCursorFunc := func(cp pagination.CursorPaginatable) (*string, error) { return ptr.String("dummy-cursor-value"), nil }

	type testCase struct {
		expectStartCursorError      error
		expectEndCursorError        error
		input                       *GetMembershipsInput
		expectErrorCode             errors.CodeType
		name                        string
		expectPageInfo              pagination.PageInfo
		expectTrails                []string
		getBeforeCursorFromPrevious bool
		sortedDescending            bool
		expectHasStartCursor        bool
		getAfterCursorFromPrevious  bool
		expectHasEndCursor          bool
	}

	/*
		template test case:

		{
			name: "",
			input: &GetMembershipsInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			},
			sortedDescending             bool
			getBeforeCursorFromPrevious: false,
			getAfterCursorFromPrevious:  false,
			expectErrorCode:             nil,
			expectTrails:                []string{},
			expectPageInfo: pagination.PageInfo{
				Cursor:          nil,
				TotalCount:      0,
				HasNextPage:     false,
				HasPreviousPage: false,
			},
			expectStartCursorError: nil,
			expectHasStartCursor:   false,
			expectEndCursorError:   nil,
			expectHasEndCursor:     false,
		}
	*/

	testCases := []testCase{

		{
			name:                 "non-nil but mostly empty input",
			input:                &GetMembershipsInput{},
			expectTrails:         allTrails,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allTrails)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "populated sort and pagination, nil filter",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			expectTrails:         allTrailsByUpdateTime,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allTrailsByUpdateTime)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "sort in ascending order of time of last update",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
			},
			expectTrails:         allTrailsByUpdateTime,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allTrailsByUpdateTime)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "sort in descending order of time of last update",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtDesc),
			},
			sortedDescending:     true,
			expectTrails:         reverseTrailsByUpdateTime,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(reverseTrailsByUpdateTime)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "pagination: everything at once",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
			},
			expectTrails:         allTrailsByUpdateTime,
			expectPageInfo:       pagination.PageInfo{TotalCount: int32(len(allTrailsByUpdateTime)), Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "pagination: first one",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			},
			expectTrails: allTrailsByUpdateTime[0:1],
			expectPageInfo: pagination.PageInfo{
				TotalCount:  int32(len(allTrailsByUpdateTime)),
				Cursor:      dummyCursorFunc,
				HasNextPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "pagination: first seven",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(7),
				},
			},
			expectTrails: allTrailsByUpdateTime[0:7],
			expectPageInfo: pagination.PageInfo{
				TotalCount:  int32(len(allTrailsByUpdateTime)),
				Cursor:      dummyCursorFunc,
				HasNextPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		// When Last is supplied, the sort order is intended to be reversed.
		{
			name: "pagination: last two",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					Last: ptr.Int32(2),
				},
			},
			sortedDescending: true,
			expectTrails:     reverseTrailsByUpdateTime[0:2],
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(reverseTrailsByUpdateTime)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     false,
				HasPreviousPage: true,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "pagination, before and after, expect error",
			input: &GetMembershipsInput{
				Sort:              ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{},
			},
			getAfterCursorFromPrevious:  true,
			getBeforeCursorFromPrevious: true,
			expectErrorCode:             errors.EInternal,
			expectTrails:                []string{},
			expectPageInfo:              pagination.PageInfo{},
		},
		{
			name: "pagination, first one and last two, expect error",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
					Last:  ptr.Int32(2),
				},
			},
			expectErrorCode: errors.EInternal,
			expectTrails:    []string{},
			expectPageInfo: pagination.PageInfo{
				TotalCount:      int32(len(allTrailsByUpdateTime)),
				Cursor:          dummyCursorFunc,
				HasNextPage:     true,
				HasPreviousPage: false,
			},
			expectHasStartCursor: true,
		},

		{
			name: "fully-populated types, nothing allowed through filters",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
				// Don't pass in Organization or Project ID since they can't be used together.
				Filter: &MembershipFilter{
					UserID:           ptr.String(""),
					TeamID:           ptr.String(""),
					RoleID:           ptr.String(""),
					MembershipIDs:    []string{},
					MembershipScopes: []models.ScopeType{},
				},
			},
			expectErrorCode: errors.EInternal,
			expectTrails:    []string{},
			expectPageInfo:  pagination.PageInfo{},
		},
		{
			name: "filter, both organization and project ID, negative",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
					ProjectID:      &createdWarmupOutput.projects[0].Metadata.ID,
				},
			},
			expectErrorCode: errors.EInvalid,
			expectTrails:    []string{},
			expectPageInfo:  pagination.PageInfo{},
		},
		{
			name: "filter, role ID, positive",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					RoleID: &createdWarmupOutput.roles[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.roles[0].Metadata.ID),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.roles[0].Metadata.ID))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, role ID, non-existent",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					RoleID: ptr.String(nonExistentID),
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},

		{
			name: "filter, role ID, invalid",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					RoleID: ptr.String(invalidID),
				},
			},
			expectErrorCode: errors.EInternal,
			expectTrails:    []string{},
			expectPageInfo:  pagination.PageInfo{},
		},
		{
			name: "filter, user member ID, positive",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID: &createdWarmupOutput.users[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.users[0].Username),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.users[0].Username))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, user member ID, exists, no memberships",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID: ptr.String(findUserIDFromName(createdWarmupOutput.users, "user-99")),
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, user member ID, non-existent",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID: ptr.String(nonExistentID),
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, user member ID, invalid",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID: ptr.String(invalidID),
				},
			},
			expectErrorCode: errors.EInternal,
			expectTrails:    []string{},
			expectPageInfo:  pagination.PageInfo{},
		},
		{
			name: "filter, team ID, positive",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID: &createdWarmupOutput.teams[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.teams[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.teams[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, team ID, exists, no memberships",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID: ptr.String(findTeamIDFromName(createdWarmupOutput.teams, "team-99")),
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, team ID, non-existent",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID: ptr.String(nonExistentID),
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, team ID, invalid",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID: ptr.String(invalidID),
				},
			},
			expectErrorCode: errors.EInternal,
			expectTrails:    []string{},
			expectPageInfo:  pagination.PageInfo{},
		},
		{
			name: "filter, service account ID, positive",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.serviceAccounts[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.serviceAccounts[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, service account exists but no memberships",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: &createdWarmupOutput.serviceAccounts[1].Metadata.ID,
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, membership types, organization membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					MembershipScopes: []models.ScopeType{models.OrganizationScope},
				},
			},
			expectTrails: findMatchingTrails(allTrails, "organization"),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, "organization"))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, membership types, project membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					MembershipScopes: []models.ScopeType{models.ProjectScope},
				},
			},
			expectTrails: findMatchingTrails(allTrails, "project"),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, "project"))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, membership types, global membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					MembershipScopes: []models.ScopeType{models.GlobalScope},
				},
			},
			expectTrails: findMatchingTrails(allTrails, "global"),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, "global"))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, membership types, all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					MembershipScopes: []models.ScopeType{
						models.GlobalScope,
						models.ProjectScope,
						models.OrganizationScope,
					},
				},
			},
			expectTrails: allTrailsByUpdateTime,
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(allTrailsByUpdateTime)),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, organization ID, positive without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				},
			},
			// We expect only organization memberships
			expectTrails: findMatchingTrails(allTrails, createdWarmupOutput.organizations[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, createdWarmupOutput.organizations[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, organization ID, positive with organization membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{models.OrganizationScope},
				},
			},
			expectTrails: findMatchingTrails(allTrails, createdWarmupOutput.organizations[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, createdWarmupOutput.organizations[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, organization ID, positive with project membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{models.ProjectScope},
				},
			},
			// We expect only project memberships within the organization.
			expectTrails: findMatchingTrails(allTrails, createdWarmupOutput.projects[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, createdWarmupOutput.projects[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, organization ID, positive with organization and project membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.ProjectScope,
					},
				},
			},
			// We expect only organization and project memberships within the organization.
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.organizations[0].Name, createdWarmupOutput.projects[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(
					findMatchingTrails(
						allTrailsByUpdateTime,
						createdWarmupOutput.organizations[0].Name,
						createdWarmupOutput.projects[0].Name,
					),
				)),
				Cursor: dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, organization ID, positive with organization and global membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
					},
				},
			},
			// We expect only organization and global memberships.
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.organizations[0].Name, "global"),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.organizations[0].Name, "global"))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, organization ID, positive with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
						models.ProjectScope,
					},
				},
			},
			expectTrails: allTrailsByUpdateTime,
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(allTrailsByUpdateTime)),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, organization ID, exists but no memberships",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: ptr.String(findOrganizationIDFromName(createdWarmupOutput.organizations, "organization-99")),
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, organization ID, non-existent",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: ptr.String(nonExistentID),
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, organization ID, invalid",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					OrganizationID: ptr.String(invalidID),
				},
			},
			expectErrorCode: errors.EInternal,
			expectTrails:    []string{},
			expectPageInfo:  pagination.PageInfo{},
		},
		{
			name: "filter, project ID, positive without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				},
			},
			// We expect only project memberships
			expectTrails: findMatchingTrails(allTrails, createdWarmupOutput.projects[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, createdWarmupOutput.projects[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, project ID, positive with project membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ProjectID:        &createdWarmupOutput.projects[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{models.ProjectScope},
				},
			},
			expectTrails: findMatchingTrails(allTrails, createdWarmupOutput.projects[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrails, createdWarmupOutput.projects[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, project ID, positive with project and organization membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.ProjectScope,
						models.OrganizationScope,
					},
				},
			},
			// We expect only project and organization memberships within the project.
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.projects[0].Name, createdWarmupOutput.organizations[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.projects[0].Name, createdWarmupOutput.organizations[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, project ID, positive with project and global membership type",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.ProjectScope,
						models.GlobalScope,
					},
				},
			},
			// We expect only project and global memberships.
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.projects[0].Name, "global"),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.projects[0].Name, "global"))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, project ID, positive with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.ProjectScope,
						models.GlobalScope,
						models.OrganizationScope,
					},
				},
			},
			expectTrails: allTrailsByUpdateTime,
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(allTrailsByUpdateTime)),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, project ID, exists but no memberships",
			input: &GetMembershipsInput{
				Sort:   ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{ProjectID: ptr.String(findProjectIDFromName(createdWarmupOutput.projects, "project-99"))},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, project ID, non-existent",
			input: &GetMembershipsInput{
				Sort:   ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{ProjectID: ptr.String(nonExistentID)},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, project ID, invalid",
			input: &GetMembershipsInput{
				Sort:   ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{ProjectID: ptr.String(invalidID)},
			},
			expectErrorCode: errors.EInternal,
			expectTrails:    []string{},
			expectPageInfo:  pagination.PageInfo{},
		},
		{
			name: "filter, membership IDs, positive",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					MembershipIDs: []string{createdWarmupOutput.memberships[1].Metadata.ID},
				},
			},
			expectTrails:         []string{allTrailsByUpdateTime[1]},
			expectPageInfo:       pagination.PageInfo{TotalCount: 1, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, membership IDs, non-existent",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					MembershipIDs: []string{nonExistentID},
				},
			},
			expectTrails:         []string{},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},

		{
			name: "filter, membership IDs, invalid",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					MembershipIDs: []string{invalidID},
				},
			},
			expectErrorCode:      errors.EInternal,
			expectTrails:         []string{},
			expectPageInfo:       pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination UserID and OrganizationID without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID:         &createdWarmupOutput.users[0].Metadata.ID,
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				},
			},
			// We expect only organization memberships to be returned.
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, fmt.Sprintf("%s--%s--",
				createdWarmupOutput.organizations[0].Name, createdWarmupOutput.users[0].Username)),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, fmt.Sprintf("%s--%s--",
					createdWarmupOutput.organizations[0].Name, createdWarmupOutput.users[0].Username)))),
				Cursor: dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination UserID and OrganizationID, with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID:         &createdWarmupOutput.users[0].Metadata.ID,
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
						models.ProjectScope,
					},
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.users[0].Username),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.users[0].Username))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination UserID and OrganizationID, negative",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID:         ptr.String(findUserIDFromName(createdWarmupOutput.users, "user-99")),
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, combination UserID and ProjectID without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID:    &createdWarmupOutput.users[0].Metadata.ID,
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime,
				fmt.Sprintf("%s--%s--",
					createdWarmupOutput.projects[0].Name,
					createdWarmupOutput.users[0].Username,
				),
			),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime,
					fmt.Sprintf("%s--%s--",
						createdWarmupOutput.projects[0].Name,
						createdWarmupOutput.users[0].Username,
					),
				))),
				Cursor: dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination UserID and ProjectID, with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					UserID:    &createdWarmupOutput.users[0].Metadata.ID,
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
						models.ProjectScope,
					},
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.users[0].Username),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.users[0].Username))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination TeamID and OrganizationID without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID:         &createdWarmupOutput.teams[0].Metadata.ID,
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				},
			},
			// We expect only organization memberships to be returned.
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.organizations[0].Name+"--"+createdWarmupOutput.teams[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.organizations[0].Name+"--"+createdWarmupOutput.teams[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination TeamID and OrganizationID, with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID:         &createdWarmupOutput.teams[0].Metadata.ID,
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
						models.ProjectScope,
					},
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.teams[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.teams[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination TeamID and OrganizationID, negative",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID:         ptr.String(findTeamIDFromName(createdWarmupOutput.teams, "team-99")),
					OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, combination TeamID and ProjectID without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID:    &createdWarmupOutput.teams[0].Metadata.ID,
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime,
				fmt.Sprintf("%s--%s--",
					createdWarmupOutput.projects[0].Name,
					createdWarmupOutput.teams[0].Name,
				),
			),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime,
					fmt.Sprintf("%s--%s--",
						createdWarmupOutput.projects[0].Name,
						createdWarmupOutput.teams[0].Name,
					),
				))),
				Cursor: dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination TeamID and ProjectID, with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID:    &createdWarmupOutput.teams[0].Metadata.ID,
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
						models.ProjectScope,
					},
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.teams[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.teams[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination TeamID and ProjectID, negative",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					TeamID:    ptr.String(findTeamIDFromName(createdWarmupOutput.teams, "team-99")),
					ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, combination service account ID and OrganizationID without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
					OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, fmt.Sprintf("%s--%s--",
				createdWarmupOutput.organizations[0].Name, createdWarmupOutput.serviceAccounts[0].Name)),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, fmt.Sprintf("%s--%s--",
					createdWarmupOutput.organizations[0].Name, createdWarmupOutput.serviceAccounts[0].Name)))),
				Cursor: dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination service account ID and OrganizationID, with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
					OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
						models.ProjectScope,
					},
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.serviceAccounts[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.serviceAccounts[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination service account ID and OrganizationID, negative",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: ptr.String(findServiceAccountIDFromName(createdWarmupOutput.serviceAccounts, "service-account-99")),
					OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
		{
			name: "filter, combination service account ID and ProjectID without membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
					ProjectID:        &createdWarmupOutput.projects[0].Metadata.ID,
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime,
				fmt.Sprintf("%s--%s--",
					createdWarmupOutput.projects[0].Name,
					createdWarmupOutput.serviceAccounts[0].Name,
				),
			),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime,
					fmt.Sprintf("%s--%s--",
						createdWarmupOutput.projects[0].Name,
						createdWarmupOutput.serviceAccounts[0].Name,
					),
				))),
				Cursor: dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination service account ID and ProjectID, with all membership types",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
					ProjectID:        &createdWarmupOutput.projects[0].Metadata.ID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
						models.ProjectScope,
					},
				},
			},
			expectTrails: findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.serviceAccounts[0].Name),
			expectPageInfo: pagination.PageInfo{
				TotalCount: int32(len(findMatchingTrails(allTrailsByUpdateTime, createdWarmupOutput.serviceAccounts[0].Name))),
				Cursor:     dummyCursorFunc,
			},
			expectHasStartCursor: true,
			expectHasEndCursor:   true,
		},
		{
			name: "filter, combination service account ID and ProjectID, negative",
			input: &GetMembershipsInput{
				Sort: ptrMembershipSortableField(MembershipSortableFieldUpdatedAtAsc),
				Filter: &MembershipFilter{
					ServiceAccountID: ptr.String(findServiceAccountIDFromName(createdWarmupOutput.serviceAccounts, "service-account-99")),
					ProjectID:        &createdWarmupOutput.projects[0].Metadata.ID,
				},
			},
			expectTrails:   []string{},
			expectPageInfo: pagination.PageInfo{TotalCount: 0, Cursor: dummyCursorFunc},
		},
	}

	var (
		previousEndCursorValue   *string
		previousStartCursorValue *string
	)
	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// For some pagination tests, a previous case's cursor value gets piped into the next case.
			if test.getAfterCursorFromPrevious || test.getBeforeCursorFromPrevious {

				// Make sure there's a place to put it.
				require.NotNil(t, test.input.PaginationOptions)

				if test.getAfterCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousEndCursorValue)
					test.input.PaginationOptions.After = previousEndCursorValue
				}

				if test.getBeforeCursorFromPrevious {
					// Make sure there's a previous value to use.
					require.NotNil(t, previousStartCursorValue)
					test.input.PaginationOptions.Before = previousStartCursorValue
				}

				// Clear the values so they won't be used twice.
				previousEndCursorValue = nil
				previousStartCursorValue = nil
			}

			// GetMemberships(ctx context.Context, input *GetMembershipsInput) (*MembershipResult, error)
			membershipsResult, err := testClient.client.Memberships.GetMemberships(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			// If there was no error, check the results.
			if err == nil {

				// Never returns nil if error is nil.
				require.NotNil(t, membershipsResult.PageInfo)
				assert.NotNil(t, membershipsResult.Memberships)
				pageInfo := membershipsResult.PageInfo
				memberships := membershipsResult.Memberships

				// Check the memberships result by comparing a list of the trails.
				infos := membershipInfoFromMemberships(
					createdWarmupOutput.holderIDs2Name,
					createdWarmupOutput.organizations,
					createdWarmupOutput.projects,
					memberships,
				)
				resultTrails := trailsFromMembershipInfo(infos, test.sortedDescending)

				// If no sort direction was specified, sort the results here for repeatability.
				if test.input.Sort == nil {
					sort.Strings(resultTrails)
				}

				assert.Equal(t, len(test.expectTrails), len(resultTrails))
				assert.Equal(t, test.expectTrails, resultTrails)

				assert.Equal(t, test.expectPageInfo.HasNextPage, pageInfo.HasNextPage)
				assert.Equal(t, test.expectPageInfo.HasPreviousPage, pageInfo.HasPreviousPage)
				assert.Equal(t, test.expectPageInfo.TotalCount, pageInfo.TotalCount)
				assert.Equal(t, test.expectPageInfo.Cursor != nil, pageInfo.Cursor != nil)

				// Compare the cursor function results only if there is at least one membership returned.
				// If there are no memberships returned, there is no argument to pass to the cursor function.
				// Also, don't try to reverse engineer to compare the cursor string values.
				if len(memberships) > 0 {
					resultStartCursor, resultStartCursorError := pageInfo.Cursor(&memberships[0])
					resultEndCursor, resultEndCursorError := pageInfo.Cursor(&memberships[len(memberships)-1])
					assert.Equal(t, test.expectStartCursorError, resultStartCursorError)
					assert.Equal(t, test.expectHasStartCursor, resultStartCursor != nil)
					assert.Equal(t, test.expectEndCursorError, resultEndCursorError)
					assert.Equal(t, test.expectHasEndCursor, resultEndCursor != nil)

					// Capture the ending cursor values for the next case.
					previousEndCursorValue = resultEndCursor
					previousStartCursorValue = resultStartCursor
				}
			}
		})
	}
}

func TestCreateMembership(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOutput, err := createWarmupMemberships(ctx, testClient, membershipWarmupsInput{
		users:           standardWarmupUsersForMemberships,
		organizations:   standardWarmupOrganizationsForMemberships,
		roles:           standardWarmupRolesForMemberships,
		teams:           standardWarmupTeamsForMemberships,
		teamMembers:     standardWarmupTeamMembersForMemberships,
		projects:        standardWarmupProjectsForMemberships,
		serviceAccounts: standardWarmupServiceAccountsForMemberships,
	})
	require.Nil(t, err)

	type testCase struct {
		input           *models.Membership
		expectErrorCode errors.CodeType
		expectCreated   *models.Membership
		name            string
	}

	now := currentTime()
	/*
		test case template

		{
		name            string
		input           *models.Membership
		expectErrorCode errors.CodeType
		expectCreated   *models.Membership
		}
	*/

	testCases := []testCase{
		{
			name: "positive, user, organization membership",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				UserID:         &createdWarmupOutput.users[1].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				UserID:         &createdWarmupOutput.users[1].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
		},

		{
			name: "positive, team, organization membership",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				TeamID:         &createdWarmupOutput.teams[1].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				TeamID:         &createdWarmupOutput.teams[1].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
		},

		{
			name: "positive, service account, organization membership",
			input: &models.Membership{
				OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.OrganizationScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				Scope:            models.OrganizationScope,
			},
		},

		{
			name: "positive, user, project membership",
			input: &models.Membership{
				ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				UserID:    &createdWarmupOutput.users[0].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				Scope:     models.ProjectScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				UserID:    &createdWarmupOutput.users[0].Metadata.ID,
				Scope:     models.ProjectScope,
			},
		},

		{
			name: "positive, team, project membership",
			input: &models.Membership{
				ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				TeamID:    &createdWarmupOutput.teams[1].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				Scope:     models.ProjectScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				TeamID:    &createdWarmupOutput.teams[1].Metadata.ID,
				Scope:     models.ProjectScope,
			},
		},

		{
			name: "positive, service account, project membership",
			input: &models.Membership{
				ProjectID:        &createdWarmupOutput.projects[0].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.ProjectScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				ProjectID:        &createdWarmupOutput.projects[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				Scope:            models.ProjectScope,
			},
		},

		{
			name: "positive, user, global membership",
			input: &models.Membership{
				UserID: &createdWarmupOutput.users[0].Metadata.ID,
				RoleID: createdWarmupOutput.roles[0].Metadata.ID,
				Scope:  models.GlobalScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				RoleID: createdWarmupOutput.roles[0].Metadata.ID,
				UserID: &createdWarmupOutput.users[0].Metadata.ID,
				Scope:  models.GlobalScope,
			},
		},

		{
			name: "positive, team, global membership",
			input: &models.Membership{
				TeamID: &createdWarmupOutput.teams[0].Metadata.ID,
				RoleID: createdWarmupOutput.roles[0].Metadata.ID,
				Scope:  models.GlobalScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				RoleID: createdWarmupOutput.roles[0].Metadata.ID,
				TeamID: &createdWarmupOutput.teams[0].Metadata.ID,
				Scope:  models.GlobalScope,
			},
		},

		{
			name: "positive, service account, global membership",
			input: &models.Membership{
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.GlobalScope,
			},
			expectCreated: &models.Membership{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
				},
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				Scope:            models.GlobalScope,
			},
		},

		{
			name: "negative, duplicate organization membership, user",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				UserID:         &createdWarmupOutput.users[1].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "negative, duplicate organization membership, team",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				TeamID:         &createdWarmupOutput.teams[1].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "negative, duplicate organization membership, service account",
			input: &models.Membership{
				OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.OrganizationScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "negative, duplicate project membership, user",
			input: &models.Membership{
				ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				UserID:    &createdWarmupOutput.users[0].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				Scope:     models.ProjectScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "negative, duplicate project membership, team",
			input: &models.Membership{
				ProjectID: &createdWarmupOutput.projects[0].Metadata.ID,
				TeamID:    &createdWarmupOutput.teams[1].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				Scope:     models.ProjectScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "negative, duplicate project membership, service account",
			input: &models.Membership{
				ProjectID:        &createdWarmupOutput.projects[0].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.ProjectScope,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "negative, non-existent user",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				UserID:         ptr.String(nonExistentID),
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "negative, non-existent team",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				TeamID:         ptr.String(nonExistentID),
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "negative, non-existent service account",
			input: &models.Membership{
				OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				ServiceAccountID: ptr.String(nonExistentID),
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.OrganizationScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "negative, invalid user",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				UserID:         ptr.String(invalidID),
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "negative, invalid team",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				TeamID:         ptr.String(invalidID),
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "negative, invalid service account",
			input: &models.Membership{
				OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				ServiceAccountID: ptr.String(invalidID),
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.OrganizationScope,
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "negative, non-existent role",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[1].Metadata.ID,
				UserID:         &createdWarmupOutput.users[2].Metadata.ID,
				RoleID:         nonExistentID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "negative, invalid role",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				UserID:         &createdWarmupOutput.users[1].Metadata.ID,
				RoleID:         invalidID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "negative, non-existent organization",
			input: &models.Membership{
				OrganizationID: ptr.String(nonExistentID),
				UserID:         &createdWarmupOutput.users[1].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "negative, invalid organization",
			input: &models.Membership{
				OrganizationID: ptr.String(invalidID),
				UserID:         &createdWarmupOutput.users[1].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "negative, non-existent project",
			input: &models.Membership{
				ProjectID: ptr.String(nonExistentID),
				UserID:    &createdWarmupOutput.users[1].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				Scope:     models.ProjectScope,
			},
			expectErrorCode: errors.ENotFound,
		},

		{
			name: "negative, invalid project",
			input: &models.Membership{
				ProjectID: ptr.String(invalidID),
				UserID:    &createdWarmupOutput.users[1].Metadata.ID,
				RoleID:    createdWarmupOutput.roles[0].Metadata.ID,
				Scope:     models.ProjectScope,
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "negative, creating a membership with user, team and service account",
			input: &models.Membership{
				OrganizationID:   &createdWarmupOutput.organizations[0].Metadata.ID,
				UserID:           &createdWarmupOutput.users[1].Metadata.ID,
				TeamID:           &createdWarmupOutput.teams[1].Metadata.ID,
				ServiceAccountID: &createdWarmupOutput.serviceAccounts[0].Metadata.ID,
				RoleID:           createdWarmupOutput.roles[0].Metadata.ID,
				Scope:            models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},

		{
			name: "negative, creating a membership with no user or team or service account",
			input: &models.Membership{
				OrganizationID: &createdWarmupOutput.organizations[0].Metadata.ID,
				RoleID:         createdWarmupOutput.roles[0].Metadata.ID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualCreated, err := testClient.client.Memberships.CreateMembership(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectCreated != nil {
				// the positive case
				require.NotNil(t, actualCreated)
				// The creation process must set the creation and last updated timestamps
				// between when the test case was created and when it the result is checked.
				whenCreated := test.expectCreated.Metadata.CreationTimestamp
				now := currentTime()

				compareMemberships(t, test.expectCreated, actualCreated, false, timeBounds{
					createLow:  whenCreated,
					createHigh: &now,
					updateLow:  whenCreated,
					updateHigh: &now,
				})
			} else {
				// the negative and defective cases
				assert.Nil(t, actualCreated)
			}
		})
	}
}

func TestUpdateMembership(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOutput, err := createWarmupMemberships(ctx, testClient, membershipWarmupsInput{
		users:           standardWarmupUsersForMemberships,
		organizations:   standardWarmupOrganizationsForMemberships,
		memberships:     standardWarmupMemberships,
		roles:           standardWarmupRolesForMemberships,
		teams:           standardWarmupTeamsForMemberships,
		teamMembers:     standardWarmupTeamMembersForMemberships,
		projects:        standardWarmupProjectsForMemberships,
		serviceAccounts: standardWarmupServiceAccountsForMemberships,
	})
	require.Nil(t, err)

	allMembershipInfos := membershipInfoFromMemberships(
		createdWarmupOutput.holderIDs2Name,
		createdWarmupOutput.organizations,
		createdWarmupOutput.projects,
		createdWarmupOutput.memberships,
	)

	type testCase struct {
		input           *models.Membership
		expectErrorCode errors.CodeType
		expectUpdated   *models.Membership
		name            string
	}

	/*
		test case template

		{
			name            string
			input           *models.Membership
			expectErrorCode errors.CodeType
			expectUpdated   *models.Membership
		}
	*/

	testCases := []testCase{}

	for ix, preUpdate := range createdWarmupOutput.memberships {
		now := currentTime()
		testCases = append(testCases, testCase{
			name: "positive-" + buildTrail(allMembershipInfos[ix]),
			input: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID:      preUpdate.Metadata.ID,
					Version: preUpdate.Metadata.Version,
				},
				RoleID:           rotateRole(preUpdate.RoleID, createdWarmupOutput.roles),
				OrganizationID:   preUpdate.OrganizationID,
				ProjectID:        preUpdate.ProjectID,
				UserID:           preUpdate.UserID,
				TeamID:           preUpdate.TeamID,
				ServiceAccountID: preUpdate.ServiceAccountID,
				Scope:            preUpdate.Scope,
			},
			expectUpdated: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID:                   preUpdate.Metadata.ID,
					Version:              preUpdate.Metadata.Version + 1,
					CreationTimestamp:    preUpdate.Metadata.CreationTimestamp,
					LastUpdatedTimestamp: &now,
				},
				RoleID:           rotateRole(preUpdate.RoleID, createdWarmupOutput.roles),
				OrganizationID:   preUpdate.OrganizationID,
				ProjectID:        preUpdate.ProjectID,
				UserID:           preUpdate.UserID,
				TeamID:           preUpdate.TeamID,
				ServiceAccountID: preUpdate.ServiceAccountID,
				Scope:            preUpdate.Scope,
			},
		})
	}

	testCases = append(testCases, testCase{
		name: "negative, non-exist",
		input: &models.Membership{
			Metadata: models.ResourceMetadata{
				ID:      nonExistentID,
				Version: 1,
			},
			RoleID: createdWarmupOutput.roles[0].Metadata.ID,
		},
		expectErrorCode: errors.EOptimisticLock,
	},
		testCase{
			name: "negative, invalid",
			input: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: 1,
				},
				RoleID: createdWarmupOutput.roles[0].Metadata.ID,
			},
			expectErrorCode: errors.EInternal,
		})

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualUpdated, err := testClient.client.Memberships.UpdateMembership(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectUpdated != nil {
				// the positive case
				require.NotNil(t, actualUpdated)
				// The creation process must set the creation and last updated timestamps
				// between when the test case was created and when it the result is checked.
				whenCreated := test.expectUpdated.Metadata.CreationTimestamp
				now := currentTime()

				compareMemberships(t, test.expectUpdated, actualUpdated, false, timeBounds{
					createLow:  whenCreated,
					createHigh: &now,
					updateLow:  whenCreated,
					updateHigh: &now,
				})
			} else {
				// the negative and defective cases
				assert.Nil(t, actualUpdated)
			}
		})
	}
}

func TestDeleteMembership(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	createdWarmupOutput, err := createWarmupMemberships(ctx, testClient, membershipWarmupsInput{
		users:           standardWarmupUsersForMemberships,
		organizations:   standardWarmupOrganizationsForMemberships,
		memberships:     standardWarmupMemberships,
		roles:           standardWarmupRolesForMemberships,
		teams:           standardWarmupTeamsForMemberships,
		teamMembers:     standardWarmupTeamMembersForMemberships,
		projects:        standardWarmupProjectsForMemberships,
		serviceAccounts: standardWarmupServiceAccountsForMemberships,
	})
	require.Nil(t, err)

	allMembershipInfos := membershipInfoFromMemberships(
		createdWarmupOutput.holderIDs2Name,
		createdWarmupOutput.organizations,
		createdWarmupOutput.projects,
		createdWarmupOutput.memberships,
	)

	type testCase struct {
		input            *models.Membership
		expectErrorCode  errors.CodeType
		name             string
		expectMembership bool
	}

	/*
		test case template

		{
			name             string
			input            *models.Membership
			expectErrorCode  errors.CodeType
			expectMembership bool
		}
	*/

	testCases := []testCase{}
	for ix, toDelete := range createdWarmupOutput.memberships {
		testCases = append(testCases, testCase{
			name: "positive-" + buildTrail(allMembershipInfos[ix]),
			input: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID:      toDelete.Metadata.ID,
					Version: toDelete.Metadata.Version,
				},
			},
		})
	}

	testCases = append(testCases,
		testCase{
			name: "negative, non-exist",
			input: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: nonExistentID,
				},
			},
			expectErrorCode: errors.EOptimisticLock,
		},
		testCase{
			name: "negative, defective-id",
			input: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: invalidID,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
	)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.Memberships.DeleteMembership(ctx, test.input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
		})
	}
}

// Common utility functions.

// reverseMap returns a map that does the inverse mapping of the input.
// Because names and IDs are unique within a given domain, collisions are not possible.
func reverseMap(input map[string]string) map[string]string {
	result := make(map[string]string)
	for name, id := range input {
		result[id] = name
	}
	return result
}

func ptrMembershipSortableField(arg MembershipSortableField) *MembershipSortableField {
	return &arg
}

func (oss membershipInfoNameSlice) Len() int {
	return len(oss)
}

func (oss membershipInfoNameSlice) Swap(i, j int) {
	oss[i], oss[j] = oss[j], oss[i]
}

func (oss membershipInfoNameSlice) Less(i, j int) bool {
	return oss[i].name < oss[j].name
}

func (oss membershipInfoTimeSlice) Len() int {
	return len(oss)
}

func (oss membershipInfoTimeSlice) Swap(i, j int) {
	oss[i], oss[j] = oss[j], oss[i]
}

func (oss membershipInfoTimeSlice) Less(i, j int) bool {
	return oss[i].updateTime.Before(oss[j].updateTime)
}

// Standard warmup teams for tests in this module:
var standardWarmupTeamsForMemberships = []models.Team{
	{
		Name:        "team-1",
		Description: "team 1 for membership tests",
	},
	{
		Name:        "team-99",
		Description: "team 99 for membership tests",
	},
}

var standardWarmupTeamMembersForMemberships = []models.TeamMember{
	{
		UserID: "user-team-1",
		TeamID: "team-1",
	},
}

// Standard warmup users for tests in this module:
// Please note: all users are _NON_-admin.
var standardWarmupUsersForMemberships = []models.User{
	{
		Username: "user-1",
		Email:    "user-1@example.com",
	},
	{
		Username: "user-team-1",
		Email:    "user-team-1@example.com",
	},
	{
		Username: "user-99",
		Email:    "user-99@example.com",
	},
}

// Standard warmup organizations for tests in this module:
// These organizations are in a linear chain of descent.
// The create function will derive the parent name and name from the organization name.
var standardWarmupOrganizationsForMemberships = []models.Organization{
	{
		Description: "organization 1 for testing membership functions",
		Name:        "organization-1",
		CreatedBy:   "someone-1",
	},
	{
		Description: "organization 99 for testing membership functions",
		Name:        "organization-99",
		CreatedBy:   "someone-99",
	},
}

// Standard warmup projects for tests in this module:
var standardWarmupProjectsForMemberships = []models.Project{
	{
		Name:        "project-1",
		Description: "project 2 for membership tests",
		OrgID:       "organization-1",
	},
	{
		Name:        "project-99",
		Description: "project 99 for membership tests",
		OrgID:       "organization-99",
	},
}

// Standard warmup roles for tests in this module:
var standardWarmupRolesForMemberships = []models.Role{
	{
		Name:        "role-1",
		Description: "role 1 for membership tests",
		CreatedBy:   "someone-1",
	},
	{
		Name:        "role-99",
		Description: "role 99 for membership tests",
		CreatedBy:   "someone-99",
	},
}

// Standard warmup service accounts for tests in this module:
var standardWarmupServiceAccountsForMemberships = []models.ServiceAccount{
	{
		Name:           "service-account-1",
		Description:    "service account 1 for membership tests",
		OrganizationID: ptr.String("organization-1"),
		Scope:          models.OrganizationScope, // Only org service accounts can be members.
		CreatedBy:      "someone-1",
	},
	{
		Name:           "service-account-99",
		Description:    "service account 99 for membership tests",
		OrganizationID: ptr.String("organization-99"),
		Scope:          models.OrganizationScope,
		CreatedBy:      "someone-99",
	},
}

// Standard warmup memberships for tests in this module:
var standardWarmupMemberships = []models.Membership{
	// User memberships:
	{
		UserID:         ptr.String("user-1"),
		RoleID:         "role-1",
		OrganizationID: ptr.String("organization-1"),
		Scope:          models.OrganizationScope,
	},
	{
		UserID:    ptr.String("user-1"),
		RoleID:    "role-1",
		ProjectID: ptr.String("project-1"),
		Scope:     models.ProjectScope,
	},
	{
		UserID: ptr.String("user-1"),
		RoleID: "role-1",
		Scope:  models.GlobalScope,
	},
	// Team memberships:
	{
		TeamID:         ptr.String("team-1"),
		RoleID:         "role-1",
		OrganizationID: ptr.String("organization-1"),
		Scope:          models.OrganizationScope,
	},
	{
		TeamID:    ptr.String("team-1"),
		RoleID:    "role-1",
		ProjectID: ptr.String("project-1"),
		Scope:     models.ProjectScope,
	},
	{
		TeamID: ptr.String("team-1"),
		RoleID: "role-1",
		Scope:  models.GlobalScope,
	},
	// Service account memberships:
	{
		ServiceAccountID: ptr.String("service-account-1"),
		RoleID:           "role-1",
		OrganizationID:   ptr.String("organization-1"),
		Scope:            models.OrganizationScope,
	},
	{
		ServiceAccountID: ptr.String("service-account-1"),
		RoleID:           "role-1",
		ProjectID:        ptr.String("project-1"),
		Scope:            models.ProjectScope,
	},
	{
		ServiceAccountID: ptr.String("service-account-1"),
		RoleID:           "role-1",
		Scope:            models.GlobalScope,
	},
}

// createWarmupMemberships creates some objects for a test
// The objects to create can be standard or otherwise.
func createWarmupMemberships(
	ctx context.Context,
	testClient *testClient,
	input membershipWarmupsInput,
) (*membershipWarmupsOutput, error) {
	resultOrganizations, organizationName2ID, err := createInitialOrganizations(ctx, testClient, input.organizations)
	if err != nil {
		return nil, err
	}

	resultTeams, teamName2ID, err := createInitialTeams(ctx, testClient, input.teams)
	if err != nil {
		return nil, err
	}

	resultUsers, username2ID, err := createInitialUsers(ctx, testClient, input.users)
	if err != nil {
		return nil, err
	}

	resultTeamMembers, err := createInitialTeamMembers(ctx, testClient, teamName2ID, username2ID, input.teamMembers)
	if err != nil {
		return nil, err
	}

	resultServiceAccounts, saName2ID, err := createInitialServiceAccounts(ctx, testClient, organizationName2ID, input.serviceAccounts)
	if err != nil {
		return nil, err
	}

	resultProjects, projectName2ID, err := createInitialProjects(ctx, testClient, organizationName2ID, input.projects)
	if err != nil {
		return nil, err
	}

	resultRoles, roleName2ID, err := createInitialRoles(ctx, testClient, input.roles)
	if err != nil {
		return nil, err
	}

	resultMemberships, err := createInitialMemberships(ctx, testClient,
		username2ID, teamName2ID, saName2ID, organizationName2ID, projectName2ID, roleName2ID, input.memberships)
	if err != nil {
		return nil, err
	}

	return &membershipWarmupsOutput{
		teams:           resultTeams,
		teamMembers:     resultTeamMembers,
		users:           resultUsers,
		organizations:   resultOrganizations,
		projects:        resultProjects,
		memberships:     resultMemberships,
		roles:           resultRoles,
		serviceAccounts: resultServiceAccounts,
		holderIDs2Name: holderIDs2Name{
			userIDs2Name:           reverseMap(username2ID),
			teamIDs2Name:           reverseMap(teamName2ID),
			serviceAccountIDs2Name: reverseMap(saName2ID),
		},
	}, nil
}

// createInitialMemberships creates some warmup memberships.
func createInitialMemberships(
	ctx context.Context,
	testClient *testClient,
	userMap,
	teamMap,
	serviceAccountMap,
	orgMap,
	projectMap,
	rolesMap map[string]string,
	toCreate []models.Membership,
) ([]models.Membership, error) {
	result := []models.Membership{}

	for _, input := range toCreate {
		translated := &models.Membership{
			RoleID: rolesMap[input.RoleID],
			Scope:  input.Scope,
		}

		if input.OrganizationID != nil {
			translated.OrganizationID = ptr.String(orgMap[*input.OrganizationID])
		}

		if input.ProjectID != nil {
			translated.ProjectID = ptr.String(projectMap[*input.ProjectID])
		}

		if input.UserID != nil {
			translated.UserID = ptr.String(userMap[*input.UserID])
		}

		if input.TeamID != nil {
			translated.TeamID = ptr.String(teamMap[*input.TeamID])
		}

		if input.ServiceAccountID != nil {
			translated.ServiceAccountID = ptr.String(serviceAccountMap[*input.ServiceAccountID])
		}

		created, err := testClient.client.Memberships.CreateMembership(ctx, translated)
		if err != nil {
			return nil, err
		}

		result = append(result, *created)
	}

	return result, nil
}

// membershipInfoFromMemberships returns a slice of membershipInfo,
// not necessarily sorted in any order.
func membershipInfoFromMemberships(
	holderIDs2Name holderIDs2Name,
	organizations []models.Organization,
	projects []models.Project,
	memberships []models.Membership,
) []membershipInfo {
	result := []membershipInfo{}

	for _, membership := range memberships {

		var holder string
		switch {
		case membership.UserID != nil:
			holder = holderIDs2Name.userIDs2Name[*membership.UserID]
		case membership.TeamID != nil:
			holder = holderIDs2Name.teamIDs2Name[*membership.TeamID]
		case membership.ServiceAccountID != nil:
			holder = holderIDs2Name.serviceAccountIDs2Name[*membership.ServiceAccountID]
		}

		var targetNamespace string
		switch membership.Scope {
		case models.OrganizationScope:
			for _, org := range organizations {
				if org.Metadata.ID == *membership.OrganizationID {
					targetNamespace = org.Name
					break
				}
			}
		case models.ProjectScope:
			for _, project := range projects {
				if project.Metadata.ID == *membership.ProjectID {
					targetNamespace = project.Name
					break
				}
			}
		case models.GlobalScope:
			targetNamespace = "global"
		}

		result = append(result, membershipInfo{
			name:         targetNamespace,
			membershipID: membership.Metadata.ID,
			holder:       holder,
			role:         membership.RoleID,
			updateTime:   *membership.Metadata.LastUpdatedTimestamp,
		})
	}

	return result
}

/*
trailsFromMembershipInfo preserves order to a point but not beyond.

Results from GetMemberships are sorted by name but _NOT_ by holder or role.
In order to conveniently compare lists of memberships, it is necessary to sort the
membership trails within the same name.

If sortedDescending is true, the trails within a given name are sorted in descending order.
*/

func trailsFromMembershipInfo(
	membershipInfos []membershipInfo,
	sortedDescending bool,
) []string {
	result := []string{}

	sameName := []string{}
	thisName := ""
	for _, membershipInfo := range membershipInfos {
		thisTrail := buildTrail(membershipInfo)

		if (len(sameName) > 0) && (membershipInfo.name != thisName) {
			sort.Strings(sameName)
			if sortedDescending {
				sameName = reverseStringSlice(sameName)
			}
			result = append(result, sameName...)
			sameName = []string{}
		}

		sameName = append(sameName, thisTrail)
		thisName = membershipInfo.name
	}

	sort.Strings(sameName)
	if sortedDescending {
		sameName = reverseStringSlice(sameName)
	}
	result = append(result, sameName...)

	return result
}

// buildTrail constructs the trail for a membership
func buildTrail(input membershipInfo) string {
	return (input.name + "--" + input.holder + "--" + input.role)
}

// Compare two membership objects, including bounds for creation and updated times.
func compareMemberships(
	t *testing.T,
	expected,
	actual *models.Membership,
	checkID bool,
	times timeBounds,
) {
	if checkID {
		assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	}
	assert.Equal(t, expected.Metadata.Version, actual.Metadata.Version)

	// Not possible to always know the PRN beforehand since we need the MembershipID.
	assert.NotEmpty(t, actual.Metadata.PRN)

	// Compare timestamps.
	compareTime(t, times.createLow, times.createHigh, actual.Metadata.CreationTimestamp)
	compareTime(t, times.updateLow, times.updateHigh, actual.Metadata.LastUpdatedTimestamp)

	assert.Equal(t, expected.RoleID, actual.RoleID)
	assert.Equal(t, expected.Scope, actual.Scope)
	assert.Equal(t, expected.UserID, actual.UserID)
	assert.Equal(t, expected.TeamID, actual.TeamID)
	assert.Equal(t, expected.ServiceAccountID, actual.ServiceAccountID)
	assert.Equal(t, expected.OrganizationID, actual.OrganizationID)
	assert.Equal(t, expected.ProjectID, actual.ProjectID)
}

func findMatchingTrails(allTrails []string, s ...string) []string {
	result := []string{}

	for _, trail := range allTrails {
		for _, target := range s {
			if strings.Contains(trail, target) {
				result = append(result, trail)
				break
			}
		}
	}

	return result
}

// findUserIDFromName returns the matching user ID for the specified name.
func findUserIDFromName(users []models.User, m string) string {
	for _, user := range users {
		if user.Username == m {
			return user.Metadata.ID
		}
	}
	return ""
}

// findTeamIDFromName returns the matching team ID for the specified name.
func findTeamIDFromName(teams []models.Team, m string) string {
	for _, team := range teams {
		if team.Name == m {
			return team.Metadata.ID
		}
	}
	return ""
}

// findOrganizationIDFromName returns the matching organization ID for the specified name.
func findOrganizationIDFromName(organizations []models.Organization, m string) string {
	for _, organization := range organizations {
		if organization.Name == m {
			return organization.Metadata.ID
		}
	}
	return ""
}

// findProjectIDFromName returns the matching project ID for the specified name.
func findProjectIDFromName(projects []models.Project, m string) string {
	for _, project := range projects {
		if project.Name == m {
			return project.Metadata.ID
		}
	}
	return ""
}

// findServiceAccountIDFromName returns the matching service account ID for the specified name.
func findServiceAccountIDFromName(serviceAccounts []models.ServiceAccount, m string) string {
	for _, serviceAccount := range serviceAccounts {
		if serviceAccount.Name == m {
			return serviceAccount.Metadata.ID
		}
	}
	return ""
}

// rotateRole returns a different role from what was passed in.
// If other modules need this function, move it to dbclient_test.
func rotateRole(input string, roles []models.Role) string {
	switch {
	case input == roles[0].Metadata.ID:
		return roles[1].Metadata.ID
	}

	return roles[0].Metadata.ID
}
