package db

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

//go:generate go tool mockery --name ActivityEvents --inpackage --case underscore

// ActivityEvents encapsulates the logic for interfacing with the activity events database.
type ActivityEvents interface {
	GetActivityEventByID(ctx context.Context, id string) (*models.ActivityEvent, error)
	GetActivityEvents(ctx context.Context, input *GetActivityEventsInput) (*ActivityEventsResult, error)
	CreateActivityEvent(ctx context.Context, input *models.ActivityEvent) (*models.ActivityEvent, error)
}

// ActivityEventSortableField represents a sortable field for activity events.
type ActivityEventSortableField string

// ActivityEventSortableField values.
const (
	ActivityEventSortableFieldCreatedAtAsc  ActivityEventSortableField = "CREATED_AT_ASC"
	ActivityEventSortableFieldCreatedAtDesc ActivityEventSortableField = "CREATED_AT_DESC"
	ActivityEventSortableFieldActionAsc     ActivityEventSortableField = "ACTION_ASC"
	ActivityEventSortableFieldActionDesc    ActivityEventSortableField = "ACTION_DESC"
)

func (sf ActivityEventSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch sf {
	case ActivityEventSortableFieldCreatedAtAsc, ActivityEventSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "activity_events", Col: "created_at"}
	case ActivityEventSortableFieldActionAsc, ActivityEventSortableFieldActionDesc:
		return &pagination.FieldDescriptor{Key: "action", Table: "activity_events", Col: "action"}
	default:
		return nil
	}
}

func (sf ActivityEventSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(sf), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// ActivityEventMembershipRequirement represents a requirement for querying activity events for a user or service account.
type ActivityEventMembershipRequirement struct {
	UserID           *string
	ServiceAccountID *string
}

// ActivityEventFilter contains the supported fields for filtering activity event resources
type ActivityEventFilter struct {
	TimeRangeEnd          *time.Time
	UserID                *string
	ServiceAccountID      *string
	OrganizationID        *string
	ProjectID             *string
	ReleaseTargetID       *string
	TimeRangeStart        *time.Time
	MembershipRequirement *ActivityEventMembershipRequirement
	Actions               []models.ActivityEventAction
	TargetTypes           []models.ActivityEventTargetType
}

// GetActivityEventsInput is the input for listing activity events.
type GetActivityEventsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ActivityEventSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter contains the supported fields for filtering ActivityEvent resources
	Filter *ActivityEventFilter
}

// ActivityEventsResult contains the response data and page information
type ActivityEventsResult struct {
	PageInfo       *pagination.PageInfo
	ActivityEvents []models.ActivityEvent
}

type activityEvents struct {
	dbClient *Client
}

var activityEventFieldList = append(metadataFieldList,
	"user_id",
	"service_account_id",
	"organization_id",
	"project_id",
	"action",
	"target_type",
	"payload",
	"role_target_id",
	"organization_target_id",
	"project_target_id",
	"project_variable_set_target_id",
	"membership_target_id",
	"agent_target_id",
	"pipeline_template_target_id",
	"pipeline_target_id",
	"team_target_id",
	"environment_target_id",
	"service_account_target_id",
	"approval_rule_target_id",
	"lifecycle_template_target_id",
	"release_lifecycle_target_id",
	"release_target_id",
	"plugin_target_id",
	"plugin_version_target_id",
	"comment_target_id",
	"vcs_provider_target_id",
	"thread_target_id",
	"environment_rule_target_id",
)

// NewActivityEvents creates a new activity events database client.
func NewActivityEvents(dbClient *Client) ActivityEvents {
	return &activityEvents{
		dbClient: dbClient,
	}
}

func (a *activityEvents) GetActivityEventByID(ctx context.Context, id string) (*models.ActivityEvent, error) {
	ctx, span := tracer.Start(ctx, "db.GetActivityEventByID")
	defer span.End()

	query := dialect.From("activity_events").
		Select(a.getSelectFields()...).
		Where(goqu.I("activity_events.id").Eq(id))

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	activityEvent, err := scanActivityEvent(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return activityEvent, nil
}

func (a *activityEvents) GetActivityEvents(ctx context.Context, input *GetActivityEventsInput) (*ActivityEventsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetActivityEvents")
	defer span.End()

	ex := goqu.And()

	selectEx := dialect.From("activity_events").
		LeftJoin(goqu.T("organizations"), goqu.On(goqu.I("activity_events.organization_id").Eq(goqu.I("organizations.id")))).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("activity_events.project_id").Eq(goqu.I("projects.id")))).
		Select(a.getSelectFields()...)

	if input.Filter != nil {
		if input.Filter.UserID != nil {
			ex = ex.Append(goqu.I("activity_events.user_id").Eq(*input.Filter.UserID))
		}
		if input.Filter.ServiceAccountID != nil {
			ex = ex.Append(goqu.I("activity_events.service_account_id").Eq(*input.Filter.ServiceAccountID))
		}
		if input.Filter.OrganizationID != nil {
			// Include both organization and project activity events.
			ex = ex.Append(goqu.Or(
				goqu.I("activity_events.organization_id").Eq(*input.Filter.OrganizationID),
				goqu.I("projects.org_id").Eq(input.Filter.OrganizationID),
			))
		}
		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("activity_events.project_id").Eq(*input.Filter.ProjectID))
		}
		if input.Filter.ReleaseTargetID != nil {
			// We only want threads, not comments for a release.
			selectEx = selectEx.LeftJoin(goqu.T("threads"), goqu.On(goqu.I("threads.id").Eq(goqu.I("activity_events.thread_target_id"))))
			selectEx = selectEx.LeftJoin(goqu.T("pipelines"), goqu.On(goqu.I("pipelines.id").Eq(goqu.I("activity_events.pipeline_target_id"))))

			ex = ex.Append(
				// Use an OR condition to get all release events including comments, release lifecycle pipelines, participants, etc.
				goqu.Or(
					goqu.I("activity_events.release_target_id").Eq(*input.Filter.ReleaseTargetID),
					goqu.I("threads.release_id").Eq(*input.Filter.ReleaseTargetID),
					goqu.And(
						goqu.I("pipelines.type").Eq(models.ReleaseLifecyclePipelineType),
						goqu.I("pipelines.release_id").Eq(*input.Filter.ReleaseTargetID),
					),
				),
			)
		}
		if input.Filter.TimeRangeStart != nil {
			// Must use UTC here otherwise, queries will return unexpected results.
			ex = ex.Append(goqu.I("activity_events.created_at").Gte(input.Filter.TimeRangeStart.UTC()))
		}
		if input.Filter.TimeRangeEnd != nil {
			// Must use UTC here otherwise, queries will return unexpected results.
			ex = ex.Append(goqu.I("activity_events.created_at").Lte(input.Filter.TimeRangeEnd.UTC()))
		}
		if len(input.Filter.Actions) > 0 {
			ex = ex.Append(goqu.I("activity_events.action").In(input.Filter.Actions))
		}
		if len(input.Filter.TargetTypes) > 0 {
			ex = ex.Append(goqu.I("activity_events.target_type").In(input.Filter.TargetTypes))
		}

		if input.Filter.MembershipRequirement != nil {
			ex = ex.Append(
				goqu.Or(
					organizationMembershipExpressionBuilder{
						userID:           input.Filter.MembershipRequirement.UserID,
						serviceAccountID: input.Filter.MembershipRequirement.ServiceAccountID,
					}.build(),
					projectMembershipExpressionBuilder{
						userID:           input.Filter.MembershipRequirement.UserID,
						serviceAccountID: input.Filter.MembershipRequirement.ServiceAccountID,
					}.build(),
				),
			)
		}
	}

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	query := selectEx.Where(ex)

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "activity_events", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, a.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.ActivityEvent{}
	for rows.Next() {
		item, err := scanActivityEvent(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan rows", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := ActivityEventsResult{
		PageInfo:       rows.GetPageInfo(),
		ActivityEvents: results,
	}

	return &result, nil
}

func (a *activityEvents) CreateActivityEvent(ctx context.Context, activityEvent *models.ActivityEvent) (*models.ActivityEvent, error) {
	ctx, span := tracer.Start(ctx, "db.CreateActivityEvent")
	defer span.End()

	// Use target type to fan out target ID to the various columns.
	var (
		roleTargetID               *string
		organizationTargetID       *string
		projectTargetID            *string
		projectVariableSetTargetID *string
		membershipTargetID         *string
		agentTargetID              *string
		pipelineTemplateTargetID   *string
		pipelineTargetID           *string
		teamTargetID               *string
		environmentTargetID        *string
		serviceAccountTargetID     *string
		approvalTargetID           *string
		lifecycleTemplateTargetID  *string
		releaseLifecycleTargetID   *string
		releaseTargetID            *string
		pluginTargetID             *string
		pluginVersionTargetID      *string
		commentTargetID            *string
		vcsProviderTargetID        *string
		threadTargetID             *string
		environmentRuleTargetID    *string
	)

	switch activityEvent.TargetType {
	case models.TargetRole:
		roleTargetID = activityEvent.TargetID
	case models.TargetOrganization:
		organizationTargetID = activityEvent.TargetID
	case models.TargetProject:
		projectTargetID = activityEvent.TargetID
	case models.TargetProjectVariableSet:
		projectVariableSetTargetID = activityEvent.TargetID
	case models.TargetGlobal:
		// no-op - global target does not have a target ID
	case models.TargetMembership:
		membershipTargetID = activityEvent.TargetID
	case models.TargetAgent:
		agentTargetID = activityEvent.TargetID
	case models.TargetPipelineTemplate:
		pipelineTemplateTargetID = activityEvent.TargetID
	case models.TargetPipeline:
		pipelineTargetID = activityEvent.TargetID
	case models.TargetTeam:
		teamTargetID = activityEvent.TargetID
	case models.TargetEnvironment:
		environmentTargetID = activityEvent.TargetID
	case models.TargetServiceAccount:
		serviceAccountTargetID = activityEvent.TargetID
	case models.TargetApprovalRule:
		approvalTargetID = activityEvent.TargetID
	case models.TargetLifecycleTemplate:
		lifecycleTemplateTargetID = activityEvent.TargetID
	case models.TargetReleaseLifecycle:
		releaseLifecycleTargetID = activityEvent.TargetID
	case models.TargetRelease:
		releaseTargetID = activityEvent.TargetID
	case models.TargetPlugin:
		pluginTargetID = activityEvent.TargetID
	case models.TargetPluginVersion:
		pluginVersionTargetID = activityEvent.TargetID
	case models.TargetComment:
		commentTargetID = activityEvent.TargetID
	case models.TargetVCSProvider:
		vcsProviderTargetID = activityEvent.TargetID
	case models.TargetThread:
		threadTargetID = activityEvent.TargetID
	case models.TargetEnvironmentRule:
		environmentRuleTargetID = activityEvent.TargetID
	default:
		// theoretically cannot happen, but in case of a rainy day
		return nil, errors.New("invalid target type %s", activityEvent.TargetType, errors.WithSpan(span))
	}

	var payload any
	if activityEvent.Payload != nil {
		payload = activityEvent.Payload
	}

	timestamp := currentTime()

	sql, args, err := dialect.Insert("activity_events").
		Prepared(true).
		Rows(
			goqu.Record{
				"id":                             newResourceID(),
				"version":                        initialResourceVersion,
				"created_at":                     timestamp,
				"updated_at":                     timestamp,
				"user_id":                        activityEvent.UserID,
				"service_account_id":             activityEvent.ServiceAccountID,
				"organization_id":                activityEvent.OrganizationID,
				"project_id":                     activityEvent.ProjectID,
				"action":                         activityEvent.Action,
				"target_type":                    activityEvent.TargetType,
				"payload":                        payload,
				"role_target_id":                 roleTargetID,
				"organization_target_id":         organizationTargetID,
				"project_target_id":              projectTargetID,
				"project_variable_set_target_id": projectVariableSetTargetID,
				"membership_target_id":           membershipTargetID,
				"agent_target_id":                agentTargetID,
				"pipeline_template_target_id":    pipelineTemplateTargetID,
				"pipeline_target_id":             pipelineTargetID,
				"team_target_id":                 teamTargetID,
				"environment_target_id":          environmentTargetID,
				"service_account_target_id":      serviceAccountTargetID,
				"approval_rule_target_id":        approvalTargetID,
				"lifecycle_template_target_id":   lifecycleTemplateTargetID,
				"release_lifecycle_target_id":    releaseLifecycleTargetID,
				"release_target_id":              releaseTargetID,
				"plugin_target_id":               pluginTargetID,
				"plugin_version_target_id":       pluginVersionTargetID,
				"comment_target_id":              commentTargetID,
				"vcs_provider_target_id":         vcsProviderTargetID,
				"thread_target_id":               threadTargetID,
				"environment_rule_target_id":     environmentRuleTargetID,
			}).Returning(a.getSelectFields()...).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	createdActivityEvent, err := scanActivityEvent(a.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				return nil, errors.New("%s not found",
					strings.ToLower(activityEvent.TargetType.String()),
					errors.WithErrorCode(errors.ENotFound),
					errors.WithSpan(span),
				)
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdActivityEvent, nil
}

func (a *activityEvents) getSelectFields() []interface{} {
	selectFields := []interface{}{}

	for _, field := range activityEventFieldList {
		selectFields = append(selectFields, fmt.Sprintf("activity_events.%s", field))
	}

	return selectFields
}

func scanActivityEvent(row scanner) (*models.ActivityEvent, error) {
	activityEvent := &models.ActivityEvent{}

	// Must use target type to fan out target ID to the various columns.
	var (
		roleTargetID               *string
		organizationTargetID       *string
		projectTargetID            *string
		projectVariableSetTargetID *string
		membershipTargetID         *string
		agentTargetID              *string
		pipelineTemplateTargetID   *string
		pipelineTargetID           *string
		teamTargetID               *string
		environmentTargetID        *string
		serviceAccountTargetID     *string
		approvalTargetID           *string
		lifecycleTemplateTargetID  *string
		releaseLifecycleTargetID   *string
		releaseTargetID            *string
		pluginTargetID             *string
		pluginVersionTargetID      *string
		commentTargetID            *string
		vcsProviderTargetID        *string
		threadTargetID             *string
		environmentRuleTargetID    *string
	)

	fields := []interface{}{
		&activityEvent.Metadata.ID,
		&activityEvent.Metadata.CreationTimestamp,
		&activityEvent.Metadata.LastUpdatedTimestamp,
		&activityEvent.Metadata.Version,
		&activityEvent.UserID,
		&activityEvent.ServiceAccountID,
		&activityEvent.OrganizationID,
		&activityEvent.ProjectID,
		&activityEvent.Action,
		&activityEvent.TargetType,
		&activityEvent.Payload,
		&roleTargetID,
		&organizationTargetID,
		&projectTargetID,
		&projectVariableSetTargetID,
		&membershipTargetID,
		&agentTargetID,
		&pipelineTemplateTargetID,
		&pipelineTargetID,
		&teamTargetID,
		&environmentTargetID,
		&serviceAccountTargetID,
		&approvalTargetID,
		&lifecycleTemplateTargetID,
		&releaseLifecycleTargetID,
		&releaseTargetID,
		&pluginTargetID,
		&pluginVersionTargetID,
		&commentTargetID,
		&vcsProviderTargetID,
		&threadTargetID,
		&environmentRuleTargetID,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	activityEvent.Metadata.PRN = models.ActivityEventResource.BuildPRN(gid.ToGlobalID(gid.ActivityEventType, activityEvent.Metadata.ID))

	switch activityEvent.TargetType {
	case models.TargetRole:
		activityEvent.TargetID = roleTargetID
	case models.TargetOrganization:
		activityEvent.TargetID = organizationTargetID
	case models.TargetProject:
		activityEvent.TargetID = projectTargetID
	case models.TargetProjectVariableSet:
		activityEvent.TargetID = projectVariableSetTargetID
	case models.TargetGlobal:
		// no-op - global target does not have a target ID
	case models.TargetMembership:
		activityEvent.TargetID = membershipTargetID
	case models.TargetAgent:
		activityEvent.TargetID = agentTargetID
	case models.TargetPipelineTemplate:
		activityEvent.TargetID = pipelineTemplateTargetID
	case models.TargetPipeline:
		activityEvent.TargetID = pipelineTargetID
	case models.TargetTeam:
		activityEvent.TargetID = teamTargetID
	case models.TargetEnvironment:
		activityEvent.TargetID = environmentTargetID
	case models.TargetServiceAccount:
		activityEvent.TargetID = serviceAccountTargetID
	case models.TargetApprovalRule:
		activityEvent.TargetID = approvalTargetID
	case models.TargetLifecycleTemplate:
		activityEvent.TargetID = lifecycleTemplateTargetID
	case models.TargetReleaseLifecycle:
		activityEvent.TargetID = releaseLifecycleTargetID
	case models.TargetRelease:
		activityEvent.TargetID = releaseTargetID
	case models.TargetPlugin:
		activityEvent.TargetID = pluginTargetID
	case models.TargetPluginVersion:
		activityEvent.TargetID = pluginVersionTargetID
	case models.TargetComment:
		activityEvent.TargetID = commentTargetID
	case models.TargetVCSProvider:
		activityEvent.TargetID = vcsProviderTargetID
	case models.TargetThread:
		activityEvent.TargetID = threadTargetID
	case models.TargetEnvironmentRule:
		activityEvent.TargetID = environmentRuleTargetID
	default:
		// theoretically cannot happen, but in case of a rainy day
		return nil, errors.New("invalid target type: %s", activityEvent.TargetType)
	}

	return activityEvent, nil
}
