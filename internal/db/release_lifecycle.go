package db

//go:generate go tool mockery --name ReleaseLifecycles --inpackage --case underscore

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ReleaseLifecycles encapsulates the logic to access ReleaseLifecycles from the database
type ReleaseLifecycles interface {
	GetReleaseLifecycleByID(ctx context.Context, id string) (*models.ReleaseLifecycle, error)
	GetReleaseLifecycleByPRN(ctx context.Context, prn string) (*models.ReleaseLifecycle, error)
	GetReleaseLifecycles(ctx context.Context, input *GetReleaseLifecyclesInput) (*ReleaseLifecyclesResult, error)
	CreateReleaseLifecycle(ctx context.Context, releaseLifecycle *models.ReleaseLifecycle) (*models.ReleaseLifecycle, error)
	UpdateReleaseLifecycle(ctx context.Context, releaseLifecycle *models.ReleaseLifecycle) (*models.ReleaseLifecycle, error)
	DeleteReleaseLifecycle(ctx context.Context, releaseLifecycle *models.ReleaseLifecycle) error
}

// ReleaseLifecycleFilter contains the supported fields for filtering ReleaseLifecycle resources
type ReleaseLifecycleFilter struct {
	OrganizationID         *string
	ProjectID              *string
	ReleaseLifecycleScopes []models.ScopeType
	Search                 *string
	ReleaseLifecycleIDs    []string
}

// ReleaseLifecycleSortableField represents the fields that a release lifecycle can be sorted by
type ReleaseLifecycleSortableField string

// ReleaseLifecycleSortableField constants
const (
	ReleaseLifecycleSortableFieldUpdatedAtAsc  ReleaseLifecycleSortableField = "UPDATED_AT_ASC"
	ReleaseLifecycleSortableFieldUpdatedAtDesc ReleaseLifecycleSortableField = "UPDATED_AT_DESC"
)

func (os ReleaseLifecycleSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch os {
	case ReleaseLifecycleSortableFieldUpdatedAtAsc, ReleaseLifecycleSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "release_lifecycles", Col: "updated_at"}
	default:
		return nil
	}
}

func (os ReleaseLifecycleSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(os), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetReleaseLifecyclesInput is the input for listing release lifecycles
type GetReleaseLifecyclesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ReleaseLifecycleSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ReleaseLifecycleFilter
}

// validate validates the input for GetReleaseLifecyclesInput
func (i *GetReleaseLifecyclesInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// ReleaseLifecyclesResult contains the response data and page information
type ReleaseLifecyclesResult struct {
	PageInfo          *pagination.PageInfo
	ReleaseLifecycles []*models.ReleaseLifecycle
}

var releaseLifecycleFieldList = append(metadataFieldList,
	"created_by", "name", "organization_id", "lifecycle_template_id", "environments", "scope", "project_id")

type releaseLifecycles struct {
	dbClient *Client
}

// NewReleaseLifecycles returns an instance of the ReleaseLifecycles interface
func NewReleaseLifecycles(dbClient *Client) ReleaseLifecycles {
	return &releaseLifecycles{dbClient: dbClient}
}

func (p *releaseLifecycles) GetReleaseLifecycleByID(ctx context.Context, id string) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "db.GetReleaseLifecycleByID")
	defer span.End()

	return p.getReleaseLifecycle(ctx, goqu.Ex{"release_lifecycles.id": id})
}

func (p *releaseLifecycles) GetReleaseLifecycleByPRN(ctx context.Context, prn string) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "db.GetReleaseLifecycleByPRN")
	defer span.End()

	path, err := models.ReleaseLifecycleResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	ex := goqu.Ex{}

	switch len(parts) {
	case 2:
		// This is an org/release lifecycle name.
		ex["organizations.name"] = parts[0]
		ex["release_lifecycles.name"] = parts[1]
		ex["release_lifecycles.scope"] = models.OrganizationScope
	case 3:
		// This is a project-scoped release lifecycle path.
		ex["organizations.name"] = parts[0]
		ex["projects.name"] = parts[1]
		ex["release_lifecycles.name"] = parts[2]
		ex["release_lifecycles.scope"] = models.ProjectScope
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getReleaseLifecycle(ctx, ex)
}

func (p *releaseLifecycles) GetReleaseLifecycles(ctx context.Context, input *GetReleaseLifecyclesInput) (*ReleaseLifecyclesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetReleaseLifecycles")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.OrganizationID != nil {
			ex = ex.Append(goqu.I("release_lifecycles.organization_id").Eq(*input.Filter.OrganizationID))
		}

		if input.Filter.ProjectID != nil {
			ex = ex.Append(
				// Use an OR condition to get both organization and project release lifecycles.
				goqu.Or(
					goqu.I("release_lifecycles.project_id").Eq(*input.Filter.ProjectID),
					goqu.And(
						goqu.I("release_lifecycles.organization_id").In(
							dialect.From("projects").
								Select("org_id").
								Where(goqu.I("id").Eq(*input.Filter.ProjectID)),
						),
						goqu.I("release_lifecycles.scope").Eq(models.OrganizationScope),
					),
				),
			)
		}

		if len(input.Filter.ReleaseLifecycleScopes) > 0 {
			ex = ex.Append(goqu.I("release_lifecycles.scope").In(input.Filter.ReleaseLifecycleScopes))
		}

		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("release_lifecycles.name").ILike("%" + *input.Filter.Search + "%"))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.ReleaseLifecycleIDs) > 0 {
			ex = ex.Append(goqu.I("release_lifecycles.id").In(input.Filter.ReleaseLifecycleIDs))
		}
	}

	query := dialect.From(goqu.T("release_lifecycles")).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"release_lifecycles.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"release_lifecycles.project_id": goqu.I("projects.id")})).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "release_lifecycles", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.ReleaseLifecycle{}
	for rows.Next() {
		item, err := scanReleaseLifecycle(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &ReleaseLifecyclesResult{
		PageInfo:          rows.GetPageInfo(),
		ReleaseLifecycles: results,
	}

	return result, nil
}

func (p *releaseLifecycles) CreateReleaseLifecycle(ctx context.Context,
	releaseLifecycle *models.ReleaseLifecycle) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "db.CreateReleaseLifecycle")
	defer span.End()

	environments, err := p.marshalEnvironments(releaseLifecycle.EnvironmentNames)
	if err != nil {
		return nil, err
	}

	timestamp := currentTime()

	sql, args, err := dialect.From("release_lifecycles").
		Prepared(true).
		With("release_lifecycles",
			dialect.Insert("release_lifecycles").
				Rows(goqu.Record{
					"id":                    newResourceID(),
					"version":               initialResourceVersion,
					"created_at":            timestamp,
					"updated_at":            timestamp,
					"created_by":            releaseLifecycle.CreatedBy,
					"name":                  releaseLifecycle.Name,
					"organization_id":       releaseLifecycle.OrganizationID,
					"lifecycle_template_id": releaseLifecycle.LifecycleTemplateID,
					"environments":          environments,
					"scope":                 releaseLifecycle.Scope,
					"project_id":            releaseLifecycle.ProjectID,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"release_lifecycles.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"release_lifecycles.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdReleaseLifecycle, err := scanReleaseLifecycle(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_release_lifecycle_organization_id":
					return nil, errors.New("organization does not exist",
						errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_release_lifecycle_lifecycle_template_id":
					return nil, errors.New("lifecycle template does not exist",
						errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_release_lifecycle_project_id":
					return nil, errors.New("project does not exist",
						errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}

			if isUniqueViolation(pgErr) {
				return nil, errors.New("release lifecycle with name %s already exists in organization with ID %s",
					releaseLifecycle.Name, releaseLifecycle.OrganizationID, errors.WithSpan(span), errors.WithErrorCode(errors.EConflict))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdReleaseLifecycle, nil
}

func (p *releaseLifecycles) UpdateReleaseLifecycle(ctx context.Context,
	releaseLifecycle *models.ReleaseLifecycle) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateReleaseLifecycle")
	defer span.End()

	environments, err := p.marshalEnvironments(releaseLifecycle.EnvironmentNames)
	if err != nil {
		return nil, err
	}

	timestamp := currentTime()

	sql, args, err := dialect.From("release_lifecycles").
		Prepared(true).
		With("release_lifecycles",
			dialect.Update("release_lifecycles").
				Set(
					goqu.Record{
						"version":               goqu.L("? + ?", goqu.C("version"), 1),
						"updated_at":            timestamp,
						"lifecycle_template_id": releaseLifecycle.LifecycleTemplateID,
						"environments":          environments,
					}).
				Where(goqu.Ex{"id": releaseLifecycle.Metadata.ID, "version": releaseLifecycle.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"release_lifecycles.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"release_lifecycles.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedReleaseLifecycle, err := scanReleaseLifecycle(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedReleaseLifecycle, nil
}

func (p *releaseLifecycles) DeleteReleaseLifecycle(ctx context.Context, toDelete *models.ReleaseLifecycle) error {
	ctx, span := tracer.Start(ctx, "db.DeleteReleaseLifecycle")
	defer span.End()

	sql, args, err := dialect.From("release_lifecycles").
		Prepared(true).
		With("release_lifecycles",
			dialect.Delete("release_lifecycles").
				Where(goqu.Ex{"id": toDelete.Metadata.ID, "version": toDelete.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"release_lifecycles.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"release_lifecycles.project_id": goqu.I("projects.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanReleaseLifecycle(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *releaseLifecycles) getReleaseLifecycle(ctx context.Context, exp exp.Expression) (*models.ReleaseLifecycle, error) {
	query := dialect.From(goqu.T("release_lifecycles")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"release_lifecycles.organization_id": goqu.I("organizations.id")})).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.Ex{"release_lifecycles.project_id": goqu.I("projects.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	releaseLifecycle, err := scanReleaseLifecycle(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return releaseLifecycle, nil
}

func (*releaseLifecycles) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range releaseLifecycleFieldList {
		selectFields = append(selectFields, fmt.Sprintf("release_lifecycles.%s", field))
	}

	selectFields = append(selectFields, "organizations.name", "projects.name")

	return selectFields
}

func (*releaseLifecycles) marshalEnvironments(environments []string) ([]byte, error) {
	environmentsJSON, err := json.Marshal(environments)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal environment names to JSON: %w", err)
	}

	return environmentsJSON, nil
}

func scanReleaseLifecycle(row scanner) (*models.ReleaseLifecycle, error) {
	var organizationName string
	var projectName sql.NullString

	releaseLifecycle := &models.ReleaseLifecycle{}

	fields := []interface{}{
		&releaseLifecycle.Metadata.ID,
		&releaseLifecycle.Metadata.CreationTimestamp,
		&releaseLifecycle.Metadata.LastUpdatedTimestamp,
		&releaseLifecycle.Metadata.Version,
		&releaseLifecycle.CreatedBy,
		&releaseLifecycle.Name,
		&releaseLifecycle.OrganizationID,
		&releaseLifecycle.LifecycleTemplateID,
		&releaseLifecycle.EnvironmentNames,
		&releaseLifecycle.Scope,
		&releaseLifecycle.ProjectID,
		&organizationName,
		&projectName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	switch releaseLifecycle.Scope {
	case models.OrganizationScope:
		releaseLifecycle.Metadata.PRN = models.ReleaseLifecycleResource.BuildPRN(organizationName, releaseLifecycle.Name)
	case models.ProjectScope:
		releaseLifecycle.Metadata.PRN = models.ReleaseLifecycleResource.BuildPRN(organizationName, projectName.String, releaseLifecycle.Name)
	default:
		return nil, fmt.Errorf("unexpected release lifecycle scope: %s", releaseLifecycle.Scope)
	}

	return releaseLifecycle, nil
}
