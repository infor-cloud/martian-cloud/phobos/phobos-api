package db

//go:generate go tool mockery --name Memberships --inpackage --case underscore

import (
	"context"
	"database/sql"
	"fmt"
	"slices"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Memberships encapsulates the logic to access memberships from the database
type Memberships interface {
	GetMembershipByID(ctx context.Context, id string) (*models.Membership, error)
	GetMembershipByPRN(ctx context.Context, prn string) (*models.Membership, error)
	GetMemberships(ctx context.Context, input *GetMembershipsInput) (*MembershipsResult, error)
	CreateMembership(ctx context.Context, input *models.Membership) (*models.Membership, error)
	UpdateMembership(ctx context.Context, membership *models.Membership) (*models.Membership, error)
	DeleteMembership(ctx context.Context, membership *models.Membership) error
}

// MembershipSortableField represents the fields that a membership can be sorted by
type MembershipSortableField string

// MembershipSortableField constants
const (
	MembershipSortableFieldUpdatedAtAsc  MembershipSortableField = "UPDATED_AT_ASC"
	MembershipSortableFieldUpdatedAtDesc MembershipSortableField = "UPDATED_AT_DESC"
)

func (m MembershipSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch m {
	case MembershipSortableFieldUpdatedAtAsc, MembershipSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "memberships", Col: "updated_at"}
	default:
		return nil
	}
}

func (m MembershipSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(m), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// MembershipFilter contains the supported fields for filtering Membership resources
type MembershipFilter struct {
	UserID           *string
	TeamID           *string
	ServiceAccountID *string
	OrganizationID   *string
	ProjectID        *string
	RoleID           *string
	MembershipScopes []models.ScopeType
	MembershipIDs    []string
}

// GetMembershipsInput is the input for listing memberships
type GetMembershipsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *MembershipSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *MembershipFilter
}

// MembershipsResult contains the response data and page information
type MembershipsResult struct {
	PageInfo    *pagination.PageInfo
	Memberships []models.Membership
}

type memberships struct {
	dbClient *Client
}

var membershipFieldList = append(metadataFieldList, "role_id", "user_id", "team_id", "service_account_id", "organization_id", "project_id", "scope")

// NewMemberships returns an instance of the Memberships interface
func NewMemberships(dbClient *Client) Memberships {
	return &memberships{dbClient: dbClient}
}

func (m *memberships) GetMembershipByID(
	ctx context.Context,
	id string,
) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "db.GetMembershipByID")
	defer span.End()

	return m.getMembership(ctx, goqu.Ex{"memberships.id": id})
}

func (m *memberships) GetMembershipByPRN(ctx context.Context, prn string) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "db.GetMembershipByPRN")
	defer span.End()

	path, err := models.MembershipResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	var expr exp.Ex

	switch len(parts) {
	case 1:
		// This is a global membership.
		expr = goqu.Ex{
			"memberships.id": gid.FromGlobalID(parts[0]),
		}
	case 2:
		// This is an organization membership.
		expr = goqu.Ex{
			"organizations.name": parts[0],
			"memberships.id":     gid.FromGlobalID(parts[1]),
		}
	case 3:
		// This is a project membership.
		expr = goqu.Ex{
			"organizations.name": parts[0],
			"projects.name":      parts[1],
			"memberships.id":     gid.FromGlobalID(parts[2]),
		}
	default:
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return m.getMembership(ctx, expr)
}

func (m *memberships) GetMemberships(
	ctx context.Context,
	input *GetMembershipsInput,
) (*MembershipsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetMemberships")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.UserID != nil {
			ex = ex.Append(goqu.Or(
				// This filters for direct membership.
				goqu.I("memberships.user_id").Eq(*input.Filter.UserID),

				// This filters for indirect via the user being a team member.
				goqu.I("memberships.team_id").In(
					dialect.From("team_members").
						Select("team_id").
						Where(goqu.I("team_members.user_id").Eq(*input.Filter.UserID))),
			))
		}
		if input.Filter.TeamID != nil {
			ex = ex.Append(goqu.I("memberships.team_id").Eq(*input.Filter.TeamID))
		}
		if input.Filter.ServiceAccountID != nil {
			ex = ex.Append(goqu.I("memberships.service_account_id").Eq(*input.Filter.ServiceAccountID))
		}
		if input.Filter.RoleID != nil {
			ex = ex.Append(goqu.I("memberships.role_id").Eq(*input.Filter.RoleID))
		}
		if len(input.Filter.MembershipScopes) > 0 {
			ex = ex.Append(goqu.I("memberships.scope").In(input.Filter.MembershipScopes))
		}
		if input.Filter.OrganizationID != nil && input.Filter.ProjectID != nil {
			return nil, errors.New("cannot filter memberships for both organization and project", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
		if input.Filter.OrganizationID != nil {
			orExp := goqu.Or(goqu.I("memberships.organization_id").Eq(*input.Filter.OrganizationID))

			if slices.Contains(input.Filter.MembershipScopes, models.ProjectScope) {
				orExp = orExp.Append(goqu.I("projects.org_id").Eq(*input.Filter.OrganizationID))
			}

			if slices.Contains(input.Filter.MembershipScopes, models.GlobalScope) {
				orExp = orExp.Append(goqu.I("memberships.scope").Eq(models.GlobalScope))
			}

			ex = ex.Append(orExp)
		}
		if input.Filter.ProjectID != nil {
			orExp := goqu.Or(goqu.I("memberships.project_id").Eq(*input.Filter.ProjectID))

			if slices.Contains(input.Filter.MembershipScopes, models.OrganizationScope) {
				// Must use a sub query to filter for organization membership.
				orExp = orExp.Append(
					goqu.And(
						goqu.I("memberships.organization_id").In(
							dialect.From("projects").
								Select("org_id").
								Where(goqu.I("projects.id").Eq(*input.Filter.ProjectID)),
						),
						goqu.I("memberships.scope").Eq(models.OrganizationScope),
					),
				)
			}

			if slices.Contains(input.Filter.MembershipScopes, models.GlobalScope) {
				orExp = orExp.Append(goqu.I("memberships.scope").Eq(models.GlobalScope))
			}

			ex = ex.Append(orExp)
		}
		if len(input.Filter.MembershipIDs) > 0 {
			ex = ex.Append(goqu.I("memberships.id").In(input.Filter.MembershipIDs))
		}
	}

	// Must left join organizations on both memberships.organization_id and projects.org_id since
	// membership.organization_id is only available for organization memberships and we still need
	// to return the organization name for project memberships PRN.
	orgJoinCondition := goqu.Or(
		goqu.I("memberships.organization_id").Eq(goqu.I("organizations.id")),
		goqu.I("projects.org_id").Eq(goqu.I("organizations.id")),
	)

	query := dialect.From("memberships").
		Select(m.getSelectFields()...).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("memberships.project_id").Eq(goqu.I("projects.id")))).
		LeftJoin(goqu.T("organizations"), goqu.On(orgJoinCondition)).
		Where(ex)

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "memberships", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, m.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Membership{}
	for rows.Next() {
		item, err := scanMembership(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &MembershipsResult{
		PageInfo:    rows.GetPageInfo(),
		Memberships: results,
	}

	return result, nil
}

func (m *memberships) CreateMembership(
	ctx context.Context,
	input *models.Membership,
) (*models.Membership, error) {

	ctx, span := tracer.Start(ctx, "db.CreateMembership")
	defer span.End()

	timestamp := currentTime()

	record := goqu.Record{
		"id":              newResourceID(),
		"version":         initialResourceVersion,
		"created_at":      timestamp,
		"updated_at":      timestamp,
		"role_id":         input.RoleID,
		"organization_id": input.OrganizationID,
		"project_id":      input.ProjectID,
		"scope":           input.Scope,
	}

	count := 0
	if input.UserID != nil {
		record["user_id"] = input.UserID
		count++
	}

	if input.TeamID != nil {
		record["team_id"] = input.TeamID
		count++
	}

	if input.ServiceAccountID != nil {
		record["service_account_id"] = input.ServiceAccountID
		count++
	}

	if count != 1 {
		return nil, errors.New("only one of user_id, team_id or service_account_id must be specified", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Must left join organizations on both memberships.organization_id and projects.org_id since
	// membership.organization_id is only available for organization memberships and we still need
	// to return the organization name for project memberships PRN.
	orgJoinCondition := goqu.Or(
		goqu.I("memberships.organization_id").Eq(goqu.I("organizations.id")),
		goqu.I("projects.org_id").Eq(goqu.I("organizations.id")),
	)

	sql, args, err := dialect.From("memberships").
		Prepared(true).
		With("memberships", dialect.Insert("memberships").Rows(record).Returning("*")).
		Select(m.getSelectFields()...).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("memberships.project_id").Eq(goqu.I("projects.id")))).
		LeftJoin(goqu.T("organizations"), goqu.On(orgJoinCondition)).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdMembership, err := scanMembership(m.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("member already exists", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_memberships_organization_id":
					return nil, errors.New("organization does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_memberships_project_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_memberships_user_id":
					return nil, errors.New("user does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_memberships_team_id":
					return nil, errors.New("team does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_memberships_service_account_id":
					return nil, errors.New("service account does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				case "fk_memberships_role_id":
					return nil, errors.New("role does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdMembership, nil
}

func (m *memberships) UpdateMembership(
	ctx context.Context,
	membership *models.Membership,
) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateMembership")
	defer span.End()

	timestamp := currentTime()

	// Must left join organizations on both memberships.organization_id and projects.org_id since
	// membership.organization_id is only available for organization memberships and we still need
	// to return the organization name for project memberships PRN.
	orgJoinCondition := goqu.Or(
		goqu.I("memberships.organization_id").Eq(goqu.I("organizations.id")),
		goqu.I("projects.org_id").Eq(goqu.I("organizations.id")),
	)

	sql, args, err := dialect.From("memberships").
		Prepared(true).
		With("memberships",
			dialect.Update("memberships").
				Set(goqu.Record{
					"version":    goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at": timestamp,
					"role_id":    membership.RoleID,
				}).
				Where(goqu.Ex{"id": membership.Metadata.ID, "version": membership.Metadata.Version}).
				Returning("*"),
		).Select(m.getSelectFields()...).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("memberships.project_id").Eq(goqu.I("projects.id")))).
		LeftJoin(goqu.T("organizations"), goqu.On(orgJoinCondition)).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedMembership, err := scanMembership(m.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		if pgErr := asPgError(err); pgErr != nil {
			if isUniqueViolation(pgErr) {
				return nil, errors.New("member already exists", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
			}
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_memberships_role_id":
					return nil, errors.New("role does not exist", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
				}
			}
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedMembership, nil
}

func (m *memberships) DeleteMembership(
	ctx context.Context,
	membership *models.Membership,
) error {
	ctx, span := tracer.Start(ctx, "db.DeleteMembership")
	defer span.End()

	// Must left join organizations on both memberships.organization_id and projects.org_id since
	// membership.organization_id is only available for organization memberships and we still need
	// to return the organization name for project memberships PRN.
	orgJoinCondition := goqu.Or(
		goqu.I("memberships.organization_id").Eq(goqu.I("organizations.id")),
		goqu.I("projects.org_id").Eq(goqu.I("organizations.id")),
	)

	sql, args, err := dialect.From("memberships").
		Prepared(true).
		With("memberships",
			dialect.Delete("memberships").
				Where(goqu.Ex{"id": membership.Metadata.ID, "version": membership.Metadata.Version}).
				Returning("*"),
		).Select(m.getSelectFields()...).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("memberships.project_id").Eq(goqu.I("projects.id")))).
		LeftJoin(goqu.T("organizations"), goqu.On(orgJoinCondition)).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanMembership(m.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			return ErrOptimisticLockError
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return errors.Wrap(pgErr, pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (m *memberships) getMembership(ctx context.Context, ex goqu.Ex) (*models.Membership, error) {
	// Must left join organizations on both memberships.organization_id and projects.org_id since
	// membership.organization_id is only available for organization memberships and we still need
	// to return the organization name for project memberships PRN.
	orgJoinCondition := goqu.Or(
		goqu.I("memberships.organization_id").Eq(goqu.I("organizations.id")),
		goqu.I("projects.org_id").Eq(goqu.I("organizations.id")),
	)

	sql, args, err := dialect.From("memberships").
		Prepared(true).
		Select(m.getSelectFields()...).
		LeftJoin(goqu.T("projects"), goqu.On(goqu.I("memberships.project_id").Eq(goqu.I("projects.id")))).
		LeftJoin(goqu.T("organizations"), goqu.On(orgJoinCondition)).
		Where(ex).ToSQL()
	if err != nil {
		return nil, err
	}

	membership, err := scanMembership(m.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return membership, nil
}

func (*memberships) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range membershipFieldList {
		selectFields = append(selectFields, fmt.Sprintf("memberships.%s", field))
	}

	selectFields = append(selectFields, "projects.name")
	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanMembership(row scanner) (*models.Membership, error) {
	var organizationName, projectName sql.NullString
	membership := &models.Membership{}

	fields := []interface{}{
		&membership.Metadata.ID,
		&membership.Metadata.CreationTimestamp,
		&membership.Metadata.LastUpdatedTimestamp,
		&membership.Metadata.Version,
		&membership.RoleID,
		&membership.UserID,
		&membership.TeamID,
		&membership.ServiceAccountID,
		&membership.OrganizationID,
		&membership.ProjectID,
		&membership.Scope,
		&projectName,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	globalID := gid.ToGlobalID(gid.MembershipType, membership.Metadata.ID)

	switch membership.Scope {
	case models.GlobalScope:
		membership.Metadata.PRN = models.MembershipResource.BuildPRN(globalID)
	case models.OrganizationScope:
		membership.Metadata.PRN = models.MembershipResource.BuildPRN(organizationName.String, globalID)
	case models.ProjectScope:
		membership.Metadata.PRN = models.MembershipResource.BuildPRN(organizationName.String, projectName.String, globalID)
	default:
		return nil, fmt.Errorf("unexpected membership scope: %s", membership.Scope)
	}

	return membership, nil
}
