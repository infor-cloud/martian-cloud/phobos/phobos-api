//go:build integration

package db

import (
	"context"
	"fmt"
	"net/url"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Miscellaneous values used throughout testing.
var (
	sampleOAuthState = uuid.New()
	gitHubURL        = url.URL{Scheme: "https", Host: "github.com"}
)

func (r VCSProviderSortableField) getValue() string {
	return string(r)
}

func TestVCSProvider_GetProviderByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	provider, err := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
		Name:           "test-github-provider",
		URL:            gitHubURL,
		OrganizationID: org.Metadata.ID,
		Scope:          models.OrganizationScope,
		Type:           models.GitHubType,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		id              string
		expectProvider  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by id",
			id:             provider.Metadata.ID,
			expectProvider: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualProvider, err := testClient.client.VCSProviders.GetProviderByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProvider {
				require.NotNil(t, actualProvider)
				assert.Equal(t, test.id, actualProvider.Metadata.ID)
			} else {
				assert.Nil(t, actualProvider)
			}
		})
	}
}

func TestVCSProvider_GetProviderByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	// Org VCS provider.
	orgProvider, err := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
		Name:           "org-github-provider",
		URL:            gitHubURL,
		OrganizationID: org.Metadata.ID,
		Scope:          models.OrganizationScope,
		Type:           models.GitHubType,
	})
	require.NoError(t, err)

	// Project VCS provider.
	projProvider, err := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
		Name:           "proj-github-provider",
		URL:            gitHubURL,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &project.Metadata.ID,
		Scope:          models.ProjectScope,
		Type:           models.GitHubType,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		prn             string
		expectProvider  bool
	}

	testCases := []testCase{
		{
			name:           "get org resource by PRN",
			prn:            orgProvider.Metadata.PRN,
			expectProvider: true,
		},
		{
			name:           "get project resource by PRN",
			prn:            projProvider.Metadata.PRN,
			expectProvider: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.VCSProviderResource.BuildPRN(org.Name, "unknown"),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualProvider, err := testClient.client.VCSProviders.GetProviderByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProvider {
				require.NotNil(t, actualProvider)
				assert.Equal(t, test.prn, actualProvider.Metadata.PRN)
			} else {
				assert.Nil(t, actualProvider)
			}
		})
	}
}

func TestVCSProvider_GetProviderByOAuthState(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	provider, err := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
		Name:              "test-github-provider",
		URL:               gitHubURL,
		OrganizationID:    org.Metadata.ID,
		Scope:             models.OrganizationScope,
		Type:              models.GitHubType,
		AuthType:          models.OAuthType,
		OAuthClientID:     ptr.String("client-id"),
		OAuthClientSecret: ptr.String("client-secret"),
		OAuthState:        ptr.String(sampleOAuthState.String()),
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		oauthState      string
		expectProvider  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by oauth state",
			oauthState:     *provider.OAuthState,
			expectProvider: true,
		},
		{
			name:       "resource with state not found",
			oauthState: uuid.New().String(),
		},
		{
			name:            "get resource with invalid state will return an error",
			oauthState:      "not-a-uuid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualProvider, err := testClient.client.VCSProviders.GetProviderByOAuthState(ctx, test.oauthState)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectProvider {
				require.NotNil(t, actualProvider)
				require.NotNil(t, actualProvider.OAuthState)
				assert.Equal(t, test.oauthState, *actualProvider.OAuthState)
			} else {
				assert.Nil(t, actualProvider)
			}
		})
	}
}

func TestVCSProvider_GetProviders(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	providers := make([]*models.VCSProvider, 10)
	for i := 0; i < len(providers); i++ {
		provider, cErr := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
			Name:              fmt.Sprintf("test-provider-%d", i),
			URL:               gitHubURL,
			OrganizationID:    org.Metadata.ID,
			Scope:             models.OrganizationScope,
			Type:              models.GitHubType,
			AuthType:          models.OAuthType,
			OAuthClientID:     ptr.String("client-id"),
			OAuthClientSecret: ptr.String("client-secret"),
			OAuthState:        ptr.String(sampleOAuthState.String()),
		})
		require.NoError(t, cErr)

		providers[i] = provider
	}

	// Project VCS provider.
	projProvider, err := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
		Name:           "proj-github-provider",
		URL:            gitHubURL,
		OrganizationID: org.Metadata.ID,
		ProjectID:      &project.Metadata.ID,
		Scope:          models.ProjectScope,
		Type:           models.GitHubType,
	})
	require.NoError(t, err)

	providers = append(providers, projProvider)

	type testCase struct {
		filter            *VCSProviderFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "all providers",
			filter:            &VCSProviderFilter{},
			expectResultCount: len(providers),
		},
		{
			name: "filter by full search",
			filter: &VCSProviderFilter{
				Search: ptr.String("test-provider"),
			},
			expectResultCount: len(providers) - 1,
		},
		{
			name: "filter by empty search",
			filter: &VCSProviderFilter{
				Search: ptr.String(""),
			},
			expectResultCount: len(providers),
		},
		{
			name: "filter by organization",
			filter: &VCSProviderFilter{
				OrganizationID: &org.Metadata.ID,
			},
			expectResultCount: len(providers),
		},
		{
			name: "filter by project",
			filter: &VCSProviderFilter{
				ProjectID: &project.Metadata.ID,
			},
			expectResultCount: len(providers),
		},
		{
			name: "filter by project provider type",
			filter: &VCSProviderFilter{
				VCSProviderScopes: []models.ScopeType{
					models.ProjectScope,
				},
			},
			expectResultCount: 1,
		},
		{
			name: "filter by org provider type",
			filter: &VCSProviderFilter{
				VCSProviderScopes: []models.ScopeType{
					models.OrganizationScope,
				},
			},
			expectResultCount: len(providers) - 1,
		},
		{
			name: "filter by provider ids",
			filter: &VCSProviderFilter{
				VCSProviderIDs: []string{
					providers[0].Metadata.ID,
					providers[1].Metadata.ID,
					providers[2].Metadata.ID,
				},
			},
			expectResultCount: 3,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.VCSProviders.GetProviders(ctx, &GetVCSProvidersInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)
			assert.Equal(t, test.expectResultCount, len(result.VCSProviders))
		})
	}
}

func TestVCSProvider_GetProvidersWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, err = testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
			Name:           fmt.Sprintf("test-provider-%d", i),
			OrganizationID: org.Metadata.ID,
			Scope:          models.OrganizationScope,
		})
		require.NoError(t, err)
	}

	sortableFields := []sortableField{
		VCSProviderSortableFieldCreatedAtAsc,
		VCSProviderSortableFieldCreatedAtDesc,
		VCSProviderSortableFieldUpdatedAtAsc,
		VCSProviderSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := VCSProviderSortableField(sortByField.getValue())

		result, err := testClient.client.VCSProviders.GetProviders(ctx, &GetVCSProvidersInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for _, resource := range result.VCSProviders {
			resourceCopy := resource
			resources = append(resources, &resourceCopy)
		}

		return result.PageInfo, resources, nil
	})
}

func TestVCSProvider_CreateVCSProvider(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	project, err := testClient.client.Projects.CreateProject(ctx, &models.Project{
		Name:  "test-project",
		OrgID: org.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		provider        *models.VCSProvider
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create org provider",
			provider: &models.VCSProvider{
				Name:              "test-org-provider",
				URL:               gitHubURL,
				OrganizationID:    org.Metadata.ID,
				Scope:             models.OrganizationScope,
				Type:              models.GitHubType,
				AuthType:          models.OAuthType,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
		},
		{
			name: "successfully create project provider",
			provider: &models.VCSProvider{
				Name:              "test-proj-provider",
				URL:               gitHubURL,
				OrganizationID:    org.Metadata.ID,
				ProjectID:         &project.Metadata.ID,
				Scope:             models.ProjectScope,
				Type:              models.GitHubType,
				AuthType:          models.OAuthType,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
		},
		{
			name: "create will fail for duplicate org provider",
			provider: &models.VCSProvider{
				Name:              "test-org-provider",
				URL:               gitHubURL,
				OrganizationID:    org.Metadata.ID,
				Scope:             models.OrganizationScope,
				Type:              models.GitHubType,
				AuthType:          models.OAuthType,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
			expectErrorCode: errors.EConflict,
		},
		{
			name: "create will fail for duplicate project provider",
			provider: &models.VCSProvider{
				Name:              "test-proj-provider",
				URL:               gitHubURL,
				OrganizationID:    org.Metadata.ID,
				ProjectID:         &project.Metadata.ID,
				Scope:             models.ProjectScope,
				Type:              models.GitHubType,
				AuthType:          models.OAuthType,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
			expectErrorCode: errors.EConflict,
		},
		{
			name: "create will fail since organization doesn't exist",
			provider: &models.VCSProvider{
				Name:           "test-org-provider-2",
				OrganizationID: nonExistentID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "create will fail since project doesn't exist",
			provider: &models.VCSProvider{
				Name:           "test-proj-provider-2",
				OrganizationID: org.Metadata.ID,
				ProjectID:      ptr.String(nonExistentID),
				Scope:          models.ProjectScope,
			},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualProvider, err := testClient.client.VCSProviders.CreateProvider(ctx, test.provider)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, actualProvider)
		})
	}
}

func TestVCSProvider_UpdateProvider(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	provider, err := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
		Name:              "test-github-provider",
		URL:               gitHubURL,
		OrganizationID:    org.Metadata.ID,
		Scope:             models.OrganizationScope,
		Type:              models.GitHubType,
		AuthType:          models.OAuthType,
		OAuthClientID:     ptr.String("client-id"),
		OAuthClientSecret: ptr.String("client-secret"),
		OAuthState:        ptr.String(sampleOAuthState.String()),
	})
	require.NoError(t, err)

	oauthToken := "newTokenValue"

	type testCase struct {
		name            string
		oAuthToken      string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "successfully update resource",
			version: provider.Metadata.Version,
		},
		{
			name:            "update will fail because of resource version mismatch",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualProvider, err := testClient.client.VCSProviders.UpdateProvider(ctx, &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID:      provider.Metadata.ID,
					Version: test.version,
				},
				OAuthAccessToken: &oauthToken,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, actualProvider)
			require.NotNil(t, actualProvider.OAuthAccessToken)
			assert.Equal(t, oauthToken, *actualProvider.OAuthAccessToken)
		})
	}
}

func TestVCSProvider_DeleteProvider(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	org, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "test-org",
	})
	require.NoError(t, err)

	provider, err := testClient.client.VCSProviders.CreateProvider(ctx, &models.VCSProvider{
		Name:              "test-github-provider",
		URL:               gitHubURL,
		OrganizationID:    org.Metadata.ID,
		Scope:             models.OrganizationScope,
		Type:              models.GitHubType,
		AuthType:          models.OAuthType,
		OAuthClientID:     ptr.String("client-id"),
		OAuthClientSecret: ptr.String("client-secret"),
		OAuthState:        ptr.String(sampleOAuthState.String()),
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		id              string
		version         int
	}

	testCases := []testCase{
		{
			name:            "delete will fail because resource version doesn't match",
			id:              provider.Metadata.ID,
			expectErrorCode: errors.EOptimisticLock,
			version:         -1,
		},
		{
			name:    "successfully delete resource",
			id:      provider.Metadata.ID,
			version: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.VCSProviders.DeleteProvider(ctx, &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID:      test.id,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
