package db

//go:generate go tool mockery --name ProjectVariables --inpackage --case underscore

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/jackc/pgx/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ProjectVariables encapsulates the logic to access ProjectVariables from the database
type ProjectVariables interface {
	GetProjectVariableByID(ctx context.Context, id string) (*models.ProjectVariable, error)
	GetProjectVariableByPRN(ctx context.Context, prn string) (*models.ProjectVariable, error)
	GetProjectVariables(ctx context.Context, input *GetProjectVariablesInput) (*ProjectVariablesResult, error)
	CreateProjectVariable(ctx context.Context, variable *models.ProjectVariable) (*models.ProjectVariable, error)
	DeleteProjectVariable(ctx context.Context, variable *models.ProjectVariable) error
}

// ProjectVariableFilter contains the supported fields for filtering ProjectVariable resources
type ProjectVariableFilter struct {
	ProjectVariableSetID      *string
	ProjectID                 *string
	PipelineType              *models.PipelineType
	VariableKey               *string
	CreatedAtTimeRangeEnd     *time.Time
	Search                    *string
	ProjectVariableIDs        []string
	EnvironmentScopes         []string
	ExcludeProjectVariableIDs []string
}

// ProjectVariableSortableField represents the fields that a project can be sorted by
type ProjectVariableSortableField string

// ProjectVariableSortableField constants
const (
	ProjectVariableSortableFieldCreatedAtAsc  ProjectVariableSortableField = "CREATED_AT_ASC"
	ProjectVariableSortableFieldCreatedAtDesc ProjectVariableSortableField = "CREATED_AT_DESC"
	ProjectVariableSortableFieldKeyAsc        ProjectVariableSortableField = "KEY_ASC"
	ProjectVariableSortableFieldKeyDesc       ProjectVariableSortableField = "KEY_DESC"
)

func (pv ProjectVariableSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch pv {
	case ProjectVariableSortableFieldCreatedAtAsc, ProjectVariableSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "project_variables", Col: "created_at"}
	case ProjectVariableSortableFieldKeyAsc, ProjectVariableSortableFieldKeyDesc:
		return &pagination.FieldDescriptor{Key: "key", Table: "project_variables", Col: "key"}
	default:
		return nil
	}
}

func (pv ProjectVariableSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(pv), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// GetProjectVariablesInput is the input for listing projectVariables
type GetProjectVariablesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *ProjectVariableSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *ProjectVariableFilter
}

// validate checks the input for GetProjectVariablesInput
func (i *GetProjectVariablesInput) validate() error {
	if i.Filter != nil {
		if i.Filter.Search != nil && len(*i.Filter.Search) > maxSearchFilterLength {
			return fmt.Errorf("search filter exceeds maximum length of %d", maxSearchFilterLength)
		}
	}

	return nil
}

// ProjectVariablesResult contains the response data and page information
type ProjectVariablesResult struct {
	PageInfo         *pagination.PageInfo
	ProjectVariables []*models.ProjectVariable
}

var projectVariableFieldList = append(metadataFieldList, "project_id", "key", "value", "environment_scope", "pipeline_type", "created_by")

type projectVariables struct {
	dbClient *Client
}

// NewProjectVariables returns an instance of the ProjectVariables interface
func NewProjectVariables(dbClient *Client) ProjectVariables {
	return &projectVariables{dbClient: dbClient}
}

func (p *projectVariables) GetProjectVariableByID(ctx context.Context, id string) (*models.ProjectVariable, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectVariableByID")
	defer span.End()

	return p.getProjectVariable(ctx, goqu.Ex{"project_variables.id": id})
}

func (p *projectVariables) GetProjectVariableByPRN(ctx context.Context, prn string) (*models.ProjectVariable, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectVariableByPRN")
	defer span.End()

	path, err := models.ProjectVariableResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	if len(parts) != 3 {
		return nil, errors.New("invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return p.getProjectVariable(ctx, goqu.Ex{
		"organizations.name":   parts[0],
		"projects.name":        parts[1],
		"project_variables.id": parts[2],
	})
}

func (p *projectVariables) GetProjectVariables(ctx context.Context, input *GetProjectVariablesInput) (*ProjectVariablesResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetProjectVariables")
	defer span.End()

	if err := input.validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("project_variables.project_id").Eq(*input.Filter.ProjectID))
		}

		if input.Filter.ProjectVariableSetID != nil {
			ex = ex.Append(goqu.I("project_variable_set_variable_relation.variable_set_id").Eq(*input.Filter.ProjectVariableSetID))
		}

		// This check avoids an SQL syntax error if an empty slice is provided.
		if len(input.Filter.ProjectVariableIDs) > 0 {
			ex = ex.Append(goqu.I("project_variables.id").In(input.Filter.ProjectVariableIDs))
		}

		if len(input.Filter.ExcludeProjectVariableIDs) > 0 {
			ex = ex.Append(goqu.I("project_variables.id").NotIn(input.Filter.ExcludeProjectVariableIDs))
		}

		if input.Filter.PipelineType != nil {
			ex = ex.Append(goqu.I("project_variables.pipeline_type").Eq(string(*input.Filter.PipelineType)))
		}

		if input.Filter.VariableKey != nil {
			ex = ex.Append(goqu.I("project_variables.key").Eq(*input.Filter.VariableKey))
		}

		if len(input.Filter.EnvironmentScopes) > 0 {
			ex = ex.Append(goqu.I("project_variables.environment_scope").In(input.Filter.EnvironmentScopes))
		}

		if input.Filter.CreatedAtTimeRangeEnd != nil {
			// Must use UTC here otherwise, queries will return unexpected results.
			ex = ex.Append(goqu.I("project_variables.created_at").Lte(input.Filter.CreatedAtTimeRangeEnd.UTC()))
		}

		if input.Filter.Search != nil && *input.Filter.Search != "" {
			ex = ex.Append(goqu.I("project_variables.key").ILike("%" + *input.Filter.Search + "%"))
		}
	}

	query := dialect.From(goqu.T("project_variables")).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variables.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(ex)

	if input.Filter != nil && input.Filter.ProjectVariableSetID != nil {
		query = query.LeftJoin(goqu.T("project_variable_set_variable_relation"), goqu.On(goqu.Ex{"project_variables.id": goqu.I("project_variable_set_variable_relation.variable_id")}))
	}

	sortDirection := pagination.AscSort

	var sortBy *pagination.FieldDescriptor
	if input.Sort != nil {
		sortDirection = input.Sort.getSortDirection()
		sortBy = input.Sort.getFieldDescriptor()
	}

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "project_variables", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, p.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []*models.ProjectVariable{}
	for rows.Next() {
		item, err := scanProjectVariable(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := &ProjectVariablesResult{
		PageInfo:         rows.GetPageInfo(),
		ProjectVariables: results,
	}

	return result, nil
}

func (p *projectVariables) CreateProjectVariable(ctx context.Context, variable *models.ProjectVariable) (*models.ProjectVariable, error) {
	ctx, span := tracer.Start(ctx, "db.CreateProjectVariable")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("project_variables").
		Prepared(true).
		With("project_variables",
			dialect.Insert("project_variables").Rows(
				goqu.Record{
					"id":                newResourceID(),
					"version":           initialResourceVersion,
					"created_at":        timestamp,
					"updated_at":        timestamp,
					"project_id":        variable.ProjectID,
					"key":               variable.Key,
					"value":             variable.Value,
					"environment_scope": variable.EnvironmentScope,
					"pipeline_type":     variable.PipelineType,
					"created_by":        variable.CreatedBy,
				}).Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variables.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdVariable, err := scanProjectVariable(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if pgErr := asPgError(err); pgErr != nil {
			if isForeignKeyViolation(pgErr) {
				switch pgErr.ConstraintName {
				case "fk_project_id":
					return nil, errors.New("project does not exist", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
				}
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdVariable, nil
}

func (p *projectVariables) DeleteProjectVariable(ctx context.Context, variable *models.ProjectVariable) error {
	ctx, span := tracer.Start(ctx, "db.DeleteProjectVariable")
	defer span.End()

	sql, args, err := dialect.From("project_variables").
		Prepared(true).
		With("project_variables",
			dialect.Delete("project_variables").
				Where(goqu.Ex{"id": variable.Metadata.ID, "version": variable.Metadata.Version}).
				Returning("*"),
		).Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variables.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	if _, err := scanProjectVariable(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...)); err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return ErrOptimisticLockError
		}

		return errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return nil
}

func (p *projectVariables) getProjectVariable(ctx context.Context, exp exp.Expression) (*models.ProjectVariable, error) {
	query := dialect.From(goqu.T("project_variables")).
		Prepared(true).
		Select(p.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"project_variables.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, err
	}

	project, err := scanProjectVariable(p.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, err
	}

	return project, nil
}

func (*projectVariables) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range projectVariableFieldList {
		selectFields = append(selectFields, fmt.Sprintf("project_variables.%s", field))
	}

	selectFields = append(selectFields, "projects.name")
	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanProjectVariable(row scanner) (*models.ProjectVariable, error) {
	var organizationName string
	var projectName string

	variable := &models.ProjectVariable{}

	fields := []interface{}{
		&variable.Metadata.ID,
		&variable.Metadata.CreationTimestamp,
		&variable.Metadata.LastUpdatedTimestamp,
		&variable.Metadata.Version,
		&variable.ProjectID,
		&variable.Key,
		&variable.Value,
		&variable.EnvironmentScope,
		&variable.PipelineType,
		&variable.CreatedBy,
		&projectName,
		&organizationName,
	}

	if err := row.Scan(fields...); err != nil {
		return nil, err
	}

	variable.Metadata.PRN = models.ProjectVariableResource.BuildPRN(organizationName, projectName, variable.Metadata.ID)

	return variable, nil
}
