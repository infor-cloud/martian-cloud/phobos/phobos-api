package db

//go:generate go tool mockery --name Jobs --inpackage --case underscore

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/jackc/pgx/v4"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Jobs encapsulates the logic to access jobs from the database
type Jobs interface {
	GetJobByID(ctx context.Context, id string) (*models.Job, error)
	GetJobByPRN(ctx context.Context, prn string) (*models.Job, error)
	GetJobs(ctx context.Context, input *GetJobsInput) (*JobsResult, error)
	UpdateJob(ctx context.Context, job *models.Job) (*models.Job, error)
	CreateJob(ctx context.Context, job *models.Job) (*models.Job, error)
	GetJobCountForAgent(ctx context.Context, agentID string) (int, error)
	AddJobToPipelineTask(ctx context.Context, pipelineID string, taskPath string, job *models.Job) error
}

// JobSortableField represents the fields that a job can be sorted by
type JobSortableField string

// GroupSortableField constants
const (
	JobSortableFieldCreatedAtAsc          JobSortableField = "CREATED_AT_ASC"
	JobSortableFieldCreatedAtDesc         JobSortableField = "CREATED_AT_DESC"
	JobSortableFieldUpdatedAtAsc          JobSortableField = "UPDATED_AT_ASC"
	JobSortableFieldUpdatedAtDesc         JobSortableField = "UPDATED_AT_DESC"
	JobSortableFieldCancelRequestedAtDesc JobSortableField = "CANCEL_REQUESTED_AT_DESC"
	defaultGetJobsSort                                     = JobSortableFieldCreatedAtAsc
)

func (js JobSortableField) getValue() string {
	return string(js)
}

func (js JobSortableField) getFieldDescriptor() *pagination.FieldDescriptor {
	switch js {
	case JobSortableFieldCreatedAtAsc, JobSortableFieldCreatedAtDesc:
		return &pagination.FieldDescriptor{Key: "created_at", Table: "jobs", Col: "created_at"}
	case JobSortableFieldUpdatedAtAsc, JobSortableFieldUpdatedAtDesc:
		return &pagination.FieldDescriptor{Key: "updated_at", Table: "jobs", Col: "updated_at"}
	case JobSortableFieldCancelRequestedAtDesc:
		return &pagination.FieldDescriptor{Key: "cancel_requested_at", Table: "jobs", Col: "cancel_requested_at"}
	default:
		return nil
	}
}

func (js JobSortableField) getSortDirection() pagination.SortDirection {
	if strings.HasSuffix(string(js), "_DESC") {
		return pagination.DescSort
	}
	return pagination.AscSort
}

// JobTagFilter is a filter condition for job tags
type JobTagFilter struct {
	ExcludeUntaggedJobs *bool
	TagSuperset         []string
}

// JobFilter contains the supported fields for filtering Job resources
type JobFilter struct {
	ProjectID        *string
	OrgID            *string
	PipelineID       *string
	PipelineTaskPath *string
	JobType          *models.JobType
	AgentID          *string
	TagFilter        *JobTagFilter
	JobStatuses      []models.JobStatus
	JobIDs           []string
}

// GetJobsInput is the input for listing jobs
type GetJobsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *JobSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Filter is used to filter the results
	Filter *JobFilter
}

// JobsResult contains the response data and page information
type JobsResult struct {
	PageInfo *pagination.PageInfo
	Jobs     []models.Job
}

type jobs struct {
	dbClient *Client
}

var jobFieldList = append(metadataFieldList,
	"status",
	"type",
	"project_id",
	"data",
	"cancel_requested_at",
	"agent_id",
	"agent_name",
	"agent_type",
	"queued_at",
	"pending_at",
	"running_at",
	"finished_at",
	"max_job_duration",
	"force_canceled",
	"force_canceled_by",
	"tags",
	"image",
)

// NewJobs returns an instance of the Jobs interface
func NewJobs(dbClient *Client) Jobs {
	return &jobs{dbClient: dbClient}
}

func (j *jobs) AddJobToPipelineTask(ctx context.Context, pipelineID string, taskPath string, job *models.Job) error {
	ctx, span := tracer.Start(ctx, "db.AddJobToPipelineTask")
	defer span.End()

	sql, args, err := dialect.Insert("task_jobs_relation").
		Prepared(true).
		Rows(goqu.Record{
			"pipeline_id": pipelineID,
			"task_path":   taskPath,
			"job_id":      job.Metadata.ID,
		}).ToSQL()
	if err != nil {
		return errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	_, err = j.dbClient.getConnection(ctx).Exec(ctx, sql, args...)
	return err
}

func (j *jobs) GetJobByID(ctx context.Context, id string) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "db.GetJobByID")
	defer span.End()

	return j.getJob(ctx, goqu.Ex{"jobs.id": id})
}

func (j *jobs) GetJobByPRN(ctx context.Context, prn string) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "db.GetJobByPRN")
	defer span.End()

	path, err := models.JobResource.ResourcePathFromPRN(prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	parts := strings.Split(path, "/")

	if len(parts) != 3 {
		return nil, errors.Wrap(err, "invalid PRN", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return j.getJob(ctx, goqu.Ex{
		"organizations.name": parts[0],
		"projects.name":      parts[1],
		"jobs.id":            gid.FromGlobalID(parts[2]),
	})
}

func (j *jobs) GetJobs(ctx context.Context, input *GetJobsInput) (*JobsResult, error) {
	ctx, span := tracer.Start(ctx, "db.GetJobs")
	defer span.End()

	ex := goqu.And()

	if input.Filter != nil {
		if input.Filter.ProjectID != nil {
			ex = ex.Append(goqu.I("jobs.project_id").Eq(*input.Filter.ProjectID))
		}

		if input.Filter.JobType != nil {
			ex = ex.Append(goqu.I("jobs.type").Eq(*input.Filter.JobType))
		}

		if len(input.Filter.JobStatuses) > 0 {
			ex = ex.Append(goqu.I("jobs.status").In(input.Filter.JobStatuses))
		}

		if input.Filter.JobIDs != nil {
			ex = ex.Append(goqu.I("jobs.id").In(input.Filter.JobIDs))
		}

		if input.Filter.PipelineID != nil {
			ex = ex.Append(goqu.I("task_jobs_relation.pipeline_id").Eq(*input.Filter.PipelineID))
		}

		if input.Filter.PipelineTaskPath != nil {
			ex = ex.Append(goqu.I("task_jobs_relation.task_path").Eq(*input.Filter.PipelineTaskPath))
		}

		if input.Filter.AgentID != nil {
			ex = ex.Append(goqu.I("jobs.agent_id").Eq(input.Filter.AgentID))
		}

		if input.Filter.OrgID != nil {
			ex = ex.Append(goqu.I("organizations.id").Eq(*input.Filter.OrgID))
		}

		if input.Filter.TagFilter != nil {
			if input.Filter.TagFilter.ExcludeUntaggedJobs != nil && *input.Filter.TagFilter.ExcludeUntaggedJobs {
				ex = ex.Append(goqu.L("jsonb_array_length(jobs.tags) > 0"))
			}
			if input.Filter.TagFilter.TagSuperset != nil {
				json, err := json.Marshal(input.Filter.TagFilter.TagSuperset)
				if err != nil {
					return nil, err
				}
				// This filter condition will only return jobs where the job tags are a subset of the tag superset list specified
				// in the filter
				ex = ex.Append(goqu.L(fmt.Sprintf("jobs.tags <@ '%s'", json)))
			}
		}
	}

	query := dialect.From(goqu.T("jobs")).
		Select(j.getSelectFields()...).
		LeftJoin(goqu.T("task_jobs_relation"), goqu.On(goqu.Ex{"jobs.id": goqu.I("task_jobs_relation.job_id")})).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"jobs.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(ex)

	toSort := defaultGetJobsSort
	if input.Sort != nil {
		toSort = *input.Sort
	}
	sortBy := toSort.getFieldDescriptor()
	sortDirection := toSort.getSortDirection()

	qBuilder, err := pagination.NewPaginatedQueryBuilder(
		input.PaginationOptions,
		&pagination.FieldDescriptor{Key: "id", Table: "jobs", Col: "id"},
		pagination.WithSortByField(sortBy, sortDirection),
	)

	if err != nil {
		return nil, errors.Wrap(err, "failed to build query", errors.WithSpan(span))
	}

	rows, err := qBuilder.Execute(ctx, j.dbClient.getConnection(ctx), query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	defer rows.Close()

	// Scan rows
	results := []models.Job{}
	for rows.Next() {
		item, err := scanJob(rows)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan row", errors.WithSpan(span))
		}

		results = append(results, *item)
	}

	if err := rows.Finalize(&results); err != nil {
		return nil, errors.Wrap(err, "failed to finalize rows", errors.WithSpan(span))
	}

	result := JobsResult{
		PageInfo: rows.GetPageInfo(),
		Jobs:     results,
	}

	return &result, nil
}

func (j *jobs) UpdateJob(ctx context.Context, job *models.Job) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "db.UpdateJob")
	defer span.End()

	timestamp := currentTime()

	sql, args, err := dialect.From("jobs").
		Prepared(true).
		With("jobs",
			dialect.Update("jobs").
				Set(goqu.Record{
					"version":             goqu.L("? + ?", goqu.C("version"), 1),
					"updated_at":          timestamp,
					"status":              job.GetStatus(),
					"cancel_requested_at": job.CancelRequestedTimestamp,
					"queued_at":           job.Timestamps.QueuedTimestamp,
					"pending_at":          job.Timestamps.PendingTimestamp,
					"running_at":          job.Timestamps.RunningTimestamp,
					"finished_at":         job.Timestamps.FinishedTimestamp,
					"agent_id":            job.AgentID,
					"agent_name":          job.AgentName,
					"agent_type":          job.AgentType,
					"force_canceled":      job.ForceCanceled,
					"force_canceled_by":   job.ForceCanceledBy,
				}).
				Where(goqu.Ex{"id": job.Metadata.ID, "version": job.Metadata.Version}).
				Returning("*"),
		).Select(j.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"jobs.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	updatedJob, err := scanJob(j.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))

	if err != nil {
		if err == pgx.ErrNoRows {
			tracing.RecordError(span, err, "optimistic lock error")
			return nil, ErrOptimisticLockError
		}
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return updatedJob, nil
}

func (j *jobs) CreateJob(ctx context.Context, job *models.Job) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "db.CreateJob")
	defer span.End()

	timestamp := currentTime()

	data, err := json.Marshal(job.Data)
	if err != nil {
		return nil, err
	}

	tags, err := json.Marshal(job.Tags)
	if err != nil {
		return nil, err
	}

	sql, args, err := dialect.From("jobs").
		Prepared(true).
		With("jobs",
			dialect.Insert("jobs").Rows(
				goqu.Record{
					"id":                  newResourceID(),
					"version":             initialResourceVersion,
					"created_at":          timestamp,
					"updated_at":          timestamp,
					"status":              job.GetStatus(),
					"type":                job.Type,
					"project_id":          job.ProjectID,
					"data":                data,
					"cancel_requested_at": job.CancelRequestedTimestamp,
					"queued_at":           job.Timestamps.QueuedTimestamp,
					"pending_at":          job.Timestamps.PendingTimestamp,
					"running_at":          job.Timestamps.RunningTimestamp,
					"finished_at":         job.Timestamps.FinishedTimestamp,
					"max_job_duration":    job.MaxJobDuration,
					"agent_id":            job.AgentID,
					"agent_name":          job.AgentName,
					"agent_type":          job.AgentType,
					"force_canceled":      job.ForceCanceled,
					"force_canceled_by":   job.ForceCanceledBy,
					"tags":                tags,
					"image":               job.Image,
				}).Returning("*"),
		).Select(j.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"jobs.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	createdJob, err := scanJob(j.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))

	if err != nil {
		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return createdJob, nil
}

func (j *jobs) GetJobCountForAgent(ctx context.Context, agentID string) (int, error) {
	ctx, span := tracer.Start(ctx, "db.GetJobCountForAgent")
	defer span.End()

	var count int
	query := dialect.From(goqu.T("jobs")).
		Prepared(true).
		Select(goqu.COUNT("*")).Where(goqu.Ex{
		"agent_id": agentID,
		"status":   []string{string(models.JobPending), string(models.JobRunning)},
	})

	sql, args, err := query.ToSQL()
	if err != nil {
		return 0, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	err = j.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...).Scan(&count)
	if err != nil {
		return 0, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}
	return count, nil
}

func (j *jobs) getJob(ctx context.Context, exp goqu.Ex) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "db.getJob")
	defer span.End()

	query := dialect.From(goqu.T("jobs")).
		Prepared(true).
		Select(j.getSelectFields()...).
		InnerJoin(goqu.T("projects"), goqu.On(goqu.Ex{"jobs.project_id": goqu.I("projects.id")})).
		InnerJoin(goqu.T("organizations"), goqu.On(goqu.Ex{"projects.org_id": goqu.I("organizations.id")})).
		Where(exp)

	sql, args, err := query.ToSQL()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate SQL", errors.WithSpan(span))
	}

	job, err := scanJob(j.dbClient.getConnection(ctx).QueryRow(ctx, sql, args...))

	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}

		if pgErr := asPgError(err); pgErr != nil {
			if isInvalidIDViolation(pgErr) {
				return nil, errors.Wrap(pgErr, pgErr.Message, errors.WithSpan(span), errors.WithErrorCode(errors.EInvalid))
			}
		}

		return nil, errors.Wrap(err, "failed to execute query", errors.WithSpan(span))
	}

	return job, nil
}

func (*jobs) getSelectFields() []interface{} {
	selectFields := []interface{}{}
	for _, field := range jobFieldList {
		selectFields = append(selectFields, fmt.Sprintf("jobs.%s", field))
	}

	selectFields = append(selectFields, "projects.name")
	selectFields = append(selectFields, "organizations.name")

	return selectFields
}

func scanJob(row scanner) (*models.Job, error) {
	var cancelRequestedAt sql.NullTime
	var queuedAt sql.NullTime
	var pendingAt sql.NullTime
	var runningAt sql.NullTime
	var finishedAt sql.NullTime
	var data []byte
	var status models.JobStatus
	var projectName, organizationName string

	job := &models.Job{}

	fields := []interface{}{
		&job.Metadata.ID,
		&job.Metadata.CreationTimestamp,
		&job.Metadata.LastUpdatedTimestamp,
		&job.Metadata.Version,
		&status,
		&job.Type,
		&job.ProjectID,
		&data,
		&cancelRequestedAt,
		&job.AgentID,
		&job.AgentName,
		&job.AgentType,
		&queuedAt,
		&pendingAt,
		&runningAt,
		&finishedAt,
		&job.MaxJobDuration,
		&job.ForceCanceled,
		&job.ForceCanceledBy,
		&job.Tags,
		&job.Image,
		&projectName,
		&organizationName,
	}

	err := row.Scan(fields...)

	if err != nil {
		return nil, err
	}

	switch job.Type {
	case models.JobTaskType:
		var taskData models.JobTaskData
		if err = json.Unmarshal(data, &taskData); err != nil {
			return nil, err
		}
		job.Data = &taskData
	}

	if err := job.SetStatus(status); err != nil {
		return nil, err
	}

	if cancelRequestedAt.Valid {
		job.CancelRequestedTimestamp = &cancelRequestedAt.Time
	}

	if queuedAt.Valid {
		job.Timestamps.QueuedTimestamp = &queuedAt.Time
	}

	if pendingAt.Valid {
		job.Timestamps.PendingTimestamp = &pendingAt.Time
	}

	if runningAt.Valid {
		job.Timestamps.RunningTimestamp = &runningAt.Time
	}

	if finishedAt.Valid {
		job.Timestamps.FinishedTimestamp = &finishedAt.Time
	}

	job.Metadata.PRN = models.JobResource.BuildPRN(
		organizationName,
		projectName,
		gid.ToGlobalID(gid.JobType, job.Metadata.ID),
	)

	return job, nil
}
