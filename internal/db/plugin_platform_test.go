//go:build integration

package db

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// getValue implements the sortableField interface for testing.
func (sf PluginPlatformSortableField) getValue() string {
	return string(sf)
}

func TestGetPluginPlatformByID(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	pluginPlatform, err := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
		OperatingSystem: "linux",
		Architecture:    "amd64",
		PluginVersionID: pluginVersion.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		id              string
		expectPlatform  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by id",
			id:             pluginPlatform.Metadata.ID,
			expectPlatform: true,
		},
		{
			name: "resource with id not found",
			id:   nonExistentID,
		},
		{
			name:            "get resource with invalid id will return an error",
			id:              invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPlatform, err := testClient.client.PluginPlatforms.GetPluginPlatformByID(ctx, test.id)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPlatform {
				require.NotNil(t, actualPlatform)
				assert.Equal(t, test.id, actualPlatform.Metadata.ID)
			} else {
				assert.Nil(t, actualPlatform)
			}
		})
	}
}

func TestGetPluginPlatformByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	pluginPlatform, err := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
		OperatingSystem: "linux",
		Architecture:    "amd64",
		PluginVersionID: pluginVersion.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		prn             string
		expectPlatform  bool
	}

	testCases := []testCase{
		{
			name:           "get resource by PRN",
			prn:            pluginPlatform.Metadata.PRN,
			expectPlatform: true,
		},
		{
			name: "resource with PRN not found",
			prn:  models.PluginPlatformResource.BuildPRN(organization.Name, plugin.Name, pluginVersion.SemanticVersion, "linux", "unknown"),
		},
		{
			name:            "get resource with invalid PRN will return an error",
			prn:             "prn:invalid",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPlatform, err := testClient.client.PluginPlatforms.GetPluginPlatformByPRN(ctx, test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectPlatform {
				require.NotNil(t, actualPlatform)
				assert.Equal(t, test.prn, actualPlatform.Metadata.PRN)
			} else {
				assert.Nil(t, actualPlatform)
			}
		})
	}
}

func TestGetPluginPlatforms(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion2, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "2.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	platforms := make([]*models.PluginPlatform, 10)
	for i := 0; i < len(platforms); i++ {
		platform, eErr := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
			// For the sake of testing, we'll just fabricate the OS and architecture
			// as this will still allow us to test the filtering.
			OperatingSystem: fmt.Sprintf("os-%d", i),
			Architecture:    fmt.Sprintf("arch-%d", i),
			PluginVersionID: pluginVersion.Metadata.ID,
		})
		require.NoError(t, eErr)

		platforms[i] = platform
	}

	platform, err := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
		OperatingSystem: "os-1",
		Architecture:    "arch-1",
		PluginVersionID: pluginVersion2.Metadata.ID,
		BinaryUploaded:  true,
	})
	require.NoError(t, err)

	platforms = append(platforms, platform)

	type testCase struct {
		filter            *PluginPlatformFilter
		name              string
		expectErrorCode   errors.CodeType
		expectResultCount int
	}

	testCases := []testCase{
		{
			name:              "get all resources",
			filter:            &PluginPlatformFilter{},
			expectResultCount: len(platforms),
		},
		{
			name: "get resources by plugin version id 1",
			filter: &PluginPlatformFilter{
				PluginVersionID: &pluginVersion.Metadata.ID,
			},
			expectResultCount: len(platforms) - 1,
		},
		{
			name: "get resources by plugin version id 2",
			filter: &PluginPlatformFilter{
				PluginVersionID: &pluginVersion2.Metadata.ID,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by operating system",
			filter: &PluginPlatformFilter{
				OperatingSystem: &platforms[0].OperatingSystem,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by architecture",
			filter: &PluginPlatformFilter{
				Architecture: &platforms[0].Architecture,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by binary uploaded",
			filter: &PluginPlatformFilter{
				BinaryUploaded: &platform.BinaryUploaded,
			},
			expectResultCount: 1,
		},
		{
			name: "get resources by plugin id",
			filter: &PluginPlatformFilter{
				PluginID: &plugin.Metadata.ID,
			},
			expectResultCount: len(platforms),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			result, err := testClient.client.PluginPlatforms.GetPluginPlatforms(ctx, &GetPluginPlatformsInput{
				Filter: test.filter,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, result)
			assert.Len(t, result.PluginPlatforms, test.expectResultCount)
		})
	}
}

func TestGetPlatformsWithPaginationAndSorting(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	resourceCount := 10
	for i := 0; i < resourceCount; i++ {
		_, eErr := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
			OperatingSystem: fmt.Sprintf("os-%d", i),
			Architecture:    fmt.Sprintf("arch-%d", i),
			PluginVersionID: pluginVersion.Metadata.ID,
		})
		require.NoError(t, eErr)
	}

	sortableFields := []sortableField{
		PluginPlatformSortableFieldUpdatedAtAsc,
		PluginPlatformSortableFieldUpdatedAtDesc,
	}

	testResourcePaginationAndSorting(ctx, t, resourceCount, sortableFields, func(ctx context.Context, sortByField sortableField, paginationOptions *pagination.Options) (*pagination.PageInfo, []pagination.CursorPaginatable, error) {
		sortBy := PluginPlatformSortableField(sortByField.getValue())

		result, err := testClient.client.PluginPlatforms.GetPluginPlatforms(ctx, &GetPluginPlatformsInput{
			Sort:              &sortBy,
			PaginationOptions: paginationOptions,
		})
		if err != nil {
			return nil, nil, err
		}

		resources := []pagination.CursorPaginatable{}
		for ix := range result.PluginPlatforms {
			resources = append(resources, &result.PluginPlatforms[ix])
		}

		return result.PageInfo, resources, nil
	})
}

func TestCreatePluginPlatform(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		pluginVersionID string
	}

	testCases := []testCase{
		{
			name:            "create resource",
			pluginVersionID: pluginVersion.Metadata.ID,
		},
		{
			name:            "create resource with non-existent plugin version id will return an error",
			pluginVersionID: nonExistentID,
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "duplicate resource will return an error",
			pluginVersionID: pluginVersion.Metadata.ID,
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			platform, err := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
				OperatingSystem: "linux",
				Architecture:    "amd64",
				PluginVersionID: test.pluginVersionID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, platform)
		})
	}
}

func TestUpdatePluginPlatform(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	platform, err := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
		OperatingSystem: "linux",
		Architecture:    "amd64",
		PluginVersionID: pluginVersion.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		version         int
	}

	testCases := []testCase{
		{
			name:    "update resource",
			version: platform.Metadata.Version,
		},
		{
			name:            "update resource with invalid version will return an error",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			actualPlatform, err := testClient.client.PluginPlatforms.UpdatePluginPlatform(ctx, &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID:      platform.Metadata.ID,
					Version: test.version,
				},
				BinaryUploaded: true,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, actualPlatform)
			assert.True(t, actualPlatform.BinaryUploaded)
		})
	}
}

func TestDeletePluginPlatform(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	organization, err := testClient.client.Organizations.CreateOrganization(ctx, &models.Organization{
		Name: "neutron",
	})
	require.NoError(t, err)

	plugin, err := testClient.client.Plugins.CreatePlugin(ctx, &models.Plugin{
		Name:           "gitlab",
		OrganizationID: organization.Metadata.ID,
	})
	require.NoError(t, err)

	pluginVersion, err := testClient.client.PluginVersions.CreatePluginVersion(ctx, &models.PluginVersion{
		SemanticVersion: "1.0.0",
		PluginID:        plugin.Metadata.ID,
	})
	require.NoError(t, err)

	platform, err := testClient.client.PluginPlatforms.CreatePluginPlatform(ctx, &models.PluginPlatform{
		OperatingSystem: "linux",
		Architecture:    "amd64",
		PluginVersionID: pluginVersion.Metadata.ID,
	})
	require.NoError(t, err)

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		version         int
	}

	testCases := []testCase{
		{
			name:    "delete resource",
			version: platform.Metadata.Version,
		},
		{
			name:            "delete resource with invalid version will return an error",
			version:         -1,
			expectErrorCode: errors.EOptimisticLock,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			err := testClient.client.PluginPlatforms.DeletePluginPlatform(ctx, &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID:      platform.Metadata.ID,
					Version: test.version,
				},
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
