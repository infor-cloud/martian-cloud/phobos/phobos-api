//go:build integration

package db

import (
	"context"
	"fmt"
	"sort"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// agentInfo aids convenience in accessing the information
// TestGetAgents needs about the warmup objects.
type agentInfo struct {
	updateTime time.Time
	id         string
	name       string
}

// agentInfoIDSlice makes a slice of agentInfo sortable by ID string
type agentInfoIDSlice []agentInfo

// agentInfoUpdateSlice makes a slice of agentInfo sortable by last updated time
type agentInfoUpdateSlice []agentInfo

// warmupAgents holds the inputs to and outputs from createWarmupAgents.
type warmupAgents struct {
	organizations []models.Organization
	agents        []models.Agent
}

func TestGetAgentByID(t *testing.T) {

	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupAgents(ctx, testClient, warmupAgents{
		organizations: standardWarmupOrganizationsForAgents,
		agents:        standardWarmupAgents,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		expectAgent     *models.Agent
		name            string
		searchID        string
	}

	/*
		test case template

		{
			name            string
			searchID        string
			expectErrorCode errors.CodeType
			expectAgent     *models.Agent
		}
	*/

	testCases := []testCase{
		{
			name:        "get agent by ID",
			searchID:    warmupItems.agents[0].Metadata.ID,
			expectAgent: &warmupItems.agents[0],
		},

		{
			name:     "returns nil because agent does not exist",
			searchID: nonExistentID,
		},

		{
			name:            "returns an error because the agent ID is invalid",
			searchID:        invalidID,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			actualAgent, err := testClient.client.Agents.GetAgentByID(ctx, test.searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectAgent != nil {
				require.NotNil(t, actualAgent)
				assert.Equal(t, test.expectAgent, actualAgent)
			} else {
				assert.Nil(t, actualAgent)
			}
		})
	}
}

func TestGetAgentByPRN(t *testing.T) {
	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupAgents(ctx, testClient, warmupAgents{
		organizations: standardWarmupOrganizationsForAgents,
		agents:        standardWarmupAgents,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		expectAgent     *models.Agent
		name            string
		searchPRN       string
	}

	/*
		test case template

		{
			name            string
			searchPRN       string
			expectErrorCode errors.CodeType
			expectAgent     *models.Agent
		}
	*/

	testCases := []testCase{}

	for ix := range warmupItems.agents {
		testCases = append(testCases, testCase{
			name:        "get agent by PRN" + warmupItems.agents[ix].Name,
			searchPRN:   warmupItems.agents[ix].Metadata.PRN,
			expectAgent: &warmupItems.agents[ix],
		})
	}

	testCases = append(testCases, []testCase{
		{
			name:      "returns nil because agent does not exist",
			searchPRN: models.AgentResource.BuildPRN("non-existent", "non-existent"),
		},
		{
			name:            "returns an error because the agent PRN is invalid",
			searchPRN:       "invalid",
			expectErrorCode: errors.EInvalid,
		},
	}...)

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			actualAgent, err := testClient.client.Agents.GetAgentByPRN(ctx, test.searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.Nil(t, err)

			if test.expectAgent != nil {
				require.NotNil(t, actualAgent)
				assert.Equal(t, test.expectAgent, actualAgent)
			} else {
				assert.Nil(t, actualAgent)
			}
		})
	}
}

func TestGetAgentsWithPagination(t *testing.T) {

	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupAgents(ctx, testClient, warmupAgents{
		organizations: standardWarmupOrganizationsForAgents,
		agents:        standardWarmupAgents,
	})
	require.Nil(t, err)

	// Query for first page
	middleIndex := len(warmupItems.agents) / 2
	page1, err := testClient.client.Agents.GetAgents(ctx, &GetAgentsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(int32(middleIndex)),
		},
	})
	require.Nil(t, err)

	assert.Equal(t, middleIndex, len(page1.Agents))
	assert.True(t, page1.PageInfo.HasNextPage)
	assert.False(t, page1.PageInfo.HasPreviousPage)

	cursor, err := page1.PageInfo.Cursor(&page1.Agents[len(page1.Agents)-1])
	require.Nil(t, err)

	remaining := len(warmupItems.agents) - middleIndex
	page2, err := testClient.client.Agents.GetAgents(ctx, &GetAgentsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(int32(remaining)),
			After: cursor,
		},
	})
	require.Nil(t, err)

	assert.Equal(t, remaining, len(page2.Agents))
	assert.True(t, page2.PageInfo.HasPreviousPage)
	assert.False(t, page2.PageInfo.HasNextPage)
}

func TestGetAgents(t *testing.T) {

	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupAgents(ctx, testClient, warmupAgents{
		organizations: standardWarmupOrganizationsForAgents,
		agents:        standardWarmupAgents,
	})
	require.Nil(t, err)

	allAgentInfos := agentInfoFromAgents(warmupItems.agents)

	// Sort by agent IDs.
	sort.Sort(agentInfoIDSlice(allAgentInfos))
	allAgentIDs := agentIDsFromAgentInfos(allAgentInfos)

	// Sort by last update times.
	sort.Sort(agentInfoUpdateSlice(allAgentInfos))
	allAgentIDsByTime := agentIDsFromAgentInfos(allAgentInfos)
	reverseAgentIDsByTime := reverseStringSlice(allAgentIDsByTime)

	type testCase struct {
		input           *GetAgentsInput
		expectErrorCode errors.CodeType
		name            string
		expectAgentIDs  []string
	}

	/*
		test case template

		{
			name            string
			input           *GetAgentsInput
			expectErrorCode errors.CodeType
			expectAgentIDs  []string
		 }
	*/

	testCases := []testCase{
		{
			name: "non-nil but mostly empty input",
			input: &GetAgentsInput{
				Sort:              nil,
				PaginationOptions: nil,
				Filter:            nil,
			},
			expectAgentIDs: allAgentIDs,
		},

		{
			name: "populated sort and pagination, nil filter",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(100),
				},
				Filter: nil,
			},
			expectAgentIDs: allAgentIDsByTime,
		},

		{
			name: "sort in ascending order of time of last update",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
			},
			expectAgentIDs: allAgentIDsByTime,
		},

		{
			name: "sort in descending order of time of last update",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtDesc),
			},
			expectAgentIDs: reverseAgentIDsByTime,
		},

		{
			name: "pagination, first one and last two, expect error",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
					Last:  ptr.Int32(2),
				},
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "filter, organization ID, positive",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				Filter: &AgentFilter{
					OrganizationID: warmupItems.agents[0].OrganizationID,
				},
			},
			expectAgentIDs: allAgentIDsByTime[0:1],
		},

		{
			name: "filter, organization ID, non-existent",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				Filter: &AgentFilter{
					OrganizationID: ptr.String(nonExistentID),
				},
			},
			expectAgentIDs: []string{},
		},

		{
			name: "filter, organization ID, invalid",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				Filter: &AgentFilter{
					OrganizationID: ptr.String(invalidID),
				},
			},
			expectErrorCode: errors.EInternal,
			expectAgentIDs:  []string{},
		},

		{
			name: "filter, agent IDs, positive",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				Filter: &AgentFilter{
					AgentIDs: allAgentIDsByTime,
				},
			},
			expectAgentIDs: allAgentIDsByTime,
		},

		{
			name: "filter, agent IDs, non-existent",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				Filter: &AgentFilter{
					AgentIDs: []string{nonExistentID},
				},
			},
			expectAgentIDs: []string{},
		},

		{
			name: "filter, agent IDs, invalid ID",
			input: &GetAgentsInput{
				Sort: ptrAgentSortableField(AgentSortableFieldUpdatedAtAsc),
				Filter: &AgentFilter{
					AgentIDs: []string{invalidID},
				},
			},
			expectErrorCode: errors.EInternal,
			expectAgentIDs:  []string{},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			agentsResult, err := testClient.client.Agents.GetAgents(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err == nil {
				// Never returns nil if error is nil.
				require.NotNil(t, agentsResult.PageInfo)

				agents := agentsResult.Agents

				// Check the agents result by comparing a list of the agent IDs.
				actualAgentIDs := []string{}
				for _, agent := range agents {
					actualAgentIDs = append(actualAgentIDs, agent.Metadata.ID)
				}

				// If no sort direction was specified, sort the results here for repeatability.
				if test.input.Sort == nil {
					sort.Strings(actualAgentIDs)
				}

				assert.Equal(t, len(test.expectAgentIDs), len(actualAgentIDs))
				assert.Equal(t, test.expectAgentIDs, actualAgentIDs)
			}
		})
	}
}

func TestCreateAgent(t *testing.T) {

	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupAgents(ctx, testClient, warmupAgents{
		organizations: standardWarmupOrganizationsForAgents,
	})
	require.Nil(t, err)

	type testCase struct {
		toCreate        *models.Agent
		expectCreated   *models.Agent
		expectErrorCode errors.CodeType
		name            string
	}

	/*
		test case template

		{
			name            string
			toCreate        *models.Agent
			expectErrorCode errors.CodeType
			expectCreated   *models.Agent
		}
	*/

	now := time.Now()
	testCases := []testCase{
		{
			name: "positive",
			toCreate: &models.Agent{
				Type:           models.OrganizationAgent,
				Name:           "agent-create-test",
				OrganizationID: &warmupItems.organizations[0].Metadata.ID,
				CreatedBy:      "TestCreateAgent",
			},
			expectCreated: &models.Agent{
				Metadata: models.ResourceMetadata{
					Version:           initialResourceVersion,
					CreationTimestamp: &now,
					PRN:               models.AgentResource.BuildPRN(warmupItems.organizations[0].Name, "agent-create-test"),
				},
				Type:           models.OrganizationAgent,
				Name:           "agent-create-test",
				OrganizationID: &warmupItems.organizations[0].Metadata.ID,
				CreatedBy:      "TestCreateAgent",
			},
		},

		{
			name: "duplicate organization ID and agent name",
			toCreate: &models.Agent{
				Type:           models.OrganizationAgent,
				Name:           "agent-create-test",
				OrganizationID: &warmupItems.organizations[0].Metadata.ID,
			},
			expectErrorCode: errors.EConflict,
		},

		{
			name: "negative, non-existent organization ID",
			toCreate: &models.Agent{
				Type:           models.OrganizationAgent,
				Name:           "agent-create-test-non-existent-organization-id",
				OrganizationID: ptr.String(nonExistentID),
			},
			expectErrorCode: errors.EInternal,
		},

		{
			name: "negative, invalid organization ID",
			toCreate: &models.Agent{
				Type:           models.OrganizationAgent,
				Name:           "agent-create-test-invalid-organization-id",
				OrganizationID: ptr.String(invalidID),
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			actualCreated, err := testClient.client.Agents.CreateAgent(ctx, test.toCreate)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectCreated != nil {
				require.NotNil(t, actualCreated)

				// The creation process must set the creation and last updated timestamps
				// between when the test case was created and when it the result is checked.
				whenCreated := test.expectCreated.Metadata.CreationTimestamp
				now := time.Now()

				compareAgents(t, test.expectCreated, actualCreated, false, &timeBounds{
					createLow:  whenCreated,
					createHigh: &now,
					updateLow:  whenCreated,
					updateHigh: &now,
				})
			} else {
				assert.Nil(t, actualCreated)
			}
		})
	}
}

func TestUpdateAgent(t *testing.T) {

	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupAgents(ctx, testClient, warmupAgents{
		organizations: standardWarmupOrganizationsForAgents,
		agents:        standardWarmupAgents,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		toUpdate        *models.Agent
		expectUpdated   *models.Agent
		name            string
	}

	/*
		test case template

		{
			name            string
			toUpdate        *models.Agent
			expectErrorCode errors.CodeType
			expectUpdated   *models.Agent
		}
	*/

	// Looks up by ID and version.  Also requires organization ID.
	positiveAgent := warmupItems.agents[0]
	positiveOrganization := warmupItems.organizations[0]

	now := time.Now()
	testCases := []testCase{

		{
			name: "positive",
			toUpdate: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID:      positiveAgent.Metadata.ID,
					Version: initialResourceVersion,
				},
				Type:           models.OrganizationAgent,
				Name:           positiveAgent.Name,
				Description:    "Updated description",
				OrganizationID: &positiveOrganization.Metadata.ID,
			},
			expectUpdated: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID:                   positiveAgent.Metadata.ID,
					Version:              initialResourceVersion + 1,
					CreationTimestamp:    positiveAgent.Metadata.CreationTimestamp,
					LastUpdatedTimestamp: &now,
					PRN:                  positiveAgent.Metadata.PRN,
				},
				Type:           models.OrganizationAgent,
				Name:           positiveAgent.Name,
				Description:    "Updated description",
				OrganizationID: positiveAgent.OrganizationID,
				CreatedBy:      positiveAgent.CreatedBy,
			},
		},

		{
			name: "negative, non-existent agent ID",
			toUpdate: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: initialResourceVersion,
				},
			},
			expectErrorCode: errors.EOptimisticLock,
		},

		{
			name: "defective-ID",
			toUpdate: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: initialResourceVersion,
				},
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			actualAgent, err := testClient.client.Agents.UpdateAgent(ctx, test.toUpdate)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.expectUpdated != nil {
				// The creation process must set the creation and last updated timestamps
				// between when the test case was created and when it the result is checked.
				whenCreated := test.expectUpdated.Metadata.CreationTimestamp
				now := currentTime()

				require.NotNil(t, actualAgent)
				compareAgents(t, test.expectUpdated, actualAgent, false, &timeBounds{
					createLow:  whenCreated,
					createHigh: &now,
					updateLow:  whenCreated,
					updateHigh: &now,
				})
			} else {
				assert.Nil(t, actualAgent)
			}
		})
	}
}

func TestDeleteAgent(t *testing.T) {

	ctx := context.Background()
	testClient := newTestClient(ctx, t)
	defer testClient.close(ctx)

	warmupItems, err := createWarmupAgents(ctx, testClient, warmupAgents{
		organizations: standardWarmupOrganizationsForAgents,
		agents:        standardWarmupAgents,
	})
	require.Nil(t, err)

	type testCase struct {
		expectErrorCode errors.CodeType
		toDelete        *models.Agent
		name            string
	}

	/*
		test case template

		{
			name            string
			toDelete        *models.Agent
			expectErrorCode errors.CodeType
		}
	*/

	testCases := []testCase{

		{
			name: "positive",
			toDelete: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID:      warmupItems.agents[0].Metadata.ID,
					Version: initialResourceVersion,
				},
			},
		},

		{
			name: "negative, non-existent agent ID",
			toDelete: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID:      nonExistentID,
					Version: initialResourceVersion,
				},
			},
			expectErrorCode: errors.EOptimisticLock,
		},

		{
			name: "defective-ID",
			toDelete: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID:      invalidID,
					Version: initialResourceVersion,
				},
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {

			err := testClient.client.Agents.DeleteAgent(ctx, test.toDelete)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
		})
	}
}

//////////////////////////////////////////////////////////////////////////////

// Common utility structures and functions:

// Standard warmup organization(s) for tests in this agent:
var standardWarmupOrganizationsForAgents = []models.Organization{
	// Top-level organizations:
	{
		Name:        "top-level-organization-0-for-agents",
		Description: "top level organization 0 for testing agent functions",
		CreatedBy:   "someone-g0",
	},
	{
		Name:        "top-level-organization-1-for-agents",
		Description: "top level organization 1 for testing agent functions",
		CreatedBy:   "someone-g1",
	},
	{
		Name:        "top-level-organization-2-for-agents",
		Description: "top level organization 2 for testing agent functions",
		CreatedBy:   "someone-g2",
	},
}

// Standard warmup agents for tests in this agent:
// The ID fields will be replaced by the real IDs during the create function.
var standardWarmupAgents = []models.Agent{
	{
		// This one is public.
		Type:           models.OrganizationAgent,
		Name:           "1-agent-0",
		OrganizationID: ptr.String("top-level-organization-0-for-agents"),
		CreatedBy:      "someone",
	},
	{
		Type:           models.OrganizationAgent,
		Name:           "1-agent-1",
		OrganizationID: ptr.String("top-level-organization-1-for-agents"),
		CreatedBy:      "someone",
	},
	{
		Type:      models.SharedAgent,
		Name:      "6-agent-shared",
		CreatedBy: "someone",
	},
}

// createWarmupAgents creates some warmup agents for a test
// The warmup agents to create can be standard or otherwise.
func createWarmupAgents(ctx context.Context, testClient *testClient,
	input warmupAgents) (*warmupAgents, error) {

	// It is necessary to create several organizations in order to provide the necessary IDs for the agents.

	resultOrganizations, organizationName2ID, err := createInitialOrganizations(ctx, testClient, input.organizations)
	if err != nil {
		return nil, err
	}

	resultAgents, _, err := createInitialAgents(ctx, testClient, organizationName2ID, input.agents)
	if err != nil {
		return nil, err
	}

	return &warmupAgents{
		organizations: resultOrganizations,
		agents:        resultAgents,
	}, nil
}

// createInitialAgents creates some warmup agents for a test.
func createInitialAgents(ctx context.Context, testClient *testClient,
	organizationName2ID map[string]string, toCreate []models.Agent) (
	[]models.Agent, map[string]string, error) {
	result := []models.Agent{}
	name2ID := make(map[string]string)

	for _, input := range toCreate {
		if input.OrganizationID != nil {
			organizationID, ok := organizationName2ID[*input.OrganizationID]
			if !ok {
				return nil, nil, fmt.Errorf("createInitialAgents failed to look up organization name: %s", *input.OrganizationID)
			}
			input.OrganizationID = &organizationID
		}

		created, err := testClient.client.Agents.CreateAgent(ctx, &input)
		if err != nil {
			return nil, nil, err
		}

		result = append(result, *created)
		name2ID[input.Name] = created.Metadata.ID
	}

	return result, name2ID, nil
}

func ptrAgentSortableField(arg AgentSortableField) *AgentSortableField {
	return &arg
}

func (wis agentInfoIDSlice) Len() int {
	return len(wis)
}

func (wis agentInfoIDSlice) Swap(i, j int) {
	wis[i], wis[j] = wis[j], wis[i]
}

func (wis agentInfoIDSlice) Less(i, j int) bool {
	return wis[i].id < wis[j].id
}

func (wis agentInfoUpdateSlice) Len() int {
	return len(wis)
}

func (wis agentInfoUpdateSlice) Swap(i, j int) {
	wis[i], wis[j] = wis[j], wis[i]
}

func (wis agentInfoUpdateSlice) Less(i, j int) bool {
	return wis[i].updateTime.Before(wis[j].updateTime)
}

// agentInfoFromAgents returns a slice of agentInfo, not necessarily sorted in any order.
func agentInfoFromAgents(agents []models.Agent) []agentInfo {
	result := []agentInfo{}

	for _, tp := range agents {
		result = append(result, agentInfo{
			id:         tp.Metadata.ID,
			name:       tp.Name,
			updateTime: *tp.Metadata.LastUpdatedTimestamp,
		})
	}

	return result
}

// agentIDsFromAgentInfos preserves order
func agentIDsFromAgentInfos(agentInfos []agentInfo) []string {
	result := []string{}
	for _, agentInfo := range agentInfos {
		result = append(result, agentInfo.id)
	}
	return result
}

// compareAgents compares two agent objects, including bounds for creation and updated times.
// If times is nil, it compares the exact metadata timestamps.
func compareAgents(t *testing.T, expected, actual *models.Agent,
	checkID bool, times *timeBounds) {

	assert.Equal(t, expected.Name, actual.Name)
	assert.Equal(t, expected.Type, actual.Type)
	assert.Equal(t, expected.OrganizationID, actual.OrganizationID)
	assert.Equal(t, expected.Description, actual.Description)
	assert.Equal(t, expected.CreatedBy, actual.CreatedBy)
	assert.Equal(t, expected.Disabled, actual.Disabled)

	if checkID {
		assert.Equal(t, expected.Metadata.ID, actual.Metadata.ID)
	}
	assert.Equal(t, expected.Metadata.Version, actual.Metadata.Version)
	assert.Equal(t, expected.Metadata.PRN, actual.Metadata.PRN)

	// Compare timestamps.
	if times != nil {
		compareTime(t, times.createLow, times.createHigh, actual.Metadata.CreationTimestamp)
		compareTime(t, times.updateLow, times.updateHigh, actual.Metadata.LastUpdatedTimestamp)
	} else {
		assert.Equal(t, expected.Metadata.CreationTimestamp, actual.Metadata.CreationTimestamp)
		assert.Equal(t, expected.Metadata.LastUpdatedTimestamp, actual.Metadata.LastUpdatedTimestamp)
	}
}
