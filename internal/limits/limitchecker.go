// Package limits package
package limits

//go:generate go tool mockery --name LimitChecker --inpackage --case underscore

import (
	"context"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

const (
	// ResourceLimitTimePeriod is the time period used for time-based resource limits.
	// Only resources created within the last time period will account towards the limit.
	ResourceLimitTimePeriod = 24 * time.Hour
)

// ResourceLimitName is an enum for the names that will be used as keys when doing the checks.
type ResourceLimitName string

// ResourceLimitName constants
const (
	ResourceLimitTotalRoles                                     ResourceLimitName = "TotalRoles"
	ResourceLimitPipelineTemplatesPerProjectPerTimePeriod       ResourceLimitName = "PipelineTemplatesPerProjectPerTimePeriod"
	ResourceLimitPipelinesPerProjectPerTimePeriod               ResourceLimitName = "PipelinesPerProjectPerTimePeriod"
	ResourceLimitAgentsPerOrganization                          ResourceLimitName = "AgentsPerOrganization"
	ResourceLimitEnvironmentsPerProject                         ResourceLimitName = "EnvironmentsPerProject"
	ResourceLimitServiceAccountsPerOrganization                 ResourceLimitName = "ServiceAccountsPerOrganization"
	ResourceLimitServiceAccountsPerProject                      ResourceLimitName = "ServiceAccountsPerProject"
	ResourceLimitLifecycleTemplatesPerOrganizationPerTimePeriod ResourceLimitName = "LifecycleTemplatesPerOrganizationPerTimePeriod"
	ResourceLimitLifecycleTemplatesPerProjectPerTimePeriod      ResourceLimitName = "LifecycleTemplatesPerProjectPerTimePeriod"
	ResourceLimitReleaseLifecyclesPerOrganization               ResourceLimitName = "ReleaseLifecyclesPerOrganization"
	ResourceLimitReleaseLifecyclesPerProject                    ResourceLimitName = "ReleaseLifecyclesPerProject"
	ResourceLimitApprovalRulesPerOrganization                   ResourceLimitName = "ApprovalRulesPerOrganization"
	ResourceLimitApprovalRulesPerProject                        ResourceLimitName = "ApprovalRulesPerProject"
	ResourceLimitVariableSetsPerProject                         ResourceLimitName = "VariableSetsPerProject"
	ResourceLimitReleasesPerProjectPerTimePeriod                ResourceLimitName = "ReleasesPerProjectPerTimePeriod"
	ResourceLimitSessionsPerAgent                               ResourceLimitName = "SessionsPerAgent"
	ResourceLimitThreadsPerPipeline                             ResourceLimitName = "ThreadsPerPipeline"
	ResourceLimitThreadsPerRelease                              ResourceLimitName = "ThreadsPerRelease"
	ResourceLimitCommentsPerThread                              ResourceLimitName = "CommentsPerThread"
	ResourceLimitPluginsPerOrganization                         ResourceLimitName = "PluginsPerOrganization"
	ResourceLimitVersionsPerPlugin                              ResourceLimitName = "VersionsPerPlugin"
	ResourceLimitPlatformsPerPluginVersion                      ResourceLimitName = "PlatformsPerPluginVersion"
	ResourceLimitVCSProvidersPerOrganization                    ResourceLimitName = "VCSProvidersPerOrganization"     // Only applies to Org VCS providers type.
	ResourceLimitVCSProvidersPerProject                         ResourceLimitName = "VCSProvidersPerProject"          // Only applies to Project VCS providers type.
	ResourceLimitEnvironmentRulesPerOrganization                ResourceLimitName = "EnvironmentRulesPerOrganization" // Only applies to Org environment protection rules.
)

// LimitChecker implements functionality related to resource limits.
type LimitChecker interface {
	CheckLimit(ctx context.Context, name ResourceLimitName, toCheck int32) error
}

type limitChecker struct {
	dbClient *db.Client
}

// NewLimitChecker creates an instance of LimitChecker
func NewLimitChecker(
	dbClient *db.Client,
) LimitChecker {
	return &limitChecker{
		dbClient: dbClient,
	}
}

// CheckLimit returns an error or nil based on a limit check.
// The returned error is already wrapped if appropriate.
// The toCheck argument is int32 rather than int, because most calls come from something.PageInfo.TotalCount.
func (c *limitChecker) CheckLimit(ctx context.Context, name ResourceLimitName, toCheck int32) error {
	limit, err := c.dbClient.ResourceLimits.GetResourceLimit(ctx, string(name))
	if err != nil {
		return err
	}
	if limit == nil {
		return errors.New("invalid resource limit name: %s", string(name), errors.WithErrorCode(errors.EInvalid))
	}

	if int(toCheck) > limit.Value {
		return errors.New("for limit %s: value %d exceeds limit of %d", name, toCheck, limit.Value, errors.WithErrorCode(errors.EInvalid))
	}

	// A valid limit value was found, and there is no violation.
	return nil
}
