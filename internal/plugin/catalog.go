// Package plugin defines a plugin store the API uses
// including, an object store, rate limiting, etc.
package plugin

import (
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/plugin/email"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/go-limiter"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/jws"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/objectstore"
)

// Catalog contains the available plugins.
type Catalog struct {
	ObjectStore           objectstore.ObjectStore
	JWSProvider           jws.Provider
	GraphqlRateLimitStore limiter.Store
	HTTPRateLimitStore    limiter.Store
	EmailProvider         email.Provider
}
