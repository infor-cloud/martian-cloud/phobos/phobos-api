package auth

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	terrors "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"go.opentelemetry.io/otel/attribute"
)

// JobCaller represents a job subject
type JobCaller struct {
	dbClient   *db.Client
	JobID      string
	ProjectID  string
	authorized bool
}

// GetSubject returns the subject identifier for this caller
func (j *JobCaller) GetSubject() string {
	return j.JobID
}

// IsAdmin returns true if the caller is an admin
func (j *JobCaller) IsAdmin() bool {
	return false
}

// Authorized marks the caller as authorized
func (j *JobCaller) Authorized() {
	j.authorized = true
}

// UnauthorizedError returns the unauthorized error for this specific caller type
func (j *JobCaller) UnauthorizedError(ctx context.Context, hasViewerAccess bool) error {
	// Get project path
	project, err := j.dbClient.Projects.GetProjectByID(ctx, j.ProjectID)
	if err != nil {
		return err
	}

	projectPRN := "unknown project"
	if project != nil {
		projectPRN = project.Metadata.PRN
	}

	forbiddenMsg := fmt.Sprintf(
		"job in project %s is not authorized to perform the requested operation.",
		projectPRN,
	)

	// If subject has at least viewer permissions then return 403, if not, return 404
	if hasViewerAccess {
		return terrors.New(
			forbiddenMsg,
			terrors.WithErrorCode(terrors.EForbidden),
		)
	}

	return terrors.New(
		"either the requested resource does not exist or the %s",
		forbiddenMsg,
		terrors.WithErrorCode(terrors.ENotFound),
	)
}

// RequirePermission will return an error if the caller doesn't have the specified permissions
func (j *JobCaller) RequirePermission(ctx context.Context, perm models.Permission, checks ...func(*constraints)) error {
	ctx, span := tracer.Start(ctx, "auth.RequirePermission")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "job"))

	if j.authorized {
		return nil
	}

	handlerFunc, ok := j.getPermissionHandler(perm)
	if !ok {
		span.SetAttributes(attribute.Bool("authorized", false))
		return j.UnauthorizedError(ctx, false)
	}

	result := handlerFunc(ctx, &perm, getConstraints(checks...))
	span.SetAttributes(attribute.Bool("authorized", (result == nil)))
	return result
}

// RequireAccessToInheritableResource will return an error if caller doesn't have permissions to inherited resources.
func (j *JobCaller) RequireAccessToInheritableResource(ctx context.Context, _ models.ResourceType, checks ...func(*constraints)) error {
	ctx, span := tracer.Start(ctx, "auth.RequireAccessToInheritableResource")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "job"))

	if j.authorized {
		return nil
	}

	c := getConstraints(checks...)

	if len(c.organizationIDs) > 0 {
		project, err := j.dbClient.Projects.GetProjectByID(ctx, j.ProjectID)
		if err != nil {
			span.SetAttributes(attribute.Bool("authorized", false))
			return err
		}

		if project == nil {
			span.SetAttributes(attribute.Bool("authorized", false))
			return j.UnauthorizedError(ctx, false)
		}

		// Job caller must only access resources under the same
		// organization as the project it's associated with.
		for _, orgID := range c.organizationIDs {
			if orgID != project.OrgID {
				span.SetAttributes(attribute.Bool("authorized", false))
				return j.UnauthorizedError(ctx, false)
			}
		}

		span.SetAttributes(attribute.Bool("authorized", true))
		return nil
	}

	span.SetAttributes(attribute.Bool("authorized", false))
	return errMissingConstraints
}

// handleViewProjectPermission delegates the appropriate project check based on the Constraints.
func (j *JobCaller) handleViewProjectPermission(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.projectID == nil {
		return errMissingConstraints
	}

	if j.ProjectID != *checks.projectID {
		return j.UnauthorizedError(ctx, false)
	}

	return nil
}

func (j *JobCaller) handleJobPermission(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.jobID == nil {
		return errMissingConstraints
	}

	if j.JobID == *checks.jobID {
		return nil
	}

	return j.UnauthorizedError(ctx, false)
}

func (j *JobCaller) handleViewPipelinePermission(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.pipelineID == nil {
		return errMissingConstraints
	}

	// Get job config data to verify this job has permission to updated the specified pipeline
	job, err := j.dbClient.Jobs.GetJobByID(ctx, j.JobID)
	if err != nil {
		return err
	}

	if job == nil {
		return j.UnauthorizedError(ctx, false)
	}

	switch data := job.Data.(type) {
	case *models.JobTaskData:
		if data.PipelineID != *checks.pipelineID {
			return j.UnauthorizedError(ctx, false)
		}
	default:
		return j.UnauthorizedError(ctx, false)
	}

	return nil
}

func (j *JobCaller) handleUpdatePipelinePermission(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.pipelineID == nil {
		return errMissingConstraints
	}

	// Get job config data to verify this job has permission to updated the specified pipeline
	job, err := j.dbClient.Jobs.GetJobByID(ctx, j.JobID)
	if err != nil {
		return err
	}

	if job == nil {
		return j.UnauthorizedError(ctx, false)
	}

	switch data := job.Data.(type) {
	case *models.JobTaskData:
		if data.PipelineID != *checks.pipelineID {
			return j.UnauthorizedError(ctx, false)
		}
		if checks.pipelineTaskPath != nil && data.TaskPath != *checks.pipelineTaskPath {
			return j.UnauthorizedError(ctx, false)
		}
		if checks.pipelineActionPath != nil && !strings.HasPrefix(*checks.pipelineActionPath, data.TaskPath) {
			return j.UnauthorizedError(ctx, false)
		}
	default:
		return j.UnauthorizedError(ctx, false)
	}

	return nil
}

// getPermissionHandler returns a permissionTypeHandler for a given permission.
func (j *JobCaller) getPermissionHandler(perm models.Permission) (permissionTypeHandler, bool) {
	handlerMap := map[models.Permission]permissionTypeHandler{
		models.ViewPipeline:         j.handleViewPipelinePermission,
		models.ViewPipelineTemplate: j.handleViewProjectPermission,
		models.ViewJob:              j.handleJobPermission,
		models.UpdateJobState:       j.handleJobPermission,
		models.UpdatePipelineState:  j.handleUpdatePipelinePermission,
		models.ViewVariable:         j.handleViewPipelinePermission,
		models.ViewVCSProvider:      j.handleViewProjectPermission,
	}

	handlerFunc, ok := handlerMap[perm]
	return handlerFunc, ok
}
