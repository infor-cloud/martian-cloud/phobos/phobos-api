package auth

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

func TestUserCaller_GetSubject(t *testing.T) {
	caller := UserCaller{User: &models.User{Email: "user@email"}}
	assert.Equal(t, "user@email", caller.GetSubject())
}

func TestUserCaller_IsAdmin(t *testing.T) {
	caller := UserCaller{User: &models.User{Metadata: models.ResourceMetadata{ID: "user1"}}}
	assert.False(t, caller.IsAdmin())

	caller.User.Admin = true
	assert.True(t, caller.IsAdmin())
}

func TestUserCaller_RequirePermissions(t *testing.T) {
	teamID := "team1"
	organizationID := "organization-1"

	caller := UserCaller{User: &models.User{Metadata: models.ResourceMetadata{ID: "user1"}, Email: "user@email"}}
	ctx := WithCaller(context.Background(), &caller)

	testCases := []struct {
		name            string
		expectErrorCode errors.CodeType
		teamMember      *models.TeamMember
		perm            models.Permission
		constraints     []func(*constraints)
		isAdmin         bool
		withAuthorizer  bool
	}{
		{
			name:           "access is granted by the authorizer",
			perm:           models.ViewOrganization,
			constraints:    []func(*constraints){WithOrganizationID(organizationID)},
			withAuthorizer: true,
		},
		{
			name:            "access denied by the authorizer because a permission is not satisfied",
			perm:            models.DeleteMembership,
			constraints:     []func(*constraints){WithOrganizationID(organizationID)},
			expectErrorCode: errors.ENotFound,
			withAuthorizer:  true,
		},
		{
			name:    "permissions are only granted since user is admin",
			perm:    models.CreateTeam,
			isAdmin: true,
		},
		{
			name:            "access forbidden because user must be an admin",
			perm:            models.CreateTeam,
			constraints:     []func(*constraints){WithTeamID("team-1")},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:        "team update allowed since user is an admin",
			perm:        models.UpdateTeam,
			constraints: []func(*constraints){WithTeamID(teamID)},
			isAdmin:     true,
		},
		{
			name:            "access denied because user is not an admin or a team maintainer",
			teamMember:      &models.TeamMember{IsMaintainer: false},
			perm:            models.UpdateTeam,
			constraints:     []func(*constraints){WithTeamID(teamID)},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "access denied because team member not found",
			perm:            models.UpdateTeam,
			constraints:     []func(*constraints){WithTeamID(teamID)},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "access denied because user is not a maintainer",
			teamMember:      &models.TeamMember{IsMaintainer: false},
			perm:            models.UpdateTeam,
			constraints:     []func(*constraints){WithTeamID(teamID)},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "access denied because required constraints are not specified",
			perm:            models.ViewOrganization,
			expectErrorCode: errors.EInternal, // missing constraints
			withAuthorizer:  true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockAuthorizer := NewMockAuthorizer(t)
			mockTeamMembers := db.NewMockTeamMembers(t)

			if test.perm == models.UpdateTeam && !test.isAdmin {
				mockTeamMembers.On("GetTeamMember", mock.Anything, caller.User.Metadata.ID, teamID).Return(test.teamMember, nil)
			}

			if test.withAuthorizer {
				mockAuthorizer.On("RequirePermissions", mock.Anything, []models.Permission{test.perm}, mock.Anything).
					Return(func(_ context.Context, _ []models.Permission, _ ...func(*constraints)) error {
						if test.expectErrorCode != "" {
							return errors.New("mock-injected error", errors.WithErrorCode(test.expectErrorCode))
						}
						return nil
					})
			}

			caller.User.Admin = test.isAdmin
			caller.authorizer = mockAuthorizer
			caller.dbClient = &db.Client{TeamMembers: mockTeamMembers}

			assert.Equal(t, test.expectErrorCode, errors.ErrorCode(caller.RequirePermission(ctx, test.perm, test.constraints...)))
		})
	}
}

func TestUserCaller_RequireAccessToInheritableResource(t *testing.T) {
	caller := UserCaller{User: &models.User{Metadata: models.ResourceMetadata{ID: "user1"}, Email: "user@email"}}
	ctx := WithCaller(context.Background(), &caller)

	testCases := []struct {
		name            string
		expectErrorCode errors.CodeType
		resourceType    models.ResourceType
		constraints     []func(*constraints)
		isAdmin         bool
		withAuthorizer  bool
	}{
		{
			name:           "permission granted by the authorizer",
			resourceType:   models.ProjectResource,
			constraints:    []func(*constraints){WithProjectID("project-1")},
			withAuthorizer: true,
		},
		{
			name:            "access denied by the authorizer because a permission is not satisfied",
			resourceType:    models.JobResource,
			constraints:     []func(*constraints){WithJobID("job-1")},
			expectErrorCode: errors.ENotFound,
			withAuthorizer:  true,
		},
		{
			name:         "permissions granted since user is admin",
			resourceType: models.OrganizationResource,
			isAdmin:      true,
		},
		{
			name:            "access denied because required constraints are not specified",
			resourceType:    models.ServiceAccountResource,
			expectErrorCode: errors.EInternal, // missing constraints
			withAuthorizer:  true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockAuthorizer := NewMockAuthorizer(t)

			if test.withAuthorizer {
				mockAuthorizer.On("RequireAccessToInheritableResource", mock.Anything, []models.ResourceType{test.resourceType}, mock.Anything).
					Return(func(_ context.Context, _ []models.ResourceType, _ ...func(*constraints)) error {
						if test.expectErrorCode != "" {
							return errors.New("mock-injected error", errors.WithErrorCode(test.expectErrorCode))
						}
						return nil
					})
			}

			caller.authorizer = mockAuthorizer
			caller.User.Admin = test.isAdmin
			assert.Equal(t, test.expectErrorCode,
				errors.ErrorCode(caller.RequireAccessToInheritableResource(ctx, test.resourceType, test.constraints...)))
		})
	}
}
