package auth

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jws"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	terrors "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"go.opentelemetry.io/otel/attribute"
)

// UserCaller represents a user subject
type UserCaller struct {
	User          *models.User
	authorizer    Authorizer
	dbClient      *db.Client
	teamListCache []models.Team // lazy init
	authorized    bool
}

// NewUserCaller returns a new UserCaller
func NewUserCaller(user *models.User, authorizer Authorizer, dbClient *db.Client) *UserCaller {
	return &UserCaller{
		User:       user,
		authorizer: authorizer,
		dbClient:   dbClient,
	}
}

// GetSubject returns the subject identifier for this caller
func (u *UserCaller) GetSubject() string {
	return u.User.Email
}

// IsAdmin returns true if the caller is an admin
func (u *UserCaller) IsAdmin() bool {
	return u.User.Admin
}

// Authorized marks the caller as authorized
func (u *UserCaller) Authorized() {
	u.authorized = true
}

// UnauthorizedError returns the unauthorized error for this specific caller type
func (u *UserCaller) UnauthorizedError(_ context.Context, hasViewerAccess bool) error {
	// If subject has at least viewer permissions then return 403, if not, return 404
	if hasViewerAccess {
		return terrors.New(
			"user %s is not authorized to perform the requested operation: ensure that the user has been added as a member to the organization or project with the role required to perform the requested operation",
			u.GetSubject(),
			terrors.WithErrorCode(terrors.EForbidden),
		)
	}

	return terrors.New(
		"either the requested resource does not exist or the user %s is not authorized to perform the requested operation: ensure that the user has been added as a member to the organization or project with the role required to perform the requested operation",
		u.GetSubject(),
		terrors.WithErrorCode(terrors.ENotFound),
	)
}

// RequirePermission will return an error if the caller doesn't have the specified permissions
func (u *UserCaller) RequirePermission(ctx context.Context, perm models.Permission, checks ...func(*constraints)) error {
	ctx, span := tracer.Start(ctx, "auth.RequirePermission")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "user"))

	if u.authorized {
		return nil
	}

	if perm.IsAssignable() && u.User.Admin {
		// User is an admin, so assignable permission can be granted.
		span.SetAttributes(attribute.Bool("authorized", true))
		return nil
	}

	if handlerFunc, ok := u.getPermissionHandler(perm); ok {
		result := handlerFunc(ctx, &perm, getConstraints(checks...))
		span.SetAttributes(attribute.Bool("authorized", (result == nil)))
		return result
	}

	result := u.authorizer.RequirePermissions(ctx, []models.Permission{perm}, checks...)
	span.SetAttributes(attribute.Bool("authorized", (result == nil)))
	return result
}

// RequireAccessToInheritableResource will return an error if caller doesn't have permissions to inherited resources.
func (u *UserCaller) RequireAccessToInheritableResource(ctx context.Context, resourceType models.ResourceType, checks ...func(*constraints)) error {
	ctx, span := tracer.Start(ctx, "auth.RequireAccessToInheritableResource")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "user"))

	if u.authorized {
		return nil
	}

	perm := models.Permission{Action: models.ViewAction, ResourceType: resourceType}
	if perm.IsAssignable() && u.User.Admin {
		// User is an admin, so assignable permission can be granted.
		span.SetAttributes(attribute.Bool("authorized", true))
		return nil
	}

	result := u.authorizer.RequireAccessToInheritableResource(ctx, []models.ResourceType{resourceType}, checks...)
	span.SetAttributes(attribute.Bool("authorized", (result == nil)))
	return result
}

// requireTeamUpdateAccess will return an error if the specified access is not allowed to the indicated team.
func (u *UserCaller) requireTeamUpdateAccess(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.teamID == nil {
		return errMissingConstraints
	}

	if u.User.Admin {
		return nil
	}

	teamMember, err := u.dbClient.TeamMembers.GetTeamMember(ctx, u.User.Metadata.ID, *checks.teamID)
	if err != nil {
		return err
	}

	// Allow access only if caller is a team member and is a maintainer.
	if teamMember != nil && teamMember.IsMaintainer {
		return nil
	}

	// All others are denied. Viewer access is available to everyone.
	return u.UnauthorizedError(ctx, true)
}

func (u *UserCaller) requireAdmin(ctx context.Context, _ *models.Permission, _ *constraints) error {
	if u.User.Admin {
		return nil
	}

	return u.UnauthorizedError(ctx, false)
}

// getPermissionHandler returns a permissionTypeHandler for a given permission.
func (u *UserCaller) getPermissionHandler(perm models.Permission) (permissionTypeHandler, bool) {
	handlerMap := map[models.Permission]permissionTypeHandler{
		models.CreateTeam: u.requireAdmin,
		models.DeleteTeam: u.requireAdmin,
		models.CreateUser: u.requireAdmin,
		models.UpdateUser: u.requireAdmin,
		models.DeleteUser: u.requireAdmin,
		models.UpdateTeam: u.requireTeamUpdateAccess,
	}

	handler, ok := handlerMap[perm]
	return handler, ok
}

// GetTeams does lazy initialization of the list of teams for this user caller.
func (u *UserCaller) GetTeams(ctx context.Context) ([]models.Team, error) {
	if u.teamListCache != nil {
		return u.teamListCache, nil
	}

	getResult, err := u.dbClient.Teams.GetTeams(ctx, &db.GetTeamsInput{
		Filter: &db.TeamFilter{
			UserID: &u.User.Metadata.ID,
		},
	})
	if err != nil {
		return nil, err
	}
	result := getResult.Teams

	u.teamListCache = result

	return result, nil
}

// IdentityProviderConfig encompasses the information for an identity provider
type IdentityProviderConfig struct {
	Issuer        string
	ClientID      string
	UsernameClaim string
	JwksURI       string
	TokenEndpoint string
	AuthEndpoint  string
}

type externalIdentity struct {
	ID       string
	Issuer   string
	Username string
	Email    string
}

// UserAuth implements JWT authentication
type UserAuth struct {
	idpMap      map[string]IdentityProviderConfig
	jwkRegistry *jwk.Cache
	logger      logger.Logger
	dbClient    *db.Client
}

const (
	defaultKeyAlgorithm         = jwa.RS256
	jwtRefreshIntervalInMinutes = 60
)

// NewUserAuth creates an instance of UserAuth
func NewUserAuth(
	ctx context.Context,
	identityProviders []IdentityProviderConfig,
	logger logger.Logger,
	dbClient *db.Client,
) *UserAuth {
	idpMap := make(map[string]IdentityProviderConfig)

	jwkRegistry := jwk.NewCache(ctx)

	for _, idp := range identityProviders {
		idpMap[idp.Issuer] = idp

		jwkRegistry.Register(idp.JwksURI, jwk.WithMinRefreshInterval(jwtRefreshIntervalInMinutes*time.Minute))

		_, err := jwkRegistry.Refresh(ctx, idp.JwksURI)
		if err != nil {
			logger.Errorf("failed to load keyset for IDP %s: %s", idp.Issuer, err)
		}
	}

	return &UserAuth{idpMap, jwkRegistry, logger, dbClient}
}

func (u *UserAuth) getKey(ctx context.Context, kid string, idp IdentityProviderConfig) (jwk.Key, error) {
	keyset, err := u.jwkRegistry.Get(ctx, idp.JwksURI)
	if err != nil {
		return nil, errors.New("failed to load key set for identity provider " + idp.Issuer)
	}

	key, found := keyset.LookupKeyID(kid)
	if !found {
		// Attempt to refresh the keyset for the IDP because the keys may have been updated
		keyset, err := u.jwkRegistry.Refresh(ctx, idp.JwksURI)
		if err != nil {
			return nil, errors.New("failed to load key set for identity provider " + idp.Issuer)
		}

		key, found = keyset.LookupKeyID(kid)
		if !found {
			return nil, errors.New("failed to load key set for identity provider " + idp.Issuer)
		}

		return key, nil
	}

	return key, nil
}

// GetUsernameClaim returns the username from a JWT token
func (u *UserAuth) GetUsernameClaim(token jwt.Token) (string, error) {
	idp, ok := u.idpMap[token.Issuer()]
	if !ok {
		return "", errors.New("identity provider not found for token with issuer " + token.Issuer())
	}

	username, ok := token.Get(idp.UsernameClaim)
	if !ok {
		return "", errors.New("token with issuer " + token.Issuer() + " is missing " + idp.UsernameClaim + " field")
	}

	return username.(string), nil
}

// Authenticate validates a user JWT and returns a UserCaller
func (u *UserAuth) Authenticate(ctx context.Context, tokenString string, useCache bool) (*UserCaller, error) {
	tokenBytes := []byte(tokenString)

	// Parse token headers
	msg, err := jws.Parse(tokenBytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse token headers %w", err)
	}

	signatures := msg.Signatures()
	if len(signatures) < 1 {
		return nil, errors.New("token is missing signature")
	}

	// Parse jwt
	decodedToken, err := jwt.Parse(tokenBytes, jwt.WithVerify(false))
	if err != nil {
		return nil, fmt.Errorf("failed to decode token %w", err)
	}

	kid := signatures[0].ProtectedHeaders().KeyID()
	idp := u.idpMap[decodedToken.Issuer()]

	key, err := u.getKey(ctx, kid, idp)
	if err != nil {
		return nil, err
	}

	alg := key.Algorithm()
	if alg.String() == "" {
		alg = defaultKeyAlgorithm
	}

	if err = key.Set(jwk.AlgorithmKey, alg); err != nil {
		return nil, err
	}

	keySet := jwk.NewSet()
	if err = keySet.AddKey(key); err != nil {
		return nil, err
	}

	// Verify Token Signature
	if _, vErr := jws.Verify(tokenBytes, jws.WithKeySet(keySet)); vErr != nil {
		return nil, vErr
	}

	// Validate claims
	if vErr := jwt.Validate(decodedToken, jwt.WithAudience(idp.ClientID), jwt.WithIssuer(idp.Issuer)); vErr != nil {
		return nil, vErr
	}

	username, err := u.GetUsernameClaim(decodedToken)
	if err != nil {
		return nil, err
	}

	// Get user from DB, user will be created if this is the first login.
	userModel, err := u.dbClient.Users.GetUserByExternalID(ctx, decodedToken.Issuer(), decodedToken.Subject())
	if err != nil {
		return nil, terrors.Wrap(err, "failed to get user by external identity")
	}

	if userModel == nil {
		email, ok := decodedToken.Get("email")
		if !ok {
			return nil, fmt.Errorf("email claim missing from token")
		}

		userModel, err = u.createUser(ctx, &externalIdentity{
			Issuer:   decodedToken.Issuer(),
			ID:       decodedToken.Subject(),
			Username: ParseUsername(username),
			Email:    strings.ToLower(email.(string)),
		})
		if err != nil {
			u.logger.Errorf("Failed to create user in db: %v", err)
			return nil, err
		}
	}

	// If user is not active (disabled via SCIM), return EUnauthorized.
	if !userModel.Active {
		return nil, terrors.New(
			"User is disabled",
			terrors.WithErrorCode(terrors.EUnauthorized),
		)
	}

	return NewUserCaller(
		userModel,
		newAuthorizer(u.dbClient, &userModel.Metadata.ID, nil, useCache),
		u.dbClient,
	), nil
}

func (u *UserAuth) createUser(ctx context.Context, identity *externalIdentity) (*models.User, error) {
	// Create user since this if the first time we've seen this user identity
	user, err := u.createUserWithExternalID(ctx, identity)
	if err != nil && terrors.ErrorCode(err) == terrors.EConflict {
		// The conflict error may be due to an existing user that is using the same username or the user for
		// the external identity may already exist but this is the first time we've seen this external ID. If
		// the user's email matches then we can link the external ID to the existing user.

		// Query user by email to check if email matches
		user, err = u.dbClient.Users.GetUserByEmail(ctx, identity.Email)
		if err != nil {
			return nil, err
		}

		if user != nil {
			err = u.dbClient.Users.LinkUserWithExternalID(ctx, identity.Issuer, identity.ID, user.Metadata.ID)
			// Ignore conflict errors since another instance may have already linked the external identity
			if err != nil && terrors.ErrorCode(err) != terrors.EConflict {
				return nil, terrors.Wrap(
					err,
					"failed to link user with external identity",
				)
			}
		} else {
			// User not found with email so we need to create a new user with a number added to their username
			resp, uErr := u.dbClient.Users.GetUsers(ctx, &db.GetUsersInput{Filter: &db.UserFilter{UsernamePrefix: &identity.Username}})
			if uErr != nil {
				return nil, uErr
			}
			newUsername := fmt.Sprintf("%s%d", identity.Username, resp.PageInfo.TotalCount+1)
			// Create user with new username
			user, err = u.createUserWithExternalID(ctx, &externalIdentity{
				ID:       identity.ID,
				Issuer:   identity.Issuer,
				Email:    identity.Email,
				Username: newUsername,
			})
			if err != nil {
				return nil, err
			}
		}
	} else if err != nil {
		return nil, err
	}

	return user, nil
}

func (u *UserAuth) createUserWithExternalID(ctx context.Context, identity *externalIdentity) (*models.User, error) {
	txContext, err := u.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, err
	}

	defer func() {
		if txErr := u.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			u.logger.Errorf("failed to rollback tx for createUserWithExternalID: %v", txErr)
		}
	}()

	input := &models.User{
		Username: identity.Username,
		Email:    identity.Email,
		Active:   true,
	}

	user, err := u.dbClient.Users.CreateUser(txContext, input)
	if err != nil {
		return nil, err
	}

	err = u.dbClient.Users.LinkUserWithExternalID(txContext, identity.Issuer, identity.ID, user.Metadata.ID)
	if err != nil {
		return nil, err
	}

	if err := u.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, err
	}

	return user, nil
}

// ParseUsername parses the username, if any, from the email.
func ParseUsername(username string) string {
	resp := username
	at := strings.LastIndex(username, "@")
	if at >= 0 {
		// Remove email domain
		resp = username[:at]
	}
	// Convert username to lowercase
	return strings.ToLower(resp)
}
