// Package auth authenticates and authorizes a subject
// attempting to access API resources.
package auth

import (
	"context"
	"fmt"

	"github.com/lestrrat-go/jwx/v2/jwt"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

var (
	errUnauthorized = "token is unauthorized"
	errExpired      = "token is expired"
	errNBFInvalid   = "token nbf validation failed"
	errIATInvalid   = "token iat validation failed"
	errNoTokenFound = "no token found"     // nolint
	errAlgoInvalid  = "algorithm mismatch" // nolint
)

// Valid token types used as private claims for tokens
// issued by Phobos.
const (
	JobTokenType            string = "job"
	SCIMTokenType           string = "scim"
	ServiceAccountTokenType string = "service_account"
)

// Authenticator is used to authenticate JWT tokens
type Authenticator struct {
	userAuth  *UserAuth
	idp       *IdentityProvider
	dbClient  *db.Client
	issuerURL string
}

// NewAuthenticator creates a new Authenticator instance
func NewAuthenticator(userAuth *UserAuth, idp *IdentityProvider, dbClient *db.Client, issuerURL string) *Authenticator {
	return &Authenticator{
		userAuth:  userAuth,
		idp:       idp,
		dbClient:  dbClient,
		issuerURL: issuerURL,
	}
}

// Authenticate verifies the token and returns a Caller
func (a *Authenticator) Authenticate(ctx context.Context, tokenString string, useCache bool) (Caller, error) {
	if tokenString == "" {
		return nil, errors.New("Authentication token is missing", errors.WithErrorCode(errors.EUnauthorized))
	}

	decodedToken, err := jwt.Parse([]byte(tokenString), jwt.WithVerify(false))
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode token", errors.WithErrorCode(errors.EUnauthorized))
	}

	if decodedToken.Issuer() == a.issuerURL {
		// This is a non-user token
		output, vtErr := a.idp.VerifyToken(ctx, tokenString)
		if vtErr != nil {
			return nil, errors.New(errorReason(vtErr), errors.WithErrorCode(errors.EUnauthorized))
		}

		tokenType, ok := output.PrivateClaims["type"]
		if !ok {
			return nil, fmt.Errorf("failed to get token type")
		}

		switch tokenType {
		case ServiceAccountTokenType:
			serviceAccountID := gid.FromGlobalID(output.PrivateClaims["service_account_id"])
			return NewServiceAccountCaller(
				serviceAccountID,
				output.PrivateClaims["service_account_prn"],
				newAuthorizer(a.dbClient, nil, &serviceAccountID, useCache),
				a.dbClient,
			), nil
		case JobTokenType:
			return &JobCaller{
				JobID:     gid.FromGlobalID(output.PrivateClaims["job_id"]),
				ProjectID: gid.FromGlobalID(output.PrivateClaims["project_id"]),
				dbClient:  a.dbClient,
			}, nil
		case SCIMTokenType:
			scimCaller, sErr := a.verifySCIMTokenClaim(ctx, output.Token)
			if sErr != nil {
				return nil, errors.New(errorReason(sErr), errors.WithErrorCode(errors.EUnauthorized))
			}
			return scimCaller, nil
		default:
			return nil, errors.New("Unsupported token type received")
		}
	}

	// This is a user token
	caller, err := a.userAuth.Authenticate(ctx, tokenString, useCache)
	if err != nil {
		return nil, errors.New(errorReason(err), errors.WithErrorCode(errors.EUnauthorized))
	}

	return caller, nil
}

// verifySCIMToken verifies the JwtID field is known.
func (a *Authenticator) verifySCIMTokenClaim(ctx context.Context, token jwt.Token) (*SCIMCaller, error) {
	// Get the token claim to verify it is known.
	tokenClaim, err := a.dbClient.SCIMTokens.GetTokenByNonce(ctx, token.JwtID())
	if err != nil {
		return nil, err
	}

	if tokenClaim == nil {
		return nil, fmt.Errorf("scim token has an invalid jti claim")
	}

	return NewSCIMCaller(a.dbClient), nil
}

// ErrorReason will normalize the error message
func errorReason(err error) string {
	switch err.Error() {
	case "exp not satisfied":
		return errExpired
	case "iat not satisfied":
		return errIATInvalid
	case "nbf not satisfied":
		return errNBFInvalid
	default:
		return errUnauthorized
	}
}
