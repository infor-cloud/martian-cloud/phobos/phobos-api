package auth

//go:generate go tool mockery --name Caller --inpackage --case underscore

import (
	"context"
	goerror "errors"
	"net/http"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"go.opentelemetry.io/otel/attribute"
)

// errMissingConstraints is the error returned when required constraints or permissions are missing.
var errMissingConstraints = goerror.New("missing required permissions or constraints")

// constraints defines permission constraints that should be checked
// when a subject is being authorized to modify or view a Phobos resource.
type constraints struct {
	pipelineID         *string
	pipelineTaskPath   *string
	pipelineActionPath *string
	jobID              *string
	projectID          *string
	teamID             *string
	userID             *string
	agentID            *string
	organizationIDs    []string
}

// getConstraints returns a constraints struct.
func getConstraints(checks ...func(*constraints)) *constraints {
	constraint := &constraints{}
	for _, c := range checks {
		c(constraint)
	}

	return constraint
}

// WithOrganizationIDs sets the organizationIDs on constraints struct.
func WithOrganizationIDs(ids []string) func(*constraints) {
	return func(c *constraints) {
		c.organizationIDs = ids
	}
}

// WithOrganizationID sets the organization ID on constraints struct.
func WithOrganizationID(id string) func(*constraints) {
	return func(c *constraints) {
		c.organizationIDs = []string{id}
	}
}

// WithProjectID sets the project ID on the constraints struct.
func WithProjectID(id string) func(*constraints) {
	return func(c *constraints) {
		c.projectID = &id
	}
}

// WithJobID sets the job ID on the constraints struct.
func WithJobID(id string) func(*constraints) {
	return func(c *constraints) {
		c.jobID = &id
	}
}

// WithPipelineID sets the pipeline ID on the constraints struct.
func WithPipelineID(id string) func(*constraints) {
	return func(c *constraints) {
		c.pipelineID = &id
	}
}

// WithPipelineTask sets the pipeline task path on the constraints struct.
func WithPipelineTask(path string) func(*constraints) {
	return func(c *constraints) {
		c.pipelineTaskPath = &path
	}
}

// WithPipelineAction sets the pipeline action path on the constraints struct.
func WithPipelineAction(path string) func(*constraints) {
	return func(c *constraints) {
		c.pipelineActionPath = &path
	}
}

// WithUserID sets the UserID on constraints struct.
func WithUserID(id string) func(*constraints) {
	return func(c *constraints) {
		c.userID = &id
	}
}

// WithTeamID sets the TeamID on Constraints struct.
func WithTeamID(id string) func(*constraints) {
	return func(c *constraints) {
		c.teamID = &id
	}
}

// WithAgentID sets the agentID on Constraints struct.
func WithAgentID(id string) func(*constraints) {
	return func(c *constraints) {
		c.agentID = &id
	}
}

// Uses the context key pattern
type contextKey string

// contextKeyCaller accesses the caller object.
var contextKeyCaller = contextKey("caller")

// contextKeySubject accesses the subject string.
var contextKeySubject = contextKey("subject")

func (c contextKey) String() string {
	return "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth " + string(c)
}

// GetCaller returns a context's caller.  Return nil if no caller was found on the context.
func GetCaller(ctx context.Context) Caller {
	caller, ok := ctx.Value(contextKeyCaller).(Caller)
	if !ok {
		return nil
	}
	return caller
}

// GetSubject returns a context's subject.  Return nil if no subject was found on the context.
func GetSubject(ctx context.Context) *string {
	subject, ok := ctx.Value(contextKeySubject).(string)
	if !ok {
		return nil
	}
	return &subject
}

// permissionTypeHandler allows delegating checks based on the permission type.
type permissionTypeHandler func(ctx context.Context, perm *models.Permission, checks *constraints) error

// noopPermissionHandler handles any use-cases where the permission is automatically granted for a caller.
func noopPermissionHandler(_ context.Context, _ *models.Permission, _ *constraints) error {
	return nil
}

// SystemCaller is the caller subject for internal system calls
type SystemCaller struct {
	Subject string
}

// GetSubject returns the subject identifier for this caller
func (s *SystemCaller) GetSubject() string {
	if s.Subject != "" {
		return s.Subject
	}
	return "system"
}

// Authorized marks the caller as authorized
func (s *SystemCaller) Authorized() {
	// System caller is always authorized
}

// IsAdmin returns true if the caller is an admin
func (s *SystemCaller) IsAdmin() bool {
	// System caller is always an admin
	return true
}

// RequirePermission will return an error if the caller doesn't have the specified permissions
func (s *SystemCaller) RequirePermission(ctx context.Context, _ models.Permission, _ ...func(*constraints)) error {
	_, span := tracer.Start(ctx, "auth.RequirePermission")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "system"), attribute.Bool("authorized", true))

	// Return nil because system caller is authorized to perform any action
	return nil
}

// RequireAccessToInheritableResource will return an error if the caller doesn't have access to the specified resource type
func (s *SystemCaller) RequireAccessToInheritableResource(ctx context.Context, _ models.ResourceType, _ ...func(*constraints)) error {
	_, span := tracer.Start(ctx, "auth.RequireAccessToInheritableResource")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "system"), attribute.Bool("authorized", true))

	// Return nil because system caller is authorized to perform any action
	return nil
}

// UnauthorizedError returns the unauthorized error for this specific caller type
func (s *SystemCaller) UnauthorizedError(_ context.Context, _ bool) error {
	return errors.New(
		"system caller is not authorized to perform this action",
		errors.WithErrorCode(errors.EForbidden),
	)
}

// Caller represents a subject performing an API request
type Caller interface {
	GetSubject() string
	IsAdmin() bool
	RequirePermission(ctx context.Context, perm models.Permission, checks ...func(*constraints)) error
	RequireAccessToInheritableResource(ctx context.Context, resourceType models.ResourceType, checks ...func(*constraints)) error
	UnauthorizedError(ctx context.Context, hasViewerAccess bool) error
	Authorized()
}

// WithCaller adds the caller to the context
func WithCaller(ctx context.Context, caller Caller) context.Context {
	return context.WithValue(ctx, contextKeyCaller, caller)
}

// WithSubject adds the subject string to the context
func WithSubject(ctx context.Context, subject string) context.Context {
	return context.WithValue(ctx, contextKeySubject, subject)
}

// AuthorizeCaller verifies that a caller has been authenticated and returns the caller
func AuthorizeCaller(ctx context.Context) (Caller, error) {
	ctx, span := tracer.Start(ctx, "auth.AuthorizeCaller")
	defer span.End()

	caller, ok := ctx.Value(contextKeyCaller).(Caller)
	span.SetAttributes(attribute.Bool("authenticated", ok))
	if !ok {
		// This intentionally does not record an error on the span.
		return nil, errors.New("Authentication is required", errors.WithErrorCode(errors.EUnauthorized))
	}

	return caller, nil
}

// HandleCaller will invoke the provided callback based on the type of caller
func HandleCaller(
	ctx context.Context,
	userHandler func(ctx context.Context, c *UserCaller) error,
	serviceAccountHandler func(ctx context.Context, c *ServiceAccountCaller) error,
) error {
	caller, err := AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	switch cType := caller.(type) {
	case *UserCaller:
		return userHandler(ctx, cType)
	case *ServiceAccountCaller:
		return serviceAccountHandler(ctx, cType)
	default:
		return errors.New("Invalid caller type", errors.WithErrorCode(errors.EForbidden))
	}
}

// FindToken returns the bearer token from an HTTP request
func FindToken(r *http.Request) string {
	// Get token from authorization header.
	bearer := r.Header.Get("Authorization")
	if len(bearer) > 7 && strings.ToUpper(bearer[0:6]) == "BEARER" {
		return bearer[7:]
	}

	return ""
}
