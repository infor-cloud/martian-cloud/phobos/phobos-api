package auth

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	terrors "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"go.opentelemetry.io/otel/attribute"
)

// SCIMCaller represents a SCIM subject.
type SCIMCaller struct {
	dbClient   *db.Client
	authorized bool
}

// NewSCIMCaller returns a new SCIM caller.
func NewSCIMCaller(dbClient *db.Client) *SCIMCaller {
	return &SCIMCaller{dbClient: dbClient}
}

// GetSubject returns the subject identifier for this caller.
func (s *SCIMCaller) GetSubject() string {
	return "scim"
}

// IsAdmin returns true if the caller is an admin.
func (s *SCIMCaller) IsAdmin() bool {
	return false
}

// Authorized marks the caller as authorized
func (s *SCIMCaller) Authorized() {
	s.authorized = true
}

// UnauthorizedError returns the unauthorized error for this specific caller type
func (s *SCIMCaller) UnauthorizedError(_ context.Context, hasViewerAccess bool) error {
	forbiddenMsg := "SCIM caller is not authorized to perform the requested operation"

	// If subject has at least viewer permissions then return 403, if not, return 404
	if hasViewerAccess {
		return terrors.New(
			forbiddenMsg,
			terrors.WithErrorCode(terrors.EForbidden),
		)
	}

	return terrors.New(
		"either the requested resource does not exist or the %s",
		forbiddenMsg,
		terrors.WithErrorCode(terrors.ENotFound),
	)
}

// RequirePermission will return an error if the caller doesn't have the specified models.
func (s *SCIMCaller) RequirePermission(ctx context.Context, perm models.Permission, checks ...func(*constraints)) error {
	ctx, span := tracer.Start(ctx, "auth.RequirePermission")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "scim"))

	if s.authorized {
		return nil
	}

	handlerFunc, ok := s.getPermissionHandler(perm)
	if !ok {
		span.SetAttributes(attribute.Bool("authorized", false))
		return s.UnauthorizedError(ctx, false)
	}

	result := handlerFunc(ctx, &perm, getConstraints(checks...))
	span.SetAttributes(attribute.Bool("authorized", (result == nil)))
	return result
}

// RequireAccessToInheritableResource will return an error if the caller doesn't have access to the specified resource type.
func (s *SCIMCaller) RequireAccessToInheritableResource(ctx context.Context, _ models.ResourceType, _ ...func(*constraints)) error {
	ctx, span := tracer.Start(ctx, "auth.RequireAccessToInheritableResource")
	defer span.End()
	span.SetAttributes(attribute.String("callerType", "scim"), attribute.Bool("authorized", false))

	// Return an authorization error since SCIM does not need any access to inherited resources.
	return s.UnauthorizedError(ctx, false)
}

// requireTeamDeleteAccess will return an error if the specified caller is not allowed to delete a team.
func (s *SCIMCaller) requireTeamDeleteAccess(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.teamID == nil {
		return errMissingConstraints
	}

	team, err := s.dbClient.Teams.GetTeamByID(ctx, *checks.teamID)
	if err != nil {
		return err
	}

	// Only allow deleting teams which are created via SCIM.
	if team != nil && team.SCIMExternalID != nil {
		return nil
	}

	return s.UnauthorizedError(ctx, false)
}

// requireUserDeleteAccess will return an error if the specified caller is not allowed to delete a user.
func (s *SCIMCaller) requireUserDeleteAccess(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.userID == nil {
		return errMissingConstraints
	}

	user, err := s.dbClient.Users.GetUserByID(ctx, *checks.userID)
	if err != nil {
		return err
	}

	// Only allow deleting users created via SCIM.
	if user != nil && user.SCIMExternalID != nil {
		return nil
	}

	return s.UnauthorizedError(ctx, false)
}

// getPermissionHandler returns a permissionTypeHandler for a given permission.
func (s *SCIMCaller) getPermissionHandler(perm models.Permission) (permissionTypeHandler, bool) {
	handlerMap := map[models.Permission]permissionTypeHandler{
		models.DeleteTeam: s.requireTeamDeleteAccess,
		models.DeleteUser: s.requireUserDeleteAccess,
		models.CreateTeam: noopPermissionHandler,
		models.UpdateTeam: noopPermissionHandler,
		models.CreateUser: noopPermissionHandler,
		models.UpdateUser: noopPermissionHandler,
	}

	handlerFunc, ok := handlerMap[perm]
	return handlerFunc, ok
}
