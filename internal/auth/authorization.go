package auth

//go:generate go tool mockery --name Authorizer --inpackage --case underscore

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
)

const (
	resourceNotFoundErrorMsg = "Resource not found"
)

type cacheKey struct {
	organizationID *string
	projectID      *string
}

func (c cacheKey) getOrganizationIDKey() string {
	return fmt.Sprintf("organization_id::%s", *c.organizationID)
}

func (c cacheKey) getProjectIDKey() string {
	return fmt.Sprintf("project_id::%s", *c.projectID)
}

func (c cacheKey) getGlobalMembershipKey() string {
	return "global"
}

// Authorizer is used to authorize access to Phobos resources.
type Authorizer interface {
	RequirePermissions(ctx context.Context, perms []models.Permission, checks ...func(*constraints)) error
	RequireAccessToInheritableResource(ctx context.Context, resourceType []models.ResourceType, checks ...func(*constraints)) error
}

type authorizer struct {
	dbClient            *db.Client
	rolePermissionCache map[string][]models.Permission
	membershipCache     map[string]map[string]struct{}
	userID              *string
	serviceAccountID    *string
	lock                sync.RWMutex
	useCache            bool
}

func newAuthorizer(dbClient *db.Client, userID *string, serviceAccountID *string, useCache bool) *authorizer {
	return &authorizer{
		dbClient:            dbClient,
		userID:              userID,
		serviceAccountID:    serviceAccountID,
		rolePermissionCache: map[string][]models.Permission{},
		membershipCache:     map[string]map[string]struct{}{},
		useCache:            useCache,
	}
}

func (a *authorizer) RequirePermissions(ctx context.Context, perms []models.Permission, checks ...func(*constraints)) error {
	c := getConstraints(checks...)

	if len(perms) == 0 || !a.hasConstraints(c) {
		return errMissingConstraints
	}

	for ix := range perms {
		if c.projectID != nil {
			if err := a.requireAccessToProject(ctx, *c.projectID, &perms[ix]); err != nil {
				return err
			}
		}
		for _, orgID := range c.organizationIDs {
			if err := a.requireAccessToOrganization(ctx, orgID, &perms[ix]); err != nil {
				return err
			}
		}
	}

	return nil
}

func (a *authorizer) RequireAccessToInheritableResource(ctx context.Context, resourceType []models.ResourceType, checks ...func(*constraints)) error {
	c := getConstraints(checks...)

	if len(resourceType) == 0 || len(c.organizationIDs) == 0 {
		return errMissingConstraints
	}

	for _, rt := range resourceType {
		perm := &models.Permission{Action: models.ViewAction, ResourceType: rt}
		for _, orgID := range c.organizationIDs {
			if err := a.requireAccessToInheritedOrganizationResources(ctx, orgID, perm); err != nil {
				return err
			}
		}
	}

	return nil
}

func (a *authorizer) requireAccessToOrganization(ctx context.Context, orgID string, perm *models.Permission) error {
	if a.isPermissionCached(&cacheKey{organizationID: &orgID}, perm) {
		return nil
	}

	resp, err := a.getMemberships(ctx, &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			OrganizationID: &orgID,
			// Look for only organization and global memberships.
			MembershipScopes: []models.ScopeType{
				models.OrganizationScope,
				models.GlobalScope,
			},
		},
	})
	if err != nil {
		return err
	}

	if resp.PageInfo.TotalCount == 0 {
		return a.authorizationError(ctx, false)
	}

	return a.requirePermission(ctx, resp.Memberships, perm)
}

func (a *authorizer) requireAccessToProject(ctx context.Context, projectID string, perm *models.Permission) error {
	if a.isPermissionCached(&cacheKey{projectID: &projectID}, perm) {
		return nil
	}

	membershipsResult, err := a.getMemberships(ctx, &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			ProjectID: &projectID,
			// Look for all membership types.
			MembershipScopes: []models.ScopeType{
				models.OrganizationScope,
				models.ProjectScope,
				models.GlobalScope,
			},
		},
	})
	if err != nil {
		return err
	}

	if membershipsResult.PageInfo.TotalCount == 0 {
		return a.authorizationError(ctx, false)
	}

	return a.requirePermission(ctx, membershipsResult.Memberships, perm)
}

func (a *authorizer) requireAccessToInheritedOrganizationResources(
	ctx context.Context,
	organizationID string,
	perm *models.Permission,
) error {
	if a.isPermissionCached(&cacheKey{organizationID: &organizationID}, perm) {
		return nil
	}

	memberships, err := a.getMemberships(ctx, &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			OrganizationID: &organizationID,
			// Look for all membership types.
			MembershipScopes: []models.ScopeType{
				models.OrganizationScope,
				models.ProjectScope,
				models.GlobalScope,
			},
		},
	})
	if err != nil {
		return err
	}

	if memberships.PageInfo.TotalCount == 0 {
		return a.authorizationError(ctx, false)
	}

	return a.requirePermission(ctx, memberships.Memberships, perm)
}

// requirePermission returns an error if the target permission can't be found within memberships.
func (a *authorizer) requirePermission(
	ctx context.Context,
	memberships []models.Membership,
	target *models.Permission,
) error {
	hasViewerAccess := false
	for ix := range memberships {
		perms, err := a.getPermissionsFromMembership(ctx, &memberships[ix])
		if err != nil {
			return err
		}

		for _, p := range perms {
			if p.GTE(target) {
				return nil
			}

			// Determine if caller has at least viewer access for resource
			// if an authorizationError is to be returned.
			if p.ResourceType == target.ResourceType && p.Action.HasViewerAccess() {
				hasViewerAccess = true
			}
		}
	}

	return a.authorizationError(ctx, hasViewerAccess)
}

// isPermissionCached returns true if the permission is found in cache.
// It only looks for an exact match, meaning, a permission with View
// Action will not be automatically granted if a subject has a permission
// with Create, Update, Delete for that ResourceType.
func (a *authorizer) isPermissionCached(key *cacheKey, perm *models.Permission) bool {
	if !a.useCache {
		return false
	}

	a.lock.RLock()
	defer a.lock.RUnlock()

	if key.organizationID != nil {
		if cachedPerms, ok := a.membershipCache[cacheKey{organizationID: key.organizationID}.getOrganizationIDKey()]; ok {
			if _, ok := cachedPerms[perm.String()]; ok {
				return ok
			}
		}
	} else if key.projectID != nil {
		if cachedPerms, ok := a.membershipCache[cacheKey{projectID: key.projectID}.getProjectIDKey()]; ok {
			if _, ok := cachedPerms[perm.String()]; ok {
				return ok
			}
		}
	}

	// Check if permission is cached for a global membership.
	cachedPerms, ok := a.membershipCache[cacheKey{}.getGlobalMembershipKey()]
	if !ok {
		return false
	}

	_, ok = cachedPerms[perm.String()]
	return ok
}

// addToCache adds specified perms to role and membership caches.
func (a *authorizer) addToCache(membership *models.Membership, perms []models.Permission) {
	if !a.useCache {
		// Cache isn't being used.
		return
	}

	a.lock.Lock()
	defer a.lock.Unlock()

	// Add role perms to cache.
	if _, ok := a.rolePermissionCache[membership.RoleID]; !ok {
		a.rolePermissionCache[membership.RoleID] = perms
	}

	switch membership.Scope {
	case models.OrganizationScope:
		a.addMembershipToCache(cacheKey{organizationID: membership.OrganizationID}.getOrganizationIDKey(), perms)
	case models.ProjectScope:
		a.addMembershipToCache(cacheKey{projectID: membership.ProjectID}.getProjectIDKey(), perms)
	case models.GlobalScope:
		a.addMembershipToCache(cacheKey{}.getGlobalMembershipKey(), perms)
	}
}

func (a *authorizer) addMembershipToCache(key string, perms []models.Permission) {
	if _, ok := a.membershipCache[key]; !ok {
		permsMap := make(map[string]struct{}, len(perms))
		for _, p := range perms {
			permsMap[p.String()] = struct{}{}
		}
		a.membershipCache[key] = permsMap
	}
}

func (a *authorizer) getPermissionsFromMembership(ctx context.Context, membership *models.Membership) ([]models.Permission, error) {
	if a.useCache {
		a.lock.RLock()
		if perms, ok := a.rolePermissionCache[membership.RoleID]; ok {
			a.lock.RUnlock()
			return perms, nil
		}
		a.lock.RUnlock()
	}

	// Check if this a default role, in which case we can use the permissions from the map.
	if perms, ok := models.DefaultRoleID(membership.RoleID).Permissions(); ok {
		a.addToCache(membership, perms)
		return perms, nil
	}

	// Get the role by ID.
	role, err := a.dbClient.Roles.GetRoleByID(ctx, membership.RoleID)
	if err != nil {
		return nil, err
	}

	if role == nil {
		return nil, a.authorizationError(ctx, false)
	}

	perms := role.GetPermissions()

	// Add to membership and role cache.
	a.addToCache(membership, perms)

	return perms, nil
}

func (a *authorizer) getMemberships(
	ctx context.Context,
	input *db.GetMembershipsInput,
) (*db.MembershipsResult, error) {
	if input.Filter == nil {
		input.Filter = &db.MembershipFilter{}
	}

	input.Filter.UserID = a.userID
	input.Filter.ServiceAccountID = a.serviceAccountID

	resp, err := a.dbClient.Memberships.GetMemberships(ctx, input)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// hasConstraints returns true if at least one of the required constraints are specified.
func (*authorizer) hasConstraints(checks *constraints) bool {
	return len(checks.organizationIDs) > 0 || checks.projectID != nil
}

func (a *authorizer) authorizationError(ctx context.Context, hasViewerAccessLevel bool) error {
	caller, err := AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	return caller.UnauthorizedError(ctx, hasViewerAccessLevel)
}
