package auth

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

func TestServiceAccountCaller_GetSubject(t *testing.T) {
	caller := ServiceAccountCaller{ServiceAccountPRN: models.ServiceAccountResource.BuildPRN("service-account-1")}
	assert.Equal(t, "prn:service_account:service-account-1", caller.GetSubject())
}

func TestServiceAccountCaller_IsAdmin(t *testing.T) {
	caller := ServiceAccountCaller{ServiceAccountID: "service-account-1"}
	assert.False(t, caller.IsAdmin())
}

func TestServiceAccountCaller_RequirePermissions(t *testing.T) {
	caller := ServiceAccountCaller{
		ServiceAccountID: "service-account-1",
	}
	ctx := WithCaller(context.Background(), &caller)

	testCases := []struct {
		expectErrorCode errors.CodeType
		perm            models.Permission
		name            string
		constraints     []func(*constraints)
		withAuthorizer  bool
	}{
		{
			name:           "access is granted by the authorizer",
			perm:           models.ViewOrganization,
			constraints:    []func(*constraints){WithOrganizationID("organization-1")},
			withAuthorizer: true,
		},
		{
			name:            "access is denied by the authorizer",
			perm:            models.CreateProject,
			constraints:     []func(*constraints){WithProjectID("project-1")},
			expectErrorCode: errors.ENotFound,
			withAuthorizer:  true,
		},
		{
			name:            "access denied because required constraints are not specified",
			perm:            models.ViewProject,
			expectErrorCode: errors.EInternal,
			withAuthorizer:  true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockAuthorizer := NewMockAuthorizer(t)

			if test.withAuthorizer {
				mockAuthorizer.On("RequirePermissions", mock.Anything, []models.Permission{test.perm}, mock.Anything).
					Return(func(_ context.Context, _ []models.Permission, _ ...func(*constraints)) error {
						if test.expectErrorCode != "" {
							return errors.New("mock-injected error", errors.WithErrorCode(test.expectErrorCode))
						}
						return nil
					})
			}

			caller.authorizer = mockAuthorizer
			err := caller.RequirePermission(ctx, test.perm, test.constraints...)
			if test.expectErrorCode != "" {
				require.NotNil(t, err)
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
			require.Nil(t, err)
		})
	}
}

func TestServiceAccountCaller_RequireAccessToInheritableResource(t *testing.T) {
	caller := ServiceAccountCaller{
		ServiceAccountID: "service-account-1",
	}
	ctx := WithCaller(context.Background(), &caller)

	testCases := []struct {
		expectErrorCode errors.CodeType
		name            string
		resourceType    models.ResourceType
		constraints     []func(*constraints)
		withAuthorizer  bool
	}{
		{
			name:           "access is granted by the authorizer",
			resourceType:   models.ProjectResource,
			constraints:    []func(*constraints){WithOrganizationID("organization-1")},
			withAuthorizer: true,
		},
		{
			name:            "access is denied by the authorizer",
			resourceType:    models.JobResource,
			constraints:     []func(*constraints){WithOrganizationID("organization-1")},
			expectErrorCode: errors.ENotFound,
			withAuthorizer:  true,
		},
		{
			name:            "access denied because required constraints are not specified",
			resourceType:    models.ProjectResource,
			expectErrorCode: errors.EInternal, // missing constraints
			withAuthorizer:  true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockAuthorizer := NewMockAuthorizer(t)

			if test.withAuthorizer {
				mockAuthorizer.On("RequireAccessToInheritableResource", mock.Anything, []models.ResourceType{test.resourceType}, mock.Anything).
					Return(func(_ context.Context, _ []models.ResourceType, _ ...func(*constraints)) error {
						if test.expectErrorCode != "" {
							return errors.New("mock-injected error", errors.WithErrorCode(test.expectErrorCode))
						}
						return nil
					})
			}

			caller.authorizer = mockAuthorizer
			assert.Equal(t, test.expectErrorCode,
				errors.ErrorCode(caller.RequireAccessToInheritableResource(ctx, test.resourceType, test.constraints...)),
			)
		})
	}
}
