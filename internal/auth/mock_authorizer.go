// Code generated by mockery v2.53.0. DO NOT EDIT.

package auth

import (
	context "context"

	mock "github.com/stretchr/testify/mock"
	models "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
)

// MockAuthorizer is an autogenerated mock type for the Authorizer type
type MockAuthorizer struct {
	mock.Mock
}

// RequireAccessToInheritableResource provides a mock function with given fields: ctx, resourceType, checks
func (_m *MockAuthorizer) RequireAccessToInheritableResource(ctx context.Context, resourceType []models.ResourceType, checks ...func(*constraints)) error {
	_va := make([]interface{}, len(checks))
	for _i := range checks {
		_va[_i] = checks[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, resourceType)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for RequireAccessToInheritableResource")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []models.ResourceType, ...func(*constraints)) error); ok {
		r0 = rf(ctx, resourceType, checks...)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// RequirePermissions provides a mock function with given fields: ctx, perms, checks
func (_m *MockAuthorizer) RequirePermissions(ctx context.Context, perms []models.Permission, checks ...func(*constraints)) error {
	_va := make([]interface{}, len(checks))
	for _i := range checks {
		_va[_i] = checks[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, perms)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for RequirePermissions")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, []models.Permission, ...func(*constraints)) error); ok {
		r0 = rf(ctx, perms, checks...)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NewMockAuthorizer creates a new instance of MockAuthorizer. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMockAuthorizer(t interface {
	mock.TestingT
	Cleanup(func())
}) *MockAuthorizer {
	mock := &MockAuthorizer{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
