package auth

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

func TestSCIMCaller_GetSubject(t *testing.T) {
	caller := SCIMCaller{}
	assert.Equal(t, "scim", caller.GetSubject())
}

func TestSCIMCaller_IsAdmin(t *testing.T) {
	caller := SCIMCaller{}
	assert.False(t, caller.IsAdmin())
}

func TestSCIMCaller_RequirePermissions(t *testing.T) {
	invalid := "invalid"
	teamID := "team-1"
	userID := "user-1"
	scimExternalID := "scim-1"

	caller := SCIMCaller{}
	ctx := WithCaller(context.Background(), &caller)

	testCases := []struct {
		expectErrorCode errors.CodeType
		team            *models.Team
		user            *models.User
		name            string
		perms           []models.Permission
		constraints     []func(*constraints)
	}{
		{
			name: "viewing, creating, updating a team or a user; grant access",
			perms: []models.Permission{
				models.CreateTeam,
				models.UpdateTeam,
				models.CreateUser,
				models.UpdateUser,
			},
		},
		{
			name:        "deleting a team created by SCIM",
			team:        &models.Team{SCIMExternalID: &scimExternalID},
			perms:       []models.Permission{models.DeleteTeam},
			constraints: []func(*constraints){WithTeamID(teamID)},
		},
		{
			name:            "access denied because deleting a team not created by SCIM",
			team:            &models.Team{},
			perms:           []models.Permission{models.DeleteTeam},
			constraints:     []func(*constraints){WithTeamID(teamID)},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "access denied because deleting a team that doesn't exist",
			perms:           []models.Permission{models.DeleteTeam},
			constraints:     []func(*constraints){WithTeamID(invalid)},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:        "deleting a user created by SCIM",
			user:        &models.User{SCIMExternalID: &scimExternalID},
			perms:       []models.Permission{models.DeleteUser},
			constraints: []func(*constraints){WithUserID(userID)},
		},
		{
			name:            "access denied because deleting a user not created by SCIM",
			user:            &models.User{},
			perms:           []models.Permission{models.DeleteUser},
			constraints:     []func(*constraints){WithUserID(userID)},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "access denied because deleting a user that doesn't exist",
			perms:           []models.Permission{models.DeleteUser},
			constraints:     []func(*constraints){WithUserID(invalid)},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "access denied because required constraints not provided",
			perms:           []models.Permission{models.DeleteTeam, models.DeleteUser},
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "access denied because no permissions specified",
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "access denied because permission is never available to caller",
			perms:           []models.Permission{models.CreateMembership},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockTeams := db.NewMockTeams(t)
			mockUsers := db.NewMockUsers(t)

			constraints := getConstraints(test.constraints...)

			if constraints.teamID != nil {
				mockTeams.On("GetTeamByID", mock.Anything, mock.Anything).Return(test.team, nil)
			}

			if constraints.userID != nil {
				mockUsers.On("GetUserByID", mock.Anything, mock.Anything).Return(test.user, nil)
			}

			caller.dbClient = &db.Client{Teams: mockTeams, Users: mockUsers}

			for _, perm := range test.perms {
				err := caller.RequirePermission(ctx, perm, test.constraints...)
				if test.expectErrorCode != "" {
					require.NotNil(t, err)
					assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
					return
				}
				require.Nil(t, err)
			}
		})
	}
}

func TestSCIMCaller_RequireAccessToInheritableResource(t *testing.T) {
	caller := SCIMCaller{}
	err := caller.RequireAccessToInheritableResource(WithCaller(context.Background(), &caller), models.AgentResource, nil)
	assert.Equal(t, errors.ENotFound, errors.ErrorCode(err))
}
