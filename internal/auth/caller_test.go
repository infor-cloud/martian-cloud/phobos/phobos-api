package auth

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
)

func TestSystemCaller_GetSubject(t *testing.T) {
	caller := SystemCaller{}
	assert.Equal(t, "system", caller.GetSubject())
}

func TestSystemCaller_IsAdmin(t *testing.T) {
	caller := SystemCaller{}
	assert.True(t, caller.IsAdmin())
}

func TestSystemCaller_RequirePermission(t *testing.T) {
	caller := SystemCaller{}
	assert.Nil(t, caller.RequirePermission(context.TODO(), models.ViewOrganization, nil))
}

func TestSystemCaller_RequireAccessToInheritableResource(t *testing.T) {
	caller := SystemCaller{}
	assert.Nil(t, caller.RequireAccessToInheritableResource(context.TODO(),
		models.AgentResource, nil))
}
