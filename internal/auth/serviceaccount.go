package auth

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	terrors "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// ServiceAccountCaller represents a service account subject
type ServiceAccountCaller struct {
	authorizer        Authorizer
	dbClient          *db.Client
	ServiceAccountID  string
	ServiceAccountPRN string
	authorized        bool
}

// NewServiceAccountCaller returns a new ServiceAccountCaller
func NewServiceAccountCaller(
	id,
	prn string,
	authorizer Authorizer,
	dbClient *db.Client,
) *ServiceAccountCaller {
	return &ServiceAccountCaller{
		ServiceAccountID:  id,
		ServiceAccountPRN: prn,
		authorizer:        authorizer,
		dbClient:          dbClient,
	}
}

// GetSubject returns the subject identifier for this caller
func (s *ServiceAccountCaller) GetSubject() string {
	return s.ServiceAccountPRN
}

// IsAdmin returns true if the caller is an admin
func (s *ServiceAccountCaller) IsAdmin() bool {
	return false
}

// Authorized marks the caller as authorized
func (s *ServiceAccountCaller) Authorized() {
	s.authorized = true
}

// UnauthorizedError returns the unauthorized error for this specific caller type
func (s *ServiceAccountCaller) UnauthorizedError(_ context.Context, hasViewerAccess bool) error {
	// If subject has at least viewer permissions then return 403, if not, return 404
	if hasViewerAccess {
		return terrors.New(
			"service account %s is not authorized to perform the requested operation: ensure that the service account has been added as a member to the organization or project with the role required to perform the requested operation",
			s.GetSubject(),
			terrors.WithErrorCode(terrors.EForbidden),
		)
	}

	return terrors.New(
		"either the requested resource does not exist or the service account %s is not authorized to perform the requested operation: ensure that the service account has been added as a member to the organization or project with the role required to perform the requested operation",
		s.GetSubject(),
		terrors.WithErrorCode(terrors.ENotFound),
	)
}

// RequirePermission will return an error if the caller doesn't have the specified permissions
func (s *ServiceAccountCaller) RequirePermission(ctx context.Context, perm models.Permission, checks ...func(*constraints)) error {
	if s.authorized {
		return nil
	}

	if handlerFunc, ok := s.getPermissionHandler(perm); ok {
		return handlerFunc(ctx, &perm, getConstraints(checks...))
	}

	return s.authorizer.RequirePermissions(ctx, []models.Permission{perm}, checks...)
}

// RequireAccessToInheritableResource will return an error if caller doesn't have permissions to inherited resources.
func (s *ServiceAccountCaller) RequireAccessToInheritableResource(ctx context.Context, resourceType models.ResourceType, checks ...func(*constraints)) error {
	if s.authorized {
		return nil
	}

	// If the check is for an agent resource,
	// and if the service account is assigned to the agent,
	// that needs to count as the service account having viewer permission for the agent.
	if resourceType == models.AgentResource {
		if c := getConstraints(checks...); c.agentID != nil {
			if err := s.requireAgentAccess(ctx, &models.ViewAgent, c); err == nil {
				return nil
			}
		}
	}

	return s.authorizer.RequireAccessToInheritableResource(ctx, []models.ResourceType{resourceType}, checks...)
}

// getPermissionHandler returns a permissionTypeHandler for a given permission.
func (s *ServiceAccountCaller) getPermissionHandler(perm models.Permission) (permissionTypeHandler, bool) {
	handlerMap := map[models.Permission]permissionTypeHandler{
		models.ClaimJob:           s.requireAgentAccess,
		models.CreateAgentSession: s.requireAgentAccess,
		models.UpdateAgentSession: s.requireAgentAccess,
	}

	handler, ok := handlerMap[perm]
	return handler, ok
}

// requireAgentAccess will return an error if the caller is not allowed to claim a job as the specified agent
func (s *ServiceAccountCaller) requireAgentAccess(ctx context.Context, _ *models.Permission, checks *constraints) error {
	if checks.agentID == nil {
		return errMissingConstraints
	}

	// Verify that service account is assigned to agent
	resp, err := s.dbClient.ServiceAccounts.GetServiceAccounts(ctx, &db.GetServiceAccountsInput{
		Filter: &db.ServiceAccountFilter{
			AgentID:           checks.agentID,
			ServiceAccountIDs: []string{s.ServiceAccountID},
		},
	})
	if err != nil {
		return err
	}

	if resp.PageInfo.TotalCount > 0 {
		return nil
	}

	return s.UnauthorizedError(ctx, true)
}
