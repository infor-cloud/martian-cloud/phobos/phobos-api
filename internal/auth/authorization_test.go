package auth

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

const forbiddenErrorMsg = "testSubject is not authorized to perform the requested operation"

func TestRequirePermissions(t *testing.T) {
	userID := "user-1"
	organizationID := "organization-1"

	customRole := &models.Role{
		Metadata: models.ResourceMetadata{
			ID: "role-1",
		},
	}
	customRole.SetPermissions([]models.Permission{models.CreateMembership})

	type testCase struct {
		name               string
		expectErrorMsg     string
		requirePerms       []models.Permission
		memberships        []models.Membership
		requireConstraints []func(*constraints)
	}

	testCases := []testCase{
		{
			name:               "user with default role is granted access",
			requirePerms:       []models.Permission{models.ViewOrganization, models.ViewMembership},
			requireConstraints: []func(*constraints){WithOrganizationID(organizationID)},
			memberships: []models.Membership{
				{RoleID: models.DeveloperRoleID.String(), OrganizationID: &organizationID},
			},
		},
		{
			name:               "user with custom role is granted access",
			requirePerms:       []models.Permission{models.CreateMembership},
			requireConstraints: []func(*constraints){WithOrganizationID(organizationID)},
			memberships: []models.Membership{
				{RoleID: customRole.Metadata.ID, OrganizationID: &organizationID},
			},
		},
		{
			name:               "access denied because permission is not satisfied",
			requirePerms:       []models.Permission{models.CreateMembership},
			requireConstraints: []func(*constraints){WithOrganizationID(organizationID)},
			memberships: []models.Membership{
				{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID},
			},
			expectErrorMsg: forbiddenErrorMsg,
		},
		{
			name:               "access denied because no permissions are specified",
			requireConstraints: []func(*constraints){WithOrganizationID(organizationID)},
			expectErrorMsg:     errMissingConstraints.Error(),
		},
		{
			name:           "access denied because required constraints are missing",
			requirePerms:   []models.Permission{models.ViewMembership},
			expectErrorMsg: errMissingConstraints.Error(),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := NewMockCaller(t)
			mockRoles := db.NewMockRoles(t)
			mockMemberships := db.NewMockMemberships(t)

			// Mock Caller.
			if test.expectErrorMsg == forbiddenErrorMsg {
				mockCaller.On("GetSubject").Return("testSubject").Maybe()
			}

			mockCaller.On("UnauthorizedError", mock.Anything, mock.Anything).Return(func(_ context.Context, _ bool) error {
				if test.expectErrorMsg != "" {
					return errors.New(test.expectErrorMsg)
				}
				return nil
			}).Maybe()

			if test.expectErrorMsg != errMissingConstraints.Error() {
				// Mock Organization memberships.
				dbInput := &db.GetMembershipsInput{
					Filter: &db.MembershipFilter{
						OrganizationID: &organizationID,
						UserID:         &userID,
						MembershipScopes: []models.ScopeType{
							models.OrganizationScope,
							models.GlobalScope,
						},
					},
				}

				dbResult := &db.MembershipsResult{Memberships: test.memberships, PageInfo: &pagination.PageInfo{TotalCount: int32(len(test.memberships))}}
				mockMemberships.On("GetMemberships", mock.Anything, dbInput).Return(dbResult, nil)
			}

			// Mock Roles.
			for _, m := range test.memberships {
				if m.RoleID == customRole.Metadata.ID {
					mockRoles.On("GetRoleByID", mock.Anything, customRole.Metadata.ID).Return(customRole, nil)
				}
			}

			dbClient := &db.Client{
				Roles:       mockRoles,
				Memberships: mockMemberships,
			}

			authorizer := newAuthorizer(dbClient, &userID, nil, false)

			err := authorizer.RequirePermissions(WithCaller(ctx, mockCaller), test.requirePerms, test.requireConstraints...)

			if test.expectErrorMsg != "" {
				assert.EqualError(t, err, test.expectErrorMsg)
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestRequireAccessToProject(t *testing.T) {
	sampleProjectID := "project-1"
	sampleOrgID := "org-1"
	userID := "user-1"

	type testCase struct {
		name           string
		requirePerm    *models.Permission
		memberships    *db.MembershipsResult
		expectErrorMsg string
	}

	testCases := []testCase{
		{
			name:        "user has permission in project membership",
			requirePerm: &models.ViewProject,
			memberships: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), ProjectID: &sampleProjectID},
				},
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
			},
		},
		{
			name:        "user has permission in organization membership",
			requirePerm: &models.ViewProject,
			memberships: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), OrganizationID: &sampleOrgID},
				},
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
			},
		},
		// Subject has deployer in organization membership and viewer in project membership.
		// CreatePipeline should be granted since the deployer takes precedence.
		{
			name:        "higher permission granted in organization membership",
			requirePerm: &models.ExecutePipeline,
			memberships: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), ProjectID: &sampleProjectID},
					{RoleID: models.DeveloperRoleID.String(), OrganizationID: &sampleOrgID},
				},
				PageInfo: &pagination.PageInfo{
					TotalCount: 2,
				},
			},
		},
		{
			name:        "user does not have permission",
			requirePerm: &models.CreateProject,
			memberships: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), ProjectID: &sampleProjectID},
					{RoleID: models.ViewerRoleID.String(), OrganizationID: &sampleOrgID},
				},
				PageInfo: &pagination.PageInfo{
					TotalCount: 2,
				},
			},
			expectErrorMsg: forbiddenErrorMsg,
		},
		{
			name:           "user is not a member of the organization or project",
			requirePerm:    &models.ViewProject,
			memberships:    &db.MembershipsResult{PageInfo: &pagination.PageInfo{TotalCount: 0}},
			expectErrorMsg: resourceNotFoundErrorMsg,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := NewMockCaller(t)
			mockMemberships := db.NewMockMemberships(t)

			mockCaller.On("GetSubject").Return("testSubject").Maybe()

			mockCaller.On("UnauthorizedError", mock.Anything, mock.Anything).Return(func(_ context.Context, _ bool) error {
				if test.expectErrorMsg != "" {
					return errors.New(test.expectErrorMsg)
				}
				return nil
			}).Maybe()

			membershipDBInput := &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					UserID:    &userID,
					ProjectID: &sampleProjectID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.ProjectScope,
						models.GlobalScope,
					},
				},
			}
			mockMemberships.On("GetMemberships", mock.Anything, membershipDBInput).Return(test.memberships, nil)

			dbClient := &db.Client{
				Memberships: mockMemberships,
			}

			a := newAuthorizer(dbClient, &userID, nil, false)

			err := a.requireAccessToProject(WithCaller(ctx, mockCaller), sampleProjectID, test.requirePerm)

			if test.expectErrorMsg != "" {
				assert.EqualError(t, err, test.expectErrorMsg)
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestRequireAccessToOrganization(t *testing.T) {
	userID := "user-1"
	organizationID := "organization-1"

	type testCase struct {
		requirePerm      *models.Permission
		membershipResult *db.MembershipsResult
		name             string
		expectErrorMsg   string
	}

	testCases := []testCase{
		{
			name:        "user has permission in organization",
			requirePerm: &models.ViewOrganization,
			membershipResult: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID},
				},
				PageInfo: &pagination.PageInfo{TotalCount: 1},
			},
		},
		{
			name:        "organization membership not found",
			requirePerm: &models.ViewOrganization,
			membershipResult: &db.MembershipsResult{
				PageInfo: &pagination.PageInfo{TotalCount: 0},
			},
			expectErrorMsg: resourceNotFoundErrorMsg,
		},
		{
			name:        "permission not satisfied but has viewer access",
			requirePerm: &models.CreateMembership,
			membershipResult: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID},
				},
				PageInfo: &pagination.PageInfo{TotalCount: 1},
			},
			expectErrorMsg: forbiddenErrorMsg,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := NewMockCaller(t)
			mockMemberships := db.NewMockMemberships(t)

			// Mock Caller.
			if test.expectErrorMsg == forbiddenErrorMsg {
				mockCaller.On("GetSubject").Return("testSubject").Maybe()
			}

			mockCaller.On("UnauthorizedError", mock.Anything, mock.Anything).Return(func(_ context.Context, _ bool) error {
				if test.expectErrorMsg != "" {
					return errors.New(test.expectErrorMsg)
				}
				return nil
			}).Maybe()

			// Mock Organization memberships.
			membershipDBInput := &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					OrganizationID: &organizationID,
					UserID:         &userID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.GlobalScope,
					},
				},
			}

			mockMemberships.On("GetMemberships", mock.Anything, membershipDBInput).Return(test.membershipResult, nil)

			dbClient := &db.Client{
				Memberships: mockMemberships,
			}

			a := newAuthorizer(dbClient, &userID, nil, false)

			err := a.requireAccessToOrganization(WithCaller(ctx, mockCaller), organizationID, test.requirePerm)

			if test.expectErrorMsg != "" {
				assert.EqualError(t, err, test.expectErrorMsg)
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestRequireAccessToInheritedOrganizationResources(t *testing.T) {
	userID := "user-1"
	projectID := "project-1"
	organizationID := "organization-1"

	customRole := &models.Role{
		Metadata: models.ResourceMetadata{
			ID: "role-1",
		},
	}
	customRole.SetPermissions([]models.Permission{models.ViewJob})

	type testCase struct {
		membershipResult *db.MembershipsResult
		name             string
		expectErrorMsg   string
	}

	testCases := []testCase{
		{
			name: "permission satisfied from project membership",
			membershipResult: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), ProjectID: &projectID},
				},
				PageInfo: &pagination.PageInfo{TotalCount: 1},
			},
		},
		{
			name: "permission satisfied from organization membership",
			membershipResult: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID},
				},
				PageInfo: &pagination.PageInfo{TotalCount: 1},
			},
		},
		{
			name: "has both project and organization membership",
			membershipResult: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: models.ViewerRoleID.String(), ProjectID: &projectID},
					{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID},
				},
				PageInfo: &pagination.PageInfo{TotalCount: 2},
			},
		},
		// We use a custom role to demonstrate that the permission is not satisfied.
		{
			name: "permission not satisfied for any membership",
			membershipResult: &db.MembershipsResult{
				Memberships: []models.Membership{
					{RoleID: customRole.Metadata.ID, ProjectID: &projectID},
				},
				PageInfo: &pagination.PageInfo{TotalCount: 1},
			},
			expectErrorMsg: resourceNotFoundErrorMsg,
		},
		{
			name:             "no memberships found for organization",
			membershipResult: &db.MembershipsResult{PageInfo: &pagination.PageInfo{TotalCount: 0}},
			expectErrorMsg:   resourceNotFoundErrorMsg,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := NewMockCaller(t)
			mockMemberships := db.NewMockMemberships(t)
			mockRoles := db.NewMockRoles(t)

			mockCaller.On("UnauthorizedError", mock.Anything, mock.Anything).Return(func(_ context.Context, _ bool) error {
				if test.expectErrorMsg != "" {
					return errors.New(test.expectErrorMsg)
				}
				return nil
			}).Maybe()

			// Mock Memberships.
			membershipDBInput := &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					UserID:         &userID,
					OrganizationID: &organizationID,
					MembershipScopes: []models.ScopeType{
						models.OrganizationScope,
						models.ProjectScope,
						models.GlobalScope,
					},
				},
			}

			mockMemberships.On("GetMemberships", mock.Anything, membershipDBInput).Return(test.membershipResult, nil)

			// Mock Roles.
			for _, m := range test.membershipResult.Memberships {
				if m.RoleID == customRole.Metadata.ID {
					mockRoles.On("GetRoleByID", mock.Anything, customRole.Metadata.ID).Return(customRole, nil)
				}
			}

			dbClient := &db.Client{
				Memberships: mockMemberships,
				Roles:       mockRoles,
			}

			a := newAuthorizer(dbClient, &userID, nil, false)

			err := a.requireAccessToInheritedOrganizationResources(WithCaller(ctx, mockCaller), organizationID, &models.ViewAgent)

			if test.expectErrorMsg != "" {
				assert.EqualError(t, err, test.expectErrorMsg)
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestRequirePermission(t *testing.T) {
	customRole := &models.Role{
		Metadata: models.ResourceMetadata{
			ID: "role-1",
		},
	}
	customRole.SetPermissions([]models.Permission{models.ViewJob})

	type testCase struct {
		requirePerm     *models.Permission
		name            string
		expectErrorMsg  string
		membershipsList []models.Membership
	}

	testCases := []testCase{
		{
			name:            "permission satisfied",
			requirePerm:     &models.ViewAgent,
			membershipsList: []models.Membership{{RoleID: models.ViewerRoleID.String()}},
		},
		{
			name:            "permission not satisfied",
			requirePerm:     &models.ViewAgent,
			membershipsList: []models.Membership{{RoleID: customRole.Metadata.ID}},
			expectErrorMsg:  resourceNotFoundErrorMsg,
		},
		{
			name:        "permission satisfied for at least one of the required memberships",
			requirePerm: &models.ViewAgent,
			membershipsList: []models.Membership{
				{RoleID: customRole.Metadata.ID},
				{RoleID: models.ViewerRoleID.String()},
			},
		},
		{
			name:        "forbidden error returned since caller has viewer access but not the required permission",
			requirePerm: &models.CreateMembership,
			membershipsList: []models.Membership{
				{RoleID: models.ViewerRoleID.String()},
			},
			expectErrorMsg: forbiddenErrorMsg,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := NewMockCaller(t)
			mockRoles := db.NewMockRoles(t)

			if test.expectErrorMsg == forbiddenErrorMsg {
				mockCaller.On("GetSubject").Return("testSubject").Maybe()
			}

			mockCaller.On("UnauthorizedError", mock.Anything, mock.Anything).Return(func(_ context.Context, _ bool) error {
				if test.expectErrorMsg != "" {
					return errors.New(test.expectErrorMsg)
				}
				return nil
			}).Maybe()

			// Mock Roles.
			for _, m := range test.membershipsList {
				if m.RoleID == customRole.Metadata.ID {
					mockRoles.On("GetRoleByID", mock.Anything, customRole.Metadata.ID).Return(customRole, nil).Once()
				}
			}

			dbClient := &db.Client{
				Roles: mockRoles,
			}

			a := newAuthorizer(dbClient, nil, nil, false)

			err := a.requirePermission(WithCaller(ctx, mockCaller), test.membershipsList, test.requirePerm)

			if test.expectErrorMsg != "" {
				assert.EqualError(t, err, test.expectErrorMsg)
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestIsPermissionCached(t *testing.T) {
	organizationID := "organization-1"
	projectID := "project-1"

	type testCase struct {
		name           string
		inputKey       cacheKey
		requirePerm    *models.Permission
		memberships    []models.Membership
		expectCacheHit bool
	}

	testCases := []testCase{
		{
			name:        "cache hit for organization id",
			inputKey:    cacheKey{organizationID: &organizationID},
			requirePerm: &models.ViewOrganization,
			memberships: []models.Membership{
				{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID, Scope: models.OrganizationScope},
			},
			expectCacheHit: true,
		},
		{
			name:        "cache hit for project id",
			inputKey:    cacheKey{projectID: &projectID},
			requirePerm: &models.ViewProject,
			memberships: []models.Membership{
				{RoleID: models.ViewerRoleID.String(), ProjectID: &projectID, Scope: models.ProjectScope},
			},
			expectCacheHit: true,
		},
		{
			name:        "cache hit for global membership",
			inputKey:    cacheKey{organizationID: &organizationID},
			requirePerm: &models.ViewAgent,
			memberships: []models.Membership{
				{RoleID: models.OwnerRoleID.String(), Scope: models.GlobalScope},
			},
			expectCacheHit: true,
		},
		{
			name:        "both organization and global membership, cache hit for global membership",
			inputKey:    cacheKey{organizationID: &organizationID},
			requirePerm: &models.CreateProject,
			memberships: []models.Membership{
				{RoleID: models.OwnerRoleID.String(), Scope: models.GlobalScope},
				{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID, Scope: models.OrganizationScope},
			},
			expectCacheHit: true,
		},
		{
			name:        "both project and global membership, cache hit for global membership",
			inputKey:    cacheKey{projectID: &projectID},
			requirePerm: &models.CreateProject,
			memberships: []models.Membership{
				{RoleID: models.OwnerRoleID.String(), Scope: models.GlobalScope},
				{RoleID: models.ViewerRoleID.String(), ProjectID: &projectID, Scope: models.ProjectScope},
			},
			expectCacheHit: true,
		},
		{
			name:        "cache miss because role does not have required permission",
			inputKey:    cacheKey{organizationID: &organizationID},
			requirePerm: &models.CreateMembership,
			memberships: []models.Membership{
				{RoleID: models.ViewerRoleID.String(), OrganizationID: &organizationID, Scope: models.OrganizationScope},
			},
		},
		{
			name:        "cache miss because cache is empty",
			inputKey:    cacheKey{organizationID: &organizationID},
			requirePerm: &models.UpdateMembership,
			memberships: []models.Membership{},
		},
	}

	for _, test := range testCases {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		a := newAuthorizer(nil, nil, nil, true)

		// Populate cache.
		for ix := range test.memberships {
			_, _ = a.getPermissionsFromMembership(ctx, &test.memberships[ix])
		}

		actualCacheHit := a.isPermissionCached(&test.inputKey, test.requirePerm)
		assert.Equal(t, test.expectCacheHit, actualCacheHit)
	}
}
