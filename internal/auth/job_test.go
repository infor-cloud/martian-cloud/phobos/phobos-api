package auth

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

func TestJobCaller_GetSubject(t *testing.T) {
	caller := JobCaller{JobID: "job1"}
	assert.Equal(t, "job1", caller.GetSubject())
}

func TestJobCaller_IsAdmin(t *testing.T) {
	caller := JobCaller{JobID: "job1"}
	assert.False(t, caller.IsAdmin())
}

func TestJobCaller_RequireAccessToInheritableResource(t *testing.T) {
	jobID := "job1"

	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID:  "proj1",
			PRN: "prn-part-0:prn-part-1:org1/proj1", // to keep project's GetOrgName from hitting index out of range
		},
		OrgID: "org1",
	}

	caller := JobCaller{
		JobID:     jobID,
		ProjectID: project.Metadata.ID,
	}

	ctx := WithCaller(context.Background(), &caller)

	testCases := []struct {
		expectErrorCode errors.CodeType
		name            string
		permType        models.ResourceType
		constraints     []func(*constraints)
	}{
		{
			name:        "job is requesting access to an inheritable resource in its own organization",
			permType:    models.AgentResource,
			constraints: []func(*constraints){WithOrganizationID("org1")},
		},
		{
			name:            "job is requesting access to an inheritable resource in an organization it does not have access to",
			permType:        models.AgentResource,
			constraints:     []func(*constraints){WithOrganizationID("org2")},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockProjects := db.NewMockProjects(t)

			mockProjects.On("GetProjectByID", mock.Anything, caller.ProjectID).Return(project, nil).Maybe()

			caller.dbClient = &db.Client{
				Projects: mockProjects,
			}

			err := caller.RequireAccessToInheritableResource(ctx, test.permType, test.constraints...)
			if test.expectErrorCode != "" {
				require.NotNil(t, err)
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
			require.Nil(t, err)
		})
	}
}

func TestJobCaller_RequirePermissions(t *testing.T) {
	jobID := "job1"

	job := &models.Job{
		Metadata: models.ResourceMetadata{
			ID: jobID,
		},
		Data: &models.JobTaskData{
			PipelineID: "pipeline1",
			TaskPath:   "pipeline.stage.one.task.one",
		},
	}

	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID:  "proj1",
			PRN: "prn-part-0:prn-part-1:org1/proj1", // to keep project's GetOrgName from hitting index out of range
		},
	}

	caller := JobCaller{
		JobID:     jobID,
		ProjectID: project.Metadata.ID,
	}

	ctx := WithCaller(context.Background(), &caller)

	testCases := []struct {
		expectErrorCode errors.CodeType
		name            string
		injectJob       *models.Job
		injectProject   *models.Project
		perms           models.Permission
		constraints     []func(*constraints)
	}{
		{
			name:        "job is requesting access to view itself",
			perms:       models.ViewJob,
			constraints: []func(*constraints){WithJobID(caller.JobID)},
		},
		{
			name:            "job is requesting access to view a job it does not have access to",
			perms:           models.ViewJob,
			constraints:     []func(*constraints){WithJobID("job2")},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:        "job is requesting access to update itself",
			perms:       models.UpdateJobState,
			constraints: []func(*constraints){WithJobID(caller.JobID)},
		},
		{
			name:            "access denied because job is requesting access to another job",
			perms:           models.UpdateJobState,
			constraints:     []func(*constraints){WithJobID("job2")},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:        "job is requesting access to update a pipeline task it has access to",
			perms:       models.UpdatePipelineState,
			injectJob:   job,
			constraints: []func(*constraints){WithJobID(caller.JobID), WithPipelineID("pipeline1"), WithPipelineTask("pipeline.stage.one.task.one")},
		},
		{
			name:            "job is requesting access to update a pipeline it does not have access to",
			perms:           models.UpdatePipelineState,
			injectJob:       job,
			constraints:     []func(*constraints){WithJobID(caller.JobID), WithPipelineID("pipeline2"), WithPipelineTask("pipeline.stage.one.task.one")},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "job is requesting access to update a pipeline task it does not have access to",
			perms:           models.UpdatePipelineState,
			injectJob:       job,
			constraints:     []func(*constraints){WithJobID(caller.JobID), WithPipelineID("pipeline1"), WithPipelineTask("pipeline.stage.one.task.two")},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:        "job is requesting access to view a pipeline it has access to",
			perms:       models.ViewPipeline,
			injectJob:   job,
			constraints: []func(*constraints){WithJobID(caller.JobID), WithPipelineID("pipeline1")},
		},
		{
			name:            "job is requesting access to view a pipeline it does not have access to",
			perms:           models.ViewPipeline,
			injectJob:       job,
			constraints:     []func(*constraints){WithJobID(caller.JobID), WithPipelineID("pipeline2")},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:          "job is requesting access to view a pipeline template in a project it has access to",
			perms:         models.ViewPipelineTemplate,
			injectJob:     job,
			injectProject: project,
			constraints:   []func(*constraints){WithJobID(caller.JobID), WithProjectID(project.Metadata.ID)},
		},
		{
			name:            "job is requesting access to view a pipeline template it does not have access to",
			perms:           models.ViewPipelineTemplate,
			injectJob:       job,
			injectProject:   project,
			constraints:     []func(*constraints){WithJobID(caller.JobID), WithProjectID("project2")},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "access denied because no permissions specified",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "access denied because permission is never available to caller",
			perms:           models.CreateProject,
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockJobs := db.NewMockJobs(t)
			mockProjects := db.NewMockProjects(t)

			mockProjects.On("GetProjectByID", mock.Anything, caller.ProjectID).Return(test.injectProject, nil).Maybe()
			mockJobs.On("GetJobByID", mock.Anything, caller.JobID).Return(test.injectJob, nil).Maybe()

			caller.dbClient = &db.Client{
				Jobs:     mockJobs,
				Projects: mockProjects,
			}

			err := caller.RequirePermission(ctx, test.perms, test.constraints...)
			if test.expectErrorCode != "" {
				require.NotNil(t, err)
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
			require.Nil(t, err)
		})
	}
}
