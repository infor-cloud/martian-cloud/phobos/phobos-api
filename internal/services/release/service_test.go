package release

import (
	"bytes"
	"context"
	"io"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	lifecycleTemplateDataStore := lifecycletemplate.NewMockDataStore(t)
	pipelineTemplateDataStore := pipelinetemplate.NewMockDataStore(t)
	pipelineTemplateService := pipelinetemplate.NewMockService(t)
	pipelineService := pipeline.NewMockService(t)
	limitChecker := limits.NewMockLimitChecker(t)
	activityService := activityevent.NewMockService(t)
	releaseDataStore := NewMockDataStore(t)

	expect := &service{
		logger:                     logger,
		dbClient:                   dbClient,
		lifecycleTemplateDataStore: lifecycleTemplateDataStore,
		pipelineTemplateDataStore:  pipelineTemplateDataStore,
		pipelineTemplateService:    pipelineTemplateService,
		pipelineService:            pipelineService,
		limitChecker:               limitChecker,
		templateBuilder:            NewTemplateBuilder(),
		activityService:            activityService,
		releaseDataStore:           releaseDataStore,
	}

	assert.Equal(t, expect, NewService(
		logger,
		dbClient,
		lifecycleTemplateDataStore,
		pipelineTemplateDataStore,
		pipelineTemplateService,
		pipelineService,
		limitChecker,
		activityService,
		releaseDataStore,
	))
}

func TestGetReleaseByID(t *testing.T) {
	sampleRelease := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		release         *models.Release
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully get release by id",
			release: sampleRelease,
		},
		{
			name:            "release not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "subject is not authorized to get release",
			release:         sampleRelease,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)

			mockReleases.On("GetReleaseByID", mock.Anything, sampleRelease.Metadata.ID).Return(test.release, nil)

			if test.release != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewRelease, mock.Anything, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Releases: mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			release, err := service.GetReleaseByID(auth.WithCaller(ctx, mockCaller), sampleRelease.Metadata.ID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, sampleRelease, release)
		})
	}
}

func TestGetReleaseByPRN(t *testing.T) {
	sampleRelease := &models.Release{
		Metadata: models.ResourceMetadata{
			ID:  "release-1",
			PRN: models.ReleaseResource.BuildPRN("org-1", "project-1", "release-1"),
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		release         *models.Release
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully get release by PRN",
			release: sampleRelease,
		},
		{
			name:            "release not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "subject is not authorized to get release",
			release:         sampleRelease,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)

			mockReleases.On("GetReleaseByPRN", mock.Anything, sampleRelease.Metadata.PRN).Return(test.release, nil)

			if test.release != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewRelease, mock.Anything, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Releases: mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			release, err := service.GetReleaseByPRN(auth.WithCaller(ctx, mockCaller), sampleRelease.Metadata.PRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, sampleRelease, release)
		})
	}
}

func TestGetReleaseByIDs(t *testing.T) {
	sampleRelease := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		release         *models.Release
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully get releases by ids",
			release: sampleRelease,
		},
		{
			name:            "subject is not authorized to get releases",
			release:         sampleRelease,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)

			mockReleases.On("GetReleases", mock.Anything, &db.GetReleasesInput{
				Filter: &db.ReleaseFilter{
					ReleaseIDs: []string{test.release.Metadata.ID},
				},
			}).Return(&db.ReleasesResult{
				Releases: []*models.Release{test.release},
			}, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewRelease, mock.Anything, mock.Anything).Return(test.authError).Times(1)

			dbClient := &db.Client{
				Releases: mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			releases, err := service.GetReleasesByIDs(auth.WithCaller(ctx, mockCaller), []string{test.release.Metadata.ID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Len(t, releases, 1)
			assert.Equal(t, []*models.Release{test.release}, releases)
		})
	}
}

func TestGetReleaseNotes(t *testing.T) {
	sampleRelease := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		release         *models.Release
		releaseNotes    string
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:         "successfully get release notes",
			release:      sampleRelease,
			releaseNotes: "release 0.0.1",
		},
		{
			name:            "release not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "subject is not authorized to get release",
			release:         sampleRelease,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)

			mockReleaseDataStore := NewMockDataStore(t)

			mockReleaseDataStore.On("GetReleaseNotes", mock.Anything, sampleRelease.Metadata.ID).Return(io.NopCloser(strings.NewReader(test.releaseNotes)), nil).Maybe()

			mockReleases.On("GetReleaseByID", mock.Anything, sampleRelease.Metadata.ID).Return(test.release, nil)

			if test.release != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewRelease, mock.Anything, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Releases: mockReleases,
			}

			service := &service{
				dbClient:         dbClient,
				releaseDataStore: mockReleaseDataStore,
			}

			releaseNotes, err := service.GetReleaseNotes(auth.WithCaller(ctx, mockCaller), sampleRelease.Metadata.ID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.releaseNotes, releaseNotes)
		})
	}
}

func TestGetReleases(t *testing.T) {
	sampleRelease := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		release         *models.Release
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully get releases",
			release: sampleRelease,
		},
		{
			name:            "subject is not authorized to get releases",
			release:         sampleRelease,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewRelease, mock.Anything, mock.Anything).Return(test.authError).Times(1)

			if test.expectErrorCode == "" {
				mockReleases.On("GetReleases", mock.Anything, &db.GetReleasesInput{
					Filter: &db.ReleaseFilter{
						ProjectID: ptr.String(test.release.ProjectID),
					},
				}).Return(&db.ReleasesResult{
					Releases: []*models.Release{test.release},
				}, nil)
			}

			dbClient := &db.Client{
				Releases: mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			releasesResult, err := service.GetReleases(auth.WithCaller(ctx, mockCaller), &GetReleasesInput{
				ProjectID: &test.release.ProjectID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Len(t, releasesResult.Releases, 1)
			assert.Equal(t, []*models.Release{test.release}, releasesResult.Releases)
		})
	}
}

func TestCreateRelease(t *testing.T) {
	testSubject := "testSubject"
	projectID := "project-1"
	organizationID := "org-1"
	releaseID := "release-1"
	semanticVersion := "1.0.0"
	currentTime := time.Now().UTC()

	sampleDeploymentTemplatesInput := []DeploymentTemplateInput{
		{
			EnvironmentName:    "account1",
			PipelineTemplateID: "pipeline-template-1",
		},
		{
			EnvironmentName:    "account2",
			PipelineTemplateID: "pipeline-template-1",
		},
	}

	samplePipelineTemplate := &models.PipelineTemplate{
		Metadata: models.ResourceMetadata{
			ID: "pipeline-template-1",
		},
		ProjectID: projectID,
		Status:    models.PipelineTemplateUploaded,
		Versioned: true,
	}

	sampleOrgReleaseLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: "release-lifecycle-1",
		},
		EnvironmentNames:    []string{"account1", "account2"},
		LifecycleTemplateID: "lifecycle-template-1",
		Scope:               models.OrganizationScope,
		OrganizationID:      organizationID,
	}

	sampleProjReleaseLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: "release-lifecycle-1",
		},
		EnvironmentNames:    []string{"account1", "account2"},
		LifecycleTemplateID: "lifecycle-template-1",
		Scope:               models.ProjectScope,
		ProjectID:           &projectID,
	}

	// sampleValidLifecycleTemplate is the sample lifecycle template data
	sampleValidLifecycleTemplate := `
	plugin exec {}

	stage dev {
	  deployment {
		  environment = "account1"
	  }
	  deployment {
		  environment = "account2"
	  }
	}`

	sampleConvertedPipelineTemplate := `
	plugin exec {}

	stage dev {
		pipeline "deployment_account1" {
			environment   = "account1"
			pipeline_type = "deployment"
			template_id   = system.deployment_account1_template_id
			when          = "manual"
			variables     = system.deployment_account1_vars
		}
		pipeline "deployment_account2" {
			environment   = "account2"
			pipeline_type = "deployment"
			template_id   = system.deployment_account2_template_id
			when          = "manual"
			variables     = system.deployment_account2_vars
		}
	}`

	sampleDeploymentTemplate := `
	plugin exec {}

	variable "some_string" {
		type    = string
		default = "some_default_value"
	}

	variable "some_list" {
		type    = list(string)
		default = ["some_default_value"]
	}

	stage dev {
		task "t1" {
			action "exec_command" {
				command = <<EOF
					echo "hello world"
				EOF
			}
		}
	}`

	// Must parse the converted template since the bytes will be different
	// if casted directly.
	pipelineTemplate, diags := hclwrite.ParseConfig([]byte(sampleConvertedPipelineTemplate), "pipeline.hcl", hcl.InitialPos)
	require.Nil(t, diags)

	type testCase struct {
		authError                   error
		limitExceededError          error
		lifecycle                   *models.ReleaseLifecycle
		deploymentTemplate          *models.PipelineTemplate
		input                       *CreateReleaseInput
		lifecycleTemplateData       string
		name                        string
		currentLatestReleaseVersion *string
		expectLatest                bool
		expectPreRelease            bool
		expectErrorCode             errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create latest release",
			input: &CreateReleaseInput{
				ProjectID:           projectID,
				LifecycleID:         sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:     "1.0.0",
				DeploymentTemplates: sampleDeploymentTemplatesInput,
				DueDate:             ptr.Time(time.Now().UTC().Add(time.Hour)),
				UserParticipantIDs:  []string{"user-1"},
				Variables: []VariableInput{
					{
						Key:      "some_string",
						Value:    "some new value",
						Category: models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account1"),
						Key:             "some_list",
						Value:           "[\"some_new_value\"]",
						Category:        models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account2"),
						Key:             "some_list",
						Value:           "[\"some_other_value\"]",
						Category:        models.HCLVariable,
					},
				},
			},
			lifecycle:             sampleOrgReleaseLifecycle,
			deploymentTemplate:    samplePipelineTemplate,
			lifecycleTemplateData: sampleValidLifecycleTemplate,
			expectLatest:          true,
		},
		{
			name: "successfully create pre-release",
			input: &CreateReleaseInput{
				ProjectID:           projectID,
				LifecycleID:         sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:     "1.0.0-beta",
				DeploymentTemplates: sampleDeploymentTemplatesInput,
				DueDate:             ptr.Time(time.Now().UTC().Add(time.Hour)),
				UserParticipantIDs:  []string{"user-1"},
				Variables: []VariableInput{
					{
						Key:      "some_string",
						Value:    "some new value",
						Category: models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account1"),
						Key:             "some_list",
						Value:           "[\"some_new_value\"]",
						Category:        models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account2"),
						Key:             "some_list",
						Value:           "[\"some_other_value\"]",
						Category:        models.HCLVariable,
					},
				},
			},
			lifecycle:                   sampleOrgReleaseLifecycle,
			deploymentTemplate:          samplePipelineTemplate,
			lifecycleTemplateData:       sampleValidLifecycleTemplate,
			expectLatest:                false,
			expectPreRelease:            true,
			currentLatestReleaseVersion: ptr.String("0.1.0"),
		},
		{
			name: "successfully create new release wich is now the latest",
			input: &CreateReleaseInput{
				ProjectID:           projectID,
				LifecycleID:         sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:     "1.0.0",
				DeploymentTemplates: sampleDeploymentTemplatesInput,
				DueDate:             ptr.Time(time.Now().UTC().Add(time.Hour)),
				UserParticipantIDs:  []string{"user-1"},
				Variables: []VariableInput{
					{
						Key:      "some_string",
						Value:    "some new value",
						Category: models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account1"),
						Key:             "some_list",
						Value:           "[\"some_new_value\"]",
						Category:        models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account2"),
						Key:             "some_list",
						Value:           "[\"some_other_value\"]",
						Category:        models.HCLVariable,
					},
				},
			},
			lifecycle:                   sampleOrgReleaseLifecycle,
			deploymentTemplate:          samplePipelineTemplate,
			lifecycleTemplateData:       sampleValidLifecycleTemplate,
			expectLatest:                true,
			currentLatestReleaseVersion: ptr.String("0.1.0"),
		},
		{
			name: "subject is not authorized to create release",
			input: &CreateReleaseInput{
				ProjectID: projectID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "release lifecycle not found",
			input: &CreateReleaseInput{
				ProjectID:   projectID,
				LifecycleID: sampleOrgReleaseLifecycle.Metadata.ID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "org release lifecycle is outside organization",
			input: &CreateReleaseInput{
				ProjectID:   projectID,
				LifecycleID: sampleOrgReleaseLifecycle.Metadata.ID,
			},
			lifecycle: &models.ReleaseLifecycle{
				Scope:          models.OrganizationScope,
				OrganizationID: "org-2",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "project release lifecycle is outside organization",
			input: &CreateReleaseInput{
				ProjectID:   projectID,
				LifecycleID: sampleProjReleaseLifecycle.Metadata.ID,
			},
			lifecycle: &models.ReleaseLifecycle{
				Scope:     models.ProjectScope,
				ProjectID: ptr.String("proj-2"),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "supplied semantic version is invalid",
			input: &CreateReleaseInput{
				ProjectID:       projectID,
				LifecycleID:     sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion: "invalid",
			},
			lifecycle:       sampleOrgReleaseLifecycle,
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "release lifecycle template is invalid",
			input: &CreateReleaseInput{
				ProjectID:       projectID,
				LifecycleID:     sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion: semanticVersion,
			},
			lifecycle:             sampleOrgReleaseLifecycle,
			lifecycleTemplateData: "invalid",
			expectErrorCode:       errors.EInvalid,
		},
		{
			name: "duplicate global variables are not allowed",
			input: &CreateReleaseInput{
				ProjectID:       projectID,
				LifecycleID:     sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion: semanticVersion,
				Variables: []VariableInput{
					{
						Key:      "some_string",
						Value:    "some new value",
						Category: models.HCLVariable,
					},
					{
						Key:      "some_string",
						Value:    "some new value",
						Category: models.HCLVariable,
					},
				},
			},
			lifecycle:             sampleOrgReleaseLifecycle,
			lifecycleTemplateData: sampleValidLifecycleTemplate,
			expectErrorCode:       errors.EInvalid,
		},
		{
			name: "duplicate deployment variables are not allowed",
			input: &CreateReleaseInput{
				ProjectID:       projectID,
				LifecycleID:     sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion: semanticVersion,
				Variables: []VariableInput{
					{
						EnvironmentName: ptr.String("account1"),
						Key:             "some_string",
						Value:           "some new value",
						Category:        models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account1"),
						Key:             "some_string",
						Value:           "some new value",
						Category:        models.HCLVariable,
					},
				},
			},
			lifecycle:             sampleOrgReleaseLifecycle,
			lifecycleTemplateData: sampleValidLifecycleTemplate,
			expectErrorCode:       errors.EInvalid,
		},
		{
			name: "deployment variable was already declared as a global variable",
			input: &CreateReleaseInput{
				ProjectID:       projectID,
				LifecycleID:     sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion: semanticVersion,
				Variables: []VariableInput{
					{
						Key:      "some_string",
						Value:    "some new value",
						Category: models.HCLVariable,
					},
					{
						EnvironmentName: ptr.String("account1"),
						Key:             "some_string",
						Value:           "some new value",
						Category:        models.HCLVariable,
					},
				},
			},
			lifecycle:             sampleOrgReleaseLifecycle,
			lifecycleTemplateData: sampleValidLifecycleTemplate,
			expectErrorCode:       errors.EInvalid,
		},
		{
			name: "cannot target an environment variable to a deployment",
			input: &CreateReleaseInput{
				ProjectID:       projectID,
				LifecycleID:     sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion: semanticVersion,
				Variables: []VariableInput{
					{
						EnvironmentName: ptr.String("account1"),
						Key:             "some_string",
						Value:           "some new value",
						Category:        models.EnvironmentVariable,
					},
				},
			},
			lifecycle:             sampleOrgReleaseLifecycle,
			lifecycleTemplateData: sampleValidLifecycleTemplate,
			expectErrorCode:       errors.EInvalid,
		},
		{
			name: "deployment template not found",
			input: &CreateReleaseInput{
				ProjectID:           projectID,
				LifecycleID:         sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:     semanticVersion,
				DeploymentTemplates: sampleDeploymentTemplatesInput,
			},
			lifecycle:             sampleOrgReleaseLifecycle,
			lifecycleTemplateData: sampleValidLifecycleTemplate,
			expectErrorCode:       errors.EInvalid,
		},
		{
			name: "deployment template outside project",
			input: &CreateReleaseInput{
				ProjectID:           projectID,
				LifecycleID:         sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:     semanticVersion,
				DeploymentTemplates: sampleDeploymentTemplatesInput,
			},
			lifecycle: sampleOrgReleaseLifecycle,
			deploymentTemplate: &models.PipelineTemplate{
				ProjectID: "project-2",
				Versioned: true,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "deployment template is not uploaded",
			input: &CreateReleaseInput{
				ProjectID:           projectID,
				LifecycleID:         sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:     semanticVersion,
				DeploymentTemplates: sampleDeploymentTemplatesInput,
			},
			lifecycle: sampleOrgReleaseLifecycle,
			deploymentTemplate: &models.PipelineTemplate{
				ProjectID: projectID,
				Status:    models.PipelineTemplatePending,
				Versioned: true,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "deployment template must be versioned",
			input: &CreateReleaseInput{
				ProjectID:           projectID,
				LifecycleID:         sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:     semanticVersion,
				DeploymentTemplates: sampleDeploymentTemplatesInput,
			},
			lifecycle: sampleOrgReleaseLifecycle,
			deploymentTemplate: &models.PipelineTemplate{
				ProjectID: projectID,
				Status:    models.PipelineTemplatePending,
				Versioned: false,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "release due date must not be in the past",
			input: &CreateReleaseInput{
				ProjectID:          projectID,
				LifecycleID:        sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion:    semanticVersion,
				DueDate:            ptr.Time(time.Now().UTC().Add(-time.Hour)),
				UserParticipantIDs: []string{"user-1"},
			},
			lifecycle:       sampleOrgReleaseLifecycle,
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "release must be associated with at least one participant",
			input: &CreateReleaseInput{
				ProjectID:       projectID,
				LifecycleID:     sampleOrgReleaseLifecycle.Metadata.ID,
				SemanticVersion: semanticVersion,
			},
			lifecycle:       sampleOrgReleaseLifecycle,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)
			mockLifecycles := db.NewMockReleaseLifecycles(t)
			mockPipelinesService := pipeline.NewMockService(t)
			mockLifecycleDataStore := lifecycletemplate.NewMockDataStore(t)
			mockPipelineTemplateDataStore := pipelinetemplate.NewMockDataStore(t)
			mockPipelineTemplatesService := pipelinetemplate.NewMockService(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockLimits := limits.NewMockLimitChecker(t)
			mockTransactions := db.NewMockTransactions(t)
			mockProjects := db.NewMockProjects(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockReleaseDataStore := NewMockDataStore(t)

			mockCaller.On("RequirePermission", mock.Anything, models.CreateRelease, mock.Anything).Return(test.authError)
			mockCaller.On("GetSubject").Return(testSubject).Maybe()
			mockCaller.On("Authorized").Maybe()

			if test.authError == nil {
				mockLifecycles.On("GetReleaseLifecycleByID", mock.Anything, sampleOrgReleaseLifecycle.Metadata.ID).Return(test.lifecycle, nil)
			}

			if test.lifecycle != nil {
				mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(&models.Project{
					Metadata: models.ResourceMetadata{
						ID: test.input.ProjectID,
					},
					OrgID: organizationID,
				}, nil)

			}

			mockLifecycleDataStore.On("GetData", mock.Anything, sampleOrgReleaseLifecycle.LifecycleTemplateID).
				Return(io.NopCloser(strings.NewReader(test.lifecycleTemplateData)), nil).Maybe()

			mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, samplePipelineTemplate.Metadata.ID).Return(test.deploymentTemplate, nil).Maybe()

			mockPipelineTemplateDataStore.On("GetData", mock.Anything, samplePipelineTemplate.Metadata.ID).Return(
				func(_ context.Context, _ string) (io.ReadCloser, error) {
					return io.NopCloser(strings.NewReader(sampleDeploymentTemplate)), nil
				},
			).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			mockReleases.On("CreateRelease", mock.Anything, &models.Release{
				SemanticVersion:    test.input.SemanticVersion,
				ProjectID:          projectID,
				CreatedBy:          testSubject,
				DueDate:            test.input.DueDate,
				UserParticipantIDs: test.input.UserParticipantIDs,
				Latest:             test.expectLatest,
				PreRelease:         test.expectPreRelease,
			}).Return(&models.Release{
				Metadata: models.ResourceMetadata{
					ID:                releaseID,
					CreationTimestamp: &currentTime,
				},
				ProjectID:          projectID,
				SemanticVersion:    test.input.SemanticVersion,
				CreatedBy:          testSubject,
				DueDate:            test.input.DueDate,
				UserParticipantIDs: test.input.UserParticipantIDs,
				Latest:             test.expectLatest,
				PreRelease:         test.expectPreRelease,
			}, nil).Maybe()

			// This is the query to get the existing latest release
			mockReleases.On("GetReleases", mock.Anything, &db.GetReleasesInput{
				Filter: &db.ReleaseFilter{
					ProjectID: &projectID,
					Latest:    ptr.Bool(true),
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			}).Return(func(_ context.Context, _ *db.GetReleasesInput) *db.ReleasesResult {
				if test.currentLatestReleaseVersion == nil {
					return &db.ReleasesResult{}
				}

				return &db.ReleasesResult{
					Releases: []*models.Release{
						{
							SemanticVersion: *test.currentLatestReleaseVersion,
							Latest:          true,
						},
					},
				}
			}, nil).Maybe()

			// If the new release is latest then we need to mock out the call to update the current latest
			if test.currentLatestReleaseVersion != nil && test.expectLatest {
				mockReleases.On("UpdateRelease", mock.Anything, &models.Release{
					SemanticVersion: *test.currentLatestReleaseVersion,
					Latest:          false,
				}).Return(nil, nil).Once()
			}

			// This is the query to check if the new release violates the release limit
			mockReleases.On("GetReleases", mock.Anything, &db.GetReleasesInput{
				Filter: &db.ReleaseFilter{
					TimeRangeStart: ptr.Time(currentTime.Add(-limits.ResourceLimitTimePeriod)),
					ProjectID:      &projectID,
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
			}).Return(&db.ReleasesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
			}, nil).Maybe()

			mockLimits.On("CheckLimit", mock.Anything, limits.ResourceLimitReleasesPerProjectPerTimePeriod, int32(1)).Return(test.limitExceededError).Maybe()

			if test.expectErrorCode == "" {
				mockPipelineTemplatesService.On("CreatePipelineTemplate", mock.Anything, &pipelinetemplate.CreatePipelineTemplateInput{
					ProjectID: projectID,
				}).Return(samplePipelineTemplate, nil)

				mockPipelineTemplatesService.On("UploadPipelineTemplate", mock.Anything, &pipelinetemplate.UploadPipelineTemplateInput{
					Reader: bytes.NewReader(pipelineTemplate.Bytes()),
					ID:     samplePipelineTemplate.Metadata.ID,
				}).Return(nil)

				createPipelineInputMatcher := mock.MatchedBy(func(input *pipeline.CreatePipelineInput) bool {
					for _, e := range test.lifecycle.EnvironmentNames {
						// Verify system variables are set for each environment
						if _, ok := input.SystemVariables["deployment_"+e+"_template_id"]; !ok {
							return false
						}
						if _, ok := input.SystemVariables["deployment_"+e+"_vars"]; !ok {
							return false
						}
					}

					return input.ReleaseID != nil &&
						*input.ReleaseID == releaseID &&
						input.PipelineTemplateID == samplePipelineTemplate.Metadata.ID &&
						input.Type == models.ReleaseLifecyclePipelineType
				})
				mockPipelinesService.On("CreatePipeline", mock.Anything, createPipelineInputMatcher).Return(&models.Pipeline{}, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &projectID,
						Action:     models.ActionCreate,
						TargetType: models.TargetRelease,
						TargetID:   &releaseID,
					},
				).Return(&models.ActivityEvent{}, nil)

				if test.input != nil {
					mockReleaseDataStore.On("UploadReleaseNotes", mock.Anything, releaseID, test.input.Notes).Return(nil)
				}

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			dbClient := &db.Client{
				Projects:          mockProjects,
				Releases:          mockReleases,
				Transactions:      mockTransactions,
				ReleaseLifecycles: mockLifecycles,
				PipelineTemplates: mockPipelineTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:                   dbClient,
				logger:                     logger,
				lifecycleTemplateDataStore: mockLifecycleDataStore,
				pipelineTemplateDataStore:  mockPipelineTemplateDataStore,
				pipelineTemplateService:    mockPipelineTemplatesService,
				pipelineService:            mockPipelinesService,
				limitChecker:               mockLimits,
				templateBuilder:            NewTemplateBuilder(),
				activityService:            mockActivityEvents,
				releaseDataStore:           mockReleaseDataStore,
			}

			release, err := service.CreateRelease(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.NotNil(t, release)
			assert.Equal(t, releaseID, release.Metadata.ID)
		})
	}
}

func TestUpdateRelease(t *testing.T) {
	sampleRelease := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		SemanticVersion:    "1.0.0",
		ProjectID:          "project-1",
		UserParticipantIDs: []string{"user-1"},
	}

	type testCase struct {
		authError       error
		release         *models.Release
		input           *UpdateReleaseInput
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully update a release",
			release: sampleRelease,
			input: &UpdateReleaseInput{
				ReleaseID: sampleRelease.Metadata.ID,
				Notes:     ptr.String("release 1.0.0"),
				DueDate:   ptr.Time(time.Now().UTC().Add(time.Hour)),
			},
		},
		{
			name:            "release not found",
			expectErrorCode: errors.ENotFound,
			input: &UpdateReleaseInput{
				ReleaseID: sampleRelease.Metadata.ID,
			},
		},
		{
			name:    "subject is not authorized to update release",
			release: sampleRelease,
			input: &UpdateReleaseInput{
				ReleaseID: sampleRelease.Metadata.ID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockReleases.On("GetReleaseByID", mock.Anything, sampleRelease.Metadata.ID).Return(test.release, nil)

			if test.release != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateRelease, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockCaller.On("GetSubject").Return("testSubject")

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockReleases.On("UpdateRelease", mock.Anything, test.release).Return(
					func(_ context.Context, release *models.Release) (*models.Release, error) {
						require.Equal(t, test.input.DueDate, release.DueDate)
						return release, nil
					},
				)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &sampleRelease.ProjectID,
						Action:     models.ActionUpdate,
						TargetType: models.TargetRelease,
						TargetID:   &sampleRelease.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Releases:     mockReleases,
				Transactions: mockTransactions,
			}

			mockReleaseDataStore := NewMockDataStore(t)

			if test.input.Notes != nil {
				mockReleaseDataStore.On("UploadReleaseNotes", mock.Anything, test.input.ReleaseID, *test.input.Notes).Return(nil)
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:         dbClient,
				logger:           logger,
				activityService:  mockActivityEvents,
				releaseDataStore: mockReleaseDataStore,
			}

			updateRelease, err := service.UpdateRelease(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.release.DueDate, updateRelease.DueDate)
		})
	}
}

func TestDeleteRelease(t *testing.T) {
	releaseID := "release-1"

	type testCase struct {
		authError              error
		release                *models.Release
		name                   string
		expectNewLatestRelease *models.Release
		existingReleases       []*models.Release
		expectErrorCode        errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully delete release",
			release: &models.Release{
				Metadata: models.ResourceMetadata{
					ID: releaseID,
				},
				SemanticVersion: "1.0.0",
				ProjectID:       "project-1",
			},
		},
		{
			name: "successfully delete latest release and set existing release as latest",
			release: &models.Release{
				Metadata: models.ResourceMetadata{
					ID: releaseID,
				},
				SemanticVersion: "1.0.0",
				ProjectID:       "project-1",
				Latest:          true,
			},
			existingReleases: []*models.Release{
				{Metadata: models.ResourceMetadata{ID: releaseID}, SemanticVersion: "1.0.0"},
				{Metadata: models.ResourceMetadata{ID: "release-2"}, SemanticVersion: "0.1.0"},
				{Metadata: models.ResourceMetadata{ID: "release-3"}, SemanticVersion: "0.2.0-beta"},
			},
			expectNewLatestRelease: &models.Release{Metadata: models.ResourceMetadata{ID: "release-2"}, SemanticVersion: "0.1.0"},
		},
		{
			name:            "release not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to delete release",
			release: &models.Release{
				Metadata: models.ResourceMetadata{
					ID: releaseID,
				},
				SemanticVersion: "1.0.0",
				ProjectID:       "project-1",
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		test := test

		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockReleases.On("GetReleaseByID", mock.Anything, releaseID).Return(test.release, nil)

			if test.release != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.DeleteRelease, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockCaller.On("GetSubject").Return("testSubject")

				if test.release != nil && test.release.Latest {
					// If the release being deleted is latest, we need to mock out the call
					// to get the next latest release.
					sort := db.ReleaseSortableFieldCreatedAtDesc
					mockReleases.On("GetReleases", mock.Anything, &db.GetReleasesInput{
						Sort: &sort,
						Filter: &db.ReleaseFilter{
							ProjectID: &test.release.ProjectID,
						},
					}).Return(func(_ context.Context, _ *db.GetReleasesInput) *db.ReleasesResult {
						if len(test.existingReleases) == 0 {
							return &db.ReleasesResult{}
						}

						return &db.ReleasesResult{
							Releases: test.existingReleases,
						}
					}, nil)

					if test.expectNewLatestRelease != nil {
						mockReleases.On("UpdateRelease", mock.Anything, &models.Release{
							Metadata: models.ResourceMetadata{
								ID: test.expectNewLatestRelease.Metadata.ID,
							},
							SemanticVersion: test.expectNewLatestRelease.SemanticVersion,
							Latest:          true,
						}).Return(nil, nil).Once()
					}
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockReleases.On("DeleteRelease", mock.Anything, test.release).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &test.release.ProjectID,
						Action:     models.ActionDelete,
						TargetType: models.TargetProject,
						TargetID:   &test.release.ProjectID,
						Payload: &models.ActivityEventDeleteResourcePayload{
							Name: &test.release.SemanticVersion, // Semantic versions are enough to identify a release in a project.
							ID:   test.release.Metadata.ID,
							Type: models.TargetRelease.String(),
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Releases:     mockReleases,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			err := service.DeleteRelease(auth.WithCaller(ctx, mockCaller), &DeleteReleaseInput{
				ID: releaseID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestUpdateDeploymentPipeline(t *testing.T) {
	releaseID := "release-1"

	type testCase struct {
		authError       error
		input           *UpdateDeploymentPipelineInput
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully update a deployment pipeline",
			input: &UpdateDeploymentPipelineInput{
				ReleaseID:       releaseID,
				EnvironmentName: "account1",
				Variables: []pipeline.Variable{
					{
						Key:      "some_string",
						Value:    "some new value",
						Category: models.HCLVariable,
					},
				},
				PipelineTemplateID: ptr.String("pipeline-template-1"),
			},
		},
		{
			name: "deployment pipeline not found",
			input: &UpdateDeploymentPipelineInput{
				ReleaseID:       releaseID,
				EnvironmentName: "non-existent",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "subject is not authorized to update a deployment pipeline",
			input: &UpdateDeploymentPipelineInput{
				ReleaseID: releaseID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)
			mockPipelineService := pipeline.NewMockService(t)

			release := &models.Release{
				Metadata: models.ResourceMetadata{
					ID: "release-1",
				},
				SemanticVersion: "1.0.0",
				ProjectID:       "project-1",
			}

			releasePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-1",
				},
				Stages: []*models.PipelineStage{
					{
						Name: "dev",
						NestedPipelines: []*models.NestedPipeline{
							{
								Path:            "pipeline.stage.dev.pipeline.account1",
								EnvironmentName: ptr.String("account1"),
							},
						},
					},
				},
			}

			mockCaller.On("GetSubject").Return("testSubject").Maybe()
			mockCaller.On("Authorized").Maybe()

			mockReleases.On("GetReleaseByID", mock.Anything, release.Metadata.ID).Return(release, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateRelease, mock.Anything).Return(test.authError)

			mockPipelines.On("GetPipelineByReleaseID", mock.Anything, release.Metadata.ID).Return(releasePipeline, nil).Maybe()

			if test.expectErrorCode == "" {
				mockPipelineService.On("UpdateNestedPipeline", mock.Anything, &pipeline.UpdateNestedPipelineInput{
					ParentPipelineID:       releasePipeline.Metadata.ID,
					ParentPipelineNodePath: "pipeline.stage.dev.pipeline.account1",
					Variables:              test.input.Variables,
					PipelineTemplateID:     test.input.PipelineTemplateID,
				}).Return(releasePipeline, nil)
			}

			dbClient := &db.Client{
				Releases:  mockReleases,
				Pipelines: mockPipelines,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				pipelineService: mockPipelineService,
			}

			updatedPipeline, err := service.UpdateDeploymentPipeline(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, updatedPipeline)
		})
	}
}

func TestAddParticipantToRelease(t *testing.T) {
	releaseID := "release-1"
	projectID := "project-1"
	subject := "testSubject"
	userID := "user-1"
	teamID := "team-1"

	type testCase struct {
		authError       error
		input           *ParticipantInput
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully add a user to a release",
			input: &ParticipantInput{
				ReleaseID: releaseID,
				UserID:    &userID,
			},
		},
		{
			name: "successfully add a team to a release",
			input: &ParticipantInput{
				ReleaseID: releaseID,
				TeamID:    &teamID,
			},
		},
		{
			name:            "subject is not authorized to add a participant to a release",
			input:           &ParticipantInput{ReleaseID: releaseID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "release not found",
			input:           &ParticipantInput{ReleaseID: "non-existent"},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "cannot add both a user and a team to a release at once",
			input: &ParticipantInput{
				ReleaseID: releaseID,
				UserID:    &userID,
				TeamID:    &teamID,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockReleases.On("GetReleaseByID", mock.Anything, test.input.ReleaseID).Return(
				func(_ context.Context, id string) (*models.Release, error) {
					if id != releaseID {
						return nil, nil
					}

					return &models.Release{
						Metadata: models.ResourceMetadata{
							ID: releaseID,
						},
						ProjectID:          projectID,
						SemanticVersion:    "1.0.0",
						CreatedBy:          subject,
						UserParticipantIDs: []string{"user-2"},
					}, nil
				},
			)

			if test.input.ReleaseID == releaseID {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateRelease, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockCaller.On("GetSubject").Return(subject)

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockReleases.On("UpdateRelease", mock.Anything, mock.Anything).Return(
					func(_ context.Context, release *models.Release) (*models.Release, error) {
						if test.input.UserID != nil {
							require.Contains(t, release.UserParticipantIDs, *test.input.UserID)
						}
						if test.input.TeamID != nil {
							require.Contains(t, release.TeamParticipantIDs, *test.input.TeamID)
						}

						return release, nil
					},
				)

				participantID := userID
				participantType := models.TargetUser
				if test.input.TeamID != nil {
					participantID = teamID
					participantType = models.TargetTeam
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &projectID,
						Action:     models.ActionUpdate,
						TargetType: models.TargetRelease,
						TargetID:   &releaseID,
						Payload: &models.ActivityEventUpdateReleasePayload{
							ID:         participantID,
							Type:       participantType.String(),
							ChangeType: models.AddParticipantReleaseChangeType,
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Releases:     mockReleases,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			updatedRelease, err := service.AddParticipantToRelease(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedRelease)

			if test.input.UserID != nil {
				assert.Contains(t, updatedRelease.UserParticipantIDs, *test.input.UserID)
			}

			if test.input.TeamID != nil {
				assert.Contains(t, updatedRelease.TeamParticipantIDs, *test.input.TeamID)
			}
		})
	}
}

func TestRemoveParticipantFromRelease(t *testing.T) {
	releaseID := "release-1"
	projectID := "project-1"
	subject := "testSubject"
	userID := "user-1"
	teamID := "team-1"

	type testCase struct {
		authError       error
		input           *ParticipantInput
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully remove a user from a release",
			input: &ParticipantInput{
				ReleaseID: releaseID,
				UserID:    &userID,
			},
		},
		{
			name: "successfully remove a team from a release",
			input: &ParticipantInput{
				ReleaseID: releaseID,
				TeamID:    &teamID,
			},
		},
		{
			name:            "subject is not authorized to remove a participant from a release",
			input:           &ParticipantInput{ReleaseID: releaseID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "release not found",
			input:           &ParticipantInput{ReleaseID: "non-existent"},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "cannot remove both a user and a team from a release at once",
			input: &ParticipantInput{
				ReleaseID: releaseID,
				UserID:    &userID,
				TeamID:    &teamID,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleases := db.NewMockReleases(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockReleases.On("GetReleaseByID", mock.Anything, test.input.ReleaseID).Return(
				func(_ context.Context, id string) (*models.Release, error) {
					if id != releaseID {
						return nil, nil
					}

					return &models.Release{
						Metadata: models.ResourceMetadata{
							ID: releaseID,
						},
						ProjectID:          projectID,
						SemanticVersion:    "1.0.0",
						CreatedBy:          subject,
						UserParticipantIDs: []string{"user-1"},
						TeamParticipantIDs: []string{"team-1"},
					}, nil
				},
			)

			if test.input.ReleaseID == releaseID {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateRelease, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockCaller.On("GetSubject").Return(subject)

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockReleases.On("UpdateRelease", mock.Anything, mock.Anything).Return(
					func(_ context.Context, release *models.Release) (*models.Release, error) {
						if test.input.UserID != nil {
							require.NotContains(t, release.UserParticipantIDs, *test.input.UserID)
						}
						if test.input.TeamID != nil {
							require.NotContains(t, release.TeamParticipantIDs, *test.input.TeamID)
						}

						return release, nil
					},
				)

				participantID := userID
				participantType := models.TargetUser
				if test.input.TeamID != nil {
					participantID = teamID
					participantType = models.TargetTeam
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &projectID,
						Action:     models.ActionUpdate,
						TargetType: models.TargetRelease,
						TargetID:   &releaseID,
						Payload: &models.ActivityEventUpdateReleasePayload{
							ID:         participantID,
							Type:       participantType.String(),
							ChangeType: models.RemoveParticipantReleaseChangeType,
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Releases:     mockReleases,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			updatedRelease, err := service.RemoveParticipantFromRelease(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.input.UserID != nil {
				assert.NotContains(t, updatedRelease.UserParticipantIDs, *test.input.UserID)
			}

			if test.input.TeamID != nil {
				assert.NotContains(t, updatedRelease.TeamParticipantIDs, *test.input.TeamID)
			}
		})
	}
}
