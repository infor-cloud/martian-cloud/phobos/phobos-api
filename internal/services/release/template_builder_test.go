package release

import (
	"testing"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTemplateBuilder_Build(t *testing.T) {
	// sampleValidLifecycleTemplate is the sample lifecycle template data
	validLifecycleTemplate := `
	plugin exec {}

	stages = ["dev"]

	stage dev {
	  deployment {
		environment = "account1"
		when        = "auto"
	  }
	  deployment {
		environment = "account2"
	  }
	}`

	convertedPipelineTemplate := `
	plugin exec {}

	stages = ["dev"]

	stage dev {
	  pipeline "deployment_account1" {
		environment   = "account1"
		when          = "auto"
		pipeline_type = "deployment"
		template_id   = system.deployment_account1_template_id
		variables     = system.deployment_account1_vars
	  }
	  pipeline "deployment_account2" {
		environment   = "account2"
		pipeline_type = "deployment"
		template_id   = system.deployment_account2_template_id
		when          = "manual"
		variables     = system.deployment_account2_vars
	  }
	}`

	// Must parse the converted template since the bytes will be different
	// if casted directly.
	pipelineTemplate, diags := hclwrite.ParseConfig([]byte(convertedPipelineTemplate), "pipeline.hcl", hcl.InitialPos)
	require.Nil(t, diags)

	actualTemplate, err := NewTemplateBuilder().Build([]byte(validLifecycleTemplate))
	require.NoError(t, err)

	assert.Equal(t, string(pipelineTemplate.Bytes()), string(actualTemplate))
}
