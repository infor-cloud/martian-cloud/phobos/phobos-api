package release

//go:generate go tool mockery --name DataStore --inpackage --case underscore

import (
	"context"
	"io"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/objectstore"
)

const (
	objectStoreKeyPrefix = "releases/"
)

// DataStore interface encapsulates the logic for saving and retrieving pipeline template data
type DataStore interface {
	UploadReleaseNotes(ctx context.Context, releaseID string, data string) error
	GetReleaseNotes(ctx context.Context, releaseID string) (io.ReadCloser, error)
}

type dataStore struct {
	objectStore objectstore.ObjectStore
}

// NewDataStore creates an instance of the DataStore interface
func NewDataStore(objectStore objectstore.ObjectStore) DataStore {
	return &dataStore{objectStore: objectStore}
}

func (ds *dataStore) UploadReleaseNotes(ctx context.Context, releaseID string, data string) error {
	return ds.objectStore.UploadObject(ctx, ds.objectStoreKey(releaseID), strings.NewReader(data))
}

func (ds *dataStore) GetReleaseNotes(ctx context.Context, releaseID string) (io.ReadCloser, error) {
	return ds.objectStore.GetObjectStream(ctx, ds.objectStoreKey(releaseID), nil)
}

func (ds *dataStore) objectStoreKey(id string) string {
	return objectStoreKeyPrefix + id
}
