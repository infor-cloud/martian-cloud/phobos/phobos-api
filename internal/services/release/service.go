// Package release provides the release service.
// A release is composed of a set of assets, a
// snapshot of the automation used to deploy
// the assets, and the state of the automation
package release

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"slices"
	"time"

	msemver "github.com/Masterminds/semver/v3"
	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-version"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/semver"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const maxReleaseNotesLength = 1024 * 128

// GetReleasesInput is the input for querying a list of releases.
type GetReleasesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ReleaseSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// ProjectID filters the releases by the specified project
	ProjectID *string
	// UserParticipantID filters the releases by the specified user participant
	UserParticipantID *string
	// Latest filters the releases to only the latest release
	Latest *bool
}

// VariableInput represents a release variable.
type VariableInput struct {
	EnvironmentName *string
	Key             string
	Value           string
	Category        models.VariableCategory
}

// validate validates the variable.
func (v *VariableInput) validate() error {
	if v.EnvironmentName != nil && v.Category != models.HCLVariable {
		return errors.New("only HCL variables can be scoped to a deployment environment", errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}

// DeploymentTemplateInput is the input for supplying a deployment template.
type DeploymentTemplateInput struct {
	EnvironmentName    string
	PipelineTemplateID string
}

// CreateReleaseInput is the input for creating a release.
type CreateReleaseInput struct {
	DueDate             *time.Time
	VariableSetRevision *string
	ProjectID           string
	LifecycleID         string
	SemanticVersion     string
	Notes               string
	DeploymentTemplates []DeploymentTemplateInput
	Variables           []VariableInput
	UserParticipantIDs  []string
	TeamParticipantIDs  []string
}

// UpdateReleaseInput is the input for updating a release.
type UpdateReleaseInput struct {
	Notes     *string
	DueDate   *time.Time
	ReleaseID string
}

// UpdateDeploymentPipelineInput is the input for updating a release deployment.
type UpdateDeploymentPipelineInput struct {
	ReleaseID          string
	EnvironmentName    string
	PipelineTemplateID *string
	Variables          []pipeline.Variable
}

// DeleteReleaseInput is the input for deleting a release.
type DeleteReleaseInput struct {
	MetadataVersion *int
	ID              string
}

// ParticipantInput is the input for adding a participant to a release.
type ParticipantInput struct {
	UserID    *string
	TeamID    *string
	ReleaseID string
}

// Service is the interface that provides release methods.
type Service interface {
	GetReleaseByID(ctx context.Context, id string) (*models.Release, error)
	GetReleaseByPRN(ctx context.Context, prn string) (*models.Release, error)
	GetReleasesByIDs(ctx context.Context, idList []string) ([]*models.Release, error)
	GetReleaseNotes(ctx context.Context, releaseID string) (string, error)
	GetReleases(ctx context.Context, input *GetReleasesInput) (*db.ReleasesResult, error)
	CreateRelease(ctx context.Context, input *CreateReleaseInput) (*models.Release, error)
	UpdateRelease(ctx context.Context, input *UpdateReleaseInput) (*models.Release, error)
	DeleteRelease(ctx context.Context, input *DeleteReleaseInput) error
	UpdateDeploymentPipeline(ctx context.Context, input *UpdateDeploymentPipelineInput) (*models.Pipeline, error)
	AddParticipantToRelease(ctx context.Context, input *ParticipantInput) (*models.Release, error)
	RemoveParticipantFromRelease(ctx context.Context, input *ParticipantInput) (*models.Release, error)
}

type service struct {
	logger                     logger.Logger
	dbClient                   *db.Client
	lifecycleTemplateDataStore lifecycletemplate.DataStore
	pipelineTemplateDataStore  pipelinetemplate.DataStore
	pipelineTemplateService    pipelinetemplate.Service
	pipelineService            pipeline.Service
	limitChecker               limits.LimitChecker
	templateBuilder            *TemplateBuilder
	activityService            activityevent.Service
	releaseDataStore           DataStore
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	lifecycleTemplateDataStore lifecycletemplate.DataStore,
	pipelineTemplateDataStore pipelinetemplate.DataStore,
	pipelineTemplateService pipelinetemplate.Service,
	pipelineService pipeline.Service,
	limitChecker limits.LimitChecker,
	activityService activityevent.Service,
	releaseDataStore DataStore,
) Service {
	return &service{
		logger:                     logger,
		dbClient:                   dbClient,
		lifecycleTemplateDataStore: lifecycleTemplateDataStore,
		pipelineTemplateDataStore:  pipelineTemplateDataStore,
		pipelineTemplateService:    pipelineTemplateService,
		pipelineService:            pipelineService,
		limitChecker:               limitChecker,
		templateBuilder:            NewTemplateBuilder(),
		activityService:            activityService,
		releaseDataStore:           releaseDataStore,
	}
}

func (s *service) GetReleaseByID(ctx context.Context, id string) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleaseByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	release, err := s.getReleaseByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return nil, err
	}

	return release, nil
}

func (s *service) GetReleaseByPRN(ctx context.Context, prn string) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleaseByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	release, err := s.dbClient.Releases.GetReleaseByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release by PRN", errors.WithSpan(span))
	}

	if release == nil {
		return nil, errors.New("release not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	err = caller.RequirePermission(ctx, models.ViewRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return nil, err
	}

	return release, nil
}

func (s *service) GetReleasesByIDs(ctx context.Context, idList []string) ([]*models.Release, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleasesByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	resp, err := s.dbClient.Releases.GetReleases(ctx, &db.GetReleasesInput{
		Filter: &db.ReleaseFilter{
			ReleaseIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get releases", errors.WithSpan(span))
	}

	for _, r := range resp.Releases {
		err = caller.RequirePermission(ctx, models.ViewRelease, auth.WithProjectID(r.ProjectID))
		if err != nil {
			return nil, err
		}
	}

	return resp.Releases, nil
}

func (s *service) GetReleaseNotes(ctx context.Context, releaseID string) (string, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleaseNotes")
	span.SetAttributes(attribute.String("releaseId", releaseID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return "", err
	}

	release, err := s.getReleaseByID(ctx, span, releaseID)
	if err != nil {
		return "", err
	}

	err = caller.RequirePermission(ctx, models.ViewRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return "", err
	}

	rdr, err := s.releaseDataStore.GetReleaseNotes(ctx, releaseID)
	if err != nil {
		return "", errors.Wrap(err, "failed to download release notes from object store")
	}
	defer rdr.Close()

	notesBuf, err := io.ReadAll(rdr)
	if err != nil {
		return "", errors.Wrap(err, "failed to read release notes")
	}

	return string(notesBuf), nil
}

func (s *service) GetReleases(ctx context.Context, input *GetReleasesInput) (*db.ReleasesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleases")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	filter := &db.ReleaseFilter{
		ProjectID:         input.ProjectID,
		UserParticipantID: input.UserParticipantID,
		Latest:            input.Latest,
	}

	if input.ProjectID != nil {
		err = caller.RequirePermission(ctx, models.ViewRelease, auth.WithProjectID(*input.ProjectID))
		if err != nil {
			return nil, err
		}
	} else {
		// Only return releases for projects that the caller is a member of
		if err = auth.HandleCaller(
			ctx,
			func(_ context.Context, c *auth.UserCaller) error {
				if !c.User.Admin {
					filter.UserMemberID = &c.User.Metadata.ID
				}
				return nil
			},
			func(_ context.Context, c *auth.ServiceAccountCaller) error {
				filter.ServiceAccountMemberID = &c.ServiceAccountID
				return nil
			},
		); err != nil {
			return nil, errors.Wrap(err, "invalid caller type", errors.WithSpan(span))
		}
	}

	dbInput := &db.GetReleasesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter:            filter,
	}

	resp, err := s.dbClient.Releases.GetReleases(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get releases", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) UpdateDeploymentPipeline(ctx context.Context, input *UpdateDeploymentPipelineInput) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateDeploymentPipeline")
	span.SetAttributes(attribute.String("release", input.ReleaseID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	release, err := s.getReleaseByID(ctx, span, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.UpdateRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return nil, err
	}

	// Mark caller as authorized
	caller.Authorized()

	releasePipeline, err := s.dbClient.Pipelines.GetPipelineByReleaseID(ctx, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	if releasePipeline == nil {
		// Return an internal error here since this should never happen
		return nil, errors.New("pipeline not found for release %s", input.ReleaseID)
	}

	// Find the nested pipeline associated with the environment.
	var nestedPipeline *models.NestedPipeline
	for _, s := range releasePipeline.Stages {
		for _, p := range s.NestedPipelines {
			if *p.EnvironmentName == input.EnvironmentName {
				nestedPipeline = p
				break
			}
		}
		if nestedPipeline != nil {
			break
		}
	}

	if nestedPipeline == nil {
		return nil, errors.New("nested pipeline for environment %q not found", input.EnvironmentName, errors.WithErrorCode(errors.EInvalid))
	}

	// Use the pipeline service to update the nested pipeline
	pipeline, err := s.pipelineService.UpdateNestedPipeline(
		ctx,
		&pipeline.UpdateNestedPipelineInput{
			ParentPipelineID:       releasePipeline.Metadata.ID,
			ParentPipelineNodePath: nestedPipeline.Path,
			Variables:              input.Variables,
			PipelineTemplateID:     input.PipelineTemplateID,
		},
	)
	if err != nil {
		return nil, err
	}

	return pipeline, nil
}

func (s *service) UpdateRelease(ctx context.Context, input *UpdateReleaseInput) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateRelease")
	span.SetAttributes(attribute.String("release", input.ReleaseID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	release, err := s.getReleaseByID(ctx, span, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.UpdateRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return nil, err
	}

	// Allow nulling DueDate.
	release.DueDate = input.DueDate

	if err = release.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateRelease: %v", txErr)
		}
	}()

	if _, err = s.dbClient.Releases.UpdateRelease(txContext, release); err != nil {
		return nil, errors.Wrap(err, "failed to update release", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &release.ProjectID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetRelease,
			TargetID:   &release.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if input.Notes != nil {
		// Upload Release Notes
		if err = s.releaseDataStore.UploadReleaseNotes(ctx, input.ReleaseID, *input.Notes); err != nil {
			return nil, err
		}
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a release.",
		"caller", caller.GetSubject(),
		"releaseID", input.ReleaseID,
		"projectID", release.ProjectID,
	)

	return release, nil
}

func (s *service) CreateRelease(ctx context.Context, input *CreateReleaseInput) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateRelease")
	span.SetAttributes(attribute.String("project", input.ProjectID))
	span.SetAttributes(attribute.String("lifecycle", input.LifecycleID))
	span.SetAttributes(attribute.String("version", input.SemanticVersion))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CreateRelease, auth.WithProjectID(input.ProjectID))
	if err != nil {
		return nil, err
	}

	// Mark caller as authorized
	caller.Authorized()

	releaseLifecycle, err := s.dbClient.ReleaseLifecycles.GetReleaseLifecycleByID(ctx, input.LifecycleID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release lifecycle", errors.WithSpan(span))
	}

	if releaseLifecycle == nil {
		return nil, errors.New("release lifecycle not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	project, err := s.dbClient.Projects.GetProjectByID(ctx, input.ProjectID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project", errors.WithSpan(span))
	}

	if project == nil {
		return nil, errors.New("project not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	switch releaseLifecycle.Scope {
	case models.OrganizationScope:
		if releaseLifecycle.OrganizationID != project.OrgID {
			return nil, errors.New("release lifecycle does not belong to the project's organization",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	case models.ProjectScope:
		if *releaseLifecycle.ProjectID != project.Metadata.ID {
			return nil, errors.New("release lifecycle does not belong to the project",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	if len(input.Notes) > maxReleaseNotesLength {
		return nil, errors.New("invalid notes, cannot be greater than %d characters", maxReleaseNotesLength, errors.WithErrorCode(errors.EInvalid))
	}

	// This will attempt to parse the semantic version and convert it to a semver version if possible
	parsedVersion, err := msemver.NewVersion(input.SemanticVersion)
	if err != nil {
		return nil, errors.Wrap(err, "invalid semantic version", errors.WithErrorCode(errors.EInvalid))
	}

	releaseToCreate := &models.Release{
		ProjectID:          input.ProjectID,
		SemanticVersion:    parsedVersion.String(),
		CreatedBy:          caller.GetSubject(),
		UserParticipantIDs: input.UserParticipantIDs,
		TeamParticipantIDs: input.TeamParticipantIDs,
		DueDate:            input.DueDate,
		LifecyclePRN:       releaseLifecycle.Metadata.PRN,
	}

	if releaseToCreate.DueDate != nil && time.Now().UTC().After(*releaseToCreate.DueDate) {
		return nil, errors.New("due date cannot be in the past", errors.WithErrorCode(errors.EInvalid))
	}

	if err = releaseToCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	semanticVersion, sErr := version.NewSemver(releaseToCreate.SemanticVersion)
	if sErr != nil {
		return nil, errors.Wrap(sErr, "failed to create release semantic version", errors.WithSpan(span))
	}
	// Check if this is a pre-release
	releaseToCreate.PreRelease = semanticVersion.Prerelease() != ""

	rdr, err := s.lifecycleTemplateDataStore.GetData(ctx, releaseLifecycle.LifecycleTemplateID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle template data", errors.WithSpan(span))
	}
	defer rdr.Close()

	// Parse the lifecycle template to ensure it is valid.
	lifecycleConfig, rawLifecycleHCL, err := config.NewLifecycleTemplate(rdr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse lifecycle template", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	deploymentTemplateInputMap := map[string]DeploymentTemplateInput{}
	for _, deploymentTemplate := range input.DeploymentTemplates {
		deploymentTemplateInputMap[deploymentTemplate.EnvironmentName] = deploymentTemplate
	}

	// Verify that a deployment template is provided for each environment in the lifecycle
	for _, stage := range lifecycleConfig.Stages {
		for _, deployment := range stage.Deployments {
			if _, ok := deploymentTemplateInputMap[deployment.EnvironmentName]; !ok {
				return nil, errors.New(
					"deployment environment %s is missing a deployment template",
					deployment.EnvironmentName,
					errors.WithErrorCode(errors.EInvalid),
					errors.WithSpan(span),
				)
			}
		}
	}

	// Validate templates
	seenTemplates := map[string]struct{}{}
	for _, t := range input.DeploymentTemplates {
		if _, ok := seenTemplates[t.PipelineTemplateID]; !ok {
			template, tErr := s.dbClient.PipelineTemplates.GetPipelineTemplateByID(ctx, t.PipelineTemplateID)
			if tErr != nil {
				return nil, errors.Wrap(tErr, "deployment environment %s has an invalid template %s", t.EnvironmentName, t.PipelineTemplateID, errors.WithErrorCode(errors.EInvalid))
			}

			if template == nil || template.ProjectID != input.ProjectID {
				return nil, errors.New("deployment environment %s is referencing an invalid template %s", t.EnvironmentName, t.PipelineTemplateID, errors.WithErrorCode(errors.EInvalid))
			}

			if !template.Versioned {
				return nil, errors.New("releases can only use versioned pipeline templates", errors.WithErrorCode(errors.EInvalid))
			}

			if template.Status != models.PipelineTemplateUploaded {
				return nil, errors.New("deployment template %s is not uploaded", t.PipelineTemplateID, errors.WithErrorCode(errors.EInvalid))
			}
		}
		seenTemplates[t.PipelineTemplateID] = struct{}{}
	}

	// Create the deployment variables for this release.
	deploymentVariables, environmentVariables, err := s.createDeploymentVariables(input.Variables, lifecycleConfig.Stages)
	if err != nil {
		return nil, errors.Wrap(err, "failed to process release variables", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Load the system variables for the release pipeline.
	systemVariables, err := s.loadSystemVariablesForReleasePipeline(ctx, input.DeploymentTemplates, deploymentVariables)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load variables", errors.WithSpan(span))
	}

	// Build the pipeline template from the lifecycle template.
	pipelineHCL, err := s.templateBuilder.Build(rawLifecycleHCL)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build pipeline template", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Check if this version is greater than the current latest
	latestReleaseResp, err := s.dbClient.Releases.GetReleases(ctx, &db.GetReleasesInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
		Filter: &db.ReleaseFilter{
			ProjectID: &input.ProjectID,
			Latest:    ptr.Bool(true),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get previous latest release", errors.WithSpan(span))
	}

	var prevLatest *models.Release
	if len(latestReleaseResp.Releases) == 0 {
		// There are no releases in the project, so this is the latest.
		releaseToCreate.Latest = true
	} else {
		prevLatest = latestReleaseResp.Releases[0]
		prevSemanticVersion, sErr := version.NewSemver(prevLatest.SemanticVersion)
		if sErr != nil {
			return nil, errors.Wrap(sErr, "semantic version validation failed", errors.WithSpan(span))
		}
		if semver.IsSemverGreaterThan(semanticVersion, prevSemanticVersion) {
			// New release will be set to latest
			releaseToCreate.Latest = true
			// Prev latest will be set to false
			prevLatest.Latest = false
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateRelease: %v", txErr)
		}
	}()

	// If prev latest release exists and latest has been set to false then we need to update it
	if prevLatest != nil && !prevLatest.Latest {
		if _, err = s.dbClient.Releases.UpdateRelease(txContext, prevLatest); err != nil {
			return nil, err
		}
	}

	// Create the release since we'll need the ID to create the pipeline.
	createdRelease, err := s.dbClient.Releases.CreateRelease(txContext, releaseToCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create release", errors.WithSpan(span))
	}

	// Get the number of releases in this project to check whether we just violated the limit.
	releasesResult, err := s.dbClient.Releases.GetReleases(txContext, &db.GetReleasesInput{
		Filter: &db.ReleaseFilter{
			TimeRangeStart: ptr.Time(createdRelease.Metadata.CreationTimestamp.Add(-limits.ResourceLimitTimePeriod)),
			ProjectID:      &input.ProjectID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get releases", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(
		txContext,
		limits.ResourceLimitReleasesPerProjectPerTimePeriod,
		releasesResult.PageInfo.TotalCount,
	); err != nil {
		return nil, errors.Wrap(err, "limit check failed", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Create the pipeline template for this release.
	createdPipelineTemplate, err := s.pipelineTemplateService.CreatePipelineTemplate(txContext, &pipelinetemplate.CreatePipelineTemplateInput{
		ProjectID: input.ProjectID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create pipeline template", errors.WithSpan(span))
	}

	// Template must be uploaded before pipeline can be created.
	if err = s.pipelineTemplateService.UploadPipelineTemplate(
		txContext,
		&pipelinetemplate.UploadPipelineTemplateInput{
			Reader: bytes.NewReader(pipelineHCL), // Use the "converted" template.
			ID:     createdPipelineTemplate.Metadata.ID,
		}); err != nil {
		return nil, errors.Wrap(err, "failed to upload pipeline template", errors.WithSpan(span))
	}

	// Create the pipeline for this release.
	createdPipeline, err := s.pipelineService.CreatePipeline(
		txContext,
		&pipeline.CreatePipelineInput{
			PipelineTemplateID:  createdPipelineTemplate.Metadata.ID,
			ReleaseID:           &createdRelease.Metadata.ID,
			Type:                models.ReleaseLifecyclePipelineType,
			Variables:           environmentVariables,
			SystemVariables:     systemVariables,
			VariableSetRevision: input.VariableSetRevision,
		},
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create pipeline", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &createdRelease.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetRelease,
			TargetID:   &createdRelease.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	// Upload Release Notes
	if err = s.releaseDataStore.UploadReleaseNotes(ctx, createdRelease.Metadata.ID, input.Notes); err != nil {
		return nil, err
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a release.",
		"caller", caller.GetSubject(),
		"releaseID", createdRelease.Metadata.ID,
		"projectID", createdRelease.ProjectID,
		"pipelineID", createdPipeline.Metadata.ID,
	)

	return createdRelease, nil
}

func (s *service) DeleteRelease(ctx context.Context, input *DeleteReleaseInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteRelease")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	release, err := s.getReleaseByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	err = caller.RequirePermission(ctx, models.DeleteRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return err
	}

	if input.MetadataVersion != nil {
		release.Metadata.Version = *input.MetadataVersion
	}

	// Reset latest flag if we're deleting the latest version
	var newLatestVersion *models.Release
	if release.Latest {
		// Sort by descending creation time to speed up the search for the new latest version since typically
		// the latest version is the most recent one.
		sort := db.ReleaseSortableFieldCreatedAtDesc
		releasesResp, rErr := s.dbClient.Releases.GetReleases(ctx, &db.GetReleasesInput{
			Sort: &sort,
			Filter: &db.ReleaseFilter{
				ProjectID: &release.ProjectID,
			},
		})
		if rErr != nil {
			return errors.Wrap(rErr, "failed to get releases", errors.WithSpan(span))
		}

		for _, r := range releasesResp.Releases {
			rCopy := r

			// Skip if this is the release we're deleting
			if r.Metadata.ID == release.Metadata.ID {
				continue
			}

			if newLatestVersion == nil {
				newLatestVersion = rCopy
				continue
			}

			latestSemanticVersion, lsErr := version.NewSemver(newLatestVersion.SemanticVersion)
			if lsErr != nil {
				return errors.Wrap(lsErr, "failed to validate semantic version", errors.WithSpan(span))
			}

			currentSemanticVersion, csErr := version.NewSemver(rCopy.SemanticVersion)
			if csErr != nil {
				return errors.Wrap(csErr, "failed to validate semantic version", errors.WithSpan(span))
			}

			if semver.IsSemverGreaterThan(currentSemanticVersion, latestSemanticVersion) {
				newLatestVersion = rCopy
			}
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteRelease: %v", txErr)
		}
	}()

	if err = s.dbClient.Releases.DeleteRelease(txContext, release); err != nil {
		return err
	}

	if newLatestVersion != nil {
		s.logger.Infof(
			"Deleted latest release %s, latest flag is being set to latest version %s",
			release.Metadata.PRN,
			newLatestVersion.SemanticVersion,
		)
		newLatestVersion.Latest = true
		if _, err = s.dbClient.Releases.UpdateRelease(txContext, newLatestVersion); err != nil {
			return err
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &release.ProjectID,
			Action:     models.ActionDelete,
			TargetType: models.TargetProject,
			TargetID:   &release.ProjectID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &release.SemanticVersion, // Semantic versions are enough to identify a release in a project.
				ID:   release.Metadata.ID,
				Type: models.TargetRelease.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a release.",
		"caller", caller.GetSubject(),
		"releaseID", input.ID,
		"projectID", release.ProjectID,
	)

	return nil
}

func (s *service) AddParticipantToRelease(ctx context.Context, input *ParticipantInput) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "svc.AddParticipantToRelease")
	span.SetAttributes(attribute.String("release", input.ReleaseID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	release, err := s.getReleaseByID(ctx, span, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.UpdateRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return nil, err
	}

	var (
		count           int
		participantType string
		participantID   string
	)

	if input.UserID != nil {
		count++
		participantType = models.TargetUser.String()
		participantID = *input.UserID
		release.UserParticipantIDs = append(release.UserParticipantIDs, *input.UserID)
	}

	if input.TeamID != nil {
		count++
		participantType = models.TargetTeam.String()
		participantID = *input.TeamID
		release.TeamParticipantIDs = append(release.TeamParticipantIDs, *input.TeamID)
	}

	if count != 1 {
		return nil, errors.New("either user or team must be provided", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if err = release.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer AddParticipantToRelease: %v", txErr)
		}
	}()

	updatedRelease, err := s.dbClient.Releases.UpdateRelease(txContext, release)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update release", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &release.ProjectID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetRelease,
			TargetID:   &updatedRelease.Metadata.ID,
			Payload: &models.ActivityEventUpdateReleasePayload{
				ID:         participantID,
				Type:       participantType,
				ChangeType: models.AddParticipantReleaseChangeType,
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Added a participant to a release.",
		"caller", caller.GetSubject(),
		"releaseID", input.ReleaseID,
		"participantType", participantType,
		"participantID", participantID,
	)

	return updatedRelease, nil
}

func (s *service) RemoveParticipantFromRelease(ctx context.Context, input *ParticipantInput) (*models.Release, error) {
	ctx, span := tracer.Start(ctx, "svc.RemoveParticipantFromRelease")
	span.SetAttributes(attribute.String("release", input.ReleaseID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	release, err := s.getReleaseByID(ctx, span, input.ReleaseID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.UpdateRelease, auth.WithProjectID(release.ProjectID))
	if err != nil {
		return nil, err
	}

	var (
		count           int
		participantID   string
		participantType string
	)
	if input.UserID != nil {
		index := slices.Index(release.UserParticipantIDs, *input.UserID)

		if index == -1 {
			return nil, errors.New("user with id %s is not a participant in the release", *input.UserID, errors.WithErrorCode(errors.EInvalid))
		}

		count++
		participantID = *input.UserID
		participantType = models.TargetUser.String()
		release.UserParticipantIDs = append(release.UserParticipantIDs[:index], release.UserParticipantIDs[index+1:]...)
	}

	if input.TeamID != nil {
		index := slices.Index(release.TeamParticipantIDs, *input.TeamID)

		if index == -1 {
			return nil, errors.New("team with id %s is not a participant in the release", *input.TeamID, errors.WithErrorCode(errors.EInvalid))
		}

		count++
		participantID = *input.TeamID
		participantType = models.TargetTeam.String()
		release.TeamParticipantIDs = append(release.TeamParticipantIDs[:index], release.TeamParticipantIDs[index+1:]...)
	}

	if count != 1 {
		return nil, errors.New("either user or team must be provided", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if err = release.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer RemoveParticipantFromRelease: %v", txErr)
		}
	}()

	updatedRelease, err := s.dbClient.Releases.UpdateRelease(txContext, release)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update release", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &release.ProjectID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetRelease,
			TargetID:   &updatedRelease.Metadata.ID,
			Payload: &models.ActivityEventUpdateReleasePayload{
				ID:         participantID,
				Type:       participantType,
				ChangeType: models.RemoveParticipantReleaseChangeType,
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Removed a participant from a release.",
		"caller", caller.GetSubject(),
		"releaseID", input.ReleaseID,
		"participantType", participantType,
		"participantID", participantID,
	)

	return updatedRelease, nil
}

// loadDeployments loads the deployment assets for a release, such as,
// the pipeline template, variables, and HCL config.
func (s *service) loadSystemVariablesForReleasePipeline(
	ctx context.Context,
	deploymentTemplates []DeploymentTemplateInput,
	variables map[string][]pipeline.Variable,
) (map[string]cty.Value, error) {
	// Track the templates we've already processed by ID.
	processedTemplates := map[string]map[string]hcl.Body{}

	for _, t := range deploymentTemplates {
		if _, ok := processedTemplates[t.PipelineTemplateID]; !ok {
			templateData, err := s.pipelineTemplateDataStore.GetData(ctx, t.PipelineTemplateID)
			if err != nil {
				return nil, errors.Wrap(err, "error downloading deployment template data")
			}
			defer templateData.Close()

			rawHCL, err := io.ReadAll(templateData)
			if err != nil {
				return nil, errors.Wrap(err, "failed to read pipeline template data")
			}

			// Parse config
			file, diags := hclsyntax.ParseConfig(rawHCL, fmt.Sprintf("deployment_%s.hcl", t.EnvironmentName), hcl.InitialPos)
			if diags.HasErrors() {
				return nil, errors.Wrap(diags, "deployment environment %s template is invalid", t.EnvironmentName, errors.WithErrorCode(errors.EInvalid))
			}

			// Get top-level schema
			schema, _ := gohcl.ImpliedBodySchema(&config.PipelineTemplate{})

			// Parse contents using schema
			content, diags := file.Body.Content(schema)
			if diags.HasErrors() {
				return nil, errors.Wrap(diags, "deployment environment %s template is invalid", t.EnvironmentName, errors.WithErrorCode(errors.EInvalid))
			}

			// Get variable HCL bodies
			variableBodies := map[string]hcl.Body{}
			for _, block := range content.Blocks {
				if block.Type == "variable" {
					variableBodies[block.Labels[0]] = block.Body
				}
			}

			processedTemplates[t.PipelineTemplateID] = variableBodies
		}
	}

	systemVariables := map[string]cty.Value{}

	for _, deploymentTemplate := range deploymentTemplates {
		hclVariableBodies := processedTemplates[deploymentTemplate.PipelineTemplateID]

		ctyValueMap := map[string]cty.Value{}

		vList, ok := variables[deploymentTemplate.EnvironmentName]
		if ok {
			varMap := map[string]string{}
			for _, v := range vList {
				varMap[v.Key] = v.Value
			}

			// Only include the variable body for variables that are passed in
			filteredBodyMap := map[string]hcl.Body{}
			for k := range varMap {
				if body, ok := hclVariableBodies[k]; ok {
					filteredBodyMap[k] = body
				}
			}

			values, err := variable.GetCtyValuesForVariables(filteredBodyMap, varMap, false)
			if err != nil {
				return nil, errors.Wrap(err, "invalid variables for deployment %q", deploymentTemplate.EnvironmentName, errors.WithErrorCode(errors.EInvalid))
			}

			ctyValueMap = values
		}

		systemVariables["deployment_"+deploymentTemplate.EnvironmentName+"_vars"] = cty.ObjectVal(ctyValueMap)
		systemVariables["deployment_"+deploymentTemplate.EnvironmentName+"_template_id"] = cty.StringVal(gid.ToGlobalID(gid.PipelineTemplateType, deploymentTemplate.PipelineTemplateID))
	}

	return systemVariables, nil
}

// createDeploymentVariables creates the deployment variables by de-duplicating them,
// and returning the deployment and environment variables.
func (s *service) createDeploymentVariables(
	variables []VariableInput,
	stages []*config.LifecycleStage,
) (map[string][]pipeline.Variable, []pipeline.Variable, error) {
	var (
		hclVariables         []VariableInput
		environmentVariables []pipeline.Variable
		seen                 = map[string]string{}
	)

	// De-duplicate the variables by key and category.
	for _, variable := range variables {
		if err := variable.validate(); err != nil {
			return nil, nil, err
		}

		mapKey := fmt.Sprintf("%s::%s", variable.Key, variable.Category)

		if variable.EnvironmentName != nil {
			if _, ok := seen[mapKey]; ok {
				return nil, nil, errors.New("variable %s cannot be scoped to an environment and be global", variable.Key, errors.WithErrorCode(errors.EInvalid))
			}

			mapKey = fmt.Sprintf("%s::%s", *variable.EnvironmentName, mapKey)
		}

		if _, ok := seen[mapKey]; ok {
			return nil, nil, errors.New("duplicate variable %s", variable.Key, errors.WithErrorCode(errors.EInvalid))
		}

		seen[mapKey] = variable.Value

		// Add the variable to the appropriate list.
		switch variable.Category {
		case models.HCLVariable:
			hclVariables = append(hclVariables, variable)
		case models.EnvironmentVariable:
			environmentVariables = append(environmentVariables, pipeline.Variable{
				Key:      variable.Key,
				Value:    variable.Value,
				Category: variable.Category,
			})
		}
	}

	// Create the deployment variables.
	deploymentVariables := map[string][]pipeline.Variable{}
	for _, stage := range stages {
		for _, deployment := range stage.Deployments {
			for _, variable := range hclVariables {
				// If the variable is global (i.e. not scoped to an environment) or is scoped to this environment, create it.
				if variable.EnvironmentName == nil || *variable.EnvironmentName == deployment.EnvironmentName {
					v := pipeline.Variable{
						Key:      variable.Key,
						Value:    variable.Value,
						Category: variable.Category,
					}

					deploymentVariables[deployment.EnvironmentName] = append(deploymentVariables[deployment.EnvironmentName], v)
				}
			}
		}
	}

	return deploymentVariables, environmentVariables, nil
}

func (s *service) getReleaseByID(ctx context.Context, span trace.Span, id string) (*models.Release, error) {
	gotRelease, err := s.dbClient.Releases.GetReleaseByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release by ID", errors.WithSpan(span))
	}

	if gotRelease == nil {
		return nil, errors.New("release not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return gotRelease, nil
}
