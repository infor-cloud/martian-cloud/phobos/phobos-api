package release

import (
	"fmt"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// TemplateBuilder builds a template from a release.
type TemplateBuilder struct{}

// NewTemplateBuilder creates a new TemplateBuilder.
func NewTemplateBuilder() *TemplateBuilder {
	return &TemplateBuilder{}
}

// Build builds a pipeline template from raw release lifecycle HCL.
// It converts all lifecycle blocks to pipeline blocks and updates
// the template ID and variables for each deployment block.
func (b *TemplateBuilder) Build(rawLifecycleHCL []byte) ([]byte, error) {
	hclFile, diags := hclwrite.ParseConfig(rawLifecycleHCL, "lifecycle.hcl", hcl.InitialPos)
	if diags.HasErrors() {
		return nil, errors.Wrap(diags, "failed to parse lifecycle HCL", errors.WithErrorCode(errors.EInvalid))
	}

	rootBody := hclFile.Body()

	for _, rootBlock := range rootBody.Blocks() {
		if rootBlock.Type() != "stage" {
			// We're only interested in stage blocks.
			continue
		}

		for _, stageChildBlock := range rootBlock.Body().Blocks() {

			/*
				Convert the deployment block to a pipeline block and update the template ID,
				variables, dependencies, pipeline type and when condition.
				Produces the following:
				pipeline "deployment_<environment_name>" {
					template_id   = system.deployment_<environment_name>_template_id
					pipeline_type = "deployment"
					when          = "manual"
					variables     = system.deployment_<environment_name>_vars
				}
			*/

			if stageChildBlock.Type() == "deployment" {
				stageChildBlock.SetType("pipeline") // Convert the deployment block to a pipeline block.

				deploymentBody := stageChildBlock.Body()

				// Get the environment attribute from the deployment block.
				environmentAttribute := deploymentBody.GetAttribute("environment")

				if environmentAttribute == nil {
					// This should never happen since we validate the lifecycle template
					// so it's most likely a bug in the code.
					return nil, errors.New("environment attribute not found in deployment block")
				}

				// Build the expression tokens for the environment attribute.
				// We must walk the tokens to parse the environment name.
				tokens := environmentAttribute.Expr().BuildTokens(nil)

				var (
					environmentName string
					diags           hcl.Diagnostics
				)

				// Find the environment name in the expression tokens.
				for _, token := range tokens {
					if token.Type != hclsyntax.TokenQuotedLit {
						// Skip tokens that are not quoted literals (e.g. quotes or whitespace).
						continue
					}

					// Attempt to parse the environment name as a Go string.
					// This will correctly handle any escape sequences.
					environmentName, diags = hclsyntax.ParseStringLiteralToken(hclsyntax.Token{
						Type:  token.Type,
						Bytes: token.Bytes,
					})
					if diags.HasErrors() {
						return nil, errors.Wrap(diags, "failed to parse environment name", errors.WithErrorCode(errors.EInvalid))
					}

					// We only expect one quoted literal token.
					break
				}

				// Add a label to the "deployment", now "pipeline" block (its environment name).
				deploymentLabel := fmt.Sprintf("deployment_%s", environmentName)
				stageChildBlock.SetLabels([]string{deploymentLabel})

				deploymentBody.SetAttributeRaw("pipeline_type", hclwrite.TokensForIdentifier("\"deployment\""))

				// Add a new attribute to the deployment block for the template ID.
				deploymentBody.SetAttributeRaw("template_id", hclwrite.TokensForIdentifier("system.deployment_"+environmentName+"_template_id"))

				// Make deployments pipelines manual.
				if deploymentBody.GetAttribute("when") == nil {
					deploymentBody.SetAttributeValue("when", cty.StringVal(string(models.ManualPipeline)))
				}

				dependenciesAttribute := deploymentBody.GetAttribute("dependencies")

				if dependenciesAttribute != nil {
					// Find the dependency in the expression tokens.
					ctyDependencies := []cty.Value{}
					for _, token := range dependenciesAttribute.Expr().BuildTokens(nil) {
						if token.Type != hclsyntax.TokenQuotedLit {
							// Skip tokens that are not quoted literals (e.g. quotes or whitespace).
							continue
						}

						// Attempt to parse the dependency name as a Go string.
						// This will correctly handle any escape sequences.
						dependency, diags := hclsyntax.ParseStringLiteralToken(hclsyntax.Token{
							Type:  token.Type,
							Bytes: token.Bytes,
						})
						if diags.HasErrors() {
							return nil, errors.Wrap(diags, "failed to parse deployment dependency", errors.WithErrorCode(errors.EInvalid))
						}

						// Necessary to append "deployment_" before the dependency name
						// since that'll be the name of the nested pipeline.
						ctyDependencies = append(ctyDependencies, cty.StringVal(fmt.Sprintf("deployment_%s", dependency)))
					}

					deploymentBody.SetAttributeValue("dependencies", cty.ListVal(ctyDependencies))
				}

				// Add a new attribute to the deployment block for the variables.
				deploymentBody.SetAttributeRaw("variables", hclwrite.TokensForIdentifier("system.deployment_"+environmentName+"_vars"))
			}
		}
	}

	// Format the HCL file and return the bytes.
	return hclwrite.Format(hclFile.Bytes()), nil
}
