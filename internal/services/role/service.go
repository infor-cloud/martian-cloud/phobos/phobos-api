// Package role implements the service layer functionality
// related to Phobos roles. Roles allow a Phobos subject
// to access resources offered by the API.
package role

import (
	"context"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
)

// GetRolesInput is the input for querying a list of roles.
type GetRolesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.RoleSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Search filters role list by roleName
	Search *string
}

// CreateRoleInput is the input for creating a Role.
type CreateRoleInput struct {
	Name        string
	Description string
	Permissions []models.Permission
}

// UpdateRoleInput is the input for updating a Role.
type UpdateRoleInput struct {
	Role *models.Role
}

// DeleteRoleInput is the input for deleting a Role.
type DeleteRoleInput struct {
	Role  *models.Role
	Force bool
}

// Service implements all the functionality related to Roles.
type Service interface {
	GetAvailablePermissions(ctx context.Context) ([]string, error)
	GetRoleByID(ctx context.Context, id string) (*models.Role, error)
	GetRoleByPRN(ctx context.Context, prn string) (*models.Role, error)
	GetRoleByName(ctx context.Context, name string) (*models.Role, error)
	GetRolesByIDs(ctx context.Context, idList []string) ([]models.Role, error)
	GetRoles(ctx context.Context, input *GetRolesInput) (*db.RolesResult, error)
	CreateRole(ctx context.Context, input *CreateRoleInput) (*models.Role, error)
	UpdateRole(ctx context.Context, input *UpdateRoleInput) (*models.Role, error)
	DeleteRole(ctx context.Context, input *DeleteRoleInput) error
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	limitChecker    limits.LimitChecker
	activityService activityevent.Service
}

// NewService creates an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		activityService: activityService,
	}
}

func (s *service) GetAvailablePermissions(ctx context.Context) ([]string, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAvailablePermissions")
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	return models.GetAssignablePermissions(), nil
}

func (s *service) GetRoleByID(ctx context.Context, id string) (*models.Role, error) {
	ctx, span := tracer.Start(ctx, "svc.GetRoleByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	role, err := s.getRoleByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get role by ID", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return role, nil
}

func (s *service) GetRoleByPRN(ctx context.Context, prn string) (*models.Role, error) {
	ctx, span := tracer.Start(ctx, "svc.GetRoleByName")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	role, err := s.dbClient.Roles.GetRoleByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get role by PRN", errors.WithSpan(span))
	}

	if role == nil {
		return nil, errors.New("role with PRN %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return role, nil
}

func (s *service) GetRoleByName(ctx context.Context, name string) (*models.Role, error) {
	ctx, span := tracer.Start(ctx, "svc.GetRoleByName")
	span.SetAttributes(attribute.String("name", name))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	role, err := s.dbClient.Roles.GetRoleByName(ctx, name)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get role by name", errors.WithSpan(span))
	}

	if role == nil {
		return nil, errors.New("role with name %s not found", name, errors.WithErrorCode(errors.ENotFound))
	}

	return role, nil
}

func (s *service) GetRolesByIDs(ctx context.Context, idList []string) ([]models.Role, error) {
	ctx, span := tracer.Start(ctx, "svc.GetRolesByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	result, err := s.dbClient.Roles.GetRoles(ctx, &db.GetRolesInput{
		Filter: &db.RoleFilter{
			RoleIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get roles", errors.WithSpan(span))
	}

	return result.Roles, nil
}

func (s *service) GetRoles(ctx context.Context, input *GetRolesInput) (*db.RolesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetRoles")
	if input.Search != nil {
		span.SetAttributes(attribute.String("search", *input.Search))
	}
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	dbInput := &db.GetRolesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.RoleFilter{
			Search: input.Search,
		},
	}

	return s.dbClient.Roles.GetRoles(ctx, dbInput)
}

func (s *service) CreateRole(ctx context.Context, input *CreateRoleInput) (*models.Role, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateRole")
	span.SetAttributes(attribute.String("name", input.Name))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// Only system admins are allowed to create roles
	if !caller.IsAdmin() {
		return nil, errors.New("Only system admins can create roles", errors.WithErrorCode(errors.EForbidden))
	}

	toCreate := &models.Role{
		Name:        input.Name,
		Description: input.Description,
		CreatedBy:   caller.GetSubject(),
	}

	toCreate.SetPermissions(input.Permissions)

	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate role model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Must do a transaction in order to roll back if the limit was exceeded.
	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateRole: %v", txErr)
		}
	}()

	createdRole, err := s.dbClient.Roles.CreateRole(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create role", errors.WithSpan(span))
	}

	// Get the total number of roles to check whether we just violated the limit.
	rolesResult, err := s.dbClient.Roles.GetRoles(txContext, &db.GetRolesInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get roles", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitTotalRoles, rolesResult.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "limit check failed", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			Action:     models.ActionCreate,
			TargetType: models.TargetRole,
			TargetID:   &createdRole.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a role.",
		"caller", caller.GetSubject(),
		"roleName", input.Name,
		"roleID", createdRole.Metadata.ID,
	)

	return createdRole, nil
}

func (s *service) UpdateRole(ctx context.Context, input *UpdateRoleInput) (*models.Role, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateRole")
	span.SetAttributes(attribute.String("id", input.Role.Metadata.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// Only system admins are allowed to update roles
	if !caller.IsAdmin() {
		return nil, errors.New("Only system admins can update roles", errors.WithErrorCode(errors.EForbidden))
	}

	if models.DefaultRoleID(input.Role.Metadata.ID).IsDefaultRole() {
		return nil, errors.New("Default roles are read-only", errors.WithErrorCode(errors.EForbidden))
	}

	if err = input.Role.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate role model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Must do a transaction in order to roll back if the limit was exceeded.
	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateRole: %v", txErr)
		}
	}()

	updatedRole, err := s.dbClient.Roles.UpdateRole(txContext, input.Role)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			Action:     models.ActionUpdate,
			TargetType: models.TargetRole,
			TargetID:   &updatedRole.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a role.",
		"caller", caller.GetSubject(),
		"roleName", input.Role.Name,
		"roleID", input.Role.Metadata.ID,
	)

	return updatedRole, nil
}

func (s *service) DeleteRole(ctx context.Context, input *DeleteRoleInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteRole")
	span.SetAttributes(attribute.String("id", input.Role.Metadata.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	// Only system admins are allowed to delete roles
	if !caller.IsAdmin() {
		return errors.New("Only system admins can delete roles", errors.WithErrorCode(errors.EForbidden))
	}

	if models.DefaultRoleID(input.Role.Metadata.ID).IsDefaultRole() {
		return errors.New("Default roles are read-only", errors.WithErrorCode(errors.EForbidden))
	}

	// Get all the memberships, if any, for this role.
	result, err := s.dbClient.Memberships.GetMemberships(ctx, &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			RoleID: &input.Role.Metadata.ID, // Filter by Role's ID.
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0), // We only need the count.
		},
	})
	if err != nil {
		return errors.Wrap(err, "failed to get memberships", errors.WithSpan(span))
	}

	if !input.Force && result.PageInfo.TotalCount > 0 {
		return errors.New(
			"This Role can't be deleted because it's currently associated with %d memberships. "+
				"Setting force to true will automatically remove all associated memberships.", len(result.Memberships),
			errors.WithErrorCode(errors.EConflict),
		)
	}

	if err := s.dbClient.Roles.DeleteRole(ctx, input.Role); err != nil {
		return err
	}

	s.logger.Infow("Deleted a role.",
		"caller", caller.GetSubject(),
		"roleName", input.Role.Name,
		"roleID", input.Role.Metadata.ID,
	)

	return nil
}

func (s *service) getRoleByID(ctx context.Context, id string) (*models.Role, error) {
	role, err := s.dbClient.Roles.GetRoleByID(ctx, id)
	if err != nil {
		return nil, err
	}

	if role == nil {
		return nil, errors.New("role with id %s not found", id, errors.WithErrorCode(errors.ENotFound))
	}

	return role, nil
}
