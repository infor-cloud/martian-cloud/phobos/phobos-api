package role

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	limitChecker := limits.NewMockLimitChecker(t)
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		activityService: activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, limitChecker, activityService))
}

func TestGetAvailablePermissions(t *testing.T) {
	testCases := []struct {
		name            string
		expectErrorCode errors.CodeType
		expectPerms     []string
		withCaller      bool
	}{
		{
			name:        "successfully retrieve all available permissions",
			expectPerms: models.GetAssignablePermissions(),
			withCaller:  true,
		},
		{
			name:            "without caller",
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)
			}

			service := &service{}
			actualPerms, err := service.GetAvailablePermissions(ctx)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectPerms, actualPerms)
		})
	}
}

func TestGetRoleByID(t *testing.T) {
	roleID := "role-1"

	testCases := []struct {
		caller          auth.Caller
		name            string
		expectedRole    *models.Role
		expectErrorCode errors.CodeType
	}{
		{
			name:         "Role was found",
			caller:       &auth.SystemCaller{},
			expectedRole: &models.Role{Name: "role"},
		},
		{
			name:            "role does not exist",
			caller:          &auth.SystemCaller{},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "without caller",
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockRoles := db.NewMockRoles(t)

			if test.caller != nil {
				mockRoles.On("GetRoleByID", mock.Anything, roleID).Return(test.expectedRole, nil)
			}

			dbClient := &db.Client{
				Roles: mockRoles,
			}

			service := &service{
				dbClient: dbClient,
			}
			actualRole, err := service.GetRoleByID(auth.WithCaller(context.TODO(), test.caller), roleID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectedRole, actualRole)
		})
	}
}

func TestGetRoleByPRN(t *testing.T) {
	rolePRN := "prn:role:role-1"

	testCases := []struct {
		caller          auth.Caller
		name            string
		expectedRole    *models.Role
		expectErrorCode errors.CodeType
	}{
		{
			name:         "Role was found",
			caller:       &auth.SystemCaller{},
			expectedRole: &models.Role{Name: "role"},
		},
		{
			name:            "role does not exist",
			caller:          &auth.SystemCaller{},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "without caller",
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			mockRoles := db.NewMockRoles(t)

			if test.caller != nil {
				mockRoles.On("GetRoleByPRN", mock.Anything, rolePRN).Return(test.expectedRole, nil)
			}

			dbClient := &db.Client{
				Roles: mockRoles,
			}

			service := &service{
				dbClient: dbClient,
			}
			actualRole, err := service.GetRoleByPRN(auth.WithCaller(context.TODO(), test.caller), rolePRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectedRole, actualRole)
		})
	}
}

func TestGetRoleByName(t *testing.T) {
	roleName := "some-role"

	testCases := []struct {
		name            string
		expectedRole    *models.Role
		expectErrorCode errors.CodeType
		withCaller      bool
	}{
		{
			name:         "Role was found",
			expectedRole: &models.Role{Name: "role"},
			withCaller:   true,
		},
		{
			name:            "role does not exist",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "without caller",
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRoles := db.NewMockRoles(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockRoles.On("GetRoleByName", mock.Anything, roleName).Return(test.expectedRole, nil)
			}

			dbClient := &db.Client{
				Roles: mockRoles,
			}

			service := &service{
				dbClient: dbClient,
			}
			actualRole, err := service.GetRoleByName(ctx, roleName)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectedRole, actualRole)
		})
	}
}

func TestGetRolesByIDs(t *testing.T) {
	testCases := []struct {
		name            string
		expectErrorCode errors.CodeType
		input           []string
		expectResult    []models.Role
		withCaller      bool
	}{
		{
			name:         "successfully retrieve roles",
			input:        []string{"role-1", "role-2"},
			expectResult: []models.Role{{Name: "role"}, {Name: "another"}},
			withCaller:   true,
		},
		{
			name:            "without caller",
			input:           []string{"role-1", "role-2"},
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRoles := db.NewMockRoles(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)
				dbInput := &db.GetRolesInput{Filter: &db.RoleFilter{RoleIDs: test.input}}
				mockRoles.On("GetRoles", mock.Anything, dbInput).Return(&db.RolesResult{Roles: test.expectResult}, nil)
			}

			dbClient := &db.Client{
				Roles: mockRoles,
			}

			service := &service{
				dbClient: dbClient,
			}
			actualRoles, err := service.GetRolesByIDs(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualRoles)
		})
	}
}

func TestGetRoles(t *testing.T) {
	sort := db.RoleSortableFieldNameAsc

	testCases := []struct {
		input           *GetRolesInput
		expectResult    *db.RolesResult
		name            string
		expectErrorCode errors.CodeType
		withCaller      bool
	}{
		{
			name: "successfully retrieve roles",
			input: &GetRolesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Search: ptr.String("role"),
			},
			expectResult: &db.RolesResult{
				Roles: []models.Role{{Name: "role"}},
			},
			withCaller: true,
		},
		{
			name:            "without caller",
			input:           &GetRolesInput{Search: ptr.String("role")},
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRoles := db.NewMockRoles(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				dbInput := &db.GetRolesInput{
					Sort:              test.input.Sort,
					PaginationOptions: test.input.PaginationOptions,
					Filter: &db.RoleFilter{
						Search: test.input.Search,
					},
				}
				mockRoles.On("GetRoles", mock.Anything, dbInput).Return(test.expectResult, nil)
			}

			dbClient := &db.Client{
				Roles: mockRoles,
			}

			service := &service{
				dbClient: dbClient,
			}
			actualRoles, err := service.GetRoles(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualRoles)
		})
	}
}

func TestCreateRole(t *testing.T) {
	testSubject := "testSubject"
	roleID := "role-1"

	testCases := []struct {
		limitError      error
		input           *CreateRoleInput
		expectRole      *models.Role
		name            string
		expectErrorCode errors.CodeType
		injectCount     int32
		exceedsLimit    bool
		isAdmin         bool
	}{
		{
			name: "successfully create a role",
			input: &CreateRoleInput{
				Name:        "role",
				Description: "Some new role.",
				Permissions: []models.Permission{},
			},
			injectCount:  4,
			exceedsLimit: false,
			expectRole: &models.Role{
				Metadata: models.ResourceMetadata{
					ID: roleID,
				},
				Name:        "role",
				Description: "Some new role.",
				CreatedBy:   testSubject,
			},
			isAdmin: true,
		},
		{
			name: "permissions are not assignable",
			input: &CreateRoleInput{
				Name:        "role",
				Description: "Some new role.",
				Permissions: []models.Permission{{ResourceType: models.OrganizationResource, Action: models.CreateAction}},
			},
			expectErrorCode: errors.EInvalid,
			isAdmin:         true,
		},
		{
			name: "user is not an admin",
			input: &CreateRoleInput{
				Name:        "role",
				Description: "Some new role.",
				Permissions: []models.Permission{},
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "exceeds limit",
			input: &CreateRoleInput{
				Name:        "role",
				Description: "Some new role.",
				Permissions: []models.Permission{},
			},
			injectCount:     5,
			exceedsLimit:    true,
			limitError:      errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
			isAdmin:         true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockRoles := db.NewMockRoles(t)
			mockCaller := auth.NewMockCaller(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("IsAdmin").Return(test.isAdmin)
			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			if (test.expectRole != nil) || test.exceedsLimit {
				if test.expectRole != nil {
					test.expectRole.SetPermissions(test.input.Permissions)
				}

				mockRoles.On("CreateRole", mock.Anything, mock.Anything).Return(test.expectRole, nil)

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				if !test.exceedsLimit {
					mockTransactions.On("CommitTx", mock.Anything).Return(nil)

					mockActivityEvents.On("CreateActivityEvent", mock.Anything,
						&activityevent.CreateActivityEventInput{
							Action:     models.ActionCreate,
							TargetType: models.TargetRole,
							TargetID:   &roleID,
						},
					).Return(&models.ActivityEvent{}, nil)
				}

				mockRoles.On("GetRoles", mock.Anything, mock.Anything).Return(&db.RolesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: test.injectCount,
					},
				}, nil)

				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitTotalRoles, mock.Anything).Return(test.limitError)
			}

			dbClient := &db.Client{
				Roles:        mockRoles,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
			}

			actualRole, err := service.CreateRole(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectRole, actualRole)
			assert.Equal(t, test.expectRole.GetPermissions(), actualRole.GetPermissions())
		})
	}
}

func TestUpdateRole(t *testing.T) {
	testSubject := "testSubject"

	testCases := []struct {
		input           *UpdateRoleInput
		expectRole      *models.Role
		name            string
		expectErrorCode errors.CodeType
		updatePerms     []models.Permission
		expectPerms     []models.Permission
		isAdmin         bool
	}{
		{
			name: "successfully update a role",
			input: &UpdateRoleInput{
				Role: &models.Role{
					Metadata: models.ResourceMetadata{
						ID: "role-1",
					},
					Name: "role",
				},
			},
			updatePerms: []models.Permission{
				models.CreateMembership,
				models.CreateMembership, // Should be deduplicated.
			},
			expectRole: &models.Role{
				Metadata: models.ResourceMetadata{
					ID: "role-1",
				},
				Name: "role",
			},
			expectPerms: []models.Permission{models.CreateMembership},
			isAdmin:     true,
		},
		{
			name: "permissions are not assignable",
			input: &UpdateRoleInput{
				Role: &models.Role{
					Metadata: models.ResourceMetadata{
						ID: "role-1",
					},
					Name: "role",
				},
			},
			updatePerms: []models.Permission{
				{ResourceType: models.OrganizationResource, Action: models.CreateAction},
			},
			isAdmin:         true,
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "updating a default role",
			input: &UpdateRoleInput{
				Role: &models.Role{
					Metadata: models.ResourceMetadata{
						ID: models.OwnerRoleID.String(),
					},
					Name:        "owner",
					Description: "updated description",
				},
			},
			expectErrorCode: errors.EForbidden,
			isAdmin:         true,
		},
		{
			name: "not an admin",
			input: &UpdateRoleInput{
				Role: &models.Role{},
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockRoles := db.NewMockRoles(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("IsAdmin").Return(test.isAdmin)
			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			// Set models.
			test.input.Role.SetPermissions(test.updatePerms)

			if test.expectRole != nil {

				test.expectRole.SetPermissions(test.expectPerms)

				mockRoles.On("UpdateRole", mock.Anything, test.expectRole).Return(test.expectRole, nil)

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						Action:     models.ActionUpdate,
						TargetType: models.TargetRole,
						TargetID:   &test.expectRole.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Roles:        mockRoles,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			actualRole, err := service.UpdateRole(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectRole, actualRole)
			assert.Equal(t, test.expectPerms, actualRole.GetPermissions())
		})
	}
}

func TestDeleteRole(t *testing.T) {
	testSubject := "testSubject"

	testCases := []struct {
		input                *DeleteRoleInput
		name                 string
		expectErrorCode      errors.CodeType
		totalMembershipCount int32
		isAdmin              bool
	}{
		{
			name: "successfully delete a role without force option",
			input: &DeleteRoleInput{
				Role: &models.Role{
					Metadata: models.ResourceMetadata{
						ID: "role-1",
					},
				},
			},
			isAdmin: true,
		},
		{
			name:                 "successfully deleting a role associated with organization memberships with force option",
			totalMembershipCount: 1,
			input: &DeleteRoleInput{
				Role: &models.Role{
					Metadata: models.ResourceMetadata{
						ID: "role-1",
					},
				},
				Force: true,
			},
			isAdmin: true,
		},
		{
			name:                 "deleting a role associated with organization memberships without force option",
			totalMembershipCount: 5,
			input: &DeleteRoleInput{
				Role: &models.Role{
					Metadata: models.ResourceMetadata{
						ID: "role-1",
					},
				},
			},
			expectErrorCode: errors.EConflict,
			isAdmin:         true,
		},
		{
			name: "deleting a default role",
			input: &DeleteRoleInput{
				Role: &models.Role{
					Metadata: models.ResourceMetadata{
						ID: models.OwnerRoleID.String(),
					},
				},
			},
			expectErrorCode: errors.EForbidden,
			isAdmin:         true,
		},
		{
			name: "user is not an admin",
			input: &DeleteRoleInput{
				Role: &models.Role{},
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockRoles := db.NewMockRoles(t)
			mockMemberships := db.NewMockMemberships(t)

			mockCaller.On("IsAdmin").Return(test.isAdmin)
			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			membershipsInput := &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					RoleID: &test.input.Role.Metadata.ID,
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
			}
			membershipsResult := &db.MembershipsResult{PageInfo: &pagination.PageInfo{TotalCount: test.totalMembershipCount}}
			mockMemberships.On("GetMemberships", mock.Anything, membershipsInput).Return(membershipsResult, nil).Maybe()

			if test.expectErrorCode == "" {
				mockRoles.On("DeleteRole", mock.Anything, test.input.Role).Return(nil)
			}

			dbClient := &db.Client{
				Memberships: mockMemberships,
				Roles:       mockRoles,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient: dbClient,
				logger:   logger,
			}

			err := service.DeleteRole(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
