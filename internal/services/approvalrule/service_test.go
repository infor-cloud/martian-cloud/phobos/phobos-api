package approvalrule

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	limitChecker := limits.NewMockLimitChecker(t)
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		activityService: activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, limitChecker, activityService))
}

func TestGetApprovalRuleByID(t *testing.T) {
	ruleID := "rule-1"
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		name            string
		input           string
		permissionError error
		approvalRule    *models.ApprovalRule
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:  "successfully return an org rule",
			input: ruleID,
			approvalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope: models.OrganizationScope,
				OrgID: orgID,
			},
		},
		{
			name:  "successfully return a project rule",
			input: ruleID,
			approvalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope:     models.ProjectScope,
				OrgID:     orgID,
				ProjectID: &projID,
			},
		},
		{
			name:  "org rule, permission error",
			input: ruleID,
			approvalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope: models.OrganizationScope,
				OrgID: orgID,
			},
			permissionError: errors.New("injected org rule permission error", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:  "proj rule, permission error",
			input: ruleID,
			approvalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope:     models.ProjectScope,
				OrgID:     orgID,
				ProjectID: &projID,
			},
			permissionError: errors.New("injected proj rule permission error", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockApprovalRules := db.NewMockApprovalRules(t)
			mockCaller := auth.NewMockCaller(t)

			mockApprovalRules.On("GetApprovalRuleByID", mock.Anything, ruleID).Return(test.approvalRule, nil)

			switch test.approvalRule.Scope {
			case models.OrganizationScope:
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ApprovalRuleResource, mock.Anything).
					Return(test.permissionError)
			case models.ProjectScope:
				mockCaller.On("RequirePermission", mock.Anything, models.ViewApprovalRule, mock.Anything).
					Return(test.permissionError)
			}

			dbClient := &db.Client{
				ApprovalRules: mockApprovalRules,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualApprovalRule, err := service.GetApprovalRuleByID(auth.WithCaller(ctx, mockCaller), ruleID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.approvalRule, actualApprovalRule)
		})
	}
}

func TestGetApprovalRuleByPRN(t *testing.T) {
	rulePRN := "prn:approval_rule:rule-1"
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		expectApprovalRule *models.ApprovalRule
		name               string
		injectAuthError    error
		expectErrorCode    errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return an org rule",
			expectApprovalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					PRN: rulePRN,
				},
				Scope: models.OrganizationScope,
				OrgID: orgID,
			},
		},
		{
			name: "successfully return a project rule",
			expectApprovalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					PRN: rulePRN,
				},
				Scope:     models.ProjectScope,
				OrgID:     orgID,
				ProjectID: &projID,
			},
		},
		{
			name:            "rule does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view org rule",
			expectApprovalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					PRN: rulePRN,
				},
				Scope: models.OrganizationScope,
				OrgID: orgID,
			},
			injectAuthError: errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project rule",
			expectApprovalRule: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					PRN: rulePRN,
				},
				Scope:     models.ProjectScope,
				OrgID:     orgID,
				ProjectID: &projID,
			},
			injectAuthError: errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockApprovalRules := db.NewMockApprovalRules(t)
			mockCaller := auth.NewMockCaller(t)

			mockApprovalRules.On("GetApprovalRuleByPRN", mock.Anything, rulePRN).Return(test.expectApprovalRule, nil)

			if test.expectApprovalRule != nil {
				switch test.expectApprovalRule.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ApprovalRuleResource, mock.Anything).
						Return(test.injectAuthError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewApprovalRule, mock.Anything).
						Return(test.injectAuthError)
				}
			}

			dbClient := &db.Client{
				ApprovalRules: mockApprovalRules,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualApprovalRule, err := service.GetApprovalRuleByPRN(auth.WithCaller(ctx, mockCaller), rulePRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectApprovalRule, actualApprovalRule)
		})
	}
}

func TestGetApprovalRulesByIDs(t *testing.T) {
	orgRuleID := "org-rule-1"
	projRuleID := "proj-rule-1"
	orgID := "org-1"
	projID := "proj-1"
	inputList := []string{orgRuleID, projRuleID}

	rules := []*models.ApprovalRule{
		{
			Metadata: models.ResourceMetadata{
				ID: orgRuleID,
			},
			Scope: models.OrganizationScope,
			OrgID: orgID,
		},
		{
			Metadata: models.ResourceMetadata{
				ID: projRuleID,
			},
			Scope:     models.ProjectScope,
			OrgID:     orgID,
			ProjectID: &projID,
		},
	}

	type testCase struct {
		orgAuthError        error
		projAuthError       error
		name                string
		expectErrorCode     errors.CodeType
		injectRules         []*models.ApprovalRule
		expectApprovalRules []*models.ApprovalRule
	}

	testCases := []testCase{
		{
			name:                "successfully return rules",
			injectRules:         rules,
			expectApprovalRules: rules,
		},
		{
			name:                "caller does not have permission to view org rule",
			injectRules:         rules,
			orgAuthError:        errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:     errors.EForbidden,
			expectApprovalRules: []*models.ApprovalRule{},
		},
		{
			name:                "caller does not have permission to view proj rule",
			injectRules:         rules,
			projAuthError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:     errors.EForbidden,
			expectApprovalRules: []*models.ApprovalRule{},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockApprovalRules := db.NewMockApprovalRules(t)

			dbInput := &db.GetApprovalRulesInput{
				Filter: &db.ApprovalRuleFilter{
					ApprovalRuleIDs: inputList,
				},
			}
			dbResult := &db.ApprovalRulesResult{
				ApprovalRules: test.injectRules,
			}

			mockApprovalRules.On("GetApprovalRules", mock.Anything, dbInput).Return(dbResult, nil)

			mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ApprovalRuleResource, mock.Anything).
				Return(test.orgAuthError).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.ViewApprovalRule, mock.Anything).
				Return(test.projAuthError).Maybe()

			dbClient := &db.Client{
				ApprovalRules: mockApprovalRules,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualApprovalRules, err := service.GetApprovalRulesByIDs(auth.WithCaller(ctx, mockCaller), inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.ElementsMatch(t, test.expectApprovalRules, actualApprovalRules)
		})
	}
}

func TestGetApprovalRules(t *testing.T) {
	sort := db.ApprovalRuleSortableFieldUpdatedAtAsc
	orgID := "org-1"
	projectID := "proj-1"

	type testCase struct {
		injectAuthError error
		input           *GetApprovalRulesInput
		expectResult    *db.ApprovalRulesResult
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return rules for an organization",
			input: &GetApprovalRulesInput{
				OrganizationID:     &orgID,
				ApprovalRuleScopes: []models.ScopeType{models.OrganizationScope},
				Sort:               &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			},
			expectResult: &db.ApprovalRulesResult{
				ApprovalRules: []*models.ApprovalRule{rulePtr(models.ApprovalRule{Name: "rule-1"})},
			},
		},
		{
			name: "successfully return rules for a project",
			input: &GetApprovalRulesInput{
				ProjectID:          &projectID,
				ApprovalRuleScopes: []models.ScopeType{models.ProjectScope},
				Sort:               &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			},
			expectResult: &db.ApprovalRulesResult{
				ApprovalRules: []*models.ApprovalRule{rulePtr(models.ApprovalRule{Name: "rule-1"})},
			},
		},
		{
			name: "error if no organization or project is specified",
			input: &GetApprovalRulesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "authorization error, looking for organization",
			input: &GetApprovalRulesInput{
				OrganizationID: &orgID,
				Sort:           &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			},
			expectErrorCode: errors.EUnauthorized,
			injectAuthError: errors.New("Unauthorized", errors.WithErrorCode(errors.EUnauthorized)),
		},
		{
			name: "authorization error, looking for project",
			input: &GetApprovalRulesInput{
				ProjectID: &projectID,
				Sort:      &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			},
			expectErrorCode: errors.EUnauthorized,
			injectAuthError: errors.New("Unauthorized", errors.WithErrorCode(errors.EUnauthorized)),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.MockCaller{}
			mockCaller.Test(t)

			askForScopes := []models.ScopeType{}
			if test.input.OrganizationID != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ApprovalRuleResource, mock.Anything).
					Return(test.injectAuthError)
				askForScopes = append(askForScopes, models.OrganizationScope)
			}

			if test.input.ProjectID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewApprovalRule, mock.Anything).
					Return(test.injectAuthError)
				askForScopes = append(askForScopes, models.ProjectScope)
			}

			mockApprovalRules := db.NewMockApprovalRules(t)

			if test.expectResult != nil {
				dbInput := &db.GetApprovalRulesInput{
					Sort:              test.input.Sort,
					PaginationOptions: test.input.PaginationOptions,
					Filter: &db.ApprovalRuleFilter{
						OrgID:              test.input.OrganizationID,
						ProjectID:          test.input.ProjectID,
						ApprovalRuleScopes: askForScopes,
					},
				}
				mockApprovalRules.On("GetApprovalRules", mock.Anything, dbInput).Return(test.expectResult, nil)
			}

			dbClient := &db.Client{
				ApprovalRules: mockApprovalRules,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualResult, err := service.GetApprovalRules(auth.WithCaller(ctx, &mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestCreateApprovalRule(t *testing.T) {
	limit := 10
	orgID := "org-1"
	otherOrgID := "org-2"
	projID := "proj-1"
	otherProjID := "proj-2"

	type testCase struct {
		injectAuthError            error
		input                      *CreateApprovalRuleInput
		expect                     *models.ApprovalRule
		name                       string
		expectErrorCode            errors.CodeType
		existingServiceAccounts    []*models.ServiceAccount
		injectRulesPerOrganization int
	}

	testCases := []testCase{
		{
			name: "successfully create a new organization rule",
			input: &CreateApprovalRuleInput{
				Name:              "some-rule",
				Description:       "This is a new rule",
				Scope:             models.OrganizationScope,
				OrgID:             &orgID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			expect: &models.ApprovalRule{
				Name:              "some-rule",
				Description:       "This is a new rule",
				CreatedBy:         "testuser",
				Scope:             models.OrganizationScope,
				OrgID:             "org-1",
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			existingServiceAccounts: []*models.ServiceAccount{
				{
					Metadata: models.ResourceMetadata{
						ID: "service-account-1",
					},
				},
			},
		},
		{
			name: "successfully create a new project rule",
			input: &CreateApprovalRuleInput{
				Name:              "some-rule",
				Description:       "This is a new rule",
				Scope:             models.ProjectScope,
				ProjectID:         &projID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			expect: &models.ApprovalRule{
				Name:              "some-rule",
				Description:       "This is a new rule",
				CreatedBy:         "testuser",
				Scope:             models.ProjectScope,
				OrgID:             orgID,
				ProjectID:         &projID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			existingServiceAccounts: []*models.ServiceAccount{
				{
					Metadata: models.ResourceMetadata{
						ID: "service-account-1",
					},
				},
			},
		},
		{
			name: "limit exceeded",
			input: &CreateApprovalRuleInput{
				Name:              "some-rule",
				Description:       "This is a new rule",
				Scope:             models.OrganizationScope,
				OrgID:             &orgID,
				ApprovalsRequired: 1,
				UserIDs:           []string{"user-1"},
			},
			expect: &models.ApprovalRule{
				Name:              "some-rule",
				Description:       "This is a new rule",
				CreatedBy:         "testuser",
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				ApprovalsRequired: 1,
				UserIDs:           []string{"user-1"},
			},
			injectRulesPerOrganization: limit + 1,
			expectErrorCode:            errors.EInvalid,
		},
		{
			name: "service account not in org, organization rule",
			input: &CreateApprovalRuleInput{
				Name:              "some-rule",
				Description:       "This is a new rule",
				Scope:             models.OrganizationScope,
				OrgID:             &otherOrgID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			existingServiceAccounts: []*models.ServiceAccount{},
			expectErrorCode:         errors.EInvalid,
		},
		{
			name: "service account not in org, project rule",
			input: &CreateApprovalRuleInput{
				Name:              "some-rule",
				Description:       "This is a new rule",
				Scope:             models.ProjectScope,
				ProjectID:         &otherProjID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			existingServiceAccounts: []*models.ServiceAccount{},
			expectErrorCode:         errors.EInvalid,
		},
		{
			name: "not allowed: project-scope service account on org-scope approval rule",
			input: &CreateApprovalRuleInput{
				Name:              "some-rule",
				Description:       "This would be a new rule",
				Scope:             models.OrganizationScope,
				OrgID:             &orgID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			existingServiceAccounts: []*models.ServiceAccount{
				{
					Metadata: models.ResourceMetadata{
						ID: "service-account-1",
					},
					Scope: models.ProjectScope,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "authorization error",
			input: &CreateApprovalRuleInput{
				Name:              "some-rule",
				Description:       "This is a new rule",
				Scope:             models.OrganizationScope,
				OrgID:             &orgID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			injectAuthError: errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.MockCaller{}
			mockCaller.Test(t)

			mockCaller.On("RequirePermission", mock.Anything, models.CreateApprovalRule, mock.Anything).Return(test.injectAuthError)

			mockCaller.On("GetSubject").Return("testuser")

			mockApprovalRules := db.NewMockApprovalRules(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockProjects := db.NewMockProjects(t)

			if test.existingServiceAccounts != nil {
				mockServiceAccounts.On("GetServiceAccounts", mock.Anything, &db.GetServiceAccountsInput{
					Filter: &db.ServiceAccountFilter{
						ServiceAccountIDs: test.input.ServiceAccountIDs,
						OrganizationID:    test.input.OrgID,
						ProjectID:         test.input.ProjectID,
					},
				}).Return(&db.ServiceAccountsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: int32(len(test.existingServiceAccounts)),
					},
					ServiceAccounts: test.existingServiceAccounts,
				}, nil).Maybe()
			}

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			if test.expect != nil {
				orgID := &test.expect.OrgID
				scope := models.OrganizationScope
				if test.input.ProjectID != nil {
					orgID = nil
					scope = models.ProjectScope
				}
				mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
					Filter: &db.ApprovalRuleFilter{
						ApprovalRuleScopes: []models.ScopeType{scope},
						OrgID:              orgID,
						ProjectID:          test.expect.ProjectID,
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.ApprovalRulesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: int32(test.injectRulesPerOrganization),
					},
				}, nil).Maybe()

				mockApprovalRules.On("CreateApprovalRule", mock.Anything, test.expect).Return(test.expect, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: &test.expect.OrgID,
						ProjectID:      test.input.ProjectID,
						Action:         models.ActionCreate,
						TargetType:     models.TargetApprovalRule,
						TargetID:       &test.expect.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil).Maybe()
			}

			var checkLimitResponse error
			if test.injectRulesPerOrganization > limit {
				checkLimitResponse = errors.New("limit exceeded", errors.WithErrorCode(errors.EInvalid))
			}
			limitToCheck := limits.ResourceLimitApprovalRulesPerOrganization
			if test.input.ProjectID != nil {
				limitToCheck = limits.ResourceLimitApprovalRulesPerProject
			}
			mockLimitChecker.On("CheckLimit", mock.Anything, limitToCheck, int32(test.injectRulesPerOrganization)).
				Return(checkLimitResponse).Maybe()

			if test.input.ProjectID != nil {
				mockProjects.On("GetProjectByID", mock.Anything, *test.input.ProjectID).
					Return(&models.Project{
						OrgID: orgID,
					}, nil).Maybe()
			}

			dbClient := &db.Client{
				ApprovalRules:   mockApprovalRules,
				ServiceAccounts: mockServiceAccounts,
				Transactions:    mockTransactions,
				Projects:        mockProjects,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
			}

			actualCreated, err := service.CreateApprovalRule(auth.WithCaller(ctx, &mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expect, actualCreated)
		})
	}
}

func TestUpdateApprovalRule(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	ruleID := "rule-1"

	type testCase struct {
		injectAuthError         error
		input                   *UpdateApprovalRuleInput
		expect                  *models.ApprovalRule
		name                    string
		expectErrorCode         errors.CodeType
		existingServiceAccounts []*models.ServiceAccount
	}

	testCases := []testCase{
		{
			name: "successful update of an organization rule",
			input: &UpdateApprovalRuleInput{
				ID:                ruleID,
				Description:       ptr.String("This is a new rule"),
				ServiceAccountIDs: []string{"service-account-1"},
			},
			expect: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Name:              "some-rule",
				Description:       "This is a new rule",
				CreatedBy:         "testuser",
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
				UserIDs:           []string{},
				TeamIDs:           []string{},
			},
			existingServiceAccounts: []*models.ServiceAccount{
				{
					Metadata: models.ResourceMetadata{
						ID: "service-account-1",
					},
				},
			},
		},
		{
			name: "successful update of a project rule",
			input: &UpdateApprovalRuleInput{
				ID:                ruleID,
				Description:       ptr.String("This is a new rule"),
				ServiceAccountIDs: []string{"service-account-1"},
			},
			expect: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Name:              "some-rule",
				Description:       "This is a new rule",
				CreatedBy:         "testuser",
				Scope:             models.ProjectScope,
				OrgID:             orgID,
				ProjectID:         &projectID,
				ApprovalsRequired: 1,
				ServiceAccountIDs: []string{"service-account-1"},
				UserIDs:           []string{},
				TeamIDs:           []string{},
			},
			existingServiceAccounts: []*models.ServiceAccount{
				{
					Metadata: models.ResourceMetadata{
						ID: "service-account-1",
					},
				},
			},
		},
		{
			name: "service account not in org, organization rule",
			input: &UpdateApprovalRuleInput{
				ID:                ruleID,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			expect: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope: models.OrganizationScope,
				OrgID: orgID,
			},
			existingServiceAccounts: []*models.ServiceAccount{},
			expectErrorCode:         errors.EInvalid,
		},
		{
			name: "service account not in org, project rule",
			input: &UpdateApprovalRuleInput{
				ID:                ruleID,
				ServiceAccountIDs: []string{"service-account-1"},
			},
			expect: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope:     models.ProjectScope,
				OrgID:     orgID,
				ProjectID: &projectID,
			},
			existingServiceAccounts: []*models.ServiceAccount{},
			expectErrorCode:         errors.EInvalid,
		},
		{
			name: "not allowed: project-scope service account on org-scope approval rule",
			input: &UpdateApprovalRuleInput{
				ID:                ruleID,
				Description:       ptr.String("This is an updated rule"),
				ServiceAccountIDs: []string{"service-account-1"},
			},
			existingServiceAccounts: []*models.ServiceAccount{
				{
					Metadata: models.ResourceMetadata{
						ID: "service-account-1",
					},
					Scope: models.ProjectScope,
				},
			},
			expect: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope: models.OrganizationScope,
				OrgID: orgID,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "org rule, permission error",
			input: &UpdateApprovalRuleInput{
				ID: ruleID,
			},
			expect: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope: models.OrganizationScope,
				OrgID: orgID,
			},
			injectAuthError: errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "project rule, permission error",
			input: &UpdateApprovalRuleInput{
				ID: ruleID,
			},
			expect: &models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				Scope:     models.ProjectScope,
				OrgID:     orgID,
				ProjectID: &projectID,
			},
			injectAuthError: errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.MockCaller{}
			mockCaller.Test(t)

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateApprovalRule, mock.Anything).
				Return(test.injectAuthError).Maybe()
			mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ApprovalRuleResource, mock.Anything).
				Return(test.injectAuthError).Maybe()

			mockCaller.On("GetSubject").Return("testuser")

			mockApprovalRules := db.NewMockApprovalRules(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.existingServiceAccounts != nil {
				localOrgID := &orgID
				if test.expect.ProjectID != nil {
					localOrgID = nil
				}
				mockServiceAccounts.On("GetServiceAccounts", mock.Anything, &db.GetServiceAccountsInput{
					Filter: &db.ServiceAccountFilter{
						ServiceAccountIDs: test.input.ServiceAccountIDs,
						OrganizationID:    localOrgID,
						ProjectID:         test.expect.ProjectID,
					},
				}).Return(&db.ServiceAccountsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: int32(len(test.existingServiceAccounts)),
					},
					ServiceAccounts: test.existingServiceAccounts,
				}, nil).Maybe()
			}

			mockApprovalRules.On("GetApprovalRuleByID", mock.Anything, ruleID).Return(test.expect, nil)

			if test.expectErrorCode == "" {
				mockApprovalRules.On("UpdateApprovalRule", mock.Anything, test.expect).Return(test.expect, nil)

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: &test.expect.OrgID,
						ProjectID:      test.expect.ProjectID,
						Action:         models.ActionUpdate,
						TargetType:     models.TargetApprovalRule,
						TargetID:       &test.expect.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				ApprovalRules:   mockApprovalRules,
				ServiceAccounts: mockServiceAccounts,
				Transactions:    mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			actual, err := service.UpdateApprovalRule(auth.WithCaller(ctx, &mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expect, actual)
		})
	}
}

func TestDeleteApprovalRule(t *testing.T) {
	orgID := "org-1"
	projID := "proj-1"
	ruleID := "rule-1"

	input := &DeleteApprovalRuleInput{
		ID: ruleID,
	}

	orgApprovalRule := &models.ApprovalRule{
		Metadata: models.ResourceMetadata{
			ID: ruleID,
		},
		Scope: models.OrganizationScope,
		OrgID: orgID,
	}

	projApprovalRule := &models.ApprovalRule{
		Metadata: models.ResourceMetadata{
			ID: ruleID,
		},
		Scope:     models.ProjectScope,
		OrgID:     orgID,
		ProjectID: &projID,
	}

	type testCase struct {
		existingApprovalRule *models.ApprovalRule
		name                 string
		injectAuthError      error
		expectErrorCode      errors.CodeType
	}

	testCases := []testCase{
		{
			name:                 "successfully delete an org rule",
			existingApprovalRule: orgApprovalRule,
		},
		{
			name:                 "successfully delete a proj rule",
			existingApprovalRule: projApprovalRule,
		},
		{
			name:            "rule does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                 "org rule, permission error",
			existingApprovalRule: orgApprovalRule,
			injectAuthError:      errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:      errors.EForbidden,
		},
		{
			name:                 "project rule, permission error",
			existingApprovalRule: projApprovalRule,
			injectAuthError:      errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:      errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.MockCaller{}
			mockCaller.Test(t)

			mockCaller.On("RequirePermission", mock.Anything, models.DeleteApprovalRule, mock.Anything).Return(test.injectAuthError)

			mockCaller.On("GetSubject").Return("someone@somewhere.invalid")

			mockOrganizations := db.NewMockOrganizations(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockApprovalRules.On("GetApprovalRuleByID", mock.Anything, input.ID).Return(test.existingApprovalRule, nil)

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockApprovalRules.On("DeleteApprovalRule", mock.Anything, test.existingApprovalRule).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: &test.existingApprovalRule.OrgID,
						ProjectID:      test.existingApprovalRule.ProjectID,
						Action:         models.ActionDelete,
						TargetType:     models.TargetOrganization,
						TargetID:       &test.existingApprovalRule.OrgID,
						Payload: &models.ActivityEventDeleteResourcePayload{
							Name: &test.existingApprovalRule.Name,
							ID:   test.existingApprovalRule.Metadata.ID,
							Type: models.TargetApprovalRule.String(),
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Organizations: mockOrganizations,
				ApprovalRules: mockApprovalRules,
				Transactions:  mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			err := service.DeleteApprovalRule(auth.WithCaller(ctx, &mockCaller), input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func rulePtr(rule models.ApprovalRule) *models.ApprovalRule {
	return &rule
}
