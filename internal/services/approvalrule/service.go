// Package approvalrule implements functionality related to Phobos approval rules.
package approvalrule

import (
	"context"
	"strings"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetApprovalRulesInput is the input for querying a list of approval rules
type GetApprovalRulesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ApprovalRuleSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// OrganizationID filters the approval rules by the specified organization
	OrganizationID *string
	// ProjectID filters the approval rules by the specified project
	ProjectID *string
	// ApprovalRuleScopes will filter the approval rules that are scope to specific level
	ApprovalRuleScopes []models.ScopeType
}

// CreateApprovalRuleInput is the input for creating a approval rule
type CreateApprovalRuleInput struct {
	Scope             models.ScopeType
	OrgID             *string
	ProjectID         *string
	Name              string
	Description       string
	UserIDs           []string
	ServiceAccountIDs []string
	TeamIDs           []string
	ApprovalsRequired int
}

// UpdateApprovalRuleInput is the input for updating a approval rule
// For now, a approval rule cannot move between orgs or into or out of an org.
type UpdateApprovalRuleInput struct {
	Version           *int
	Description       *string
	ID                string
	UserIDs           []string
	ServiceAccountIDs []string
	TeamIDs           []string
}

// DeleteApprovalRuleInput is the input for deleting a approval rule
type DeleteApprovalRuleInput struct {
	Version *int
	ID      string
}

// Service implements all approval rule related functionality
type Service interface {
	GetApprovalRuleByID(ctx context.Context, id string) (*models.ApprovalRule, error)
	GetApprovalRuleByPRN(ctx context.Context, prn string) (*models.ApprovalRule, error)
	GetApprovalRulesByIDs(ctx context.Context, idList []string) ([]*models.ApprovalRule, error)
	GetApprovalRules(ctx context.Context, input *GetApprovalRulesInput) (*db.ApprovalRulesResult, error)
	CreateApprovalRule(ctx context.Context, input *CreateApprovalRuleInput) (*models.ApprovalRule, error)
	UpdateApprovalRule(ctx context.Context, input *UpdateApprovalRuleInput) (*models.ApprovalRule, error)
	DeleteApprovalRule(ctx context.Context, input *DeleteApprovalRuleInput) error
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	limitChecker    limits.LimitChecker
	activityService activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		activityService: activityService,
	}
}

func (s *service) GetApprovalRuleByPRN(ctx context.Context, prn string) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "svc.GetApprovalRuleByPRN")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	rule, err := s.dbClient.ApprovalRules.GetApprovalRuleByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get approval rule by PRN", errors.WithSpan(span))
	}

	if rule == nil {
		return nil, errors.New("approval rule with prn %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	// Check whether the caller is allowed to view this approval rule.
	switch rule.Scope {
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.ApprovalRuleResource, auth.WithOrganizationID(rule.OrgID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewApprovalRule, auth.WithProjectID(*rule.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid approval rule scope: %s", rule.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return rule, nil
}

func (s *service) GetApprovalRuleByID(ctx context.Context, id string) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "svc.GetApprovalRuleByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	rule, err := s.getApprovalRuleByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this approval rule.
	switch rule.Scope {
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.ApprovalRuleResource, auth.WithOrganizationID(rule.OrgID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewApprovalRule, auth.WithProjectID(*rule.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid approval rule scope: %s", rule.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return rule, nil
}

func (s *service) GetApprovalRulesByIDs(ctx context.Context, idList []string) ([]*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "svc.GetApprovalRulesByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetApprovalRulesInput{
		Filter: &db.ApprovalRuleFilter{
			ApprovalRuleIDs: idList,
		},
	}

	resp, err := s.dbClient.ApprovalRules.GetApprovalRules(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get approval rules", errors.WithSpan(span))
	}

	for _, rule := range resp.ApprovalRules {
		switch rule.Scope {
		case models.OrganizationScope:
			err = caller.RequireAccessToInheritableResource(ctx, models.ApprovalRuleResource, auth.WithOrganizationID(rule.OrgID))
			if err != nil {
				return nil, err
			}
		case models.ProjectScope:
			err = caller.RequirePermission(ctx, models.ViewApprovalRule, auth.WithProjectID(*rule.ProjectID))
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("invalid approval rule scope: %s", rule.Scope,
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	return resp.ApprovalRules, nil
}

func (s *service) GetApprovalRules(ctx context.Context, input *GetApprovalRulesInput) (*db.ApprovalRulesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetApprovalRules")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetApprovalRulesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ApprovalRuleFilter{
			OrgID:              input.OrganizationID,
			ProjectID:          input.ProjectID,
			ApprovalRuleScopes: input.ApprovalRuleScopes,
		},
	}

	switch {
	case input.OrganizationID != nil:
		if oErr := caller.RequireAccessToInheritableResource(ctx, models.ApprovalRuleResource,
			auth.WithOrganizationID(*input.OrganizationID)); oErr != nil {
			return nil, oErr
		}
	case input.ProjectID != nil:
		if pErr := caller.RequirePermission(ctx, models.ViewApprovalRule,
			auth.WithProjectID(*input.ProjectID)); pErr != nil {
			return nil, pErr
		}
	default:
		return nil, errors.New("either an organization or project id must be specified",
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	resp, err := s.dbClient.ApprovalRules.GetApprovalRules(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get approval rules", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) CreateApprovalRule(ctx context.Context, input *CreateApprovalRuleInput) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateApprovalRule")
	span.SetAttributes(attribute.String("name", input.Name))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var orgID string
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrgID == nil {
			return nil, errors.New("to create an approval rule of organization scope, must supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.ProjectID != nil {
			return nil, errors.New("to create an approval rule of organization scope, not allowed to supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		orgID = *input.OrgID
		err = caller.RequirePermission(ctx, models.CreateApprovalRule, auth.WithOrganizationID(orgID))
		if err != nil {
			return nil, err
		}

		if err = s.verifyServiceAccountsInOrg(ctx, orgID, input.ServiceAccountIDs); err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("to create an approval rule of project scope, must supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.OrgID != nil {
			return nil, errors.New("to create an approval rule of project scope, not allowed to supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		projectID := *input.ProjectID
		err = caller.RequirePermission(ctx, models.CreateApprovalRule, auth.WithProjectID(projectID))
		if err != nil {
			return nil, err
		}

		if err = s.verifyServiceAccountsInProject(ctx, projectID, input.ServiceAccountIDs); err != nil {
			return nil, err
		}

		// Get the project in order to have the organization ID.
		project, pErr := s.dbClient.Projects.GetProjectByID(ctx, projectID)
		if pErr != nil {
			return nil, pErr
		}
		if project == nil {
			return nil, errors.New("failed to find project with ID %s", projectID, errors.WithSpan(span))
		}

		orgID = project.OrgID
	default:
		return nil, errors.New("not allowed to create an approval rule with %s scope", input.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	toCreate := &models.ApprovalRule{
		Name:              input.Name,
		Description:       input.Description,
		Scope:             input.Scope,
		OrgID:             orgID,
		ProjectID:         input.ProjectID,
		CreatedBy:         caller.GetSubject(),
		UserIDs:           input.UserIDs,
		ServiceAccountIDs: input.ServiceAccountIDs,
		TeamIDs:           input.TeamIDs,
		ApprovalsRequired: input.ApprovalsRequired,
	}

	// Validate model
	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate a approval rule model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateApprovalRule: %v", txErr)
		}
	}()

	rule, err := s.dbClient.ApprovalRules.CreateApprovalRule(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create approval rule", errors.WithSpan(span))
	}

	// Check limit if an org approval rule.
	if input.Scope.Equals(models.OrganizationScope) {
		newApprovalRules, gErr := s.dbClient.ApprovalRules.GetApprovalRules(txContext, &db.GetApprovalRulesInput{
			Filter: &db.ApprovalRuleFilter{
				OrgID:              &rule.OrgID,
				ApprovalRuleScopes: []models.ScopeType{models.OrganizationScope},
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
		})
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get organization's approval rules", errors.WithSpan(span))
		}

		if cErr := s.limitChecker.CheckLimit(
			txContext,
			limits.ResourceLimitApprovalRulesPerOrganization,
			newApprovalRules.PageInfo.TotalCount,
		); cErr != nil {
			return nil, cErr
		}
	}

	// Check limit if an project approval rule.
	if input.Scope.Equals(models.ProjectScope) {
		newApprovalRules, gErr := s.dbClient.ApprovalRules.GetApprovalRules(txContext, &db.GetApprovalRulesInput{
			Filter: &db.ApprovalRuleFilter{
				ProjectID:          rule.ProjectID,
				ApprovalRuleScopes: []models.ScopeType{models.ProjectScope},
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
		})
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get project's approval rules", errors.WithSpan(span))
		}

		if cErr := s.limitChecker.CheckLimit(
			txContext,
			limits.ResourceLimitApprovalRulesPerProject,
			newApprovalRules.PageInfo.TotalCount,
		); cErr != nil {
			return nil, cErr
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &rule.OrgID,
			ProjectID:      rule.ProjectID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetApprovalRule,
			TargetID:       &rule.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a approval rule.",
		"caller", caller.GetSubject(),
		"name", rule.Name,
		"approval ruleID", rule.Metadata.ID,
	)

	return rule, nil
}

func (s *service) UpdateApprovalRule(ctx context.Context, input *UpdateApprovalRuleInput) (*models.ApprovalRule, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateApprovalRule")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	rule, err := s.getApprovalRuleByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	switch rule.Scope {
	case models.OrganizationScope:
		err = caller.RequirePermission(ctx, models.UpdateApprovalRule, auth.WithOrganizationID(rule.OrgID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.UpdateApprovalRule, auth.WithProjectID(*rule.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid approval rule scope: %s", rule.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if input.Version != nil {
		rule.Metadata.Version = *input.Version
	}

	if input.Description != nil {
		rule.Description = *input.Description
	}

	if input.UserIDs != nil {
		rule.UserIDs = input.UserIDs
	}

	if input.ServiceAccountIDs != nil {
		switch rule.Scope {
		case models.OrganizationScope:
			if err = s.verifyServiceAccountsInOrg(ctx, rule.OrgID, input.ServiceAccountIDs); err != nil {
				return nil, err
			}
		case models.ProjectScope:
			if err = s.verifyServiceAccountsInProject(ctx, *rule.ProjectID, input.ServiceAccountIDs); err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("invalid approval rule scope: %s", rule.Scope,
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		rule.ServiceAccountIDs = input.ServiceAccountIDs
	}

	if input.TeamIDs != nil {
		rule.TeamIDs = input.TeamIDs
	}

	if err = rule.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate approval rule model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateApprovalRule: %v", txErr)
		}
	}()

	updatedRule, err := s.dbClient.ApprovalRules.UpdateApprovalRule(txContext, rule)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &rule.OrgID,
			ProjectID:      rule.ProjectID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetApprovalRule,
			TargetID:       &rule.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated an approval rule.",
		"caller", caller.GetSubject(),
		"approvalRuleName", rule.Name,
		"approvalRuleID", rule.Metadata.ID,
	)

	return updatedRule, nil
}

func (s *service) DeleteApprovalRule(ctx context.Context, input *DeleteApprovalRuleInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteApprovalRule")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	rule, err := s.getApprovalRuleByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	switch rule.Scope {
	case models.OrganizationScope:
		err = caller.RequirePermission(ctx, models.DeleteApprovalRule, auth.WithOrganizationID(rule.OrgID))
		if err != nil {
			return err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.DeleteApprovalRule, auth.WithProjectID(*rule.ProjectID))
		if err != nil {
			return err
		}
	default:
		return errors.New("invalid approval rule scope: %s", rule.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if input.Version != nil {
		rule.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteApprovalRule: %v", txErr)
		}
	}()

	if err = s.dbClient.ApprovalRules.DeleteApprovalRule(txContext, rule); err != nil {
		return err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &rule.OrgID,
			ProjectID:      rule.ProjectID,
			Action:         models.ActionDelete,
			TargetType:     models.TargetOrganization,
			TargetID:       &rule.OrgID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &rule.Name,
				ID:   rule.Metadata.ID,
				Type: models.TargetApprovalRule.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted an approval rule.",
		"caller", caller.GetSubject(),
		"approvalRuleName", rule.Name,
		"approvalRuleID", rule.Metadata.ID,
	)

	return nil
}

func (s *service) verifyServiceAccountsInOrg(ctx context.Context, orgID string, serviceAccountIDs []string) error {
	if len(serviceAccountIDs) == 0 {
		return nil
	}

	response, err := s.dbClient.ServiceAccounts.GetServiceAccounts(ctx, &db.GetServiceAccountsInput{
		Filter: &db.ServiceAccountFilter{
			ServiceAccountIDs: serviceAccountIDs,
			OrganizationID:    &orgID,
		},
	})
	if err != nil {
		return err
	}
	if int(response.PageInfo.TotalCount) != len(serviceAccountIDs) {
		return errors.New("one or more service accounts not found", errors.WithErrorCode(errors.EInvalid))
	}

	foundMap := map[string]bool{}
	for _, sa := range response.ServiceAccounts {
		foundMap[sa.Metadata.ID] = true

		// Prohibit assigning a project-scope service account to an org-scope approval rule.
		if sa.Scope.Equals(models.ProjectScope) {
			return errors.New("not allowed to assign project-scope service account %s to organization-scope approval rule",
				sa.Name,
				errors.WithErrorCode(errors.EInvalid))
		}
	}

	notFound := []string{}
	for _, id := range serviceAccountIDs {
		if _, ok := foundMap[id]; !ok {
			notFound = append(notFound, id)
		}
	}

	if len(notFound) > 0 {
		return errors.New("the following service accounts do not exist in org %s: %s", orgID, strings.Join(notFound, ","), errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}

func (s *service) verifyServiceAccountsInProject(ctx context.Context, projectID string, serviceAccountIDs []string) error {
	if len(serviceAccountIDs) == 0 {
		return nil
	}

	// This returns project-scoped service accounts in the project and
	// organization-scoped service accounts in the relevant organization.
	response, err := s.dbClient.ServiceAccounts.GetServiceAccounts(ctx, &db.GetServiceAccountsInput{
		Filter: &db.ServiceAccountFilter{
			ServiceAccountIDs: serviceAccountIDs,
			ProjectID:         &projectID,
		},
	})
	if err != nil {
		return err
	}
	if int(response.PageInfo.TotalCount) != len(serviceAccountIDs) {
		return errors.New("one or more service accounts not found", errors.WithErrorCode(errors.EInvalid))
	}

	foundMap := map[string]interface{}{}
	for _, sa := range response.ServiceAccounts {
		foundMap[sa.Metadata.ID] = nil
	}

	notFound := []string{}
	for _, id := range serviceAccountIDs {
		if _, ok := foundMap[id]; !ok {
			notFound = append(notFound, id)
		}
	}

	if len(notFound) > 0 {
		return errors.New("the following service accounts do not exist in project %s: %s",
			projectID, strings.Join(notFound, ","), errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}

func (s *service) getApprovalRuleByID(ctx context.Context, span trace.Span, id string) (*models.ApprovalRule, error) {
	rule, err := s.dbClient.ApprovalRules.GetApprovalRuleByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get approval rule by ID", errors.WithSpan(span))
	}

	if rule == nil {
		return nil, errors.New(
			"approval rule with id %s not found", id,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	return rule, nil
}
