package membership

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		activityService: activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, activityService))
}

func TestGetMemberships(t *testing.T) {
	organizationID := "organization-1"
	projectID := "project-1"
	userID := "user-1"
	serviceAccountID := "service-account-1"

	type testCase struct {
		name            string
		authError       error
		input           *GetMembershipsInput
		expectErrorCode errors.CodeType
		isUserCaller    bool
		isAdmin         bool
	}

	testCases := []testCase{
		// Positive cases:
		{
			name: "successfully query for project memberships",
			input: &GetMembershipsInput{
				ProjectID:        &projectID,
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
		},
		{
			name: "successfully query for organization memberships",
			input: &GetMembershipsInput{
				OrganizationID:   &organizationID,
				MembershipScopes: []models.ScopeType{models.OrganizationScope},
			},
		},
		{
			name: "user queries for their memberships",
			input: &GetMembershipsInput{
				UserID:           &userID,
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
			isUserCaller: true,
		},
		{
			name: "admin queries for a users memberships",
			input: &GetMembershipsInput{
				UserID:           &userID,
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
			isAdmin:      true,
			isUserCaller: true,
		},
		{
			name: "successfully query for an organization service account memberships",
			input: &GetMembershipsInput{
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					OrganizationID: &organizationID,
					Scope:          models.OrganizationScope,
				},
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
		},
		{
			name: "admin queries by membership types",
			input: &GetMembershipsInput{
				MembershipScopes: []models.ScopeType{models.GlobalScope},
			},
			isAdmin: true,
		},
		// Negative cases:
		{
			name: "caller does not have permission to query project memberships",
			input: &GetMembershipsInput{
				ProjectID:        &projectID,
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "caller does not have permission to query organization memberships",
			input: &GetMembershipsInput{
				OrganizationID:   &organizationID,
				MembershipScopes: []models.ScopeType{models.OrganizationScope},
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "user cannot query for other user's memberships",
			input: &GetMembershipsInput{
				UserID:           ptr.String("user-2"),
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
			isUserCaller:    true,
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "caller does not have permission to query organization service account memberships",
			input: &GetMembershipsInput{
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					OrganizationID: &organizationID,
					Scope:          models.OrganizationScope,
				},
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "cannot query for global service account memberships",
			input: &GetMembershipsInput{
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					Scope: models.GlobalScope,
				},
				MembershipScopes: []models.ScopeType{models.ProjectScope, models.OrganizationScope},
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "non admin user cannot query for global memberships",
			input: &GetMembershipsInput{
				MembershipScopes: []models.ScopeType{models.GlobalScope},
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)

			// Add the appropriate caller for the test case.
			var caller auth.Caller = mockCaller
			if test.isUserCaller {
				caller = &auth.UserCaller{
					User: &models.User{
						Metadata: models.ResourceMetadata{
							ID: userID,
						},
						Admin: test.isAdmin,
					},
				}
			}

			// Mock caller or auth checks.
			switch {
			case test.input.ProjectID != nil,
				test.input.OrganizationID != nil,
				test.input.ServiceAccount != nil && test.input.ServiceAccount.Scope.Equals(models.OrganizationScope):
				mockCaller.On("RequirePermission", mock.Anything, models.ViewMembership, mock.Anything).Return(test.authError)
			}

			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()

			// Mock memberships.
			if test.expectErrorCode == "" {
				dbInput := &db.GetMembershipsInput{
					Filter: &db.MembershipFilter{
						ProjectID:        test.input.ProjectID,
						OrganizationID:   test.input.OrganizationID,
						UserID:           test.input.UserID,
						MembershipScopes: test.input.MembershipScopes,
					},
				}

				if test.input.ServiceAccount != nil {
					dbInput.Filter.ServiceAccountID = &test.input.ServiceAccount.Metadata.ID
				}

				mockMemberships.On("GetMemberships", mock.Anything, dbInput).Return(&db.MembershipsResult{}, nil)
			}

			dbClient := &db.Client{
				Memberships: mockMemberships,
			}

			service := &service{
				dbClient: dbClient,
			}

			_, err := service.GetMemberships(auth.WithCaller(ctx, caller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestGetMembershipByID(t *testing.T) {
	organizationID := "organization-1"
	projectID := "project-1"
	membershipID := "membership-1"

	type testCase struct {
		authError          error
		existingMembership *models.Membership
		name               string
		expectErrorCode    errors.CodeType
		isAdmin            bool
	}

	testCases := []testCase{
		{
			name: "successfully return organization membership",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: membershipID,
				},
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "successfully return project membership",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: membershipID,
				},
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name: "admin queries for a global membership",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: membershipID,
				},
				Scope: models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name:            "membership does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "caller is not a member of the organization",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: membershipID,
				},
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "caller is not a member of the project or organization",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: membershipID,
				},
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "non admin user cannot query for global memberships",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					ID: membershipID,
				},
				Scope: models.GlobalScope,
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)

			if test.existingMembership != nil {
				if !test.existingMembership.Scope.Equals(models.GlobalScope) {
					mockCaller.On("RequirePermission", mock.Anything, models.ViewMembership, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin", mock.Anything).Return(test.isAdmin)
				}
			}

			mockMemberships.On("GetMembershipByID", mock.Anything, membershipID).Return(test.existingMembership, nil)

			dbClient := &db.Client{
				Memberships: mockMemberships,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualMembership, err := service.GetMembershipByID(auth.WithCaller(ctx, mockCaller), membershipID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.authError != nil {
				assert.Equal(t, test.authError, err)
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingMembership, actualMembership)
		})
	}
}

func TestGetMembershipByPRN(t *testing.T) {
	organizationID := "organization-1"
	projectID := "project-1"
	membershipPRN := "prn:membership:membership-1" // Note: PRN not necessarily reflective of membership type.

	type testCase struct {
		authError          error
		existingMembership *models.Membership
		name               string
		expectErrorCode    errors.CodeType
		isAdmin            bool
	}

	testCases := []testCase{
		{
			name: "successfully return organization membership",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					PRN: membershipPRN,
				},
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "successfully return project membership",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					PRN: membershipPRN,
				},
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name: "admin queries for a global membership",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					PRN: membershipPRN,
				},
				Scope: models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name:            "membership does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "caller is not a member of the organization",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					PRN: membershipPRN,
				},
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "caller is not a member of the project or organization",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					PRN: membershipPRN,
				},
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "non admin user cannot query for global memberships",
			existingMembership: &models.Membership{
				Metadata: models.ResourceMetadata{
					PRN: membershipPRN,
				},
				Scope: models.GlobalScope,
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)

			if test.existingMembership != nil {
				if !test.existingMembership.Scope.Equals(models.GlobalScope) {
					mockCaller.On("RequirePermission", mock.Anything, models.ViewMembership, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin", mock.Anything).Return(test.isAdmin)
				}
			}

			mockMemberships.On("GetMembershipByPRN", mock.Anything, membershipPRN).Return(test.existingMembership, nil)

			dbClient := &db.Client{
				Memberships: mockMemberships,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualMembership, err := service.GetMembershipByPRN(auth.WithCaller(ctx, mockCaller), membershipPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.authError != nil {
				assert.Equal(t, test.authError, err)
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingMembership, actualMembership)
		})
	}
}

func TestGetMembershipsByIDs(t *testing.T) {
	organizationID := "organization-1"
	inputList := []string{"membership-1"}

	type testCase struct {
		name                string
		authError           error
		expectErrorCode     errors.CodeType
		existingMemberships []models.Membership
		isAdmin             bool
	}

	testCases := []testCase{
		{
			name: "successfully return organization memberships",
			existingMemberships: []models.Membership{
				{
					Metadata: models.ResourceMetadata{
						ID: inputList[0],
					},
					OrganizationID: &organizationID,
					Scope:          models.OrganizationScope,
				},
			},
		},
		{
			name: "successfully return project memberships",
			existingMemberships: []models.Membership{
				{
					Metadata: models.ResourceMetadata{
						ID: inputList[0],
					},
					ProjectID: &organizationID,
					Scope:     models.ProjectScope,
				},
			},
		},
		{
			name: "admin user queries for global memberships",
			existingMemberships: []models.Membership{
				{
					Metadata: models.ResourceMetadata{
						ID: inputList[0],
					},
					Scope: models.GlobalScope,
				},
			},
			isAdmin: true,
		},
		{
			name: "caller is not a member in all organizations",
			existingMemberships: []models.Membership{
				{
					Metadata: models.ResourceMetadata{
						ID: inputList[0],
					},
					OrganizationID: &organizationID,
					Scope:          models.OrganizationScope,
				},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "non admin user cannot query for global memberships",
			existingMemberships: []models.Membership{
				{
					Metadata: models.ResourceMetadata{
						ID: inputList[0],
					},
					Scope: models.GlobalScope,
				},
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)

			if len(test.existingMemberships) > 0 {
				if !test.existingMemberships[0].Scope.Equals(models.GlobalScope) {
					mockCaller.On("RequirePermission", mock.Anything, models.ViewMembership, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin", mock.Anything).Return(test.isAdmin)
				}
			}

			dbInput := &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					MembershipIDs: inputList,
				},
			}

			dbResult := &db.MembershipsResult{
				Memberships: test.existingMemberships,
			}

			mockMemberships.On("GetMemberships", mock.Anything, dbInput).Return(dbResult, nil).Times(len(test.existingMemberships))

			dbClient := &db.Client{
				Memberships: mockMemberships,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualMemberships, err := service.GetMembershipsByIDs(auth.WithCaller(ctx, mockCaller), inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingMemberships, actualMemberships)
		})
	}
}

func TestCreateOrganizationMembership(t *testing.T) {
	organizationID := "organization-1"
	userID := "user-1"
	serviceAccountID := "service-account-1"
	roleName := "some-role-1"
	projectID := "project-1"

	type testCase struct {
		authError       error
		input           *CreateOrganizationMembershipInput
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create organization membership",
			input: &CreateOrganizationMembershipInput{
				OrganizationID: organizationID,
				User:           &models.User{Metadata: models.ResourceMetadata{ID: userID}},
				RoleID:         models.OwnerRoleID.String(),
			},
		},
		{
			name: "caller does not have permission to create organization membership",
			input: &CreateOrganizationMembershipInput{
				OrganizationID: organizationID,
				User:           &models.User{Metadata: models.ResourceMetadata{ID: userID}},
				RoleID:         models.OwnerRoleID.String(),
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "cannot create organization membership for a global service account",
			input: &CreateOrganizationMembershipInput{
				OrganizationID: organizationID,
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					Scope: models.GlobalScope,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "cannot create organization membership for a service account that is not in the organization",
			input: &CreateOrganizationMembershipInput{
				OrganizationID: organizationID,
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					OrganizationID: ptr.String("organization-2"),
					Scope:          models.OrganizationScope,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "cannot create organization membership for a project service account",
			input: &CreateOrganizationMembershipInput{
				OrganizationID: organizationID,
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					Scope:     models.ProjectScope,
					ProjectID: &projectID,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockRoles := db.NewMockRoles(t)

			mockCaller.On("RequirePermission", mock.Anything, models.CreateMembership, mock.Anything).Return(test.authError)

			if test.expectErrorCode == "" {
				toCreate := &models.Membership{
					OrganizationID: &organizationID,
					RoleID:         test.input.RoleID,
					Scope:          models.OrganizationScope,
				}

				payload := &models.ActivityEventCreateMembershipPayload{
					Role: roleName,
				}

				if test.input.User != nil {
					payload.UserID = &test.input.User.Metadata.ID
					toCreate.UserID = &test.input.User.Metadata.ID
				}

				if test.input.ServiceAccount != nil {
					payload.ServiceAccountID = &test.input.ServiceAccount.Metadata.ID
					toCreate.ServiceAccountID = &test.input.ServiceAccount.Metadata.ID
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockRoles.On("GetRoleByID", mock.Anything, test.input.RoleID).Return(&models.Role{
					Name: roleName,
				}, nil)

				mockMemberships.On("CreateMembership", mock.Anything, toCreate).Return(toCreate, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: &organizationID,
						Action:         models.ActionCreateMembership,
						TargetType:     models.TargetOrganization,
						TargetID:       &organizationID,
						Payload:        payload,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Memberships:  mockMemberships,
				Transactions: mockTransactions,
				Roles:        mockRoles,
			}

			service := &service{
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			actualMembership, err := service.CreateOrganizationMembership(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.NotNil(t, actualMembership)
		})
	}
}

func TestCreateProjectMembership(t *testing.T) {
	projectID := "project-1"
	organizationID := "organization-1"
	userID := "user-1"
	serviceAccountID := "service-account-1"
	roleName := "some-role-1"
	otherProjectID := "other-project-id"

	type testCase struct {
		authError       error
		input           *CreateProjectMembershipInput
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create project membership",
			input: &CreateProjectMembershipInput{
				ProjectID: projectID,
				User:      &models.User{Metadata: models.ResourceMetadata{ID: userID}},
				RoleID:    models.OwnerRoleID.String(),
			},
		},
		{
			name: "caller does not have permission to create project membership",
			input: &CreateProjectMembershipInput{
				ProjectID: projectID,
				User:      &models.User{Metadata: models.ResourceMetadata{ID: userID}},
				RoleID:    models.OwnerRoleID.String(),
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "cannot create project membership for a global service account",
			input: &CreateProjectMembershipInput{
				ProjectID: projectID,
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					Scope: models.GlobalScope,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "cannot create project membership for a service account outside of project's organization",
			input: &CreateProjectMembershipInput{
				ProjectID: projectID,
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					OrganizationID: ptr.String("organization-2"),
					Scope:          models.OrganizationScope,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "cannot create project membership for a project service account that lives in a different project",
			input: &CreateProjectMembershipInput{
				ProjectID: projectID,
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					Scope:     models.ProjectScope,
					ProjectID: &otherProjectID,
				},
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockRoles := db.NewMockRoles(t)

			mockCaller.On("RequirePermission", mock.Anything, models.CreateMembership, mock.Anything).Return(test.authError)

			if test.input.ServiceAccount != nil && test.input.ServiceAccount.Scope.Equals(models.OrganizationScope) {
				mockProjects.On("GetProjectByID", mock.Anything, test.input.ProjectID).Return(&models.Project{
					Metadata: models.ResourceMetadata{
						ID: projectID,
					},
					OrgID: organizationID,
				}, nil)
			}

			if test.expectErrorCode == "" {
				toCreate := &models.Membership{
					ProjectID: &projectID,
					RoleID:    test.input.RoleID,
					Scope:     models.ProjectScope,
				}

				payload := &models.ActivityEventCreateMembershipPayload{
					Role: roleName,
				}

				if test.input.User != nil {
					payload.UserID = &test.input.User.Metadata.ID
					toCreate.UserID = &test.input.User.Metadata.ID
				}

				if test.input.ServiceAccount != nil {
					payload.ServiceAccountID = &test.input.ServiceAccount.Metadata.ID
					toCreate.ServiceAccountID = &test.input.ServiceAccount.Metadata.ID
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockMemberships.On("CreateMembership", mock.Anything, toCreate).Return(toCreate, nil)

				mockRoles.On("GetRoleByID", mock.Anything, test.input.RoleID).Return(&models.Role{
					Name: roleName,
				}, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &projectID,
						Action:     models.ActionCreateMembership,
						TargetType: models.TargetProject,
						TargetID:   &projectID,
						Payload:    payload,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Memberships:  mockMemberships,
				Projects:     mockProjects,
				Transactions: mockTransactions,
				Roles:        mockRoles,
			}

			service := &service{
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			actualMembership, err := service.CreateProjectMembership(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.NotNil(t, actualMembership)
		})
	}
}

func TestCreateGlobalMembership(t *testing.T) {
	userID := "user-1"
	organizationID := "organization-1"
	serviceAccountID := "service-account-1"
	roleName := "some-role-1"

	type testCase struct {
		input           *CreateGlobalMembershipInput
		name            string
		expectErrorCode errors.CodeType
		isAdmin         bool
	}

	testCases := []testCase{
		{
			name: "admin user creates global membership",
			input: &CreateGlobalMembershipInput{
				User:   &models.User{Metadata: models.ResourceMetadata{ID: userID}},
				RoleID: models.OwnerRoleID.String(),
			},
			isAdmin: true,
		},
		{
			name: "cannot create global membership for a non-global service account",
			input: &CreateGlobalMembershipInput{
				ServiceAccount: &models.ServiceAccount{
					Metadata: models.ResourceMetadata{
						ID: serviceAccountID,
					},
					OrganizationID: &organizationID,
					Scope:          models.OrganizationScope,
				},
			},
			expectErrorCode: errors.EInvalid,
			isAdmin:         true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)
			mockTransactions := db.NewMockTransactions(t)
			mockRoles := db.NewMockRoles(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("IsAdmin", mock.Anything).Return(test.isAdmin)

			if test.expectErrorCode == "" {
				toCreate := &models.Membership{
					RoleID: test.input.RoleID,
					Scope:  models.GlobalScope,
				}

				payload := &models.ActivityEventCreateMembershipPayload{
					Role: roleName,
				}

				if test.input.User != nil {
					payload.UserID = &test.input.User.Metadata.ID
					toCreate.UserID = &test.input.User.Metadata.ID
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockMemberships.On("CreateMembership", mock.Anything, toCreate).Return(toCreate, nil)

				mockRoles.On("GetRoleByID", mock.Anything, test.input.RoleID).Return(&models.Role{
					Name: roleName,
				}, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						Action:     models.ActionCreateMembership,
						TargetType: models.TargetGlobal,
						Payload:    payload,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Memberships:  mockMemberships,
				Transactions: mockTransactions,
				Roles:        mockRoles,
			}

			service := &service{
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			actualMembership, err := service.CreateGlobalMembership(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.NotNil(t, actualMembership)
		})
	}
}

func TestUpdateMembership(t *testing.T) {
	userID := "user-1"
	organizationID := "organization-1"
	projectID := "project-1"
	membershipID := "membership-1"
	previousRole := "previous-role-1"
	newRole := "new-role-1"

	type testCase struct {
		authError          error
		existingMembership *models.Membership
		name               string
		expectErrorCode    errors.CodeType
		anotherAdminExists bool
		isAdmin            bool
	}

	testCases := []testCase{
		{
			name: "successfully update organization membership",
			existingMembership: &models.Membership{
				UserID:         &userID,
				RoleID:         models.OwnerRoleID.String(),
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			anotherAdminExists: true,
		},
		{
			name: "successfully update project membership",
			existingMembership: &models.Membership{
				UserID:    &userID,
				RoleID:    models.OwnerRoleID.String(),
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name: "admin user updates global membership",
			existingMembership: &models.Membership{
				UserID: &userID,
				RoleID: models.OwnerRoleID.String(),
				Scope:  models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name: "non admin user cannot update global membership",
			existingMembership: &models.Membership{
				UserID: &userID,
				RoleID: models.OwnerRoleID.String(),
				Scope:  models.GlobalScope,
			},
			expectErrorCode: errors.EForbidden,
		},
		// Can't orphan an organization but can "orphan" a project since subjects will
		// still have access to the project if they are a member of the organization.
		{
			name: "can downgrade project membership regardless of other admins",
			existingMembership: &models.Membership{
				UserID:    &userID,
				RoleID:    models.OwnerRoleID.String(),
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name:            "organization membership does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "cannot downgrade membership as it's the only owner for the organization",
			existingMembership: &models.Membership{
				UserID:         &userID,
				RoleID:         models.OwnerRoleID.String(),
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "caller does not have permission to update memberships",
			existingMembership: &models.Membership{
				UserID:         &userID,
				RoleID:         models.OwnerRoleID.String(),
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			authError: errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)
			mockRoles := db.NewMockRoles(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.existingMembership != nil {
				if !test.existingMembership.Scope.Equals(models.GlobalScope) {
					mockCaller.On("RequirePermission", mock.Anything, models.UpdateMembership, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin", mock.Anything).Return(test.isAdmin)
				}
			}

			mockMemberships.On("GetMembershipByID", mock.Anything, membershipID).Return(test.existingMembership, nil)

			dbInput := &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					OrganizationID: &organizationID,
				},
			}

			roleID := models.DeveloperRoleID.String()
			if test.anotherAdminExists {
				roleID = models.OwnerRoleID.String()
			}

			// This allows us to verify that an organization cannot be orphaned.
			dbResult := &db.MembershipsResult{
				Memberships: []models.Membership{
					{
						Metadata: models.ResourceMetadata{
							ID: "membership-2",
						},
						UserID:         ptr.String("user-2"),
						RoleID:         roleID,
						OrganizationID: &organizationID,
					},
				},
			}

			mockMemberships.On("GetMemberships", mock.Anything, dbInput).Return(dbResult, nil).Maybe()

			if test.expectErrorCode == "" && test.authError == nil {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockRoles.On("GetRoleByID", mock.Anything, mock.Anything).Return(func(_ context.Context, id string) (*models.Role, error) {
					if id == models.DeveloperRoleID.String() {
						return &models.Role{
							Name: newRole,
						}, nil
					}

					return &models.Role{
						Name: previousRole,
					}, nil
				})

				mockMemberships.On("UpdateMembership", mock.Anything, test.existingMembership).Return(test.existingMembership, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: test.existingMembership.OrganizationID,
						ProjectID:      test.existingMembership.ProjectID,
						Action:         models.ActionUpdate,
						TargetType:     models.TargetMembership,
						TargetID:       &test.existingMembership.Metadata.ID,
						Payload: &models.ActivityEventUpdateMembershipPayload{
							PrevRole: previousRole,
							NewRole:  newRole,
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Memberships:  mockMemberships,
				Transactions: mockTransactions,
				Roles:        mockRoles,
			}

			service := &service{
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			input := &UpdateMembershipInput{
				ID:     membershipID,
				RoleID: models.DeveloperRoleID.String(),
			}

			actualMembership, err := service.UpdateMembership(auth.WithCaller(ctx, mockCaller), input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.authError != nil {
				assert.Equal(t, errors.ErrorCode(test.authError), errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingMembership, actualMembership)
		})
	}
}

func TestDeleteMembership(t *testing.T) {
	userID := "user-1"
	organizationID := "organization-1"
	projectID := "project-1"
	membershipID := "membership-1"

	input := &DeleteMembershipInput{
		ID: membershipID,
	}

	type testCase struct {
		authError          error
		existingMembership *models.Membership
		name               string
		expectErrorCode    errors.CodeType
		anotherAdminExists bool
		isAdmin            bool
	}

	testCases := []testCase{
		{
			name: "successfully delete organization membership",
			existingMembership: &models.Membership{
				UserID:         &userID,
				RoleID:         models.OwnerRoleID.String(),
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			anotherAdminExists: true,
		},
		{
			name: "successfully delete project membership",
			existingMembership: &models.Membership{
				UserID:    &userID,
				RoleID:    models.OwnerRoleID.String(),
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name: "admin user deletes global membership",
			existingMembership: &models.Membership{
				UserID: &userID,
				RoleID: models.OwnerRoleID.String(),
				Scope:  models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name: "non admin user cannot delete global membership",
			existingMembership: &models.Membership{
				UserID: &userID,
				RoleID: models.OwnerRoleID.String(),
				Scope:  models.GlobalScope,
			},
			expectErrorCode: errors.EForbidden,
		},
		// Can't orphan an organization but can "orphan" a project since subjects will
		// still have access to the project if they are a member of the organization.
		{
			name: "can delete project membership regardless of other admins",
			existingMembership: &models.Membership{
				UserID:    &userID,
				RoleID:    models.OwnerRoleID.String(),
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name:            "organization membership does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "cannot downgrade membership as it's the only owner for the organization",
			existingMembership: &models.Membership{
				UserID:         &userID,
				RoleID:         models.OwnerRoleID.String(),
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "caller does not have permission to update memberships",
			existingMembership: &models.Membership{
				UserID:         &userID,
				RoleID:         models.OwnerRoleID.String(),
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			authError: errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMemberships := db.NewMockMemberships(t)
			mockCaller := auth.NewMockCaller(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.existingMembership != nil {
				if !test.existingMembership.Scope.Equals(models.GlobalScope) {
					mockCaller.On("RequirePermission", mock.Anything, models.DeleteMembership, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin", mock.Anything).Return(test.isAdmin)
				}
			}

			mockMemberships.On("GetMembershipByID", mock.Anything, membershipID).Return(test.existingMembership, nil)

			dbInput := &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					OrganizationID: &organizationID,
				},
			}

			roleID := models.DeveloperRoleID.String()
			if test.anotherAdminExists {
				roleID = models.OwnerRoleID.String()
			}

			// This allows us to verify that an organization cannot be orphaned.
			dbResult := &db.MembershipsResult{
				Memberships: []models.Membership{
					{
						Metadata: models.ResourceMetadata{
							ID: "membership-2",
						},
						UserID:         ptr.String("user-2"),
						RoleID:         roleID,
						OrganizationID: &organizationID,
					},
				},
			}

			mockMemberships.On("GetMemberships", mock.Anything, dbInput).Return(dbResult, nil).Maybe()

			if test.expectErrorCode == "" && test.authError == nil {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockMemberships.On("DeleteMembership", mock.Anything, test.existingMembership).Return(nil)

				var (
					targetType models.ActivityEventTargetType
					targetID   *string
				)

				switch test.existingMembership.Scope {
				case models.OrganizationScope:
					targetType = models.TargetOrganization
					targetID = test.existingMembership.OrganizationID
				case models.ProjectScope:
					targetType = models.TargetProject
					targetID = test.existingMembership.ProjectID
				case models.GlobalScope:
					targetType = models.TargetGlobal
					// Leave targetID nil for global memberships
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: test.existingMembership.OrganizationID,
						ProjectID:      test.existingMembership.ProjectID,
						Action:         models.ActionRemoveMembership,
						TargetType:     targetType,
						TargetID:       targetID,
						Payload: &models.ActivityEventRemoveMembershipPayload{
							UserID:           test.existingMembership.UserID,
							ServiceAccountID: test.existingMembership.ServiceAccountID,
							TeamID:           test.existingMembership.TeamID,
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Memberships:  mockMemberships,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			err := service.DeleteMembership(auth.WithCaller(ctx, mockCaller), input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if test.authError != nil {
				assert.Equal(t, errors.ErrorCode(test.authError), errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
