// Package membership contains all logic for memberships.
package membership

//go:generate go tool mockery --name Service --inpackage --case underscore

import (
	"context"
	"slices"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetMembershipsInput is the input for retrieving memberships for an organization or project.
type GetMembershipsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.MembershipSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// OrganizationID filters the memberships by organization ID
	OrganizationID *string
	// ProjectID filters the memberships by project ID
	ProjectID *string
	// UserID filters the memberships by user ID
	UserID *string
	// ServiceAccount filters the memberships by service account
	ServiceAccount *models.ServiceAccount
	// MembershipScopes filters the memberships by scope
	MembershipScopes []models.ScopeType
}

// CreateOrganizationMembershipInput is the input for creating an organization membership
type CreateOrganizationMembershipInput struct {
	OrganizationID string
	User           *models.User
	Team           *models.Team
	ServiceAccount *models.ServiceAccount
	RoleID         string
}

// CreateProjectMembershipInput is the input for creating a project membership
type CreateProjectMembershipInput struct {
	ProjectID      string
	User           *models.User
	Team           *models.Team
	ServiceAccount *models.ServiceAccount
	RoleID         string
}

// CreateGlobalMembershipInput is the input for creating a global membership
type CreateGlobalMembershipInput struct {
	User           *models.User
	Team           *models.Team
	ServiceAccount *models.ServiceAccount
	RoleID         string
}

// UpdateMembershipInput is the input for updating a membership
type UpdateMembershipInput struct {
	Version *int
	RoleID  string
	ID      string
}

// DeleteMembershipInput is the input for deleting a membership
type DeleteMembershipInput struct {
	Version *int
	ID      string
}

// Service implements all Membership related functionality
type Service interface {
	GetMemberships(ctx context.Context, input *GetMembershipsInput) (*db.MembershipsResult, error)
	GetMembershipByID(ctx context.Context, id string) (*models.Membership, error)
	GetMembershipByPRN(ctx context.Context, prn string) (*models.Membership, error)
	GetMembershipsByIDs(ctx context.Context, idList []string) ([]models.Membership, error)
	CreateOrganizationMembership(ctx context.Context, input *CreateOrganizationMembershipInput) (*models.Membership, error)
	CreateProjectMembership(ctx context.Context, input *CreateProjectMembershipInput) (*models.Membership, error)
	CreateGlobalMembership(ctx context.Context, input *CreateGlobalMembershipInput) (*models.Membership, error)
	UpdateMembership(ctx context.Context, input *UpdateMembershipInput) (*models.Membership, error)
	DeleteMembership(ctx context.Context, input *DeleteMembershipInput) error
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	activityService activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		activityService: activityService,
	}
}

func (s *service) GetMemberships(
	ctx context.Context,
	input *GetMembershipsInput,
) (*db.MembershipsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetMemberships")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	filter := &db.MembershipFilter{
		MembershipScopes: input.MembershipScopes,
	}

	if slices.Contains(input.MembershipScopes, models.GlobalScope) && !caller.IsAdmin() {
		return nil, errors.New("Only system admins can query for global memberships", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	switch {
	case input.ProjectID != nil:
		err = caller.RequirePermission(ctx, models.ViewMembership, auth.WithProjectID(*input.ProjectID))
		if err != nil {
			return nil, err
		}

		filter.ProjectID = input.ProjectID
	case input.OrganizationID != nil:
		err = caller.RequirePermission(ctx, models.ViewMembership, auth.WithOrganizationID(*input.OrganizationID))
		if err != nil {
			return nil, err
		}

		filter.OrganizationID = input.OrganizationID
	case input.UserID != nil:
		userCaller, ok := caller.(*auth.UserCaller)
		if !ok {
			return nil, errors.New("Unsupported caller type, only users are allowed to query memberships by user ID", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
		}

		if !userCaller.User.Admin && userCaller.User.Metadata.ID != *input.UserID {
			return nil, errors.New("User %s is not authorized to query for memberships for user %s", userCaller.User.Username, *input.UserID, errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
		}

		filter.UserID = input.UserID
	case input.ServiceAccount != nil:
		if !input.ServiceAccount.Scope.Equals(models.OrganizationScope) {
			return nil, errors.New("Cannot query memberships for service account scope %s", input.ServiceAccount.Scope, errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
		}

		err = caller.RequirePermission(ctx, models.ViewMembership, auth.WithOrganizationID(*input.ServiceAccount.OrganizationID))
		if err != nil {
			return nil, err
		}

		filter.ServiceAccountID = &input.ServiceAccount.Metadata.ID
	default:
		if !caller.IsAdmin() {
			return nil, errors.New("Only system admins can query for all memberships", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
		}
	}

	dbInput := &db.GetMembershipsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter:            filter,
	}

	result, err := s.dbClient.Memberships.GetMemberships(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get memberships", errors.WithSpan(span))
	}

	return result, nil
}

func (s *service) GetMembershipByID(ctx context.Context, id string) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "svc.GetMembershipByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	membership, err := s.getMembershipByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	if err = s.requireMembershipPermission(ctx, caller, models.ViewMembership, membership); err != nil {
		return nil, err
	}

	return membership, nil
}

func (s *service) GetMembershipByPRN(ctx context.Context, prn string) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "svc.GetMembershipByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	membership, err := s.dbClient.Memberships.GetMembershipByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get membership by PRN", errors.WithSpan(span))
	}

	if membership == nil {
		return nil, errors.New("membership with PRN %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	if err = s.requireMembershipPermission(ctx, caller, models.ViewMembership, membership); err != nil {
		return nil, err
	}

	return membership, nil
}

func (s *service) GetMembershipsByIDs(ctx context.Context, idList []string) ([]models.Membership, error) {
	ctx, span := tracer.Start(ctx, "svc.GetMembershipsByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			MembershipIDs: idList,
		},
	}

	resp, err := s.dbClient.Memberships.GetMemberships(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get memberships", errors.WithSpan(span))
	}

	for ix := range resp.Memberships {
		if err = s.requireMembershipPermission(ctx, caller, models.ViewMembership, &resp.Memberships[ix]); err != nil {
			return nil, err
		}
	}

	return resp.Memberships, nil
}

func (s *service) CreateOrganizationMembership(
	ctx context.Context,
	input *CreateOrganizationMembershipInput,
) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateOrganizationMembership")
	span.SetAttributes(attribute.String("Organization ID", input.OrganizationID))
	span.SetAttributes(attribute.String("Role ID", input.RoleID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CreateMembership, auth.WithOrganizationID(input.OrganizationID))
	if err != nil {
		return nil, err
	}

	toCreate := &models.Membership{
		RoleID:         input.RoleID,
		OrganizationID: &input.OrganizationID,
		Scope:          models.OrganizationScope,
	}

	if input.User != nil {
		toCreate.UserID = &input.User.Metadata.ID
	}

	if input.Team != nil {
		toCreate.TeamID = &input.Team.Metadata.ID
	}

	if input.ServiceAccount != nil {
		toCreate.ServiceAccountID = &input.ServiceAccount.Metadata.ID

		switch input.ServiceAccount.Scope {
		case models.GlobalScope:
			return nil, errors.New("global service account is not allowed to be a member of an organization",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		case models.OrganizationScope:
			if *input.ServiceAccount.OrganizationID != input.OrganizationID {
				return nil, errors.New("service account does not belong to the organization",
					errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
			}
		case models.ProjectScope:
			return nil, errors.New("project service account is not allowed to be a member of an organization",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateOrganizationMembership: %v", txErr)
		}
	}()

	createdMembership, err := s.dbClient.Memberships.CreateMembership(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create membership", errors.WithSpan(span))
	}

	role, err := s.getRoleByID(ctx, span, input.RoleID)
	if err != nil {
		return nil, err
	}

	if _, err := s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &input.OrganizationID,
			Action:         models.ActionCreateMembership,
			TargetType:     models.TargetOrganization,
			TargetID:       &input.OrganizationID,
			Payload: &models.ActivityEventCreateMembershipPayload{
				UserID:           createdMembership.UserID,
				ServiceAccountID: createdMembership.ServiceAccountID,
				TeamID:           createdMembership.TeamID,
				Role:             role.Name,
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return createdMembership, nil
}

func (s *service) CreateProjectMembership(
	ctx context.Context,
	input *CreateProjectMembershipInput,
) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateProjectMembership")
	span.SetAttributes(attribute.String("Project ID", input.ProjectID))
	span.SetAttributes(attribute.String("Role ID", input.RoleID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CreateMembership, auth.WithProjectID(input.ProjectID))
	if err != nil {
		return nil, err
	}

	toCreate := &models.Membership{
		RoleID:    input.RoleID,
		ProjectID: &input.ProjectID,
		Scope:     models.ProjectScope,
	}

	if input.User != nil {
		toCreate.UserID = &input.User.Metadata.ID
	}

	if input.Team != nil {
		toCreate.TeamID = &input.Team.Metadata.ID
	}

	if input.ServiceAccount != nil {
		toCreate.ServiceAccountID = &input.ServiceAccount.Metadata.ID

		switch input.ServiceAccount.Scope {
		case models.GlobalScope:
			return nil, errors.New("global service account is not allowed to be a member of a project",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		case models.OrganizationScope:
			p, pErr := s.getProjectByID(ctx, span, input.ProjectID)
			if pErr != nil {
				return nil, pErr
			}

			if *input.ServiceAccount.OrganizationID != p.OrgID {
				return nil, errors.New("service account %s does not belong to organization %s",
					input.ServiceAccount.Metadata.ID, p.OrgID, errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
			}
		case models.ProjectScope:
			if *input.ServiceAccount.ProjectID != input.ProjectID {
				return nil, errors.New("service account does not belong to the project",
					errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
			}
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateProjectMembership: %v", txErr)
		}
	}()

	createdMembership, err := s.dbClient.Memberships.CreateMembership(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create membership", errors.WithSpan(span))
	}

	role, err := s.getRoleByID(txContext, span, input.RoleID)
	if err != nil {
		return nil, err
	}

	if _, err := s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &input.ProjectID,
			Action:     models.ActionCreateMembership,
			TargetType: models.TargetProject,
			TargetID:   &input.ProjectID,
			Payload: &models.ActivityEventCreateMembershipPayload{
				UserID:           createdMembership.UserID,
				ServiceAccountID: createdMembership.ServiceAccountID,
				TeamID:           createdMembership.TeamID,
				Role:             role.Name,
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return createdMembership, nil
}

func (s *service) CreateGlobalMembership(
	ctx context.Context,
	input *CreateGlobalMembershipInput,
) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateGlobalMembership")
	span.SetAttributes(attribute.String("Role ID", input.RoleID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if !caller.IsAdmin() {
		return nil, errors.New("Only system admins can create global memberships", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	toCreate := &models.Membership{
		RoleID: input.RoleID,
		Scope:  models.GlobalScope,
	}

	if input.User != nil {
		toCreate.UserID = &input.User.Metadata.ID
	}

	if input.Team != nil {
		toCreate.TeamID = &input.Team.Metadata.ID
	}

	if input.ServiceAccount != nil {
		toCreate.ServiceAccountID = &input.ServiceAccount.Metadata.ID

		if !input.ServiceAccount.Scope.Equals(models.GlobalScope) {
			return nil, errors.New("only global service accounts can be associated with global memberships", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateGlobalMembership: %v", txErr)
		}
	}()

	createdMembership, err := s.dbClient.Memberships.CreateMembership(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create membership", errors.WithSpan(span))
	}

	role, err := s.getRoleByID(txContext, span, input.RoleID)
	if err != nil {
		return nil, err
	}

	if _, err := s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			Action:     models.ActionCreateMembership,
			TargetType: models.TargetGlobal,
			// Leave TargetID nil for global memberships
			Payload: &models.ActivityEventCreateMembershipPayload{
				UserID:           createdMembership.UserID,
				ServiceAccountID: createdMembership.ServiceAccountID,
				TeamID:           createdMembership.TeamID,
				Role:             role.Name,
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return createdMembership, nil
}

func (s *service) UpdateMembership(
	ctx context.Context,
	input *UpdateMembershipInput,
) (*models.Membership, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateMembership")
	span.SetAttributes(attribute.String("Role ID", input.RoleID))
	span.SetAttributes(attribute.String("Membership ID", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	currentMembership, err := s.getMembershipByID(ctx, span, input.ID)
	if err != nil {
		return nil, err
	}

	if err = s.requireMembershipPermission(ctx, caller, models.UpdateMembership, currentMembership); err != nil {
		return nil, err
	}

	if currentMembership.RoleID == input.RoleID {
		// Noop if role being changed to is the same as current role.
		return currentMembership, nil
	}

	// If this membership is an org owner, verify it's not the only owner to prevent the org from becoming orphaned.
	if currentMembership.Scope.Equals(models.OrganizationScope) &&
		models.OwnerRoleID.Equals(currentMembership.RoleID) &&
		!models.OwnerRoleID.Equals(input.RoleID) {
		if err = s.verifyNotOnlyOwner(ctx, currentMembership); err != nil {
			return nil, errors.Wrap(err, "failed to verify this membership is not the only organization owner", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	previousRole, err := s.getRoleByID(ctx, span, currentMembership.RoleID)
	if err != nil {
		return nil, err
	}

	newRole, err := s.getRoleByID(ctx, span, input.RoleID)
	if err != nil {
		return nil, err
	}

	// Update fields.
	currentMembership.RoleID = input.RoleID

	if input.Version != nil {
		currentMembership.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateMembership: %v", txErr)
		}
	}()

	updatedMembership, err := s.dbClient.Memberships.UpdateMembership(txContext, currentMembership)
	if err != nil {
		return nil, err
	}

	if _, err := s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: updatedMembership.OrganizationID,
			ProjectID:      updatedMembership.ProjectID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetMembership,
			TargetID:       &updatedMembership.Metadata.ID,
			Payload: &models.ActivityEventUpdateMembershipPayload{
				PrevRole: previousRole.Name,
				NewRole:  newRole.Name,
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return updatedMembership, nil
}

func (s *service) DeleteMembership(ctx context.Context, input *DeleteMembershipInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteMembership")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	currentMembership, err := s.getMembershipByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	if err = s.requireMembershipPermission(ctx, caller, models.DeleteMembership, currentMembership); err != nil {
		return err
	}

	// Prevent an organization from being orphaned if this is the only owner.
	if currentMembership.Scope.Equals(models.OrganizationScope) && models.OwnerRoleID.Equals(currentMembership.RoleID) {
		if err = s.verifyNotOnlyOwner(ctx, currentMembership); err != nil {
			return errors.Wrap(err, "failed to verify this membership is not the only owner", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	if input.Version != nil {
		currentMembership.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteMembership: %v", txErr)
		}
	}()

	if err = s.dbClient.Memberships.DeleteMembership(txContext, currentMembership); err != nil {
		return err
	}

	var (
		targetType models.ActivityEventTargetType
		targetID   *string
	)

	switch currentMembership.Scope {
	case models.OrganizationScope:
		targetType = models.TargetOrganization
		targetID = currentMembership.OrganizationID
	case models.ProjectScope:
		targetType = models.TargetProject
		targetID = currentMembership.ProjectID
	case models.GlobalScope:
		targetType = models.TargetGlobal
		// Leave targetID nil for global memberships
	}

	if _, err := s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: currentMembership.OrganizationID,
			ProjectID:      currentMembership.ProjectID,
			Action:         models.ActionRemoveMembership,
			TargetType:     targetType,
			TargetID:       targetID,
			Payload: &models.ActivityEventRemoveMembershipPayload{
				UserID:           currentMembership.UserID,
				ServiceAccountID: currentMembership.ServiceAccountID,
				TeamID:           currentMembership.TeamID,
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return nil
}

func (s *service) verifyNotOnlyOwner(ctx context.Context, membership *models.Membership) error {
	// Get all memberships for an organization or project.
	resp, err := s.dbClient.Memberships.GetMemberships(ctx, &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			OrganizationID: membership.OrganizationID,
		},
	})
	if err != nil {
		return err
	}

	otherAdminFound := false
	for _, m := range resp.Memberships {
		if models.OwnerRoleID.Equals(m.RoleID) && m.Metadata.ID != membership.Metadata.ID {
			otherAdminFound = true
			break
		}
	}

	if !otherAdminFound {
		return errors.New(
			"organization membership cannot be modified because it's the only owner of organization %s",
			*membership.OrganizationID,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	return nil
}

func (s *service) requireMembershipPermission(
	ctx context.Context,
	caller auth.Caller,
	permission models.Permission,
	membership *models.Membership,
) error {
	switch membership.Scope {
	case models.ProjectScope:
		err := caller.RequirePermission(ctx, permission, auth.WithProjectID(*membership.ProjectID))
		if err != nil {
			return err
		}
	case models.OrganizationScope:
		err := caller.RequirePermission(ctx, permission, auth.WithOrganizationID(*membership.OrganizationID))
		if err != nil {
			return err
		}
	default:
		if !caller.IsAdmin() {
			return errors.New("Only system admins can %s global memberships", permission.Action, errors.WithErrorCode(errors.EForbidden))
		}
	}

	return nil
}

func (s *service) getMembershipByID(ctx context.Context, span trace.Span, id string) (*models.Membership, error) {
	membership, err := s.dbClient.Memberships.GetMembershipByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get membership by ID", errors.WithSpan(span))
	}

	if membership == nil {
		return nil, errors.New("membership with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return membership, nil
}

func (s *service) getProjectByID(ctx context.Context, span trace.Span, id string) (*models.Project, error) {
	p, gErr := s.dbClient.Projects.GetProjectByID(ctx, id)
	if gErr != nil {
		return nil, errors.Wrap(gErr, "failed to get project by ID", errors.WithSpan(span), errors.WithErrorCode(errors.ENotFound))
	}

	if p == nil {
		return nil, errors.New("project with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return p, nil
}

func (s *service) getRoleByID(ctx context.Context, span trace.Span, id string) (*models.Role, error) {
	role, err := s.dbClient.Roles.GetRoleByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get role by ID", errors.WithSpan(span))
	}

	if role == nil {
		return nil, errors.New("role with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return role, nil
}
