// Package lifecycletemplate implements functionality related to Phobos lifecycle templates.
package lifecycletemplate

import (
	"context"
	"io"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetLifecycleTemplatesInput is the input for querying a list of lifecycle templates
type GetLifecycleTemplatesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.LifecycleTemplateSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// OrganizationID filters the lifecycle templates by the specified organization
	OrganizationID *string
	// ProjectID filters the lifecycle templates by the specified project
	ProjectID *string
	// LifecycleTemplateScopes will filter the lifecycle templates that are scope to specific level
	LifecycleTemplateScopes []models.ScopeType
}

// CreateLifecycleTemplateInput is the input for creating a lifecycle template
type CreateLifecycleTemplateInput struct {
	OrganizationID *string
	ProjectID      *string
	Scope          models.ScopeType
}

// UploadLifecycleTemplateInput is the input for uploading to a lifecycle template
type UploadLifecycleTemplateInput struct {
	Reader io.Reader
	ID     string
}

// Service implements all lifecycle template related functionality
// Please note the HCL data is NOT returned by the Get... methods other than GetLifecycleTemplateData.
type Service interface {
	GetLifecycleTemplateByID(ctx context.Context, id string) (*models.LifecycleTemplate, error)
	GetLifecycleTemplateByPRN(ctx context.Context, prn string) (*models.LifecycleTemplate, error)
	GetLifecycleTemplatesByIDs(ctx context.Context, idList []string) ([]*models.LifecycleTemplate, error)
	GetLifecycleTemplates(ctx context.Context, input *GetLifecycleTemplatesInput) (*db.LifecycleTemplatesResult, error)
	GetLifecycleTemplateData(ctx context.Context, id string) (io.ReadCloser, error)
	CreateLifecycleTemplate(ctx context.Context, input *CreateLifecycleTemplateInput) (*models.LifecycleTemplate, error)
	UploadLifecycleTemplate(ctx context.Context, input *UploadLifecycleTemplateInput) error
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	limitChecker    limits.LimitChecker
	dataStore       DataStore
	activityService activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	dataStore DataStore,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		dataStore:       dataStore,
		limitChecker:    limitChecker,
		activityService: activityService,
	}
}

// Please note the HCL data is NOT returned by this method.
func (s *service) GetLifecycleTemplateByID(ctx context.Context, id string) (*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.GetLifecycleTemplateByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	lifecycleTemplate, err := s.getLifecycleTemplateByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this lifecycle template.
	switch lifecycleTemplate.Scope {
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.LifecycleTemplateResource,
			auth.WithOrganizationID(lifecycleTemplate.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewLifecycleTemplate, auth.WithProjectID(*lifecycleTemplate.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid lifecycle template scope: %s", lifecycleTemplate.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return lifecycleTemplate, nil
}

func (s *service) GetLifecycleTemplateByPRN(ctx context.Context, prn string) (*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.GetLifecycleTemplateByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	lifecycleTemplate, err := s.dbClient.LifecycleTemplates.GetLifecycleTemplateByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle template by PRN", errors.WithSpan(span))
	}

	if lifecycleTemplate == nil {
		return nil, errors.New(
			"lifecycle template with prn %s not found", prn,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	switch lifecycleTemplate.Scope {
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.LifecycleTemplateResource,
			auth.WithOrganizationID(lifecycleTemplate.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewLifecycleTemplate, auth.WithProjectID(*lifecycleTemplate.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid lifecycle template scope: %s", lifecycleTemplate.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return lifecycleTemplate, nil
}

// Please note the HCL data is NOT returned by this method.
func (s *service) GetLifecycleTemplatesByIDs(ctx context.Context, idList []string) ([]*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.GetLifecycleTemplatesByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetLifecycleTemplatesInput{
		Filter: &db.LifecycleTemplateFilter{
			LifecycleTemplateIDs: idList,
		},
	}

	resp, err := s.dbClient.LifecycleTemplates.GetLifecycleTemplates(ctx,
		dbInput,
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle templates", errors.WithSpan(span))
	}

	// Make sure caller has permission to view lifecycle templates in all the parent organizations or projects.
	for _, lt := range resp.LifecycleTemplates {
		switch lt.Scope {
		case models.OrganizationScope:
			err = caller.RequireAccessToInheritableResource(ctx, models.LifecycleTemplateResource,
				auth.WithOrganizationID(lt.OrganizationID))
			if err != nil {
				return nil, err
			}
		case models.ProjectScope:
			err = caller.RequirePermission(ctx, models.ViewLifecycleTemplate, auth.WithProjectID(*lt.ProjectID))
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("invalid lifecycle template scope: %s", lt.Scope,
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	return resp.LifecycleTemplates, nil
}

// Please note the HCL data is NOT returned by this method.
func (s *service) GetLifecycleTemplates(ctx context.Context, input *GetLifecycleTemplatesInput) (*db.LifecycleTemplatesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetLifecycleTemplates")
	if input.OrganizationID != nil {
		span.SetAttributes(attribute.String("organization", *input.OrganizationID))
	}
	if input.ProjectID != nil {
		span.SetAttributes(attribute.String("project", *input.ProjectID))
	}

	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	switch {
	case input.OrganizationID != nil:
		if oErr := caller.RequireAccessToInheritableResource(ctx, models.LifecycleTemplateResource,
			auth.WithOrganizationID(*input.OrganizationID)); oErr != nil {
			return nil, oErr
		}
	case input.ProjectID != nil:
		if pErr := caller.RequirePermission(ctx, models.ViewLifecycleTemplate,
			auth.WithProjectID(*input.ProjectID)); pErr != nil {
			return nil, pErr
		}
	default:
		return nil, errors.New("either an organization or project id must be specified",
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	resp, err := s.dbClient.LifecycleTemplates.GetLifecycleTemplates(ctx, &db.GetLifecycleTemplatesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.LifecycleTemplateFilter{
			OrganizationID:          input.OrganizationID,
			ProjectID:               input.ProjectID,
			LifecycleTemplateScopes: input.LifecycleTemplateScopes,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle templates", errors.WithSpan(span))
	}

	return resp, nil
}

// Please note this method is the only Get... method that returns HCL data.
func (s *service) GetLifecycleTemplateData(ctx context.Context, id string) (io.ReadCloser, error) {
	ctx, span := tracer.Start(ctx, "svc.GetLifecycleTemplateData")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	lifecycleTemplate, err := s.getLifecycleTemplateByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this lifecycle template.
	err = caller.RequireAccessToInheritableResource(ctx, models.LifecycleTemplateResource, auth.WithOrganizationID(lifecycleTemplate.OrganizationID))
	if err != nil {
		return nil, err
	}

	// Return an error if no HCL data has been uploaded.
	if lifecycleTemplate.Status != models.LifecycleTemplateUploaded {
		return nil, errors.New("no lifecycle template data has been uploaded", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Get the HCL data from the data store.
	rdr, err := s.dataStore.GetData(ctx, lifecycleTemplate.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "error downloading lifecycle template data", errors.WithSpan(span))
	}

	return rdr, nil
}

func (s *service) CreateLifecycleTemplate(ctx context.Context, input *CreateLifecycleTemplateInput) (*models.LifecycleTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateLifecycleTemplate")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var orgID string
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("to create a lifecycle template of organization scope, must supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.ProjectID != nil {
			return nil, errors.New("to create a lifecycle template of organization scope, not allowed to supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		orgID = *input.OrganizationID
		err = caller.RequirePermission(ctx, models.CreateLifecycleTemplate, auth.WithOrganizationID(orgID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("to create a lifecycle template of project scope, must supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.OrganizationID != nil {
			return nil, errors.New("to create a lifecycle template of project scope, not allowed to supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		projectID := *input.ProjectID
		err = caller.RequirePermission(ctx, models.CreateLifecycleTemplate, auth.WithProjectID(projectID))
		if err != nil {
			return nil, err
		}

		// Get the project in order to have the organization ID.
		project, pErr := s.dbClient.Projects.GetProjectByID(ctx, projectID)
		if pErr != nil {
			return nil, pErr
		}
		if project == nil {
			return nil, errors.New("failed to find project with ID %s", projectID, errors.WithSpan(span))
		}

		orgID = project.OrgID
	default:
		return nil, errors.New("not allowed to create a lifecycle template with %s scope", input.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Must do a transaction in order to roll back if the limit was exceeded.
	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateLifecycleTemplate: %v", txErr)
		}
	}()

	lifecycleTemplate, err := s.dbClient.LifecycleTemplates.CreateLifecycleTemplate(txContext, &models.LifecycleTemplate{
		Scope:          input.Scope,
		OrganizationID: orgID,
		ProjectID:      input.ProjectID,
		Status:         models.LifecycleTemplatePending,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create lifecycle template", errors.WithSpan(span))
	}

	// Check the limit if an org lifecycle template.
	if input.Scope.Equals(models.OrganizationScope) {
		lifecycleTemplatesResult, gErr := s.dbClient.LifecycleTemplates.GetLifecycleTemplates(txContext, &db.GetLifecycleTemplatesInput{
			Filter: &db.LifecycleTemplateFilter{
				TimeRangeStart:          ptr.Time(lifecycleTemplate.Metadata.CreationTimestamp.Add(-limits.ResourceLimitTimePeriod)),
				OrganizationID:          input.OrganizationID,
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
		})
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get lifecycle templates", errors.WithSpan(span))
		}

		if gErr = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitLifecycleTemplatesPerOrganizationPerTimePeriod,
			lifecycleTemplatesResult.PageInfo.TotalCount); gErr != nil {
			return nil, gErr
		}
	}

	// Check the limit if a project lifecycle template.
	if input.Scope.Equals(models.ProjectScope) {
		lifecycleTemplatesResult, gErr := s.dbClient.LifecycleTemplates.GetLifecycleTemplates(txContext, &db.GetLifecycleTemplatesInput{
			Filter: &db.LifecycleTemplateFilter{
				TimeRangeStart:          ptr.Time(lifecycleTemplate.Metadata.CreationTimestamp.Add(-limits.ResourceLimitTimePeriod)),
				ProjectID:               input.ProjectID,
				LifecycleTemplateScopes: []models.ScopeType{models.ProjectScope},
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
		})
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get lifecycle templates", errors.WithSpan(span))
		}

		if gErr = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitLifecycleTemplatesPerProjectPerTimePeriod,
			lifecycleTemplatesResult.PageInfo.TotalCount); gErr != nil {
			return nil, gErr
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &lifecycleTemplate.OrganizationID,
			ProjectID:      lifecycleTemplate.ProjectID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetLifecycleTemplate,
			TargetID:       &lifecycleTemplate.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a lifecycle template.",
		"caller", caller.GetSubject(),
		"lifecycleTemplateID", lifecycleTemplate.Metadata.ID,
		"organizationID", lifecycleTemplate.OrganizationID,
	)

	return lifecycleTemplate, nil
}

// UploadLifecycleTemplate uploads a template.
// No updates are allowed.
func (s *service) UploadLifecycleTemplate(ctx context.Context, input *UploadLifecycleTemplateInput) error {
	ctx, span := tracer.Start(ctx, "svc.UploadLifecycleTemplate")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	lifecycleTemplate, err := s.getLifecycleTemplateByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	switch lifecycleTemplate.Scope {
	case models.OrganizationScope:
		err = caller.RequirePermission(ctx, models.CreateLifecycleTemplate, auth.WithOrganizationID(lifecycleTemplate.OrganizationID))
		if err != nil {
			return err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.CreateLifecycleTemplate, auth.WithProjectID(*lifecycleTemplate.ProjectID))
		if err != nil {
			return err
		}
	default:
		return errors.New("not allowed to upload a lifecycle template with %s scope", lifecycleTemplate.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if lifecycleTemplate.Status == models.LifecycleTemplateUploaded {
		// Uploading again (aka updating) is not allowed.
		return errors.New("lifecycle template has already been uploaded; updating is not allowed", errors.WithErrorCode(errors.EInvalid))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UploadLifecycleTemplate: %v", txErr)
		}
	}()

	// Update status of the lifecycle template to uploaded before uploading the HCL file.
	lifecycleTemplate.Status = models.LifecycleTemplateUploaded
	if _, err = s.dbClient.LifecycleTemplates.UpdateLifecycleTemplate(txContext, lifecycleTemplate); err != nil {
		return err
	}

	if err = s.dataStore.UploadData(ctx, input.ID, input.Reader); err != nil {
		return errors.Wrap(err, "Failed to write lifecycle template HCL file to object storage", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Uploaded a lifecycle template.",
		"caller", caller.GetSubject(),
		"lifecycleTemplateID", input.ID,
	)

	return nil
}

// getLifecycleTemplateByID returns a non-nil lifecycle template.
// If there is an error or no such template exists, it records the error in the span.
func (s *service) getLifecycleTemplateByID(ctx context.Context, span trace.Span, id string) (*models.LifecycleTemplate, error) {
	gotLifecycleTemplate, err := s.dbClient.LifecycleTemplates.GetLifecycleTemplateByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle template by ID", errors.WithSpan(span))
	}

	if gotLifecycleTemplate == nil {
		return nil, errors.New(
			"lifecycle template with id %s not found", id,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	return gotLifecycleTemplate, nil
}
