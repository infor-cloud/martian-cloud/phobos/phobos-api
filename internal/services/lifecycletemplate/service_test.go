package lifecycletemplate

import (
	"bytes"
	"context"
	"io"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	dataStore := NewMockDataStore(t)
	limitChecker := limits.NewMockLimitChecker(t)
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		dataStore:       dataStore,
		limitChecker:    limitChecker,
		activityService: activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, limitChecker, dataStore, activityService))
}

func TestGetLifecycleTemplateByID(t *testing.T) {
	orgID := "org-id-1"
	projID := "project-id-1"
	lifecycleTemplateID := "lifecycle-template-1"

	orgLifecycleTemplate := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			ID: lifecycleTemplateID,
		},
		Scope:          models.OrganizationScope,
		OrganizationID: orgID,
	}

	projLifecycleTemplate := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			ID: lifecycleTemplateID,
		},
		Scope:          models.ProjectScope,
		OrganizationID: orgID,
		ProjectID:      &projID,
	}

	type testCase struct {
		permissionError         error
		findTemplate            *models.LifecycleTemplate
		expectLifecycleTemplate *models.LifecycleTemplate
		name                    string
		expectErrorCode         errors.CodeType
		withCaller              bool
	}

	/*
		test case template

		type testCase struct {
			name                           string
			withCaller                     bool
			findTemplate                   *models.LifecycleTemplate
			injectPermissionError          error
			expectErrorCode                errors.CodeType
			expectLifecycleTemplate *models.LifecycleTemplate
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "template ID not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "org scope, permission check failed",
			withCaller:      true,
			findTemplate:    orgLifecycleTemplate,
			permissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "proj scope, permission check failed",
			withCaller:      true,
			findTemplate:    projLifecycleTemplate,
			permissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:                    "successfully return an org-scoped lifecycle template",
			withCaller:              true,
			findTemplate:            orgLifecycleTemplate,
			expectLifecycleTemplate: orgLifecycleTemplate,
		},
		{
			name:                    "successfully return a proj-scoped lifecycle template",
			withCaller:              true,
			findTemplate:            projLifecycleTemplate,
			expectLifecycleTemplate: projLifecycleTemplate,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockOrganizations := db.NewMockOrganizations(t)
			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockLifecycleTemplates.On("GetLifecycleTemplateByID", mock.Anything, lifecycleTemplateID).
					Return(test.findTemplate, nil)

				if test.findTemplate != nil {
					switch test.findTemplate.Scope {
					case models.OrganizationScope:
						mockCaller.On("RequireAccessToInheritableResource", mock.Anything,
							models.LifecycleTemplateResource, mock.Anything).
							Return(test.permissionError)
					case models.ProjectScope:
						mockCaller.On("RequirePermission", mock.Anything, models.ViewLifecycleTemplate, mock.Anything).
							Return(test.permissionError)
					}
				}
			}

			dbClient := &db.Client{
				Organizations:      mockOrganizations,
				LifecycleTemplates: mockLifecycleTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualLifecycleTemplate, err := service.GetLifecycleTemplateByID(ctx, lifecycleTemplateID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectLifecycleTemplate, actualLifecycleTemplate)
		})
	}
}

func TestGetLifecycleTemplateByPRN(t *testing.T) {
	orgID := "some-org"
	projID := "some-proj"
	orgLifecycleTemplatePRN := "prn:lifecycle_template:some-org/some-lifecycle-template"
	projLifecycleTemplatePRN := "prn:lifecycle_template:some-org/some-proj/some-lifecycle-template"

	orgLifecycleTemplate := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			PRN: orgLifecycleTemplatePRN,
		},
		Scope:          models.OrganizationScope,
		OrganizationID: orgID,
	}

	projLifecycleTemplate := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			PRN: projLifecycleTemplatePRN,
		},
		Scope:          models.ProjectScope,
		OrganizationID: orgID,
		ProjectID:      &projID,
	}

	type testCase struct {
		permissionError         error
		findPRN                 string
		template                *models.LifecycleTemplate
		expectLifecycleTemplate *models.LifecycleTemplate
		name                    string
		expectErrorCode         errors.CodeType
		withCaller              bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			findPRN:         orgLifecycleTemplatePRN,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "org template not found",
			withCaller:      true,
			findPRN:         orgLifecycleTemplatePRN,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "proj template not found",
			withCaller:      true,
			findPRN:         orgLifecycleTemplatePRN,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "org, permission check failed",
			withCaller:      true,
			findPRN:         orgLifecycleTemplatePRN,
			template:        orgLifecycleTemplate,
			permissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "proj, permission check failed",
			withCaller:      true,
			findPRN:         projLifecycleTemplatePRN,
			template:        projLifecycleTemplate,
			permissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:                    "successfully return a lifecycle template",
			withCaller:              true,
			findPRN:                 orgLifecycleTemplatePRN,
			template:                orgLifecycleTemplate,
			expectLifecycleTemplate: orgLifecycleTemplate,
		},
		{
			name:                    "successfully return a lifecycle template",
			withCaller:              true,
			findPRN:                 projLifecycleTemplatePRN,
			template:                projLifecycleTemplate,
			expectLifecycleTemplate: projLifecycleTemplate,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockLifecycleTemplates.On("GetLifecycleTemplateByPRN", mock.Anything, orgLifecycleTemplatePRN).
					Return(test.template, nil).Maybe()
				mockLifecycleTemplates.On("GetLifecycleTemplateByPRN", mock.Anything, projLifecycleTemplatePRN).
					Return(test.template, nil).Maybe()

				if test.template != nil {
					switch test.template.Scope {
					case models.OrganizationScope:
						mockCaller.On("RequireAccessToInheritableResource", mock.Anything,
							models.LifecycleTemplateResource, mock.Anything).
							Return(test.permissionError)
					case models.ProjectScope:
						mockCaller.On("RequirePermission", mock.Anything, models.ViewLifecycleTemplate, mock.Anything).
							Return(test.permissionError)
					}
				}
			}

			dbClient := &db.Client{
				LifecycleTemplates: mockLifecycleTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualLifecycleTemplate, err := service.GetLifecycleTemplateByPRN(ctx, test.findPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectLifecycleTemplate, actualLifecycleTemplate)
		})
	}
}

func TestGetLifecycleTemplatesByIDs(t *testing.T) {
	inputList := []string{"pt-1", "pt-2"}

	goodTemplates := []*models.LifecycleTemplate{
		{
			Scope:          models.OrganizationScope,
			OrganizationID: "orgID-1",
		},
		{
			Scope:          models.ProjectScope,
			OrganizationID: "orgID-2",
			ProjectID:      ptr.String("projID-2"),
		},
	}

	type testCase struct {
		name                     string
		expectErrorCode          errors.CodeType
		expectLifecycleTemplates []*models.LifecycleTemplate
		withCaller               bool
	}

	/*
		test case template

		type testCase struct {
			name                            string
			withCaller                      bool
			expectErrorCode                 errors.CodeType
			expectLifecycleTemplates []*models.LifecycleTemplate
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "templates not found, possibly due to lack of permissions",
			withCaller: true,
		},
		{
			name:                     "successfully return lifecycle templates",
			withCaller:               true,
			expectLifecycleTemplates: goodTemplates,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)

			if test.withCaller {
				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)

				dbInput := &db.GetLifecycleTemplatesInput{
					Filter: &db.LifecycleTemplateFilter{
						LifecycleTemplateIDs: inputList,
					},
				}
				dbResult := &db.LifecycleTemplatesResult{
					LifecycleTemplates: test.expectLifecycleTemplates,
				}

				mockLifecycleTemplates.On("GetLifecycleTemplates", mock.Anything, dbInput).
					Return(dbResult, nil)
			}

			dbClient := &db.Client{
				LifecycleTemplates: mockLifecycleTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualLifecycleTemplates, err := service.GetLifecycleTemplatesByIDs(ctx, inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectLifecycleTemplates, actualLifecycleTemplates)
		})
	}
}

func TestGetLifecycleTemplates(t *testing.T) {
	orgID1 := "organization-ID-1"
	projID1 := "project-ID-1"
	orgTemplateID := "org-template-ID-1"
	projTemplateID := "proj-template-ID-1"
	sort := db.LifecycleTemplateSortableFieldUpdatedAtAsc

	type testCase struct {
		permissionError error
		input           *GetLifecycleTemplatesInput
		expectResult    *db.LifecycleTemplatesResult
		name            string
		expectErrorCode errors.CodeType
		fromDB          []*models.LifecycleTemplate
		withCaller      bool
	}

	/*
		test case template

		type testCase struct {
		name            string
		withCaller      bool
		input           *GetLifecycleTemplatesInput
		permissionError error
		fromDB          []*models.LifecycleTemplate
		expectResult    *db.LifecycleTemplatesResult
		expectErrorCode errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name: "without caller",
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				OrganizationID: &orgID1,
			},
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "org scope, permission error",
			withCaller: true,
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				OrganizationID:          &orgID1,
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
			},
			permissionError: errors.New("injected org scope permission error", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "proj scope, permission error",
			withCaller: true,
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				ProjectID:               &projID1,
				LifecycleTemplateScopes: []models.ScopeType{models.ProjectScope},
			},
			permissionError: errors.New("injected proj scope permission error", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "org scope, not found",
			withCaller: true,
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				OrganizationID:          ptr.String("non-existent-org-id"),
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
			},
			fromDB: []*models.LifecycleTemplate{
				{},
			},
			expectResult: &db.LifecycleTemplatesResult{
				LifecycleTemplates: []*models.LifecycleTemplate{
					{},
				},
			},
		},
		{
			name:       "proj scope, not found",
			withCaller: true,
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				ProjectID:               &projID1,
				LifecycleTemplateScopes: []models.ScopeType{models.ProjectScope},
			},
			fromDB: []*models.LifecycleTemplate{
				{},
			},
			expectResult: &db.LifecycleTemplatesResult{
				LifecycleTemplates: []*models.LifecycleTemplate{
					{},
				},
			},
		},
		{
			name:       "org scope, successfully returned",
			withCaller: true,
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				OrganizationID:          &orgID1,
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope},
			},
			fromDB: []*models.LifecycleTemplate{
				{
					Metadata: models.ResourceMetadata{
						ID: orgTemplateID,
					},
					Scope:          models.OrganizationScope,
					OrganizationID: orgID1,
				},
			},
			expectResult: &db.LifecycleTemplatesResult{
				LifecycleTemplates: []*models.LifecycleTemplate{
					{
						Metadata: models.ResourceMetadata{
							ID: orgTemplateID,
						},
						Scope:          models.OrganizationScope,
						OrganizationID: orgID1,
					},
				},
			},
		},
		{
			name:       "proj scope, successfully returned",
			withCaller: true,
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				ProjectID:               &projID1,
				LifecycleTemplateScopes: []models.ScopeType{models.ProjectScope},
			},
			fromDB: []*models.LifecycleTemplate{
				{
					Metadata: models.ResourceMetadata{
						ID: projTemplateID,
					},
					Scope:          models.OrganizationScope,
					OrganizationID: orgID1,
					ProjectID:      &projID1,
				},
			},
			expectResult: &db.LifecycleTemplatesResult{
				LifecycleTemplates: []*models.LifecycleTemplate{
					{
						Metadata: models.ResourceMetadata{
							ID: projTemplateID,
						},
						Scope:          models.OrganizationScope,
						OrganizationID: orgID1,
						ProjectID:      &projID1,
					},
				},
			},
		},
		{
			name:       "both scopes, successfully returned",
			withCaller: true,
			input: &GetLifecycleTemplatesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				OrganizationID:          &orgID1,
				LifecycleTemplateScopes: []models.ScopeType{models.OrganizationScope, models.ProjectScope},
			},
			fromDB: []*models.LifecycleTemplate{
				{
					Metadata: models.ResourceMetadata{
						ID: orgTemplateID,
					},
					Scope:          models.OrganizationScope,
					OrganizationID: orgID1,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: projTemplateID,
					},
					Scope:          models.OrganizationScope,
					OrganizationID: orgID1,
					ProjectID:      &projID1,
				},
			},
			expectResult: &db.LifecycleTemplatesResult{
				LifecycleTemplates: []*models.LifecycleTemplate{
					{
						Metadata: models.ResourceMetadata{
							ID: orgTemplateID,
						},
						Scope:          models.OrganizationScope,
						OrganizationID: orgID1,
					},
					{
						Metadata: models.ResourceMetadata{
							ID: projTemplateID,
						},
						Scope:          models.OrganizationScope,
						OrganizationID: orgID1,
						ProjectID:      &projID1,
					},
				},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)
			mockCaller := auth.NewMockCaller(t)

			mockCaller.On("RequireAccessToInheritableResource", mock.Anything,
				models.LifecycleTemplateResource, mock.Anything).
				Return(test.permissionError).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.ViewLifecycleTemplate, mock.Anything).
				Return(test.permissionError).Maybe()

			if test.expectResult != nil {
				dbInput := &db.GetLifecycleTemplatesInput{
					Sort:              test.input.Sort,
					PaginationOptions: test.input.PaginationOptions,
					Filter: &db.LifecycleTemplateFilter{
						LifecycleTemplateScopes: test.input.LifecycleTemplateScopes,
						OrganizationID:          test.input.OrganizationID,
						ProjectID:               test.input.ProjectID,
					},
				}
				mockLifecycleTemplates.On("GetLifecycleTemplates", mock.Anything, dbInput).Return(test.expectResult, nil)
			}

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)
			}

			dbClient := &db.Client{
				LifecycleTemplates: mockLifecycleTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualResult, err := service.GetLifecycleTemplates(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestGetLifecycleTemplateData(t *testing.T) {
	lifecycleTemplateID := "lifecycle-template-1"

	goodLifecycleTemplate := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			ID: lifecycleTemplateID,
		},
	}

	goodData := []byte("this is good lifecycle template data")

	type testCase struct {
		injectPermissionOrDataError error
		injectDownloadError         error
		findTemplate                *models.LifecycleTemplate
		name                        string
		expectErrorCode             errors.CodeType
		expectData                  []byte
		withCaller                  bool
		hasBeenUploaded             bool
	}

	/*
		test case template

		type testCase struct {
			name                        string
			withCaller                  bool
			findTemplate                *models.LifecycleTemplate
			hasBeenUploaded             bool
			injectPermissionOrDataError error
			injectDownloadError         error
			expectErrorCode             errors.CodeType
			expectData                  []byte
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "template not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                        "permission check failed",
			withCaller:                  true,
			findTemplate:                goodLifecycleTemplate,
			injectPermissionOrDataError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:             errors.EForbidden,
		},
		{
			name:                        "data has not yet been uploaded",
			withCaller:                  true,
			findTemplate:                goodLifecycleTemplate,
			injectPermissionOrDataError: errors.New("no lifecycle template data has been uploaded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode:             errors.EInvalid,
		},
		{
			name:                "download failed",
			withCaller:          true,
			findTemplate:        goodLifecycleTemplate,
			hasBeenUploaded:     true,
			injectDownloadError: errors.New("error while downloading lifecycle template data"),
			expectErrorCode:     errors.EInternal,
		},
		{
			name:            "successfully return data",
			withCaller:      true,
			findTemplate:    goodLifecycleTemplate,
			hasBeenUploaded: true,
			expectData:      goodData,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockOrganizations := db.NewMockOrganizations(t)
			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)
			mockCaller := auth.NewMockCaller(t)
			mockDataStore := NewMockDataStore(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				// Make a (possibly modified) copy of test.findTemplate.
				var copyFindTemplate *models.LifecycleTemplate
				if test.findTemplate != nil {
					copyFoundTemplate := *test.findTemplate
					if test.hasBeenUploaded {
						copyFoundTemplate.Status = models.LifecycleTemplateUploaded
					}
					copyFindTemplate = &copyFoundTemplate
				}

				mockLifecycleTemplates.On("GetLifecycleTemplateByID", mock.Anything, lifecycleTemplateID).
					Return(copyFindTemplate, nil)

				if copyFindTemplate != nil {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.LifecycleTemplateResource, mock.Anything).
						Return(test.injectPermissionOrDataError)

					if test.injectPermissionOrDataError == nil {

						mockDataStore.On("GetData", mock.Anything, mock.Anything).
							Return(func(_ context.Context, _ string) (io.ReadCloser, error) {
								copyGoodData := goodData
								return io.NopCloser(bytes.NewReader(copyGoodData)), test.injectDownloadError
							})
					}
				}
			}

			dbClient := &db.Client{
				Organizations:      mockOrganizations,
				LifecycleTemplates: mockLifecycleTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:    logger,
				dbClient:  dbClient,
				dataStore: mockDataStore,
			}

			actualReader, err := service.GetLifecycleTemplateData(ctx, lifecycleTemplateID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			actualData, err := io.ReadAll(actualReader)
			assert.Nil(t, err)
			actualReader.Close()

			assert.Equal(t, test.expectData, actualData)
		})
	}
}

func TestCreateLifecycleTemplate(t *testing.T) {
	currentTime := time.Now().UTC()

	parentOrg := &models.Organization{
		Metadata: models.ResourceMetadata{
			ID: "org-1",
		},
	}

	parentProj := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: "proj-1",
		},
	}

	orgLifecycleTemplate := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			CreationTimestamp: &currentTime,
		},
		Scope:          models.OrganizationScope,
		OrganizationID: parentOrg.Metadata.ID,
	}

	projLifecycleTemplate := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			CreationTimestamp: &currentTime,
		},
		Scope:          models.ProjectScope,
		OrganizationID: parentOrg.Metadata.ID,
		ProjectID:      &parentProj.Metadata.ID,
	}

	type testCase struct {
		injectPermissionError error
		injectCreateError     error
		limitError            error
		input                 *CreateLifecycleTemplateInput
		organizationID        string
		projectID             *string
		expectCreated         *models.LifecycleTemplate
		name                  string
		expectErrorCode       errors.CodeType
		injectCount           int32
		withCaller            bool
		exceedsLimit          bool
	}

	/*
		test case template

		type testCase struct {
			name                  string
			withCaller            bool
			input                 *CreateLifecycleTemplateInput
			organizationID        string
			projectID             *string
			injectPermissionError error
			injectCreateError     error
			injectCount           int32
			exceedsLimit          bool
			limitError            error
			expectErrorCode       errors.CodeType
			expectCreated         *models.LifecycleTemplate
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "org, permission check failed",
			withCaller: true,
			input: &CreateLifecycleTemplateInput{
				Scope:          models.OrganizationScope,
				OrganizationID: &parentOrg.Metadata.ID,
			},
			organizationID:        parentOrg.Metadata.ID,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:       "proj, permission check failed",
			withCaller: true,
			input: &CreateLifecycleTemplateInput{
				Scope:     models.ProjectScope,
				ProjectID: &parentProj.Metadata.ID,
			},
			organizationID:        parentOrg.Metadata.ID,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:       "creation failed due to unspecified internal problem",
			withCaller: true,
			input: &CreateLifecycleTemplateInput{
				Scope:          models.OrganizationScope,
				OrganizationID: &parentOrg.Metadata.ID,
			},
			organizationID:    parentOrg.Metadata.ID,
			injectCreateError: errors.New("injected error, creation failed"),
			expectErrorCode:   errors.EInternal,
		},
		{
			name:       "org, exceeds limit",
			withCaller: true,
			input: &CreateLifecycleTemplateInput{
				Scope:          models.OrganizationScope,
				OrganizationID: &parentOrg.Metadata.ID,
			},
			organizationID:  parentOrg.Metadata.ID,
			expectCreated:   projLifecycleTemplate,
			injectCount:     5,
			limitError:      errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			exceedsLimit:    true,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "proj, exceeds limit",
			withCaller: true,
			input: &CreateLifecycleTemplateInput{
				Scope:     models.ProjectScope,
				ProjectID: &parentProj.Metadata.ID,
			},
			organizationID:  parentOrg.Metadata.ID,
			projectID:       &parentProj.Metadata.ID,
			expectCreated:   orgLifecycleTemplate,
			injectCount:     5,
			limitError:      errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			exceedsLimit:    true,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "successfully create a new organization lifecycle template",
			withCaller: true,
			input: &CreateLifecycleTemplateInput{
				Scope:          models.OrganizationScope,
				OrganizationID: &parentOrg.Metadata.ID,
			},
			organizationID: parentOrg.Metadata.ID,
			injectCount:    4,
			exceedsLimit:   false,
			expectCreated:  orgLifecycleTemplate,
		},
		{
			name:       "successfully create a new project lifecycle template",
			withCaller: true,
			input: &CreateLifecycleTemplateInput{
				Scope:     models.ProjectScope,
				ProjectID: &parentProj.Metadata.ID,
			},
			organizationID: parentOrg.Metadata.ID,
			projectID:      &parentProj.Metadata.ID,
			injectCount:    4,
			exceedsLimit:   false,
			expectCreated:  projLifecycleTemplate,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)
			mockUsers := db.NewMockUsers(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
					Return(test.injectPermissionError)

				if test.input.Scope == models.ProjectScope {
					mockProjects.On("GetProjectByID", mock.Anything, *test.input.ProjectID).
						Return(&models.Project{
							OrgID: parentOrg.Metadata.ID,
						}, nil).Maybe()
				}

				if test.injectPermissionError == nil {
					mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
					mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

					mockLifecycleTemplates.On("CreateLifecycleTemplate", mock.Anything, mock.Anything).
						Return(test.expectCreated, test.injectCreateError)

					if test.injectCreateError == nil {
						mockLifecycleTemplates.On("GetLifecycleTemplates", mock.Anything, mock.Anything).
							Return(&db.LifecycleTemplatesResult{
								PageInfo: &pagination.PageInfo{
									TotalCount: test.injectCount,
								},
							}, nil)

						checkLimit := limits.ResourceLimitLifecycleTemplatesPerOrganizationPerTimePeriod
						if test.projectID != nil {
							checkLimit = limits.ResourceLimitLifecycleTemplatesPerProjectPerTimePeriod
						}
						mockLimitChecker.On("CheckLimit", mock.Anything, checkLimit, test.injectCount).
							Return(test.limitError)

						if !test.exceedsLimit {
							mockCaller.On("GetSubject").Return("mock-caller-subject")

							mockActivityEvents.On("CreateActivityEvent", mock.Anything,
								&activityevent.CreateActivityEventInput{
									OrganizationID: &test.organizationID,
									ProjectID:      test.projectID,
									Action:         models.ActionCreate,
									TargetType:     models.TargetLifecycleTemplate,
									TargetID:       &test.expectCreated.Metadata.ID,
								},
							).Return(&models.ActivityEvent{}, nil)

							mockTransactions.On("CommitTx", mock.Anything).Return(nil)
						}
					}
				}
			}

			dbClient := &db.Client{
				Users:              mockUsers,
				Projects:           mockProjects,
				LifecycleTemplates: mockLifecycleTemplates,
				Transactions:       mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
			}

			actualCreated, err := service.CreateLifecycleTemplate(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreated, actualCreated)
		})
	}
}

func TestUploadLifecycleTemplate(t *testing.T) {
	lifecycleTemplateID := "lifecycle-template-1"
	organizationID := "organization-1"
	projectID := "project-1"
	sampleReader := strings.NewReader("lifecycle-data")

	type testCase struct {
		name                      string
		existingLifecycleTemplate *models.LifecycleTemplate
		authError                 error
		expectErrorCode           errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully upload org lifecycle template",
			existingLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID: lifecycleTemplateID,
				},
				Scope:          models.OrganizationScope,
				OrganizationID: organizationID,
				Status:         models.LifecycleTemplatePending,
			},
		},
		{
			name: "successfully upload proj lifecycle template",
			existingLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID: lifecycleTemplateID,
				},
				Scope:          models.OrganizationScope,
				OrganizationID: organizationID,
				ProjectID:      &projectID,
				Status:         models.LifecycleTemplatePending,
			},
		},
		{
			name: "cannot upload lifecycle template since it was already uploaded",
			existingLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID: lifecycleTemplateID,
				},
				Scope:          models.OrganizationScope,
				OrganizationID: organizationID,
				Status:         models.LifecycleTemplateUploaded,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "lifecycle template not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to upload lifecycle template",
			existingLifecycleTemplate: &models.LifecycleTemplate{
				Metadata: models.ResourceMetadata{
					ID: lifecycleTemplateID,
				},
				Scope:          models.OrganizationScope,
				OrganizationID: organizationID,
				Status:         models.LifecycleTemplatePending,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockDataStore := NewMockDataStore(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)

			mockLifecycleTemplates.On("GetLifecycleTemplateByID", mock.Anything, lifecycleTemplateID).Return(test.existingLifecycleTemplate, nil)

			if test.existingLifecycleTemplate != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.CreateLifecycleTemplate, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockLifecycleTemplates.On("UpdateLifecycleTemplate", mock.Anything, mock.Anything).Return(test.existingLifecycleTemplate, nil)

				mockDataStore.On("UploadData", mock.Anything, lifecycleTemplateID, sampleReader).Return(nil)

				mockCaller.On("GetSubject").Return("testSubject")
			}

			dbClient := &db.Client{
				Transactions:       mockTransactions,
				LifecycleTemplates: mockLifecycleTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:    logger,
				dbClient:  dbClient,
				dataStore: mockDataStore,
			}

			err := service.UploadLifecycleTemplate(auth.WithCaller(ctx, mockCaller), &UploadLifecycleTemplateInput{
				Reader: sampleReader,
				ID:     lifecycleTemplateID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
