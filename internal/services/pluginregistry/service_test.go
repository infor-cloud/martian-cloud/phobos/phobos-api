package pluginregistry

import (
	"bytes"
	context "context"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockRegistryStore := NewMockRegistryStore(t)
	mockLimitChecker := limits.NewMockLimitChecker(t)
	mockActivityEvents := activityevent.NewMockService(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		registryStore:   mockRegistryStore,
		limitChecker:    mockLimitChecker,
		activityService: mockActivityEvents,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, mockRegistryStore, mockLimitChecker, mockActivityEvents))
}

func TestGetPluginByID(t *testing.T) {
	pluginID := "pluginID"
	organizationID := "orgID"

	type testCase struct {
		name            string
		plugin          *models.Plugin
		authError       error
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "get a public plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
			},
		},
		{
			name: "get a private plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
				Private:        true,
			},
		},
		{
			name: "subject is not authorized to view private plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
				Private:        true,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPlugins := db.NewMockPlugins(t)
			mockCaller := auth.NewMockCaller(t)

			mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(test.plugin, nil)

			if test.plugin != nil && test.plugin.Private {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Plugins: mockPlugins,
			}

			service := &service{
				dbClient: dbClient,
			}

			plugin, err := service.GetPluginByID(auth.WithCaller(ctx, mockCaller), pluginID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.plugin, plugin)
		})
	}
}

func TestGetPluginByPRN(t *testing.T) {
	pluginID := "pluginID"
	pluginPRN := "org/plugin"
	organizationID := "orgID"

	type testCase struct {
		name            string
		plugin          *models.Plugin
		authError       error
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "get a public plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID:  pluginID,
					PRN: pluginPRN,
				},
				OrganizationID: organizationID,
			},
		},
		{
			name: "get a private plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID:  pluginID,
					PRN: pluginPRN,
				},
				OrganizationID: organizationID,
				Private:        true,
			},
		},
		{
			name: "subject is not authorized to view private plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID:  pluginID,
					PRN: pluginPRN,
				},
				OrganizationID: organizationID,
				Private:        true,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPlugins := db.NewMockPlugins(t)
			mockCaller := auth.NewMockCaller(t)

			mockPlugins.On("GetPluginByPRN", mock.Anything, pluginPRN).Return(test.plugin, nil)

			if test.plugin != nil && test.plugin.Private {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Plugins: mockPlugins,
			}

			service := &service{
				dbClient: dbClient,
			}

			plugin, err := service.GetPluginByPRN(auth.WithCaller(ctx, mockCaller), pluginPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.plugin, plugin)
		})
	}
}

func TestGetPlugins(t *testing.T) {
	organizationID := "orgID"
	userID := "userID"
	serviceAccountID := "serviceAccountID"
	userCallerType := "user"
	serviceAccountCallerType := "service-account"

	type testCase struct {
		authError       error
		organizationID  *string
		name            string
		callerType      string
		expectErrorCode errors.CodeType
		isAdmin         bool
	}

	tests := []testCase{
		{
			name:       "non admin gets all plugins they have access to",
			callerType: userCallerType,
		},
		{
			name:           "non admin gets all plugins by organization",
			organizationID: &organizationID,
			callerType:     userCallerType,
		},
		{
			name:            "user is not authorized to view organization",
			organizationID:  &organizationID,
			callerType:      userCallerType,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "admin gets all plugins",
			callerType: userCallerType,
			isAdmin:    true,
		},
		{
			name:           "admin gets all plugins by organization",
			organizationID: &organizationID,
			callerType:     userCallerType,
			isAdmin:        true,
		},
		{
			name:       "service account gets all plugins they have access to",
			callerType: serviceAccountCallerType,
		},
		{
			name:           "service account gets all plugins by organization",
			organizationID: &organizationID,
			callerType:     serviceAccountCallerType,
		},
		{
			name:            "service account is not authorized to view organization",
			organizationID:  &organizationID,
			callerType:      serviceAccountCallerType,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPlugins := db.NewMockPlugins(t)
			mockAuthorizer := auth.NewMockAuthorizer(t)

			mockAuthorizer.On("RequireAccessToInheritableResource", mock.Anything, []models.ResourceType{models.PluginResource}, mock.Anything).Return(test.authError).Maybe()

			if test.authError == nil {
				dbInput := &db.GetPluginsInput{
					Filter: &db.PluginFilter{
						OrganizationID: test.organizationID,
					},
				}

				// Add user or service account ID to the filter if the caller is not an admin
				// and they're not filtering by organization.
				if !test.isAdmin && test.organizationID == nil {
					switch test.callerType {
					case userCallerType:
						dbInput.Filter.UserID = &userID
					case serviceAccountCallerType:
						dbInput.Filter.ServiceAccountID = &serviceAccountID
					}
				}

				mockPlugins.On("GetPlugins", mock.Anything, dbInput).Return(&db.PluginsResult{}, nil)
			}

			// Differentiate between user and service account callers to ensure filters are applied correctly.
			var caller auth.Caller
			switch test.callerType {
			case userCallerType:
				caller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userID,
						},
						Admin: test.isAdmin,
					},
					mockAuthorizer,
					nil,
				)
			case serviceAccountCallerType:
				caller = auth.NewServiceAccountCaller(serviceAccountID, models.ServiceAccountResource.BuildPRN("some-org", "some-sa"), mockAuthorizer, nil)
			default:
				t.Fatalf("unexpected caller type: %s", test.callerType)
			}

			dbClient := &db.Client{
				Plugins: mockPlugins,
			}

			service := &service{
				dbClient: dbClient,
			}

			plugins, err := service.GetPlugins(auth.WithCaller(ctx, caller), &GetPluginsInput{
				OrganizationID: test.organizationID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, plugins)
		})
	}
}

func TestGetPluginsByIDs(t *testing.T) {
	pluginIDs := []string{"pluginID1", "pluginID2"}
	organizationID := "orgID"

	type testCase struct {
		name            string
		authError       error
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "get plugins by IDs",
		},
		{
			name:            "subject is not authorized to view plugin",
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPlugins := db.NewMockPlugins(t)
			mockCaller := auth.NewMockCaller(t)

			mockPlugins.On("GetPlugins", mock.Anything, &db.GetPluginsInput{
				Filter: &db.PluginFilter{
					PluginIDs: pluginIDs,
				},
			}).Return(func(_ context.Context, _ *db.GetPluginsInput) (*db.PluginsResult, error) {
				plugins := make([]models.Plugin, len(pluginIDs))
				for i, pluginID := range pluginIDs {
					plugins[i] = models.Plugin{
						Metadata: models.ResourceMetadata{
							ID: pluginID,
						},
						OrganizationID: organizationID,
						Private:        i%2 == 0, // Every other plugin is private
					}
				}

				return &db.PluginsResult{
					Plugins: plugins,
				}, nil
			})

			// Require access to only private plugins (every other plugin is private).
			mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError).Times(len(pluginIDs) / 2)

			dbClient := &db.Client{
				Plugins: mockPlugins,
			}

			service := &service{
				dbClient: dbClient,
			}

			plugins, err := service.GetPluginsByIDs(auth.WithCaller(ctx, mockCaller), pluginIDs)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Len(t, plugins, len(pluginIDs))
		})
	}
}

func TestGetPluginVersionByID(t *testing.T) {
	pluginID := "pluginID"

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: "pluginVersionID",
		},
		PluginID: pluginID,
	}

	type testCase struct {
		name            string
		pluginVersion   *models.PluginVersion
		authError       error
		expectErrorCode errors.CodeType
		privatePlugin   bool
	}

	tests := []testCase{
		{
			name:          "get a public plugin version",
			pluginVersion: pluginVersion,
		},
		{
			name:          "get a private plugin version",
			pluginVersion: pluginVersion,
			privatePlugin: true,
		},
		{
			name:            "subject is not authorized to view private plugin",
			pluginVersion:   pluginVersion,
			privatePlugin:   true,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin version not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersion.Metadata.ID).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(&models.Plugin{
					Metadata: models.ResourceMetadata{
						ID: pluginID,
					},
					OrganizationID: "orgID",
					Private:        test.privatePlugin,
				}, nil)
			}

			if test.privatePlugin {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPluginVersion, err := service.GetPluginVersionByID(auth.WithCaller(ctx, mockCaller), pluginVersion.Metadata.ID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pluginVersion, actualPluginVersion)
		})
	}
}

func TestGetPluginVersionByPRN(t *testing.T) {
	pluginID := "pluginID"
	pluginVersionPRN := "org/plugin/version"

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID:  "pluginVersionID",
			PRN: pluginVersionPRN,
		},
		PluginID: pluginID,
	}

	type testCase struct {
		name            string
		pluginVersion   *models.PluginVersion
		authError       error
		expectErrorCode errors.CodeType
		privatePlugin   bool
	}

	tests := []testCase{
		{
			name:          "get a public plugin's version",
			pluginVersion: pluginVersion,
		},
		{
			name:          "get a private plugin's version",
			pluginVersion: pluginVersion,
			privatePlugin: true,
		},
		{
			name:            "subject is not authorized to view private plugin",
			pluginVersion:   pluginVersion,
			privatePlugin:   true,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin version not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)

			mockPluginVersions.On("GetPluginVersionByPRN", mock.Anything, pluginVersionPRN).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(&models.Plugin{
					Metadata: models.ResourceMetadata{
						ID: pluginID,
					},
					OrganizationID: "orgID",
					Private:        test.privatePlugin,
				}, nil)
			}

			if test.privatePlugin {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPluginVersion, err := service.GetPluginVersionByPRN(auth.WithCaller(ctx, mockCaller), pluginVersionPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pluginVersion, actualPluginVersion)
		})
	}
}

func TestGetPluginVersions(t *testing.T) {
	pluginID := "pluginID"
	organizationID := "orgID"

	type testCase struct {
		authError       error
		plugin          *models.Plugin
		name            string
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "get public plugin versions",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
			},
		},
		{
			name: "get private plugin versions",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
				Private:        true,
			},
		},
		{
			name: "subject is not authorized to view private plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
				Private:        true,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)

			mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(test.plugin, nil)

			if test.plugin != nil && test.plugin.Private {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockPluginVersions.On("GetPluginVersions", mock.Anything, &db.GetPluginVersionsInput{
					Filter: &db.PluginVersionFilter{
						PluginID: &pluginID,
					},
				}).Return(&db.PluginVersionsResult{}, nil)
			}

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
			}

			service := &service{
				dbClient: dbClient,
			}

			pluginVersions, err := service.GetPluginVersions(auth.WithCaller(ctx, mockCaller), &GetPluginVersionsInput{
				PluginID: pluginID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, pluginVersions)
		})
	}
}

func TestGetPluginVersionsByIDs(t *testing.T) {
	pluginID := "pluginID"
	pluginVersionIDs := []string{"pluginVersionID1"}

	type testCase struct {
		name            string
		authError       error
		expectErrorCode errors.CodeType
		privatePlugin   bool
	}

	tests := []testCase{
		{
			name: "get plugin versions by IDs",
		},
		{
			name:            "subject is not authorized to view plugin",
			privatePlugin:   true,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)

			mockPluginVersions.On("GetPluginVersions", mock.Anything, &db.GetPluginVersionsInput{
				Filter: &db.PluginVersionFilter{
					PluginVersionIDs: pluginVersionIDs,
				},
			}).Return(func(_ context.Context, _ *db.GetPluginVersionsInput) (*db.PluginVersionsResult, error) {
				pluginVersions := make([]models.PluginVersion, len(pluginVersionIDs))
				for i, pluginVersionID := range pluginVersionIDs {
					pluginVersions[i] = models.PluginVersion{
						Metadata: models.ResourceMetadata{
							ID: pluginVersionID,
						},
						PluginID: pluginID,
					}
				}

				return &db.PluginVersionsResult{
					PluginVersions: pluginVersions,
				}, nil
			})

			mockPlugins.On("GetPlugins", mock.Anything, &db.GetPluginsInput{
				Filter: &db.PluginFilter{
					PluginIDs: []string{pluginID},
				},
			}).Return(&db.PluginsResult{
				Plugins: []models.Plugin{
					{
						Metadata: models.ResourceMetadata{
							ID: pluginID,
						},
						OrganizationID: "orgID",
						Private:        test.privatePlugin,
					},
				},
			}, nil).Times(len(pluginVersionIDs))

			if test.privatePlugin {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError).Times(len(pluginVersionIDs))
			}

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
			}

			service := &service{
				dbClient: dbClient,
			}

			pluginVersions, err := service.GetPluginVersionsByIDs(auth.WithCaller(ctx, mockCaller), pluginVersionIDs)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Len(t, pluginVersions, len(pluginVersionIDs))
		})
	}
}

func TestGetPluginVersionReadme(t *testing.T) {
	readme := "readme"
	pluginID := "pluginID"
	orgID := "orgID"

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: "pluginVersionID",
		},
		PluginID: pluginID,
	}

	type testCase struct {
		authError       error
		plugin          *models.Plugin
		name            string
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "get public plugin version readme",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: orgID,
			},
		},
		{
			name: "get private plugin version readme",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: orgID,
				Private:        true,
			},
		},
		{
			name: "subject is not authorized to view private plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: orgID,
				Private:        true,
			},
		},
		{
			name:            "plugin not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockRegistryStore := NewMockRegistryStore(t)

			mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(test.plugin, nil)

			if test.plugin != nil && test.plugin.Private {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockRegistryStore.On("GetPluginVersionReadme", mock.Anything, pluginVersion, test.plugin).Return(io.NopCloser(strings.NewReader(readme)), nil)
			}

			dbClient := &db.Client{
				Plugins: mockPlugins,
			}

			service := &service{
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			actualReadme, err := service.GetPluginVersionReadme(auth.WithCaller(ctx, mockCaller), pluginVersion)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, readme, actualReadme)
		})
	}
}

func TestGetPluginVersionDocFile(t *testing.T) {
	docFileContents := "my plugin doc"
	docFilePath := "index.md"
	pluginID := "pluginID"
	orgID := "orgID"

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: "pluginVersionID",
		},
		PluginID: pluginID,
		DocFiles: map[string]*models.PluginVersionDocFileMetadata{
			docFilePath: {
				FilePath: docFilePath,
			},
		},
	}

	type testCase struct {
		authError       error
		plugin          *models.Plugin
		name            string
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "get public plugin version doc file",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: orgID,
			},
		},
		{
			name: "get private plugin version doc file",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: orgID,
				Private:        true,
			},
		},
		{
			name: "subject is not authorized to view private plugin",
			plugin: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: orgID,
				Private:        true,
			},
		},
		{
			name:            "plugin not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockRegistryStore := NewMockRegistryStore(t)

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersion.Metadata.ID).Return(pluginVersion, nil)
			mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(test.plugin, nil)

			if test.plugin != nil && test.plugin.Private {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockRegistryStore.On("GetPluginVersionDocFile", mock.Anything, pluginVersion, test.plugin, docFilePath).Return(io.NopCloser(strings.NewReader(docFileContents)), nil)
			}

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
			}

			service := &service{
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			fileContents, err := service.GetPluginVersionDocFile(auth.WithCaller(ctx, mockCaller), &GetPluginVersionDocFileInput{
				PluginVersionID: pluginVersion.Metadata.ID,
				FilePath:        docFilePath,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, docFileContents, fileContents)
		})
	}
}

func TestGetPluginPlatformByID(t *testing.T) {
	pluginID := "pluginID"
	pluginVersionID := "pluginVersionID"
	platformID := "platformID"

	pluginPlatform := &models.PluginPlatform{
		Metadata: models.ResourceMetadata{
			ID: platformID,
		},
		PluginVersionID: pluginVersionID,
	}

	type testCase struct {
		name            string
		pluginPlatform  *models.PluginPlatform
		authError       error
		expectErrorCode errors.CodeType
		privatePlugin   bool
	}

	tests := []testCase{
		{
			name:           "get a public plugin platform",
			pluginPlatform: pluginPlatform,
		},
		{
			name:           "get a private plugin platform",
			pluginPlatform: pluginPlatform,
			privatePlugin:  true,
		},
		{
			name:            "subject is not authorized to view private plugin",
			pluginPlatform:  pluginPlatform,
			privatePlugin:   true,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin platform not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockPluginPlatforms := db.NewMockPluginPlatforms(t)

			mockPluginPlatforms.On("GetPluginPlatformByID", mock.Anything, platformID).Return(test.pluginPlatform, nil)

			if test.pluginPlatform != nil {
				mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersionID).Return(&models.PluginVersion{
					Metadata: models.ResourceMetadata{
						ID: pluginVersionID,
					},
					PluginID: pluginID,
				}, nil)

				mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(&models.Plugin{
					Metadata: models.ResourceMetadata{
						ID: pluginID,
					},
					OrganizationID: "orgID",
					Private:        test.privatePlugin,
				}, nil)

				if test.privatePlugin {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				Plugins:         mockPlugins,
				PluginVersions:  mockPluginVersions,
				PluginPlatforms: mockPluginPlatforms,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPluginPlatform, err := service.GetPluginPlatformByID(auth.WithCaller(ctx, mockCaller), platformID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pluginPlatform, actualPluginPlatform)
		})
	}
}

func TestGetPluginPlatformByPRN(t *testing.T) {
	pluginID := "pluginID"
	pluginVersionID := "pluginVersionID"
	platformPRN := "org/plugin/version/platform"

	pluginPlatform := &models.PluginPlatform{
		Metadata: models.ResourceMetadata{
			ID:  "platformID",
			PRN: platformPRN,
		},
		PluginVersionID: pluginVersionID,
	}

	type testCase struct {
		name            string
		pluginPlatform  *models.PluginPlatform
		authError       error
		expectErrorCode errors.CodeType
		privatePlugin   bool
	}

	tests := []testCase{
		{
			name:           "get a public plugin platform",
			pluginPlatform: pluginPlatform,
		},
		{
			name:           "get a private plugin platform",
			pluginPlatform: pluginPlatform,
			privatePlugin:  true,
		},
		{
			name:            "subject is not authorized to view private plugin",
			pluginPlatform:  pluginPlatform,
			privatePlugin:   true,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin platform not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockPluginPlatforms := db.NewMockPluginPlatforms(t)

			mockPluginPlatforms.On("GetPluginPlatformByPRN", mock.Anything, platformPRN).Return(test.pluginPlatform, nil)

			if test.pluginPlatform != nil {
				mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersionID).Return(&models.PluginVersion{
					Metadata: models.ResourceMetadata{
						ID: pluginVersionID,
					},
					PluginID: pluginID,
				}, nil)

				mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(&models.Plugin{
					Metadata: models.ResourceMetadata{
						ID: pluginID,
					},
					OrganizationID: "orgID",
					Private:        test.privatePlugin,
				}, nil)

				if test.privatePlugin {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				Plugins:         mockPlugins,
				PluginVersions:  mockPluginVersions,
				PluginPlatforms: mockPluginPlatforms,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPluginPlatform, err := service.GetPluginPlatformByPRN(auth.WithCaller(ctx, mockCaller), platformPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pluginPlatform, actualPluginPlatform)
		})
	}
}

func TestGetPluginPlatforms(t *testing.T) {
	pluginID := "pluginID"
	orgID := "orgID"

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: "pluginVersionID",
		},
		PluginID: pluginID,
	}

	type testCase struct {
		authError       error
		pluginVersion   *models.PluginVersion
		plugin          *models.Plugin
		input           *GetPluginPlatformsInput
		name            string
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name:          "get public plugin platforms by plugin version ID",
			pluginVersion: pluginVersion,
			plugin:        &models.Plugin{Metadata: models.ResourceMetadata{ID: pluginID}, OrganizationID: orgID},
			input:         &GetPluginPlatformsInput{PluginVersionID: &pluginVersion.Metadata.ID},
		},
		{
			name:   "get public plugin platforms by plugin ID",
			plugin: &models.Plugin{Metadata: models.ResourceMetadata{ID: pluginID}, OrganizationID: orgID},
			input:  &GetPluginPlatformsInput{PluginID: &pluginID},
		},
		{
			name:          "get private plugin platforms by plugin version ID",
			pluginVersion: pluginVersion,
			plugin:        &models.Plugin{Metadata: models.ResourceMetadata{ID: pluginID}, OrganizationID: orgID, Private: true},
			input:         &GetPluginPlatformsInput{PluginVersionID: &pluginVersion.Metadata.ID},
		},
		{
			name:   "get private plugin platforms by plugin ID",
			plugin: &models.Plugin{Metadata: models.ResourceMetadata{ID: pluginID}, OrganizationID: orgID, Private: true},
			input:  &GetPluginPlatformsInput{PluginID: &pluginID},
		},
		{
			name:            "subject is not authorized to view private plugin",
			pluginVersion:   pluginVersion,
			plugin:          &models.Plugin{Metadata: models.ResourceMetadata{ID: pluginID}, OrganizationID: orgID, Private: true},
			input:           &GetPluginPlatformsInput{PluginVersionID: &pluginVersion.Metadata.ID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin not found",
			input:           &GetPluginPlatformsInput{PluginID: &pluginID},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "plugin version not found",
			input:           &GetPluginPlatformsInput{PluginVersionID: &pluginVersion.Metadata.ID},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "plugin version ID and plugin ID are both nil",
			input:           &GetPluginPlatformsInput{},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:          "plugin version ID and plugin ID are both set",
			input:         &GetPluginPlatformsInput{PluginID: &pluginID, PluginVersionID: &pluginVersion.Metadata.ID},
			plugin:        &models.Plugin{Metadata: models.ResourceMetadata{ID: pluginID}, OrganizationID: orgID},
			pluginVersion: pluginVersion,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockPluginPlatforms := db.NewMockPluginPlatforms(t)

			if test.input.PluginVersionID != nil {
				mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersion.Metadata.ID).Return(test.pluginVersion, nil)
			}

			if test.input.PluginID != nil || test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(test.plugin, nil)
			}

			if test.plugin != nil && test.plugin.Private {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockPluginPlatforms.On("GetPluginPlatforms", mock.Anything, &db.GetPluginPlatformsInput{
					Filter: &db.PluginPlatformFilter{
						PluginID:        test.input.PluginID,
						PluginVersionID: test.input.PluginVersionID,
					},
				}).Return(&db.PluginPlatformsResult{}, nil)
			}

			dbClient := &db.Client{
				Plugins:         mockPlugins,
				PluginVersions:  mockPluginVersions,
				PluginPlatforms: mockPluginPlatforms,
			}

			service := &service{
				dbClient: dbClient,
			}

			pluginPlatforms, err := service.GetPluginPlatforms(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, pluginPlatforms)
		})
	}
}

func TestGetPluginPlatformDownloadURLs(t *testing.T) {
	downloadURL := "http://object-store.tld/download"
	shaSumsURL := "http://object-store.tld/checksum"
	pluginID := "pluginID"

	pluginPlatform := &models.PluginPlatform{
		Metadata: models.ResourceMetadata{
			ID: "platformID",
		},
		PluginVersionID: "pluginVersionID",
	}

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: pluginPlatform.PluginVersionID,
		},
		PluginID: "pluginID",
	}

	type testCase struct {
		name            string
		authError       error
		pluginPlatform  *models.PluginPlatform
		expectErrorCode errors.CodeType
		privatePlugin   bool
	}

	tests := []testCase{
		{
			name:           "get plugin platform download URLs",
			pluginPlatform: pluginPlatform,
		},
		{
			name:            "subject is not authorized to view plugin",
			pluginPlatform:  pluginPlatform,
			privatePlugin:   true,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin platform not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockRegistryStore := NewMockRegistryStore(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockPluginPlatforms := db.NewMockPluginPlatforms(t)

			mockPluginPlatforms.On("GetPluginPlatformByID", mock.Anything, pluginPlatform.Metadata.ID).Return(test.pluginPlatform, nil)

			plugin := &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: "pluginID",
				},
				OrganizationID: "orgID",
				Private:        test.privatePlugin,
			}

			if test.pluginPlatform != nil {
				mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginPlatform.PluginVersionID).Return(pluginVersion, nil)

				mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(plugin, nil)

				if test.privatePlugin {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.PluginResource, mock.Anything).Return(test.authError)
				}
			}

			if test.expectErrorCode == "" {
				mockRegistryStore.On("GetPluginPlatformBinaryPresignedURL", mock.Anything, pluginPlatform, pluginVersion, plugin).Return(downloadURL, nil)
				mockRegistryStore.On("GetPluginVersionSHASumsPresignedURL", mock.Anything, pluginVersion, plugin).Return(shaSumsURL, nil)
			}

			dbClient := &db.Client{
				Plugins:         mockPlugins,
				PluginVersions:  mockPluginVersions,
				PluginPlatforms: mockPluginPlatforms,
			}

			service := &service{
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			actualDownloadURLs, err := service.GetPluginPlatformDownloadURLs(auth.WithCaller(ctx, mockCaller), pluginPlatform.Metadata.ID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, actualDownloadURLs)
			assert.Equal(t, downloadURL, actualDownloadURLs.DownloadURL)
			assert.Equal(t, shaSumsURL, actualDownloadURLs.SHASumsURL)
		})
	}
}

func TestCreatePlugin(t *testing.T) {
	pluginID := "pluginID"
	organizationID := "orgID"
	testSubject := "testSubject"

	type testCase struct {
		authError       error
		limitError      error
		input           *CreatePluginInput
		expectCreated   *models.Plugin
		name            string
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "create plugin",
			input: &CreatePluginInput{
				OrganizationID: organizationID,
				Name:           "gitlab",
				RepositoryURL:  "https://gitlab.com/gitlab-org/gitlab",
				Private:        true,
			},
			expectCreated: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
				Name:           "gitlab",
				RepositoryURL:  "https://gitlab.com/gitlab-org/gitlab",
				CreatedBy:      testSubject,
				Private:        true,
			},
		},
		{
			name:            "subject is not authorized to create plugin",
			input:           &CreatePluginInput{OrganizationID: organizationID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "invalid plugin name",
			input: &CreatePluginInput{
				OrganizationID: organizationID,
				Name:           "invalid name",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "invalid repository URL",
			input: &CreatePluginInput{
				OrganizationID: organizationID,
				Name:           "gitlab",
				RepositoryURL:  "invalid url",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "limit exceeded",
			input: &CreatePluginInput{
				OrganizationID: organizationID,
				Name:           "gitlab",
			},
			expectCreated: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: pluginID,
				},
				OrganizationID: organizationID,
				Name:           "gitlab",
				CreatedBy:      testSubject,
			},
			limitError:      errors.New("Limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockLimits := limits.NewMockLimitChecker(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.CreatePlugin, mock.Anything).Return(test.authError)

			if test.expectCreated != nil {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

				mockPlugins.On("CreatePlugin", mock.Anything, mock.Anything).Return(test.expectCreated, nil)

				mockPlugins.On("GetPlugins", mock.Anything, &db.GetPluginsInput{
					Filter: &db.PluginFilter{
						OrganizationID: &organizationID,
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.PluginsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil)

				mockLimits.On("CheckLimit", mock.Anything, limits.ResourceLimitPluginsPerOrganization, int32(1)).Return(test.limitError)
			}

			if test.expectErrorCode == "" {
				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					Action:         models.ActionCreate,
					TargetType:     models.TargetPlugin,
					OrganizationID: &organizationID,
					TargetID:       &pluginID,
				}).Return(&models.ActivityEvent{}, nil)

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:      mockPlugins,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimits,
				activityService: mockActivityEvents,
			}

			created, err := service.CreatePlugin(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectCreated, created)
		})
	}
}

func TestCreatePluginVersion(t *testing.T) {
	pluginVersionID := "pluginVersionID"
	previousLatestVersionID := "previousLatestVersionID"
	testSubject := "testSubject"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		OrganizationID: "orgID",
	}

	type testCase struct {
		authError             error
		limitError            error
		plugin                *models.Plugin
		input                 *CreatePluginVersionInput
		expectCreated         *models.PluginVersion
		previousLatestVersion *models.PluginVersion
		name                  string
		expectErrorCode       errors.CodeType
	}

	tests := []testCase{
		{
			name:   "creating a plugin version greater than previous should make this version the latest",
			plugin: plugin,
			input: &CreatePluginVersionInput{
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "1.0.0",
				Protocols:       []string{"1.0"},
			},
			expectCreated: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "1.0.0",
				Protocols:       []string{"1.0"},
				CreatedBy:       testSubject,
				Latest:          true,
			},
			previousLatestVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: previousLatestVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "0.1.0",
				Protocols:       []string{"1.0"},
				Latest:          true,
			},
		},
		{
			name:   "creating a plugin version less than previous should not make this version the latest",
			plugin: plugin,
			input: &CreatePluginVersionInput{
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "0.1.0",
				Protocols:       []string{"1.0"},
			},
			expectCreated: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "0.1.0",
				Protocols:       []string{"1.0"},
				CreatedBy:       testSubject,
			},
			previousLatestVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: previousLatestVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "1.0.0",
				Protocols:       []string{"1.0"},
				Latest:          true,
			},
		},
		{
			name:   "creating a plugin version with no previous versions should make this version the latest",
			plugin: plugin,
			input: &CreatePluginVersionInput{
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "1.0.0",
				Protocols:       []string{"1.0"},
			},
			expectCreated: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "1.0.0",
				Protocols:       []string{"1.0"},
				CreatedBy:       testSubject,
				Latest:          true,
			},
		},
		{
			name:            "subject is not authorized to create plugin version",
			plugin:          plugin,
			input:           &CreatePluginVersionInput{PluginID: plugin.Metadata.ID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin not found",
			input:           &CreatePluginVersionInput{PluginID: plugin.Metadata.ID},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "invalid semantic version",
			plugin:          plugin,
			input:           &CreatePluginVersionInput{PluginID: plugin.Metadata.ID, SemanticVersion: "invalid"},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:   "invalid protocols",
			plugin: plugin,
			input: &CreatePluginVersionInput{
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "1.0.0",
				Protocols:       []string{"invalid"},
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockLimits := limits.NewMockLimitChecker(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(test.plugin, nil)

			if test.plugin != nil {
				mockCaller.On("GetSubject").Return(testSubject).Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)

				mockPluginVersions.On("GetPluginVersions", mock.Anything, &db.GetPluginVersionsInput{
					Filter: &db.PluginVersionFilter{
						PluginID: &plugin.Metadata.ID,
						Latest:   ptr.Bool(true),
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(1),
					},
				}).Return(func(_ context.Context, _ *db.GetPluginVersionsInput) (*db.PluginVersionsResult, error) {
					result := &db.PluginVersionsResult{
						PluginVersions: []models.PluginVersion{},
						PageInfo: &pagination.PageInfo{
							TotalCount: 0,
						},
					}
					if test.previousLatestVersion != nil {
						result.PluginVersions = append(result.PluginVersions, *test.previousLatestVersion)
						result.PageInfo.TotalCount = 1
					}

					return result, nil
				}).Maybe()

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

				if test.previousLatestVersion != nil && test.expectCreated != nil && test.expectCreated.Latest {
					mockPluginVersions.On("UpdatePluginVersion", mock.Anything, mock.Anything).Return(
						func(_ context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
							if test.expectCreated != nil && test.expectCreated.Latest {
								// Ensure that the previous latest version is no longer the latest
								require.False(t, pluginVersion.Latest)
							}
							return pluginVersion, nil
						},
					)
				}
			}

			if test.expectCreated != nil {
				mockPluginVersions.On("CreatePluginVersion", mock.Anything, mock.Anything).Return(test.expectCreated, nil)
				mockPluginVersions.On("GetPluginVersions", mock.Anything, &db.GetPluginVersionsInput{
					Filter: &db.PluginVersionFilter{
						PluginID: &plugin.Metadata.ID,
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(func(_ context.Context, _ *db.GetPluginVersionsInput) (*db.PluginVersionsResult, error) {
					return &db.PluginVersionsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
					}, nil
				})

				mockLimits.On("CheckLimit", mock.Anything, limits.ResourceLimitVersionsPerPlugin, int32(1)).Return(test.limitError)
			}

			if test.expectErrorCode == "" {
				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					Action:         models.ActionCreate,
					TargetType:     models.TargetPluginVersion,
					OrganizationID: &plugin.OrganizationID,
					TargetID:       &pluginVersionID,
				}).Return(&models.ActivityEvent{}, nil)

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
				Transactions:   mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimits,
				activityService: mockActivityEvents,
			}

			created, err := service.CreatePluginVersion(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectCreated, created)
		})
	}
}

func TestCreatePluginPlatform(t *testing.T) {
	pluginPlatformID := "pluginPlatformID"
	testSubject := "testSubject"

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: "pluginVersionID",
		},
		PluginID: "pluginID",
	}

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		OrganizationID: "orgID",
	}

	type testCase struct {
		authError       error
		pluginVersion   *models.PluginVersion
		expectCreated   *models.PluginPlatform
		name            string
		limitError      error
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name:          "create plugin platform",
			pluginVersion: pluginVersion,
			expectCreated: &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID: pluginPlatformID,
				},
				PluginVersionID: pluginVersion.Metadata.ID,
				CreatedBy:       testSubject,
			},
		},
		{
			name:            "subject is not authorized to create plugin platform",
			pluginVersion:   pluginVersion,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin version not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:          "limit exceeded",
			pluginVersion: pluginVersion,
			expectCreated: &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID: pluginPlatformID,
				},
				PluginVersionID: pluginVersion.Metadata.ID,
			},
			limitError:      errors.New("Limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockLimits := limits.NewMockLimitChecker(t)
			mockTransactions := db.NewMockTransactions(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockPluginPlatforms := db.NewMockPluginPlatforms(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersion.Metadata.ID).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, pluginVersion.PluginID).Return(plugin, nil)

				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)

				if test.authError == nil {
					mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
					mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

					mockPluginPlatforms.On("CreatePluginPlatform", mock.Anything, mock.Anything).Return(test.expectCreated, nil)

					mockPluginPlatforms.On("GetPluginPlatforms", mock.Anything, &db.GetPluginPlatformsInput{
						Filter: &db.PluginPlatformFilter{
							PluginVersionID: &pluginVersion.Metadata.ID,
						},
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(0),
						},
					}).Return(&db.PluginPlatformsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
					}, nil)

					mockLimits.On("CheckLimit", mock.Anything, limits.ResourceLimitPlatformsPerPluginVersion, int32(1)).Return(test.limitError)
				}
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:         mockPlugins,
				PluginVersions:  mockPluginVersions,
				PluginPlatforms: mockPluginPlatforms,
				Transactions:    mockTransactions,
			}

			service := &service{
				logger:       logger,
				dbClient:     dbClient,
				limitChecker: mockLimits,
			}

			created, err := service.CreatePluginPlatform(auth.WithCaller(ctx, mockCaller), &CreatePluginPlatformInput{
				PluginVersionID: pluginVersion.Metadata.ID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectCreated, created)
		})
	}
}

func TestUpdatePlugin(t *testing.T) {
	testSubject := "testSubject"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	type testCase struct {
		authError       error
		input           *UpdatePluginInput
		expectUpdated   *models.Plugin
		plugin          *models.Plugin
		name            string
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name:   "update plugin",
			plugin: plugin,
			input: &UpdatePluginInput{
				ID:            plugin.Metadata.ID,
				RepositoryURL: ptr.String("https://gitlab.com/gitlab-org/gitlab"),
				Private:       ptr.Bool(true),
			},
			expectUpdated: &models.Plugin{
				Metadata: models.ResourceMetadata{
					ID: "pluginID",
				},
				OrganizationID: "orgID",
				RepositoryURL:  "https://gitlab.com/gitlab-org/gitlab",
				Private:        true,
			},
		},
		{
			name:            "subject is not authorized to update plugin",
			plugin:          plugin,
			input:           &UpdatePluginInput{ID: plugin.Metadata.ID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin not found",
			input:           &UpdatePluginInput{ID: plugin.Metadata.ID},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "invalid repository URL",
			plugin:          plugin,
			input:           &UpdatePluginInput{ID: plugin.Metadata.ID, RepositoryURL: ptr.String("invalid url")},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(test.plugin, nil)

			if test.plugin != nil {
				mockCaller.On("GetSubject").Return(testSubject).Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPlugins.On("UpdatePlugin", mock.Anything, mock.Anything).Return(test.expectUpdated, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					Action:         models.ActionUpdate,
					TargetType:     models.TargetPlugin,
					OrganizationID: &plugin.OrganizationID,
					TargetID:       &plugin.Metadata.ID,
				}).Return(&models.ActivityEvent{}, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:      mockPlugins,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			updated, err := service.UpdatePlugin(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectUpdated, updated)
		})
	}
}

func TestUploadPluginPlatformBinary(t *testing.T) {
	binaryDataReader := bytes.NewReader([]byte("binary data"))
	pluginPlatformID := "pluginPlatformID"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: "pluginVersionID",
		},
		PluginID: plugin.Metadata.ID,
	}

	type testCase struct {
		name            string
		authError       error
		pluginPlatform  *models.PluginPlatform
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "upload plugin platform binary",
			pluginPlatform: &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID: pluginPlatformID,
				},
				PluginVersionID: pluginVersion.Metadata.ID,
			},
		},
		{
			name: "subject is not authorized to upload plugin platform binary",
			pluginPlatform: &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID: pluginPlatformID,
				},
				PluginVersionID: pluginVersion.Metadata.ID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin platform not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "platform binary already uploaded",
			pluginPlatform: &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID: pluginPlatformID,
				},
				PluginVersionID: pluginVersion.Metadata.ID,
				BinaryUploaded:  true,
			},
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockPluginPlatforms := db.NewMockPluginPlatforms(t)
			mockRegistryStore := NewMockRegistryStore(t)
			mockTransactions := db.NewMockTransactions(t)

			mockPluginPlatforms.On("GetPluginPlatformByID", mock.Anything, pluginPlatformID).Return(test.pluginPlatform, nil)

			if test.pluginPlatform != nil {
				mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersion.Metadata.ID).Return(pluginVersion, nil)

				mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(plugin, nil)

				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPluginPlatforms.On("UpdatePluginPlatform", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pluginPlatform *models.PluginPlatform) (*models.PluginPlatform, error) {
						require.True(t, pluginPlatform.BinaryUploaded)
						return pluginPlatform, nil
					},
				)

				mockRegistryStore.On("UploadPluginPlatformBinary", mock.Anything, test.pluginPlatform, pluginVersion, plugin, binaryDataReader).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:         mockPlugins,
				PluginVersions:  mockPluginVersions,
				PluginPlatforms: mockPluginPlatforms,
				Transactions:    mockTransactions,
			}

			service := &service{
				logger:        logger,
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			err := service.UploadPluginPlatformBinary(auth.WithCaller(ctx, mockCaller), pluginPlatformID, binaryDataReader)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestUploadPluginVersionDocFile(t *testing.T) {
	fileContents := strings.NewReader("docs data")
	pluginVersionID := "pluginVersionID"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	type testCase struct {
		name            string
		authError       error
		input           *UploadPluginVersionDocFileInput
		pluginVersion   *models.PluginVersion
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "upload plugin version doc file",
			input: &UploadPluginVersionDocFileInput{
				PluginVersionID: pluginVersionID,
				Category:        models.PluginVersionDocCategoryOverview,
				Title:           "Overview",
				Name:            "index",
				Body:            fileContents,
			},
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
				DocFiles: map[string]*models.PluginVersionDocFileMetadata{},
			},
		},
		{
			name: "plugin doc file category is invalid",
			input: &UploadPluginVersionDocFileInput{
				PluginVersionID: pluginVersionID,
				Category:        "INVALID",
				Title:           "Overview",
				Name:            "index",
				Body:            fileContents,
			},
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
				DocFiles: map[string]*models.PluginVersionDocFileMetadata{},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "overview doc file should not have a subcategory",
			input: &UploadPluginVersionDocFileInput{
				PluginVersionID: pluginVersionID,
				Category:        models.PluginVersionDocCategoryOverview,
				Subcategory:     "Invalid",
				Title:           "Overview",
				Name:            "index",
				Body:            fileContents,
			},
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
				DocFiles: map[string]*models.PluginVersionDocFileMetadata{},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "subject is not authorized to upload plugin version doc file",
			input: &UploadPluginVersionDocFileInput{
				PluginVersionID: pluginVersionID,
				Category:        models.PluginVersionDocCategoryOverview,
				Title:           "Overview",
				Name:            "index",
				Body:            fileContents,
			},
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
				DocFiles: map[string]*models.PluginVersionDocFileMetadata{},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "plugin version not found",
			input: &UploadPluginVersionDocFileInput{
				PluginVersionID: pluginVersionID,
				Category:        models.PluginVersionDocCategoryOverview,
				Title:           "Overview",
				Name:            "index",
				Body:            fileContents,
			},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockRegistryStore := NewMockRegistryStore(t)
			mockTransactions := db.NewMockTransactions(t)

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersionID).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(plugin, nil)

				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				filePath, err := test.input.FilePath()
				require.NoError(t, err)

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPluginVersions.On("UpdatePluginVersion", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
						require.Equal(t, &models.PluginVersionDocFileMetadata{
							Category:    test.input.Category,
							Subcategory: test.input.Subcategory,
							Title:       test.input.Title,
							Name:        test.input.Name,
							FilePath:    filePath,
						}, pluginVersion.DocFiles[filePath])
						return pluginVersion, nil
					},
				)

				mockRegistryStore.On("UploadPluginVersionDocFile", mock.Anything, test.pluginVersion, plugin, filePath, fileContents).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
				Transactions:   mockTransactions,
			}

			service := &service{
				logger:        logger,
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			err := service.UploadPluginVersionDocFile(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestUploadPluginVersionSchema(t *testing.T) {
	schemaDataReader := strings.NewReader("schema data")
	pluginVersionID := "pluginVersionID"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	type testCase struct {
		name            string
		authError       error
		pluginVersion   *models.PluginVersion
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "upload plugin version schema",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
			},
		},
		{
			name: "subject is not authorized to upload plugin version schema",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin version not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "plugin version schema already uploaded",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:       plugin.Metadata.ID,
				SchemaUploaded: true,
			},
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockRegistryStore := NewMockRegistryStore(t)
			mockTransactions := db.NewMockTransactions(t)

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersionID).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(plugin, nil)

				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPluginVersions.On("UpdatePluginVersion", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
						require.True(t, pluginVersion.SchemaUploaded)
						return pluginVersion, nil
					},
				)

				mockRegistryStore.On("UploadPluginVersionSchema", mock.Anything, test.pluginVersion, plugin, schemaDataReader).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
				Transactions:   mockTransactions,
			}

			service := &service{
				logger:        logger,
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			err := service.UploadPluginVersionSchema(auth.WithCaller(ctx, mockCaller), pluginVersionID, schemaDataReader)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestUploadPluginVersionReadme(t *testing.T) {
	readmeDataReader := strings.NewReader("readme data")
	pluginVersionID := "pluginVersionID"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	type testCase struct {
		name            string
		authError       error
		pluginVersion   *models.PluginVersion
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "upload plugin version readme",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
			},
		},
		{
			name: "subject is not authorized to upload plugin version readme",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin version not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "plugin version readme already uploaded",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:       plugin.Metadata.ID,
				ReadmeUploaded: true,
			},
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockRegistryStore := NewMockRegistryStore(t)
			mockTransactions := db.NewMockTransactions(t)

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersionID).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(plugin, nil)

				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPluginVersions.On("UpdatePluginVersion", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
						require.True(t, pluginVersion.ReadmeUploaded)
						return pluginVersion, nil
					},
				)

				mockRegistryStore.On("UploadPluginVersionReadme", mock.Anything, test.pluginVersion, plugin, readmeDataReader).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
				Transactions:   mockTransactions,
			}

			service := &service{
				logger:        logger,
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			err := service.UploadPluginVersionReadme(auth.WithCaller(ctx, mockCaller), pluginVersionID, readmeDataReader)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestUploadPluginVersionSHASums(t *testing.T) {
	shasumsDataReader := strings.NewReader("shasums data")
	pluginVersionID := "pluginVersionID"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	type testCase struct {
		name            string
		authError       error
		pluginVersion   *models.PluginVersion
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "upload plugin version shasums",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
			},
		},
		{
			name: "subject is not authorized to upload plugin version shasums",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID: plugin.Metadata.ID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin version not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "plugin version shasums already uploaded",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SHASumsUploaded: true,
			},
			expectErrorCode: errors.EConflict,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockRegistryStore := NewMockRegistryStore(t)
			mockTransactions := db.NewMockTransactions(t)

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersionID).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(plugin, nil)

				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPluginVersions.On("UpdatePluginVersion", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
						require.True(t, pluginVersion.SHASumsUploaded)
						return pluginVersion, nil
					},
				)

				mockRegistryStore.On("UploadPluginVersionSHASums", mock.Anything, test.pluginVersion, plugin, shasumsDataReader).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
				Transactions:   mockTransactions,
			}

			service := &service{
				logger:        logger,
				dbClient:      dbClient,
				registryStore: mockRegistryStore,
			}

			err := service.UploadPluginVersionSHASums(auth.WithCaller(ctx, mockCaller), pluginVersionID, shasumsDataReader)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestDeletePlugin(t *testing.T) {
	pluginID := "pluginID"
	testSubject := "testSubject"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: pluginID,
		},
		OrganizationID: "orgID",
		Name:           "gitlab",
	}

	type testCase struct {
		authError       error
		plugin          *models.Plugin
		name            string
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name:   "delete plugin",
			plugin: plugin,
		},
		{
			name:            "subject is not authorized to delete plugin",
			plugin:          plugin,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockPlugins.On("GetPluginByID", mock.Anything, pluginID).Return(test.plugin, nil)

			if test.plugin != nil {
				mockCaller.On("GetSubject").Return(testSubject).Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.DeletePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPlugins.On("DeletePlugin", mock.Anything, plugin).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					Action:         models.ActionDelete,
					TargetType:     models.TargetOrganization,
					OrganizationID: &plugin.OrganizationID,
					TargetID:       &plugin.OrganizationID,
					Payload: &models.ActivityEventDeleteResourcePayload{
						Name: &plugin.Name,
						ID:   plugin.Metadata.ID,
						Type: models.TargetPlugin.String(),
					},
				}).Return(&models.ActivityEvent{}, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:      mockPlugins,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			err := service.DeletePlugin(auth.WithCaller(ctx, mockCaller), &DeletePluginInput{ID: pluginID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestDeletePluginVersion(t *testing.T) {
	pluginVersionID := "pluginVersionID"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID:  "pluginID",
			PRN: models.PluginResource.BuildPRN("neutron", "gitlab"),
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	type testCase struct {
		name            string
		authError       error
		pluginVersion   *models.PluginVersion
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "delete non latest plugin version",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "0.1.0",
			},
		},
		{
			name: "delete latest plugin version",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "0.1.0",
				Latest:          true,
			},
		},
		{
			name: "subject is not authorized to delete plugin version",
			pluginVersion: &models.PluginVersion{
				Metadata: models.ResourceMetadata{
					ID: pluginVersionID,
				},
				PluginID:        plugin.Metadata.ID,
				SemanticVersion: "0.1.0",
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin version not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockTransactions := db.NewMockTransactions(t)

			mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersionID).Return(test.pluginVersion, nil)

			if test.pluginVersion != nil {
				mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(plugin, nil)

				mockCaller.On("GetSubject").Return("testSubject").Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)

				if test.pluginVersion.Latest {
					mockPluginVersions.On("GetPluginVersions", mock.Anything, &db.GetPluginVersionsInput{
						Filter: &db.PluginVersionFilter{
							PluginID: &plugin.Metadata.ID,
						},
					}).Return(func(_ context.Context, _ *db.GetPluginVersionsInput) (*db.PluginVersionsResult, error) {
						result := &db.PluginVersionsResult{
							PluginVersions: []models.PluginVersion{*test.pluginVersion},
							PageInfo:       &pagination.PageInfo{},
						}

						// Add a couple more plugin versions to ensure new latest version is marked properly.
						for i := 1; i < 3; i++ {
							result.PluginVersions = append(result.PluginVersions, models.PluginVersion{
								Metadata: models.ResourceMetadata{
									ID: fmt.Sprintf("pluginVersionID-%d", i),
								},
								PluginID:        plugin.Metadata.ID,
								SemanticVersion: fmt.Sprintf("0.0.%d", i),
							})
						}

						return result, nil
					})
				}
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPluginVersions.On("DeletePluginVersion", mock.Anything, test.pluginVersion).Return(nil)

				if test.pluginVersion.Latest {
					mockPluginVersions.On("UpdatePluginVersion", mock.Anything, mock.Anything).Return(
						func(_ context.Context, pluginVersion *models.PluginVersion) (*models.PluginVersion, error) {
							// Ensure that the new latest version is marked properly.
							require.True(t, pluginVersion.Latest)
							require.Equal(t, "pluginVersionID-2", pluginVersion.Metadata.ID)
							return pluginVersion, nil
						},
					)
				}
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:        mockPlugins,
				PluginVersions: mockPluginVersions,
				Transactions:   mockTransactions,
			}

			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			err := service.DeletePluginVersion(auth.WithCaller(ctx, mockCaller), &DeletePluginVersionInput{ID: pluginVersionID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestDeletePluginPlatform(t *testing.T) {
	pluginPlatformID := "pluginPlatformID"

	plugin := &models.Plugin{
		Metadata: models.ResourceMetadata{
			ID: "pluginID",
		},
		Name:           "gitlab",
		OrganizationID: "orgID",
	}

	pluginVersion := &models.PluginVersion{
		Metadata: models.ResourceMetadata{
			ID: "pluginVersionID",
		},
		PluginID: plugin.Metadata.ID,
	}

	type testCase struct {
		name            string
		authError       error
		pluginPlatform  *models.PluginPlatform
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "delete plugin platform",
			pluginPlatform: &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID: pluginPlatformID,
				},
				PluginVersionID: pluginVersion.Metadata.ID,
			},
		},
		{
			name: "subject is not authorized to delete plugin platform",
			pluginPlatform: &models.PluginPlatform{
				Metadata: models.ResourceMetadata{
					ID: pluginPlatformID,
				},
				PluginVersionID: pluginVersion.Metadata.ID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "plugin platform not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPlugins := db.NewMockPlugins(t)
			mockPluginVersions := db.NewMockPluginVersions(t)
			mockPluginPlatforms := db.NewMockPluginPlatforms(t)

			mockPluginPlatforms.On("GetPluginPlatformByID", mock.Anything, pluginPlatformID).Return(test.pluginPlatform, nil)

			if test.pluginPlatform != nil {
				mockPluginVersions.On("GetPluginVersionByID", mock.Anything, pluginVersion.Metadata.ID).Return(pluginVersion, nil)

				mockPlugins.On("GetPluginByID", mock.Anything, plugin.Metadata.ID).Return(plugin, nil)

				mockCaller.On("GetSubject").Return("testSubject").Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePlugin, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockPluginPlatforms.On("DeletePluginPlatform", mock.Anything, test.pluginPlatform).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Plugins:         mockPlugins,
				PluginVersions:  mockPluginVersions,
				PluginPlatforms: mockPluginPlatforms,
			}

			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			err := service.DeletePluginPlatform(auth.WithCaller(ctx, mockCaller), &DeletePluginPlatformInput{ID: pluginPlatformID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
