// Package pluginregistry implements a service for interacting with the plugin registry.
package pluginregistry

import (
	"context"
	"fmt"
	"io"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-version"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/semver"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetPluginsInput is the input for the GetPlugins method
type GetPluginsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.PluginSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// OrganizationID is the organization id to filter by
	OrganizationID *string
	// Search is the search term to filter by
	Search *string
}

// GetPluginVersionsInput is the input for the GetPluginVersions method
type GetPluginVersionsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.PluginVersionSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// SemanticVersion is the semantic version to filter by
	SemanticVersion *string
	// SHASumsUploaded specifies whether to return only plugin versions with SHASums uploaded
	SHASumsUploaded *bool
	// Latest specifies whether to return only the latest version
	Latest *bool
	// PluginID is the plugin id to filter by
	PluginID string
}

// GetPluginPlatformsInput is the input for the GetPluginPlatforms method
type GetPluginPlatformsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.PluginPlatformSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// PluginVersionID is the plugin version id to filter by
	PluginVersionID *string
	// PluginID is the plugin id to filter by
	PluginID *string
	// OperatingSystem is the operating system to filter by
	OperatingSystem *string
	// Architecture is the architecture to filter by
	Architecture *string
	// BinaryUploaded specifies whether to return only plugin platforms with binaries uploaded
	BinaryUploaded *bool
}

// PluginPlatformDownloadURLs contains the signed URLs for downloading a plugin platform
type PluginPlatformDownloadURLs struct {
	DownloadURL string
	SHASumsURL  string
}

// CreatePluginInput is the input for the CreatePlugin method
type CreatePluginInput struct {
	Name           string
	OrganizationID string
	RepositoryURL  string
	Private        bool
}

// CreatePluginVersionInput is the input for the CreatePluginVersion method
type CreatePluginVersionInput struct {
	PluginID        string
	SemanticVersion string
	Protocols       []string
}

// CreatePluginPlatformInput is the input for the CreatePluginPlatform method
type CreatePluginPlatformInput struct {
	PluginVersionID string
	OperatingSystem string
	Architecture    string
	Filename        string
	SHASum          string
}

// UpdatePluginInput is the input for the UpdatePlugin method
type UpdatePluginInput struct {
	RepositoryURL *string
	Private       *bool
	Version       *int
	ID            string
}

// DeletePluginInput is the input for the DeletePlugin method
type DeletePluginInput struct {
	Version *int
	ID      string
}

// DeletePluginVersionInput is the input for the DeletePluginVersion method
type DeletePluginVersionInput struct {
	Version *int
	ID      string
}

// DeletePluginPlatformInput is the input for the DeletePluginPlatform method
type DeletePluginPlatformInput struct {
	Version *int
	ID      string
}

// UploadPluginVersionDocFileInput is the input for uploading a plugin version doc file
type UploadPluginVersionDocFileInput struct {
	Body            io.Reader
	PluginVersionID string
	Category        models.PluginVersionDocCategory
	Subcategory     string
	Title           string
	Name            string
}

// FilePath returns the file path for the doc file
func (u *UploadPluginVersionDocFileInput) FilePath() (string, error) {
	var filePath string
	switch u.Category {
	case models.PluginVersionDocCategoryOverview:
		filePath = "index.md"
	case models.PluginVersionDocCategoryGuide:
		filePath = fmt.Sprintf("guides/%s.md", u.Name)
	case models.PluginVersionDocCategoryAction:
		filePath = fmt.Sprintf("actions/%s.md", u.Name)
	default:
		return "", errors.New("invalid doc category %s", u.Category, errors.WithErrorCode(errors.EInvalid))
	}
	return filePath, nil
}

// GetPluginVersionDocFileInput is the input for getting a plugin version doc file
type GetPluginVersionDocFileInput struct {
	PluginVersionID string
	FilePath        string
}

// Service is the interface for the plugin registry service
type Service interface {
	GetPluginByID(ctx context.Context, id string) (*models.Plugin, error)
	GetPluginByPRN(ctx context.Context, prn string) (*models.Plugin, error)
	GetPlugins(ctx context.Context, input *GetPluginsInput) (*db.PluginsResult, error)
	GetPluginsByIDs(ctx context.Context, idList []string) ([]models.Plugin, error)
	GetPluginVersionByID(ctx context.Context, id string) (*models.PluginVersion, error)
	GetPluginVersionByPRN(ctx context.Context, prn string) (*models.PluginVersion, error)
	GetPluginVersions(ctx context.Context, input *GetPluginVersionsInput) (*db.PluginVersionsResult, error)
	GetPluginVersionsByIDs(ctx context.Context, idList []string) ([]models.PluginVersion, error)
	GetPluginVersionDocFile(ctx context.Context, input *GetPluginVersionDocFileInput) (string, error)
	GetPluginVersionReadme(ctx context.Context, pluginVersion *models.PluginVersion) (string, error)
	GetPluginPlatformByID(ctx context.Context, id string) (*models.PluginPlatform, error)
	GetPluginPlatformByPRN(ctx context.Context, prn string) (*models.PluginPlatform, error)
	GetPluginPlatforms(ctx context.Context, input *GetPluginPlatformsInput) (*db.PluginPlatformsResult, error)
	GetPluginPlatformDownloadURLs(ctx context.Context, pluginPlatformID string) (*PluginPlatformDownloadURLs, error)
	CreatePlugin(ctx context.Context, input *CreatePluginInput) (*models.Plugin, error)
	CreatePluginVersion(ctx context.Context, input *CreatePluginVersionInput) (*models.PluginVersion, error)
	CreatePluginPlatform(ctx context.Context, input *CreatePluginPlatformInput) (*models.PluginPlatform, error)
	UpdatePlugin(ctx context.Context, input *UpdatePluginInput) (*models.Plugin, error)
	UploadPluginPlatformBinary(ctx context.Context, pluginPlatformID string, body io.Reader) error
	UploadPluginVersionDocFile(ctx context.Context, input *UploadPluginVersionDocFileInput) error
	UploadPluginVersionReadme(ctx context.Context, pluginVersionID string, body io.Reader) error
	UploadPluginVersionSchema(ctx context.Context, pluginVersionID string, body io.Reader) error
	UploadPluginVersionSHASums(ctx context.Context, pluginVersionID string, body io.Reader) error
	DeletePlugin(ctx context.Context, input *DeletePluginInput) error
	DeletePluginVersion(ctx context.Context, input *DeletePluginVersionInput) error
	DeletePluginPlatform(ctx context.Context, input *DeletePluginPlatformInput) error
}

// service is the implementation of the plugin registry service
type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	registryStore   RegistryStore
	limitChecker    limits.LimitChecker
	activityService activityevent.Service
}

// NewService creates a new instance of the plugin registry service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	registryStore RegistryStore,
	limitChecker limits.LimitChecker,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		registryStore:   registryStore,
		activityService: activityService,
	}
}

func (s *service) GetPluginByID(ctx context.Context, id string) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	return plugin, nil
}

func (s *service) GetPluginByPRN(ctx context.Context, prn string) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	plugin, err := s.dbClient.Plugins.GetPluginByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin by prn", errors.WithSpan(span))
	}

	if plugin == nil {
		return nil, errors.New("plugin with prn %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	return plugin, nil
}

func (s *service) GetPlugins(ctx context.Context, input *GetPluginsInput) (*db.PluginsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPlugins")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetPluginsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.PluginFilter{
			OrganizationID: input.OrganizationID,
			Search:         input.Search,
		},
	}

	if dbInput.Filter.OrganizationID != nil {
		if err := caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(*input.OrganizationID)); err != nil {
			return nil, err
		}
	} else if !caller.IsAdmin() {
		// Limit to the current user or service account
		if err := auth.HandleCaller(
			ctx,
			func(_ context.Context, c *auth.UserCaller) error {
				dbInput.Filter.UserID = &c.User.Metadata.ID
				return nil
			},
			func(_ context.Context, c *auth.ServiceAccountCaller) error {
				dbInput.Filter.ServiceAccountID = &c.ServiceAccountID
				return nil
			},
		); err != nil {
			return nil, errors.Wrap(err, "failed to handle caller", errors.WithSpan(span))
		}
	}

	return s.dbClient.Plugins.GetPlugins(ctx, dbInput)
}

func (s *service) GetPluginsByIDs(ctx context.Context, idList []string) ([]models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginsByIDs")
	span.SetAttributes(attribute.StringSlice("idList", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	response, err := s.dbClient.Plugins.GetPlugins(ctx, &db.GetPluginsInput{
		Filter: &db.PluginFilter{
			PluginIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugins by ids", errors.WithSpan(span))
	}

	organizationIDs := []string{}
	for _, plugin := range response.Plugins {
		if plugin.Private {
			organizationIDs = append(organizationIDs, plugin.OrganizationID)
		}
	}

	if len(organizationIDs) > 0 {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationIDs(organizationIDs)); err != nil {
			return nil, err
		}
	}

	return response.Plugins, nil
}

func (s *service) GetPluginVersionByID(ctx context.Context, id string) (*models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginVersionByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	return pluginVersion, nil
}

func (s *service) GetPluginVersionByPRN(ctx context.Context, prn string) (*models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginVersionByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pluginVersion, err := s.dbClient.PluginVersions.GetPluginVersionByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin version by prn", errors.WithSpan(span))
	}

	if pluginVersion == nil {
		return nil, errors.New("plugin version with prn %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	return pluginVersion, nil
}

func (s *service) GetPluginVersions(ctx context.Context, input *GetPluginVersionsInput) (*db.PluginVersionsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginVersions")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, input.PluginID)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	dbInput := &db.GetPluginVersionsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.PluginVersionFilter{
			PluginID:        &input.PluginID,
			SemanticVersion: input.SemanticVersion,
			SHASumsUploaded: input.SHASumsUploaded,
			Latest:          input.Latest,
		},
	}

	return s.dbClient.PluginVersions.GetPluginVersions(ctx, dbInput)
}

func (s *service) GetPluginVersionsByIDs(ctx context.Context, idList []string) ([]models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginVersionsByIDs")
	span.SetAttributes(attribute.StringSlice("idList", idList))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	response, err := s.dbClient.PluginVersions.GetPluginVersions(ctx, &db.GetPluginVersionsInput{
		Filter: &db.PluginVersionFilter{
			PluginVersionIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin versions by ids", errors.WithSpan(span))
	}

	pluginIDs := []string{}
	for _, pluginVersion := range response.PluginVersions {
		pluginIDs = append(pluginIDs, pluginVersion.PluginID)
	}

	// Ensure the caller has access to the plugins
	if _, err := s.GetPluginsByIDs(ctx, pluginIDs); err != nil {
		return nil, err
	}

	return response.PluginVersions, nil
}

func (s *service) GetPluginVersionDocFile(ctx context.Context, input *GetPluginVersionDocFileInput) (string, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginVersionDocFile")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return "", err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, input.PluginVersionID)
	if err != nil {
		return "", err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return "", err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return "", err
		}
	}

	if _, ok := pluginVersion.DocFiles[input.FilePath]; !ok {
		return "", errors.New("doc file with path %s not found", input.FilePath, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	reader, err := s.registryStore.GetPluginVersionDocFile(ctx, pluginVersion, plugin, input.FilePath)
	if err != nil {
		return "", errors.Wrap(err, "failed to get plugin version doc file", errors.WithSpan(span))
	}
	defer reader.Close()

	buffer, err := io.ReadAll(reader)
	if err != nil {
		return "", errors.Wrap(err, "failed to read plugin version doc file", errors.WithSpan(span))
	}

	return string(buffer), nil
}

func (s *service) GetPluginVersionReadme(ctx context.Context, pluginVersion *models.PluginVersion) (string, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginVersionReadme")
	span.SetAttributes(attribute.String("plugin id", pluginVersion.PluginID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return "", err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return "", err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return "", err
		}
	}

	reader, err := s.registryStore.GetPluginVersionReadme(ctx, pluginVersion, plugin)
	if err != nil {
		return "", errors.Wrap(err, "failed to get plugin version readme", errors.WithSpan(span))
	}
	defer reader.Close()

	buffer, err := io.ReadAll(reader)
	if err != nil {
		return "", errors.Wrap(err, "failed to read readme", errors.WithSpan(span))
	}

	return string(buffer), nil
}

func (s *service) GetPluginPlatformByID(ctx context.Context, id string) (*models.PluginPlatform, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginPlatformByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pluginPlatform, err := s.getPluginPlatformByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginPlatform.PluginVersionID)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	return pluginPlatform, nil
}

func (s *service) GetPluginPlatformByPRN(ctx context.Context, prn string) (*models.PluginPlatform, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginPlatformByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pluginPlatform, err := s.dbClient.PluginPlatforms.GetPluginPlatformByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin platform by prn", errors.WithSpan(span))
	}

	if pluginPlatform == nil {
		return nil, errors.New("plugin platform with prn %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginPlatform.PluginVersionID)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	return pluginPlatform, nil
}

func (s *service) GetPluginPlatforms(ctx context.Context, input *GetPluginPlatformsInput) (*db.PluginPlatformsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginPlatforms")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var pluginID string
	switch {
	case input.PluginVersionID != nil:
		pluginVersion, pErr := s.getPluginVersionByID(ctx, span, *input.PluginVersionID)
		if pErr != nil {
			return nil, pErr
		}

		pluginID = pluginVersion.PluginID
	case input.PluginID != nil:
		pluginID = *input.PluginID
	default:
		return nil, errors.New("either plugin version id or plugin id must be specified",
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	plugin, err := s.getPluginByID(ctx, span, pluginID)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	dbInput := &db.GetPluginPlatformsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.PluginPlatformFilter{
			PluginVersionID: input.PluginVersionID,
			PluginID:        input.PluginID,
			OperatingSystem: input.OperatingSystem,
			Architecture:    input.Architecture,
			BinaryUploaded:  input.BinaryUploaded,
		},
	}

	return s.dbClient.PluginPlatforms.GetPluginPlatforms(ctx, dbInput)
}

func (s *service) GetPluginPlatformDownloadURLs(ctx context.Context, pluginPlatformID string) (*PluginPlatformDownloadURLs, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPluginPlatformDownloadURLs")
	span.SetAttributes(attribute.String("plugin platform id", pluginPlatformID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pluginPlatform, err := s.getPluginPlatformByID(ctx, span, pluginPlatformID)
	if err != nil {
		return nil, err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginPlatform.PluginVersionID)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return nil, err
	}

	if plugin.Private {
		if err = caller.RequireAccessToInheritableResource(ctx, models.PluginResource, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
			return nil, err
		}
	}

	downloadURL, err := s.registryStore.GetPluginPlatformBinaryPresignedURL(ctx, pluginPlatform, pluginVersion, plugin)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin platform binary presigned url", errors.WithSpan(span))
	}

	shaSumsURL, err := s.registryStore.GetPluginVersionSHASumsPresignedURL(ctx, pluginVersion, plugin)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin version SHA sums presigned url", errors.WithSpan(span))
	}

	return &PluginPlatformDownloadURLs{
		DownloadURL: downloadURL,
		SHASumsURL:  shaSumsURL,
	}, nil
}

func (s *service) CreatePlugin(ctx context.Context, input *CreatePluginInput) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "svc.CreatePlugin")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.CreatePlugin, auth.WithOrganizationID(input.OrganizationID)); err != nil {
		return nil, err
	}

	toCreate := &models.Plugin{
		Name:           input.Name,
		OrganizationID: input.OrganizationID,
		RepositoryURL:  input.RepositoryURL,
		Private:        input.Private,
		CreatedBy:      caller.GetSubject(),
	}

	if err = toCreate.Validate(); err != nil {
		return nil, err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreatePlugin: %v", txErr)
		}
	}()

	createdPlugin, err := s.dbClient.Plugins.CreatePlugin(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create plugin", errors.WithSpan(span))
	}

	// Check if we're still in the limit
	response, err := s.dbClient.Plugins.GetPlugins(txContext, &db.GetPluginsInput{
		Filter: &db.PluginFilter{
			OrganizationID: &createdPlugin.OrganizationID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugins", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitPluginsPerOrganization, response.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "failed to check limit", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, &activityevent.CreateActivityEventInput{
		Action:         models.ActionCreate,
		TargetType:     models.TargetPlugin,
		OrganizationID: &createdPlugin.OrganizationID,
		TargetID:       &createdPlugin.Metadata.ID,
	}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a plugin",
		"caller", caller.GetSubject(),
		"plugin_name", createdPlugin.Name,
		"organization_id", createdPlugin.OrganizationID,
	)

	return createdPlugin, nil
}

func (s *service) CreatePluginVersion(ctx context.Context, input *CreatePluginVersionInput) (*models.PluginVersion, error) {
	ctx, span := tracer.Start(ctx, "svc.CreatePluginVersion")
	span.SetAttributes(attribute.String("plugin id", input.PluginID))
	span.SetAttributes(attribute.String("semantic version", input.SemanticVersion))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, input.PluginID)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return nil, err
	}

	semanticVersion, err := version.NewSemver(input.SemanticVersion)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse semantic version", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Check if this version is greater than the latest version
	versionsResp, err := s.dbClient.PluginVersions.GetPluginVersions(ctx, &db.GetPluginVersionsInput{
		Filter: &db.PluginVersionFilter{
			PluginID: &input.PluginID,
			Latest:   ptr.Bool(true),
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin versions", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for CreatePluginVersion: %v", txErr)
		}
	}()

	isLatest := false
	if versionsResp.PageInfo.TotalCount > 0 {
		// If there are versions, check if this is the latest
		prevLatest := versionsResp.PluginVersions[0]
		prevSemanticVersion, vErr := version.NewSemver(prevLatest.SemanticVersion)
		if vErr != nil {
			return nil, errors.Wrap(vErr, "failed to parse semantic version", errors.WithSpan(span))
		}

		if semver.IsSemverGreaterThan(semanticVersion, prevSemanticVersion) {
			isLatest = true
			prevLatest.Latest = false

			// Update the previous version so it's no longer the latest
			if _, err = s.dbClient.PluginVersions.UpdatePluginVersion(txContext, &prevLatest); err != nil {
				return nil, errors.Wrap(err, "failed to update plugin version", errors.WithSpan(span))
			}
		}
	} else {
		// If there are no versions, this is the latest
		isLatest = true
	}

	toCreate := &models.PluginVersion{
		PluginID:        input.PluginID,
		SemanticVersion: semanticVersion.String(),
		Latest:          isLatest,
		CreatedBy:       caller.GetSubject(),
		Protocols:       input.Protocols,
		DocFiles:        map[string]*models.PluginVersionDocFileMetadata{},
	}

	if err = toCreate.Validate(); err != nil {
		return nil, err
	}

	createdVersion, err := s.dbClient.PluginVersions.CreatePluginVersion(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create plugin version", errors.WithSpan(span))
	}

	newVersions, err := s.dbClient.PluginVersions.GetPluginVersions(txContext, &db.GetPluginVersionsInput{
		Filter: &db.PluginVersionFilter{
			PluginID: &input.PluginID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin versions", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitVersionsPerPlugin, newVersions.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "failed to check limit", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, &activityevent.CreateActivityEventInput{
		Action:         models.ActionCreate,
		TargetType:     models.TargetPluginVersion,
		OrganizationID: &plugin.OrganizationID,
		TargetID:       &createdVersion.Metadata.ID,
	}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a plugin version",
		"caller", caller.GetSubject(),
		"plugin_id", input.PluginID,
		"semantic_version", input.SemanticVersion,
	)

	return createdVersion, nil
}

func (s *service) CreatePluginPlatform(ctx context.Context, input *CreatePluginPlatformInput) (*models.PluginPlatform, error) {
	ctx, span := tracer.Start(ctx, "svc.CreatePluginPlatform")
	span.SetAttributes(attribute.String("plugin version id", input.PluginVersionID))
	span.SetAttributes(attribute.String("operating system", input.OperatingSystem))
	span.SetAttributes(attribute.String("architecture", input.Architecture))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, input.PluginVersionID)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return nil, err
	}

	toCreate := &models.PluginPlatform{
		PluginVersionID: input.PluginVersionID,
		OperatingSystem: input.OperatingSystem,
		Architecture:    input.Architecture,
		SHASum:          input.SHASum,
		Filename:        input.Filename,
		CreatedBy:       caller.GetSubject(),
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for CreatePluginPlatform: %v", txErr)
		}
	}()

	createdPlatform, err := s.dbClient.PluginPlatforms.CreatePluginPlatform(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create plugin platform", errors.WithSpan(span))
	}

	newPlatforms, err := s.dbClient.PluginPlatforms.GetPluginPlatforms(txContext, &db.GetPluginPlatformsInput{
		Filter: &db.PluginPlatformFilter{
			PluginVersionID: &input.PluginVersionID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin platforms", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitPlatformsPerPluginVersion, newPlatforms.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "failed to check limit", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a plugin platform",
		"caller", caller.GetSubject(),
		"plugin_version_id", input.PluginVersionID,
		"operating_system", input.OperatingSystem,
		"architecture", input.Architecture,
	)

	return createdPlatform, nil
}

func (s *service) UpdatePlugin(ctx context.Context, input *UpdatePluginInput) (*models.Plugin, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdatePlugin")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	plugin, err := s.getPluginByID(ctx, span, input.ID)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return nil, err
	}

	if input.Version != nil {
		plugin.Metadata.Version = *input.Version
	}

	if input.RepositoryURL != nil {
		plugin.RepositoryURL = *input.RepositoryURL
	}

	if input.Private != nil {
		plugin.Private = *input.Private
	}

	if err = plugin.Validate(); err != nil {
		return nil, err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdatePlugin: %v", txErr)
		}
	}()

	updatedPlugin, err := s.dbClient.Plugins.UpdatePlugin(txContext, plugin)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update plugin", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, &activityevent.CreateActivityEventInput{
		Action:         models.ActionUpdate,
		TargetType:     models.TargetPlugin,
		OrganizationID: &plugin.OrganizationID,
		TargetID:       &plugin.Metadata.ID,
	}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a plugin",
		"caller", caller.GetSubject(),
		"plugin_name", updatedPlugin.Name,
		"organization_id", updatedPlugin.OrganizationID,
	)

	return updatedPlugin, nil
}

func (s *service) UploadPluginPlatformBinary(ctx context.Context, pluginPlatformID string, reader io.Reader) error {
	ctx, span := tracer.Start(ctx, "svc.UploadPluginPlatformBinary")
	span.SetAttributes(attribute.String("plugin platform id", pluginPlatformID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pluginPlatform, err := s.getPluginPlatformByID(ctx, span, pluginPlatformID)
	if err != nil {
		return err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginPlatform.PluginVersionID)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	if pluginPlatform.BinaryUploaded {
		return errors.New("plugin platform binary already uploaded", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
	}

	pluginPlatform.BinaryUploaded = true

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx in service layer UploadPluginPlatformBinary: %v", txErr)
		}
	}()

	if _, err = s.dbClient.PluginPlatforms.UpdatePluginPlatform(txContext, pluginPlatform); err != nil {
		return errors.Wrap(err, "failed to update plugin platform", errors.WithSpan(span))
	}

	if err = s.registryStore.UploadPluginPlatformBinary(ctx, pluginPlatform, pluginVersion, plugin, reader); err != nil {
		return errors.Wrap(err, "failed to upload plugin platform binary", errors.WithSpan(span))
	}

	return s.dbClient.Transactions.CommitTx(txContext)
}

func (s *service) UploadPluginVersionDocFile(ctx context.Context, input *UploadPluginVersionDocFileInput) error {
	ctx, span := tracer.Start(ctx, "svc.UploadPluginVersionDocFile")
	span.SetAttributes(attribute.String("plugin version id", input.PluginVersionID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, input.PluginVersionID)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	filePath, err := input.FilePath()
	if err != nil {
		return err
	}

	metadata := &models.PluginVersionDocFileMetadata{
		Name:        input.Name,
		Subcategory: input.Subcategory,
		Title:       input.Title,
		FilePath:    filePath,
		Category:    input.Category,
	}

	if metadata.Title == "" {
		metadata.Title = metadata.Name
	}

	// Validate the doc file metadata
	if err = metadata.Validate(); err != nil {
		return err
	}

	// Update the doc file metadata
	pluginVersion.DocFiles[filePath] = metadata

	// Verify the max number of doc files has not been exceeded
	if len(pluginVersion.DocFiles) > models.MaxPluginVersionDocFiles {
		return errors.New("maximum number of doc files reached", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx in service layer UploadPluginVersionDocFile: %v", txErr)
		}
	}()

	// Update doc file metadata in the DB
	if _, err = s.dbClient.PluginVersions.UpdatePluginVersion(txContext, pluginVersion); err != nil {
		return errors.Wrap(err, "failed to update plugin version", errors.WithSpan(span))
	}

	// Upload doc file to object storage
	if err = s.registryStore.UploadPluginVersionDocFile(ctx, pluginVersion, plugin, metadata.FilePath, input.Body); err != nil {
		return errors.Wrap(err, "failed to upload plugin version doc file", errors.WithSpan(span))
	}

	return s.dbClient.Transactions.CommitTx(txContext)
}

func (s *service) UploadPluginVersionReadme(ctx context.Context, pluginVersionID string, reader io.Reader) error {
	ctx, span := tracer.Start(ctx, "svc.UploadPluginVersionReadme")
	span.SetAttributes(attribute.String("plugin version id", pluginVersionID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginVersionID)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	if pluginVersion.ReadmeUploaded {
		return errors.New("plugin version README already uploaded", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
	}

	pluginVersion.ReadmeUploaded = true

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx in service layer UploadPluginVersionReadme: %v", txErr)
		}
	}()

	if _, err = s.dbClient.PluginVersions.UpdatePluginVersion(txContext, pluginVersion); err != nil {
		return errors.Wrap(err, "failed to update plugin version", errors.WithSpan(span))
	}

	if err = s.registryStore.UploadPluginVersionReadme(ctx, pluginVersion, plugin, reader); err != nil {
		return errors.Wrap(err, "failed to upload plugin version readme", errors.WithSpan(span))
	}

	return s.dbClient.Transactions.CommitTx(txContext)
}

func (s *service) UploadPluginVersionSchema(ctx context.Context, pluginVersionID string, reader io.Reader) error {
	ctx, span := tracer.Start(ctx, "svc.UploadPluginVersionSchema")
	span.SetAttributes(attribute.String("plugin version id", pluginVersionID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginVersionID)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	if pluginVersion.SchemaUploaded {
		return errors.New("plugin version schema already uploaded", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
	}

	pluginVersion.SchemaUploaded = true

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx in service layer UploadPluginVersionSchema: %v", txErr)
		}
	}()

	if _, err = s.dbClient.PluginVersions.UpdatePluginVersion(txContext, pluginVersion); err != nil {
		return errors.Wrap(err, "failed to update plugin version", errors.WithSpan(span))
	}

	if err = s.registryStore.UploadPluginVersionSchema(ctx, pluginVersion, plugin, reader); err != nil {
		return errors.Wrap(err, "failed to upload plugin version schema", errors.WithSpan(span))
	}

	return s.dbClient.Transactions.CommitTx(txContext)
}

func (s *service) UploadPluginVersionSHASums(ctx context.Context, pluginVersionID string, reader io.Reader) error {
	ctx, span := tracer.Start(ctx, "svc.UploadPluginVersionSHASums")
	span.SetAttributes(attribute.String("plugin version id", pluginVersionID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginVersionID)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	if pluginVersion.SHASumsUploaded {
		return errors.New("plugin version SHA sums already uploaded", errors.WithErrorCode(errors.EConflict), errors.WithSpan(span))
	}

	pluginVersion.SHASumsUploaded = true

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx in service layer UploadPluginVersionSHASums: %v", txErr)
		}
	}()

	if _, err = s.dbClient.PluginVersions.UpdatePluginVersion(txContext, pluginVersion); err != nil {
		return errors.Wrap(err, "failed to update plugin version", errors.WithSpan(span))
	}

	if err = s.registryStore.UploadPluginVersionSHASums(ctx, pluginVersion, plugin, reader); err != nil {
		return errors.Wrap(err, "failed to upload plugin version SHA sums", errors.WithSpan(span))
	}

	return s.dbClient.Transactions.CommitTx(txContext)
}

func (s *service) DeletePlugin(ctx context.Context, input *DeletePluginInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeletePlugin")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.DeletePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	if input.Version != nil {
		plugin.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeletePlugin: %v", txErr)
		}
	}()

	if err = s.dbClient.Plugins.DeletePlugin(txContext, plugin); err != nil {
		return errors.Wrap(err, "failed to delete plugin", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, &activityevent.CreateActivityEventInput{
		Action:         models.ActionDelete,
		TargetType:     models.TargetOrganization,
		OrganizationID: &plugin.OrganizationID,
		TargetID:       &plugin.OrganizationID,
		Payload: &models.ActivityEventDeleteResourcePayload{
			Name: &plugin.Name,
			ID:   plugin.Metadata.ID,
			Type: models.TargetPlugin.String(),
		},
	}); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a plugin",
		"caller", caller.GetSubject(),
		"organization_id", plugin.OrganizationID,
		"plugin_name", plugin.Name,
	)

	return nil
}

func (s *service) DeletePluginVersion(ctx context.Context, input *DeletePluginVersionInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeletePluginVersion")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	// Reset the latest version if this is the latest
	var newLatestVersion *models.PluginVersion
	if pluginVersion.Latest {
		versionsResp, gErr := s.dbClient.PluginVersions.GetPluginVersions(ctx, &db.GetPluginVersionsInput{
			Filter: &db.PluginVersionFilter{
				PluginID: &pluginVersion.PluginID,
			},
		})
		if gErr != nil {
			return errors.Wrap(gErr, "failed to get plugin versions", errors.WithSpan(span))
		}

		for _, v := range versionsResp.PluginVersions {
			vCopy := v

			// Skip the version we're deleting
			if vCopy.Metadata.ID == pluginVersion.Metadata.ID {
				continue
			}

			if newLatestVersion == nil {
				newLatestVersion = &vCopy
				continue
			}

			latestSemver, vErr := version.NewSemver(newLatestVersion.SemanticVersion)
			if vErr != nil {
				return errors.Wrap(vErr, "failed to parse semantic version", errors.WithSpan(span))
			}

			currentSemver, vErr := version.NewSemver(vCopy.SemanticVersion)
			if vErr != nil {
				return errors.Wrap(vErr, "failed to parse semantic version", errors.WithSpan(span))
			}

			if semver.IsSemverGreaterThan(currentSemver, latestSemver) {
				newLatestVersion = &vCopy
			}
		}
	}

	if input.Version != nil {
		pluginVersion.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeletePluginVersion: %v", txErr)
		}
	}()

	if err = s.dbClient.PluginVersions.DeletePluginVersion(txContext, pluginVersion); err != nil {
		return errors.Wrap(err, "failed to delete plugin version", errors.WithSpan(span))
	}

	if newLatestVersion != nil {
		// Display the PRN because it will contain the organization name
		path, pErr := models.PluginResource.ResourcePathFromPRN(plugin.Metadata.PRN)
		if pErr != nil {
			return errors.Wrap(pErr, "failed to get resource path from prn", errors.WithSpan(span))
		}

		s.logger.Infof("Deleted latest plugin version, latest flag is being set to version %s for plugin %s",
			newLatestVersion.SemanticVersion,
			path,
		)

		newLatestVersion.Latest = true
		if _, err = s.dbClient.PluginVersions.UpdatePluginVersion(txContext, newLatestVersion); err != nil {
			return errors.Wrap(err, "failed to update plugin version", errors.WithSpan(span))
		}
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a plugin version",
		"caller", caller.GetSubject(),
		"plugin_id", pluginVersion.PluginID,
		"semantic_version", pluginVersion.SemanticVersion,
	)

	return nil
}

func (s *service) DeletePluginPlatform(ctx context.Context, input *DeletePluginPlatformInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeletePluginPlatform")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pluginPlatform, err := s.getPluginPlatformByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	pluginVersion, err := s.getPluginVersionByID(ctx, span, pluginPlatform.PluginVersionID)
	if err != nil {
		return err
	}

	plugin, err := s.getPluginByID(ctx, span, pluginVersion.PluginID)
	if err != nil {
		return err
	}

	if err = caller.RequirePermission(ctx, models.UpdatePlugin, auth.WithOrganizationID(plugin.OrganizationID)); err != nil {
		return err
	}

	if input.Version != nil {
		pluginPlatform.Metadata.Version = *input.Version
	}

	if err = s.dbClient.PluginPlatforms.DeletePluginPlatform(ctx, pluginPlatform); err != nil {
		return errors.Wrap(err, "failed to delete plugin platform", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a plugin platform",
		"caller", caller.GetSubject(),
		"plugin_version_id", pluginPlatform.PluginVersionID,
		"operating_system", pluginPlatform.OperatingSystem,
		"architecture", pluginPlatform.Architecture,
	)

	return nil
}

func (s *service) getPluginByID(ctx context.Context, span trace.Span, id string) (*models.Plugin, error) {
	plugin, err := s.dbClient.Plugins.GetPluginByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin by id", errors.WithSpan(span))
	}

	if plugin == nil {
		return nil, errors.New("plugin with id %s not found", id,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	return plugin, nil
}

func (s *service) getPluginVersionByID(ctx context.Context, span trace.Span, id string) (*models.PluginVersion, error) {
	pluginVersion, err := s.dbClient.PluginVersions.GetPluginVersionByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin version by id", errors.WithSpan(span))
	}

	if pluginVersion == nil {
		return nil, errors.New("plugin version with id %s not found", id,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	return pluginVersion, nil
}

func (s *service) getPluginPlatformByID(ctx context.Context, span trace.Span, id string) (*models.PluginPlatform, error) {
	pluginPlatform, err := s.dbClient.PluginPlatforms.GetPluginPlatformByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get plugin platform by id", errors.WithSpan(span))
	}

	if pluginPlatform == nil {
		return nil, errors.New("plugin platform with id %s not found", id,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	return pluginPlatform, nil
}
