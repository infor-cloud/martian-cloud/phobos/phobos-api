// Code generated by mockery v2.53.0. DO NOT EDIT.

package pluginregistry

import (
	context "context"
	io "io"

	mock "github.com/stretchr/testify/mock"

	models "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
)

// MockRegistryStore is an autogenerated mock type for the RegistryStore type
type MockRegistryStore struct {
	mock.Mock
}

// GetPluginPlatformBinaryPresignedURL provides a mock function with given fields: ctx, pluginPlatform, pluginVersion, plugin
func (_m *MockRegistryStore) GetPluginPlatformBinaryPresignedURL(ctx context.Context, pluginPlatform *models.PluginPlatform, pluginVersion *models.PluginVersion, plugin *models.Plugin) (string, error) {
	ret := _m.Called(ctx, pluginPlatform, pluginVersion, plugin)

	if len(ret) == 0 {
		panic("no return value specified for GetPluginPlatformBinaryPresignedURL")
	}

	var r0 string
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginPlatform, *models.PluginVersion, *models.Plugin) (string, error)); ok {
		return rf(ctx, pluginPlatform, pluginVersion, plugin)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginPlatform, *models.PluginVersion, *models.Plugin) string); ok {
		r0 = rf(ctx, pluginPlatform, pluginVersion, plugin)
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func(context.Context, *models.PluginPlatform, *models.PluginVersion, *models.Plugin) error); ok {
		r1 = rf(ctx, pluginPlatform, pluginVersion, plugin)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetPluginVersionDocFile provides a mock function with given fields: ctx, pluginVersion, plugin, filePath
func (_m *MockRegistryStore) GetPluginVersionDocFile(ctx context.Context, pluginVersion *models.PluginVersion, plugin *models.Plugin, filePath string) (io.ReadCloser, error) {
	ret := _m.Called(ctx, pluginVersion, plugin, filePath)

	if len(ret) == 0 {
		panic("no return value specified for GetPluginVersionDocFile")
	}

	var r0 io.ReadCloser
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin, string) (io.ReadCloser, error)); ok {
		return rf(ctx, pluginVersion, plugin, filePath)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin, string) io.ReadCloser); ok {
		r0 = rf(ctx, pluginVersion, plugin, filePath)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(io.ReadCloser)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *models.PluginVersion, *models.Plugin, string) error); ok {
		r1 = rf(ctx, pluginVersion, plugin, filePath)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetPluginVersionReadme provides a mock function with given fields: ctx, pluginVersion, plugin
func (_m *MockRegistryStore) GetPluginVersionReadme(ctx context.Context, pluginVersion *models.PluginVersion, plugin *models.Plugin) (io.ReadCloser, error) {
	ret := _m.Called(ctx, pluginVersion, plugin)

	if len(ret) == 0 {
		panic("no return value specified for GetPluginVersionReadme")
	}

	var r0 io.ReadCloser
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin) (io.ReadCloser, error)); ok {
		return rf(ctx, pluginVersion, plugin)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin) io.ReadCloser); ok {
		r0 = rf(ctx, pluginVersion, plugin)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(io.ReadCloser)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *models.PluginVersion, *models.Plugin) error); ok {
		r1 = rf(ctx, pluginVersion, plugin)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetPluginVersionSHASumsPresignedURL provides a mock function with given fields: ctx, pluginVersion, plugin
func (_m *MockRegistryStore) GetPluginVersionSHASumsPresignedURL(ctx context.Context, pluginVersion *models.PluginVersion, plugin *models.Plugin) (string, error) {
	ret := _m.Called(ctx, pluginVersion, plugin)

	if len(ret) == 0 {
		panic("no return value specified for GetPluginVersionSHASumsPresignedURL")
	}

	var r0 string
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin) (string, error)); ok {
		return rf(ctx, pluginVersion, plugin)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin) string); ok {
		r0 = rf(ctx, pluginVersion, plugin)
	} else {
		r0 = ret.Get(0).(string)
	}

	if rf, ok := ret.Get(1).(func(context.Context, *models.PluginVersion, *models.Plugin) error); ok {
		r1 = rf(ctx, pluginVersion, plugin)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UploadPluginPlatformBinary provides a mock function with given fields: ctx, pluginPlatform, pluginVersion, plugin, body
func (_m *MockRegistryStore) UploadPluginPlatformBinary(ctx context.Context, pluginPlatform *models.PluginPlatform, pluginVersion *models.PluginVersion, plugin *models.Plugin, body io.Reader) error {
	ret := _m.Called(ctx, pluginPlatform, pluginVersion, plugin, body)

	if len(ret) == 0 {
		panic("no return value specified for UploadPluginPlatformBinary")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginPlatform, *models.PluginVersion, *models.Plugin, io.Reader) error); ok {
		r0 = rf(ctx, pluginPlatform, pluginVersion, plugin, body)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadPluginVersionDocFile provides a mock function with given fields: ctx, pluginVersion, plugin, filePath, body
func (_m *MockRegistryStore) UploadPluginVersionDocFile(ctx context.Context, pluginVersion *models.PluginVersion, plugin *models.Plugin, filePath string, body io.Reader) error {
	ret := _m.Called(ctx, pluginVersion, plugin, filePath, body)

	if len(ret) == 0 {
		panic("no return value specified for UploadPluginVersionDocFile")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin, string, io.Reader) error); ok {
		r0 = rf(ctx, pluginVersion, plugin, filePath, body)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadPluginVersionReadme provides a mock function with given fields: ctx, pluginVersion, plugin, body
func (_m *MockRegistryStore) UploadPluginVersionReadme(ctx context.Context, pluginVersion *models.PluginVersion, plugin *models.Plugin, body io.Reader) error {
	ret := _m.Called(ctx, pluginVersion, plugin, body)

	if len(ret) == 0 {
		panic("no return value specified for UploadPluginVersionReadme")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin, io.Reader) error); ok {
		r0 = rf(ctx, pluginVersion, plugin, body)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadPluginVersionSHASums provides a mock function with given fields: ctx, pluginVersion, plugin, body
func (_m *MockRegistryStore) UploadPluginVersionSHASums(ctx context.Context, pluginVersion *models.PluginVersion, plugin *models.Plugin, body io.Reader) error {
	ret := _m.Called(ctx, pluginVersion, plugin, body)

	if len(ret) == 0 {
		panic("no return value specified for UploadPluginVersionSHASums")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin, io.Reader) error); ok {
		r0 = rf(ctx, pluginVersion, plugin, body)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UploadPluginVersionSchema provides a mock function with given fields: ctx, pluginVersion, plugin, body
func (_m *MockRegistryStore) UploadPluginVersionSchema(ctx context.Context, pluginVersion *models.PluginVersion, plugin *models.Plugin, body io.Reader) error {
	ret := _m.Called(ctx, pluginVersion, plugin, body)

	if len(ret) == 0 {
		panic("no return value specified for UploadPluginVersionSchema")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.PluginVersion, *models.Plugin, io.Reader) error); ok {
		r0 = rf(ctx, pluginVersion, plugin, body)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NewMockRegistryStore creates a new instance of MockRegistryStore. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewMockRegistryStore(t interface {
	mock.TestingT
	Cleanup(func())
}) *MockRegistryStore {
	mock := &MockRegistryStore{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
