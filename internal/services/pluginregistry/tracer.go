package pluginregistry

import "go.opentelemetry.io/otel"

var tracer = otel.Tracer("pluginregistry")
