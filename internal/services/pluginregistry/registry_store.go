package pluginregistry

//go:generate go tool mockery --name RegistryStore --inpackage --case underscore

import (
	"context"
	"fmt"
	"io"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/objectstore"
)

// RegistryStore interface encapsulates the logic for saving plugin binaries and metadata
type RegistryStore interface {
	UploadPluginPlatformBinary(
		ctx context.Context,
		pluginPlatform *models.PluginPlatform,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
		body io.Reader,
	) error
	UploadPluginVersionReadme(
		ctx context.Context,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
		body io.Reader,
	) error
	UploadPluginVersionSchema(
		ctx context.Context,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
		body io.Reader,
	) error
	UploadPluginVersionDocFile(
		ctx context.Context,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
		filePath string,
		body io.Reader,
	) error
	UploadPluginVersionSHASums(
		ctx context.Context,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
		body io.Reader,
	) error
	GetPluginVersionDocFile(
		ctx context.Context,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
		filePath string,
	) (io.ReadCloser, error)
	GetPluginVersionReadme(
		ctx context.Context,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
	) (io.ReadCloser, error)
	GetPluginPlatformBinaryPresignedURL(
		ctx context.Context,
		pluginPlatform *models.PluginPlatform,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
	) (string, error)
	GetPluginVersionSHASumsPresignedURL(
		ctx context.Context,
		pluginVersion *models.PluginVersion,
		plugin *models.Plugin,
	) (string, error)
}

type registryStore struct {
	objectStore objectstore.ObjectStore
}

// NewRegistryStore creates an instance of the RegistryStore interface
func NewRegistryStore(objectStore objectstore.ObjectStore) RegistryStore {
	return &registryStore{objectStore: objectStore}
}

func (r *registryStore) GetPluginVersionDocFile(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
	filePath string,
) (io.ReadCloser, error) {
	return r.objectStore.GetObjectStream(
		ctx,
		getPluginVersionDocFileObjectKey(filePath, pluginVersion, plugin),
		nil,
	)
}

func (r *registryStore) GetPluginVersionReadme(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
) (io.ReadCloser, error) {
	return r.objectStore.GetObjectStream(
		ctx,
		getPluginVersionReadmeObjectKey(pluginVersion, plugin),
		nil,
	)
}

func (r *registryStore) GetPluginVersionSchema(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
) (io.ReadCloser, error) {
	return r.objectStore.GetObjectStream(
		ctx,
		getPluginVersionSchemaObjectKey(pluginVersion, plugin),
		nil,
	)
}

func (r *registryStore) UploadPluginPlatformBinary(
	ctx context.Context,
	pluginPlatform *models.PluginPlatform,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
	body io.Reader,
) error {
	return r.upload(ctx, getPluginPlatformObjectKey(pluginPlatform, pluginVersion, plugin), body)
}

func (r *registryStore) UploadPluginVersionReadme(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
	body io.Reader,
) error {
	return r.upload(ctx, getPluginVersionReadmeObjectKey(pluginVersion, plugin), body)
}

func (r *registryStore) UploadPluginVersionDocFile(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
	filePath string,
	body io.Reader,
) error {
	return r.upload(ctx, getPluginVersionDocFileObjectKey(filePath, pluginVersion, plugin), body)
}

func (r *registryStore) UploadPluginVersionSchema(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
	body io.Reader,
) error {
	return r.upload(ctx, getPluginVersionSchemaObjectKey(pluginVersion, plugin), body)
}

func (r *registryStore) UploadPluginVersionSHASums(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
	body io.Reader,
) error {
	return r.upload(ctx, getPluginVersionSHASumsObjectKey(pluginVersion, plugin), body)
}

func (r *registryStore) GetPluginPlatformBinaryPresignedURL(
	ctx context.Context,
	pluginPlatform *models.PluginPlatform,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
) (string, error) {
	return r.objectStore.GetPresignedURL(ctx, getPluginPlatformObjectKey(pluginPlatform, pluginVersion, plugin))
}

func (r *registryStore) GetPluginVersionSHASumsPresignedURL(
	ctx context.Context,
	pluginVersion *models.PluginVersion,
	plugin *models.Plugin,
) (string, error) {
	return r.objectStore.GetPresignedURL(ctx, getPluginVersionSHASumsObjectKey(pluginVersion, plugin))
}

func (r *registryStore) upload(ctx context.Context, key string, body io.Reader) error {
	return r.objectStore.UploadObject(ctx, key, body)
}

func getPluginVersionDocFileObjectKey(filePath string, pluginVersion *models.PluginVersion, plugin *models.Plugin) string {
	return fmt.Sprintf("registry/plugins/%s/%s/docs/%s", plugin.Metadata.ID, pluginVersion.Metadata.ID, filePath)
}

func getPluginVersionReadmeObjectKey(pluginVersion *models.PluginVersion, plugin *models.Plugin) string {
	return fmt.Sprintf("registry/plugins/%s/%s/README", plugin.Metadata.ID, pluginVersion.Metadata.ID)
}

func getPluginVersionSchemaObjectKey(pluginVersion *models.PluginVersion, plugin *models.Plugin) string {
	return fmt.Sprintf("registry/plugins/%s/%s/schema.json", plugin.Metadata.ID, pluginVersion.Metadata.ID)
}

func getPluginVersionSHASumsObjectKey(pluginVersion *models.PluginVersion, plugin *models.Plugin) string {
	return fmt.Sprintf("registry/plugins/%s/%s/SHA256SUMS", plugin.Metadata.ID, pluginVersion.Metadata.ID)
}

func getPluginPlatformObjectKey(pluginPlatform *models.PluginPlatform, pluginVersion *models.PluginVersion, plugin *models.Plugin) string {
	return fmt.Sprintf(
		"registry/plugins/%s/%s/platforms/%s_%s/plugin-%s_%s_%s_%s.zip",
		plugin.Metadata.ID,
		pluginVersion.Metadata.ID,
		pluginPlatform.OperatingSystem,
		pluginPlatform.Architecture,
		plugin.Name,
		pluginVersion.SemanticVersion,
		pluginPlatform.OperatingSystem,
		pluginPlatform.Architecture,
	)
}
