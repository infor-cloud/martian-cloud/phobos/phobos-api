// Package metric provides functionality for collecting / computing
// metrics on different Phobos resources.
package metric

import (
	"context"
	"time"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

var (
	// Max bucket count for aggregated metrics statistics query is 45
	maxBucketCount = 45
	// Max time range for metrics statistics query is 45 days
	maxTimeRange = time.Hour * 24 * 45
)

// GetMetricsInput is the input for retrieving metrics.
type GetMetricsInput struct {
	Sort              *db.MetricSortableField
	PaginationOptions *pagination.Options
	TimeRangeStart    time.Time
	TimeRangeEnd      *time.Time
	OrganizationID    *string
	ProjectID         *string
	ReleaseID         *string
	PipelineID        *string
	EnvironmentName   *string
	Tags              map[models.MetricTagName]string
	MetricName        models.MetricName
}

// GetAggregatedMetricStatisticsInput is the input for getting aggregated statistics for a metric.
type GetAggregatedMetricStatisticsInput struct {
	OrganizationID  *string
	ProjectID       *string
	ReleaseID       *string
	PipelineID      *string
	EnvironmentName *string
	Tags            map[models.MetricTagName]string
	BucketPeriod    db.MetricBucketPeriod
	MetricName      models.MetricName
	BucketCount     int
}

// GetMetricStatisticsInput is the input for getting statistics for a metric.
type GetMetricStatisticsInput struct {
	TimeRangeStart  time.Time
	TimeRangeEnd    *time.Time
	OrganizationID  *string
	ProjectID       *string
	ReleaseID       *string
	PipelineID      *string
	EnvironmentName *string
	Tags            map[models.MetricTagName]string
	MetricName      models.MetricName
}

// Service encapsulates the logic for accessing and creating metrics.
type Service interface {
	GetMetrics(ctx context.Context, input *GetMetricsInput) (*db.MetricsResult, error)
	GetAggregatedMetricStatistics(ctx context.Context, input *GetAggregatedMetricStatisticsInput) ([]*db.AggregatedMetric, error)
	GetMetricStatistics(ctx context.Context, input *GetMetricStatisticsInput) (*db.MetricStatistics, error)
}

type service struct {
	logger   logger.Logger
	dbClient *db.Client
}

// NewService creates a new Service instance.
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
) Service {
	return &service{
		logger:   logger,
		dbClient: dbClient,
	}
}

func (s *service) GetMetrics(ctx context.Context, input *GetMetricsInput) (*db.MetricsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetMetrics")
	defer span.End()

	if err := s.requirePermission(ctx, input.OrganizationID, input.ProjectID, input.ReleaseID); err != nil {
		return nil, err
	}

	return s.dbClient.Metrics.GetMetrics(ctx, &db.GetMetricsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.MetricFilter{
			TimeRangeStart:  &input.TimeRangeStart,
			TimeRangeEnd:    input.TimeRangeEnd,
			OrganizationID:  input.OrganizationID,
			ProjectID:       input.ProjectID,
			ReleaseID:       input.ReleaseID,
			PipelineID:      input.PipelineID,
			MetricName:      &input.MetricName,
			EnvironmentName: input.EnvironmentName,
			Tags:            input.Tags,
		},
	})
}

func (s *service) GetAggregatedMetricStatistics(ctx context.Context, input *GetAggregatedMetricStatisticsInput) ([]*db.AggregatedMetric, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAggregatedMetricStatistics")
	defer span.End()

	if err := s.requirePermission(ctx, input.OrganizationID, input.ProjectID, input.ReleaseID); err != nil {
		return nil, err
	}

	if input.BucketCount < 1 {
		return nil, errors.New("bucket count must be greater than 0", errors.WithErrorCode(errors.EInvalid))
	}

	if input.BucketCount > maxBucketCount {
		return nil, errors.New("bucket count must be less than or equal to %d", maxBucketCount, errors.WithErrorCode(errors.EInvalid))
	}

	return s.dbClient.Metrics.GetAggregatedMetricStatistics(ctx, &db.GetAggregatedMetricsInput{
		BucketPeriod: input.BucketPeriod,
		BucketCount:  input.BucketCount,
		Filter: &db.MetricFilter{
			OrganizationID:  input.OrganizationID,
			ProjectID:       input.ProjectID,
			ReleaseID:       input.ReleaseID,
			PipelineID:      input.PipelineID,
			MetricName:      &input.MetricName,
			EnvironmentName: input.EnvironmentName,
			Tags:            input.Tags,
		},
	})
}

func (s *service) GetMetricStatistics(ctx context.Context, input *GetMetricStatisticsInput) (*db.MetricStatistics, error) {
	ctx, span := tracer.Start(ctx, "svc.GetMetricStatistics")
	defer span.End()

	if err := s.requirePermission(ctx, input.OrganizationID, input.ProjectID, input.ReleaseID); err != nil {
		return nil, err
	}

	// Verify that the time range is within the allowed range if a release ID is not set. The range doesn't
	// need to be bounded if a release ID is set since the number of metrics scanned will be limited by the release ID filter.
	if input.ReleaseID == nil {
		timeRangeEnd := input.TimeRangeEnd
		if timeRangeEnd == nil {
			timeRangeEnd = ptr.Time(time.Now())
		}

		if timeRangeEnd.Sub(input.TimeRangeStart) > maxTimeRange {
			return nil, errors.New("time range must be less than %s", maxTimeRange,
				errors.WithErrorCode(errors.EInvalid),
				errors.WithSpan(span),
			)
		}
	}

	return s.dbClient.Metrics.GetMetricStatistics(ctx, &db.MetricFilter{
		TimeRangeStart:  &input.TimeRangeStart,
		TimeRangeEnd:    input.TimeRangeEnd,
		OrganizationID:  input.OrganizationID,
		ProjectID:       input.ProjectID,
		ReleaseID:       input.ReleaseID,
		PipelineID:      input.PipelineID,
		MetricName:      &input.MetricName,
		EnvironmentName: input.EnvironmentName,
		Tags:            input.Tags,
	})
}

func (s *service) requirePermission(ctx context.Context, orgID *string, projectID *string, releaseID *string) error {
	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	switch {
	case orgID != nil:
		if err := caller.RequirePermission(ctx, models.ViewOrganization, auth.WithOrganizationID(*orgID)); err != nil {
			return err
		}
	case projectID != nil:
		if err := caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(*projectID)); err != nil {
			return err
		}
	case releaseID != nil:
		release, err := s.getReleaseByID(ctx, *releaseID)
		if err != nil {
			return err
		}
		if cErr := caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(release.ProjectID)); cErr != nil {
			return cErr
		}
	default:
		// Only admins can query for metrics w/o specifying an organization, project, or release ID
		if !caller.IsAdmin() {
			return errors.New("either an organization, project, or release id must be specified",
				errors.WithErrorCode(errors.EInvalid),
			)
		}
	}
	return nil
}

func (s *service) getReleaseByID(ctx context.Context, releaseID string) (*models.Release, error) {
	release, err := s.dbClient.Releases.GetReleaseByID(ctx, releaseID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release by id", errors.WithErrorCode(errors.ENotFound))
	}

	if release == nil {
		return nil, errors.New("release not found", errors.WithErrorCode(errors.ENotFound))
	}

	return release, nil
}
