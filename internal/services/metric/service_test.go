package metric

import (
	"context"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}

	expect := &service{
		logger:   logger,
		dbClient: dbClient,
	}

	assert.Equal(t, expect, NewService(logger, dbClient))
}

func TestGetMetrics(t *testing.T) {
	organizationID := "org-1"
	projectID := "project-1"
	releaseID := "release-1"
	timeRangeStart := time.Now().UTC().Add(-15 * time.Minute)

	type testCase struct {
		name            string
		input           *GetMetricsInput
		authError       error
		isAdmin         bool
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "pipeline completed metric in organization",
			input: &GetMetricsInput{
				MetricName:     models.MetricPipelineCompleted,
				OrganizationID: &organizationID,
				TimeRangeStart: timeRangeStart,
			},
		},
		{
			name: "pipeline duration in project",
			input: &GetMetricsInput{
				MetricName:     models.MetricPipelineDuration,
				ProjectID:      &projectID,
				TimeRangeStart: timeRangeStart,
			},
		},
		{
			name: "subject not authorized to view organization",
			input: &GetMetricsInput{
				MetricName:     models.MetricPipelineCompleted,
				OrganizationID: &organizationID,
				TimeRangeStart: timeRangeStart,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject not authorized to view project",
			input: &GetMetricsInput{
				MetricName:     models.MetricPipelineCompleted,
				ProjectID:      &projectID,
				TimeRangeStart: timeRangeStart,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject not authorized to view release",
			input: &GetMetricsInput{
				MetricName:     models.MetricPipelineCompleted,
				ReleaseID:      &releaseID,
				TimeRangeStart: timeRangeStart,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "no organization or project id specified for non-admin",
			input: &GetMetricsInput{
				MetricName:     models.MetricPipelineCompleted,
				TimeRangeStart: timeRangeStart,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "pipeline completed metric for all orgs",
			input: &GetMetricsInput{
				MetricName:     models.MetricPipelineCompleted,
				TimeRangeStart: timeRangeStart,
			},
			isAdmin: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockMetrics := db.NewMockMetrics(t)
			mockReleases := db.NewMockReleases(t)

			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()

			if test.input.OrganizationID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewOrganization, mock.Anything).Return(test.authError)
			}

			if test.input.ProjectID != nil || test.input.ReleaseID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).Return(test.authError)
			}

			if test.input.ReleaseID != nil {
				mockReleases.On("GetReleaseByID", mock.Anything, *test.input.ReleaseID).Return(&models.Release{
					ProjectID: projectID,
				}, nil)
			}

			if test.authError == nil && test.expectErrorCode == "" {
				mockMetrics.On("GetMetrics", mock.Anything, &db.GetMetricsInput{
					Filter: &db.MetricFilter{
						MetricName:      &test.input.MetricName,
						TimeRangeStart:  ptr.Time(test.input.TimeRangeStart),
						TimeRangeEnd:    test.input.TimeRangeEnd,
						OrganizationID:  test.input.OrganizationID,
						ProjectID:       test.input.ProjectID,
						ReleaseID:       test.input.ReleaseID,
						PipelineID:      test.input.PipelineID,
						EnvironmentName: test.input.EnvironmentName,
					},
				}).Return(&db.MetricsResult{}, nil)
			}

			dbClient := &db.Client{
				Metrics:  mockMetrics,
				Releases: mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			metrics, err := service.GetMetrics(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, metrics)
		})
	}
}

func TestGetAggregatedMetricStatistics(t *testing.T) {
	organizationID := "org-1"
	projectID := "project-1"
	releaseID := "release-1"

	type testCase struct {
		name            string
		input           *GetAggregatedMetricStatisticsInput
		authError       error
		isAdmin         bool
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "deployment time to recover in organization",
			input: &GetAggregatedMetricStatisticsInput{
				MetricName:     models.MetricDeploymentRecoveryTime,
				OrganizationID: &organizationID,
				BucketPeriod:   db.MetricBucketPeriodDay,
				BucketCount:    10,
			},
		},
		{
			name: "subject not authorized to view organization",
			input: &GetAggregatedMetricStatisticsInput{
				MetricName:     models.MetricPipelineCompleted,
				OrganizationID: &organizationID,
				BucketPeriod:   db.MetricBucketPeriodDay,
				BucketCount:    10,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject not authorized to view project",
			input: &GetAggregatedMetricStatisticsInput{
				MetricName:   models.MetricPipelineCompleted,
				ProjectID:    &projectID,
				BucketPeriod: db.MetricBucketPeriodDay,
				BucketCount:  10,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject not authorized to view release",
			input: &GetAggregatedMetricStatisticsInput{
				MetricName:   models.MetricPipelineCompleted,
				ProjectID:    &releaseID,
				BucketPeriod: db.MetricBucketPeriodDay,
				BucketCount:  10,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "no organization or project id specified for non-admin",
			input: &GetAggregatedMetricStatisticsInput{
				MetricName:   models.MetricDeploymentRecoveryTime,
				BucketPeriod: db.MetricBucketPeriodDay,
				BucketCount:  10,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "allow admin to view all metrics without specifying organization or project id",
			input: &GetAggregatedMetricStatisticsInput{
				MetricName:   models.MetricDeploymentRecoveryTime,
				BucketPeriod: db.MetricBucketPeriodDay,
				BucketCount:  10,
			},
			isAdmin: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockMetrics := db.NewMockMetrics(t)
			mockReleases := db.NewMockReleases(t)

			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()

			if test.input.OrganizationID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewOrganization, mock.Anything).Return(test.authError)
			}

			if test.input.ProjectID != nil || test.input.ReleaseID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).Return(test.authError)
			}

			if test.input.ReleaseID != nil {
				mockReleases.On("GetReleaseByID", mock.Anything, *test.input.ReleaseID).Return(&models.Release{
					ProjectID: projectID,
				}, nil)
			}

			if test.authError == nil && test.expectErrorCode == "" {
				mockMetrics.On("GetAggregatedMetricStatistics", mock.Anything, &db.GetAggregatedMetricsInput{
					BucketPeriod: test.input.BucketPeriod,
					BucketCount:  test.input.BucketCount,
					Filter: &db.MetricFilter{
						MetricName:      &test.input.MetricName,
						OrganizationID:  test.input.OrganizationID,
						ProjectID:       test.input.ProjectID,
						ReleaseID:       test.input.ReleaseID,
						PipelineID:      test.input.PipelineID,
						EnvironmentName: test.input.EnvironmentName,
					},
				}).Return([]*db.AggregatedMetric{}, nil)
			}

			dbClient := &db.Client{
				Metrics:  mockMetrics,
				Releases: mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			metrics, err := service.GetAggregatedMetricStatistics(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, metrics)
		})
	}
}

func TestGetMetricStatistics(t *testing.T) {
	organizationID := "org-1"
	projectID := "project-1"
	releaseID := "release-1"
	timeRangeStart := time.Now().UTC().Add(-15 * time.Minute)

	type testCase struct {
		name            string
		input           *GetMetricStatisticsInput
		authError       error
		isAdmin         bool
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "deployment time to recover in organization",
			input: &GetMetricStatisticsInput{
				MetricName:     models.MetricDeploymentRecoveryTime,
				OrganizationID: &organizationID,
				TimeRangeStart: timeRangeStart,
			},
		},
		{
			name: "subject not authorized to view organization",
			input: &GetMetricStatisticsInput{
				MetricName:     models.MetricPipelineCompleted,
				OrganizationID: &organizationID,
				TimeRangeStart: timeRangeStart,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject not authorized to view project",
			input: &GetMetricStatisticsInput{
				MetricName:     models.MetricPipelineCompleted,
				ProjectID:      &projectID,
				TimeRangeStart: timeRangeStart,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject not authorized to view release",
			input: &GetMetricStatisticsInput{
				MetricName:     models.MetricPipelineCompleted,
				ProjectID:      &releaseID,
				TimeRangeStart: timeRangeStart,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "no organization or project id specified for non-admin",
			input: &GetMetricStatisticsInput{
				MetricName:     models.MetricPipelineCompleted,
				TimeRangeStart: timeRangeStart,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "no organization or project id specified for admin",
			input: &GetMetricStatisticsInput{
				MetricName:     models.MetricPipelineCompleted,
				TimeRangeStart: timeRangeStart,
			},
			isAdmin: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockMetrics := db.NewMockMetrics(t)
			mockReleases := db.NewMockReleases(t)

			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()

			if test.input.OrganizationID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewOrganization, mock.Anything).Return(test.authError)
			}

			if test.input.ProjectID != nil || test.input.ReleaseID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).Return(test.authError)
			}

			if test.input.ReleaseID != nil {
				mockReleases.On("GetReleaseByID", mock.Anything, *test.input.ReleaseID).Return(&models.Release{
					ProjectID: projectID,
				}, nil)
			}

			if test.authError == nil && test.expectErrorCode == "" {
				mockMetrics.On("GetMetricStatistics", mock.Anything, &db.MetricFilter{
					MetricName:      &test.input.MetricName,
					TimeRangeStart:  ptr.Time(test.input.TimeRangeStart),
					TimeRangeEnd:    test.input.TimeRangeEnd,
					OrganizationID:  test.input.OrganizationID,
					ProjectID:       test.input.ProjectID,
					ReleaseID:       test.input.ReleaseID,
					PipelineID:      test.input.PipelineID,
					EnvironmentName: test.input.EnvironmentName,
				}).Return(&db.MetricStatistics{}, nil)
			}

			dbClient := &db.Client{
				Metrics:  mockMetrics,
				Releases: mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			metrics, err := service.GetMetricStatistics(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, metrics)
		})
	}
}
