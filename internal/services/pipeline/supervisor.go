package pipeline

import (
	"context"
	"time"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// waitingTaskPollingInterval is the interval at which the Supervisor will check for waiting tasks
const waitingTaskPollingInterval = 30 * time.Second

// readyNodePollingInterval is the interval at which the Supervisor will check for ready nodes. This is
// a fallback check in case an event is missed or the event is not processed correctly.
const readyNodePollingInterval = 5 * time.Minute

// Supervisor is responsible for managing pipelines
type Supervisor struct {
	logger          logger.Logger
	dbClient        *db.Client
	pipelineUpdater Updater
	eventManager    *events.EventManager
}

// NewSupervisor creates a new pipeline Supervisor
func NewSupervisor(pipelineUpdater Updater, dbClient *db.Client, eventManager *events.EventManager, logger logger.Logger) *Supervisor {
	return &Supervisor{
		pipelineUpdater: pipelineUpdater,
		dbClient:        dbClient,
		eventManager:    eventManager,
		logger:          logger,
	}
}

// Start starts the pipeline Supervisor
func (s *Supervisor) Start(ctx context.Context) {
	// Register event handler to ensure that nested pipeline status changes are propagated to the parent pipeline
	s.pipelineUpdater.RegisterEventHandler(s.handleNestedPipelineStatusChange)
	// Register event handler to update pipeline timestamps when a pipeline starts or completes
	s.pipelineUpdater.RegisterEventHandler(s.handlePipelineTimestampUpdate)

	// Start goroutine to listen for pipeline DB events
	go func() {
		subscription := events.Subscription{
			Type:    events.PipelineSubscription,
			Actions: []events.SubscriptionAction{events.CreateAction, events.UpdateAction},
		}

		subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})
		defer s.eventManager.Unsubscribe(subscriber)

		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) {
					s.logger.Errorf("Error occurred while waiting for pipeline events: %v", err)
				}
				return
			}

			// Must do a query here to get the full model required by runReadyNodes.
			pipeline, err := s.getPipelineByID(ctx, event.ID)
			if err != nil {
				s.logger.Errorf("Error occurred while getting pipeline by id %s: %v", event.ID, err)
				continue
			}

			if err = s.runReadyNodes(ctx, pipeline); err != nil {
				s.logger.Errorf("Error occurred while checking ready nodes for pipeline %s: %v", event.ID, err)
				continue
			}
		}
	}()

	// Start goroutine to periodically check for any ready tasks or nested pipelines. Typically ready nodes
	// will be handled by the DB event handler but in some cases an event could be missed so this will ensure
	// that the ready nodes are still processed.
	go func() {
		for {
			select {
			case <-ctx.Done():
				// Exit goroutine if context is cancelled
				return
			case <-time.After(readyNodePollingInterval):
			}

			// Get all pipelines with a task or nested pipeline in the ready state
			response, err := s.dbClient.Pipelines.GetPipelines(ctx, &db.GetPipelinesInput{
				Filter: &db.PipelineFilter{
					NodeTypes:    []statemachine.NodeType{statemachine.TaskNodeType, statemachine.PipelineNodeType},
					NodeStatuses: []statemachine.NodeStatus{statemachine.ReadyNodeStatus},
				},
			})
			if err != nil {
				s.logger.Errorf("Error occurred while getting pipelines: %v", err)
				continue
			}

			for _, pipeline := range response.Pipelines {
				pipelineCopy := pipeline
				if err = s.runReadyNodes(ctx, &pipelineCopy); err != nil {
					s.logger.Errorf("Error occurred while checking ready nodes for pipeline %s: %v", pipeline.Metadata.ID, err)
				}
			}
		}
	}()

	// Start goroutine to check if any waiting tasks are ready to run
	go func() {
		for {
			select {
			case <-ctx.Done():
				// Exit goroutine if context is cancelled
				return
			case <-time.After(waitingTaskPollingInterval):
			}

			if err := s.checkWaitingNodes(ctx); err != nil {
				s.logger.Errorf("failed to check waiting nodes: %v", err)
			}
		}
	}()
}

func (s *Supervisor) checkWaitingNodes(ctx context.Context) error {
	// Query for all pipelines with waiting tasks
	response, err := s.dbClient.Pipelines.GetPipelines(ctx, &db.GetPipelinesInput{
		Filter: &db.PipelineFilter{
			NodeTypes:    []statemachine.NodeType{statemachine.TaskNodeType, statemachine.PipelineNodeType},
			NodeStatuses: []statemachine.NodeStatus{statemachine.WaitingNodeStatus},
		},
	})
	if err != nil {
		return err
	}

	now := time.Now()

	for _, pipeline := range response.Pipelines {
		for _, stage := range pipeline.Stages {
			// Check if any nested pipelines are ready to run
			for _, nestedPipeline := range stage.NestedPipelines {
				if nestedPipeline.Status == statemachine.WaitingNodeStatus &&
					nestedPipeline.ScheduledStartTime != nil &&
					now.After(*nestedPipeline.ScheduledStartTime) {
					if _, err := s.pipelineUpdater.ProcessEvent(ctx, pipeline.Metadata.ID, &RunNestedPipelineEvent{
						NestedPipelinePath: nestedPipeline.Path,
					}); errors.ErrorCode(err) == errors.EInternal {
						// Only log error if it's an internal error
						s.logger.Errorf("failed to run nested pipeline %s: %w", nestedPipeline.Path, err)
					}
				}
			}
			// Check if any tasks are ready to run
			for _, task := range stage.Tasks {
				// Task is in the waiting state so it may either be waiting for a scheduled start time or for
				// the task interval to pass if the max attempts is greater than one
				if task.Status == statemachine.WaitingNodeStatus {
					if task.ScheduledStartTime != nil && task.AttemptCount == 0 {
						// Task is waiting for a scheduled start time
						if now.After(*task.ScheduledStartTime) {
							if _, err := s.pipelineUpdater.ProcessEvent(ctx, pipeline.Metadata.ID, &RunTaskEvent{
								TaskPath: task.Path,
							}); errors.ErrorCode(err) == errors.EInternal {
								// Only log error if it's an internal error
								s.logger.Errorf("failed to run task %s: %w", task.Path, err)
							}
						}
					} else {
						// This should never happen but log an error if it does
						if task.MaxAttempts < 1 {
							s.logger.Errorf("task %s has max attempts less than one but is in a waiting state", task.Path)
							continue
						}

						// This should never happen but log an error if it does
						if task.LastAttemptFinishedAt == nil {
							s.logger.Errorf("task %s is in waiting state but has no last attempt at time", task.Path)
							continue
						}

						if time.Since(*task.LastAttemptFinishedAt).Seconds() > task.Interval.Seconds() {
							if _, err := s.pipelineUpdater.ProcessEvent(ctx, pipeline.Metadata.ID, &RunTaskEvent{
								TaskPath: task.Path,
							}); errors.ErrorCode(err) == errors.EInternal {
								// Only log error if it's an internal error
								s.logger.Errorf("failed to run task %s: %w", task.Path, err)
							}
						}
					}
				}
			}
		}
	}
	return nil
}

func (s *Supervisor) getPipelineByID(ctx context.Context, id string) (*models.Pipeline, error) {
	pipeline, err := s.dbClient.Pipelines.GetPipelineByID(ctx, id)
	if err != nil {
		return nil, err
	}

	if pipeline == nil {
		return nil, errors.New(
			"pipeline with id %s not found", id,
			errors.WithErrorCode(errors.ENotFound))
	}

	return pipeline, nil
}

func (s *Supervisor) runReadyNodes(ctx context.Context, pipeline *models.Pipeline) error {
	// Get all tasks that are in the ready state
	targetStatus := statemachine.ReadyNodeStatus
	readyNodes, err := pipeline.BuildStateMachine().GetNodes(&statemachine.GetNodesInput{
		Status:    &targetStatus,
		NodeTypes: []statemachine.NodeType{statemachine.TaskNodeType, statemachine.PipelineNodeType},
	})
	if err != nil {
		return err
	}

	nestedPipelines := []statemachine.Node{}
	tasks := []statemachine.Node{}
	for _, n := range readyNodes {
		path := n.Path()

		switch n.Type() {
		case statemachine.PipelineNodeType:
			if n.Parent() != nil {
				nestedPipeline, ok := pipeline.GetNestedPipeline(path)
				if !ok {
					return errors.New("nested pipeline with path %s not found", path)
				}

				np, err := s.getPipelineByID(ctx, nestedPipeline.LatestPipelineID)
				if err != nil {
					return err
				}

				if np.When != models.ManualPipeline {
					nestedPipelines = append(nestedPipelines, n)
				}
			}
		case statemachine.TaskNodeType:
			task, ok := pipeline.GetTask(path)
			if !ok {
				return errors.New("task with path %s not found", path)
			}
			if task.When != models.ManualPipelineTask {
				tasks = append(tasks, n)
			}
		}
	}

	for _, n := range tasks {
		path := n.Path()

		if _, err := s.pipelineUpdater.ProcessEvent(ctx, pipeline.Metadata.ID, &RunTaskEvent{
			TaskPath: path,
		}); err != nil {
			if errors.ErrorCode(err) == errors.EOptimisticLock {
				continue
			}
			s.logger.Errorf("failed to process ready task: pipeline=%s task=%s: %v", pipeline.Metadata.ID, path, err)
		}
	}

	for _, n := range nestedPipelines {
		path := n.Path()

		if _, err := s.pipelineUpdater.ProcessEvent(ctx, pipeline.Metadata.ID, &RunNestedPipelineEvent{
			NestedPipelinePath: path,
		}); err != nil {
			if errors.ErrorCode(err) == errors.EOptimisticLock {
				continue
			}
			s.logger.Errorf("failed to process ready nested pipeline: parentPipeline=%s nestedPipeline=%s: %v", pipeline.Metadata.ID, path, err)
		}
	}

	return nil
}

func (s *Supervisor) handlePipelineTimestampUpdate(ctx context.Context, _ string, changes []StatusChange, _ any) error {
	for _, changedPipeline := range changes {
		for _, change := range changedPipeline.NodeStatusChanges {
			// We only want to update the pipeline timestamps if the root pipeline has completed
			if change.NodeType == statemachine.PipelineNodeType && change.NodePath == "pipeline" {
				switch change.NewStatus {
				case statemachine.SucceededNodeStatus, statemachine.FailedNodeStatus, statemachine.CanceledNodeStatus:
					_, err := s.pipelineUpdater.ProcessEvent(ctx, changedPipeline.PipelineID, &UpdatePipelineTimestampEvent{
						CompletedTimestamp: ptr.Time(time.Now().UTC()),
					})
					return err
				case statemachine.RunningNodeStatus:
					_, err := s.pipelineUpdater.ProcessEvent(ctx, changedPipeline.PipelineID, &UpdatePipelineTimestampEvent{
						StartedTimestamp: ptr.Time(time.Now().UTC()),
					})
					return err
				}
			}
		}
	}
	return nil
}

// handleNestedPipelineStatusChange handles a nested pipeline status change event by updating the parent pipeline
// to match the nested pipeline status
func (s *Supervisor) handleNestedPipelineStatusChange(ctx context.Context, pipelineID string, _ []StatusChange, _ any) error {
	pipeline, err := s.getPipelineByID(ctx, pipelineID)
	if err != nil {
		return err
	}

	// Check if this pipeline has any nested pipeline nodes that were skipped or reset
	for _, stage := range pipeline.Stages {
		for _, nestedPipelineNode := range stage.NestedPipelines {
			switch nestedPipelineNode.Status {
			case statemachine.SkippedNodeStatus,
				statemachine.FailedNodeStatus,
				statemachine.CreatedNodeStatus,
				statemachine.ReadyNodeStatus,
				statemachine.WaitingNodeStatus,
				statemachine.ApprovalPendingNodeStatus,
				statemachine.DeferredNodeStatus:
				// Get the actual nested pipeline to check if it has been updated
				nestedPipeline, err := s.getPipelineByID(ctx, nestedPipelineNode.LatestPipelineID)
				if err != nil {
					return err
				}

				if !nestedPipeline.Status.Equals(nestedPipelineNode.Status) {
					if _, err = s.pipelineUpdater.ProcessEvent(ctx, nestedPipeline.Metadata.ID, &ForceNestedPipelineStatusChangeEvent{
						Status: nestedPipelineNode.Status,
					}); err != nil {
						return err
					}
				}
			}
		}
	}

	// Check if this pipeline has a parent that needs to be updated
	if pipeline.ParentPipelineID != nil {
		// Get parent pipeline
		parentPipeline, err := s.getPipelineByID(ctx, *pipeline.ParentPipelineID)
		if err != nil {
			return err
		}

		// Check if nested pipeline node status needs to be updated
		for _, stage := range parentPipeline.Stages {
			for _, nestedPipeline := range stage.NestedPipelines {
				if nestedPipeline.LatestPipelineID == pipeline.Metadata.ID {
					if nestedPipeline.Status != pipeline.Status {
						// Update parent pipeline to match nested pipeline status
						if _, err = s.pipelineUpdater.ProcessEvent(ctx, parentPipeline.Metadata.ID, &NestedPipelineStatusChangeEvent{
							NestedPipelinePath: nestedPipeline.Path,
							Status:             pipeline.Status,
						}); err != nil {
							return err
						}
					}
					return nil
				}
			}
		}
	}

	return nil
}
