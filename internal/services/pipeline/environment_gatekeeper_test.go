package pipeline

import (
	context "context"
	"testing"

	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	db "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	models "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

func TestNewChecker(t *testing.T) {
	dbClient := &db.Client{}

	expect := &environmentGatekeeper{
		dbClient: dbClient,
	}

	assert.Equal(t, expect, NewEnvironmentGatekeeper(dbClient))
}

func TestCheckEnvironmentRules(t *testing.T) {
	username := "username-1"
	userID := "user-id-1"
	otherUserID := "other-user-id"
	serviceAccountID := "service-account-1-id"
	otherServiceAccountID := "other-service-account-1-id"
	serviceAccountPRN := "service-account-1-prn"
	environmentName := "somewhere"
	organizationID := "organization-1"
	projectID := "project-1"
	teamName := "team-name-1"
	teamID := "team-id-1"
	roleName := "role-name-1"
	roleID := "role-id-1"
	roleDescription := "role-description-1"

	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID:  projectID,
			PRN: "prn-part-0:prn-part-1:org1/proj1", // to keep GetOrgName from hitting an index error
		},
		OrgID: organizationID,
	}

	userCaller := auth.UserCaller{
		User: &models.User{
			Metadata: models.ResourceMetadata{
				ID: userID,
			},
			Username: username,
		},
	}

	serviceAccountCaller := auth.ServiceAccountCaller{
		ServiceAccountID:  serviceAccountID,
		ServiceAccountPRN: serviceAccountPRN,
	}

	type testCase struct {
		name            string
		caller          auth.Caller
		userTeams       []models.Team
		allRules        []*models.EnvironmentRule
		allMemberships  []models.Membership
		roleMap         map[string]models.Role
		failsOrgCheck   bool
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{

		// cases with no rules
		{
			name:            "caller other than user or service account",
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:   "no rules, any user is allowed",
			caller: &userCaller,
		},
		{
			name:   "no rules, any service account is allowed",
			caller: &serviceAccountCaller,
		},

		// cases with an organization rule
		{
			name:   "org rule, user caller is not allowed",
			caller: &userCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.OrganizationScope,
					OrgID:           organizationID,
					EnvironmentName: environmentName,
					UserIDs:         []string{otherUserID},
				},
			},
			failsOrgCheck:   true,
			expectErrorCode: errors.EForbidden,
		},
		{
			name:   "org rule, user caller is allowed directly",
			caller: &userCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.OrganizationScope,
					OrgID:           organizationID,
					EnvironmentName: environmentName,
					UserIDs:         []string{userID},
				},
			},
		},
		{
			name:   "org rule, user caller is allowed through a team",
			caller: &userCaller,
			userTeams: []models.Team{
				{
					Metadata: models.ResourceMetadata{
						ID: teamID,
					},
					Name: teamName,
				},
			},
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.OrganizationScope,
					OrgID:           organizationID,
					EnvironmentName: environmentName,
					TeamIDs:         []string{teamID},
				},
			},
		},
		{
			name:   "org rule, user caller is allowed through a role",
			caller: &userCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.OrganizationScope,
					OrgID:           organizationID,
					EnvironmentName: environmentName,
					RoleIDs:         []string{roleID},
				},
			},
			allMemberships: []models.Membership{
				{
					UserID:         &userID,
					OrganizationID: &organizationID,
					RoleID:         roleID,
				},
			},
			roleMap: map[string]models.Role{
				roleID: {
					Metadata: models.ResourceMetadata{
						ID: roleID,
					},
					Name:        roleName,
					Description: roleDescription,
				},
			},
		},
		{
			name:   "org rule, user caller, user's team is allowed through a role",
			caller: &userCaller,
			userTeams: []models.Team{
				{
					Metadata: models.ResourceMetadata{
						ID: teamID,
					},
					Name: teamName,
				},
			},
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.OrganizationScope,
					OrgID:           organizationID,
					EnvironmentName: environmentName,
					RoleIDs:         []string{roleID},
				},
			},
			allMemberships: []models.Membership{
				{
					TeamID:         &teamID,
					OrganizationID: &organizationID,
					RoleID:         roleID,
				},
			},
			roleMap: map[string]models.Role{
				roleID: {
					Metadata: models.ResourceMetadata{
						ID: roleID,
					},
					Name:        roleName,
					Description: roleDescription,
				},
			},
		},
		{
			name:   "org rule, service account caller is not allowed",
			caller: &serviceAccountCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.OrganizationScope,
					OrgID:           organizationID,
					EnvironmentName: environmentName,
					UserIDs:         []string{otherServiceAccountID},
				},
			},
			failsOrgCheck:   true,
			expectErrorCode: errors.EForbidden,
		},
		{
			name:   "org rule, service account caller is allowed directly",
			caller: &serviceAccountCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:             models.OrganizationScope,
					OrgID:             organizationID,
					EnvironmentName:   environmentName,
					ServiceAccountIDs: []string{serviceAccountID},
				},
			},
		},
		{
			name:   "org rule, service account caller is allowed through a role",
			caller: &serviceAccountCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.OrganizationScope,
					OrgID:           organizationID,
					EnvironmentName: environmentName,
					RoleIDs:         []string{roleID},
				},
			},
			allMemberships: []models.Membership{
				{
					ServiceAccountID: &serviceAccountID,
					OrganizationID:   &organizationID,
					RoleID:           roleID,
				},
			},
			roleMap: map[string]models.Role{
				roleID: {
					Metadata: models.ResourceMetadata{
						ID: roleID,
					},
					Name:        roleName,
					Description: roleDescription,
				},
			},
		},

		// cases with a project rule
		{
			name:   "project rule, user caller is not allowed",
			caller: &userCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.ProjectScope,
					OrgID:           organizationID,
					ProjectID:       &projectID,
					EnvironmentName: environmentName,
					UserIDs:         []string{otherUserID},
				},
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:   "project rule, user caller is allowed directly",
			caller: &userCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.ProjectScope,
					OrgID:           organizationID,
					ProjectID:       &projectID,
					EnvironmentName: environmentName,
					UserIDs:         []string{userID},
				},
			},
		},
		{
			name:   "project rule, user caller is allowed through a team",
			caller: &userCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.ProjectScope,
					OrgID:           organizationID,
					ProjectID:       &projectID,
					EnvironmentName: environmentName,
					RoleIDs:         []string{roleID},
				},
			},
			allMemberships: []models.Membership{
				{
					UserID:         &userID,
					OrganizationID: &organizationID,
					RoleID:         roleID,
				},
			},
			roleMap: map[string]models.Role{
				roleID: {
					Metadata: models.ResourceMetadata{
						ID: roleID,
					},
					Name:        roleName,
					Description: roleDescription,
				},
			},
		},
		{
			name:   "project rule, user caller is allowed through a role",
			caller: &userCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.ProjectScope,
					OrgID:           organizationID,
					ProjectID:       &projectID,
					EnvironmentName: environmentName,
					RoleIDs:         []string{roleID},
				},
			},
			allMemberships: []models.Membership{
				{
					UserID:         &userID,
					OrganizationID: &organizationID,
					RoleID:         roleID,
				},
			},
			roleMap: map[string]models.Role{
				roleID: {
					Metadata: models.ResourceMetadata{
						ID: roleID,
					},
					Name:        roleName,
					Description: roleDescription,
				},
			},
		},
		{
			name:   "project rule, user caller, user's team is allowed through a role",
			caller: &userCaller,
			userTeams: []models.Team{
				{
					Metadata: models.ResourceMetadata{
						ID: teamID,
					},
					Name: teamName,
				},
			},
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.ProjectScope,
					OrgID:           organizationID,
					ProjectID:       &projectID,
					EnvironmentName: environmentName,
					RoleIDs:         []string{roleID},
				},
			},
			allMemberships: []models.Membership{
				{
					TeamID:         &teamID,
					OrganizationID: &organizationID,
					RoleID:         roleID,
				},
			},
			roleMap: map[string]models.Role{
				roleID: {
					Metadata: models.ResourceMetadata{
						ID: roleID,
					},
					Name:        roleName,
					Description: roleDescription,
				},
			},
		},
		{
			name:   "project rule, service account caller is not allowed",
			caller: &serviceAccountCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:             models.ProjectScope,
					OrgID:             organizationID,
					ProjectID:         &projectID,
					EnvironmentName:   environmentName,
					ServiceAccountIDs: []string{otherServiceAccountID},
				},
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:   "project rule, service account caller is allowed directly",
			caller: &serviceAccountCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:             models.OrganizationScope,
					OrgID:             organizationID,
					ProjectID:         &projectID,
					EnvironmentName:   environmentName,
					ServiceAccountIDs: []string{serviceAccountID},
				},
			},
		},
		{
			name:   "project rule, service account caller is allowed through a role",
			caller: &serviceAccountCaller,
			allRules: []*models.EnvironmentRule{
				{
					Scope:           models.ProjectScope,
					OrgID:           organizationID,
					ProjectID:       &projectID,
					EnvironmentName: environmentName,
					RoleIDs:         []string{roleID},
				},
			},
			allMemberships: []models.Membership{
				{
					ServiceAccountID: &serviceAccountID,
					OrganizationID:   &organizationID,
					RoleID:           roleID,
				},
			},
			roleMap: map[string]models.Role{
				roleID: {
					Metadata: models.ResourceMetadata{
						ID: roleID,
					},
					Name:        roleName,
					Description: roleDescription,
				},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)
			mockMemberships := db.NewMockMemberships(t)
			mockTeams := db.NewMockTeams(t)

			mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(project, nil).Maybe()

			switch test.caller.(type) {
			case *auth.ServiceAccountCaller:
				mockServiceAccounts.On("GetServiceAccountByPRN", mock.Anything, serviceAccountPRN).
					Return(&models.ServiceAccount{
						Metadata: models.ResourceMetadata{
							ID: serviceAccountID,
						},
					}, nil)
			}

			if test.caller != nil {
				mockEnvironmentRules.On("GetEnvironmentRules", mock.Anything, &db.GetEnvironmentRulesInput{
					Filter: &db.EnvironmentRuleFilter{
						OrgID:                 &organizationID,
						ProjectID:             &projectID,
						EnvironmentName:       &environmentName,
						EnvironmentRuleScopes: []models.ScopeType{models.OrganizationScope, models.ProjectScope},
					},
				}).
					Return(&db.EnvironmentRulesResult{
						EnvironmentRules: test.allRules,
					}, nil)
			}

			var userCallerID, serviceAccountCallerID *string
			switch test.caller.(type) {
			case *auth.UserCaller:
				userCallerID = &test.caller.(*auth.UserCaller).User.Metadata.ID
			case *auth.ServiceAccountCaller:
				serviceAccountCallerID = &test.caller.(*auth.ServiceAccountCaller).ServiceAccountID
			}

			mockMemberships.On("GetMemberships", mock.Anything, &db.GetMembershipsInput{
				Filter: &db.MembershipFilter{
					ProjectID:        &projectID,
					UserID:           userCallerID,
					ServiceAccountID: serviceAccountCallerID,
					MembershipScopes: []models.ScopeType{models.OrganizationScope, models.ProjectScope},
				},
			}).Return(&db.MembershipsResult{
				Memberships: test.allMemberships,
			}, nil).Maybe()

			mockTeams.On("GetTeams", mock.Anything, &db.GetTeamsInput{
				Filter: &db.TeamFilter{
					UserID: &userID,
				},
			}).
				Return(&db.TeamsResult{
					Teams: test.userTeams,
				}, nil).Maybe()

			dbClient := &db.Client{
				Projects:         mockProjects,
				ServiceAccounts:  mockServiceAccounts,
				EnvironmentRules: mockEnvironmentRules,
				Memberships:      mockMemberships,
				Teams:            mockTeams,
			}

			checker := environmentGatekeeper{
				dbClient: dbClient,
			}

			err := checker.checkEnvironmentRules(auth.WithCaller(ctx, test.caller), projectID, environmentName)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
