package pipeline

import (
	"context"
	"fmt"
	"slices"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/cronexpr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// mockEventHandler is used for testing event handlers.
type mockEventHandler struct {
	mock.Mock
}

// HandleEvent is a mock implementation of an EventHandler.
func (m *mockEventHandler) HandleEvent(ctx context.Context, pipelineID string, changes []StatusChange, event any) error {
	args := m.Called(ctx, pipelineID, changes, event)
	return args.Error(0)
}

func TestNewUpdater(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}

	expected := &updater{
		dbClient: dbClient,
		logger:   logger,
	}

	assert.Equal(t, expected, NewUpdater(dbClient, logger))
}

func TestProcessEvent(t *testing.T) {
	pipelineID := "pipeline-id"

	type testCase struct {
		name            string
		event           any
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:  "valid event",
			event: &RunPipelineEvent{},
		},
		{
			name:            "event returns an error",
			event:           &RetryTaskEvent{},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "invalid event",
			event:           &struct{}{},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockEventHandler := &mockEventHandler{}

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Twice()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Twice()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				Status: statemachine.CreatedNodeStatus,
			}

			// Mock handler
			mockEventHandler.On("HandleEvent",
				mock.Anything,
				pipelineID,
				mock.MatchedBy(func(sc []StatusChange) bool {
					// Make sure there's at least one status change.
					return len(sc) > 0
				}),
				tc.event,
			).Return(nil)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(samplePipeline, nil)
			mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(samplePipeline, nil).Maybe()

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()

			updater := &updater{
				dbClient: dbClient,
				eventHandlers: []EventHandler{
					mockEventHandler.HandleEvent,
				},
				logger: logger,
			}

			pipeline, err := updater.ProcessEvent(ctx, pipelineID, tc.event)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, pipeline)

			// Ensure any event handlers are called
			mockEventHandler.AssertExpectations(t)
		})
	}
}

func TestRegisterEventHandler(t *testing.T) {
	updater := &updater{
		eventHandlers: []EventHandler{},
	}

	updater.RegisterEventHandler(func(_ context.Context, _ string, _ []StatusChange, _ any) error {
		return nil
	})

	if len(updater.eventHandlers) != 1 {
		t.Errorf("Expected 1 event handler, got %d", len(updater.eventHandlers))
	}
}

func TestUpdater_runPipeline(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mockPipelines := db.NewMockPipelines(t)

	expectPipeline := &models.Pipeline{
		Status: statemachine.RunningNodeStatus,
	}

	mockPipelines.On("UpdatePipeline", mock.Anything, expectPipeline).Return(expectPipeline, nil)

	dbClient := &db.Client{
		Pipelines: mockPipelines,
	}

	updater := &updater{
		dbClient: dbClient,
	}

	updatedPipeline, changes, err := updater.runPipeline(ctx, &models.Pipeline{
		Status: statemachine.CreatedNodeStatus,
	})

	require.NoError(t, err)
	require.NotNil(t, updatedPipeline)
	assert.Equal(t, statemachine.RunningNodeStatus, updatedPipeline.Status)
	assert.Len(t, changes, 1)
}

func TestUpdater_forceNestedPipelineStatus(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mockPipelines := db.NewMockPipelines(t)

	expectPipeline := &models.Pipeline{
		Status: statemachine.SkippedNodeStatus,
	}

	mockPipelines.On("UpdatePipeline", mock.Anything, expectPipeline).Return(expectPipeline, nil)

	dbClient := &db.Client{
		Pipelines: mockPipelines,
	}

	updater := &updater{
		dbClient: dbClient,
	}

	updatedPipeline, changes, err := updater.forceNestedPipelineStatus(ctx, &models.Pipeline{
		Status: statemachine.RunningNodeStatus,
	}, &ForceNestedPipelineStatusChangeEvent{
		Status: statemachine.SkippedNodeStatus,
	})

	require.NoError(t, err)
	require.NotNil(t, updatedPipeline)
	assert.Equal(t, statemachine.SkippedNodeStatus, updatedPipeline.Status)
	assert.Len(t, changes, 1)
}

func TestUpdater_setNestedPipelineStatus(t *testing.T) {
	nestedPipelineID := "nested-pipeline-id"
	nestedPipelinePath := "pipeline.stage.s1.pipeline.n1"

	type testCase struct {
		event           *NestedPipelineStatusChangeEvent
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "should update nested pipeline status to running",
			event: &NestedPipelineStatusChangeEvent{
				NestedPipelinePath: nestedPipelinePath,
				Status:             statemachine.RunningNodeStatus,
			},
		},
		{
			name: "nested pipeline status is already updated",
			event: &NestedPipelineStatusChangeEvent{
				NestedPipelinePath: nestedPipelinePath,
				Status:             statemachine.ReadyNodeStatus,
			},
		},
		{
			name: "deployment pipeline succeeded so deployment should be updated",
			event: &NestedPipelineStatusChangeEvent{
				NestedPipelinePath: nestedPipelinePath,
				Status:             statemachine.SucceededNodeStatus,
			},
		},
		{
			name: "nested pipeline node does not exist",
			event: &NestedPipelineStatusChangeEvent{
				NestedPipelinePath: "pipeline.stage.s1.pipeline.non-existent",
				Status:             statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "parent-pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "n1",
								LatestPipelineID: nestedPipelineID,
								Path:             nestedPipelinePath,
								Status:           statemachine.ReadyNodeStatus,
							},
						},
					},
				},
			}

			mockPipelines.On("UpdatePipeline", mock.Anything, samplePipeline).Return(samplePipeline, nil).Maybe()

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.setNestedPipelineStatus(ctx, samplePipeline, tc.event)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			require.Equal(t, tc.event.Status, updatedPipeline.Stages[0].NestedPipelines[0].Status)

			if tc.event.Status.Equals(statemachine.ReadyNodeStatus) {
				// Ensure pipeline is not updated since status matches.
				mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 0)
				assert.Empty(t, changes)
				return
			}
		})
	}
}

func TestUpdater_runNestedPipeline(t *testing.T) {
	validNestedPipelinePath := "pipeline.stage.s1.pipeline.n1"
	nestedPipelineID := "nested-pipeline-id"

	type testCase struct {
		expectErrorCode    errors.CodeType
		name               string
		nestedPipelinePath string
		initialStatus      statemachine.NodeStatus
	}

	testCases := []testCase{
		{
			name:               "should start nested pipeline",
			nestedPipelinePath: validNestedPipelinePath,
			initialStatus:      statemachine.ReadyNodeStatus,
		},
		{
			name:               "should start deployment pipeline",
			nestedPipelinePath: validNestedPipelinePath,
			initialStatus:      statemachine.ReadyNodeStatus,
		},
		{
			name:               "nested pipeline node does not exist",
			nestedPipelinePath: "pipeline.stage.s1.pipeline.non-existent",
			expectErrorCode:    errors.EInternal,
		},
		{
			name:               "nested pipeline is already updated",
			nestedPipelinePath: validNestedPipelinePath,
			initialStatus:      statemachine.RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			parentPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "parent-pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "n1",
								LatestPipelineID: "nested-pipeline-id",
								Path:             "pipeline.stage.s1.pipeline.n1",
								Status:           tc.initialStatus,
							},
						},
					},
				},
			}

			if tc.nestedPipelinePath == validNestedPipelinePath && tc.initialStatus.Equals(statemachine.ReadyNodeStatus) {
				mockPipelines.On("UpdatePipeline", mock.Anything, parentPipeline).Return(parentPipeline, nil).Once()

				nestedPipeline := &models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: nestedPipelineID,
					},
					ParentPipelineID:       &parentPipeline.Metadata.ID,
					ParentPipelineNodePath: ptr.String("pipeline.stage.s1.pipeline.n1"),
					Type:                   models.NestedPipelineType,
					Status:                 tc.initialStatus,
					Stages: []*models.PipelineStage{
						{
							Status: statemachine.CreatedNodeStatus,
							Path:   "pipeline.stage.s3",
							Name:   "s3",
							Tasks: []*models.PipelineTask{
								{
									Status: statemachine.CreatedNodeStatus,
									Path:   "pipeline.stage.s3.task.t4",
									Name:   "t4",
								},
							},
						},
					},
				}

				mockPipelines.On("GetPipelineByID", mock.Anything, nestedPipelineID).Return(nestedPipeline, nil)

				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(p *models.Pipeline) bool {
					return p.Status == statemachine.RunningNodeStatus
				})).Return(nestedPipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.runNestedPipeline(ctx, parentPipeline, &RunNestedPipelineEvent{
				NestedPipelinePath: tc.nestedPipelinePath,
			})

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.Equal(t, statemachine.RunningNodeStatus, updatedPipeline.Stages[0].NestedPipelines[0].Status)

			if !tc.initialStatus.Equals(statemachine.ReadyNodeStatus) {
				// Ensure pipeline is not updated when status already matches.
				mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 0)
				return
			}

			// Ensure parent and nested pipelines are updated
			mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 2)
			assert.Len(t, changes, 2)
		})
	}
}

func TestUpdater_updateNestedPipeline(t *testing.T) {
	origNestedPipelineID := "nested-pipeline-1"
	newNestedPipelineID := "nested-pipeline-2"
	environmentName := "account1"
	validNestedPipelinePath := "pipeline.stage.s1.pipeline.n1"

	type testCase struct {
		expectErrorCode            errors.CodeType
		name                       string
		nestedPipelinePath         string
		nestedPipelineStatus       statemachine.NodeStatus
		currentApprovalStatus      models.PipelineApprovalStatus
		parentPipelineType         models.PipelineType
		expectNestedPipelineStatus statemachine.NodeStatus
	}

	testCases := []testCase{
		{
			name:                       "previously failed deployment pipeline does not require approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.FailedNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalNotRequired,
		},
		{
			name:                       "previously succeeded deployment pipeline does not require approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.FailedNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalNotRequired,
		},
		{
			name:                       "previously canceled deployment pipeline does not require approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.FailedNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalNotRequired,
		},
		{
			name:                       "previously ready deployment pipeline does not require approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.ReadyNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalNotRequired,
		},
		{
			name:                       "previously succeeded deployment pipeline requires approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.SucceededNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalPending,
		},
		{
			name:                       "previously failed deployment pipeline requires approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.FailedNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalPending,
		},
		{
			name:                       "previously canceled deployment pipeline requires approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.CanceledNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalPending,
		},
		{
			name:                       "previously ready deployment pipeline requires approvals, should mark as initializing",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.ReadyNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.InitializingNodeStatus,
			currentApprovalStatus:      models.ApprovalPending,
		},
		{
			name:                       "deployment in created state, should keep as-is",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.CreatedNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.CreatedNodeStatus,
			currentApprovalStatus:      models.ApprovalNotRequired,
		},
		{
			name:                       "deployment in skipped state, should keep as-is",
			nestedPipelinePath:         validNestedPipelinePath,
			nestedPipelineStatus:       statemachine.SkippedNodeStatus,
			parentPipelineType:         models.ReleaseLifecyclePipelineType,
			expectNestedPipelineStatus: statemachine.SkippedNodeStatus,
			currentApprovalStatus:      models.ApprovalNotRequired,
		},
		{
			name:                 "nested pipeline cannot be updated due to its status",
			nestedPipelinePath:   validNestedPipelinePath,
			nestedPipelineStatus: statemachine.RunningNodeStatus,
			expectErrorCode:      errors.EInvalid,
		},
		{
			name:                 "nested pipeline does not exist",
			nestedPipelinePath:   "pipeline.stage.s1.pipeline.non-existent",
			nestedPipelineStatus: statemachine.ReadyNodeStatus,
			expectErrorCode:      errors.ENotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockEnvironments := db.NewMockEnvironments(t)

			parentPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "parent-pipeline-id",
				},
				Type:      tc.parentPipelineType,
				ReleaseID: ptr.String("release-id"),
				ProjectID: "project-id",
				Status:    statemachine.FailedNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.FailedNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "n1",
								LatestPipelineID: origNestedPipelineID,
								Path:             "pipeline.stage.s1.pipeline.n1",
								Status:           tc.nestedPipelineStatus,
								EnvironmentName:  &environmentName,
								ApprovalStatus:   tc.currentApprovalStatus,
							},
						},
					},
				},
			}

			nestedPipelinePath := tc.nestedPipelinePath

			newNestedPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: newNestedPipelineID,
				},
				ParentPipelineID:       &parentPipeline.Metadata.ID,
				ParentPipelineNodePath: &nestedPipelinePath,
				ReleaseID:              parentPipeline.ReleaseID,
				ProjectID:              parentPipeline.ProjectID,
				Status:                 statemachine.CreatedNodeStatus,
				ApprovalStatus:         tc.currentApprovalStatus,
				Type:                   models.DeploymentPipelineType,
			}

			if tc.currentApprovalStatus == models.ApprovalPending {
				newNestedPipeline.ApprovalRuleIDs = []string{"approval-rule-1", "approval-rule-2"}
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, newNestedPipelineID).Return(newNestedPipeline, nil).Maybe()

			if tc.expectErrorCode == "" {
				origNestedPipeline := &models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: origNestedPipelineID,
					},
					Type:                   models.DeploymentPipelineType,
					Status:                 tc.nestedPipelineStatus,
					ReleaseID:              parentPipeline.ReleaseID,
					ParentPipelineID:       &parentPipeline.Metadata.ID,
					ParentPipelineNodePath: &nestedPipelinePath,
					ProjectID:              parentPipeline.ProjectID,
					When:                   models.AutoPipeline,
				}

				mockPipelines.On("GetPipelineByID", mock.Anything, origNestedPipelineID).Return(origNestedPipeline, nil).Once()

				if tc.currentApprovalStatus != models.ApprovalNotRequired {
					// This is for the new nested pipeline approval rules / status being reset.
					mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(pipeline *models.Pipeline) bool {
						return pipeline.Metadata.ID == newNestedPipelineID &&
							pipeline.ApprovalStatus == models.ApprovalNotRequired &&
							len(pipeline.ApprovalRuleIDs) == 0
					})).Return(newNestedPipeline, nil).Once()
				}

				// This is the update for the original nested pipeline that will be superseded.
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(pipeline *models.Pipeline) bool {
					return pipeline.Metadata.ID == origNestedPipelineID && pipeline.Superseded
				})).Return(origNestedPipeline, nil).Once()

				// This is the first update for the parent pipeline which sets the nested pipeline to ready
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(pipeline *models.Pipeline) bool {
					if pipeline.Metadata.ID == parentPipeline.Metadata.ID {
						nestedNode, ok := pipeline.GetNestedPipeline(tc.nestedPipelinePath)
						if !ok {
							return false
						}
						return nestedNode.LatestPipelineID == newNestedPipelineID &&
							nestedNode.Status == tc.expectNestedPipelineStatus &&
							nestedNode.ApprovalStatus == models.ApprovalNotRequired
					}
					return false
				})).Return(parentPipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Environments: mockEnvironments,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.updateNestedPipeline(ctx, parentPipeline, &UpdateNestedPipelineEvent{
				NestedPipelinePath: tc.nestedPipelinePath,
				NestedPipelineID:   newNestedPipelineID,
			})

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.Len(t, changes, 1)

			// Verify that the nested pipeline was updated
			assert.Equal(t, tc.expectNestedPipelineStatus, updatedPipeline.Stages[0].NestedPipelines[0].Status)
		})
	}
}

func TestUpdater_retryNestedPipeline(t *testing.T) {
	origNestedPipelineID := "nested-pipeline-1"
	newNestedPipelineID := "nested-pipeline-2"
	environmentName := "account1"

	type testCase struct {
		expectErrorCode      errors.CodeType
		name                 string
		nestedPipelinePath   string
		nestedPipelineStatus statemachine.NodeStatus
		parentPipelineType   models.PipelineType
		nestedPipelineType   models.PipelineType
	}

	testCases := []testCase{
		{
			name:                 "should retry nested pipeline and create deployment",
			nestedPipelinePath:   "pipeline.stage.s1.pipeline.n1",
			nestedPipelineStatus: statemachine.FailedNodeStatus,
			parentPipelineType:   models.ReleaseLifecyclePipelineType,
			nestedPipelineType:   models.DeploymentPipelineType,
		},
		{
			name:                 "should retry nested pipeline",
			nestedPipelinePath:   "pipeline.stage.s1.pipeline.n1",
			nestedPipelineStatus: statemachine.FailedNodeStatus,
			parentPipelineType:   models.RunbookPipelineType,
			nestedPipelineType:   models.NestedPipelineType,
		},
		{
			name:                 "nested pipeline cannot be retried due to its status",
			nestedPipelinePath:   "pipeline.stage.s1.pipeline.n1",
			nestedPipelineStatus: statemachine.ReadyNodeStatus,
			expectErrorCode:      errors.EInvalid,
		},
		{
			name:                 "nested pipeline does not exist",
			nestedPipelinePath:   "pipeline.stage.s1.pipeline.non-existent",
			nestedPipelineStatus: statemachine.ReadyNodeStatus,
			expectErrorCode:      errors.ENotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)
			mockEnvironments := db.NewMockEnvironments(t)

			parentPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "parent-pipeline-id",
				},
				Type:      tc.parentPipelineType,
				ReleaseID: ptr.String("release-id"),
				ProjectID: "project-id",
				Status:    statemachine.FailedNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.FailedNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "n1",
								LatestPipelineID: origNestedPipelineID,
								Path:             "pipeline.stage.s1.pipeline.n1",
								Status:           tc.nestedPipelineStatus,
								EnvironmentName:  &environmentName,
							},
						},
					},
				},
			}

			if tc.expectErrorCode == "" {
				nestedPipelinePath := tc.nestedPipelinePath
				origNestedPipeline := &models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: origNestedPipelineID,
					},
					Type:                   tc.nestedPipelineType,
					Status:                 tc.nestedPipelineStatus,
					ReleaseID:              parentPipeline.ReleaseID,
					ParentPipelineID:       &parentPipeline.Metadata.ID,
					ParentPipelineNodePath: &nestedPipelinePath,
					ProjectID:              parentPipeline.ProjectID,
					When:                   models.AutoPipeline,
					ApprovalStatus:         models.ApprovalApproved,
					ApprovalRuleIDs:        []string{"approval-rule-1", "approval-rule-2"},
				}

				newNestedPipeline := &models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: newNestedPipelineID,
					},
					Type:                   tc.nestedPipelineType,
					Status:                 statemachine.CreatedNodeStatus,
					ReleaseID:              parentPipeline.ReleaseID,
					ParentPipelineID:       &parentPipeline.Metadata.ID,
					ParentPipelineNodePath: &nestedPipelinePath,
					ProjectID:              parentPipeline.ProjectID,
					When:                   models.AutoPipeline,
					ApprovalStatus:         models.ApprovalApproved,
					ApprovalRuleIDs:        []string{"approval-rule-1", "approval-rule-2"},
				}

				mockPipelines.On("GetPipelineByID", mock.Anything, origNestedPipelineID).Return(origNestedPipeline, nil).Once()
				mockPipelines.On("GetPipelineByID", mock.Anything, newNestedPipelineID).Return(newNestedPipeline, nil).Once()

				approvalType := models.ApprovalTypePipeline
				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					Filter: &db.PipelineApprovalFilter{
						PipelineID: &origNestedPipelineID,
						Type:       &approvalType,
					},
				}).Return(&db.PipelineApprovalsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
					PipelineApprovals: []*models.PipelineApproval{
						{
							UserID:     ptr.String("user-1"),
							PipelineID: origNestedPipelineID,
							Type:       models.ApprovalTypePipeline,
						},
					},
				}, nil).Once()

				mockPipelineApprovals.On("CreatePipelineApproval", mock.Anything, &models.PipelineApproval{
					UserID:     ptr.String("user-1"),
					PipelineID: newNestedPipelineID,
					Type:       models.ApprovalTypePipeline,
				}).Return(nil, nil).Maybe()

				// This is the update for the original nested pipeline that will be superseded.
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(pipeline *models.Pipeline) bool {
					return pipeline.Metadata.ID == origNestedPipelineID && pipeline.Superseded
				})).Return(origNestedPipeline, nil).Once()

				// This is the update for the new nested pipeline that will replace the original.
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(pipeline *models.Pipeline) bool {
					return pipeline.Metadata.ID == newNestedPipelineID && pipeline.ApprovalStatus == origNestedPipeline.ApprovalStatus
				})).Return(newNestedPipeline, nil).Once()

				// This is the first update for the parent pipeline which sets the nested pipeline to initializing
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(pipeline *models.Pipeline) bool {
					if pipeline.Metadata.ID == parentPipeline.Metadata.ID {
						nestedNode, ok := pipeline.GetNestedPipeline(tc.nestedPipelinePath)
						if !ok {
							return false
						}
						return nestedNode.LatestPipelineID == newNestedPipelineID && nestedNode.Status == statemachine.InitializingNodeStatus
					}
					return false
				})).Return(parentPipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines:         mockPipelines,
				Environments:      mockEnvironments,
				PipelineApprovals: mockPipelineApprovals,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.retryNestedPipeline(ctx, parentPipeline, &RetryNestedPipelineEvent{
				NestedPipelinePath: tc.nestedPipelinePath,
				NestedPipelineID:   newNestedPipelineID,
			})

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.Len(t, changes, 1)

			// Verify that the nested pipeline was updated to running
			nestedPipeline := updatedPipeline.Stages[0].NestedPipelines[0]
			assert.Equal(t, statemachine.InitializingNodeStatus, nestedPipeline.Status)
			assert.Empty(t, nestedPipeline.Errors)
			assert.Nil(t, nestedPipeline.ScheduledStartTime)
			assert.Nil(t, nestedPipeline.CronSchedule)
		})
	}
}

func TestUpdater_runTask(t *testing.T) {
	jobID := "job-id"
	validTaskPath := "pipeline.stage.s1.task.t1"

	type testCase struct {
		expectErrorCode   errors.CodeType
		name              string
		taskPath          string
		initialTaskStatus statemachine.NodeStatus
		taskHasActions    bool
	}

	testCases := []testCase{
		{
			name:              "should set task with no actions to succeeded",
			taskPath:          validTaskPath,
			initialTaskStatus: statemachine.ReadyNodeStatus,
		},
		{
			name:              "should set task with actions to pending",
			taskPath:          validTaskPath,
			initialTaskStatus: statemachine.ReadyNodeStatus,
			taskHasActions:    true,
		},
		{
			name:            "task does not exist",
			taskPath:        "pipeline.stage.s1.task.non-existent",
			expectErrorCode: errors.EInternal,
		},
		{
			name:              "nothing should happen if task not in ready state",
			taskPath:          validTaskPath,
			initialTaskStatus: statemachine.RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockLogStreams := db.NewMockLogStreams(t)
			mockJobs := db.NewMockJobs(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:   "t1",
								Path:   validTaskPath,
								Status: tc.initialTaskStatus,
							},
						},
					},
				},
			}

			// Add actions to task if needed
			if tc.taskPath == validTaskPath && tc.taskHasActions {
				pipeline.Stages[0].Tasks[0].Actions = []*models.PipelineAction{
					{
						Name:   "a1",
						Path:   "pipeline.stage.s1.task.t1.action.a1",
						Status: statemachine.CreatedNodeStatus,
					},
				}

				createdJob := &models.Job{
					Metadata: models.ResourceMetadata{
						ID: jobID,
					},
				}

				mockJobs.On("CreateJob", mock.Anything, mock.Anything).Return(createdJob, nil)

				mockLogStreams.On("CreateLogStream", mock.Anything, &models.LogStream{
					JobID: &jobID,
				}).Return(&models.LogStream{}, nil)

				mockJobs.On("AddJobToPipelineTask", mock.Anything, pipeline.Metadata.ID, tc.taskPath, createdJob).Return(nil)
			}

			mockPipelines.On("UpdatePipeline", mock.Anything, pipeline).Return(pipeline, nil).Maybe()

			dbClient := &db.Client{
				Pipelines:  mockPipelines,
				LogStreams: mockLogStreams,
				Jobs:       mockJobs,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.runTask(ctx, pipeline, &RunTaskEvent{TaskPath: tc.taskPath})

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			if !tc.initialTaskStatus.Equals(statemachine.ReadyNodeStatus) {
				// Ensure pipeline is not updated
				mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 0)
				return
			}

			// Ensure pipeline is updated once
			mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 1)

			taskToCheck := updatedPipeline.Stages[0].Tasks[0]

			if tc.taskHasActions {
				assert.Equal(t, statemachine.PendingNodeStatus, taskToCheck.Status)
				require.NotNil(t, taskToCheck.LatestJobID)
				assert.Equal(t, jobID, *taskToCheck.LatestJobID)
				assert.Len(t, changes, 1)
			} else {
				// Task with no action should be set to succeeded
				assert.Equal(t, statemachine.SucceededNodeStatus, taskToCheck.Status)
				assert.Len(t, changes, 1)
			}
		})
	}
}

func TestUpdater_retryTask(t *testing.T) {
	validTaskPath := "pipeline.stage.s1.task.t1"

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		taskPath        string
		currentStatus   statemachine.NodeStatus
	}

	testCases := []testCase{
		{
			name:          "should retry failed task",
			taskPath:      validTaskPath,
			currentStatus: statemachine.FailedNodeStatus,
		},
		{
			name:          "should retry canceled task",
			taskPath:      validTaskPath,
			currentStatus: statemachine.CanceledNodeStatus,
		},
		{
			name:          "should retry succeeded task",
			taskPath:      validTaskPath,
			currentStatus: statemachine.SucceededNodeStatus,
		},
		{
			name:            "task does not exist",
			taskPath:        "pipeline.stage.s1.task.non-existent",
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "task is not in retryable state",
			taskPath:        validTaskPath,
			currentStatus:   statemachine.ReadyNodeStatus,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: tc.currentStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: tc.currentStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:   "t1",
								Path:   validTaskPath,
								Status: tc.currentStatus,
								When:   models.AutoPipelineTask,
							},
						},
					},
				},
			}

			if tc.taskPath == validTaskPath && tc.currentStatus.IsRetryableStatus() {
				mockPipelines.On("UpdatePipeline", mock.Anything, pipeline).Return(
					func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
						require.Equal(t, statemachine.InitializingNodeStatus, pipeline.Stages[0].Tasks[0].Status)
						return pipeline, nil
					},
				).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
				Jobs:      mockJobs,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.retryTask(ctx, pipeline, &RetryTaskEvent{TaskPath: tc.taskPath})

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			task := updatedPipeline.Stages[0].Tasks[0]
			assert.Equal(t, statemachine.InitializingNodeStatus, task.Status)
			assert.Empty(t, task.Errors)
			assert.Nil(t, task.ScheduledStartTime)
			assert.Nil(t, task.CronSchedule)

			if tc.currentStatus.IsRetryableStatus() {
				// Expect all nodes updated once for the reset.
				assert.Len(t, changes, 1)
			}
		})
	}
}

func TestUpdater_handleJobStatusChange(t *testing.T) {
	jobID := "job-id"
	pipelineID := "pipeline-id"
	validTaskPath := "pipeline.stage.s1.task.t1"

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		taskPath        string
		jobStatus       models.JobStatus
		jobType         models.JobType
	}

	testCases := []testCase{
		{
			name:      "should update task status to succeeded",
			taskPath:  validTaskPath,
			jobStatus: models.JobSucceeded,
			jobType:   models.JobTaskType,
		},
		{
			name:      "should update task status to failed",
			taskPath:  validTaskPath,
			jobStatus: models.JobFailed,
			jobType:   models.JobTaskType,
		},
		{
			name:      "should update task status to canceled",
			taskPath:  validTaskPath,
			jobStatus: models.JobCanceled,
			jobType:   models.JobTaskType,
		},
		{
			name:      "should update task status to running",
			taskPath:  validTaskPath,
			jobStatus: models.JobRunning,
			jobType:   models.JobTaskType,
		},
		{
			name:      "should update task status to canceling",
			taskPath:  validTaskPath,
			jobStatus: models.JobCanceling,
			jobType:   models.JobTaskType,
		},
		{
			name:      "job status does not require task status update",
			taskPath:  validTaskPath,
			jobStatus: models.JobPending,
			jobType:   models.JobTaskType,
		},
		{
			name:            "task does not exist",
			taskPath:        "pipeline.stage.s1.task.non-existent",
			jobStatus:       models.JobSucceeded,
			jobType:         models.JobTaskType,
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "invalid job data",
			jobType:         models.JobType("invalid"),
			jobStatus:       models.JobSucceeded,
			expectErrorCode: errors.EInternal,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:   "t1",
								Path:   validTaskPath,
								Status: statemachine.RunningNodeStatus,
								Actions: []*models.PipelineAction{
									{
										Name:   "a1",
										Path:   "pipeline.stage.s1.task.t1.action.a1",
										Status: statemachine.RunningNodeStatus,
									},
								},
							},
						},
					},
				},
			}

			mockPipelines.On("UpdatePipeline", mock.Anything, pipeline).Return(pipeline, nil).Maybe()

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			job := &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
			}

			switch tc.jobType {
			case models.JobTaskType:
				job.Type = models.JobTaskType
				job.Data = &models.JobTaskData{
					TaskPath:   tc.taskPath,
					PipelineID: pipelineID,
				}
				// Other job types can be added here
			}

			require.NoError(t, job.SetStatus(tc.jobStatus))

			updatedPipeline, changes, err := updater.handleJobStatusChange(ctx, pipeline, &JobStatusChangeEvent{Job: job})

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			if tc.jobStatus == models.JobPending {
				// Ensure pipeline is not updated
				mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 0)
				return
			}

			// Ensure pipeline is updated once
			mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 1)

			// Convert status to upper case to match the status in the pipeline
			assert.Equal(t, strings.ToUpper(string(tc.jobStatus)), string(updatedPipeline.Status))
			assert.Equal(t, strings.ToUpper(string(tc.jobStatus)), string(updatedPipeline.Stages[0].Status))
			assert.Equal(t, strings.ToUpper(string(tc.jobStatus)), string(updatedPipeline.Stages[0].Tasks[0].Status))

			if tc.jobStatus == models.JobCanceling || tc.jobStatus == models.JobCanceled {
				// Make sure status trickles down to actions. Only applicable for canceling/canceled jobs.
				assert.Equal(t, strings.ToUpper(string(tc.jobStatus)), string(updatedPipeline.Stages[0].Tasks[0].Actions[0].Status))
				assert.NotEmpty(t, changes)
			}
		})
	}
}

func TestUpdater_setPipelineActionStatus(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mockPipelines := db.NewMockPipelines(t)

	pipeline := &models.Pipeline{
		Status: statemachine.RunningNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name: "s1",
				Path: "pipeline.stage.s1",
				Tasks: []*models.PipelineTask{
					{
						Name:   "t1",
						Path:   "pipeline.stage.s1.task.t1",
						Status: statemachine.RunningNodeStatus,
						Actions: []*models.PipelineAction{
							{
								Name:   "a1",
								Path:   "pipeline.stage.s1.task.t1.action.a1",
								Status: statemachine.RunningNodeStatus,
							},
						},
					},
				},
			},
		},
	}

	mockPipelines.On("UpdatePipeline", mock.Anything, pipeline).Return(pipeline, nil)

	dbClient := &db.Client{
		Pipelines: mockPipelines,
	}

	updater := &updater{
		dbClient: dbClient,
	}

	updatedPipeline, changes, err := updater.setPipelineActionStatus(ctx, pipeline, &ActionStatusChangeEvent{
		Status:     statemachine.SucceededNodeStatus,
		ActionPath: "pipeline.stage.s1.task.t1.action.a1",
	})

	require.NoError(t, err)
	require.NotNil(t, updatedPipeline)
	assert.Equal(t, statemachine.SucceededNodeStatus, updatedPipeline.Stages[0].Tasks[0].Actions[0].Status)
	assert.Len(t, changes, 1)
}

func TestUpdater_handlePipelineCancellation(t *testing.T) {
	testSubject := "test-subject"
	nestedPipelineID := "nested-pipeline-id"

	type testCase struct {
		expectErrorCode errors.CodeType
		pipelineStatus  statemachine.NodeStatus
		name            string
		pipelineVersion int
		forceCancel     bool
	}

	testCases := []testCase{
		{
			name:            "should gracefully cancel pipeline",
			pipelineStatus:  statemachine.RunningNodeStatus,
			pipelineVersion: 1,
		},
		{
			name:            "should force cancel pipeline",
			pipelineStatus:  statemachine.CancelingNodeStatus,
			pipelineVersion: 1,
			forceCancel:     true,
		},
		{
			name:            "pipeline is already canceled",
			pipelineStatus:  statemachine.CanceledNodeStatus,
			pipelineVersion: 1,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "pipeline is already canceling",
			pipelineStatus:  statemachine.CancelingNodeStatus,
			pipelineVersion: 1,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "pipeline is already succeeded",
			pipelineStatus:  statemachine.SucceededNodeStatus,
			pipelineVersion: 1,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "pipeline is already failed",
			pipelineStatus:  statemachine.FailedNodeStatus,
			pipelineVersion: 1,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "pipeline is already canceled and force cancel is true",
			pipelineStatus:  statemachine.CanceledNodeStatus,
			pipelineVersion: 1,
			forceCancel:     true,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "pipeline has not yet started",
			pipelineStatus:  statemachine.CreatedNodeStatus,
			pipelineVersion: 1,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "pipeline version does not match supplied version",
			pipelineStatus:  statemachine.RunningNodeStatus,
			pipelineVersion: 2,
			expectErrorCode: errors.EConflict,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockCaller := auth.NewMockCaller(t)
			mockJobs := db.NewMockJobs(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:      "pipeline-id",
					Version: 1,
				},
				Status: tc.pipelineStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: tc.pipelineStatus,
						// Add some nested pipelines to test the recursion
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "n1",
								Path:             "pipeline.stage.s1.pipeline.n1",
								Status:           tc.pipelineStatus,
								LatestPipelineID: nestedPipelineID,
							},
						},
					},
				},
			}

			mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{
				Filter: &db.JobFilter{
					PipelineID: &samplePipeline.Metadata.ID,
					JobStatuses: []models.JobStatus{
						models.JobPending,
						models.JobRunning,
						models.JobQueued,
						models.JobCanceling,
					},
				},
			}).Return(&db.JobsResult{
				PageInfo: &pagination.PageInfo{
					// For simplicity, we assume that all jobs are finished as we test that logic separately.
					TotalCount: 0,
				},
			}, nil).Maybe()

			// Mock second time for nested pipeline
			mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{
				Filter: &db.JobFilter{
					PipelineID: &nestedPipelineID,
					JobStatuses: []models.JobStatus{
						models.JobPending,
						models.JobRunning,
						models.JobQueued,
						models.JobCanceling,
					},
				},
			}).Return(&db.JobsResult{
				PageInfo: &pagination.PageInfo{
					// For simplicity, we assume that all jobs are finished as we test that logic separately.
					TotalCount: 0,
				},
			}, nil).Maybe()

			if tc.expectErrorCode == "" {
				mockPipelines.On("GetPipelineByID", mock.Anything, nestedPipelineID).Return(&models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: nestedPipelineID,
					},
					Status: tc.pipelineStatus,
				}, nil)

				// Mocked twice for nested pipeline
				mockPipelines.On("UpdatePipeline", mock.Anything, samplePipeline).Return(samplePipeline, nil).Once()
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(&models.Pipeline{}, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
				Jobs:      mockJobs,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.handlePipelineCancellation(ctx, samplePipeline, &CancelPipelineEvent{
				Caller:  mockCaller,
				Version: ptr.Int(tc.pipelineVersion),
				Force:   tc.forceCancel,
			})

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			// Should be called twice, once for the parent pipeline and once for the nested pipeline
			mockPipelines.AssertNumberOfCalls(t, "UpdatePipeline", 2)
			mockJobs.AssertNumberOfCalls(t, "GetJobs", 2)

			assert.Len(t, changes, 2)

			if tc.forceCancel {
				assert.Equal(t, statemachine.CanceledNodeStatus, updatedPipeline.Status)
				assert.Equal(t, statemachine.CanceledNodeStatus, updatedPipeline.Stages[0].NestedPipelines[0].Status)
			} else {
				assert.Equal(t, statemachine.CancelingNodeStatus, updatedPipeline.Status)
				assert.Equal(t, statemachine.CancelingNodeStatus, updatedPipeline.Stages[0].NestedPipelines[0].Status)
			}
		})
	}
}

func TestUpdater_gracefullyCancelPipeline(t *testing.T) {
	type testCase struct {
		expectErrorCode  errors.CodeType
		name             string
		pipelineStatus   statemachine.NodeStatus
		jobStatus        models.JobStatus
		expectStatus     statemachine.NodeStatus
		hasUnfinishedJob bool
	}

	testCases := []testCase{
		{
			name:             "job is running, should set pipeline to canceling",
			pipelineStatus:   statemachine.RunningNodeStatus,
			jobStatus:        models.JobRunning,
			hasUnfinishedJob: true,
			expectStatus:     statemachine.CancelingNodeStatus,
		},
		{
			name:             "job is queued, should set pipeline to canceled",
			pipelineStatus:   statemachine.RunningNodeStatus,
			jobStatus:        models.JobQueued,
			hasUnfinishedJob: true,
			expectStatus:     statemachine.CanceledNodeStatus,
		},
		{
			name:           "pipeline has no unfinished jobs, should set pipeline to canceling",
			pipelineStatus: statemachine.RunningNodeStatus,
			expectStatus:   statemachine.CancelingNodeStatus,
		},
		{
			name:            "pipeline is already canceling, should return error",
			pipelineStatus:  statemachine.CancelingNodeStatus,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:             "job is already canceling, should return error",
			pipelineStatus:   statemachine.RunningNodeStatus,
			jobStatus:        models.JobCanceling,
			hasUnfinishedJob: true,
			expectErrorCode:  errors.EInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockJobs := db.NewMockJobs(t)

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: tc.pipelineStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: tc.pipelineStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:   "t1",
								Path:   "pipeline.stage.s1.task.t1",
								Status: tc.pipelineStatus,
								Actions: []*models.PipelineAction{
									{
										Name:   "a1",
										Path:   "pipeline.stage.s1.task.t1.action.a1",
										Status: tc.pipelineStatus,
									},
								},
							},
						},
					},
				},
			}

			dbResult := &db.JobsResult{}

			if tc.hasUnfinishedJob {
				job := models.Job{
					Metadata: models.ResourceMetadata{
						ID: "job-id",
					},
					Data: &models.JobTaskData{
						TaskPath:   "pipeline.stage.s1.task.t1",
						PipelineID: samplePipeline.Metadata.ID,
					},
				}

				require.NoError(t, job.SetStatus(tc.jobStatus))

				dbResult.Jobs = append(dbResult.Jobs, job)

				if tc.expectErrorCode == "" {
					// Use mock.Anything as second argument since we don't have control over the updated timestamp.
					mockJobs.On("UpdateJob", mock.Anything, mock.Anything).Return(
						func(_ context.Context, job *models.Job) (*models.Job, error) {
							// Ensure job was updated correctly
							require.NotNil(t, job.CancelRequestedTimestamp)
							require.Equal(t, strings.ToLower(string(tc.expectStatus)), string(job.GetStatus()))
							return job, nil
						},
					)
				}
			}

			mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{
				Filter: &db.JobFilter{
					PipelineID: &samplePipeline.Metadata.ID,
					JobStatuses: []models.JobStatus{
						models.JobPending,
						models.JobRunning,
						models.JobQueued,
						models.JobCanceling,
					},
				},
			}).Return(dbResult, nil).Maybe()

			if tc.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, samplePipeline).Return(samplePipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
				Jobs:      mockJobs,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.gracefullyCancelPipeline(ctx, samplePipeline)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.NotEmpty(t, changes)

			assert.Equal(t, tc.expectStatus, updatedPipeline.Status)
			assert.Equal(t, tc.expectStatus, updatedPipeline.Stages[0].Status)
			assert.Equal(t, tc.expectStatus, updatedPipeline.Stages[0].Tasks[0].Status)
			assert.Equal(t, tc.expectStatus, updatedPipeline.Stages[0].Tasks[0].Actions[0].Status)
		})
	}
}

func TestUpdater_forceCancelPipeline(t *testing.T) {
	testSubject := "test-subject"

	type testCase struct {
		jobCancellationTimestamp *time.Time
		expectErrorCode          errors.CodeType
		name                     string
		unfinishedJobs           int
	}

	testCases := []testCase{
		{
			name:                     "should cancel pipeline",
			jobCancellationTimestamp: ptr.Time(time.Now().UTC().Add(-100 * time.Minute)),
			unfinishedJobs:           2,
		},
		{
			name: "pipeline has no unfinished jobs",
		},
		{
			name:            "pipeline has not been gracefully canceled yet",
			unfinishedJobs:  1,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.CancelingNodeStatus,
			}

			dbResult := &db.JobsResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(tc.unfinishedJobs),
				},
			}

			for i := 0; i < tc.unfinishedJobs; i++ {
				job := models.Job{
					Metadata: models.ResourceMetadata{
						ID: fmt.Sprintf("job-id-%d", i),
					},
					CancelRequestedTimestamp: tc.jobCancellationTimestamp,
				}

				dbResult.Jobs = append(dbResult.Jobs, job)

				mockCaller.On("GetSubject").Return(testSubject)

				if tc.expectErrorCode == "" {
					// Ensure job is updated, mock will automatically assert that values match
					require.NoError(t, job.SetStatus(models.JobCanceled))
					job.ForceCanceled = true
					job.ForceCanceledBy = &testSubject

					mockJobs.On("UpdateJob", mock.Anything, mock.MatchedBy(func(j *models.Job) bool {
						return j.ForceCanceled &&
							ptr.ToString(j.ForceCanceledBy) == testSubject &&
							j.Timestamps.FinishedTimestamp != nil
					})).Return(&job, nil)
				}
			}

			mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{
				Filter: &db.JobFilter{
					PipelineID: &samplePipeline.Metadata.ID,
					JobStatuses: []models.JobStatus{
						models.JobPending,
						models.JobRunning,
						models.JobQueued,
						models.JobCanceling,
					},
				},
			}).Return(dbResult, nil)

			if tc.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, samplePipeline).Return(samplePipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
				Jobs:      mockJobs,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.forceCancelPipeline(ctx, samplePipeline, mockCaller)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.Equal(t, statemachine.CanceledNodeStatus, updatedPipeline.Status)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_fireEvents(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Use mock event handler to ensure event is passed correctly
	mockEventHandler := &mockEventHandler{}

	pipelineID := "pipeline-1"
	statusChanges := []StatusChange{}

	mockEventHandler.On("HandleEvent", mock.Anything, pipelineID, statusChanges, &RunTaskEvent{}).Return(nil)

	updater := &updater{
		eventHandlers: []EventHandler{
			mockEventHandler.HandleEvent,
		},
	}

	require.NoError(t, updater.fireEvents(ctx, pipelineID, statusChanges, &RunTaskEvent{}))
	mockEventHandler.AssertExpectations(t)
}

func TestUpdater_getUnfinishedJobsForPipeline(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mockJobs := db.NewMockJobs(t)

	mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{
		Filter: &db.JobFilter{
			PipelineID: ptr.String("pipeline-id"),
			JobStatuses: []models.JobStatus{
				models.JobPending,
				models.JobRunning,
				models.JobQueued,
				models.JobCanceling,
			},
		},
	}).Return(&db.JobsResult{
		PageInfo: &pagination.PageInfo{
			TotalCount: 1,
		},
		Jobs: []models.Job{
			{
				Metadata: models.ResourceMetadata{
					ID: "job-id",
				},
			},
		},
	}, nil)

	dbClient := &db.Client{
		Jobs: mockJobs,
	}

	updater := &updater{
		dbClient: dbClient,
	}

	jobs, err := updater.getUnfinishedJobsForPipeline(ctx, "pipeline-id")

	require.NoError(t, err)
	assert.Len(t, jobs, 1)
}

func TestUpdater_getPipelineByID(t *testing.T) {
	pipelineID := "pipeline-id"

	type testCase struct {
		expectErrorCode errors.CodeType
		name            string
		pipelineID      string
	}

	testCases := []testCase{
		{
			name:       "should return pipeline",
			pipelineID: pipelineID,
		},
		{
			name:            "pipeline does not exist",
			pipelineID:      "non-existent",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, tc.pipelineID).Return(func(_ context.Context, id string) (*models.Pipeline, error) {
				if id == pipelineID {
					return &models.Pipeline{
						Metadata: models.ResourceMetadata{
							ID: pipelineID,
						},
					}, nil
				}

				return nil, nil
			})

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			pipeline, err := updater.getPipelineByID(ctx, tc.pipelineID)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, pipeline)
			assert.Equal(t, pipelineID, pipeline.Metadata.ID)
		})
	}
}

func TestUpdater_approvePipelineTask(t *testing.T) {
	userID := "user-1"
	teamID := "team-1"
	serviceAccountID := "service-account-1"
	validTaskPath := "pipeline.stage.s1.task.t1"
	approvalRuleIDs := []string{"approval-rule-1", "approval-rule-2"}

	type testCase struct {
		event             *TaskApprovalEvent
		rules             []*models.ApprovalRule
		task              *models.PipelineTask
		name              string
		expectErrorCode   errors.CodeType
		teams             []models.Team
		existingApprovals []*models.PipelineApproval
		approvalExists    bool
		expectApproved    bool
	}

	testCases := []testCase{
		{
			name: "user successfully approves task",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "user successfully approves task with scheduled start time",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				ApprovalRuleIDs:    approvalRuleIDs,
				ApprovalStatus:     models.ApprovalPending,
				Status:             statemachine.ApprovalPendingNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "user partially approves task because one of the rules requires multiple approvals",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 2,
					UserIDs:           []string{userID},
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: false,
		},
		{
			name: "user partially approves task because user is only a valid approver for one of the rules",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{},
					TeamIDs:           []string{},
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: []string{approvalRuleIDs[0]},
				},
			},
			expectApproved: false,
		},
		{
			name: "team member successfully approves task",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{teamID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{teamID},
				},
			},
			teams: []models.Team{{Metadata: models.ResourceMetadata{ID: teamID}}},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "service account successfully approves task",
			event: &TaskApprovalEvent{
				ServiceAccountID: &serviceAccountID,
				TaskPath:         validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{serviceAccountID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{serviceAccountID},
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "user approval already exists",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			approvalExists:  true,
			expectErrorCode: errors.EConflict,
		},
		{
			name: "user and service account cannot approve simultaneously",
			event: &TaskApprovalEvent{
				UserID:           &userID,
				ServiceAccountID: &serviceAccountID,
				TaskPath:         validTaskPath,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "task not found",
			event: &TaskApprovalEvent{
				TaskPath: "pipeline.stage.s1.task.unknown",
			},
			task:            &models.PipelineTask{},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "task does not require approval",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				ApprovalStatus: models.ApprovalNotRequired,
				Status:         statemachine.WaitingNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "task approval rules were deleted",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				ApprovalStatus: models.ApprovalPending,
				Status:         statemachine.WaitingNodeStatus,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "task is not in the waiting state",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.PendingNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "one or more approval rule were not found",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "user is not authorized to approve task",
			event: &TaskApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{},
				},
			},
			teams: []models.Team{{Metadata: models.ResourceMetadata{ID: teamID}}},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "service account is not authorized to approve task",
			event: &TaskApprovalEvent{
				ServiceAccountID: &serviceAccountID,
				TaskPath:         validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{},
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				ApprovalStatus:  models.ApprovalPending,
				Status:          statemachine.ApprovalPendingNodeStatus,
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockTeams := db.NewMockTeams(t)
			mockPipelines := db.NewMockPipelines(t)
			mockToDoItems := db.NewMockToDoItems(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)
			mockProjects := db.NewMockProjects(t)

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-1",
				},
				ProjectID: "project-1",
				Status:    statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks:  []*models.PipelineTask{test.task},
					},
				},
			}

			mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
				Filter: &db.ApprovalRuleFilter{
					ApprovalRuleIDs: approvalRuleIDs,
				},
			}).Return(&db.ApprovalRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(test.rules)),
				},
				ApprovalRules: test.rules,
			}, nil).Maybe()

			mockTeams.On("GetTeams", mock.Anything, &db.GetTeamsInput{
				Filter: &db.TeamFilter{
					UserID: test.event.UserID,
				},
			}).Return(&db.TeamsResult{Teams: test.teams}, nil).Maybe()

			approvalType := models.ApprovalTypeTask
			mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
				Filter: &db.PipelineApprovalFilter{
					PipelineID: &samplePipeline.Metadata.ID,
					TaskPath:   ptr.String(test.event.TaskPath),
					Type:       &approvalType,
				},
			}).Return(&db.PipelineApprovalsResult{
				PipelineApprovals: test.existingApprovals,
				PageInfo: &pagination.PageInfo{
					TotalCount: 0,
				},
			}, nil).Maybe()

			mockToDoItems.On("GetToDoItems", mock.Anything, mock.Anything).Return(&db.ToDoItemsResult{
				ToDoItems: []*models.ToDoItem{
					{
						Metadata: models.ResourceMetadata{
							ID:                "todo-item-1",
							CreationTimestamp: ptr.Time(time.Now()),
						},
						UserIDs: []string{userID},
					},
				},
				PageInfo: &pagination.PageInfo{TotalCount: 1},
			}, nil).Maybe()

			if test.event.UserID != nil {
				mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(ti *models.ToDoItem) bool {
					return slices.Contains(ti.ResolvedByUsers, userID)
				})).Return(&models.ToDoItem{}, nil).Maybe()
			}

			if test.expectApproved {
				mockPipelines.On("UpdatePipeline",
					mock.Anything,
					mock.MatchedBy(func(p *models.Pipeline) bool {
						task, ok := p.GetTask(validTaskPath)
						if !ok {
							t.Fatalf("couldn't find task %s", validTaskPath)
						}

						if task.ApprovalStatus != models.ApprovalApproved {
							return false
						}

						if task.ScheduledStartTime != nil {
							return task.Status.Equals(statemachine.WaitingNodeStatus)
						}

						return task.Status.Equals(statemachine.ReadyNodeStatus)
					}),
				).Return(samplePipeline, nil)
			}

			approvedRules := []string{}
			for _, approval := range test.existingApprovals {
				if approval.UserID == test.event.UserID || approval.ServiceAccountID == test.event.ServiceAccountID {
					approvedRules = append(approvedRules, approval.ApprovalRuleIDs...)
				}
			}

			mockPipelineApprovals.On("CreatePipelineApproval", mock.Anything, &models.PipelineApproval{
				PipelineID:       samplePipeline.Metadata.ID,
				TaskPath:         ptr.String(test.event.TaskPath),
				UserID:           test.event.UserID,
				ServiceAccountID: test.event.ServiceAccountID,
				Type:             models.ApprovalTypeTask,
				ApprovalRuleIDs:  approvedRules,
			}).Return(nil, func(_ context.Context, _ *models.PipelineApproval) error {
				if test.approvalExists {
					return errors.New("approval already exists", errors.WithErrorCode(errors.EConflict))
				}

				return nil
			}).Maybe()

			dbClient := &db.Client{
				Teams:             mockTeams,
				Pipelines:         mockPipelines,
				ToDoItems:         mockToDoItems,
				ApprovalRules:     mockApprovalRules,
				PipelineApprovals: mockPipelineApprovals,
				Projects:          mockProjects,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.approvePipelineTask(ctx, samplePipeline, test.event)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.Equal(t, samplePipeline, updatedPipeline)
			require.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_revokePipelineTaskApproval(t *testing.T) {
	userID := "user-1"
	taskApprovalID := "task-approval-1"
	serviceAccountID := "service-account-1"
	approvalRuleIDs := []string{"approval-rule-1", "approval-rule-2"}
	validTaskPath := "pipeline.stage.s1.task.t1"

	type testCase struct {
		event                  *TaskRevokeApprovalEvent
		task                   *models.PipelineTask
		name                   string
		expectErrorCode        errors.CodeType
		rules                  []*models.ApprovalRule
		existingApprovalsCount int32
	}

	testCases := []testCase{
		{
			name: "user successfully revokes task approval",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				Status:          statemachine.ReadyNodeStatus,
				ApprovalStatus:  models.ApprovalApproved,
			},
			existingApprovalsCount: 1,
		},
		{
			name: "service account successfully revokes task approval",
			event: &TaskRevokeApprovalEvent{
				ServiceAccountID: &serviceAccountID,
				TaskPath:         validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 1,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 1,
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				Status:          statemachine.ReadyNodeStatus,
				ApprovalStatus:  models.ApprovalApproved,
			},
			existingApprovalsCount: 1,
		},
		{
			name: "task in approval pending status should remain unchanged",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					UserIDs:           []string{userID},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 2,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					UserIDs:           []string{userID},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 2,
				},
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
			},
			existingApprovalsCount: 1,
		},
		{
			name: "user and service account cannot simultaneously revoke approval",
			event: &TaskRevokeApprovalEvent{
				UserID:           &userID,
				ServiceAccountID: &serviceAccountID,
				TaskPath:         validTaskPath,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "task not found",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: "pipeline.stage.s1.task.unknown",
			},
			task:            &models.PipelineTask{},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "task does not require approvals",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.ReadyNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "approval rule associated with task was deleted",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.ReadyNodeStatus,
				ApprovalStatus: models.ApprovalPending,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "task is not in the waiting state",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				Status:          statemachine.PendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "approval rule not found",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			rules: []*models.ApprovalRule{},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "task approval not found",
			event: &TaskRevokeApprovalEvent{
				UserID:   &userID,
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:            "t1",
				Path:            validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockToDoItems := db.NewMockToDoItems(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-1",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks:  []*models.PipelineTask{test.task},
					},
				},
			}

			mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
				Filter: &db.ApprovalRuleFilter{
					ApprovalRuleIDs: approvalRuleIDs,
				},
			}).Return(&db.ApprovalRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(test.rules)),
				},
				ApprovalRules: test.rules,
			}, nil).Maybe()

			approvalType := models.ApprovalTypeTask

			if len(test.rules) > 0 {
				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					Filter: &db.PipelineApprovalFilter{
						PipelineID:       &samplePipeline.Metadata.ID,
						TaskPath:         &validTaskPath,
						UserID:           test.event.UserID,
						ServiceAccountID: test.event.ServiceAccountID,
						Type:             &approvalType,
					},
				}).Return(func(_ context.Context, _ *db.GetPipelineApprovalsInput) (*db.PipelineApprovalsResult, error) {
					result := &db.PipelineApprovalsResult{
						PipelineApprovals: []*models.PipelineApproval{},
						PageInfo: &pagination.PageInfo{
							TotalCount: test.existingApprovalsCount,
						},
					}

					for i := int32(0); i < test.existingApprovalsCount; i++ {
						result.PipelineApprovals = append(result.PipelineApprovals, &models.PipelineApproval{
							Metadata: models.ResourceMetadata{
								ID: taskApprovalID,
							},
							UserID:           test.event.UserID,
							ServiceAccountID: test.event.ServiceAccountID,
							PipelineID:       samplePipeline.Metadata.ID,
							TaskPath:         ptr.String(test.event.TaskPath),
							Type:             models.ApprovalTypeTask,
						})
					}

					return result, nil
				}).Once()
			}

			newApprovalCount := test.existingApprovalsCount - 1
			if test.existingApprovalsCount > 0 {
				mockPipelineApprovals.On("DeletePipelineApproval",
					mock.Anything,
					mock.MatchedBy(func(ta *models.PipelineApproval) bool {
						return ta.Metadata.ID == taskApprovalID
					}),
				).Return(nil)

				if test.event.UserID != nil {
					mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(1),
						},
						Filter: &db.ToDoItemFilter{
							UserID:           test.event.UserID,
							Resolved:         ptr.Bool(true),
							PipelineTargetID: &samplePipeline.Metadata.ID,
							TargetTypes: []models.ToDoItemTargetType{
								models.TaskApprovalTarget,
							},
							PayloadFilter: &db.ToDoItemPayloadFilter{
								PipelineTaskPath: &validTaskPath,
							},
						},
					}).Return(&db.ToDoItemsResult{
						PageInfo: &pagination.PageInfo{TotalCount: 1},
						ToDoItems: []*models.ToDoItem{
							{
								Metadata: models.ResourceMetadata{
									ID: "todo-item-1",
								},
								UserIDs:         []string{userID},
								ResolvedByUsers: []string{userID},
							},
						},
					}, nil)

					mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(ti *models.ToDoItem) bool {
						return !slices.Contains(ti.ResolvedByUsers, userID)
					})).Return(&models.ToDoItem{}, nil)
				}

				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					PaginationOptions: &pagination.Options{First: ptr.Int32(0)},
					Filter: &db.PipelineApprovalFilter{
						PipelineID: &samplePipeline.Metadata.ID,
						TaskPath:   &validTaskPath,
						Type:       &approvalType,
					},
				}).Return(&db.PipelineApprovalsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: newApprovalCount,
					}}, nil).Once()
			}

			mockPipelines.On("UpdatePipeline",
				mock.Anything,
				mock.MatchedBy(func(p *models.Pipeline) bool {
					task, ok := p.GetTask(validTaskPath)
					if !ok {
						t.Fatalf("couldn't find task %s", validTaskPath)
					}

					return task.Status.Equals(statemachine.ApprovalPendingNodeStatus) && task.ApprovalStatus == models.ApprovalPending
				}),
			).Return(samplePipeline, nil).Maybe()

			dbClient := &db.Client{
				Pipelines:         mockPipelines,
				ToDoItems:         mockToDoItems,
				ApprovalRules:     mockApprovalRules,
				PipelineApprovals: mockPipelineApprovals,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.revokePipelineTaskApproval(ctx, samplePipeline, test.event)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_approvePipeline(t *testing.T) {
	userID := "user-1"
	teamID := "team-1"
	serviceAccountID := "service-account-1"
	pipelineID := "pipeline-1"
	projectID := "project-1"
	approvalRuleIDs := []string{"approval-rule-1", "approval-rule-2"}

	type testCase struct {
		event             *ApprovalEvent
		rules             []*models.ApprovalRule
		pipeline          *models.Pipeline
		parentPipeline    *models.Pipeline
		name              string
		expectErrorCode   errors.CodeType
		teams             []models.Team
		existingApprovals []*models.PipelineApproval
		approvalExists    bool
		expectApproved    bool
	}

	testCases := []testCase{
		{
			name: "user successfully approves pipeline",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.RunbookPipelineType,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "user successfully approves pipeline with scheduled start time",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ParentPipelineID:       ptr.String("parent-pipeline"),
				ParentPipelineNodePath: ptr.String("pipeline.stage.s1.pipeline.p1"),
				ProjectID:              projectID,
				Status:                 statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:         models.ApprovalPending,
				ApprovalRuleIDs:        approvalRuleIDs,
				Type:                   models.RunbookPipelineType,
			},
			parentPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "parent-pipeline",
				},
				ProjectID: projectID,
				Status:    statemachine.RunningNodeStatus,
				Type:      models.RunbookPipelineType,
				Stages: []*models.PipelineStage{{
					Name: "s1",
					NestedPipelines: []*models.NestedPipeline{
						{
							Name:               "p1",
							Path:               "pipeline.stage.s1.pipeline.p1",
							LatestPipelineID:   pipelineID,
							ScheduledStartTime: ptr.Time(time.Now()),
							Status:             statemachine.ApprovalPendingNodeStatus,
							ApprovalStatus:     models.ApprovalPending,
						},
					},
				}},
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "user partially approves pipeline because rule requires more than one approval",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 2,
					UserIDs:           []string{userID},
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.RunbookPipelineType,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: false,
		},
		{
			name: "user partially approves pipeline because user is only a valid approver for one rule",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{},
					TeamIDs:           []string{teamID},
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.RunbookPipelineType,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: []string{approvalRuleIDs[0]},
				},
			},
		},
		{
			name: "team member successfully approves pipeline",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{teamID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{teamID},
				},
			},
			teams: []models.Team{{Metadata: models.ResourceMetadata{ID: teamID}}},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "service account successfully approves pipeline",
			event: &ApprovalEvent{
				ServiceAccountID: &serviceAccountID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{serviceAccountID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{serviceAccountID},
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					ServiceAccountID: &serviceAccountID,
					ApprovalRuleIDs:  approvalRuleIDs,
				},
			},
			expectApproved: true,
		},
		{
			name: "user approval already exists",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					UserIDs:           []string{userID},
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.RunbookPipelineType,
			},
			existingApprovals: []*models.PipelineApproval{
				{
					UserID:          &userID,
					ApprovalRuleIDs: approvalRuleIDs,
				},
			},
			approvalExists:  true,
			expectErrorCode: errors.EConflict,
		},
		{
			name: "user and service account cannot approve simultaneously",
			event: &ApprovalEvent{
				UserID:           &userID,
				ServiceAccountID: &serviceAccountID,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "pipeline does not require approval",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:      projectID,
				Status:         statemachine.RunningNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "one or more pipeline approval rules were deleted",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "pipeline is not in the waiting state",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.CreatedNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "user is not authorized to approve pipeline",
			event: &ApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					TeamIDs:           []string{},
				},
			},
			teams: []models.Team{{Metadata: models.ResourceMetadata{ID: teamID}}},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "service account is not authorized to approve pipeline",
			event: &ApprovalEvent{
				ServiceAccountID: &serviceAccountID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ApprovalsRequired: 1,
					ServiceAccountIDs: []string{},
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.RunbookPipelineType,
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockTeams := db.NewMockTeams(t)
			mockPipelines := db.NewMockPipelines(t)
			mockToDoItems := db.NewMockToDoItems(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)
			mockMetrics := db.NewMockMetrics(t)
			mockProjects := db.NewMockProjects(t)

			mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
				Filter: &db.ApprovalRuleFilter{
					ApprovalRuleIDs: approvalRuleIDs,
				},
			}).Return(&db.ApprovalRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(test.rules)),
				},
				ApprovalRules: test.rules,
			}, nil).Maybe()

			mockTeams.On("GetTeams", mock.Anything, &db.GetTeamsInput{
				Filter: &db.TeamFilter{
					UserID: test.event.UserID,
				},
			}).Return(&db.TeamsResult{Teams: test.teams}, nil).Maybe()

			if test.event.UserID != nil {
				mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(1),
					},
					Filter: &db.ToDoItemFilter{
						UserID:           test.event.UserID,
						Resolved:         ptr.Bool(false),
						PipelineTargetID: &pipelineID,
						TargetTypes: []models.ToDoItemTargetType{
							models.PipelineApprovalTarget,
						},
					},
				}).Return(&db.ToDoItemsResult{
					ToDoItems: []*models.ToDoItem{
						{
							Metadata: models.ResourceMetadata{
								ID: "todo-item-1",
							},
							UserIDs: []string{userID},
						},
					},
					PageInfo: &pagination.PageInfo{TotalCount: 1},
				}, nil).Maybe()

				mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(ti *models.ToDoItem) bool {
					return slices.Contains(ti.ResolvedByUsers, userID)
				})).Return(&models.ToDoItem{}, nil).Maybe()
			}

			if test.expectErrorCode == "" {
				approvalType := models.ApprovalTypePipeline
				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					Filter: &db.PipelineApprovalFilter{
						PipelineID: ptr.String(test.pipeline.Metadata.ID),
						Type:       &approvalType,
					},
				}).Return(&db.PipelineApprovalsResult{
					PipelineApprovals: test.existingApprovals,
					PageInfo: &pagination.PageInfo{
						TotalCount: 0,
					},
				}, nil).Maybe()
			}

			approvedRules := []string{}
			for _, approval := range test.existingApprovals {
				if approval.UserID == test.event.UserID || approval.ServiceAccountID == test.event.ServiceAccountID {
					approvedRules = append(approvedRules, approval.ApprovalRuleIDs...)
				}
			}

			mockPipelineApprovals.On("CreatePipelineApproval", mock.Anything, &models.PipelineApproval{
				PipelineID:       pipelineID,
				UserID:           test.event.UserID,
				ServiceAccountID: test.event.ServiceAccountID,
				Type:             models.ApprovalTypePipeline,
				ApprovalRuleIDs:  approvedRules,
			}).Return(nil, func(_ context.Context, _ *models.PipelineApproval) error {
				if test.approvalExists {
					return errors.New("approval already exists", errors.WithErrorCode(errors.EConflict))
				}

				return nil
			}).Maybe()

			if test.expectApproved {
				if test.parentPipeline != nil {
					// Mock call to get parent pipeline
					mockPipelines.On("GetPipelineByID", mock.Anything, test.parentPipeline.Metadata.ID).Return(test.parentPipeline, nil)
					// Mock call to update parent pipeline
					mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(p *models.Pipeline) bool {
						nestedPipelineNode, ok := p.GetNestedPipeline(*test.pipeline.ParentPipelineNodePath)
						if !ok {
							return false
						}
						return nestedPipelineNode.ApprovalStatus == models.ApprovalApproved
					})).Return(nil, nil)
				}

				mockPipelines.On("UpdatePipeline",
					mock.Anything,
					mock.MatchedBy(func(p *models.Pipeline) bool {
						if p.ApprovalStatus != models.ApprovalApproved {
							return false
						}

						if test.parentPipeline != nil {
							nestedPipelineNode, ok := test.parentPipeline.GetNestedPipeline(*test.pipeline.ParentPipelineNodePath)
							if !ok {
								return false
							}
							if nestedPipelineNode.ScheduledStartTime != nil {
								return p.Status.Equals(statemachine.WaitingNodeStatus)
							}
						}
						return p.Status.Equals(statemachine.ReadyNodeStatus)
					}),
				).Return(test.pipeline, nil)
			}

			dbClient := &db.Client{
				Teams:             mockTeams,
				Pipelines:         mockPipelines,
				ToDoItems:         mockToDoItems,
				ApprovalRules:     mockApprovalRules,
				PipelineApprovals: mockPipelineApprovals,
				Metrics:           mockMetrics,
				Projects:          mockProjects,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.approvePipeline(ctx, test.pipeline, test.event)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.Equal(t, test.pipeline, updatedPipeline)
			require.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_revokePipelineApproval(t *testing.T) {
	userID := "user-1"
	pipelineApprovalID := "pipeline-approval-1"
	serviceAccountID := "service-account-1"
	pipelineID := "pipeline-1"
	projectID := "project-1"
	approvalRuleIDs := []string{"approval-rule-1", "approval-rule-2"}

	type testCase struct {
		event                  *RevokeApprovalEvent
		pipeline               *models.Pipeline
		name                   string
		expectErrorCode        errors.CodeType
		rules                  []*models.ApprovalRule
		existingApprovalsCount int32
	}

	testCases := []testCase{
		{
			name: "user successfully revokes pipeline approval",
			event: &RevokeApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.RunbookPipelineType,
			},
			existingApprovalsCount: 1,
		},
		{
			name: "service account successfully revokes pipeline approval",
			event: &RevokeApprovalEvent{
				ServiceAccountID: &serviceAccountID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 1,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 1,
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.RunbookPipelineType,
			},
			existingApprovalsCount: 1,
		},
		{
			name: "pipeline in approval pending status should remain unchanged",
			event: &RevokeApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					UserIDs:           []string{userID},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 2,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					UserIDs:           []string{userID},
					ServiceAccountIDs: []string{serviceAccountID},
					ApprovalsRequired: 2,
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			existingApprovalsCount: 1,
		},
		{
			name: "user and service account cannot simultaneously revoke approval",
			event: &RevokeApprovalEvent{
				UserID:           &userID,
				ServiceAccountID: &serviceAccountID,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "pipeline does not require approvals",
			event: &RevokeApprovalEvent{
				UserID: &userID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:      projectID,
				Status:         statemachine.CreatedNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
				Type:           models.DeploymentPipelineType,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "approval rules associated with pipeline were deleted",
			event: &RevokeApprovalEvent{
				UserID: &userID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: []string{},
				Type:            models.DeploymentPipelineType,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "pipeline is not in the waiting state",
			event: &RevokeApprovalEvent{
				UserID: &userID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.RunningNodeStatus,
				ApprovalStatus:  models.ApprovalApproved,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "one or more approval rules were not found",
			event: &RevokeApprovalEvent{
				UserID: &userID,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "pipeline approval not found",
			event: &RevokeApprovalEvent{
				UserID: &userID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.ApprovalPendingNodeStatus,
				ApprovalStatus:  models.ApprovalPending,
				ApprovalRuleIDs: approvalRuleIDs,
				Type:            models.DeploymentPipelineType,
			},
			rules: []*models.ApprovalRule{
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[0],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: approvalRuleIDs[1],
					},
					UserIDs:           []string{userID},
					ApprovalsRequired: 1,
				},
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockToDoItems := db.NewMockToDoItems(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)

			mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
				Filter: &db.ApprovalRuleFilter{
					ApprovalRuleIDs: approvalRuleIDs,
				},
			}).Return(&db.ApprovalRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(test.rules)),
				},
				ApprovalRules: test.rules,
			}, nil).Maybe()

			approvalType := models.ApprovalTypePipeline
			if len(test.rules) == len(approvalRuleIDs) {
				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					Filter: &db.PipelineApprovalFilter{
						PipelineID:       ptr.String(test.pipeline.Metadata.ID),
						UserID:           test.event.UserID,
						ServiceAccountID: test.event.ServiceAccountID,
						Type:             &approvalType,
					},
				}).Return(func(_ context.Context, _ *db.GetPipelineApprovalsInput) (*db.PipelineApprovalsResult, error) {
					result := &db.PipelineApprovalsResult{
						PipelineApprovals: []*models.PipelineApproval{},
						PageInfo: &pagination.PageInfo{
							TotalCount: test.existingApprovalsCount,
						},
					}

					for i := int32(0); i < test.existingApprovalsCount; i++ {
						result.PipelineApprovals = append(result.PipelineApprovals, &models.PipelineApproval{
							Metadata: models.ResourceMetadata{
								ID: pipelineApprovalID,
							},
							UserID:           test.event.UserID,
							ServiceAccountID: test.event.ServiceAccountID,
							PipelineID:       test.pipeline.Metadata.ID,
							Type:             models.ApprovalTypePipeline,
						})
					}

					return result, nil
				}).Once()
			}

			newApprovalCount := test.existingApprovalsCount - 1
			if test.existingApprovalsCount > 0 {
				mockPipelineApprovals.On("DeletePipelineApproval",
					mock.Anything,
					mock.MatchedBy(func(ta *models.PipelineApproval) bool {
						return ta.Metadata.ID == pipelineApprovalID
					}),
				).Return(nil)

				if test.event.UserID != nil {
					mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(1),
						},
						Filter: &db.ToDoItemFilter{
							UserID:           test.event.UserID,
							Resolved:         ptr.Bool(true),
							PipelineTargetID: ptr.String(test.pipeline.Metadata.ID),
							TargetTypes: []models.ToDoItemTargetType{
								models.PipelineApprovalTarget,
							},
						},
					}).Return(&db.ToDoItemsResult{
						PageInfo: &pagination.PageInfo{TotalCount: 1},
						ToDoItems: []*models.ToDoItem{
							{
								Metadata: models.ResourceMetadata{
									ID: "todo-item-1",
								},
								UserIDs:         []string{userID},
								ResolvedByUsers: []string{userID},
							},
						},
					}, nil)

					mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(ti *models.ToDoItem) bool {
						return !slices.Contains(ti.ResolvedByUsers, userID)
					})).Return(&models.ToDoItem{}, nil)
				}

				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					PaginationOptions: &pagination.Options{First: ptr.Int32(0)},
					Filter: &db.PipelineApprovalFilter{
						PipelineID: ptr.String(test.pipeline.Metadata.ID),
						Type:       &approvalType,
					},
				}).Return(&db.PipelineApprovalsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: newApprovalCount,
					}}, nil).Once()
			}

			mockPipelines.On("UpdatePipeline",
				mock.Anything,
				mock.MatchedBy(func(p *models.Pipeline) bool {
					return p.Status.Equals(statemachine.ApprovalPendingNodeStatus) && p.ApprovalStatus == models.ApprovalPending
				}),
			).Return(test.pipeline, nil).Maybe()

			dbClient := &db.Client{
				Pipelines:         mockPipelines,
				ToDoItems:         mockToDoItems,
				ApprovalRules:     mockApprovalRules,
				PipelineApprovals: mockPipelineApprovals,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.revokePipelineApproval(ctx, test.pipeline, test.event)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_scheduleTask(t *testing.T) {
	validTaskPath := "pipeline.stage.s1.task.t1"
	cronExpression := fmt.Sprintf("* %d * * *", (time.Now().Hour()+2)%24)

	// This will be used in the tests to check the scheduled time
	exp, err := cronexpr.Parse(cronExpression)
	require.NoError(t, err)

	// Get next time based on the cron expression
	scheduledStartTime := exp.Next(time.Now().UTC()).UTC()
	require.NoError(t, err)

	type testCase struct {
		input           *ScheduleTaskEvent
		task            *models.PipelineTask
		expectedTask    *models.PipelineTask
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "should set scheduled start time for a task in the waiting state",
			input: &ScheduleTaskEvent{
				TaskPath:           validTaskPath,
				ScheduledStartTime: &scheduledStartTime,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.ReadyNodeStatus,
				When:   models.ManualPipelineTask,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			expectedTask: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.WaitingNodeStatus,
				When:               models.ManualPipelineTask,
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should set scheduled start time for a task in the created state",
			input: &ScheduleTaskEvent{
				TaskPath:           validTaskPath,
				ScheduledStartTime: &scheduledStartTime,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.CreatedNodeStatus,
				When:   models.ManualPipelineTask,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			expectedTask: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.CreatedNodeStatus,
				When:               models.ManualPipelineTask,
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should set cron schedule for a task in the created state",
			input: &ScheduleTaskEvent{
				TaskPath: validTaskPath,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.CreatedNodeStatus,
				When:               models.ManualPipelineTask,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectedTask: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.CreatedNodeStatus,
				When:   models.ManualPipelineTask,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
		},
		{
			name: "should set cron schedule for a task in the approval pending state",
			input: &ScheduleTaskEvent{
				TaskPath: validTaskPath,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.ApprovalPendingNodeStatus,
				When:               models.ManualPipelineTask,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectedTask: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.ApprovalPendingNodeStatus,
				When:   models.ManualPipelineTask,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should set cron schedule for a task in the ready state",
			input: &ScheduleTaskEvent{
				TaskPath: validTaskPath,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.ReadyNodeStatus,
				When:               models.ManualPipelineTask,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectedTask: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.WaitingNodeStatus,
				When:   models.ManualPipelineTask,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should fail because task is in the waiting state and attempt count is greater than 0",
			input: &ScheduleTaskEvent{
				TaskPath:           validTaskPath,
				ScheduledStartTime: &scheduledStartTime,
			},
			task: &models.PipelineTask{
				Name:         "t1",
				Path:         validTaskPath,
				Status:       statemachine.WaitingNodeStatus,
				When:         models.ManualPipelineTask,
				AttemptCount: 1,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "should return error because both cron schedule and start time are not specified",
			input: &ScheduleTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.WaitingNodeStatus,
				When:   models.ManualPipelineTask,
			},
			expectErrorCode: errors.EInternal,
		},
		{
			name: "should return error because both cron schedule and start time cannot be specified",
			input: &ScheduleTaskEvent{
				TaskPath:           validTaskPath,
				ScheduledStartTime: &scheduledStartTime,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * * ",
				},
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.WaitingNodeStatus,
				When:   models.ManualPipelineTask,
			},
			expectErrorCode: errors.EInternal,
		},
		{
			name: "should return error because task cannot be in the running state when setting the scheduled start time",
			input: &ScheduleTaskEvent{
				TaskPath:           validTaskPath,
				ScheduledStartTime: &scheduledStartTime,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.RunningNodeStatus,
				When:   models.ManualPipelineTask,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						Tasks: []*models.PipelineTask{
							tc.task,
						},
					},
				},
			}

			if tc.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
					return pipeline, nil
				}, nil)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.scheduleTask(ctx, pipeline, tc.input)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			taskToCheck := updatedPipeline.Stages[0].Tasks[0]

			assert.Equal(t, tc.expectedTask, taskToCheck)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_unscheduleTask(t *testing.T) {
	validTaskPath := "pipeline.stage.s1.task.t1"

	type testCase struct {
		input           *UnscheduleTaskEvent
		task            *models.PipelineTask
		expectedTask    *models.PipelineTask
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "should clear schedule for a task in the ready state",
			input: &UnscheduleTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.ReadyNodeStatus,
				When:               models.ManualPipelineTask,
				ScheduledStartTime: ptr.Time(time.Now()),
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * * ",
				},
			},
			expectedTask: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.ReadyNodeStatus,
				When:   models.ManualPipelineTask,
			},
		},
		{
			name: "should clear schedule for a task in the waiting state",
			input: &UnscheduleTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.WaitingNodeStatus,
				When:               models.ManualPipelineTask,
				ScheduledStartTime: ptr.Time(time.Now()),
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * * ",
				},
			},
			expectedTask: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.ReadyNodeStatus,
				When:   models.ManualPipelineTask,
			},
		},
		{
			name: "should return error because task cannot be in the running state when clearing the schedule",
			input: &UnscheduleTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.RunningNodeStatus,
				When:   models.ManualPipelineTask,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						Tasks: []*models.PipelineTask{
							tc.task,
						},
					},
				},
			}

			if tc.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
					return pipeline, nil
				}, nil)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.unscheduleTask(ctx, pipeline, tc.input)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			taskToCheck := updatedPipeline.Stages[0].Tasks[0]

			assert.Equal(t, tc.expectedTask, taskToCheck)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_scheduleNestedPipeline(t *testing.T) {
	validNodePath := "pipeline.stage.s1.pipeline.p1"
	cronExpression := fmt.Sprintf("* %d * * *", (time.Now().Hour()+2)%24)

	// This will be used in the tests to check the scheduled time
	exp, err := cronexpr.Parse(cronExpression)
	require.NoError(t, err)

	// Get next time based on the cron expression
	scheduledStartTime := exp.Next(time.Now().UTC()).UTC()
	require.NoError(t, err)

	type testCase struct {
		input                  *ScheduleNestedPipelineEvent
		nestedPipeline         *models.NestedPipeline
		expectedNestedPipeline *models.NestedPipeline
		expectErrorCode        errors.CodeType
		name                   string
		whenCondition          models.PipelineWhenCondition
	}

	testCases := []testCase{
		{
			name: "should set scheduled start time for a nested pipeline in the ready state",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
				ScheduledStartTime: &scheduledStartTime,
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.ReadyNodeStatus,
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:               "t1",
				Path:               validNodePath,
				Status:             statemachine.WaitingNodeStatus,
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should set scheduled start time for a nested pipeline in the created state",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
				ScheduledStartTime: &scheduledStartTime,
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.CreatedNodeStatus,
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:               "t1",
				Path:               validNodePath,
				Status:             statemachine.CreatedNodeStatus,
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should set cron schedule for a nested pipeline in the created state",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:               "t1",
				Path:               validNodePath,
				Status:             statemachine.CreatedNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.CreatedNodeStatus,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
		},
		{
			name: "should set cron schedule for a nested pipeline in the approval pending state",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:               "t1",
				Path:               validNodePath,
				Status:             statemachine.ApprovalPendingNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.ApprovalPendingNodeStatus,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should set cron schedule for a nested pipeline in the ready pending state",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:               "t1",
				Path:               validNodePath,
				Status:             statemachine.ReadyNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.WaitingNodeStatus,
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
				ScheduledStartTime: &scheduledStartTime,
			},
		},
		{
			name: "should return error because both scheduled start time and cron schedule are not specified",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "should return error because both scheduled start time and cron schedule cannot be specified",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
				ScheduledStartTime: ptr.Time(time.Now()),
				CronSchedule: &models.CronSchedule{
					Expression: cronExpression,
				},
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "should return error because nested pipeline cannot be in the running state when setting the schedule",
			input: &ScheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
				ScheduledStartTime: &scheduledStartTime,
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							tc.nestedPipeline,
						},
					},
				},
			}

			if tc.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
					return pipeline, nil
				}, nil)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.scheduleNestedPipeline(ctx, pipeline, tc.input)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			nodeToCheck := pipeline.Stages[0].NestedPipelines[0]

			assert.Equal(t, tc.expectedNestedPipeline, nodeToCheck)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_unscheduleNestedPipeline(t *testing.T) {
	validNodePath := "pipeline.stage.s1.pipeline.p1"

	type testCase struct {
		input                  *UnscheduleNestedPipelineEvent
		nestedPipeline         *models.NestedPipeline
		expectedNestedPipeline *models.NestedPipeline
		expectErrorCode        errors.CodeType
		name                   string
		whenCondition          models.PipelineWhenCondition
	}

	testCases := []testCase{
		{
			name: "should clear schedule for a nested pipeline in the ready state",
			input: &UnscheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:               "t1",
				Path:               validNodePath,
				Status:             statemachine.ReadyNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * * ",
				},
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.ReadyNodeStatus,
			},
		},
		{
			name: "should clear schedule for a nested pipeline in the waiting state",
			input: &UnscheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:               "t1",
				Path:               validNodePath,
				Status:             statemachine.WaitingNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * * ",
				},
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.ReadyNodeStatus,
			},
		},
		{
			name: "should return error because nested pipeline cannot be in the running state when clearing the schedule",
			input: &UnscheduleNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			whenCondition: models.ManualPipeline,
			nestedPipeline: &models.NestedPipeline{
				Name:   "t1",
				Path:   validNodePath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							tc.nestedPipeline,
						},
					},
				},
			}

			if tc.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
					return pipeline, nil
				}, nil)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.unscheduleNestedPipeline(ctx, pipeline, tc.input)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			nodeToCheck := pipeline.Stages[0].NestedPipelines[0]

			assert.Equal(t, tc.expectedNestedPipeline, nodeToCheck)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_initializeTask(t *testing.T) {
	validTaskPath := "pipeline.stage.s1.task.t1"
	approvalRuleIDs := []string{"approval-rule-1"}

	type testCase struct {
		input           *InitializeTaskEvent
		task            *models.PipelineTask
		expectStatus    statemachine.NodeStatus
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "event contains an error",
			input: &InitializeTaskEvent{
				Error:    errors.New("failed to initialize", errors.WithErrorCode(errors.EInvalid)),
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.InitializingNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
			},
			expectStatus: statemachine.FailedNodeStatus,
		},
		{
			name: "event contains approval rule IDs",
			input: &InitializeTaskEvent{
				TaskPath:        validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.InitializingNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
			},
			expectStatus: statemachine.ApprovalPendingNodeStatus,
		},
		{
			name: "event has no approval rule IDs or error",
			input: &InitializeTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.InitializingNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
			},
			expectStatus: statemachine.ReadyNodeStatus,
		},
		{
			name: "event has approval rule IDs but pipeline is already approved",
			input: &InitializeTaskEvent{
				TaskPath:        validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.InitializingNodeStatus,
				ApprovalStatus: models.ApprovalApproved,
			},
			expectStatus: statemachine.ReadyNodeStatus,
		},
		{
			name: "task does not require approvals and has a cron schedule",
			input: &InitializeTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.InitializingNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			expectStatus: statemachine.WaitingNodeStatus,
		},
		{
			name: "task does require approval and has a cron schedule so it should go to the approval pending state",
			input: &InitializeTaskEvent{
				TaskPath:        validTaskPath,
				ApprovalRuleIDs: approvalRuleIDs,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.InitializingNodeStatus,
				ApprovalStatus: models.ApprovalPending,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			expectStatus: statemachine.ApprovalPendingNodeStatus,
		},
		{
			name: "task does not require approvals and has a scheduled start time",
			input: &InitializeTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.InitializingNodeStatus,
				ApprovalStatus:     models.ApprovalNotRequired,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectStatus: statemachine.WaitingNodeStatus,
		},
		{
			name: "task doesn't exist",
			input: &InitializeTaskEvent{
				TaskPath: "pipeline.stage.s1.task.invalid",
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.ReadyNodeStatus,
				ApprovalStatus: models.ApprovalApproved,
			},
			expectErrorCode: errors.EInternal,
		},
		{
			name: "task is not in initializing state",
			input: &InitializeTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:           "t1",
				Path:           validTaskPath,
				Status:         statemachine.CreatedNodeStatus,
				ApprovalStatus: models.ApprovalNotRequired,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks:  []*models.PipelineTask{test.task},
					},
				},
			}

			if test.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(p *models.Pipeline) bool {
					task, ok := p.GetTask(test.input.TaskPath)
					if !ok {
						return false
					}

					if !task.Status.Equals(test.expectStatus) {
						// Status not the same.
						return false
					}

					if task.Status.IsFinalStatus() {
						// Should only go into final status if there is an error on the event.
						return task.LastAttemptFinishedAt != nil && len(task.Errors) > 0
					}

					if task.Status.Equals(statemachine.ApprovalPendingNodeStatus) {
						// Should go into APPROVAL_PENDING status.
						return slices.Equal(task.ApprovalRuleIDs, test.input.ApprovalRuleIDs) &&
							task.ApprovalStatus == models.ApprovalPending
					}

					return true
				})).Return(pipeline, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				logger:   logger,
				dbClient: dbClient,
			}

			updatedPipeline, statusChanges, err := updater.initializeTask(ctx, pipeline, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			require.Len(t, statusChanges, 1)
		})
	}
}

func TestUpdater_initializeNestedPipeline(t *testing.T) {
	validPipelinePath := "pipeline.stage.s1.pipeline.p1"
	approvalRuleIDs := []string{"approval-rule-1"}
	latestPipelineID := "latest-pipeline-1"

	type testCase struct {
		input           *InitializeNestedPipelineEvent
		nestedPipeline  *models.NestedPipeline
		expectStatus    statemachine.NodeStatus
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "event contains an error",
			input: &InitializeNestedPipelineEvent{
				Error:              errors.New("failed to initialize", errors.WithErrorCode(errors.EInvalid)),
				NestedPipelinePath: validPipelinePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.InitializingNodeStatus,
				ApprovalStatus:   models.ApprovalNotRequired,
				LatestPipelineID: latestPipelineID,
			},
			expectStatus: statemachine.FailedNodeStatus,
		},
		{
			name: "event contains approval rule IDs",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: validPipelinePath,
				ApprovalRuleIDs:    approvalRuleIDs,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.InitializingNodeStatus,
				ApprovalStatus:   models.ApprovalNotRequired,
				LatestPipelineID: latestPipelineID,
			},
			expectStatus: statemachine.ApprovalPendingNodeStatus,
		},
		{
			name: "event has no approval rule IDs or error",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: validPipelinePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.InitializingNodeStatus,
				ApprovalStatus:   models.ApprovalNotRequired,
				LatestPipelineID: latestPipelineID,
			},
			expectStatus: statemachine.ReadyNodeStatus,
		},
		{
			name: "event has approval rule IDs but pipeline is already approved",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: validPipelinePath,
				ApprovalRuleIDs:    approvalRuleIDs,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.InitializingNodeStatus,
				ApprovalStatus:   models.ApprovalApproved,
				LatestPipelineID: latestPipelineID,
			},
			expectStatus: statemachine.ReadyNodeStatus,
		},
		{
			name: "pipeline does not require approval but has a cron schedule",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: validPipelinePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.InitializingNodeStatus,
				ApprovalStatus:   models.ApprovalNotRequired,
				LatestPipelineID: latestPipelineID,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			expectStatus: statemachine.WaitingNodeStatus,
		},
		{
			name: "pipeline does not require approval but has a scheduled start time",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: validPipelinePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:               "p1",
				Path:               "pipeline.stage.s1.pipeline.p1",
				Status:             statemachine.InitializingNodeStatus,
				ApprovalStatus:     models.ApprovalNotRequired,
				LatestPipelineID:   latestPipelineID,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectStatus: statemachine.WaitingNodeStatus,
		},
		{
			name: "pipeline has a cron schedule but it requires approval so it'll go to the approval pending state",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: validPipelinePath,
				ApprovalRuleIDs:    approvalRuleIDs,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.InitializingNodeStatus,
				ApprovalStatus:   models.ApprovalPending,
				LatestPipelineID: latestPipelineID,
				CronSchedule: &models.CronSchedule{
					Expression: "* * * * *",
				},
			},
			expectStatus: statemachine.ApprovalPendingNodeStatus,
		},
		{
			name: "nested pipeline doesn't exist",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: "pipeline.stage.s1.pipeline.invalid",
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.InitializingNodeStatus,
				ApprovalStatus:   models.ApprovalNotRequired,
				LatestPipelineID: latestPipelineID,
			},
			expectErrorCode: errors.EInternal,
		},
		{
			name: "nested pipeline is not in initializing state",
			input: &InitializeNestedPipelineEvent{
				NestedPipelinePath: validPipelinePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:             "p1",
				Path:             "pipeline.stage.s1.pipeline.p1",
				Status:           statemachine.CreatedNodeStatus,
				ApprovalStatus:   models.ApprovalNotRequired,
				LatestPipelineID: latestPipelineID,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:            "s1",
						Path:            "pipeline.stage.s1",
						Status:          statemachine.RunningNodeStatus,
						NestedPipelines: []*models.NestedPipeline{test.nestedPipeline},
					},
				},
			}

			if test.expectErrorCode == "" {
				if test.expectStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
					mockPipelines.On("GetPipelineByID", mock.Anything, latestPipelineID).Return(&models.Pipeline{
						Metadata: models.ResourceMetadata{
							ID: latestPipelineID,
						},
						ApprovalStatus:  test.nestedPipeline.ApprovalStatus,
						ApprovalRuleIDs: []string{},
						Status:          test.nestedPipeline.Status,
						Type:            models.NestedPipelineType,
					}, nil)

					// This is for the nested pipeline.
					mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(p *models.Pipeline) bool {
						return p.Metadata.ID == latestPipelineID &&
							slices.Equal(p.ApprovalRuleIDs, approvalRuleIDs) &&
							p.ApprovalStatus == models.ApprovalPending
					})).Return(pipeline, nil).Once()
				}

				// This is for the parent pipeline.
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(p *models.Pipeline) bool {
					pipelineNode, ok := p.GetNestedPipeline(test.input.NestedPipelinePath)
					if !ok {
						return false
					}

					if !pipelineNode.Status.Equals(test.expectStatus) {
						// Status not the same.
						return false
					}

					if pipelineNode.Status.IsFinalStatus() {
						// Should only go into final status if there is an error on the event.
						return len(pipelineNode.Errors) > 0
					}

					if pipelineNode.Status.Equals(statemachine.ApprovalPendingNodeStatus) {
						// Should go into APPROVAL_PENDING status.
						return pipelineNode.ApprovalStatus == models.ApprovalPending
					}

					return true
				})).Return(pipeline, nil).Once()
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				logger:   logger,
				dbClient: dbClient,
			}

			updatedPipeline, statusChanges, err := updater.initializeNestedPipeline(ctx, pipeline, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			require.Len(t, statusChanges, 1)
		})
	}
}

func TestUpdater_deferNestedPipeline(t *testing.T) {
	validNodePath := "pipeline.stage.s1.pipeline.p1"

	type testCase struct {
		input           *DeferNestedPipelineEvent
		nestedPipeline  *models.NestedPipeline
		expectStatus    statemachine.NodeStatus
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "should defer nested pipeline in the waiting state",
			input: &DeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:               "p1",
				Path:               validNodePath,
				Status:             statemachine.WaitingNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should defer nested pipeline in the ready state",
			input: &DeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.ReadyNodeStatus,
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should return error because nested pipeline is in the running state",
			input: &DeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "should clear errors when deferring a nested pipeline",
			input: &DeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.WaitingNodeStatus,
				Errors: []string{"error"},
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should clear scheduled start time when deferring a nested pipeline",
			input: &DeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:               "p1",
				Path:               validNodePath,
				Status:             statemachine.WaitingNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should clear cron schedule when deferring a nested pipeline",
			input: &DeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:         "p1",
				Path:         validNodePath,
				Status:       statemachine.WaitingNodeStatus,
				CronSchedule: &models.CronSchedule{Expression: "* * * * *"},
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "nested pipeline doesn't exist",
			input: &DeferNestedPipelineEvent{
				NestedPipelinePath: "pipeline.stage.s1.pipeline.invalid",
			},
			nestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.ReadyNodeStatus,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:            "s1",
						Path:            "pipeline.stage.s1",
						Status:          statemachine.RunningNodeStatus,
						NestedPipelines: []*models.NestedPipeline{test.nestedPipeline},
					},
				},
			}

			if test.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(p *models.Pipeline) bool {
					return p.Stages[0].NestedPipelines[0].Status == test.expectStatus &&
						p.Stages[0].NestedPipelines[0].ScheduledStartTime == nil &&
						p.Stages[0].NestedPipelines[0].CronSchedule == nil &&
						len(p.Stages[0].NestedPipelines[0].Errors) == 0
				})).Return(pipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, statusChanges, err := updater.deferNestedPipeline(ctx, pipeline, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			require.Len(t, statusChanges, 1)
		})
	}
}

func TestUpdater_deferTask(t *testing.T) {
	validTaskPath := "pipeline.stage.s1.task.t1"

	type testCase struct {
		input           *DeferTaskEvent
		task            *models.PipelineTask
		expectStatus    statemachine.NodeStatus
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "should defer task in the waiting state",
			input: &DeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.WaitingNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should defer task in the ready state",
			input: &DeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.ReadyNodeStatus,
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should clear errors when deferring a task",
			input: &DeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.WaitingNodeStatus,
				Errors: []string{"error"},
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should clear scheduled start time when deferring a task",
			input: &DeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:               "t1",
				Path:               validTaskPath,
				Status:             statemachine.WaitingNodeStatus,
				ScheduledStartTime: ptr.Time(time.Now()),
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should clear cron schedule when deferring a task",
			input: &DeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:         "t1",
				Path:         validTaskPath,
				Status:       statemachine.WaitingNodeStatus,
				CronSchedule: &models.CronSchedule{Expression: "* * * * *"},
			},
			expectStatus: statemachine.DeferredNodeStatus,
		},
		{
			name: "should return error because task is in the running state",
			input: &DeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "task doesn't exist",
			input: &DeferTaskEvent{
				TaskPath: "pipeline.stage.s1.task.invalid",
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.ReadyNodeStatus,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks:  []*models.PipelineTask{test.task},
					},
				},
			}

			if test.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline", mock.Anything, mock.MatchedBy(func(p *models.Pipeline) bool {
					return p.Stages[0].Tasks[0].Status == test.expectStatus &&
						p.Stages[0].Tasks[0].ScheduledStartTime == nil &&
						p.Stages[0].Tasks[0].CronSchedule == nil &&
						len(p.Stages[0].Tasks[0].Errors) == 0
				})).Return(pipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, statusChanges, err := updater.deferTask(ctx, pipeline, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)
			require.Len(t, statusChanges, 1)
		})
	}
}

func TestUpdater_undeferNestedPipeline(t *testing.T) {
	validNodePath := "pipeline.stage.s1.pipeline.p1"

	type testCase struct {
		input                  *UndeferNestedPipelineEvent
		nestedPipeline         *models.NestedPipeline
		expectedNestedPipeline *models.NestedPipeline
		expectErrorCode        errors.CodeType
		name                   string
	}

	testCases := []testCase{
		{
			name: "should undefer nested pipeline in the deferred state",
			input: &UndeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.DeferredNodeStatus,
			},
			expectedNestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.InitializingNodeStatus,
			},
		},
		{
			name: "should return error because nested pipeline is not in the deferred state",
			input: &UndeferNestedPipelineEvent{
				NestedPipelinePath: validNodePath,
			},
			nestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "nested pipeline doesn't exist",
			input: &UndeferNestedPipelineEvent{
				NestedPipelinePath: "pipeline.stage.s1.pipeline.invalid",
			},
			nestedPipeline: &models.NestedPipeline{
				Name:   "p1",
				Path:   validNodePath,
				Status: statemachine.DeferredNodeStatus,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.SucceededNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.SucceededNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							test.nestedPipeline,
						},
					},
				},
			}

			if test.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline",
					mock.Anything,
					mock.MatchedBy(func(p *models.Pipeline) bool {
						return p.Stages[0].NestedPipelines[0].Status == test.expectedNestedPipeline.Status
					}),
				).Return(pipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.undeferNestedPipeline(ctx, pipeline, test.input)

			if test.expectErrorCode != "" {
				require.Error(t, err)
				require.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			nodeToCheck := updatedPipeline.Stages[0].NestedPipelines[0]

			assert.Equal(t, test.expectedNestedPipeline, nodeToCheck)
			assert.NotEmpty(t, changes)
		})
	}
}

func TestUpdater_undeferTask(t *testing.T) {
	validTaskPath := "pipeline.stage.s1.task.t1"

	type testCase struct {
		input           *UndeferTaskEvent
		task            *models.PipelineTask
		expectedTask    *models.PipelineTask
		expectErrorCode errors.CodeType
		name            string
	}

	testCases := []testCase{
		{
			name: "should undefer task in the deferred state",
			input: &UndeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.DeferredNodeStatus,
			},
			expectedTask: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.InitializingNodeStatus,
			},
		},
		{
			name: "should return error because task is not in the deferred state",
			input: &UndeferTaskEvent{
				TaskPath: validTaskPath,
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.RunningNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "task doesn't exist",
			input: &UndeferTaskEvent{
				TaskPath: "pipeline.stage.s1.task.invalid",
			},
			task: &models.PipelineTask{
				Name:   "t1",
				Path:   validTaskPath,
				Status: statemachine.DeferredNodeStatus,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-id",
				},
				Status: statemachine.RunningNodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.ReadyNodeStatus,
						Tasks: []*models.PipelineTask{
							test.task,
						},
					},
				},
			}

			if test.expectErrorCode == "" {
				mockPipelines.On("UpdatePipeline",
					mock.Anything,
					mock.MatchedBy(func(p *models.Pipeline) bool {
						return p.Stages[0].Tasks[0].Status == test.expectedTask.Status
					}),
				).Return(pipeline, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			updater := &updater{
				dbClient: dbClient,
			}

			updatedPipeline, changes, err := updater.undeferTask(ctx, pipeline, test.input)

			if test.expectErrorCode != "" {
				require.Error(t, err)
				require.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, updatedPipeline)

			taskToCheck := updatedPipeline.Stages[0].Tasks[0]

			assert.Equal(t, test.expectedTask, taskToCheck)
			assert.NotEmpty(t, changes)
		})
	}
}
