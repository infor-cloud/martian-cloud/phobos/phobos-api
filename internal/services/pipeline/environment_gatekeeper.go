package pipeline

//go:generate go tool mockery --name EnvironmentGatekeeper --inpackage --case underscore

import (
	"context"
	"slices"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

// EnvironmentGatekeeper implements the gatekeeper for PipelineCreate operations.
type EnvironmentGatekeeper interface {
	checkEnvironmentRules(ctx context.Context, projectID string, environmentName string) error
}

type environmentGatekeeper struct {
	dbClient *db.Client
}

// NewEnvironmentGatekeeper returns an instance of environmentGatekeeper
func NewEnvironmentGatekeeper(
	dbClient *db.Client,
) EnvironmentGatekeeper {
	return &environmentGatekeeper{
		dbClient: dbClient,
	}
}

// checkEnvironmentRules returns true iff the applicable environment protection rules are satisfied.
// To pass this check, if there is an org rule for the specified environment name, it must be satisfied.
// Also, if there is a project rule for the specified environment name, it must be satisfied.
func (c *environmentGatekeeper) checkEnvironmentRules(ctx context.Context, projectID string, environmentName string) error {

	// Based on the caller, get the user or service account name involved.
	var callerUser *models.User
	var callerServiceAccount *models.ServiceAccount

	err := auth.HandleCaller(ctx,
		func(_ context.Context, caller *auth.UserCaller) error {
			callerUser = caller.User
			return nil
		},
		func(ctx context.Context, caller *auth.ServiceAccountCaller) error {
			var err error

			callerServiceAccount, err = c.dbClient.ServiceAccounts.GetServiceAccountByPRN(ctx, caller.ServiceAccountPRN)
			if err != nil {
				return err
			}

			return nil
		},
	)
	if err != nil {
		return err
	}

	// Any other type of caller is forbidden.
	if (callerUser == nil) && (callerServiceAccount == nil) {
		return errors.New("invalid caller type to satisfy environment protection rule",
			errors.WithErrorCode(errors.EForbidden))
	}

	// Pointer to the calling user's ID.
	var callerUserID *string
	if callerUser != nil {
		callerUserID = &callerUser.Metadata.ID
	}

	// Pointer to the calling service account's ID.
	var callerServiceAccountID *string
	if callerServiceAccount != nil {
		callerServiceAccountID = &callerServiceAccount.Metadata.ID
	}

	// Get the project from the ID.
	project, err := c.dbClient.Projects.GetProjectByID(ctx, projectID)
	if err != nil {
		return err
	}
	if project == nil {
		return errors.New("project with ID %s not found", projectID, errors.WithErrorCode(errors.ENotFound))
	}

	// Get the rules for the organization and project.
	rules, err := c.dbClient.EnvironmentRules.GetEnvironmentRules(ctx, &db.GetEnvironmentRulesInput{
		Filter: &db.EnvironmentRuleFilter{
			OrgID:                 &project.OrgID,
			ProjectID:             &project.Metadata.ID,
			EnvironmentRuleScopes: []models.ScopeType{models.OrganizationScope, models.ProjectScope},
			EnvironmentName:       &environmentName,
		},
	})
	if err != nil {
		return err
	}

	// Get memberships (including org and project and indirect via teams for a user caller).
	membershipsResult, mErr := c.dbClient.Memberships.GetMemberships(ctx, &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			ProjectID:        &project.Metadata.ID,
			UserID:           callerUserID,
			ServiceAccountID: callerServiceAccountID,
			MembershipScopes: []models.ScopeType{models.OrganizationScope, models.ProjectScope},
		},
	})
	if mErr != nil {
		return mErr
	}
	memberships := membershipsResult.Memberships

	// Resolve the teams the user is a member of.
	var teams []models.Team
	if callerUser != nil {
		teamsResult, err := c.dbClient.Teams.GetTeams(ctx, &db.GetTeamsInput{
			Filter: &db.TeamFilter{
				UserID: &callerUser.Metadata.ID,
			},
		})
		if err != nil {
			return err
		}

		teams = teamsResult.Teams
	}

	// A service account cannot be a member of a team, so there is no need to check that.

	// Get the role IDs the user, service account, and teams are allowed to use.
	var roleIDs []string
	for _, m := range memberships {
		// The memberships query was run on the specific user or service account caller,
		// so we know any membership we get here matches the caller.
		// For a user caller, the memberships already include roles for the user as a members of a team.
		roleIDs = append(roleIDs, m.RoleID)
	}

	// Extract team IDs from teams.
	teamIDs := []string{}
	for _, t := range teams {
		teamIDs = append(teamIDs, t.Metadata.ID)
	}

	// There should be at most two rules: one for org level, one for project level.
	// Verify that each environment rule allows the caller.
	for _, rule := range rules.EnvironmentRules {
		if !c.checkEnvironmentRule(rule, callerUserID, callerServiceAccountID, teamIDs, roleIDs) {
			ruleHostType := "project"
			ruleHostName := project.Name
			if rule.Scope.Equals(models.OrganizationScope) {
				ruleHostType = "organization"
				ruleHostName = project.GetOrgName()
			}

			var callerType, callerName string
			switch {
			// default case should never happen
			case callerUser != nil:
				callerType = "user"
				callerName = callerUser.Username
			case callerServiceAccount != nil:
				callerType = "service account"
				callerName = callerServiceAccount.Name
			}

			return errors.New(
				"deployment can't be executed in environment %s because environment rule in %s %s is not satisfied by %s %s",
				environmentName,
				ruleHostType,
				ruleHostName,
				callerType,
				callerName,
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	}

	// If neither rule detected a violation, that means the check passed.
	return nil
}

// checkEnvironmentRule checks whether the specified rule is satisfied by the caller's user, teams, and/or roles.
// If no rules, return nil.  If any rule is satisfied, return nil.
// If there is at least one rule but none is satisfied, return an error.
func (c *environmentGatekeeper) checkEnvironmentRule(
	rule *models.EnvironmentRule,
	callerUserID, callerServiceAccountID *string, teamIDs, roleIDs []string,
) bool {

	// User
	if callerUserID != nil {
		if slices.Contains(rule.UserIDs, *callerUserID) {
			return true
		}
	}

	// Service accounts
	if callerServiceAccountID != nil {
		if slices.Contains(rule.ServiceAccountIDs, *callerServiceAccountID) {
			return true
		}
	}

	// Teams
	for _, t := range teamIDs {
		if slices.Contains(rule.TeamIDs, t) {
			return true
		}
	}

	// Roles
	for _, r := range roleIDs {
		if slices.Contains(rule.RoleIDs, r) {
			return true
		}
	}

	// No match means the check failed.
	return false
}
