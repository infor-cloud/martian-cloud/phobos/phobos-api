package pipeline

import (
	"context"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNewSupervisor(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockUpdater := NewMockUpdater(t)
	eventManager := events.NewEventManager(dbClient, logger)

	expected := &Supervisor{
		logger:          logger,
		dbClient:        dbClient,
		pipelineUpdater: mockUpdater,
		eventManager:    eventManager,
	}

	assert.Equal(t, expected, NewSupervisor(mockUpdater, dbClient, eventManager, logger))
}

func TestStart(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockUpdater := NewMockUpdater(t)
	eventManager := events.NewEventManager(dbClient, logger)

	mockUpdater.On("RegisterEventHandler", mock.Anything)

	supervisor := &Supervisor{
		logger:          logger,
		dbClient:        dbClient,
		pipelineUpdater: mockUpdater,
		eventManager:    eventManager,
	}

	supervisor.Start(ctx)
}

func TestSupervisor_checkWaitingNodes(t *testing.T) {
	pipelineID := "pipelineID"
	jobID := "jobID"

	type testCase struct {
		name                        string
		pipelines                   []models.Pipeline
		expectedNestedPipelinePaths []string
		expectedTaskPaths           []string
	}

	tests := []testCase{
		{
			name:                        "waiting tasks and nested pipelines",
			expectedNestedPipelinePaths: []string{"pipeline.stage.s1.pipeline.p1"},
			expectedTaskPaths:           []string{"pipeline.stage.s1.task.t1"},
			pipelines: []models.Pipeline{
				{
					Metadata: models.ResourceMetadata{
						ID: pipelineID,
					},
					Stages: []*models.PipelineStage{
						{
							Name: "s1",
							Path: "pipeline.stage.s1",
							Tasks: []*models.PipelineTask{
								{
									Path:                  "pipeline.stage.s1.task.t1",
									Status:                statemachine.WaitingNodeStatus,
									When:                  models.AutoPipelineTask,
									LatestJobID:           &jobID,
									Interval:              ptr.Duration(time.Minute),
									MaxAttempts:           3,
									AttemptCount:          1,
									LastAttemptFinishedAt: ptr.Time(time.Now().Add(-time.Minute * 5)),
								},
							},
						},
					},
				},
				{
					Metadata: models.ResourceMetadata{
						ID: pipelineID,
					},
					Stages: []*models.PipelineStage{
						{
							Name: "s1",
							Path: "pipeline.stage.s1",
							NestedPipelines: []*models.NestedPipeline{
								{
									Path:               "pipeline.stage.s1.pipeline.p1",
									Status:             statemachine.WaitingNodeStatus,
									ScheduledStartTime: ptr.Time(time.Now().Add(-time.Minute)),
								},
							},
						},
					},
				},
			},
		},
		{
			name:              "should run scheduled task since its scheduled start time is in the past",
			expectedTaskPaths: []string{"pipeline.stage.s1.task.t1"},
			pipelines: []models.Pipeline{
				{
					Metadata: models.ResourceMetadata{
						ID: pipelineID,
					},
					Stages: []*models.PipelineStage{
						{
							Name: "s1",
							Path: "pipeline.stage.s1",
							Tasks: []*models.PipelineTask{
								{
									Path:               "pipeline.stage.s1.task.t1",
									Status:             statemachine.WaitingNodeStatus,
									When:               models.AutoPipelineTask,
									LatestJobID:        &jobID,
									MaxAttempts:        1,
									AttemptCount:       0,
									ScheduledStartTime: ptr.Time(time.Now().Add(-time.Minute)),
								},
							},
						},
					},
				},
			},
		},
		{
			name:              "should not run scheduled task since its scheduled start time has not passed",
			expectedTaskPaths: []string{},
			pipelines: []models.Pipeline{
				{
					Metadata: models.ResourceMetadata{
						ID: pipelineID,
					},
					Stages: []*models.PipelineStage{
						{
							Name: "s1",
							Path: "pipeline.stage.s1",
							Tasks: []*models.PipelineTask{
								{
									Path:               "pipeline.stage.s1.task.t1",
									Status:             statemachine.WaitingNodeStatus,
									When:               models.AutoPipelineTask,
									LatestJobID:        &jobID,
									MaxAttempts:        1,
									AttemptCount:       0,
									ScheduledStartTime: ptr.Time(time.Now().Add(time.Minute)),
								},
							},
						},
					},
				},
			},
		},
		{
			name:              "should not run waiting task since its interval has not passed",
			expectedTaskPaths: []string{},
			pipelines: []models.Pipeline{
				{
					Metadata: models.ResourceMetadata{
						ID: pipelineID,
					},
					Stages: []*models.PipelineStage{
						{
							Name: "s1",
							Path: "pipeline.stage.s1",
							Tasks: []*models.PipelineTask{
								{
									Path:                  "pipeline.stage.s1.task.t1",
									Status:                statemachine.WaitingNodeStatus,
									When:                  models.AutoPipelineTask,
									LatestJobID:           &jobID,
									Interval:              ptr.Duration(5 * time.Minute),
									MaxAttempts:           5,
									AttemptCount:          1,
									LastAttemptFinishedAt: ptr.Time(time.Now().Add(-time.Minute)),
								},
							},
						},
					},
				},
			},
		},
		{
			name:      "no waiting nodes",
			pipelines: []models.Pipeline{},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockUpdater := NewMockUpdater(t)
			mockJobs := db.NewMockJobs(t)

			mockPipelines.On("GetPipelines", mock.Anything, &db.GetPipelinesInput{
				Filter: &db.PipelineFilter{
					NodeTypes:    []statemachine.NodeType{statemachine.TaskNodeType, statemachine.PipelineNodeType},
					NodeStatuses: []statemachine.NodeStatus{statemachine.WaitingNodeStatus},
				},
			}).Return(&db.PipelinesResult{
				Pipelines: test.pipelines,
			}, nil)

			for _, path := range test.expectedTaskPaths {
				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &RunTaskEvent{TaskPath: path}).
					Return(func(_ context.Context, id string, event any) (*models.Pipeline, error) {
						require.Equal(t, pipelineID, id)
						require.IsType(t, &RunTaskEvent{}, event)

						return nil, nil
					})
			}

			for _, path := range test.expectedNestedPipelinePaths {
				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &RunNestedPipelineEvent{NestedPipelinePath: path}).
					Return(func(_ context.Context, id string, event any) (*models.Pipeline, error) {
						require.Equal(t, pipelineID, id)
						require.IsType(t, &RunNestedPipelineEvent{}, event)

						return nil, nil
					})
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Pipelines: mockPipelines,
				Jobs:      mockJobs,
			}

			supervisor := &Supervisor{
				logger:          logger,
				dbClient:        dbClient,
				pipelineUpdater: mockUpdater,
				eventManager:    events.NewEventManager(dbClient, logger),
			}

			require.NoError(t, supervisor.checkWaitingNodes(ctx))
		})
	}
}

func TestSupervisor_getPipelineByID(t *testing.T) {
	pipelineID := "pipelineID"

	type testCase struct {
		name            string
		pipeline        *models.Pipeline
		expectErrorCode errors.CodeType
	}

	tests := []testCase{
		{
			name: "successfully get pipeline",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
			},
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(test.pipeline, nil)

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			supervisor := &Supervisor{
				dbClient: dbClient,
			}

			pipeline, err := supervisor.getPipelineByID(ctx, pipelineID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pipeline, pipeline)
		})
	}
}

func TestSupervisor_runReadyNodes(t *testing.T) {
	pipelineID := "pipelineID"
	manualNestedPipelineID := "manualNestedPipelineID"
	autoNestedPipelineID := "autoNestedPipelineID"

	type testCase struct {
		name       string
		nodeStatus statemachine.NodeStatus
	}

	tests := []testCase{
		{
			name:       "no ready nodes",
			nodeStatus: statemachine.RunningNodeStatus,
		},
		{
			name:       "ready nodes",
			nodeStatus: statemachine.ReadyNodeStatus,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockUpdater := NewMockUpdater(t)

			samplePipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				Status: test.nodeStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: test.nodeStatus,
						Tasks: []*models.PipelineTask{
							// Do a task with manual and auto when to make sure manual tasks are skipped
							{
								Name:   "t1",
								Path:   "pipeline.stage.s1.task.t1",
								Status: test.nodeStatus,
								When:   models.ManualPipelineTask,
							},
							{
								Name:   "t2",
								Path:   "pipeline.stage.s1.task.t2",
								Status: test.nodeStatus,
								When:   models.AutoPipelineTask,
							},
						},
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "p1",
								Path:             "pipeline.stage.s1.pipeline.p1",
								Status:           test.nodeStatus,
								LatestPipelineID: manualNestedPipelineID,
							},
							{
								Name:             "p2",
								Path:             "pipeline.stage.s1.pipeline.p2",
								Status:           test.nodeStatus,
								LatestPipelineID: autoNestedPipelineID,
							},
						},
					},
				},
			}

			if test.nodeStatus.Equals(statemachine.ReadyNodeStatus) {
				// Mock twice for auto and manual nested pipelines
				mockPipelines.On("GetPipelineByID", mock.Anything, autoNestedPipelineID).Return(&models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: autoNestedPipelineID,
					},
					Status: test.nodeStatus,
					When:   models.AutoPipeline,
				}, nil).Once()

				mockPipelines.On("GetPipelineByID", mock.Anything, manualNestedPipelineID).Return(&models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: manualNestedPipelineID,
					},
					Status: test.nodeStatus,
					When:   models.ManualPipeline,
				}, nil).Once()

				// Only mock the event for the auto task and nested pipeline.
				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &RunTaskEvent{
					TaskPath: "pipeline.stage.s1.task.t2",
				}).Return(&models.Pipeline{}, nil).Once()

				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &RunNestedPipelineEvent{
					NestedPipelinePath: "pipeline.stage.s1.pipeline.p2",
				}).Return(&models.Pipeline{}, nil).Once()
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			logger, _ := logger.NewForTest()

			supervisor := &Supervisor{
				dbClient:        dbClient,
				pipelineUpdater: mockUpdater,
				logger:          logger,
			}

			require.NoError(t, supervisor.runReadyNodes(ctx, samplePipeline))
		})
	}
}

func TestSupervisor_handleNestedPipelineStatusChange(t *testing.T) {
	parentPipelineID := "parentPipelineID"
	nestedPipelineID := "nestedPipelineID"
	eventPipelineID := "eventPipelineID"

	type testCase struct {
		name                       string
		eventPipelineStatus        statemachine.NodeStatus
		nestedPipelineStatus       statemachine.NodeStatus
		parentPipelineStatus       statemachine.NodeStatus
		expectNestedPipelineUpdate bool
		expectParentPipelineUpdate bool
	}

	tests := []testCase{
		{
			name:                       "nested pipeline skipped; expect both pipelines to update",
			eventPipelineStatus:        statemachine.SkippedNodeStatus,
			nestedPipelineStatus:       statemachine.CreatedNodeStatus,
			parentPipelineStatus:       statemachine.CreatedNodeStatus,
			expectNestedPipelineUpdate: true,
			expectParentPipelineUpdate: true,
		},
		{
			name:                       "nested pipeline reset; expect both pipelines to update",
			eventPipelineStatus:        statemachine.CreatedNodeStatus,
			nestedPipelineStatus:       statemachine.FailedNodeStatus,
			parentPipelineStatus:       statemachine.FailedNodeStatus,
			expectNestedPipelineUpdate: true,
			expectParentPipelineUpdate: true,
		},
		{
			name:                       "nested pipeline ready; expect both pipelines to update",
			eventPipelineStatus:        statemachine.ReadyNodeStatus,
			nestedPipelineStatus:       statemachine.PendingNodeStatus,
			parentPipelineStatus:       statemachine.PendingNodeStatus,
			expectNestedPipelineUpdate: true,
			expectParentPipelineUpdate: true,
		},
		{
			name:                       "nested pipeline approval pending; expect both pipelines to update",
			eventPipelineStatus:        statemachine.ApprovalPendingNodeStatus,
			nestedPipelineStatus:       statemachine.CreatedNodeStatus,
			parentPipelineStatus:       statemachine.CreatedNodeStatus,
			expectNestedPipelineUpdate: true,
			expectParentPipelineUpdate: true,
		},
		{
			name:                       "nested pipeline deferred; expect both pipelines to update",
			eventPipelineStatus:        statemachine.DeferredNodeStatus,
			nestedPipelineStatus:       statemachine.CreatedNodeStatus,
			parentPipelineStatus:       statemachine.CreatedNodeStatus,
			expectNestedPipelineUpdate: true,
			expectParentPipelineUpdate: true,
		},
		{
			name:                       "nested pipeline already updated; expect only parent pipeline to update",
			eventPipelineStatus:        statemachine.ReadyNodeStatus,
			nestedPipelineStatus:       statemachine.ReadyNodeStatus,
			parentPipelineStatus:       statemachine.CreatedNodeStatus,
			expectParentPipelineUpdate: true,
		},
		{
			name:                       "nested pipeline update not required; expect parent pipeline to update",
			eventPipelineStatus:        statemachine.SucceededNodeStatus,
			nestedPipelineStatus:       statemachine.SucceededNodeStatus,
			parentPipelineStatus:       statemachine.RunningNodeStatus,
			expectParentPipelineUpdate: true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockUpdater := NewMockUpdater(t)

			// Parent of the event pipeline
			parentPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: parentPipelineID,
				},
				Status: test.parentPipelineStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: test.parentPipelineStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "p1",
								Path:             "pipeline.stage.s1.pipeline.p1",
								LatestPipelineID: eventPipelineID,
								Status:           test.parentPipelineStatus,
							},
						},
					},
				},
			}

			// Event pipeline
			eventPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: eventPipelineID,
				},
				Status:           test.eventPipelineStatus,
				ParentPipelineID: &parentPipelineID,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.es1",
						Status: test.eventPipelineStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "p1",
								Path:             "pipeline.stage.es1.pipeline.en1",
								LatestPipelineID: nestedPipelineID,
								Status:           test.eventPipelineStatus,
							},
						},
					},
				},
			}

			// Child or nested pipeline of the event pipeline
			childPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ParentPipelineID: &parentPipelineID,
				Status:           test.nestedPipelineStatus,
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, eventPipelineID).Return(eventPipeline, nil).Once()

			switch test.eventPipelineStatus {
			case statemachine.SkippedNodeStatus,
				statemachine.CreatedNodeStatus,
				statemachine.ReadyNodeStatus,
				statemachine.ApprovalPendingNodeStatus,
				statemachine.DeferredNodeStatus:
				// We only expect the nested pipeline to be updated if the event pipeline is matches one of the status' above.
				mockPipelines.On("GetPipelineByID", mock.Anything, nestedPipelineID).Return(childPipeline, nil)
			}

			if test.expectNestedPipelineUpdate {
				mockUpdater.On("ProcessEvent", mock.Anything, nestedPipelineID, &ForceNestedPipelineStatusChangeEvent{
					Status: test.eventPipelineStatus,
				}).Return(&models.Pipeline{}, nil)
			}

			if test.expectParentPipelineUpdate {
				mockPipelines.On("GetPipelineByID", mock.Anything, parentPipelineID).Return(parentPipeline, nil).Once()

				mockUpdater.On("ProcessEvent", mock.Anything, parentPipelineID, &NestedPipelineStatusChangeEvent{
					NestedPipelinePath: "pipeline.stage.s1.pipeline.p1",
					Status:             test.eventPipelineStatus,
				}).Return(&models.Pipeline{}, nil)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			supervisor := &Supervisor{
				dbClient:        dbClient,
				pipelineUpdater: mockUpdater,
			}

			require.NoError(t, supervisor.handleNestedPipelineStatusChange(ctx, eventPipeline.Metadata.ID, nil, nil))
		})
	}
}
