// Package pipeline implements functionality related to Phobos pipelines.
package pipeline

//go:generate go tool mockery --name Service --inpackage --case underscore

import (
	"bytes"
	"context"
	"encoding/json"
	goerrors "errors"
	"fmt"
	"maps"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/hcl/v2"
	"github.com/zclconf/go-cty/cty"
	ctyjson "github.com/zclconf/go-cty/cty/json"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const (
	// maxVariables is the maximum number of variables allowed in a pipeline
	maxVariables = 1000
	// maxNestedPipelineLevel is the maximum number of levels of pipeline nesting allowed
	// (it also protects against infinite recursion)
	maxNestedPipelineLevel = 10

	// maxDeferReasonLength is the maximum length for a defer reason
	maxDeferReasonLength = 255
)

// GetPipelinesInput is the input for querying a list of pipelines
type GetPipelinesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.PipelineSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Started filter will only return started pipelines when true
	Started *bool
	// Completed filter will only return completed pipelines when true
	Completed *bool
	// Superseded filters the pipelines by the specified superseded status.
	Superseded *bool
	// PipelineTemplateID filters the pipelines by the specified pipeline template
	PipelineTemplateID *string
	// ParentPipelineID filters the pipelines by the specified parent pipeline
	ParentPipelineID *string
	// ParentNestedPipelineNodePath filters the pipelines by the specified parent nested pipeline node path
	ParentNestedPipelineNodePath *string
	// EnvironmentName filter will only return pipelines with the specified environment name
	EnvironmentName *string
	// ReleaseID filter will only return pipelines with the specified release ID
	ReleaseID *string
	// ProjectID filters the pipelines by the specified project.
	ProjectID string
	// PipelineType filters the pipelines by the specified pipeline types.
	PipelineTypes []models.PipelineType
}

// SubscribeToPipelineInput is the input for subscribing to a single pipeline
type SubscribeToPipelineInput struct {
	LastSeenVersion *string
	PipelineID      string
}

// SubscribeToProjectPipelinesInput is the input for subscribing to a project's pipelines
type SubscribeToProjectPipelinesInput struct {
	ProjectID string
}

// Event is a pipeline event
type Event struct {
	Pipeline *models.Pipeline
	Action   string
}

// Variable is a pipeline variable
type Variable struct {
	Value    string                  `json:"value"`
	Key      string                  `json:"key"`
	Category models.VariableCategory `json:"category"`
}

// CreatePipelineInput is the input for creating a pipeline
type CreatePipelineInput struct {
	ParentPipelineID       *string
	ParentPipelineNodePath *string
	ReleaseID              *string
	SystemVariables        map[string]cty.Value
	EnvironmentName        *string
	VariableSetRevision    *string
	When                   models.PipelineWhenCondition
	PipelineTemplateID     string
	Type                   models.PipelineType
	Variables              []Variable
	Annotations            []*models.PipelineAnnotation
	ApprovalRuleIDs        []string
}

// CancelPipelineInput is the input for deleting a pipeline
type CancelPipelineInput struct {
	Version *int
	ID      string
	Force   bool
}

// ActionOutput defines an output for a pipeline action
type ActionOutput struct {
	Name  string
	Type  []byte
	Value []byte
}

// CreatePipelineJWTInput is the input for creating a pipeline JWT
type CreatePipelineJWTInput struct {
	Expiration time.Time
	PipelineID string
	Audience   string
}

// UpdateNestedPipelineInput is the input for updating a nested pipeline
type UpdateNestedPipelineInput struct {
	ParentPipelineID       string
	ParentPipelineNodePath string
	PipelineTemplateID     *string
	Variables              []Variable
}

// SchedulePipelineNodeInput is the input for setting the scheduled start time for a pipeline node.
// Either ScheduledStartTime or CronSchedule must be provided.
type SchedulePipelineNodeInput struct {
	ScheduledStartTime *time.Time
	CronSchedule       *models.CronSchedule
	PipelineID         string
	NodeType           statemachine.NodeType
	NodePath           string
}

// Validate input
func (s *SchedulePipelineNodeInput) Validate() error {
	if s.ScheduledStartTime == nil && s.CronSchedule == nil {
		return errors.New("scheduled start time or cron schedule must be provided", errors.WithErrorCode(errors.EInvalid))
	}

	if s.ScheduledStartTime != nil && s.CronSchedule != nil {
		return errors.New("only one of scheduled start time or cron schedule can be provided", errors.WithErrorCode(errors.EInvalid))
	}

	if s.CronSchedule != nil {
		if err := s.CronSchedule.Validate(); err != nil {
			return errors.Wrap(err, "cron schedule is invalid", errors.WithErrorCode(errors.EInvalid))
		}
	}

	return nil
}

// UnschedulePipelineNodeInput is the input for unscheduling a pipeline node.
type UnschedulePipelineNodeInput struct {
	PipelineID string
	NodeType   statemachine.NodeType
	NodePath   string
}

// NodeApprovalInput is the input for approval of a pipeline node.
type NodeApprovalInput struct {
	NodePath   *string
	PipelineID string
	NodeType   statemachine.NodeType
}

// GetPipelineApprovalsInput is the input for retrieving pipeline approvals.
type GetPipelineApprovalsInput struct {
	PipelineID   string
	NodePath     *string
	ApprovalType models.PipelineApprovalType
}

// DeferPipelineNodeInput is the input for deferring a pipeline node.
type DeferPipelineNodeInput struct {
	Reason     string
	PipelineID string
	NodePath   string
	NodeType   statemachine.NodeType
}

// UndeferPipelineNodeInput is the input for undefering a pipeline node.
type UndeferPipelineNodeInput struct {
	PipelineID string
	NodePath   string
	NodeType   statemachine.NodeType
}

// convertedTemplateData is the data after converting a HCL template to models.
type convertedTemplateData struct {
	nestedPipelineTemplateIDs    map[string]string
	nestedPipelineVariables      map[string][]Variable
	nestedPipelineWhenConditions map[string]models.PipelineWhenCondition
	stages                       []*models.PipelineStage
}

// resolvePipelineVariablesInput is the input for resolving pipeline variables.
type resolvePipelineVariablesInput struct {
	project             *models.Project
	variableSetRevision *string
	environmentName     *string
	pipelineType        models.PipelineType
	variables           []Variable
}

// convertedNestedPipelineData is the data after converting a nested pipeline to models.
type convertedNestedPipelineData struct {
	when           models.PipelineWhenCondition
	templateID     string
	nestedPipeline *models.NestedPipeline
	variables      []Variable
}

// convertedHCLStageData is the data after converting a HCL stage to models.
type convertedHCLStageData struct {
	nestedPipelineTemplateIDs    map[string]string
	nestedPipelineVariables      map[string][]Variable
	nestedPipelineWhenConditions map[string]models.PipelineWhenCondition
	stages                       []*models.PipelineStage
}

// Service implements all pipeline  related functionality
type Service interface {
	GetPipelineByID(ctx context.Context, id string) (*models.Pipeline, error)
	GetPipelineByReleaseID(ctx context.Context, releaseID string) (*models.Pipeline, error)
	GetPipelineByPRN(ctx context.Context, prn string) (*models.Pipeline, error)
	GetPipelinesByIDs(ctx context.Context, idList []string) ([]models.Pipeline, error)
	GetPipelines(ctx context.Context, input *GetPipelinesInput) (*db.PipelinesResult, error)
	GetPipelineVariables(ctx context.Context, pipelineID string) ([]Variable, error)
	CreatePipeline(ctx context.Context, input *CreatePipelineInput) (*models.Pipeline, error)
	CancelPipeline(ctx context.Context, input *CancelPipelineInput) (*models.Pipeline, error)
	SubscribeToPipeline(ctx context.Context, options *SubscribeToPipelineInput) (<-chan *Event, error)
	SubscribeToProjectPipelines(ctx context.Context, options *SubscribeToProjectPipelinesInput) (<-chan *Event, error)
	ApprovePipelineNode(ctx context.Context, input *NodeApprovalInput) error
	RevokePipelineNodeApproval(ctx context.Context, input *NodeApprovalInput) error
	SetPipelineActionStatus(ctx context.Context, pipelineID string, actionPath string, status statemachine.NodeStatus) error
	SetPipelineActionOutputs(ctx context.Context, pipelineID string, actionPath string, outputs []*ActionOutput) error
	GetPipelineActionOutputs(ctx context.Context, pipelineID string, actions []string) ([]*models.PipelineActionOutput, error)
	GetPipelineApprovals(ctx context.Context, input *GetPipelineApprovalsInput) ([]*models.PipelineApproval, error)
	CreatePipelineJWT(ctx context.Context, input *CreatePipelineJWTInput) ([]byte, error)
	RetryPipelineTask(ctx context.Context, pipelineID string, taskPath string) (*models.Pipeline, error)
	RunPipelineTask(ctx context.Context, pipelineID string, taskPath string) (*models.Pipeline, error)
	RunPipeline(ctx context.Context, id string) (*models.Pipeline, error)
	RetryNestedPipeline(ctx context.Context, parentPipelineID, parentNestedPipelineNodePath string) (*models.Pipeline, error)
	UpdateNestedPipeline(ctx context.Context, input *UpdateNestedPipelineInput) (*models.Pipeline, error)
	SchedulePipelineNode(ctx context.Context, input *SchedulePipelineNodeInput) (*models.Pipeline, error)
	UnschedulePipelineNode(ctx context.Context, input *UnschedulePipelineNodeInput) (*models.Pipeline, error)
	DeferPipelineNode(ctx context.Context, input *DeferPipelineNodeInput) (*models.Pipeline, error)
	UndeferPipelineNode(ctx context.Context, input *UndeferPipelineNodeInput) (*models.Pipeline, error)
}

type service struct {
	logger                 logger.Logger
	dbClient               *db.Client
	limitChecker           limits.LimitChecker
	pipelineUpdater        Updater
	dataStore              pipelinetemplate.DataStore
	store                  Store
	eventManager           *events.EventManager
	activityService        activityevent.Service
	idp                    *auth.IdentityProvider
	environmentService     environment.Service
	environmentGatekeeper  EnvironmentGatekeeper
	maxNestedPipelineLevel int
}

// NewService returns an instance of Service
// maxNestedPipelineLevel is set here to the constant so that callers don't need to know the value but unit tests can override.
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	dataStore pipelinetemplate.DataStore,
	store Store,
	pipelineUpdater Updater,
	eventManager *events.EventManager,
	activityService activityevent.Service,
	idp *auth.IdentityProvider,
	environmentService environment.Service,
	environmentGateKeeper EnvironmentGatekeeper,
) Service {
	return &service{
		logger:                 logger,
		dbClient:               dbClient,
		limitChecker:           limitChecker,
		pipelineUpdater:        pipelineUpdater,
		dataStore:              dataStore,
		store:                  store,
		eventManager:           eventManager,
		activityService:        activityService,
		idp:                    idp,
		environmentService:     environmentService,
		environmentGatekeeper:  environmentGateKeeper,
		maxNestedPipelineLevel: maxNestedPipelineLevel,
	}
}

func (s *service) ApprovePipelineNode(ctx context.Context, input *NodeApprovalInput) error {
	ctx, span := tracer.Start(ctx, "svc.ApprovePipelineNode")
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	span.SetAttributes(attribute.String("nodeType", input.NodeType.String()))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return err
	}

	var userID *string
	var serviceAccountID *string

	if err := auth.HandleCaller(ctx, func(_ context.Context, c *auth.UserCaller) error {
		userID = &c.User.Metadata.ID
		return nil
	}, func(_ context.Context, c *auth.ServiceAccountCaller) error {
		serviceAccountID = &c.ServiceAccountID
		return nil
	}); err != nil {
		return err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer ApprovePipelineNode: %v", txErr)
		}
	}()

	var (
		activityEventInput = &activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			TargetType: models.TargetPipeline,
			Action:     models.ActionApprove,
		}
	)
	switch input.NodeType {
	case statemachine.TaskNodeType:
		if _, eventError := s.pipelineUpdater.ProcessEvent(txContext, input.PipelineID, &TaskApprovalEvent{
			TaskPath:         ptr.ToString(input.NodePath),
			UserID:           userID,
			ServiceAccountID: serviceAccountID,
		}); eventError != nil {
			return errors.Wrap(eventError, "failed to process task approval event", errors.WithSpan(span))
		}

		activityEventInput.TargetID = &input.PipelineID
		activityEventInput.Payload = &models.ActivityEventApprovePipelineNodePayload{
			NodePath: *input.NodePath,
			NodeType: input.NodeType.String(),
		}
	case statemachine.PipelineNodeType:
		if _, eventError := s.pipelineUpdater.ProcessEvent(txContext, input.PipelineID, &ApprovalEvent{
			UserID:           userID,
			ServiceAccountID: serviceAccountID,
		}); eventError != nil {
			return errors.Wrap(eventError, "failed to process pipeline approval event", errors.WithSpan(span))
		}

		activityEventInput.TargetID = pipeline.ParentPipelineID
		activityEventInput.Payload = &models.ActivityEventApprovePipelineNodePayload{
			NodePath: *pipeline.ParentPipelineNodePath,
			NodeType: input.NodeType.String(),
		}
	default:
		return errors.New("invalid node type %s: only PIPELINE and TASK node types are supported", input.NodeType, errors.WithErrorCode(errors.EInvalid))
	}

	if _, err := s.activityService.CreateActivityEvent(txContext, activityEventInput); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return nil
}

func (s *service) RevokePipelineNodeApproval(ctx context.Context, input *NodeApprovalInput) error {
	ctx, span := tracer.Start(ctx, "svc.RevokePipelineNodeApproval")
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	span.SetAttributes(attribute.String("nodeType", input.NodeType.String()))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return err
	}

	var userID *string
	var serviceAccountID *string

	if err := auth.HandleCaller(ctx, func(_ context.Context, c *auth.UserCaller) error {
		userID = &c.User.Metadata.ID
		return nil
	}, func(_ context.Context, c *auth.ServiceAccountCaller) error {
		serviceAccountID = &c.ServiceAccountID
		return nil
	}); err != nil {
		return err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer RevokePipelineNodeApproval: %v", txErr)
		}
	}()

	// The check that the subject who revokes is the same as the subject who had approved is done in the updater.

	var (
		activityEventInput = &activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			TargetType: models.TargetPipeline,
			Action:     models.ActionRevokeApproval,
		}
	)
	switch input.NodeType {
	case statemachine.TaskNodeType:
		if _, eventError := s.pipelineUpdater.ProcessEvent(txContext, input.PipelineID, &TaskRevokeApprovalEvent{
			TaskPath:         ptr.ToString(input.NodePath),
			UserID:           userID,
			ServiceAccountID: serviceAccountID,
		}); eventError != nil {
			return errors.Wrap(eventError, "failed to process task revoke approval event", errors.WithSpan(span))
		}

		activityEventInput.TargetID = &input.PipelineID
		activityEventInput.Payload = &models.ActivityEventRevokeApprovalPipelineNodePayload{
			NodePath: *input.NodePath,
			NodeType: input.NodeType.String(),
		}
	case statemachine.PipelineNodeType:
		// It's a nested pipeline.
		if _, eventError := s.pipelineUpdater.ProcessEvent(txContext, input.PipelineID, &RevokeApprovalEvent{
			UserID:           userID,
			ServiceAccountID: serviceAccountID,
		}); eventError != nil {
			return errors.Wrap(eventError, "failed to process pipeline revoke approval event", errors.WithSpan(span))
		}

		activityEventInput.TargetID = pipeline.ParentPipelineID
		activityEventInput.Payload = &models.ActivityEventRevokeApprovalPipelineNodePayload{
			NodePath: *pipeline.ParentPipelineNodePath,
			NodeType: input.NodeType.String(),
		}
	default:
		return errors.New("invalid node type %s: only PIPELINE and TASK node types are supported", input.NodeType, errors.WithErrorCode(errors.EInvalid))
	}

	if _, err := s.activityService.CreateActivityEvent(txContext, activityEventInput); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return nil
}

func (s *service) GetPipelineApprovals(ctx context.Context, input *GetPipelineApprovalsInput) ([]*models.PipelineApproval, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineApprovals")
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return nil, err
	}

	// Check whether the caller is allowed to view this pipeline.
	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	result, err := s.dbClient.PipelineApprovals.GetPipelineApprovals(ctx, &db.GetPipelineApprovalsInput{
		Filter: &db.PipelineApprovalFilter{
			PipelineID: &input.PipelineID,
			TaskPath:   input.NodePath,
			Type:       &input.ApprovalType,
		},
	})
	if err != nil {
		return nil, err
	}

	return result.PipelineApprovals, nil
}

func (s *service) GetPipelineByReleaseID(ctx context.Context, releaseID string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineByReleaseID")
	span.SetAttributes(attribute.String("releaseId", releaseID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.dbClient.Pipelines.GetPipelineByReleaseID(ctx, releaseID)
	if err != nil {
		return nil, err
	}

	if pipeline == nil {
		return nil, errors.New("pipeline with release ID %s not found", releaseID, errors.WithErrorCode(errors.ENotFound))
	}

	// Check whether the caller is allowed to view this pipeline.
	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	return pipeline, nil
}

func (s *service) GetPipelineByPRN(ctx context.Context, prn string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.dbClient.Pipelines.GetPipelineByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline by PRN", errors.WithSpan(span))
	}

	if pipeline == nil {
		return nil, errors.New(
			"pipeline with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	// Check whether the caller is allowed to view this pipeline.
	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	return pipeline, nil
}

func (s *service) GetPipelineActionOutputs(ctx context.Context, pipelineID string, actions []string) ([]*models.PipelineActionOutput, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineActionOutputs")
	span.SetAttributes(attribute.String("pipelineId", pipelineID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, pipelineID)
	if err != nil {
		return nil, err
	}

	// Check whether the caller is allowed to view this pipeline.
	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipelineID))
	if err != nil {
		return nil, err
	}

	outputs, err := s.dbClient.PipelineActionOutputs.GetPipelineActionOutputs(ctx, pipelineID, actions)
	if err != nil {
		return nil, err
	}

	return outputs, nil
}

func (s *service) CreatePipelineJWT(ctx context.Context, input *CreatePipelineJWTInput) ([]byte, error) {
	ctx, span := tracer.Start(ctx, "svc.CreatePipelineJWT")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return nil, err
	}

	// Check whether the caller is allowed to update this pipeline.
	err = caller.RequirePermission(
		ctx,
		models.UpdatePipelineState,
		auth.WithProjectID(pipeline.ProjectID),
		auth.WithPipelineID(pipeline.Metadata.ID),
	)
	if err != nil {
		return nil, err
	}

	// Verify expiration is not greater than max job duration
	if input.Expiration.After(time.Now().Add(models.MaxAllowedJobDuration)) {
		return nil, errors.New("token lifespan cannot be greater than %s minutes", models.MaxAllowedJobDuration.Minutes(), errors.WithErrorCode(errors.EInvalid))
	}

	project, err := s.dbClient.Projects.GetProjectByID(ctx, pipeline.ProjectID)
	if err != nil {
		return nil, err
	}

	if project == nil {
		return nil, errors.New("project with ID %s not found", pipeline.ProjectID, errors.WithErrorCode(errors.ENotFound))
	}

	claims := map[string]interface{}{
		"project_name":  project.Name,
		"org_name":      project.GetOrgName(),
		"pipeline_id":   gid.ToGlobalID(gid.PipelineType, pipeline.Metadata.ID),
		"pipeline_type": string(pipeline.Type),
		"is_release":    strconv.FormatBool(pipeline.ReleaseID != nil),
	}

	if pipeline.ReleaseID != nil {
		// Get release
		release, rErr := s.dbClient.Releases.GetReleaseByID(ctx, *pipeline.ReleaseID)
		if rErr != nil {
			return nil, errors.Wrap(rErr, "failed to get release", errors.WithSpan(span))
		}
		if release == nil {
			return nil, errors.New("release with ID %s not found", *pipeline.ReleaseID, errors.WithErrorCode(errors.ENotFound))
		}

		claims["release_prn"] = release.Metadata.PRN
		claims["release_lifecycle_prn"] = release.LifecyclePRN
	}

	if pipeline.EnvironmentName != nil {
		claims["environment"] = *pipeline.EnvironmentName
	}

	token, err := s.idp.GenerateToken(ctx, &auth.TokenInput{
		Expiration: &input.Expiration,
		Subject:    project.Metadata.PRN,
		Audience:   input.Audience,
		Claims:     claims,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate token", errors.WithSpan(span))
	}

	return token, nil
}

func (s *service) SetPipelineActionOutputs(ctx context.Context, pipelineID string, actionPath string, outputs []*ActionOutput) error {
	ctx, span := tracer.Start(ctx, "svc.SetPipelineActionOutputs")
	span.SetAttributes(attribute.String("pipelineId", pipelineID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pipeline, err := s.getPipelineByID(ctx, span, pipelineID)
	if err != nil {
		return err
	}

	// Check whether the caller is allowed to update this pipeline.
	err = caller.RequirePermission(
		ctx,
		models.UpdatePipelineState,
		auth.WithProjectID(pipeline.ProjectID),
		auth.WithPipelineID(pipelineID),
		auth.WithPipelineAction(actionPath),
	)
	if err != nil {
		return err
	}

	// Verify pipeline action exists
	if _, ok := pipeline.GetAction(actionPath); !ok {
		return errors.New("action %s not found in pipeline", actionPath, errors.WithErrorCode(errors.EInvalid))
	}

	outputsToCreate := []*models.PipelineActionOutput{}
	for _, o := range outputs {
		oCopy := o
		output := models.PipelineActionOutput{
			PipelineID: pipelineID,
			ActionPath: actionPath,
			Name:       oCopy.Name,
			Type:       oCopy.Type,
			Value:      oCopy.Value,
		}
		outputsToCreate = append(outputsToCreate, &output)
	}

	existingOutputs, err := s.dbClient.PipelineActionOutputs.GetPipelineActionOutputs(ctx, pipelineID, []string{actionPath})
	if err != nil {
		return err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer SetPipelineActionOutputs: %v", txErr)
		}
	}()

	// Delete existing outputs
	for _, o := range existingOutputs {
		if err := s.dbClient.PipelineActionOutputs.DeletePipelineActionOutput(txContext, o); err != nil {
			return err
		}
	}

	for _, output := range outputsToCreate {
		if _, err := s.dbClient.PipelineActionOutputs.CreatePipelineActionOutput(txContext, output); err != nil {
			return err
		}
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return nil
}

func (s *service) SetPipelineActionStatus(ctx context.Context, pipelineID string, actionPath string, status statemachine.NodeStatus) error {
	ctx, span := tracer.Start(ctx, "svc.UpdatePipelineAction")
	span.SetAttributes(attribute.String("pipelineId", pipelineID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pipeline, err := s.getPipelineByID(ctx, span, pipelineID)
	if err != nil {
		return err
	}

	// Check whether the caller is allowed to update this pipeline.
	err = caller.RequirePermission(
		ctx,
		models.UpdatePipelineState,
		auth.WithProjectID(pipeline.ProjectID),
		auth.WithPipelineID(pipelineID),
		auth.WithPipelineAction(actionPath),
	)
	if err != nil {
		return err
	}

	// Verify pipeline action exists
	if _, ok := pipeline.GetAction(actionPath); !ok {
		return errors.New("action %s not found in pipeline", actionPath, errors.WithErrorCode(errors.EInvalid))
	}

	_, err = s.pipelineUpdater.ProcessEvent(ctx, pipelineID, &ActionStatusChangeEvent{
		ActionPath: actionPath,
		Status:     status,
	})

	return err
}

func (s *service) SubscribeToPipeline(ctx context.Context, options *SubscribeToPipelineInput) (<-chan *Event, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToPipeline")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, options.PipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(options.PipelineID))
	if err != nil {
		return nil, err
	}

	subscription := events.Subscription{
		Type: events.PipelineSubscription,
		ID:   options.PipelineID,
		Actions: []events.SubscriptionAction{
			events.CreateAction,
			events.UpdateAction,
		},
	}

	subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})

	outgoing := make(chan *Event)
	go func() {
		// Defer close of outgoing channel
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		if options.LastSeenVersion != nil && strconv.Itoa(pipeline.Metadata.Version) != *options.LastSeenVersion {
			select {
			case <-ctx.Done():
				return
			case outgoing <- &Event{Pipeline: pipeline, Action: string(events.UpdateAction)}:
			}
		}

		// Wait for pipeline updates
		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) && !goerrors.Is(err, context.DeadlineExceeded) {
					s.logger.Errorf("Error occurred while waiting for pipeline events: %v", err)
				}
				return
			}

			pipeline, err := s.dbClient.Pipelines.GetPipelineByID(ctx, event.ID)
			if err != nil {
				if !errors.IsContextCanceledError(err) && !goerrors.Is(err, context.DeadlineExceeded) {
					s.logger.Errorf("Error querying for pipeline in pipeline subscription goroutine: %v", err)
				}
				continue
			}
			if pipeline == nil {
				s.logger.Errorf("Received event for pipeline that does not exist %s", event.ID)
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- &Event{Pipeline: pipeline, Action: event.Action}:
			}
		}
	}()

	return outgoing, nil
}

func (s *service) SubscribeToProjectPipelines(ctx context.Context, options *SubscribeToProjectPipelinesInput) (<-chan *Event, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToProjectPipelines")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	project, err := s.dbClient.Projects.GetProjectByID(ctx, options.ProjectID)
	if err != nil {
		return nil, err
	}
	if project == nil {
		return nil, errors.New("project with ID %s not found", options.ProjectID, errors.WithErrorCode(errors.ENotFound))
	}

	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(project.Metadata.ID))
	if err != nil {
		return nil, err
	}

	subscription := events.Subscription{
		Type: events.PipelineSubscription,
		Actions: []events.SubscriptionAction{
			events.CreateAction,
			events.UpdateAction,
		},
	}

	subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})

	outgoing := make(chan *Event)
	go func() {
		// Defer close of outgoing channel
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		// Wait for job updates
		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) && !goerrors.Is(err, context.DeadlineExceeded) {
					s.logger.Errorf("Error occurred while waiting for pipeline events: %v", err)
				}
				return
			}

			pipelineEventData, err := event.ToPipelineEventData()
			if err != nil {
				s.logger.Errorf("failed to get pipeline event data in pipeline subscription: %v", err)
				continue
			}

			if options.ProjectID != pipelineEventData.ProjectID {
				continue
			}

			pipeline, err := s.dbClient.Pipelines.GetPipelineByID(ctx, event.ID)
			if err != nil {
				if !errors.IsContextCanceledError(err) && !goerrors.Is(err, context.DeadlineExceeded) {
					s.logger.Errorf("Error querying for pipeline in pipeline subscription goroutine: %v", err)
				}
				continue
			}
			if pipeline == nil {
				s.logger.Errorf("Received event for pipeline that does not exist %s", event.ID)
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- &Event{Pipeline: pipeline, Action: event.Action}:
			}
		}
	}()

	return outgoing, nil
}

func (s *service) GetPipelineByID(ctx context.Context, id string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this pipeline.
	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(id))
	if err != nil {
		return nil, err
	}

	return pipeline, nil
}

func (s *service) GetPipelinesByIDs(ctx context.Context, idList []string) ([]models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelinesByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetPipelinesInput{
		Filter: &db.PipelineFilter{
			PipelineIDs: idList,
		},
	}

	resp, err := s.dbClient.Pipelines.GetPipelines(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipelines", errors.WithSpan(span))
	}

	// Make sure caller has permission to view pipelines in all the parent projects.
	for _, p := range resp.Pipelines {
		err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(p.ProjectID), auth.WithPipelineID(p.Metadata.ID))
		if err != nil {
			return nil, err
		}
	}

	return resp.Pipelines, nil
}

func (s *service) GetPipelines(ctx context.Context, input *GetPipelinesInput) (*db.PipelinesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelines")
	span.SetAttributes(attribute.String("project", input.ProjectID))
	if input.PipelineTemplateID != nil {
		span.SetAttributes(attribute.String("pipeline-template", *input.PipelineTemplateID))
	}
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(input.ProjectID))
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetPipelinesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.PipelineFilter{
			ProjectID:                    &input.ProjectID,
			PipelineTemplateID:           input.PipelineTemplateID,
			ParentPipelineID:             input.ParentPipelineID,
			ParentNestedPipelineNodePath: input.ParentNestedPipelineNodePath,
			Superseded:                   input.Superseded,
			PipelineTypes:                input.PipelineTypes,
			Started:                      input.Started,
			Completed:                    input.Completed,
			EnvironmentName:              input.EnvironmentName,
			ReleaseID:                    input.ReleaseID,
		},
	}

	resp, err := s.dbClient.Pipelines.GetPipelines(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipelines", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) GetPipelineVariables(ctx context.Context, pipelineID string) ([]Variable, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineVariables")
	span.SetAttributes(attribute.String("pipelineId", pipelineID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, pipelineID)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(
		ctx,
		models.ViewVariable,
		auth.WithProjectID(pipeline.ProjectID),
		auth.WithPipelineID(pipelineID),
	); err != nil {
		return nil, err
	}

	reader, err := s.store.GetPipelineVariables(ctx, pipeline.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline variables", errors.WithSpan(span))
	}

	defer reader.Close()

	var variables []Variable
	if err := json.NewDecoder(reader).Decode(&variables); err != nil {
		return nil, errors.Wrap(err, "failed to decode pipeline variables", errors.WithSpan(span))
	}

	return variables, nil
}

func (s *service) CreatePipeline(ctx context.Context, input *CreatePipelineInput) (*models.Pipeline, error) {
	// Don't allow non-root pipelines through this function.
	if input.ParentPipelineID != nil {
		return nil, errors.New("initial pipeline must be a root-level pipeline, not nested",
			errors.WithErrorCode(errors.EInvalid))
	}

	// Start at zero nested levels.
	return s.createPipeline(ctx, input, 0)
}

func (s *service) createPipeline(ctx context.Context, input *CreatePipelineInput, nestedLevel int) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.CreatePipeline")
	span.SetAttributes(attribute.String("pipeline-template", input.PipelineTemplateID))
	defer span.End()

	// Detect whether this exceeds nesting level limit.
	// NOTE: This check also protects against infinite recursion via a repeated pipeline template.
	if nestedLevel > s.maxNestedPipelineLevel {
		return nil, errors.New("exceeded allowed %d levels of nested pipelines", s.maxNestedPipelineLevel,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// Must get the pipeline template in order to check permissions, etc.
	pipelineTemplate, err := s.dbClient.PipelineTemplates.GetPipelineTemplateByID(ctx, input.PipelineTemplateID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline template", errors.WithSpan(span))
	}
	if pipelineTemplate == nil {
		return nil, errors.New("failed to find pipeline template ID %s", input.PipelineTemplateID, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipelineTemplate.ProjectID))
	if err != nil {
		return nil, err
	}

	// Mark caller as authorized
	caller.Authorized()

	// Verify that parent is not a deployment pipeline if this pipeline is a deployment pipeline
	if input.Type == models.DeploymentPipelineType && input.ParentPipelineID != nil {
		parentPipeline, pErr := s.dbClient.Pipelines.GetPipelineByID(ctx, *input.ParentPipelineID)
		if pErr != nil {
			return nil, errors.Wrap(pErr, "failed to get parent pipeline", errors.WithSpan(span))
		}
		if parentPipeline == nil {
			return nil, errors.New("parent pipeline with ID %s not found", *input.ParentPipelineID, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
		}
		if parentPipeline.Type == models.DeploymentPipelineType {
			return nil, errors.New("parent pipeline cannot be a deployment pipeline", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	// Verify that template has been uploaded successfully
	if pipelineTemplate.Status != models.PipelineTemplateUploaded {
		return nil, errors.New("pipeline template must be in the uploaded state", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if len(input.Variables) > maxVariables {
		return nil, errors.New("pipeline variables exceed limit of %d", maxVariables, errors.WithErrorCode(errors.EInvalid))
	}

	project, err := s.dbClient.Projects.GetProjectByID(ctx, pipelineTemplate.ProjectID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project", errors.WithSpan(span))
	}

	if project == nil {
		return nil, errors.New("project with ID %s not found", pipelineTemplate.ProjectID, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	// If there are applicable environment protection rules, check that they are satisfied.
	// However, the check is only performed for parent pipelines since they are started automatically.
	if (input.EnvironmentName != nil) && (input.ParentPipelineID == nil) {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipelineTemplate.ProjectID, *input.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	rdr, err := s.dataStore.GetData(ctx, pipelineTemplate.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "error downloading pipeline template data", errors.WithSpan(span))
	}
	defer rdr.Close()

	// Resolve pipeline variables will combine the provided input variables and project variables
	// if a project variable set revision is specified
	pipelineVariables, err := s.resolvePipelineVariables(ctx, &resolvePipelineVariablesInput{
		project:             project,
		variables:           input.Variables,
		variableSetRevision: input.VariableSetRevision,
		pipelineType:        input.Type,
		environmentName:     input.EnvironmentName,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to resolve pipeline variables", errors.WithSpan(span))
	}

	// Get variable cty values for HCL eval context
	rawValueMap := map[string]string{}
	for _, variable := range pipelineVariables {
		if variable.Category == models.HCLVariable {
			rawValueMap[variable.Key] = variable.Value
		}
	}

	// Load HCL Config
	hclConfig, _, err := config.NewPipelineTemplate(rdr, rawValueMap)
	if err != nil {
		return nil, errors.Wrap(err, "invalid pipeline template", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Build maps of hcl input variables and hcl config variables
	hclInputVariableKeys := map[string]bool{}
	for _, variable := range input.Variables {
		if variable.Category == models.HCLVariable {
			hclInputVariableKeys[variable.Key] = true
		}
	}
	hclConfigVariableKeys := map[string]bool{}
	for _, variable := range hclConfig.Variables {
		hclConfigVariableKeys[variable.Name] = true
	}

	// Remove project variables that are not defined in the pipeline template
	finalPipelineVariables := []Variable{}
	for _, variable := range pipelineVariables {
		if variable.Category == models.HCLVariable && !hclInputVariableKeys[variable.Key] {
			// This is a project variable so we need to check if it's defined in the pipeline template
			if _, ok := hclConfigVariableKeys[variable.Key]; !ok {
				// Skip this variable since it's not defined in the pipelien template
				continue
			}
		}
		finalPipelineVariables = append(finalPipelineVariables, variable)
	}

	// encode pipeline variables.
	encodedVars, err := json.Marshal(finalPipelineVariables)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal pipeline variables", errors.WithSpan(span))
	}

	contextVariables, err := variable.GetCtyValuesForVariables(hclConfig.GetVariableBodies(), rawValueMap, false)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode variables", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// Create HCL context
	evalCtx := hclctx.New(map[string]cty.Value{
		"var":    cty.ObjectVal(contextVariables),
		"system": cty.ObjectVal(input.SystemVariables),
	})

	// Convert HCL config to models.
	convertedData, err := s.convertHCLTemplateToModels(ctx, hclConfig, evalCtx, project.Metadata.ID, pipelineTemplate.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to convert HCL template to models", errors.WithSpan(span))
	}

	if len(input.ApprovalRuleIDs) > 0 {
		// Ensure approval rule IDs are usable and within the org. We don't
		// need any records, however, since this is just for verification.
		approvalRulesResult, aErr := s.dbClient.ApprovalRules.GetApprovalRules(ctx, &db.GetApprovalRulesInput{
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
			Filter: &db.ApprovalRuleFilter{
				ApprovalRuleIDs: input.ApprovalRuleIDs,
				OrgID:           &project.OrgID,
			},
		})
		if aErr != nil {
			return nil, errors.Wrap(aErr, "failed to get approval rules", errors.WithSpan(span))
		}

		if int(approvalRulesResult.PageInfo.TotalCount) != len(input.ApprovalRuleIDs) {
			return nil, errors.New("invalid approval rules specified",
				errors.WithErrorCode(errors.EInvalid),
				errors.WithSpan(span),
			)
		}
	}

	// Create the model.
	toCreate := &models.Pipeline{
		ParentPipelineID:       input.ParentPipelineID,
		ParentPipelineNodePath: input.ParentPipelineNodePath,
		ProjectID:              pipelineTemplate.ProjectID,
		PipelineTemplateID:     input.PipelineTemplateID,
		CreatedBy:              caller.GetSubject(),
		Status:                 statemachine.CreatedNodeStatus,
		Type:                   input.Type,
		ReleaseID:              input.ReleaseID,
		When:                   input.When,
		Stages:                 convertedData.stages,
		EnvironmentName:        input.EnvironmentName,
		Annotations:            input.Annotations,
		ApprovalStatus:         models.ApprovalNotRequired,
		ApprovalRuleIDs:        []string{},
	}

	if len(input.ApprovalRuleIDs) > 0 {
		// Using approval rules, so approval status is set to pending.
		toCreate.ApprovalStatus = models.ApprovalPending
		toCreate.ApprovalRuleIDs = input.ApprovalRuleIDs
	}

	// Validate pipeline models
	if err = toCreate.Validate(); err != nil {
		return nil, err
	}

	// Must do a transaction in order to roll back if the limit was exceeded.
	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreatePipeline: %v", txErr)
		}
	}()

	// Create the environment if it doesn't exist.
	if input.EnvironmentName != nil {
		// Get the environment.
		environmentsResp, gErr := s.dbClient.Environments.GetEnvironments(txContext, &db.GetEnvironmentsInput{
			Filter: &db.EnvironmentFilter{
				ProjectID:       &project.Metadata.ID,
				EnvironmentName: input.EnvironmentName,
			}})
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get environment", errors.WithSpan(span))
		}

		if environmentsResp.PageInfo.TotalCount == 0 {
			// Create environment since it doesn't exist
			_, err = s.environmentService.CreateEnvironment(txContext, &environment.CreateEnvironmentInput{
				ProjectID: project.Metadata.ID,
				Name:      *input.EnvironmentName,
			})
			if err != nil && errors.ErrorCode(err) != errors.EConflict {
				return nil, errors.Wrap(err, "failed to create environment", errors.WithSpan(span))
			}
		}
	}

	pipeline, err := s.dbClient.Pipelines.CreatePipeline(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create pipeline", errors.WithSpan(span))
	}

	hasNestedPipeline := false

	// Create nested pipelines.
	for _, stage := range pipeline.Stages {
		for _, nestedPipeline := range stage.NestedPipelines {
			hasNestedPipeline = true

			pipelineTemplateID, ok := convertedData.nestedPipelineTemplateIDs[nestedPipeline.Path]
			if !ok {
				return nil, errors.Wrap(err, "failed to find nested pipeline template ID for nested pipeline %s", nestedPipeline.Path, errors.WithSpan(span))
			}

			if nestedPipeline.EnvironmentName != nil && pipeline.Type == models.DeploymentPipelineType {
				return nil, errors.New("deployment pipelines cannot created nested pipelines with an environment", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
			}

			nestedPipelineType := models.NestedPipelineType
			if nestedPipeline.EnvironmentName != nil {
				nestedPipelineType = models.DeploymentPipelineType
			}

			environmentName := pipeline.EnvironmentName
			if nestedPipeline.EnvironmentName != nil {
				environmentName = nestedPipeline.EnvironmentName
			}

			toCreate := &CreatePipelineInput{
				ParentPipelineID:       &pipeline.Metadata.ID,
				ParentPipelineNodePath: ptr.String(nestedPipeline.Path),
				Type:                   nestedPipelineType,
				ReleaseID:              input.ReleaseID,
				PipelineTemplateID:     pipelineTemplateID,
				Variables:              convertedData.nestedPipelineVariables[nestedPipeline.Path],
				When:                   convertedData.nestedPipelineWhenConditions[nestedPipeline.Path],
				EnvironmentName:        environmentName,
				Annotations:            []*models.PipelineAnnotation{}, // TODO: pass annotations from pipeline template
				VariableSetRevision:    input.VariableSetRevision,
			}

			created, cErr := s.createPipeline(txContext, toCreate, nestedLevel+1)
			if cErr != nil {
				return nil, errors.Wrap(cErr, "failed to create nested pipeline %s", nestedPipeline.Path)
			}

			nestedPipeline.LatestPipelineID = created.Metadata.ID
		}
	}

	if hasNestedPipeline {
		// Update pipeline after nested pipeline IDs have been set.
		pipeline, err = s.dbClient.Pipelines.UpdatePipeline(txContext, pipeline)
		if err != nil {
			return nil, err
		}
	}

	// Get the number of pipelines in this project to check whether we just violated the limit.
	pipelinesResult, err := s.dbClient.Pipelines.GetPipelines(txContext, &db.GetPipelinesInput{
		Filter: &db.PipelineFilter{
			TimeRangeStart: ptr.Time(pipeline.Metadata.CreationTimestamp.Add(-limits.ResourceLimitTimePeriod)),
			ProjectID:      &pipeline.ProjectID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipelines", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitPipelinesPerProjectPerTimePeriod,
		pipelinesResult.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "limit check failed", errors.WithSpan(span))
	}

	if err = s.store.UploadPipelineVariables(ctx, pipeline.Metadata.ID, bytes.NewReader(encodedVars)); err != nil {
		return nil, errors.Wrap(err, "failed to upload pipeline variables", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetPipeline,
			TargetID:   &pipeline.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	if pipeline.ParentPipelineID == nil {
		// If this is a top-level pipeline, start it.
		return s.pipelineUpdater.ProcessEvent(ctx, pipeline.Metadata.ID, &RunPipelineEvent{})
	}

	s.logger.Infow("Created a pipeline.",
		"caller", caller.GetSubject(),
		"pipelineID", pipeline.Metadata.ID,
	)

	return pipeline, nil
}

func (s *service) CancelPipeline(ctx context.Context, input *CancelPipelineInput) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.CancelPipeline")
	span.SetAttributes(attribute.String("id", input.ID))
	span.SetAttributes(attribute.Bool("force", input.Force))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CancelPipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(input.ID))
	if err != nil {
		return nil, err
	}

	// If there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	// Wrap all the DB updates in a transaction, whether the cancel is forced or graceful.
	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for CancelPipeline: %v", txErr)
		}
	}()

	pipeline, err = s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &CancelPipelineEvent{
		Version: input.Version,
		Force:   input.Force,
		Caller:  caller,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to process pipeline cancellation event", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, &activityevent.CreateActivityEventInput{
		ProjectID:  &pipeline.ProjectID,
		Action:     models.ActionCancel,
		TargetType: models.TargetPipeline,
		TargetID:   &pipeline.Metadata.ID,
	}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Requested cancellation of a pipeline.",
		"caller", caller.GetSubject(),
		"pipelineID", pipeline.Metadata.ID,
		"force", input.Force,
	)

	return pipeline, nil
}

func (s *service) RetryPipelineTask(ctx context.Context, pipelineID, taskPath string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.RetryPipelineTask")
	span.SetAttributes(attribute.String("pipelineId", pipelineID))
	span.SetAttributes(attribute.String("taskPath", taskPath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, pipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// If there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer RetryPipelineTask: %v", txErr)
		}
	}()

	eventPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &RetryTaskEvent{TaskPath: taskPath})
	if eventError != nil {
		return nil, errors.Wrap(eventError, "failed to process retry task event", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			Action:     models.ActionRetry,
			TargetType: models.TargetPipeline,
			TargetID:   &pipeline.Metadata.ID,
			Payload: &models.ActivityEventRetryPipelineNodePayload{
				NodePath: taskPath,
				NodeType: statemachine.TaskNodeType.String(),
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return eventPipeline, nil
}

func (s *service) SchedulePipelineNode(ctx context.Context, input *SchedulePipelineNodeInput) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.SchedulePipelineNode")
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	span.SetAttributes(attribute.String("nodePath", input.NodePath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = input.Validate(); err != nil {
		return nil, errors.Wrap(err, "invalid input", errors.WithSpan(span))
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// For the parent pipeline, if there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer SchedulePipelineNode: %v", txErr)
		}
	}()

	var (
		eventPipeline      *models.Pipeline
		activityEventInput = &activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			Action:     models.ActionSchedule,
			TargetType: models.TargetPipeline,
		}
	)

	switch input.NodeType {
	case statemachine.PipelineNodeType:
		nestedPipelineNode, ok := pipeline.GetNestedPipeline(input.NodePath)
		if !ok {
			return nil, errors.New("nested pipeline %s not found in pipeline",
				input.NodePath, errors.WithErrorCode(errors.ENotFound))
		}

		// For the nested pipeline, if there are applicable environment protection rules, check that they are satisfied.
		// Project name should be the same for parent vs. nested pipelines.
		if nestedPipelineNode.EnvironmentName != nil {
			if eErr := s.environmentGatekeeper.checkEnvironmentRules(txContext,
				pipeline.ProjectID, *nestedPipelineNode.EnvironmentName); eErr != nil {
				// If the error is due to a rule violation, the error already includes an error code.
				return nil, eErr
			}
		}

		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &ScheduleNestedPipelineEvent{
			NestedPipelinePath: input.NodePath,
			ScheduledStartTime: input.ScheduledStartTime,
			CronSchedule:       input.CronSchedule,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process schedule pipeline node event", errors.WithSpan(span))
		}

		nestedPipelineNode, ok = updatedPipeline.GetNestedPipeline(input.NodePath)
		if !ok {
			return nil, errors.New("nested pipeline %s not found in pipeline", input.NodePath)
		}

		// Associate the activity event with the parent pipeline.
		activityEventInput.TargetID = &pipeline.Metadata.ID
		activityEventInput.Payload = &models.ActivityEventSchedulePipelineNodePayload{
			NodePath:  input.NodePath,
			NodeType:  statemachine.PipelineNodeType.String(),
			StartTime: nestedPipelineNode.ScheduledStartTime,
		}
		eventPipeline = updatedPipeline
	case statemachine.TaskNodeType:
		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &ScheduleTaskEvent{
			TaskPath:           input.NodePath,
			ScheduledStartTime: input.ScheduledStartTime,
			CronSchedule:       input.CronSchedule,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process schedule pipeline node event", errors.WithSpan(span))
		}

		taskNode, ok := updatedPipeline.GetTask(input.NodePath)
		if !ok {
			return nil, errors.New("task %s not found in pipeline", input.NodePath)
		}

		// Associate the activity event with the task and parent pipeline.
		activityEventInput.TargetID = &pipeline.Metadata.ID
		activityEventInput.Payload = &models.ActivityEventSchedulePipelineNodePayload{
			NodePath:  input.NodePath,
			NodeType:  statemachine.TaskNodeType.String(),
			StartTime: taskNode.ScheduledStartTime,
		}
		eventPipeline = updatedPipeline
	default:
		return nil, errors.New("invalid node type %s: only PIPELINE and TASK node types are supported", input.NodeType, errors.WithErrorCode(errors.EInvalid))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, activityEventInput); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return eventPipeline, nil
}

func (s *service) UnschedulePipelineNode(ctx context.Context, input *UnschedulePipelineNodeInput) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.UnschedulePipelineNode")
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	span.SetAttributes(attribute.String("nodePath", input.NodePath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// For the parent pipeline, if there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UnschedulePipelineNode: %v", txErr)
		}
	}()

	var (
		eventPipeline      *models.Pipeline
		activityEventInput = &activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			Action:     models.ActionUnschedule,
			TargetType: models.TargetPipeline,
		}
	)

	switch input.NodeType {
	case statemachine.PipelineNodeType:
		nestedPipelineNode, ok := pipeline.GetNestedPipeline(input.NodePath)
		if !ok {
			return nil, errors.New("nested pipeline %s not found in pipeline",
				input.NodePath, errors.WithErrorCode(errors.ENotFound))
		}

		// For the nested pipeline, if there are applicable environment protection rules, check that they are satisfied.
		// Project name should be the same for parent vs. nested pipelines.
		if nestedPipelineNode.EnvironmentName != nil {
			if eErr := s.environmentGatekeeper.checkEnvironmentRules(txContext,
				pipeline.ProjectID, *nestedPipelineNode.EnvironmentName); eErr != nil {
				// If the error is due to a rule violation, the error already includes an error code.
				return nil, eErr
			}
		}

		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &UnscheduleNestedPipelineEvent{
			NestedPipelinePath: input.NodePath,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process unschedule pipeline node event", errors.WithSpan(span))
		}

		// Associate the activity event with the nested pipeline.
		activityEventInput.TargetID = &pipeline.Metadata.ID
		activityEventInput.Payload = &models.ActivityEventUnschedulePipelineNodePayload{
			NodePath: input.NodePath,
			NodeType: statemachine.PipelineNodeType.String(),
		}
		eventPipeline = updatedPipeline
	case statemachine.TaskNodeType:
		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &UnscheduleTaskEvent{
			TaskPath: input.NodePath,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process unschedule pipeline node event", errors.WithSpan(span))
		}

		// Associate the activity event with the task and parent pipeline.
		activityEventInput.TargetID = &pipeline.Metadata.ID
		activityEventInput.Payload = &models.ActivityEventUnschedulePipelineNodePayload{
			NodePath: input.NodePath,
			NodeType: statemachine.TaskNodeType.String(),
		}
		eventPipeline = updatedPipeline
	default:
		return nil, errors.New("invalid node type %s: only PIPELINE and TASK node types are supported", input.NodeType, errors.WithErrorCode(errors.EInvalid))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, activityEventInput); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return eventPipeline, nil
}

func (s *service) RunPipelineTask(ctx context.Context, pipelineID, taskPath string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.RunPipelineTask")
	span.SetAttributes(attribute.String("pipelineId", pipelineID))
	span.SetAttributes(attribute.String("taskPath", taskPath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, pipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// Verify pipeline task exists.
	task, ok := pipeline.GetTask(taskPath)
	if !ok {
		return nil, errors.New("task %s not found in pipeline", taskPath, errors.WithErrorCode(errors.ENotFound))
	}

	// If there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	if task.When != models.ManualPipelineTask {
		return nil, errors.New("task %s cannot be started manually", task.Name, errors.WithErrorCode(errors.EInvalid))
	}

	if !task.Status.Equals(statemachine.ReadyNodeStatus) {
		return nil, errors.New("task %s is not in a runnable state", task.Name, errors.WithErrorCode(errors.EInvalid))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer RunPipelineTask: %v", txErr)
		}
	}()

	eventPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &RunTaskEvent{TaskPath: taskPath})
	if eventError != nil {
		return nil, errors.Wrap(eventError, "failed to process run task event", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			Action:     models.ActionStart,
			TargetType: models.TargetPipeline,
			TargetID:   &pipeline.Metadata.ID,
			Payload: &models.ActivityEventStartPipelineNodePayload{
				NodePath: taskPath,
				NodeType: statemachine.TaskNodeType.String(),
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return eventPipeline, nil
}

func (s *service) RunPipeline(ctx context.Context, id string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.RunPipeline")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// If there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	if pipeline.When != models.ManualPipeline {
		return nil, errors.New("pipeline cannot be run manually", errors.WithErrorCode(errors.EInvalid))
	}

	if !pipeline.Status.Equals(statemachine.ReadyNodeStatus) {
		return nil, errors.New("pipeline is not in a runnable state", errors.WithErrorCode(errors.EInvalid))
	}

	if pipeline.ParentPipelineID != nil {
		txContext, err := s.dbClient.Transactions.BeginTx(ctx)
		if err != nil {
			return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
		}

		defer func() {
			if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
				s.logger.Errorf("failed to rollback tx for RunPipeline: %v", txErr)
			}
		}()

		updatedPipeline, err := s.pipelineUpdater.ProcessEvent(txContext, *pipeline.ParentPipelineID, &RunNestedPipelineEvent{
			NestedPipelinePath: *pipeline.ParentPipelineNodePath,
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to process run nested pipeline event", errors.WithSpan(span))
		}

		// Create an activity event tied to the pipelines
		if _, err = s.activityService.CreateActivityEvent(txContext,
			&activityevent.CreateActivityEventInput{
				ProjectID:  &pipeline.ProjectID,
				Action:     models.ActionStart,
				TargetType: models.TargetPipeline,
				TargetID:   pipeline.ParentPipelineID,
				Payload: &models.ActivityEventStartPipelineNodePayload{
					NodePath: *pipeline.ParentPipelineNodePath,
					NodeType: statemachine.PipelineNodeType.String(),
				},
			},
		); err != nil {
			return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
		}

		if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
			return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
		}

		return updatedPipeline, nil
	}

	return s.pipelineUpdater.ProcessEvent(ctx, pipeline.Metadata.ID, &RunPipelineEvent{})
}

func (s *service) UpdateNestedPipeline(ctx context.Context, input *UpdateNestedPipelineInput) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateNestedPipeline")
	span.SetAttributes(attribute.String("parentPipelineId", input.ParentPipelineID))
	span.SetAttributes(attribute.String("parentNestedPipelineNodePath", input.ParentPipelineNodePath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	parentPipeline, err := s.getPipelineByID(ctx, span, input.ParentPipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(parentPipeline.ProjectID), auth.WithPipelineID(parentPipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// Mark caller as authorized
	caller.Authorized()

	if parentPipeline.Type != models.ReleaseLifecyclePipelineType {
		return nil, errors.New("only nested pipelines in a release lifecycle pipeline can be updated", errors.WithErrorCode(errors.EInvalid))
	}

	nestedPipelineNode, ok := parentPipeline.GetNestedPipeline(input.ParentPipelineNodePath)
	if !ok {
		return nil, errors.New("nested pipeline %s not found in pipeline", input.ParentPipelineNodePath, errors.WithErrorCode(errors.EInvalid))
	}

	nestedPipeline, err := s.getPipelineByID(ctx, span, nestedPipelineNode.LatestPipelineID)
	if err != nil {
		return nil, err
	}

	variables := input.Variables
	if variables == nil {
		// Get the variables from the pipeline, so we can recreate the pipeline with the same variables.
		variables, err = s.getPipelineVariables(ctx, nestedPipelineNode.LatestPipelineID)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get pipeline variables", errors.WithSpan(span))
		}
	}

	templateID := input.PipelineTemplateID
	if templateID == nil {
		templateID = &nestedPipeline.PipelineTemplateID
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for UpdateNestedPipeline: %v", txErr)
		}
	}()

	// Create the new pipeline, so it can be updated in the parent pipeline.
	// 1 is passed as the nestedLevel argument, because this will always be the first level of nesting.
	newPipeline, err := s.createPipeline(txContext, &CreatePipelineInput{
		EnvironmentName:        nestedPipeline.EnvironmentName,
		ParentPipelineID:       &input.ParentPipelineID,
		ParentPipelineNodePath: &input.ParentPipelineNodePath,
		Type:                   models.DeploymentPipelineType,
		ReleaseID:              parentPipeline.ReleaseID,
		PipelineTemplateID:     *templateID,
		When:                   nestedPipeline.When,
		Variables:              variables,
		Annotations:            nestedPipeline.Annotations,
		ApprovalRuleIDs:        nestedPipeline.ApprovalRuleIDs,
	}, 1)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create pipeline", errors.WithSpan(span))
	}

	pipelineToReturn, err := s.pipelineUpdater.ProcessEvent(txContext, parentPipeline.Metadata.ID, &UpdateNestedPipelineEvent{
		NestedPipelinePath:   nestedPipelineNode.Path,
		NestedPipelineID:     newPipeline.Metadata.ID,
		SupersededPipelineID: nestedPipelineNode.LatestPipelineID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to process update nested pipeline event", errors.WithSpan(span))
	}

	// Create an activity event tied to the deployment but associate with parent pipeline.
	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &newPipeline.ProjectID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetPipeline,
			TargetID:   &parentPipeline.Metadata.ID,
			Payload: &models.ActivityEventUpdatePipelineNodePayload{
				NodePath: nestedPipelineNode.Path,
				NodeType: statemachine.PipelineNodeType.String(),
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return pipelineToReturn, nil
}

func (s *service) RetryNestedPipeline(ctx context.Context, parentPipelineID, parentNestedPipelineNodePath string) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.RetryNestedPipeline")
	span.SetAttributes(attribute.String("parentPipelineId", parentPipelineID))
	span.SetAttributes(attribute.String("parentNestedPipelineNodePath", parentNestedPipelineNodePath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	parentPipeline, err := s.getPipelineByID(ctx, span, parentPipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(parentPipeline.ProjectID), auth.WithPipelineID(parentPipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// Mark caller as authorized
	caller.Authorized()

	nestedPipelineNode, ok := parentPipeline.GetNestedPipeline(parentNestedPipelineNodePath)
	if !ok {
		return nil, errors.New("nested pipeline %s not found in pipeline", parentNestedPipelineNodePath, errors.WithErrorCode(errors.ENotFound))
	}

	nestedPipeline, err := s.getPipelineByID(ctx, span, nestedPipelineNode.LatestPipelineID)
	if err != nil {
		return nil, err
	}

	// For the nested pipeline, if there are applicable environment protection rules, check that they are satisfied.
	if nestedPipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx,
			nestedPipeline.ProjectID, *nestedPipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	// Get the variables from the pipeline, so we can recreate the pipeline with the same variables.
	variables, err := s.getPipelineVariables(ctx, nestedPipeline.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline variables", errors.WithSpan(span))
	}

	// Determine the number of nested levels by walking the hierarchy.
	// Already have the parent, so start there.
	nestedLevel := 1
	walkingParentPipelineID := parentPipeline.ParentPipelineID
	for walkingParentPipelineID != nil {
		nestedLevel++
		newAncestor, gErr := s.getPipelineByID(ctx, span, *walkingParentPipelineID)
		if gErr != nil {
			// The error has already been recorded in the span.
			return nil, errors.Wrap(gErr, "failed to get the parent pipeline while checking the nested pipeline depth")
		}

		walkingParentPipelineID = newAncestor.ParentPipelineID
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for RetryNestedPipeline: %v", txErr)
		}
	}()

	// Create the new pipeline, so it can be updated in the parent pipeline.
	newPipeline, err := s.createPipeline(txContext, &CreatePipelineInput{
		EnvironmentName:        nestedPipeline.EnvironmentName,
		ParentPipelineID:       nestedPipeline.ParentPipelineID,
		ParentPipelineNodePath: nestedPipeline.ParentPipelineNodePath,
		Type:                   nestedPipeline.Type,
		ReleaseID:              nestedPipeline.ReleaseID,
		PipelineTemplateID:     nestedPipeline.PipelineTemplateID,
		When:                   nestedPipeline.When,
		Variables:              variables,
		Annotations:            nestedPipeline.Annotations,
		ApprovalRuleIDs:        nestedPipeline.ApprovalRuleIDs,
	}, nestedLevel)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create pipeline", errors.WithSpan(span))
	}

	pipelineToReturn, err := s.pipelineUpdater.ProcessEvent(txContext, parentPipeline.Metadata.ID, &RetryNestedPipelineEvent{
		NestedPipelinePath: nestedPipelineNode.Path,
		NestedPipelineID:   newPipeline.Metadata.ID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to process retry nested pipeline event", errors.WithSpan(span))
	}

	// Create an activity event tied to the deployment but associate with parent pipeline.
	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &newPipeline.ProjectID,
			Action:     models.ActionRetry,
			TargetType: models.TargetPipeline,
			TargetID:   &parentPipeline.Metadata.ID,
			Payload: &models.ActivityEventRetryPipelineNodePayload{
				NodePath: nestedPipelineNode.Path,
				NodeType: statemachine.PipelineNodeType.String(),
			},
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return pipelineToReturn, nil
}

func (s *service) DeferPipelineNode(ctx context.Context, input *DeferPipelineNodeInput) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.DeferPipelineNode")
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	span.SetAttributes(attribute.String("nodePath", input.NodePath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// If there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	if input.Reason == "" || len(input.Reason) > maxDeferReasonLength {
		return nil, errors.New("reason must not be empty and must be less than %d characters", maxDeferReasonLength, errors.WithErrorCode(errors.EInvalid))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeferPipelineNode: %v", txErr)
		}
	}()

	var (
		eventPipeline      *models.Pipeline
		activityEventInput = &activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			TargetID:   &pipeline.Metadata.ID,
			Action:     models.ActionDefer,
			TargetType: models.TargetPipeline,
		}
	)

	switch input.NodeType {
	case statemachine.PipelineNodeType:
		nestedPipelineNode, ok := pipeline.GetNestedPipeline(input.NodePath)
		if !ok {
			return nil, errors.New("nested pipeline %s not found in pipeline", input.NodePath, errors.WithErrorCode(errors.ENotFound))
		}

		// For the nested pipeline, if there are applicable environment protection rules, check that they are satisfied.
		// Project name should be the same for parent vs. nested pipelines.
		if nestedPipelineNode.EnvironmentName != nil {
			if eErr := s.environmentGatekeeper.checkEnvironmentRules(txContext, pipeline.ProjectID, *nestedPipelineNode.EnvironmentName); eErr != nil {
				// If the error is due to a rule violation, the error already includes an error code.
				return nil, eErr
			}
		}

		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &DeferNestedPipelineEvent{
			NestedPipelinePath: input.NodePath,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process defer pipeline node event", errors.WithSpan(span))
		}

		activityEventInput.Payload = &models.ActivityEventDeferPipelineNodePayload{
			NodePath: input.NodePath,
			NodeType: statemachine.PipelineNodeType.String(),
			Reason:   input.Reason,
		}
		eventPipeline = updatedPipeline
	case statemachine.TaskNodeType:
		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &DeferTaskEvent{
			TaskPath: input.NodePath,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process defer pipeline node event", errors.WithSpan(span))
		}

		activityEventInput.Payload = &models.ActivityEventDeferPipelineNodePayload{
			NodePath: input.NodePath,
			NodeType: statemachine.TaskNodeType.String(),
			Reason:   input.Reason,
		}
		eventPipeline = updatedPipeline
	default:
		return nil, errors.New("invalid node type %s: only PIPELINE and TASK node types are supported", input.NodeType, errors.WithErrorCode(errors.EInvalid))
	}

	if _, err := s.activityService.CreateActivityEvent(txContext, activityEventInput); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return eventPipeline, nil
}

func (s *service) UndeferPipelineNode(ctx context.Context, input *UndeferPipelineNodeInput) (*models.Pipeline, error) {
	ctx, span := tracer.Start(ctx, "svc.UndeferPipelineNode")
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	span.SetAttributes(attribute.String("nodePath", input.NodePath))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.getPipelineByID(ctx, span, input.PipelineID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ExecutePipeline, auth.WithProjectID(pipeline.ProjectID), auth.WithPipelineID(pipeline.Metadata.ID))
	if err != nil {
		return nil, err
	}

	// If there are applicable environment protection rules, check that they are satisfied.
	if pipeline.EnvironmentName != nil {
		if eErr := s.environmentGatekeeper.checkEnvironmentRules(ctx, pipeline.ProjectID, *pipeline.EnvironmentName); eErr != nil {
			// If the error is due to a rule violation, the error already includes an error code.
			return nil, eErr
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UndeferPipelineNode: %v", txErr)
		}
	}()

	var (
		eventPipeline      *models.Pipeline
		activityEventInput = &activityevent.CreateActivityEventInput{
			ProjectID:  &pipeline.ProjectID,
			TargetID:   &pipeline.Metadata.ID,
			Action:     models.ActionUndefer,
			TargetType: models.TargetPipeline,
		}
	)

	switch input.NodeType {
	case statemachine.PipelineNodeType:
		nestedPipelineNode, ok := pipeline.GetNestedPipeline(input.NodePath)
		if !ok {
			return nil, errors.New("nested pipeline %s not found in pipeline", input.NodePath, errors.WithErrorCode(errors.ENotFound))
		}

		// For the nested pipeline, if there are applicable environment protection rules, check that they are satisfied.
		// Project name should be the same for parent vs. nested pipelines.
		if nestedPipelineNode.EnvironmentName != nil {
			if eErr := s.environmentGatekeeper.checkEnvironmentRules(txContext, pipeline.ProjectID, *nestedPipelineNode.EnvironmentName); eErr != nil {
				// If the error is due to a rule violation, the error already includes an error code.
				return nil, eErr
			}
		}

		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &UndeferNestedPipelineEvent{
			NestedPipelinePath: input.NodePath,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process defer pipeline node event", errors.WithSpan(span))
		}

		activityEventInput.Payload = &models.ActivityEventUndeferPipelineNodePayload{
			NodePath: input.NodePath,
			NodeType: statemachine.PipelineNodeType.String(),
		}
		eventPipeline = updatedPipeline
	case statemachine.TaskNodeType:
		updatedPipeline, eventError := s.pipelineUpdater.ProcessEvent(txContext, pipeline.Metadata.ID, &UndeferTaskEvent{
			TaskPath: input.NodePath,
		})
		if eventError != nil {
			return nil, errors.Wrap(eventError, "failed to process defer pipeline node event", errors.WithSpan(span))
		}

		activityEventInput.Payload = &models.ActivityEventUndeferPipelineNodePayload{
			NodePath: input.NodePath,
			NodeType: statemachine.TaskNodeType.String(),
		}
		eventPipeline = updatedPipeline
	default:
		return nil, errors.New("invalid node type %s: only PIPELINE and TASK node types are supported", input.NodeType, errors.WithErrorCode(errors.EInvalid))
	}

	if _, err := s.activityService.CreateActivityEvent(txContext, activityEventInput); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return eventPipeline, nil
}

func (s *service) resolvePipelineVariables(
	ctx context.Context,
	input *resolvePipelineVariablesInput,
) ([]Variable, error) {
	response := []Variable{}

	// Build map of input variable for faster lookup
	hclVariableMap := map[string]Variable{}
	for _, v := range input.variables {
		response = append(response, v)

		if v.Category == models.HCLVariable {
			hclVariableMap[v.Key] = v
		}
	}

	if input.variableSetRevision != nil {
		var projectVariableSet *models.ProjectVariableSet
		if *input.variableSetRevision == "latest" {
			// Get the latest revision of the variable set.
			latestResp, err := s.dbClient.ProjectVariableSets.GetProjectVariableSets(ctx, &db.GetProjectVariableSetsInput{
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Filter: &db.ProjectVariableSetFilter{
					ProjectID: &input.project.Metadata.ID,
					Latest:    ptr.Bool(true),
				},
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to get latest project variable set")
			}

			if len(latestResp.ProjectVariableSets) > 0 {
				projectVariableSet = latestResp.ProjectVariableSets[0]
			}
		} else {
			prn := models.ProjectVariableSetResource.BuildPRN(input.project.GetOrgName(), input.project.Name, *input.variableSetRevision)
			pvs, err := s.dbClient.ProjectVariableSets.GetProjectVariableSetByPRN(ctx, prn)
			if err != nil {
				return nil, errors.Wrap(err, "failed to get project variable set %q", prn)
			}
			if pvs == nil {
				return nil, errors.New("project variable set %q not found", prn, errors.WithErrorCode(errors.ENotFound))
			}
			projectVariableSet = pvs
		}

		if projectVariableSet != nil {
			var environmentScopes []string
			if input.environmentName != nil {
				environmentScopes = []string{*input.environmentName, models.ProjectVariableEnvironmentScopeAll}
			}

			pipelineType := input.pipelineType

			// Nested pipelines are a special case since the parent pipeline can be a deployment pipeline or a runbook pipeline.
			// We'll use the presence of an environment name to determine the pipeline type since only deployment pipelines can
			// have an environment associated.
			if pipelineType == models.NestedPipelineType {
				if input.environmentName != nil {
					pipelineType = models.DeploymentPipelineType
				} else {
					pipelineType = models.RunbookPipelineType
				}
			}

			// Get variables in the variable set.
			variablesResp, err := s.dbClient.ProjectVariables.GetProjectVariables(ctx, &db.GetProjectVariablesInput{
				Filter: &db.ProjectVariableFilter{
					ProjectVariableSetID: &projectVariableSet.Metadata.ID,
					PipelineType:         &pipelineType,
					EnvironmentScopes:    environmentScopes,
				},
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to get project variables")
			}

			// If this is a deployment pipeline, we only want variables that are scoped for the target environment.
			for _, v := range variablesResp.ProjectVariables {
				if _, ok := hclVariableMap[v.Key]; ok {
					// Skip this variable because input variables take precedence.
					continue
				}
				response = append(response, Variable{
					Key:      v.Key,
					Value:    v.Value,
					Category: models.HCLVariable,
				})
			}
		}
	}

	if err := s.checkForDuplicateVariables(response); err != nil {
		return nil, err
	}

	// Sort variables by key
	sort.Slice(response, func(i, j int) bool {
		return response[i].Key < response[j].Key
	})

	return response, nil
}

func toPipelineNodeScheduleModel(schedule *config.PipelineNodeSchedule) (*time.Time, *models.CronSchedule, error) {
	var scheduledStartTime *time.Time
	var cronSchedule *models.CronSchedule

	if schedule != nil {
		switch schedule.Type {
		case config.CronScheduleType:
			options, err := schedule.GetCronOptions()
			if err != nil {
				return nil, nil, errors.Wrap(err, "failed to get cron schedule options")
			}

			cronSchedule = &models.CronSchedule{
				Expression: options.Expression,
				Timezone:   options.Timezone,
			}
		case config.DatetimeScheduleType:
			options, err := schedule.GetDatetimeOptions()
			if err != nil {
				return nil, nil, errors.Wrap(err, "failed to get datetime schedule options")
			}

			scheduledStartTime = &options.Timestamp
		default:
			return nil, nil, errors.New(
				"invalid schedule type %s, must be %s or %s",
				schedule.Type,
				config.CronScheduleType,
				config.DatetimeScheduleType,
			)
		}

	}

	return scheduledStartTime, cronSchedule, nil
}

func (s *service) toTaskModels(
	stageName string,
	hclTasks []*config.PipelineTask,
	agent *config.Agent,
) ([]*models.PipelineTask, error) {
	var tasks []*models.PipelineTask

	for _, t := range hclTasks {
		when := models.AutoPipelineTask
		if t.When != nil {
			switch *t.When {
			case "auto":
				when = models.AutoPipelineTask
			case "manual":
				when = models.ManualPipelineTask
			case "never":
				// Exclude the task from the pipeline.
				continue
			default:
				return nil, errors.New(
					"stage %s has task %s with invalid when condition value %s, must be auto, manual or never",
					stageName,
					t.Name,
					*t.When,
					errors.WithErrorCode(errors.EInvalid),
				)
			}
		}

		var agentTags []string
		if t.Agent != nil && t.Agent.Tags != nil {
			agentTags = t.Agent.Tags
		} else if agent != nil && agent.Tags != nil {
			agentTags = agent.Tags
		} else {
			agentTags = []string{}
		}

		dependencies := []string{}
		if t.Dependencies != nil {
			dependencies = t.Dependencies
		}

		maxAttempts := 1
		if t.Attempts != nil {
			maxAttempts = *t.Attempts
		}

		var interval *time.Duration
		if maxAttempts > 1 {
			interval = ptr.Duration(time.Minute * 5)
			if t.Interval != nil {
				dur, pErr := time.ParseDuration(*t.Interval)
				if pErr != nil {
					return nil, pErr
				}
				interval = &dur
			}
		}

		scheduledStartTime, cronSchedule, err := toPipelineNodeScheduleModel(t.Schedule)
		if err != nil {
			return nil, errors.Wrap(err, "stage %s has task %s with invalid schedule", stageName, t.Name)
		}

		onError := statemachine.FailOnError
		if t.OnError != nil {
			switch *t.OnError {
			case "fail":
				onError = statemachine.FailOnError
			case "continue":
				onError = statemachine.ContinueOnError
			default:
				return nil, errors.New(
					"stage %s has task %s with invalid on_error value %s, must be fail or continue",
					stageName,
					t.Name,
					*t.OnError,
					errors.WithErrorCode(errors.EInvalid),
				)
			}
		}

		task := &models.PipelineTask{
			Path:                fmt.Sprintf("pipeline.stage.%s.task.%s", stageName, t.Name),
			Name:                t.Name,
			ApprovalStatus:      models.ApprovalNotRequired,
			Status:              statemachine.CreatedNodeStatus,
			When:                when,
			Interval:            interval,
			Image:               t.Image,
			HasSuccessCondition: !t.SuccessCondition.Range().Empty(),
			MaxAttempts:         maxAttempts,
			AgentTags:           agentTags,
			Dependencies:        dependencies,
			ApprovalRuleIDs:     []string{},
			OnError:             onError,
			ScheduledStartTime:  scheduledStartTime,
			CronSchedule:        cronSchedule,
		}

		for _, a := range t.Actions {
			action := &models.PipelineAction{
				Path:       fmt.Sprintf("pipeline.stage.%s.task.%s.action.%s", stageName, t.Name, a.Name()),
				Name:       a.Name(),
				PluginName: a.PluginName(),
				ActionName: a.PluginActionName(),
				Status:     statemachine.CreatedNodeStatus,
			}
			task.Actions = append(task.Actions, action)
		}
		tasks = append(tasks, task)
	}

	return tasks, nil
}

func (s *service) checkForDuplicateVariables(pipelineVariables []Variable) error {
	variableMap := map[string]bool{}

	for _, v := range pipelineVariables {
		key := fmt.Sprintf("%s::%s", v.Key, v.Category)
		if _, ok := variableMap[key]; ok {
			return errors.New("duplicate %s variable %q found in pipeline variables", v.Category, v.Key, errors.WithErrorCode(errors.EInvalid))
		}
		variableMap[key] = true
	}

	return nil
}

// getPipelineVariables returns the pipeline variables stored in object storage.
func (s *service) getPipelineVariables(ctx context.Context, pipelineID string) ([]Variable, error) {
	reader, err := s.store.GetPipelineVariables(ctx, pipelineID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline variables")
	}
	defer reader.Close()

	var variables []Variable
	if err = json.NewDecoder(reader).Decode(&variables); err != nil {
		return nil, errors.Wrap(err, "failed to decode pipeline variables")
	}

	return variables, nil
}

// getPipelineByID returns a non-nil pipeline.
// If there is an error or no such pipeline exists, it records the error in the span.
func (s *service) getPipelineByID(ctx context.Context, span trace.Span, id string) (*models.Pipeline, error) {
	gotPipeline, err := s.dbClient.Pipelines.GetPipelineByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline by ID", errors.WithSpan(span))
	}

	if gotPipeline == nil {
		return nil, errors.New(
			"pipeline with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return gotPipeline, nil
}

// convertHCLTemplateToModels converts a HCL pipeline template nodes to their model equivalents.
func (s *service) convertHCLTemplateToModels(
	ctx context.Context,
	hclConfig *config.PipelineTemplate,
	evalCtx *hclctx.EvalContext,
	projectID string,
	pipelineTemplateID string,
) (*convertedTemplateData, error) {
	var (
		stages                       = []*models.PipelineStage{}
		nestedPipelineTemplateIDs    = map[string]string{}
		nestedPipelineVariables      = map[string][]Variable{}
		nestedPipelineWhenConditions = map[string]models.PipelineWhenCondition{}
		hclStages                    = map[string]*config.PipelineStage{}
	)

	// Build a map of stages since we'll need to add them according to the stage sequence.
	for _, stage := range hclConfig.Stages {
		// Populate the stage map.
		hclStages[stage.Name] = stage
	}

	for _, stageName := range hclConfig.GetStageOrder() {
		hclStage, ok := hclStages[stageName]
		if !ok {
			return nil, errors.New("'stage_order' list contains a stage name that is missing from the pipeline template. Ensure that the 'stage_order' block for stage %q is defined in the template", stageName, errors.WithErrorCode(errors.EInvalid))
		}

		data, err := s.convertHCLStageToModels(ctx, hclStage, evalCtx, hclConfig.Agent, projectID, pipelineTemplateID)
		if err != nil {
			return nil, err
		}

		// Copy the nested pipeline template IDs, variables and when conditions.
		maps.Copy(nestedPipelineTemplateIDs, data.nestedPipelineTemplateIDs)
		maps.Copy(nestedPipelineVariables, data.nestedPipelineVariables)
		maps.Copy(nestedPipelineWhenConditions, data.nestedPipelineWhenConditions)
		stages = append(stages, data.stages...)
	}

	if len(stages) == 0 {
		return nil, errors.New("either no stages were defined or all stages were excluded by 'when' conditions; a pipeline must have at least one valid stage", errors.WithErrorCode(errors.EInvalid))
	}

	return &convertedTemplateData{
		stages:                       stages,
		nestedPipelineTemplateIDs:    nestedPipelineTemplateIDs,
		nestedPipelineVariables:      nestedPipelineVariables,
		nestedPipelineWhenConditions: nestedPipelineWhenConditions,
	}, nil
}

// convertHCLStageToModel converts a HCL pipeline stage node to its model equivalent.
func (s *service) convertHCLStageToModels(
	ctx context.Context,
	hclStage *config.PipelineStage,
	evalCtx *hclctx.EvalContext,
	agent *config.Agent,
	projectID,
	pipelineTemplateID string,
) (*convertedHCLStageData, error) {
	preStage, err := s.createPreOrPostStage(hclStage.Name, "pre", hclStage.PreTasks, agent)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create pre-stage")
	}

	stage := &models.PipelineStage{
		Path:   fmt.Sprintf("pipeline.stage.%s", hclStage.Name),
		Name:   hclStage.Name,
		Status: statemachine.CreatedNodeStatus,
	}

	stage.Tasks, err = s.toTaskModels(hclStage.Name, hclStage.Tasks, agent)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create tasks")
	}

	nestedPipelineWhenConditions := map[string]models.PipelineWhenCondition{}
	nestedPipelineTemplateIDs := map[string]string{}
	nestedPipelineVariables := map[string][]Variable{}

	for _, hclNestedPipeline := range hclStage.NestedPipelines {
		data, cErr := s.convertHCLNestedPipelineToModel(ctx, hclNestedPipeline, evalCtx, hclStage.Name, projectID, pipelineTemplateID)
		if cErr != nil {
			return nil, cErr
		}

		// Add the nested pipeline if it is not excluded.
		if data != nil {
			path := data.nestedPipeline.Path
			nestedPipelineWhenConditions[path] = data.when
			nestedPipelineTemplateIDs[path] = data.templateID
			nestedPipelineVariables[path] = data.variables
			stage.NestedPipelines = append(stage.NestedPipelines, data.nestedPipeline)
		}
	}

	// Ensure any dependencies exist now that all tasks and nested pipelines have been processed.
	// We must do this afterwards since stage tasks could be out of order.
	if err = s.verifyDependenciesExist(stage); err != nil {
		return nil, errors.Wrap(err, "failed to validate dependencies")
	}

	postStage, err := s.createPreOrPostStage(hclStage.Name, "post", hclStage.PostTasks, agent)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create post-stage")
	}

	// Main stage is excluded if it has no tasks or nested pipelines.
	mainStageIsExcluded := len(stage.Tasks) == 0 && len(stage.NestedPipelines) == 0

	// Don't allow excluding "main" stage when a pre or post stage is defined since they can't run without it.
	if (preStage != nil || postStage != nil) && mainStageIsExcluded {
		return nil, errors.New(
			"stage %s has no tasks or nested pipelines but has a pre or post condition, ensure at least one task or nested pipeline is defined and does not have a 'when' condition of 'never'",
			hclStage.Name,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	// Append the stages in the order they were defined (pre, main, post).
	stages := []*models.PipelineStage{}
	if preStage != nil {
		stages = append(stages, preStage)
	}

	if !mainStageIsExcluded {
		stages = append(stages, stage)
	}

	if postStage != nil {
		stages = append(stages, postStage)
	}

	return &convertedHCLStageData{
		stages:                       stages,
		nestedPipelineTemplateIDs:    nestedPipelineTemplateIDs,
		nestedPipelineVariables:      nestedPipelineVariables,
		nestedPipelineWhenConditions: nestedPipelineWhenConditions,
	}, nil
}

func (s *service) convertHCLNestedPipelineToModel(
	ctx context.Context,
	hclNestedPipeline *config.NestedPipeline,
	evalCtx *hclctx.EvalContext,
	stageName,
	projectID,
	pipelineTemplateID string,
) (*convertedNestedPipelineData, error) {
	scheduledStartTime, cronSchedule, err := toPipelineNodeScheduleModel(hclNestedPipeline.Schedule)
	if err != nil {
		return nil, errors.Wrap(err, "stage %s has nested pipeline %s with invalid schedule", stageName, hclNestedPipeline.Name)
	}

	nestedPipeline := &models.NestedPipeline{
		Path:               fmt.Sprintf("pipeline.stage.%s.pipeline.%s", stageName, hclNestedPipeline.Name),
		Name:               hclNestedPipeline.Name,
		EnvironmentName:    hclNestedPipeline.EnvironmentName,
		Status:             statemachine.CreatedNodeStatus,
		ApprovalStatus:     models.ApprovalNotRequired,
		OnError:            statemachine.FailOnError,
		ScheduledStartTime: scheduledStartTime,
		CronSchedule:       cronSchedule,
	}

	when := models.AutoPipeline
	if hclNestedPipeline.When != nil {
		switch *hclNestedPipeline.When {
		case "auto":
			when = models.AutoPipeline
		case "manual":
			when = models.ManualPipeline
		case "never":
			// Exclude the nested pipeline from the pipeline.
			return nil, nil
		default:
			return nil, errors.New(
				"stage %s has nested pipeline %s with an invalid when condition",
				stageName,
				hclNestedPipeline.Name,
				errors.WithErrorCode(errors.EInvalid),
			)
		}
	}

	nestedPipeline.Dependencies = []string{}
	if hclNestedPipeline.Dependencies != nil {
		nestedPipeline.Dependencies = hclNestedPipeline.Dependencies
	}

	if hclNestedPipeline.OnError != nil {
		switch *hclNestedPipeline.OnError {
		case "fail":
			nestedPipeline.OnError = statemachine.FailOnError
		case "continue":
			nestedPipeline.OnError = statemachine.ContinueOnError
		default:
			return nil, errors.New(
				"stage %s has nested pipeline %s with an invalid on_error value %s, must be fail or continue",
				stageName,
				hclNestedPipeline.Name,
				*hclNestedPipeline.OnError,
				errors.WithErrorCode(errors.EInvalid),
			)
		}
	}

	templateID, err := s.resolveNestedPipelineTemplateID(ctx, hclNestedPipeline.TemplateID, evalCtx, projectID, pipelineTemplateID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to resolve template ID for nested pipeline %s", hclNestedPipeline.Name)
	}

	variables, err := resolveNestedPipelineVariables(hclNestedPipeline.Variables, evalCtx)
	if err != nil {
		return nil, err
	}

	return &convertedNestedPipelineData{
		when:           when,
		templateID:     templateID,
		nestedPipeline: nestedPipeline,
		variables:      variables,
	}, nil
}

// resolveNestedPipelineTemplateID resolves the nested pipeline template ID from the HCL expression.
func (s *service) resolveNestedPipelineTemplateID(
	ctx context.Context,
	templateIDExpr hcl.Expression,
	evalCtx *hclctx.EvalContext,
	projectID,
	pipelineTemplateID string,
) (string, error) {
	// Resolve template ID
	templateIDCtyVal, diags := templateIDExpr.Value(evalCtx.Context())
	if diags.HasErrors() {
		return "", errors.Wrap(diags, "failed to resolve nested pipeline template ID", errors.WithErrorCode(errors.EInvalid))
	}

	if templateIDCtyVal.Type() != cty.String {
		rawValue, mErr := ctyjson.Marshal(templateIDCtyVal, templateIDCtyVal.Type())
		if mErr != nil {
			return "", errors.Wrap(mErr, "failed to decode template ID", errors.WithErrorCode(errors.EInvalid))
		}
		return "", errors.New("invalid pipeline template ID %s", string(rawValue), errors.WithErrorCode(errors.EInvalid))
	}

	var (
		templateID = templateIDCtyVal.AsString()
		template   *models.PipelineTemplate
		err        error
	)

	// Find the pipeline template by PRN or GID.
	if strings.HasPrefix(templateID, models.PRNPrefix) {
		template, err = s.dbClient.PipelineTemplates.GetPipelineTemplateByPRN(ctx, templateID)
	} else {
		template, err = s.dbClient.PipelineTemplates.GetPipelineTemplateByID(ctx, gid.FromGlobalID(templateID))
	}
	if err != nil {
		// Wrap the error since DB errors can be generic here.
		return "", errors.Wrap(err, "invalid nested pipeline template %s", templateID)
	}

	if template == nil || template.ProjectID != projectID || template.Metadata.ID == pipelineTemplateID {
		return "", errors.New("nested pipeline is referencing an invalid pipeline template %s", templateID, errors.WithErrorCode(errors.EInvalid))
	}

	if template.Status != models.PipelineTemplateUploaded {
		return "", errors.New("nested pipeline template %s must be in the uploaded state", templateID, errors.WithErrorCode(errors.EInvalid))
	}

	return template.Metadata.ID, nil
}

// resolveNestedPipelineVariables resolves the nested pipeline variables from the HCL expression.
func resolveNestedPipelineVariables(variablesExpr hcl.Expression, evalCtx *hclctx.EvalContext) ([]Variable, error) {
	// Resolve nested pipeline variables
	ctyVal, diags := variablesExpr.Value(evalCtx.Context())
	if diags.HasErrors() {
		return nil, errors.Wrap(
			diags,
			"failed to resolve nested pipeline variables",
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	if ctyVal.IsNull() {
		// No variables to resolve
		return []Variable{}, nil
	}

	if !ctyVal.Type().IsObjectType() {
		return nil, errors.New("nested pipeline variables must be an object, got %s",
			ctyVal.Type().FriendlyName(),
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	nestedPipelineVariables := []Variable{}
	for name, value := range ctyVal.AsValueMap() {
		var strValue string
		if value.Type() == cty.String {
			// The value is a string so just return it as a string type w/o marshalling
			strValue = value.AsString()
		} else {
			// The value is not a string so it needs to be marshalled to a string
			rawValue, mErr := ctyjson.Marshal(value, value.Type())
			if mErr != nil {
				return nil, mErr
			}
			strValue = string(rawValue)
		}
		// Convert value to string
		nestedPipelineVariables = append(nestedPipelineVariables, Variable{
			Key:      name,
			Value:    strValue,
			Category: models.HCLVariable,
		})
	}

	return nestedPipelineVariables, nil
}

// createPreOrPostStage creates a pre or post stage with the given tasks.
func (s *service) createPreOrPostStage(
	mainStageName string,
	stageType string,
	stageTasks *config.PipelineCondition,
	agent *config.Agent,
) (*models.PipelineStage, error) {
	if stageTasks == nil {
		// Return since there are no tasks.
		return nil, nil
	}

	stageName := fmt.Sprintf("%s.%s", mainStageName, stageType)
	stage := &models.PipelineStage{
		Path:   fmt.Sprintf("pipeline.stage.%s", stageName),
		Name:   stageName,
		Status: statemachine.CreatedNodeStatus,
	}

	var err error
	stage.Tasks, err = s.toTaskModels(stageName, stageTasks.Tasks, agent)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create %s-tasks", stageType)
	}

	// Ensure any dependencies exist now that all pre or post tasks have been processed.
	// We must do this afterwards since stage tasks could be out of order.
	if err = s.verifyDependenciesExist(stage); err != nil {
		return nil, errors.Wrap(err, "failed to validate dependencies")
	}

	if len(stage.Tasks) == 0 {
		// Return since all tasks are excluded.
		return nil, nil
	}

	return stage, nil
}

// verifyDependenciesExist ensures dependencies exist for tasks / nested pipelines in a given stage.
func (s *service) verifyDependenciesExist(stage *models.PipelineStage) error {
	availableNodes := map[string]struct{}{}

	for _, t := range stage.Tasks {
		availableNodes[t.Name] = struct{}{}
	}

	for _, np := range stage.NestedPipelines {
		availableNodes[np.Name] = struct{}{}
	}

	for _, t := range stage.Tasks {
		for _, d := range t.Dependencies {
			if _, ok := availableNodes[d]; !ok {
				return errors.New(
					"task %q has an unknown dependency %q, the dependency either doesn't exist or has a when condition set to 'never'",
					t.Path,
					d,
					errors.WithErrorCode(errors.EInvalid),
				)
			}
		}
	}

	for _, np := range stage.NestedPipelines {
		for _, d := range np.Dependencies {
			if _, ok := availableNodes[d]; !ok {
				return errors.New(
					"nested pipeline %q has an unknown dependency %q, the dependency either doesn't exist or has a when condition set to 'never'",
					np.Path,
					d,
					errors.WithErrorCode(errors.EInvalid),
				)
			}
		}
	}

	return nil
}
