package pipeline

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/environment"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/jws"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// Directory that contains the test data for the HCL pipeline templates.
const testPipelineHCLDir = "../../../testdata/hcl/pipeline"

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	pipelineUpdater := NewMockUpdater(t)
	dataStore := NewMockStore(t)
	pipelineTemplateStore := pipelinetemplate.NewMockDataStore(t)
	limitChecker := limits.NewMockLimitChecker(t)
	eventManager := events.NewEventManager(dbClient, logger)
	activityService := activityevent.NewMockService(t)
	idp := auth.NewIdentityProvider(nil, "http://phobos")
	environmentService := environment.NewMockService(t)
	environmentGatekeeper := NewMockEnvironmentGatekeeper(t)

	expect := &service{
		logger:                 logger,
		dbClient:               dbClient,
		pipelineUpdater:        pipelineUpdater,
		limitChecker:           limitChecker,
		dataStore:              pipelineTemplateStore,
		store:                  dataStore,
		eventManager:           eventManager,
		activityService:        activityService,
		idp:                    idp,
		environmentService:     environmentService,
		environmentGatekeeper:  environmentGatekeeper,
		maxNestedPipelineLevel: maxNestedPipelineLevel,
	}

	assert.Equal(t, expect,
		NewService(logger, dbClient, limitChecker, pipelineTemplateStore, dataStore, pipelineUpdater, eventManager,
			activityService, idp, environmentService, environmentGatekeeper),
	)
}

func TestGetPipelineByID(t *testing.T) {
	pipelineID := "pipeline-1"

	goodPipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: pipelineID,
		},
	}

	type testCase struct {
		injectPermissionError error
		findPipeline          *models.Pipeline
		expectPipeline        *models.Pipeline
		name                  string
		expectErrorCode       errors.CodeType
		withCaller            bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "pipeline not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			findPipeline:          goodPipeline,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:           "successfully return a pipeline pipeline",
			withCaller:     true,
			findPipeline:   goodPipeline,
			expectPipeline: goodPipeline,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).
					Return(test.findPipeline, nil)

				if test.findPipeline != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				Projects:  mockProjects,
				Pipelines: mockPipelines,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPipeline, err := service.GetPipelineByID(ctx, pipelineID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectPipeline, actualPipeline)
		})
	}
}

func TestGetPipelineByPRN(t *testing.T) {
	pipelinePRN := "prn:pipeline:some-org/some-project/some-pipeline-id"

	goodPipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			PRN: pipelinePRN,
		},
	}

	type testCase struct {
		injectPermissionError error
		findPipeline          *models.Pipeline
		expectPipeline        *models.Pipeline
		name                  string
		expectErrorCode       errors.CodeType
		withCaller            bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "pipeline not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			findPipeline:          goodPipeline,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:           "successfully return a pipeline pipeline",
			withCaller:     true,
			findPipeline:   goodPipeline,
			expectPipeline: goodPipeline,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockPipelines.On("GetPipelineByPRN", mock.Anything, pipelinePRN).
					Return(test.findPipeline, nil)

				if test.findPipeline != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPipeline, err := service.GetPipelineByPRN(ctx, pipelinePRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectPipeline, actualPipeline)
		})
	}
}

func TestGetPipelineByReleaseID(t *testing.T) {
	releaseID := "release-1"

	samplePipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: "pipeline-1",
		},
		ReleaseID: &releaseID,
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		pipeline        *models.Pipeline
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:     "successfully return a pipeline by release ID",
			pipeline: samplePipeline,
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "subject is not authorized to access the pipeline",
			pipeline:        samplePipeline,
			authError:       errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)

			mockPipelines.On("GetPipelineByReleaseID", mock.Anything, releaseID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPipeline, err := service.GetPipelineByReleaseID(auth.WithCaller(ctx, mockCaller), releaseID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.pipeline, actualPipeline)
		})
	}
}

func TestGetPipelinesByIDs(t *testing.T) {
	inputList := []string{"pt-1", "pt-2"}

	goodPipelines := []models.Pipeline{
		{CreatedBy: "some-pt-1"},
		{CreatedBy: "some-pt-2"},
	}

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		expectPipelines []models.Pipeline
		withCaller      bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "pipelines not found, possibly due to lack of permissions",
			withCaller: true,
		},
		{
			name:            "successfully return pipeline pipelines",
			withCaller:      true,
			expectPipelines: goodPipelines,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			if test.withCaller {
				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)

				dbInput := &db.GetPipelinesInput{
					Filter: &db.PipelineFilter{
						PipelineIDs: inputList,
					},
				}
				dbResult := &db.PipelinesResult{
					Pipelines: test.expectPipelines,
				}

				mockPipelines.On("GetPipelines", mock.Anything, dbInput).
					Return(dbResult, nil)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPipelines, err := service.GetPipelinesByIDs(ctx, inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectPipelines, actualPipelines)
		})
	}
}

func TestGetPipelines(t *testing.T) {
	sort := db.PipelineSortableFieldCreatedAtAsc

	input := &GetPipelinesInput{
		Sort: &sort,
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
		ProjectID:          "proj-id-1",
		PipelineTemplateID: ptr.String("template-id-1"),
	}

	type testCase struct {
		expectResult           *db.PipelinesResult
		name                   string
		expectErrorCode        errors.CodeType
		withCaller             bool
		keepProjectID          bool
		keepPipelineTemplateID bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "pipelines not found, possibly due to lack of permissions",
			withCaller: true,
		},
		{
			name:          "successfully return pipeline pipelines result, by project, positive",
			withCaller:    true,
			keepProjectID: true,
			expectResult: &db.PipelinesResult{
				Pipelines: []models.Pipeline{{CreatedBy: "pt-1"}},
			},
		},
		{
			name:                   "successfully return pipeline pipelines result, by pipeline template, positive",
			withCaller:             true,
			keepPipelineTemplateID: true,
			expectResult: &db.PipelinesResult{
				Pipelines: []models.Pipeline{{CreatedBy: "pt-1"}},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			dbInput := &db.GetPipelinesInput{
				// No need to set Sort and PaginationOptions for this test.
				Filter: &db.PipelineFilter{
					// Will be filled in later.
				},
			}

			if test.keepProjectID {
				dbInput.Filter.ProjectID = &input.ProjectID
			}

			if test.keepPipelineTemplateID {
				dbInput.Filter.PipelineTemplateID = input.PipelineTemplateID
			}

			if test.withCaller {
				mockPipelines.On("GetPipelines", mock.Anything, mock.Anything).Return(test.expectResult, nil)

				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetPipelines(ctx, input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestGetPipelineVariables(t *testing.T) {
	samplePipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: "pipeline-1",
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		pipeline        *models.Pipeline
		name            string
		expectErrorCode errors.CodeType
		expectVariable  Variable
	}

	testCases := []testCase{
		{
			name: "successfully return pipeline variables",
			expectVariable: Variable{
				Key:      "key",
				Value:    "value",
				Category: models.HCLVariable,
			},
			pipeline: samplePipeline,
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "subject is not authorized to view variable",
			pipeline:        samplePipeline,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockStore := NewMockStore(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, samplePipeline.Metadata.ID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewVariable, mock.Anything, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				buf, err := json.Marshal([]Variable{test.expectVariable})
				require.Nil(t, err)

				mockStore.On("GetPipelineVariables", mock.Anything, test.pipeline.Metadata.ID).Return(io.NopCloser(bytes.NewReader(buf)), nil)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
			}

			service := &service{
				dbClient: dbClient,
				store:    mockStore,
			}

			actualVariables, err := service.GetPipelineVariables(auth.WithCaller(ctx, mockCaller), samplePipeline.Metadata.ID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Len(t, actualVariables, 1)
			assert.Equal(t, test.expectVariable, actualVariables[0])
		})
	}
}

func TestCreatePipeline(t *testing.T) {
	// Following test attempts to verify whether the "main" pipeline
	// and the nested pipeline are created correctly.

	mainPipelineTemplateID := "a4e70f09-527a-437c-a543-9cb1949ad6c9"
	nestedPipelineTemplateID := "85cf59db-719b-4ca9-b4d3-a6c9a19ba3fe"
	projectID := "project-1"
	pipelineID := "pipeline-1"
	organizationID := "organization-1"
	releaseID := "release-1"
	testSubject := "test-subject"
	nestedPipelineID := "nested-pipeline-1"
	projectPRN := "prn:project:bit/sample"
	projectName := "sample"
	environmentID := "environment-1"
	currentTime := time.Now().UTC()

	type testCase struct {
		authError            error
		limitExceededError   error
		input                *CreatePipelineInput
		pipelineTemplate     *models.PipelineTemplate
		project              *models.Project
		expectParentPipeline *models.Pipeline
		expectNestedPipeline *models.Pipeline
		name                 string
		rawHCLFile           string
		expectErrorCode      errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create a runbook pipeline with no variables, nested pipelines or approval rules",
			input: &CreatePipelineInput{
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				PipelineTemplateID: mainPipelineTemplateID,
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			project: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:  projectID,
					PRN: projectPRN,
				},
				Name:  projectName,
				OrgID: organizationID,
			},
			rawHCLFile: "valid_task_action_only.hcl",
			expectParentPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                pipelineID,
					CreationTimestamp: &currentTime,
				},
				ProjectID:          projectID,
				PipelineTemplateID: mainPipelineTemplateID,
				CreatedBy:          testSubject,
				Status:             statemachine.CreatedNodeStatus,
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				ApprovalStatus:     models.ApprovalNotRequired,
				ApprovalRuleIDs:    []string{},
				Stages: []*models.PipelineStage{
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "successfully create a runbook pipeline with variables, nested pipelines and approval rules",
			input: &CreatePipelineInput{
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				PipelineTemplateID: mainPipelineTemplateID,
				Variables: []Variable{
					{
						Key:      "sleep_interval",
						Value:    "20",
						Category: models.HCLVariable,
					},
				},
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			project: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:  projectID,
					PRN: projectPRN,
				},
				Name:  projectName,
				OrgID: organizationID,
			},
			rawHCLFile: "valid_full.hcl",
			expectParentPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                pipelineID,
					CreationTimestamp: &currentTime,
				},
				ProjectID:          projectID,
				PipelineTemplateID: mainPipelineTemplateID,
				CreatedBy:          testSubject,
				Status:             statemachine.CreatedNodeStatus,
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				ApprovalStatus:     models.ApprovalNotRequired,
				ApprovalRuleIDs:    []string{},
				Stages: []*models.PipelineStage{
					{
						Name:   "dev.pre",
						Path:   "pipeline.stage.dev.pre",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "check_for_changes",
								Path:            "pipeline.stage.dev.pre.task.check_for_changes",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalRuleIDs: []string{},
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								OnError:         statemachine.ContinueOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "find_changes",
										Path:       "pipeline.stage.dev.pre.task.check_for_changes.action.find_changes",
										ActionName: "command",
										PluginName: "exec",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:            "deploy",
								Path:            "pipeline.stage.dev.pipeline.deploy",
								EnvironmentName: ptr.String("account1"),
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								Dependencies:    []string{},
								OnError:         statemachine.FailOnError,
							},
						},
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								Interval:        ptr.Duration(2 * time.Minute),
								MaxAttempts:     5,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
							{
								Name:            "security_scan",
								Path:            "pipeline.stage.dev.task.security_scan",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{"execute_a_command"},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "exec_command",
										Path:       "pipeline.stage.dev.task.security_scan.action.exec_command",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
					{
						Name:   "dev.post",
						Path:   "pipeline.stage.dev.post",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "cleanup_resources",
								Path:            "pipeline.stage.dev.post.task.cleanup_resources",
								Status:          statemachine.CreatedNodeStatus,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								ApprovalStatus:  models.ApprovalNotRequired,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_cleanup",
										Path:       "pipeline.stage.dev.post.task.cleanup_resources.action.perform_cleanup",
										ActionName: "command",
										PluginName: "exec",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
			},
			expectNestedPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                nestedPipelineID,
					CreationTimestamp: &currentTime,
				},
				EnvironmentName:        ptr.String("account1"),
				Annotations:            []*models.PipelineAnnotation{},
				ProjectID:              projectID,
				PipelineTemplateID:     nestedPipelineTemplateID,
				ParentPipelineID:       &pipelineID,
				ParentPipelineNodePath: ptr.String("pipeline.stage.dev.pipeline.deploy"),
				CreatedBy:              testSubject,
				Status:                 statemachine.CreatedNodeStatus,
				Type:                   models.DeploymentPipelineType,
				When:                   models.ManualPipeline,
				ApprovalStatus:         models.ApprovalNotRequired,
				ApprovalRuleIDs:        []string{},
				Stages: []*models.PipelineStage{
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "successfully create a release pipeline with no variables, nested pipelines or approval rules",
			input: &CreatePipelineInput{
				Type:               models.ReleaseLifecyclePipelineType,
				When:               models.ManualPipeline,
				ReleaseID:          &releaseID,
				PipelineTemplateID: mainPipelineTemplateID,
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			project: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:  projectID,
					PRN: projectPRN,
				},
				Name:  projectName,
				OrgID: organizationID,
			},
			rawHCLFile: "valid_task_action_only.hcl",
			expectParentPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                pipelineID,
					CreationTimestamp: &currentTime,
				},
				ProjectID:          projectID,
				PipelineTemplateID: mainPipelineTemplateID,
				CreatedBy:          testSubject,
				Status:             statemachine.CreatedNodeStatus,
				Type:               models.ReleaseLifecyclePipelineType,
				When:               models.ManualPipeline,
				ReleaseID:          &releaseID,
				ApprovalStatus:     models.ApprovalNotRequired,
				ApprovalRuleIDs:    []string{},
				Stages: []*models.PipelineStage{
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
			},
			// Shouldn't recurse since no nested pipelines are created
		},
		{
			name: "successfully create a release pipeline with variables, nested pipelines and approval rules",
			input: &CreatePipelineInput{
				Type:               models.ReleaseLifecyclePipelineType,
				When:               models.ManualPipeline,
				ReleaseID:          &releaseID,
				PipelineTemplateID: mainPipelineTemplateID,
				Variables: []Variable{
					{
						Key:      "sleep_interval",
						Value:    "20",
						Category: models.HCLVariable,
					},
					{
						Key:      "account_name",
						Value:    "\"test-account\"",
						Category: models.HCLVariable,
					},
				},
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			project: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:  projectID,
					PRN: projectPRN,
				},
				Name:  projectName,
				OrgID: organizationID,
			},
			rawHCLFile: "valid_full.hcl",
			expectParentPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                pipelineID,
					CreationTimestamp: &currentTime,
				},
				ProjectID:          projectID,
				PipelineTemplateID: mainPipelineTemplateID,
				CreatedBy:          testSubject,
				Status:             statemachine.CreatedNodeStatus,
				Type:               models.ReleaseLifecyclePipelineType,
				When:               models.ManualPipeline,
				ApprovalStatus:     models.ApprovalNotRequired,
				ApprovalRuleIDs:    []string{},
				ReleaseID:          &releaseID,
				Stages: []*models.PipelineStage{
					{
						Name:   "dev.pre",
						Path:   "pipeline.stage.dev.pre",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "check_for_changes",
								Path:            "pipeline.stage.dev.pre.task.check_for_changes",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalRuleIDs: []string{},
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								OnError:         statemachine.ContinueOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "find_changes",
										Path:       "pipeline.stage.dev.pre.task.check_for_changes.action.find_changes",
										ActionName: "command",
										PluginName: "exec",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:            "deploy",
								Path:            "pipeline.stage.dev.pipeline.deploy",
								EnvironmentName: ptr.String("account1"),
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								OnError:         statemachine.FailOnError,
								Dependencies:    []string{},
							},
						},
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								Interval:        ptr.Duration(2 * time.Minute),
								MaxAttempts:     5,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
							{
								Name:            "security_scan",
								Path:            "pipeline.stage.dev.task.security_scan",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{"execute_a_command"},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "exec_command",
										Path:       "pipeline.stage.dev.task.security_scan.action.exec_command",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
					{
						Name:   "dev.post",
						Path:   "pipeline.stage.dev.post",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "cleanup_resources",
								Path:            "pipeline.stage.dev.post.task.cleanup_resources",
								Status:          statemachine.CreatedNodeStatus,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								ApprovalStatus:  models.ApprovalNotRequired,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_cleanup",
										Path:       "pipeline.stage.dev.post.task.cleanup_resources.action.perform_cleanup",
										ActionName: "command",
										PluginName: "exec",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
			},
			expectNestedPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                nestedPipelineID,
					CreationTimestamp: &currentTime,
				},
				EnvironmentName:        ptr.String("account1"),
				Annotations:            []*models.PipelineAnnotation{},
				ProjectID:              projectID,
				PipelineTemplateID:     nestedPipelineTemplateID,
				ParentPipelineID:       &pipelineID,
				ParentPipelineNodePath: ptr.String("pipeline.stage.dev.pipeline.deploy"),
				CreatedBy:              testSubject,
				Status:                 statemachine.CreatedNodeStatus,
				Type:                   models.DeploymentPipelineType,
				When:                   models.ManualPipeline,
				ApprovalRuleIDs:        []string{},
				ApprovalStatus:         models.ApprovalNotRequired,
				ReleaseID:              &releaseID,
				Stages: []*models.PipelineStage{
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
			},
		},
		{
			name: "pipeline template not found",
			input: &CreatePipelineInput{
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				PipelineTemplateID: mainPipelineTemplateID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "pipeline template is not uploaded",
			input: &CreatePipelineInput{
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				PipelineTemplateID: mainPipelineTemplateID,
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplatePending,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "project not found",
			input: &CreatePipelineInput{
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				PipelineTemplateID: mainPipelineTemplateID,
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "invalid HCL file",
			input: &CreatePipelineInput{
				Type:               models.RunbookPipelineType,
				When:               models.AutoPipeline,
				PipelineTemplateID: mainPipelineTemplateID,
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			project: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:  projectID,
					PRN: projectPRN,
				},
				Name:  projectName,
				OrgID: organizationID,
			},
			rawHCLFile:      "invalid_task_interval.hcl",
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "deployment pipeline cannot create nested deployment pipelines",
			input: &CreatePipelineInput{
				Type:               models.DeploymentPipelineType,
				When:               models.AutoPipeline,
				PipelineTemplateID: mainPipelineTemplateID,
			},
			pipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			project: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:  projectID,
					PRN: projectPRN,
				},
				Name:  projectName,
				OrgID: organizationID,
			},
			rawHCLFile:      "valid_full.hcl",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockStore := NewMockStore(t)
			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockEnvironments := db.NewMockEnvironments(t)
			mockTransactions := db.NewMockTransactions(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockEnvironmentService := environment.NewMockService(t)
			mockPipelineDataStore := pipelinetemplate.NewMockDataStore(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			template, err := os.Open(filepath.Join(testPipelineHCLDir, test.rawHCLFile))
			require.NoErrorf(t, err, "failed to open test HCL file %s", test.rawHCLFile)
			defer template.Close()

			mockCaller.On("GetSubject").Return(testSubject).Maybe()
			mockCaller.On("Authorized").Maybe()

			// Add the caller to the context
			ctx = auth.WithCaller(ctx, mockCaller)

			mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, mock.Anything).Return(
				func(_ context.Context, pipelineTemplateID string) (*models.PipelineTemplate, error) {
					if pipelineTemplateID == mainPipelineTemplateID {
						// Main pipeline
						return test.pipelineTemplate, nil
					}

					if pipelineTemplateID == nestedPipelineTemplateID {
						// Nested pipeline
						return &models.PipelineTemplate{
							Metadata: models.ResourceMetadata{
								ID: nestedPipelineTemplateID,
							},
							ProjectID: projectID,
							Status:    models.PipelineTemplateUploaded,
						}, nil
					}

					// Unknown pipeline template
					return nil, nil
				},
			)

			if test.pipelineTemplate != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything).
					Return(test.authError).Maybe()

				mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(test.project, nil).Maybe()

				if test.project != nil {
					mockPipelineDataStore.On("GetData", mock.Anything, mock.Anything).Return(
						func(_ context.Context, pipelineTemplateID string) (io.ReadCloser, error) {
							if pipelineTemplateID == mainPipelineTemplateID {
								// Main pipeline
								return io.NopCloser(template), nil
							}

							if pipelineTemplateID == nestedPipelineTemplateID {
								// Use an simple HCL file for the nested pipeline template
								nestedPipelineTemplate, oErr := os.Open(filepath.Join(testPipelineHCLDir, "valid_task_action_only.hcl"))
								require.NoErrorf(t, oErr, "failed to open test nested pipeline HCL file %s", test.rawHCLFile)

								return io.NopCloser(nestedPipelineTemplate), nil
							}

							// Unknown pipeline template
							return nil, nil
						},
					)
				}
			}

			mockEnvironments.On("GetEnvironments", mock.Anything, mock.Anything).Return(&db.EnvironmentsResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 0,
				},
				Environments: []models.Environment{},
			}, nil).Maybe()

			mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
				Filter: &db.ApprovalRuleFilter{
					ApprovalRuleIDs: []string{},
					OrgID:           &organizationID,
				},
			}).Return(func(_ context.Context, input *db.GetApprovalRulesInput) (*db.ApprovalRulesResult, error) {
				require.NotNil(t, input.Filter)
				require.NotNil(t, input.Filter.OrgID)

				result := &db.ApprovalRulesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: int32(len(input.Filter.ApprovalRuleIDs)),
					},
				}

				return result, nil
			}).Maybe()

			if (test.expectParentPipeline != nil) || (test.expectErrorCode == errors.EForbidden) {
				// Calculate the number of times some methods should be called to ensure recursion is working correctly.
				expectCalls := 1 // Default to 1 for the main pipeline
				if test.expectParentPipeline != nil {
					for _, stage := range test.expectParentPipeline.Stages {
						if test.expectErrorCode != errors.EForbidden {
							expectCalls += len(stage.NestedPipelines)
						}
					}
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(func(inCtx context.Context) (context.Context, error) {
					return inCtx, nil
				}, nil).Times(expectCalls)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Times(expectCalls)

				mockPipelines.On("CreatePipeline", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
						// Set the time on the pipeline we're returning since that's handled by this DB function.
						pipeline.Metadata.CreationTimestamp = &currentTime

						// Set the pipeline ID and check if it was created properly.
						switch pipeline.PipelineTemplateID {
						case mainPipelineTemplateID:
							pipeline.Metadata.ID = pipelineID
							if test.expectErrorCode != errors.EForbidden {
								require.Equal(t, test.expectParentPipeline, pipeline)
							}
						case nestedPipelineTemplateID:
							pipeline.Metadata.ID = nestedPipelineID
							require.Equal(t, test.expectNestedPipeline, pipeline)
						default:
							return nil, errors.New("unknown pipeline template ID %s", pipeline.PipelineTemplateID)
						}

						return pipeline, nil
					},
				).Times(expectCalls)

				if test.expectNestedPipeline != nil && test.expectNestedPipeline.EnvironmentName != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.expectNestedPipeline.ParentPipelineID).Return(test.expectParentPipeline, nil).Once()
				}

				if test.expectErrorCode == errors.EForbidden {
					mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).
						Return(&models.Pipeline{
							Metadata: models.ResourceMetadata{
								ID: pipelineID,
							},
						}, nil).Once()
				}

				if test.expectParentPipeline != nil {
					for _, stage := range test.expectParentPipeline.Stages {
						for _, nestedPipeline := range stage.NestedPipelines {
							if nestedPipeline.EnvironmentName == nil {
								continue
							}
							if test.expectErrorCode == errors.EForbidden {
								continue
							}

							mockEnvironmentService.On("CreateEnvironment", mock.Anything, &environment.CreateEnvironmentInput{
								ProjectID: projectID,
								Name:      *nestedPipeline.EnvironmentName,
							}).Return(&models.Environment{
								Metadata: models.ResourceMetadata{
									ID: environmentID,
								},
								ProjectID: projectID,
								Name:      *nestedPipeline.EnvironmentName,
							}, nil)
						}
					}
				}

				mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
						// Check if the pipeline was updated properly
						switch pipeline.PipelineTemplateID {
						case mainPipelineTemplateID:
							for _, stage := range test.expectParentPipeline.Stages {
								for _, nestedPipeline := range stage.NestedPipelines {
									// Set the nested pipeline ID on the parent pipeline, so the comparison works as expected.
									nestedPipeline.LatestPipelineID = nestedPipelineID
								}
							}

							require.Equal(t, test.expectParentPipeline, pipeline)

						// Ignore for nested pipelines since the nested pipeline in our tests don't have any child pipelines.
						default:
							return nil, errors.New("unknown pipeline template ID %s", pipeline.PipelineTemplateID)
						}

						return pipeline, nil
					},
				).Maybe()

				if test.expectErrorCode != errors.EForbidden {
					mockPipelines.On("GetPipelines", mock.Anything, &db.GetPipelinesInput{
						Filter: &db.PipelineFilter{
							TimeRangeStart: ptr.Time(currentTime.Add(-limits.ResourceLimitTimePeriod)),
							ProjectID:      &projectID,
						},
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(0),
						},
					}).Return(&db.PipelinesResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
					}, nil).Times(expectCalls)

					mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitPipelinesPerProjectPerTimePeriod, int32(1)).Return(test.limitExceededError).Times(expectCalls)
				}

				if test.expectErrorCode == "" {
					mockStore.On("UploadPipelineVariables", mock.Anything, mock.Anything, mock.Anything).Return(
						func(_ context.Context, id string, body io.Reader) error {
							var variables []Variable
							require.NoError(t, json.NewDecoder(body).Decode(&variables))

							// Check if the pipeline variables were uploaded properly
							switch id {
							case pipelineID:
								// Just compare the length since variables are sorted by the service layer.
								require.Len(t, variables, len(test.input.Variables))
							case nestedPipelineID:
								// Nested pipeline in our tests don't have any variables.
								require.Empty(t, variables)
							default:
								return errors.New("unknown pipeline ID %s", id)
							}

							return nil
						},
					).Times(expectCalls)

					mockActivityEvents.On("CreateActivityEvent", mock.Anything, mock.Anything).Return(
						func(_ context.Context, input *activityevent.CreateActivityEventInput) (*models.ActivityEvent, error) {
							require.NotNil(t, input.ProjectID)
							require.Equal(t, projectID, *input.ProjectID)
							require.Equal(t, models.ActionCreate, input.Action)
							require.Equal(t, models.TargetPipeline, input.TargetType)

							// Check if the activity event was created properly
							switch *input.TargetID {
							case pipelineID, nestedPipelineID:
								// Do nothing
							default:
								return nil, errors.New("unknown target ID %s", input.TargetID)
							}

							return &models.ActivityEvent{}, nil
						},
					).Times(expectCalls)

					mockTransactions.On("CommitTx", mock.Anything).Return(nil).Times(expectCalls)

					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &RunPipelineEvent{}).Return(test.expectParentPipeline, nil).Once()
				}
			}

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, test.project, "account1").
				Return(func(_ context.Context, _ *models.Project, _ string) error {
					if test.expectErrorCode == errors.EForbidden {
						return errors.New(
							"deployment can't be executed in environment and so forth and so on",
							errors.WithErrorCode(errors.EForbidden),
						)
					}

					return nil
				}).Maybe()

			dbClient := &db.Client{
				Projects:          mockProjects,
				Pipelines:         mockPipelines,
				Transactions:      mockTransactions,
				Environments:      mockEnvironments,
				ApprovalRules:     mockApprovalRules,
				PipelineTemplates: mockPipelineTemplates,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:               dbClient,
				dataStore:              mockPipelineDataStore,
				logger:                 logger,
				store:                  mockStore,
				pipelineUpdater:        mockUpdater,
				activityService:        mockActivityEvents,
				limitChecker:           mockLimitChecker,
				environmentService:     mockEnvironmentService,
				environmentGatekeeper:  mockEnvironmentGatekeeper,
				maxNestedPipelineLevel: maxNestedPipelineLevel,
			}

			pipeline, err := service.CreatePipeline(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectParentPipeline, pipeline)
		})
	}
}

func TestCreatePipeline_NestedLevels(t *testing.T) {
	// This test is specific to the checking against the nested level limit.

	overrideMaxNestedLevels := 2
	testSubject := "test-subject"
	organizationID := "org-id-1"
	projectID := "proj-id-1"
	projectPRN := "prn:project:o1/p1"
	projectName := "p1"
	environmentName := "env1"
	currentTime := time.Now().UTC()

	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID:  projectID,
			PRN: projectPRN,
		},
		Name:  projectName,
		OrgID: organizationID,
	}

	type pipelineTemplateOther struct {
		model      *models.PipelineTemplate
		hcl        string
		pipelineID string
	}

	type testCase struct {
		input             *CreatePipelineInput
		pipelineTemplates map[string]*pipelineTemplateOther
		name              string
		expectErrorCode   errors.CodeType
		expectCalls       int
	}

	/*
		Test case template:

		type testCase struct {
			name              string
			input             *CreatePipelineInput
			pipelineTemplates map[string]*pipelineTemplateOther
			expectCalls       int
			expectErrorCode   errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name: "two nested levels, no loop",
			input: &CreatePipelineInput{
				PipelineTemplateID: "template-id-0",
				Type:               models.DeploymentPipelineType,
				EnvironmentName:    &environmentName,
			},
			pipelineTemplates: map[string]*pipelineTemplateOther{
				"template-id-0": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID:  "template-id-0",
							PRN: "prn:pipeline_template:o1/p1/template-id-1/0.0.1",
						},
						Name:      ptr.String("template-0"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "0-stage-1" {
						  pipeline "nested" {
							  pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-1/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-0",
				},
				"template-id-1": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID: "template-id-1",
						},
						Name:      ptr.String("template-1"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "1-stage-1" {
							pipeline "nested" {
								pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-2/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-1",
				},
				"template-id-2": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID: "template-id-2",
						},
						Name:      ptr.String("template-2"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "2-stage-1" {
							task "2-task-1" {
							}
						}`,
					pipelineID: "pipeline-id-2",
				},
			},
			expectCalls: 3,
		},
		{
			// Please note that the self-reference case is caught by an earlier check than the limit check.
			name: "self-reference",
			input: &CreatePipelineInput{
				PipelineTemplateID: "template-id-0",
				Type:               models.DeploymentPipelineType,
				EnvironmentName:    &environmentName,
			},
			pipelineTemplates: map[string]*pipelineTemplateOther{
				"template-id-0": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID:  "template-id-0",
							PRN: "prn:pipeline_template:o1/p1/template-id-0/0.0.1",
						},
						Name:      ptr.String("template-0"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "0-stage-1" {
						  pipeline "nested" {
							  pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-0/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-0",
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "two level loop",
			input: &CreatePipelineInput{
				PipelineTemplateID: "template-id-0",
				Type:               models.DeploymentPipelineType,
				EnvironmentName:    &environmentName,
			},
			pipelineTemplates: map[string]*pipelineTemplateOther{
				"template-id-0": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID:  "template-id-0",
							PRN: "prn:pipeline_template:o1/p1/template-id-1/0.0.1",
						},
						Name:      ptr.String("template-0"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "0-stage-1" {
						  pipeline "nested" {
							  pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-1/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-0",
				},
				"template-id-1": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID: "template-id-1",
						},
						Name:      ptr.String("template-1"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "1-stage-1" {
							pipeline "nested" {
								pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-0/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-1",
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "too many levels, even without a loop",
			input: &CreatePipelineInput{
				PipelineTemplateID: "template-id-0",
				Type:               models.DeploymentPipelineType,
				EnvironmentName:    &environmentName,
			},
			pipelineTemplates: map[string]*pipelineTemplateOther{
				"template-id-0": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID:  "template-id-0",
							PRN: "prn:pipeline_template:o1/p1/template-id-1/0.0.1",
						},
						Name:      ptr.String("template-0"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "0-stage-1" {
							pipeline "nested" {
								pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-1/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-0",
				},
				"template-id-1": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID: "template-id-1",
						},
						Name:      ptr.String("template-1"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "1-stage-1" {
							pipeline "nested" {
								pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-2/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-1",
				},
				"template-id-2": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID: "template-id-2",
						},
						Name:      ptr.String("template-2"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
						stage "2-stage-1" {
							pipeline "nested" {
								pipeline_type = "nested"
								when = "manual"
								template_id = "prn:pipeline_template:o1/p1/template-id-3/0.0.1"
							}
						}`,
					pipelineID: "pipeline-id-2",
				},
				"template-id-3": {
					model: &models.PipelineTemplate{
						Metadata: models.ResourceMetadata{
							ID: "template-id-3",
						},
						Name:      ptr.String("template-3"),
						Status:    models.PipelineTemplateUploaded,
						ProjectID: projectID,
					},
					hcl: `
							stage "3-stage-1" {
								task "3-task-1" {
								}
							}`,
					pipelineID: "pipeline-id-3",
				},
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockPipelineDataStore := pipelinetemplate.NewMockDataStore(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)
			mockTransactions := db.NewMockTransactions(t)
			mockEnvironments := db.NewMockEnvironments(t)
			mockEnvironmentService := environment.NewMockService(t)
			mockPipelines := db.NewMockPipelines(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockStore := NewMockStore(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockUpdater := NewMockUpdater(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			// Add the caller to the context
			ctx = auth.WithCaller(ctx, mockCaller)

			mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, mock.Anything).Return(
				func(_ context.Context, pipelineTemplateID string) (*models.PipelineTemplate, error) {
					if found, ok := test.pipelineTemplates[pipelineTemplateID]; ok {
						return found.model, nil
					}

					return nil, fmt.Errorf("mocked GetPipelineTemplateByID: template ID not found in test.modelsAndReaders: %s",
						pipelineTemplateID)
				},
			)

			mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything).
				Return(nil).Maybe()

			mockCaller.On("Authorized").Maybe()

			mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(project, nil).Maybe()

			mockPipelineDataStore.On("GetData", mock.Anything, mock.Anything).Return(
				func(_ context.Context, pipelineTemplateID string) (io.ReadCloser, error) {
					if found, ok := test.pipelineTemplates[pipelineTemplateID]; ok {
						return io.NopCloser(strings.NewReader(found.hcl)), nil
					}

					return nil, fmt.Errorf("mocked GetData: template ID not found in test.modelsAndReaders: %s", pipelineTemplateID)
				},
			)

			mockPipelineTemplates.On("GetPipelineTemplateByPRN", mock.Anything, mock.Anything).
				Return(func(_ context.Context, pipelineTemplatePRN string) (*models.PipelineTemplate, error) {
					path, err := models.PipelineTemplateResource.ResourcePathFromPRN(pipelineTemplatePRN)
					if err != nil {
						return nil, fmt.Errorf("mocked GetPipelineTemplateByPRN failed to parse PRN: %s", pipelineTemplatePRN)
					}

					pipelineTemplateID := strings.Split(path, "/")[2]
					if found, ok := test.pipelineTemplates[pipelineTemplateID]; ok {
						return found.model, nil
					}

					return nil, fmt.Errorf("mocked GetPipelineTemplateByPRN: template ID not found in test.modelsAndReaders: %s",
						pipelineTemplateID)
				})

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, projectID, environmentName).
				Return(func(_ context.Context, _ string, _ string) error {
					if test.expectErrorCode == errors.EForbidden {
						return errors.New(
							"deployment can't be executed in environment and so forth and so on",
							errors.WithErrorCode(errors.EForbidden),
						)
					}

					return nil
				})

			mockTransactions.On("BeginTx", mock.Anything).Return(func(inCtx context.Context) (context.Context, error) {
				return inCtx, nil
			}, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			mockEnvironments.On("GetEnvironments", mock.Anything, mock.Anything).Return(&db.EnvironmentsResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 0,
				},
				Environments: []models.Environment{},
			}, nil).Maybe()

			mockEnvironmentService.On("CreateEnvironment", mock.Anything, &environment.CreateEnvironmentInput{
				ProjectID: projectID,
				Name:      environmentName,
			}).Return(&models.Environment{
				Metadata: models.ResourceMetadata{
					ID: "environment-id",
				},
				ProjectID: projectID,
				Name:      environmentName,
			}, nil).Maybe()

			mockPipelines.On("CreatePipeline", mock.Anything, mock.Anything).Return(
				func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
					// Set the time on the pipeline we're returning since that's handled by this DB function.
					pipeline.Metadata.CreationTimestamp = &currentTime

					// Set the pipeline ID.
					found, ok := test.pipelineTemplates[pipeline.PipelineTemplateID]
					if !ok {
						return nil, fmt.Errorf("in mocked CreatePipeline, failed to find pipeline ID %s", pipeline.PipelineTemplateID)
					}

					pipeline.Metadata.ID = found.pipelineID
					return pipeline, nil
				},
			).Maybe()

			if test.expectErrorCode == "" {

				mockPipelines.On("GetPipelines", mock.Anything, &db.GetPipelinesInput{
					Filter: &db.PipelineFilter{
						TimeRangeStart: ptr.Time(currentTime.Add(-limits.ResourceLimitTimePeriod)),
						ProjectID:      &projectID,
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.PipelinesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil).Times(test.expectCalls)

				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitPipelinesPerProjectPerTimePeriod, int32(1)).
					Return(nil).Times(test.expectCalls)

				mockStore.On("UploadPipelineVariables", mock.Anything, mock.Anything, mock.Anything).
					Return(nil).Times(test.expectCalls)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, mock.Anything).
					Return(&models.ActivityEvent{}, nil).Times(test.expectCalls)

				mockUpdater.On("ProcessEvent", mock.Anything, mock.Anything, &RunPipelineEvent{}).
					Return(
						func(_ context.Context, pipelineID string, _ any) (*models.Pipeline, error) {
							return &models.Pipeline{
								Metadata: models.ResourceMetadata{
									ID: pipelineID,
								},
							}, nil
						},
					).Once()

				mockPipelines.On("UpdatePipeline", mock.Anything, mock.Anything).Return(
					func(_ context.Context, pipeline *models.Pipeline) (*models.Pipeline, error) {
						return pipeline, nil
					},
				).Maybe()
			}

			dbClient := &db.Client{
				Projects:          mockProjects,
				PipelineTemplates: mockPipelineTemplates,
				Transactions:      mockTransactions,
				Environments:      mockEnvironments,
				Pipelines:         mockPipelines,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:               dbClient,
				dataStore:              mockPipelineDataStore,
				logger:                 logger,
				environmentGatekeeper:  mockEnvironmentGatekeeper,
				environmentService:     mockEnvironmentService,
				limitChecker:           mockLimitChecker,
				store:                  mockStore,
				activityService:        mockActivityEvents,
				pipelineUpdater:        mockUpdater,
				maxNestedPipelineLevel: overrideMaxNestedLevels,
			}

			_, err := service.CreatePipeline(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestResolvePipelineVariables(t *testing.T) {
	project := &models.Project{
		Metadata: models.ResourceMetadata{ID: "project-1", PRN: "prn:project:org-1/project-1"},
		Name:     "project-1",
	}

	projectVariableSet := &models.ProjectVariableSet{
		Metadata:  models.ResourceMetadata{ID: "project-variable-set-1", PRN: "prn:project_variable_set:org-1/project-1/1"},
		ProjectID: project.Metadata.ID,
		Latest:    true,
		Revision:  "1",
	}

	type testCase struct {
		name             string
		expectErrorCode  errors.CodeType
		input            *resolvePipelineVariablesInput
		projectVariables []*models.ProjectVariable
		expectVariables  []Variable
	}

	testCases := []testCase{
		{
			name: "only input variables provided",
			input: &resolvePipelineVariablesInput{
				project:      project,
				pipelineType: models.RunbookPipelineType,
				variables: []Variable{
					{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
					{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
				},
			},
			expectVariables: []Variable{
				{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
				{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
			},
		},
		{
			name: "input variables and variable set revision provided",
			input: &resolvePipelineVariablesInput{
				project:         project,
				pipelineType:    models.DeploymentPipelineType,
				environmentName: ptr.String("environment-1"),
				variables: []Variable{
					{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
					{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
				},
				variableSetRevision: ptr.String("1"),
			},
			projectVariables: []*models.ProjectVariable{
				{Key: "key-1", Value: "test", EnvironmentScope: models.ProjectVariableEnvironmentScopeAll, PipelineType: models.DeploymentPipelineType},
				{Key: "key-2", Value: "test", EnvironmentScope: models.ProjectVariableEnvironmentScopeAll, PipelineType: models.DeploymentPipelineType},
				{Key: "key-3", Value: "test", EnvironmentScope: "environment-1", PipelineType: models.DeploymentPipelineType},
			},
			expectVariables: []Variable{
				{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
				{Key: "key-2", Value: "test", Category: models.HCLVariable},
				{Key: "key-3", Value: "test", Category: models.HCLVariable},
				{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
			},
		},
		{
			name: "input variables and latest variable set revision provided",
			input: &resolvePipelineVariablesInput{
				project:         project,
				pipelineType:    models.DeploymentPipelineType,
				environmentName: ptr.String("environment-1"),
				variables: []Variable{
					{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
					{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
				},
				variableSetRevision: ptr.String("latest"),
			},
			projectVariables: []*models.ProjectVariable{
				{Key: "key-2", Value: "test", EnvironmentScope: models.ProjectVariableEnvironmentScopeAll},
			},
			expectVariables: []Variable{
				{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
				{Key: "key-2", Value: "test", Category: models.HCLVariable},
				{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
			},
		},
		{
			name: "latest variable set revision provided for nested pipeline",
			input: &resolvePipelineVariablesInput{
				project:             project,
				pipelineType:        models.NestedPipelineType,
				environmentName:     ptr.String("environment-1"),
				variables:           []Variable{},
				variableSetRevision: ptr.String("latest"),
			},
			projectVariables: []*models.ProjectVariable{
				{Key: "key-1", Value: "test", EnvironmentScope: models.ProjectVariableEnvironmentScopeAll},
			},
			expectVariables: []Variable{
				{Key: "key-1", Value: "test", Category: models.HCLVariable},
			},
		},
		{
			name: "input variables and empty project variable set",
			input: &resolvePipelineVariablesInput{
				project:         project,
				pipelineType:    models.DeploymentPipelineType,
				environmentName: ptr.String("environment-1"),
				variables: []Variable{
					{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
					{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
				},
				variableSetRevision: ptr.String("latest"),
			},
			projectVariables: []*models.ProjectVariable{},
			expectVariables: []Variable{
				{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
				{Key: "key-1", Value: "value-1", Category: models.EnvironmentVariable},
			},
		},
		{
			name: "should fail due to duplicate key",
			input: &resolvePipelineVariablesInput{
				project:      project,
				pipelineType: models.RunbookPipelineType,
				variables: []Variable{
					{Key: "key-1", Value: "value-1", Category: models.HCLVariable},
					{Key: "key-1", Value: "value-2", Category: models.HCLVariable},
				},
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		test := test
		t.Run(test.name, func(t *testing.T) {
			ctx := context.Background()

			mockProjectVariables := db.NewMockProjectVariables(t)
			mockProjectVariableSets := db.NewMockProjectVariableSets(t)

			if test.input.variableSetRevision != nil && *test.input.variableSetRevision != "latest" {
				mockProjectVariableSets.On("GetProjectVariableSetByPRN", mock.Anything, projectVariableSet.Metadata.PRN).
					Return(projectVariableSet, nil)
			}

			if test.input.variableSetRevision != nil && *test.input.variableSetRevision == "latest" {
				mockProjectVariableSets.On("GetProjectVariableSets", mock.Anything, &db.GetProjectVariableSetsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(1),
					},
					Filter: &db.ProjectVariableSetFilter{
						ProjectID: &test.input.project.Metadata.ID,
						Latest:    ptr.Bool(true),
					},
				}).
					Return(&db.ProjectVariableSetsResult{
						ProjectVariableSets: []*models.ProjectVariableSet{projectVariableSet},
					}, nil)
			}

			if test.projectVariables != nil {
				pipelineType := models.RunbookPipelineType
				var environmentScopes []string
				if test.input.environmentName != nil {
					environmentScopes = []string{*test.input.environmentName, models.ProjectVariableEnvironmentScopeAll}
					pipelineType = models.DeploymentPipelineType
				}

				mockProjectVariables.On("GetProjectVariables", mock.Anything, &db.GetProjectVariablesInput{
					Filter: &db.ProjectVariableFilter{
						ProjectVariableSetID: &projectVariableSet.Metadata.ID,
						PipelineType:         &pipelineType,
						EnvironmentScopes:    environmentScopes,
					},
				}).
					Return(&db.ProjectVariablesResult{
						ProjectVariables: test.projectVariables,
					}, nil)
			}

			dbClient := &db.Client{
				ProjectVariables:    mockProjectVariables,
				ProjectVariableSets: mockProjectVariableSets,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			variables, err := service.resolvePipelineVariables(ctx, test.input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.ElementsMatch(t, test.expectVariables, variables)
		})
	}
}

func TestCancelPipeline(t *testing.T) {
	pipelineID := "pipeline-1"
	pipelineVersion := 1
	projectID := "project-1"
	environmentName := "environment-1"

	type testCase struct {
		authError                  error
		checkEnvironmentRulesError error
		pipeline                   *models.Pipeline
		name                       string
		expectErrorCode            errors.CodeType
		isForceCancel              bool
	}

	testCases := []testCase{
		{
			name: "gracefully cancel a pipeline",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Status:    statemachine.RunningNodeStatus,
			},
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to cancel the pipeline",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "gatekeeper environment protection rule check fails",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				Status:          statemachine.RunningNodeStatus,
				EnvironmentName: &environmentName,
			},
			checkEnvironmentRulesError: errors.New("environment rule check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockUpdater := NewMockUpdater(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.CancelPipeline, mock.Anything, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockCaller.On("GetSubject").Return("test-subject")

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &CancelPipelineEvent{
					Force:   test.isForceCancel,
					Version: &pipelineVersion,
					Caller:  mockCaller,
				}).Return(test.pipeline, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionCancel,
					TargetType: models.TargetPipeline,
					TargetID:   &pipelineID,
				}).Return(&models.ActivityEvent{}, nil)
			}

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, projectID, environmentName).
				Return(test.checkEnvironmentRulesError).Maybe()

			dbClient := &db.Client{
				Transactions: mockTransactions,
				Pipelines:    mockPipelines,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				logger:                logger,
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				activityService:       mockActivityEvents,
				environmentGatekeeper: mockEnvironmentGatekeeper,
			}

			pipeline, err := service.CancelPipeline(auth.WithCaller(ctx, mockCaller), &CancelPipelineInput{
				ID:      pipelineID,
				Force:   test.isForceCancel,
				Version: &pipelineVersion,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pipeline, pipeline)
		})
	}
}

func TestCreatePipelineJWT(t *testing.T) {
	pipelineID := "pipeline-1"
	projectID := "project-1"

	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID:  projectID,
			PRN: models.ProjectResource.BuildPRN("test-org", "test-project"),
		},
		Name: "test-project",
	}

	type testCase struct {
		injectPermissionError error
		findPipeline          *models.Pipeline
		input                 *CreatePipelineJWTInput
		name                  string
		expectErrorCode       errors.CodeType
		withCaller            bool
	}

	now := time.Now().UTC()

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "pipeline not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
			input: &CreatePipelineJWTInput{
				PipelineID: pipelineID,
				Audience:   "test-audience",
				Expiration: now,
			},
		},
		{
			name:       "permission check failed",
			withCaller: true,
			input: &CreatePipelineJWTInput{
				PipelineID: pipelineID,
				Audience:   "test-audience",
				Expiration: now,
			},
			findPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:       "invalid token expiration",
			withCaller: true,
			input: &CreatePipelineJWTInput{
				PipelineID: pipelineID,
				Audience:   "test-audience",
				Expiration: now.Add(models.MaxAllowedJobDuration + time.Minute),
			},
			findPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "successfully generate token",
			withCaller: true,
			input: &CreatePipelineJWTInput{
				PipelineID: pipelineID,
				Audience:   "test-audience",
				Expiration: now,
			},
			findPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePipelineState, mock.Anything, mock.Anything).
					Return(test.injectPermissionError).Maybe()
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).
				Return(test.findPipeline, nil).Maybe()

			mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(project, nil).Maybe()

			mockJWSProvider := jws.MockProvider{}
			mockJWSProvider.Test(t)

			mockJWSProvider.On("Sign", mock.Anything, mock.Anything).Return(func(_ context.Context, token []byte) ([]byte, error) {
				return token, nil
			}, nil)

			dbClient := &db.Client{
				Projects:  mockProjects,
				Pipelines: mockPipelines,
			}

			identityProvider := auth.NewIdentityProvider(&mockJWSProvider, "https://phobos.domain")

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
				idp:      identityProvider,
			}

			token, err := service.CreatePipelineJWT(ctx, test.input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, token)

			// Parse and validate jwt
			decodedToken, err := jwt.Parse(token, jwt.WithVerify(false), jwt.WithValidate(false))
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, []string{test.input.Audience}, decodedToken.Audience())
			assert.Equal(t, project.Metadata.PRN, decodedToken.Subject())
		})
	}
}

func TestRetryPipelineTask(t *testing.T) {
	taskPath := "task-1"
	projectID := "project-1"
	environmentName := "environment-1"

	samplePipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: "pipeline-1",
		},
		ProjectID: projectID,
	}

	type testCase struct {
		authError                  error
		checkEnvironmentRulesError error
		pipeline                   *models.Pipeline
		name                       string
		expectErrorCode            errors.CodeType
	}

	testCases := []testCase{
		{
			name:     "successfully retry a pipeline task",
			pipeline: samplePipeline,
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "subject is not authorized to retry the pipeline task",
			pipeline:        samplePipeline,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "gatekeeper environment rule check fails",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-1",
				},
				ProjectID:       "project-1",
				EnvironmentName: &environmentName,
			},
			checkEnvironmentRulesError: errors.New("environment rule check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
	}

	for _, test := range testCases {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		mockUpdater := NewMockUpdater(t)
		mockCaller := auth.NewMockCaller(t)
		mockPipelines := db.NewMockPipelines(t)
		mockTransactions := db.NewMockTransactions(t)
		mockActivityEvents := activityevent.NewMockService(t)
		mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

		mockPipelines.On("GetPipelineByID", mock.Anything, samplePipeline.Metadata.ID).Return(test.pipeline, nil)

		if test.pipeline != nil {
			mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError)
		}

		if test.expectErrorCode == "" {
			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
			mockTransactions.On("CommitTx", mock.Anything).Return(nil)

			mockUpdater.On("ProcessEvent", mock.Anything, samplePipeline.Metadata.ID, &RetryTaskEvent{TaskPath: taskPath}).Return(test.pipeline, nil)

			mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
				ProjectID:  &projectID,
				Action:     models.ActionRetry,
				TargetType: models.TargetPipeline,
				TargetID:   &test.pipeline.Metadata.ID,
				Payload: &models.ActivityEventRetryPipelineNodePayload{
					NodePath: taskPath,
					NodeType: statemachine.TaskNodeType.String(),
				},
			}).Return(&models.ActivityEvent{}, nil)
		}

		mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, projectID, "test-environment").
			Return(test.checkEnvironmentRulesError).Maybe()

		dbClient := &db.Client{
			Pipelines:    mockPipelines,
			Transactions: mockTransactions,
		}

		service := &service{
			dbClient:              dbClient,
			pipelineUpdater:       mockUpdater,
			activityService:       mockActivityEvents,
			environmentGatekeeper: mockEnvironmentGatekeeper,
		}

		pipeline, err := service.RetryPipelineTask(auth.WithCaller(ctx, mockCaller), samplePipeline.Metadata.ID, taskPath)

		if test.expectErrorCode != "" {
			assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
			return
		}

		require.NoError(t, err)

		assert.Equal(t, test.pipeline, pipeline)
	}
}

func TestRunPipelineTask(t *testing.T) {
	taskPath := "task-1"
	pipelineID := "pipeline-1"
	projectID := "project-1"
	environmentName := "environment-1"

	type testCase struct {
		authError                  error
		pipeline                   *models.Pipeline
		name                       string
		checkEnvironmentRulesError error
		expectErrorCode            errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully run a pipeline task",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path:   taskPath,
								When:   models.ManualPipelineTask,
								Status: statemachine.ReadyNodeStatus,
							},
						},
					},
				},
			},
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to run the pipeline task",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "gatekeeper environment protection rule check fails",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				EnvironmentName: &environmentName,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path: taskPath,
							},
						},
					},
				},
			},
			checkEnvironmentRulesError: errors.New("environment rule check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
		{
			name: "pipeline task not found",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "pipeline task cannot be run manually",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path:   taskPath,
								When:   models.AutoPipelineTask,
								Status: statemachine.ReadyNodeStatus,
							},
						},
					},
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "pipeline task is not ready to be run",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path:   taskPath,
								When:   models.ManualPipelineTask,
								Status: statemachine.RunningNodeStatus,
							},
						},
					},
				},
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &RunTaskEvent{TaskPath: taskPath}).Return(test.pipeline, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionStart,
					TargetType: models.TargetPipeline,
					TargetID:   &pipelineID,
					Payload: &models.ActivityEventStartPipelineNodePayload{
						NodePath: taskPath,
						NodeType: statemachine.TaskNodeType.String(),
					},
				}).Return(&models.ActivityEvent{}, nil)
			}

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, projectID, environmentName).
				Return(test.checkEnvironmentRulesError).Maybe()

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				activityService:       mockActivityEvents,
				environmentGatekeeper: mockEnvironmentGatekeeper,
			}

			pipeline, err := service.RunPipelineTask(auth.WithCaller(ctx, mockCaller), pipelineID, taskPath)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pipeline, pipeline)
		})
	}
}

func TestRunPipeline(t *testing.T) {
	nestedPipelinePath := "pipeline-path"
	nestedPipelineID := "pipeline-1"
	parentPipelineID := "pipeline-2"
	projectID := "project-1"
	releaseID := "release-1"
	environmentName := "environment-1"

	type testCase struct {
		authError                  error
		checkEnvironmentRulesError error
		nestedPipeline             *models.Pipeline
		parentPipeline             *models.Pipeline
		name                       string
		expectErrorCode            errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully run a nested pipeline",
			nestedPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ProjectID:              projectID,
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &nestedPipelinePath,
				ReleaseID:              &releaseID,
				When:                   models.ManualPipeline,
				Type:                   models.DeploymentPipelineType,
				Status:                 statemachine.ReadyNodeStatus,
			},
			parentPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: parentPipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						NestedPipelines: []*models.NestedPipeline{
							{
								LatestPipelineID: nestedPipelineID,
								Path:             nestedPipelinePath,
							},
						},
					},
				},
			},
		},
		{
			name:            "nested pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to run the nested pipeline",
			nestedPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ProjectID:              projectID,
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &nestedPipelinePath,
				When:                   models.ManualPipeline,
				Status:                 statemachine.ReadyNodeStatus,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "nested pipeline cannot be run manually",
			nestedPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ProjectID:              projectID,
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &nestedPipelinePath,
				When:                   models.AutoPipeline,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "nested pipeline is not ready to be run",
			nestedPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ProjectID:              projectID,
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &nestedPipelinePath,
				Status:                 statemachine.CreatedNodeStatus,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "gatekeeper environment protection rule check fails",
			nestedPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ProjectID:              projectID,
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &nestedPipelinePath,
				When:                   models.ManualPipeline,
				Status:                 statemachine.ReadyNodeStatus,
				EnvironmentName:        &environmentName,
			},
			checkEnvironmentRulesError: errors.New("environment rule check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockUpdater := NewMockUpdater(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, nestedPipelineID).Return(test.nestedPipeline, nil)

			if test.nestedPipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockUpdater.On("ProcessEvent", mock.Anything, parentPipelineID, &RunNestedPipelineEvent{NestedPipelinePath: nestedPipelinePath}).Return(test.parentPipeline, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &projectID,
						Action:     models.ActionStart,
						TargetType: models.TargetPipeline,
						TargetID:   &parentPipelineID,
						Payload: &models.ActivityEventStartPipelineNodePayload{
							NodePath: nestedPipelinePath,
							NodeType: statemachine.PipelineNodeType.String(),
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, projectID, environmentName).
				Return(test.checkEnvironmentRulesError).Maybe()

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				activityService:       mockActivityEvents,
				environmentGatekeeper: mockEnvironmentGatekeeper,
			}

			pipeline, err := service.RunPipeline(auth.WithCaller(ctx, mockCaller), nestedPipelineID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.parentPipeline, pipeline)
		})
	}
}

func TestApprovePipelineNode(t *testing.T) {
	userID := "user-1"
	serviceAccountID := "service-account-1"
	pipelineID := "pipeline-1"
	projectID := "project-1"
	taskPath := "pipeline.stage.s1.task.t1"
	parentPipelineID := "parent-pipeline-1"
	parentPipelinePath := "pipeline.stage.s1.pipeline.p1"

	type testCase struct {
		name            string
		callerType      string
		input           *NodeApprovalInput
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "user approves task",
			callerType: "user",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   &taskPath,
			},
		},
		{
			name:       "service account approves task",
			callerType: "serviceAccount",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   &taskPath,
			},
		},
		{
			name:       "user approves nested pipeline",
			callerType: "user",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
			},
		},
		{
			name:       "service account approves nested pipeline",
			callerType: "serviceAccount",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
			},
		},
		{
			name: "invalid caller type",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "invalid node type",
			callerType: "user",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.StageNodeType,
				NodePath:   ptr.String("pipeline.stage.s1"),
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUpdater := NewMockUpdater(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			// By default, use the mock caller so we can test if it's authorized to approve.
			var testCaller auth.Caller = auth.NewMockCaller(t)
			var callerUserID, callerServiceAccountID *string
			switch test.callerType {
			case "user":
				testCaller = &auth.UserCaller{
					User: &models.User{
						Metadata: models.ResourceMetadata{
							ID: userID,
						},
					},
				}
				callerUserID = &userID
			case "serviceAccount":
				testCaller = &auth.ServiceAccountCaller{
					ServiceAccountID: serviceAccountID,
				}
				callerServiceAccountID = &serviceAccountID
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(&models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &parentPipelinePath,
				ProjectID:              projectID,
			}, nil).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			if test.expectErrorCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				activityEventInput := &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionApprove,
					TargetType: models.TargetPipeline,
				}

				switch test.input.NodeType {
				case statemachine.TaskNodeType:
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &TaskApprovalEvent{
						UserID:           callerUserID,
						ServiceAccountID: callerServiceAccountID,
						TaskPath:         taskPath,
					}).Return(nil, nil)

					activityEventInput.TargetID = &pipelineID
					activityEventInput.Payload = &models.ActivityEventApprovePipelineNodePayload{
						NodeType: test.input.NodeType.String(),
						NodePath: taskPath,
					}
				case statemachine.PipelineNodeType:
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &ApprovalEvent{
						UserID:           callerUserID,
						ServiceAccountID: callerServiceAccountID,
					}).Return(nil, nil)

					activityEventInput.TargetID = &parentPipelineID
					activityEventInput.Payload = &models.ActivityEventApprovePipelineNodePayload{
						NodeType: test.input.NodeType.String(),
						NodePath: parentPipelinePath,
					}
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, activityEventInput).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:        dbClient,
				pipelineUpdater: mockUpdater,
				activityService: mockActivityEvents,
			}

			actualError := service.ApprovePipelineNode(auth.WithCaller(ctx, testCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(actualError))
				return
			}

			require.NoError(t, actualError)
		})
	}
}

func TestRevokePipelineNodeApproval(t *testing.T) {
	userID := "user-1"
	serviceAccountID := "service-account-1"
	pipelineID := "pipeline-1"
	projectID := "project-1"
	taskPath := "pipeline.stage.s1.task.t1"
	parentPipelineID := "parent-pipeline-1"
	parentPipelinePath := "pipeline.stage.s1.pipeline.p1"

	type testCase struct {
		name            string
		callerType      string
		input           *NodeApprovalInput
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "user revokes task approval",
			callerType: "user",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   &taskPath,
			},
		},
		{
			name:       "service account revokes task approval",
			callerType: "serviceAccount",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   &taskPath,
			},
		},
		{
			name:       "user revokes nested pipeline approval",
			callerType: "user",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
			},
		},
		{
			name:       "service account revokes nested pipeline approval",
			callerType: "serviceAccount",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
			},
		},
		{
			name: "invalid caller type",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "invalid node type",
			callerType: "user",
			input: &NodeApprovalInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.StageNodeType,
				NodePath:   ptr.String("pipeline.stage.s1"),
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUpdater := NewMockUpdater(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			// By default, use the mock caller so we can test if it's authorized to approve.
			var testCaller auth.Caller = auth.NewMockCaller(t)
			var callerUserID, callerServiceAccountID *string
			switch test.callerType {
			case "user":
				testCaller = &auth.UserCaller{
					User: &models.User{
						Metadata: models.ResourceMetadata{
							ID: userID,
						},
					},
				}
				callerUserID = &userID
			case "serviceAccount":
				testCaller = &auth.ServiceAccountCaller{
					ServiceAccountID: serviceAccountID,
				}
				callerServiceAccountID = &serviceAccountID
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(&models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &parentPipelinePath,
				ProjectID:              projectID,
			}, nil).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			if test.expectErrorCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				activityEventInput := &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionRevokeApproval,
					TargetType: models.TargetPipeline,
				}

				switch test.input.NodeType {
				case statemachine.TaskNodeType:
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &TaskRevokeApprovalEvent{
						UserID:           callerUserID,
						ServiceAccountID: callerServiceAccountID,
						TaskPath:         taskPath,
					}).Return(nil, nil)

					activityEventInput.TargetID = &pipelineID
					activityEventInput.Payload = &models.ActivityEventRevokeApprovalPipelineNodePayload{
						NodeType: test.input.NodeType.String(),
						NodePath: taskPath,
					}
				case statemachine.PipelineNodeType:
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &RevokeApprovalEvent{
						UserID:           callerUserID,
						ServiceAccountID: callerServiceAccountID,
					}).Return(nil, nil)

					activityEventInput.TargetID = &parentPipelineID
					activityEventInput.Payload = &models.ActivityEventRevokeApprovalPipelineNodePayload{
						NodeType: test.input.NodeType.String(),
						NodePath: parentPipelinePath,
					}
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, activityEventInput).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:        dbClient,
				pipelineUpdater: mockUpdater,
				activityService: mockActivityEvents,
			}

			actualError := service.RevokePipelineNodeApproval(auth.WithCaller(ctx, testCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(actualError))
				return
			}

			require.NoError(t, actualError)
		})
	}
}

func TestGetPipelineApprovals(t *testing.T) {
	taskPath := "pipeline.stage.s1.task.t1"

	pipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: "pipeline-id",
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		name            string
		pipeline        *models.Pipeline
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:     "successfully get approvals",
			pipeline: pipeline,
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "subject is not authorized to view pipeline",
			pipeline:        pipeline,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewPipeline, mock.Anything, mock.Anything).Return(test.authError)
			}

			approvalType := models.ApprovalTypeTask

			if test.expectErrorCode == "" {
				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					Filter: &db.PipelineApprovalFilter{
						PipelineID: &pipeline.Metadata.ID,
						TaskPath:   &taskPath,
						Type:       &approvalType,
					},
				}).Return(&db.PipelineApprovalsResult{PipelineApprovals: []*models.PipelineApproval{}}, nil)
			}

			dbClient := &db.Client{
				Pipelines:         mockPipelines,
				PipelineApprovals: mockPipelineApprovals,
			}

			service := &service{
				dbClient: dbClient,
			}

			approvals, err := service.GetPipelineApprovals(auth.WithCaller(ctx, mockCaller), &GetPipelineApprovalsInput{
				PipelineID:   pipeline.Metadata.ID,
				NodePath:     &taskPath,
				ApprovalType: approvalType,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, approvals)
		})
	}
}

func TestUpdateNestedPipeline(t *testing.T) {
	projectID := "project-1"
	releaseID := "release-1"
	nestedPipelineID := "pipeline-1"
	parentPipelineID := "parent-pipeline"
	parentNestedPipelineNodePath := "pipeline.stage.s1.pipeline.p1"
	currentTime := time.Now().UTC()

	type testCase struct {
		name            string
		pipelineType    models.PipelineType
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:         "successfully update a nested pipeline",
			pipelineType: models.ReleaseLifecyclePipelineType,
		},
		{
			name:            "subject is not authorized to update a nested pipeline",
			pipelineType:    models.ReleaseLifecyclePipelineType,
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "only release lifecycle pipelines can be updated",
			pipelineType:    models.RunbookPipelineType,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockStore := NewMockStore(t)
			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockLimits := limits.NewMockLimitChecker(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockEnvironments := db.NewMockEnvironments(t)
			mockTemplateStore := pipelinetemplate.NewMockDataStore(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)
			mockApprovalRules := db.NewMockApprovalRules(t)

			parentPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "parent-pipeline",
				},
				ProjectID: projectID,
				Type:      test.pipelineType,
				Stages: []*models.PipelineStage{
					{
						NestedPipelines: []*models.NestedPipeline{
							{
								LatestPipelineID: nestedPipelineID,
								Path:             parentNestedPipelineNodePath,
								Status:           statemachine.FailedNodeStatus,
							},
						},
					},
				},
			}

			originalPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				Type:                   models.DeploymentPipelineType,
				ProjectID:              parentPipeline.ProjectID,
				ParentPipelineID:       &parentPipeline.Metadata.ID,
				ParentPipelineNodePath: &parentNestedPipelineNodePath,
				ReleaseID:              &releaseID,
				EnvironmentName:        ptr.String("test-environment"),
			}

			newPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                "new-pipeline",
					CreationTimestamp: &currentTime,
				},
				Type:                   originalPipeline.Type,
				ProjectID:              originalPipeline.ProjectID,
				ParentPipelineID:       originalPipeline.ParentPipelineID,
				ParentPipelineNodePath: originalPipeline.ParentPipelineNodePath,
				ReleaseID:              originalPipeline.ReleaseID,
				EnvironmentName:        ptr.String("test-environment"),
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, parentPipeline.Metadata.ID).Return(parentPipeline, nil)
			mockPipelines.On("GetPipelineByID", mock.Anything, nestedPipelineID).Return(originalPipeline, nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).
				Return(test.authError).Maybe()
			mockCaller.On("Authorized").Maybe()

			if test.expectErrorCode == "" {
				buf, err := json.Marshal([]Variable{})
				require.Nil(t, err)

				mockStore.On("GetPipelineVariables", mock.Anything, nestedPipelineID).Return(io.NopCloser(bytes.NewReader(buf)), nil)

				mockTransactions.On("BeginTx", mock.Anything).Return(func(inCtx context.Context) (context.Context, error) {
					return inCtx, nil
				}, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

				if test.expectErrorCode == "" {
					mockTransactions.On("CommitTx", mock.Anything).Return(nil)
				}

				// Begin CreatePipeline mocks.

				mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, mock.Anything).Return(&models.PipelineTemplate{
					Metadata: models.ResourceMetadata{
						ID: "pipeline-template-1",
					},
					Status: models.PipelineTemplateUploaded,
				}, nil)

				mockCaller.On("GetSubject").Return("test-subject").Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).
					Return(nil).Maybe()

				testProject := &models.Project{
					Metadata: models.ResourceMetadata{
						ID: "project-1",
					},
					OrgID: "test-org",
				}
				mockProjects.On("GetProjectByID", mock.Anything, mock.Anything).Return(testProject, nil)

				mockEnvironmentRules.On("GetEnvironmentRules", mock.Anything, mock.Anything).
					Return(&db.EnvironmentRulesResult{}, nil).Maybe()

				template, err := os.Open(filepath.Join(testPipelineHCLDir, "valid_task_action_only.hcl"))
				require.NoError(t, err)
				defer template.Close()

				mockTemplateStore.On("GetData", mock.Anything, mock.Anything).Return(io.NopCloser(template), nil)

				mockPipelines.On("CreatePipeline", mock.Anything, mock.Anything).Return(newPipeline, nil)

				mockApprovalRules.On("GetApprovalRules", mock.Anything, mock.Anything).Return(&db.ApprovalRulesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 2,
					},
				}, nil).Maybe()

				mockPipelines.On("GetPipelines", mock.Anything, mock.Anything).Return(&db.PipelinesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil)

				mockEnvironments.On("GetEnvironments", mock.Anything, mock.Anything).Return(&db.EnvironmentsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
					Environments: []models.Environment{{Name: "test-environment"}},
				}, nil)

				mockLimits.On("CheckLimit", mock.Anything, mock.Anything, int32(1)).Return(nil)

				mockStore.On("UploadPipelineVariables", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, mock.Anything).Return(&models.ActivityEvent{}, nil)

				// End CreatePipeline mocks.

				mockUpdater.On("ProcessEvent", mock.Anything, mock.Anything, mock.Anything).Return(
					func(_ context.Context, pipelineID string, event interface{}) (*models.Pipeline, error) {
						require.Equal(t, parentPipeline.Metadata.ID, pipelineID)
						require.IsType(t, &UpdateNestedPipelineEvent{}, event)
						return parentPipeline, nil
					},
				).Once()
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Projects:          mockProjects,
				Pipelines:         mockPipelines,
				Transactions:      mockTransactions,
				PipelineTemplates: mockPipelineTemplates,
				Environments:      mockEnvironments,
				EnvironmentRules:  mockEnvironmentRules,
				ApprovalRules:     mockApprovalRules,
			}

			service := &service{
				logger:                 logger,
				dbClient:               dbClient,
				pipelineUpdater:        mockUpdater,
				store:                  mockStore,
				limitChecker:           mockLimits,
				dataStore:              mockTemplateStore,
				activityService:        mockActivityEvents,
				maxNestedPipelineLevel: maxNestedPipelineLevel,
			}

			pipeline, err := service.UpdateNestedPipeline(auth.WithCaller(ctx, mockCaller), &UpdateNestedPipelineInput{
				ParentPipelineID:       parentPipelineID,
				ParentPipelineNodePath: parentNestedPipelineNodePath,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, parentPipeline, pipeline)
		})
	}
}

func TestRetryNestedPipeline(t *testing.T) {
	projectID := "project-1"
	nestedPipelineID := "pipeline-1"
	parentPipelineID := "parent-pipeline"
	parentNestedPipelineNodePath := "pipeline.stage.s1.pipeline.p1"
	currentTime := time.Now().UTC()

	type testCase struct {
		name            string
		pipeline        *models.Pipeline
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully retry a pipeline",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ProjectID:              projectID,
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &parentNestedPipelineNodePath,
				Status:                 statemachine.FailedNodeStatus,
				Type:                   models.NestedPipelineType,
			},
		},
		{
			name: "subject is not authorized to retry the pipeline",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineID,
				},
				ProjectID:              projectID,
				ParentPipelineID:       &parentPipelineID,
				ParentPipelineNodePath: &parentNestedPipelineNodePath,
				Status:                 statemachine.FailedNodeStatus,
				Type:                   models.NestedPipelineType,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockStore := NewMockStore(t)
			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockLimits := limits.NewMockLimitChecker(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockTemplateStore := pipelinetemplate.NewMockDataStore(t)

			parentPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "parent-pipeline",
				},
				ProjectID: "project-1",
				Stages: []*models.PipelineStage{
					{
						NestedPipelines: []*models.NestedPipeline{
							{
								LatestPipelineID: nestedPipelineID,
								Path:             parentNestedPipelineNodePath,
								Status:           statemachine.FailedNodeStatus,
							},
						},
					},
				},
			}

			newPipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                "new-pipeline",
					CreationTimestamp: &currentTime,
				},
				ProjectID:              parentPipeline.ProjectID,
				ParentPipelineID:       &parentPipeline.Metadata.ID,
				ParentPipelineNodePath: &parentNestedPipelineNodePath,
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, parentPipeline.Metadata.ID).Return(parentPipeline, nil).Once()

			mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError)

			if test.authError == nil {
				mockPipelines.On("GetPipelineByID", mock.Anything, nestedPipelineID).Return(test.pipeline, nil).Once()
			}

			if test.expectErrorCode == "" {
				buf, err := json.Marshal([]Variable{})
				require.Nil(t, err)

				mockStore.On("GetPipelineVariables", mock.Anything, nestedPipelineID).Return(io.NopCloser(bytes.NewReader(buf)), nil)

				mockTransactions.On("BeginTx", mock.Anything).Return(func(inCtx context.Context) (context.Context, error) {
					return inCtx, nil
				}, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				// Begin CreatePipeline mocks.

				mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, mock.Anything).Return(&models.PipelineTemplate{
					Metadata: models.ResourceMetadata{
						ID: "pipeline-template-1",
					},
					Status: models.PipelineTemplateUploaded,
				}, nil)

				mockCaller.On("GetSubject").Return("test-subject").Maybe()
				mockCaller.On("Authorized").Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(nil)

				mockProjects.On("GetProjectByID", mock.Anything, mock.Anything).Return(&models.Project{
					Metadata: models.ResourceMetadata{
						ID: "project-1",
					},
					OrgID: "test-org",
				}, nil)

				template, err := os.Open(filepath.Join(testPipelineHCLDir, "valid_task_action_only.hcl"))
				require.NoError(t, err)
				defer template.Close()

				mockTemplateStore.On("GetData", mock.Anything, mock.Anything).Return(io.NopCloser(template), nil)

				mockPipelines.On("CreatePipeline", mock.Anything, mock.Anything).Return(newPipeline, nil)

				mockPipelines.On("GetPipelines", mock.Anything, mock.Anything).Return(&db.PipelinesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil)

				mockLimits.On("CheckLimit", mock.Anything, mock.Anything, int32(1)).Return(nil)

				mockStore.On("UploadPipelineVariables", mock.Anything, mock.Anything, mock.Anything).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, mock.Anything).Return(&models.ActivityEvent{}, nil)

				// End CreatePipeline mocks.

				mockUpdater.On("ProcessEvent", mock.Anything, mock.Anything, mock.Anything).Return(
					func(_ context.Context, pipelineID string, event interface{}) (*models.Pipeline, error) {
						require.Equal(t, parentPipeline.Metadata.ID, pipelineID)
						require.IsType(t, &RetryNestedPipelineEvent{}, event)
						return parentPipeline, nil
					},
				).Once()
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Projects:          mockProjects,
				Pipelines:         mockPipelines,
				Transactions:      mockTransactions,
				PipelineTemplates: mockPipelineTemplates,
			}

			service := &service{
				logger:                 logger,
				dbClient:               dbClient,
				pipelineUpdater:        mockUpdater,
				store:                  mockStore,
				limitChecker:           mockLimits,
				dataStore:              mockTemplateStore,
				activityService:        mockActivityEvents,
				maxNestedPipelineLevel: maxNestedPipelineLevel,
			}

			pipeline, err := service.RetryNestedPipeline(auth.WithCaller(ctx, mockCaller), parentPipeline.Metadata.ID, parentNestedPipelineNodePath)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, parentPipeline, pipeline)
		})
	}
}

func TestService_checkForDuplicateVariables(t *testing.T) {
	type testCase struct {
		name           string
		input          []Variable
		exectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "should remain unchanged",
			input: []Variable{
				{
					Key:      "key-1",
					Value:    "value-1",
					Category: models.HCLVariable,
				},
				{
					Key:      "key-2",
					Value:    "value-2",
					Category: models.EnvironmentVariable,
				},
			},
		},
		{
			name: "should keep the last variable with the same key",
			input: []Variable{
				{
					Key:      "key-1",
					Value:    "value-1",
					Category: models.HCLVariable,
				},
				{
					Key:      "key-1",
					Value:    "value-2",
					Category: models.HCLVariable,
				},
				{
					Key:      "key-2",
					Value:    "value-3",
					Category: models.EnvironmentVariable,
				},
				{
					Key:      "key-2",
					Value:    "value-4",
					Category: models.EnvironmentVariable,
				},
			},
			exectErrorCode: errors.EInvalid,
		},
		{
			name:  "no variables",
			input: []Variable{},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			service := &service{}

			err := service.checkForDuplicateVariables(test.input)

			if test.exectErrorCode != "" {
				assert.Equal(t, test.exectErrorCode, errors.ErrorCode(err))
				return
			}
			require.NoError(t, err)
		})
	}
}

func TestService_getPipelineByID(t *testing.T) {
	pipelineID := "pipeline-1"
	projectID := "project-1"

	type testCase struct {
		name            string
		pipeline        *models.Pipeline
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get pipeline by ID",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelines := db.NewMockPipelines(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(test.pipeline, nil)

			service := &service{
				dbClient: &db.Client{
					Pipelines: mockPipelines,
				},
			}

			pipeline, err := service.getPipelineByID(ctx, nil, pipelineID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pipeline, pipeline)
		})
	}
}

func TestService_convertHCLTemplateToModels(t *testing.T) {
	sampleProject := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: "project-1",
		},
		Name:  "test-project",
		OrgID: "org-1",
	}

	mainPipelineTemplateID := "pipeline-template-2"
	nestedPipelineTemplateID := "85cf59db-719b-4ca9-b4d3-a6c9a19ba3fe"
	nestedPipelineTemplatePRN := models.PipelineTemplateResource.BuildPRN("test-org", sampleProject.Name, gid.ToGlobalID(gid.PipelineTemplateType, nestedPipelineTemplateID))

	type testCase struct {
		nestedPipelineTemplate *models.PipelineTemplate
		variables              map[string]string
		expectConvertedData    *convertedTemplateData
		name                   string
		rawHCLFile             string
		expectErrorCode        errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "hcl template with only a stage, task and action",
			rawHCLFile: "valid_task_action_only.hcl",
			expectConvertedData: &convertedTemplateData{
				stages: []*models.PipelineStage{
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
				nestedPipelineVariables:      map[string][]Variable{},
				nestedPipelineWhenConditions: map[string]models.PipelineWhenCondition{},
			},
		},
		{
			name:       "valid full template",
			rawHCLFile: "valid_full.hcl",
			nestedPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineTemplateID,
				},
				ProjectID: sampleProject.Metadata.ID,
				Status:    models.PipelineTemplateUploaded,
			},
			expectConvertedData: &convertedTemplateData{
				nestedPipelineTemplateIDs: map[string]string{
					"pipeline.stage.dev.pipeline.deploy": nestedPipelineTemplateID,
				},
				stages: []*models.PipelineStage{
					{
						Name:   "dev.pre",
						Path:   "pipeline.stage.dev.pre",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "check_for_changes",
								Path:            "pipeline.stage.dev.pre.task.check_for_changes",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalRuleIDs: []string{},
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{},
								OnError:         statemachine.ContinueOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "find_changes",
										Path:       "pipeline.stage.dev.pre.task.check_for_changes.action.find_changes",
										ActionName: "command",
										PluginName: "exec",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:            "deploy",
								Path:            "pipeline.stage.dev.pipeline.deploy",
								EnvironmentName: ptr.String("account1"),
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								Dependencies:    []string{},
								OnError:         statemachine.FailOnError,
							},
						},
						Tasks: []*models.PipelineTask{
							{
								Name:            "execute_a_command",
								Path:            "pipeline.stage.dev.task.execute_a_command",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								Interval:        ptr.Duration(2 * time.Minute),
								MaxAttempts:     5,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_action",
										Path:       "pipeline.stage.dev.task.execute_a_command.action.perform_action",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
							{
								Name:            "security_scan",
								Path:            "pipeline.stage.dev.task.security_scan",
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								AgentTags:       []string{},
								Dependencies:    []string{"execute_a_command"},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "exec_command",
										Path:       "pipeline.stage.dev.task.security_scan.action.exec_command",
										PluginName: "exec",
										ActionName: "command",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
					{
						Name:   "dev.post",
						Path:   "pipeline.stage.dev.post",
						Status: statemachine.CreatedNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:            "cleanup_resources",
								Path:            "pipeline.stage.dev.post.task.cleanup_resources",
								Status:          statemachine.CreatedNodeStatus,
								When:            models.AutoPipelineTask,
								MaxAttempts:     1,
								ApprovalStatus:  models.ApprovalNotRequired,
								AgentTags:       []string{},
								Dependencies:    []string{},
								ApprovalRuleIDs: []string{},
								OnError:         statemachine.FailOnError,
								Actions: []*models.PipelineAction{
									{
										Name:       "perform_cleanup",
										Path:       "pipeline.stage.dev.post.task.cleanup_resources.action.perform_cleanup",
										ActionName: "command",
										PluginName: "exec",
										Status:     statemachine.CreatedNodeStatus,
									},
								},
							},
						},
					},
				},
				nestedPipelineVariables: map[string][]Variable{
					"pipeline.stage.dev.pipeline.deploy": {},
				},
				nestedPipelineWhenConditions: map[string]models.PipelineWhenCondition{
					"pipeline.stage.dev.pipeline.deploy": models.ManualPipeline,
				},
			},
		},
		{
			name:       "valid template with nested pipeline template PRN",
			rawHCLFile: "valid_nested_pipeline_only.hcl",
			variables: map[string]string{
				"deploy_connection_retries": "5",
				"testing_grpc_enabled":      "false",
				"testing_post_endpoint":     "https://domain.tld",
			},
			nestedPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineTemplateID,
				},
				ProjectID: sampleProject.Metadata.ID,
				Status:    models.PipelineTemplateUploaded,
			},
			expectConvertedData: &convertedTemplateData{
				nestedPipelineTemplateIDs: map[string]string{
					"pipeline.stage.dev.pipeline.deploy":  nestedPipelineTemplateID,
					"pipeline.stage.dev.pipeline.testing": nestedPipelineTemplateID,
				},
				stages: []*models.PipelineStage{
					{
						Name:   "dev",
						Path:   "pipeline.stage.dev",
						Status: statemachine.CreatedNodeStatus,
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:            "deploy",
								Path:            "pipeline.stage.dev.pipeline.deploy",
								EnvironmentName: ptr.String("account1"),
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								Dependencies:    []string{},
								OnError:         statemachine.FailOnError,
							},
							{
								Name:            "testing",
								Path:            "pipeline.stage.dev.pipeline.testing",
								EnvironmentName: ptr.String("account2"),
								Status:          statemachine.CreatedNodeStatus,
								ApprovalStatus:  models.ApprovalNotRequired,
								Dependencies:    []string{},
								OnError:         statemachine.FailOnError,
							},
						},
					},
				},
				nestedPipelineVariables: map[string][]Variable{
					"pipeline.stage.dev.pipeline.deploy": {
						{
							Value:    "5",
							Key:      "deploy_connection_retries",
							Category: models.HCLVariable,
						},
						{
							Value:    "https",
							Key:      "http_protocol",
							Category: models.HCLVariable,
						},
					},
					"pipeline.stage.dev.pipeline.testing": {
						{
							Value:    `false`,
							Key:      "testing_grpc_enabled",
							Category: models.HCLVariable,
						},
						{
							Value:    "https://domain.tld",
							Key:      "testing_post_endpoint",
							Category: models.HCLVariable,
						},
					},
				},
				nestedPipelineWhenConditions: map[string]models.PipelineWhenCondition{
					"pipeline.stage.dev.pipeline.deploy":  models.ManualPipeline,
					"pipeline.stage.dev.pipeline.testing": models.AutoPipeline,
				},
			},
		},
		{
			name:            "nested pipeline template not found",
			rawHCLFile:      "valid_full.hcl",
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "nested pipeline template is not in the same project",
			rawHCLFile: "valid_full.hcl",
			nestedPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineTemplateID,
				},
				ProjectID: "project-2",
				Status:    models.PipelineTemplateUploaded,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "nested pipeline template is not uploaded",
			rawHCLFile: "valid_full.hcl",
			nestedPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: nestedPipelineTemplateID,
				},
				ProjectID: sampleProject.Metadata.ID,
				Status:    models.PipelineTemplatePending,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "nested pipeline template is same as parent pipeline template",
			rawHCLFile: "valid_full.hcl",
			nestedPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: mainPipelineTemplateID,
				},
				ProjectID: sampleProject.Metadata.ID,
				Status:    models.PipelineTemplateUploaded,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelineTemplates := db.NewMockPipelineTemplates(t)

			mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, mock.Anything).Return(test.nestedPipelineTemplate, nil).Maybe()
			mockPipelineTemplates.On("GetPipelineTemplateByPRN", mock.Anything, nestedPipelineTemplatePRN).Return(test.nestedPipelineTemplate, nil).Maybe()

			dbClient := &db.Client{
				PipelineTemplates: mockPipelineTemplates,
			}

			service := &service{
				dbClient: dbClient,
			}

			template, err := os.Open(filepath.Join(testPipelineHCLDir, test.rawHCLFile))
			require.NoErrorf(t, err, "failed to open test HCL file %s", test.rawHCLFile)
			defer template.Close()

			hclConfig, _, err := config.NewPipelineTemplate(template, test.variables)
			require.NoError(t, err)

			ctyVariableValues, err := variable.GetCtyValuesForVariables(hclConfig.GetVariableBodies(), test.variables, false)
			require.NoError(t, err)

			evalCtx := hclctx.New(map[string]cty.Value{
				"var": cty.ObjectVal(ctyVariableValues),
			})

			convertedData, err := service.convertHCLTemplateToModels(ctx, hclConfig, evalCtx, sampleProject.Metadata.ID, mainPipelineTemplateID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, convertedData)
			assert.ElementsMatch(t, test.expectConvertedData.stages, convertedData.stages)

			for path, templateID := range convertedData.nestedPipelineTemplateIDs {
				require.Equal(t, test.expectConvertedData.nestedPipelineTemplateIDs[path], templateID)
			}

			for path, variables := range convertedData.nestedPipelineVariables {
				require.Len(t, variables, len(test.expectConvertedData.nestedPipelineVariables[path]))
				require.ElementsMatch(t, test.expectConvertedData.nestedPipelineVariables[path], variables)
			}

			for path, whenCondition := range convertedData.nestedPipelineWhenConditions {
				require.Equal(t, test.expectConvertedData.nestedPipelineWhenConditions[path], whenCondition)
			}
		})
	}
}

func TestSetSchedulePipelineNode(t *testing.T) {
	pipelineID := "pipeline-1"

	now := time.Now()

	type testCase struct {
		authError                  error
		checkEnvironmentRulesError error
		name                       string
		input                      *SchedulePipelineNodeInput
		expectErrorCode            errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully schedule a pipeline task with start time",
			input: &SchedulePipelineNodeInput{
				PipelineID:         pipelineID,
				NodeType:           statemachine.TaskNodeType,
				NodePath:           "pipeline.stage.s1.task.t1",
				ScheduledStartTime: &now,
			},
		},
		{
			name: "successfully schedule a pipeline task with cron schedule",
			input: &SchedulePipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   "pipeline.stage.s1.task.t1",
				CronSchedule: &models.CronSchedule{
					Expression: "0 0 * * *",
				},
			},
		},
		{
			name: "successfully schedule a nested pipeline with start time",
			input: &SchedulePipelineNodeInput{
				PipelineID:         pipelineID,
				NodeType:           statemachine.PipelineNodeType,
				NodePath:           "pipeline.stage.s1.pipeline.p1",
				ScheduledStartTime: &now,
			},
		},
		{
			name: "subject is not authorized to schedule the pipeline task",
			input: &SchedulePipelineNodeInput{
				PipelineID:         pipelineID,
				NodeType:           statemachine.TaskNodeType,
				NodePath:           "pipeline.stage.s1.task.t1",
				ScheduledStartTime: &now,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "fail to schedule pipeline node because start time and cron schedule are not set",
			input: &SchedulePipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
				NodePath:   "pipeline.stage.s1.pipeline.p1",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "fail to schedule pipeline node due to invalid node type",
			input: &SchedulePipelineNodeInput{
				PipelineID:         pipelineID,
				NodeType:           statemachine.StageNodeType,
				NodePath:           "pipeline.stage.s1",
				ScheduledStartTime: &now,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "gatekeeper environment protection rule check fails",
			input: &SchedulePipelineNodeInput{
				PipelineID:         pipelineID,
				NodeType:           statemachine.TaskNodeType,
				NodePath:           "pipeline.stage.s1.task.t1",
				ScheduledStartTime: &now,
			},
			checkEnvironmentRulesError: errors.New("", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			projectID := "project-1"
			environmentName := "environment-1"

			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				EnvironmentName: &environmentName,
				Stages: []*models.PipelineStage{
					{
						Name: "s1",
						Path: "pipeline.stage.s1",
					},
				},
			}

			switch test.input.NodeType {
			case statemachine.TaskNodeType:
				pipeline.Stages[0].Tasks = []*models.PipelineTask{
					{
						Name:               "t1",
						Path:               "pipeline.stage.s1.task.t1",
						ScheduledStartTime: test.input.ScheduledStartTime,
					},
				}
			case statemachine.PipelineNodeType:
				pipeline.Stages[0].NestedPipelines = []*models.NestedPipeline{
					{
						Name:               "p1",
						Path:               "pipeline.stage.s1.pipeline.p1",
						EnvironmentName:    ptr.String("nested-pipeline-environment-name"),
						LatestPipelineID:   "nested-pipeline-id",
						ScheduledStartTime: test.input.ScheduledStartTime,
					},
				}
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(pipeline, nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			if test.expectErrorCode == "" {
				activityEventInput := &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionSchedule,
					TargetType: models.TargetPipeline,
				}

				if test.input.NodeType == statemachine.TaskNodeType {
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &ScheduleTaskEvent{
						TaskPath:           test.input.NodePath,
						ScheduledStartTime: test.input.ScheduledStartTime,
						CronSchedule:       test.input.CronSchedule,
					}).Return(pipeline, nil)

					activityEventInput.TargetID = &pipelineID
					activityEventInput.Payload = &models.ActivityEventSchedulePipelineNodePayload{
						NodePath:  test.input.NodePath,
						NodeType:  test.input.NodeType.String(),
						StartTime: test.input.ScheduledStartTime,
					}
				}
				if test.input.NodeType == statemachine.PipelineNodeType {
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &ScheduleNestedPipelineEvent{
						NestedPipelinePath: test.input.NodePath,
						ScheduledStartTime: test.input.ScheduledStartTime,
						CronSchedule:       test.input.CronSchedule,
					}).Return(pipeline, nil)

					activityEventInput.TargetID = &pipelineID
					activityEventInput.Payload = &models.ActivityEventSchedulePipelineNodePayload{
						NodePath:  test.input.NodePath,
						NodeType:  test.input.NodeType.String(),
						StartTime: test.input.ScheduledStartTime,
					}
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, activityEventInput).Return(&models.ActivityEvent{}, nil)
			}

			// for gatekeeper environment rules check
			mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &ScheduleTaskEvent{
				TaskPath:           test.input.NodePath,
				ScheduledStartTime: test.input.ScheduledStartTime,
				CronSchedule:       test.input.CronSchedule,
			}).Return(pipeline, nil).Maybe()

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, "project-1", "environment-1").
				Return(test.checkEnvironmentRulesError).Maybe()
			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, "project-1", "nested-pipeline-environment-name").
				Return(test.checkEnvironmentRulesError).Maybe()

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				environmentGatekeeper: mockEnvironmentGatekeeper,
				activityService:       mockActivityEvents,
			}

			_, err := service.SchedulePipelineNode(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestUnschedulePipelineNode(t *testing.T) {
	pipelineID := "pipeline-1"

	type testCase struct {
		authError                  error
		checkEnvironmentRulesError error
		name                       string
		input                      *UnschedulePipelineNodeInput
		expectErrorCode            errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully unschedule a pipeline task",
			input: &UnschedulePipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   "some.path.to.node",
			},
		},
		{
			name: "successfully unschedule a nested pipeline",
			input: &UnschedulePipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
				NodePath:   "some.path.to.node",
			},
		},
		{
			name: "subject is not authorized to unschedule the pipeline task",
			input: &UnschedulePipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   "some.path.to.node",
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "fail to unschedule pipeline node due to invalid node type",
			input: &UnschedulePipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.StageNodeType,
				NodePath:   "some.path.to.node",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "gatekeeper environment protection rule check fails",
			input: &UnschedulePipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   "some.path.to.node",
			},
			checkEnvironmentRulesError: errors.New("", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			projectID := "project-1"
			environmentName := "environment-1"

			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			nestedPipelineNode := &models.NestedPipeline{
				Path:             "some.path.to.node",
				EnvironmentName:  ptr.String("nested-pipeline-environment-name"),
				LatestPipelineID: "nested-pipeline-id",
			}

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				EnvironmentName: &environmentName,
				Stages: []*models.PipelineStage{
					{
						NestedPipelines: []*models.NestedPipeline{nestedPipelineNode},
					},
				},
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(pipeline, nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			if test.expectErrorCode == "" {
				activityEventInput := &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionUnschedule,
					TargetType: models.TargetPipeline,
				}

				if test.input.NodeType == statemachine.TaskNodeType {
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &UnscheduleTaskEvent{
						TaskPath: test.input.NodePath,
					}).Return(pipeline, nil)

					activityEventInput.TargetID = &pipelineID
					activityEventInput.Payload = &models.ActivityEventUnschedulePipelineNodePayload{
						NodePath: test.input.NodePath,
						NodeType: test.input.NodeType.String(),
					}
				}
				if test.input.NodeType == statemachine.PipelineNodeType {
					mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &UnscheduleNestedPipelineEvent{
						NestedPipelinePath: test.input.NodePath,
					}).Return(pipeline, nil)

					activityEventInput.TargetID = &pipelineID
					activityEventInput.Payload = &models.ActivityEventUnschedulePipelineNodePayload{
						NodePath: test.input.NodePath,
						NodeType: test.input.NodeType.String(),
					}
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, activityEventInput).Return(&models.ActivityEvent{}, nil)
			}

			// for gatekeeper environment rules check
			mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &UnscheduleTaskEvent{
				TaskPath: test.input.NodePath,
			}).Return(pipeline, nil).Maybe()

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, "project-1", "environment-1").
				Return(test.checkEnvironmentRulesError).Maybe()
			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, "project-1", "nested-pipeline-environment-name").
				Return(test.checkEnvironmentRulesError).Maybe()

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				activityService:       mockActivityEvents,
				environmentGatekeeper: mockEnvironmentGatekeeper,
			}

			_, err := service.UnschedulePipelineNode(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestDeferPipelineNode(t *testing.T) {
	pipelineID := "pipeline-1"
	taskPath := "pipeline.stage.s1.task.t1"
	nestedPipelinePath := "pipeline.stage.s1.pipeline.p1"
	projectID := "project-1"
	nestedPipelineID := "nested-pipeline-id"
	environmentName := "environment-1"
	reason := "some reason"

	type testCase struct {
		authError                  error
		checkEnvironmentRulesError error
		pipeline                   *models.Pipeline
		input                      *DeferPipelineNodeInput
		name                       string
		expectErrorCode            errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully defer a pipeline task",
			input: &DeferPipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   taskPath,
				Reason:     reason,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path:   taskPath,
								Status: statemachine.CreatedNodeStatus,
							},
						},
					},
				},
			},
		},
		{
			name: "successfully defer a nested pipeline",
			input: &DeferPipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
				NodePath:   nestedPipelinePath,
				Reason:     reason,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						NestedPipelines: []*models.NestedPipeline{
							{
								Path:             nestedPipelinePath,
								LatestPipelineID: nestedPipelineID,
								Status:           statemachine.CreatedNodeStatus,
							},
						},
					},
				},
			},
		},
		{
			name:            "pipeline not found",
			input:           &DeferPipelineNodeInput{PipelineID: pipelineID},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to defer the pipeline task",
			input: &DeferPipelineNodeInput{
				PipelineID: pipelineID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "gatekeeper environment protection rule check fails",
			input: &DeferPipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   taskPath,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				EnvironmentName: &environmentName,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path: taskPath,
							},
						},
					},
				},
			},
			checkEnvironmentRulesError: errors.New("environment rule check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError)
			}

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			activityEventInput := &activityevent.CreateActivityEventInput{
				ProjectID:  &projectID,
				Action:     models.ActionDefer,
				TargetType: models.TargetPipeline,
			}

			if test.input.NodeType == statemachine.TaskNodeType {
				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &DeferTaskEvent{
					TaskPath: test.input.NodePath,
				}).Return(test.pipeline, nil).Maybe()

				activityEventInput.TargetID = &pipelineID
				activityEventInput.Payload = &models.ActivityEventDeferPipelineNodePayload{
					NodePath: test.input.NodePath,
					NodeType: statemachine.TaskNodeType.String(),
					Reason:   test.input.Reason,
				}
			}

			if test.input.NodeType == statemachine.PipelineNodeType {
				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &DeferNestedPipelineEvent{
					NestedPipelinePath: test.input.NodePath,
				}).Return(test.pipeline, nil).Maybe()

				activityEventInput.TargetID = &pipelineID
				activityEventInput.Payload = &models.ActivityEventDeferPipelineNodePayload{
					NodePath: test.input.NodePath,
					NodeType: statemachine.PipelineNodeType.String(),
					Reason:   test.input.Reason,
				}
			}

			mockActivityEvents.On("CreateActivityEvent", mock.Anything, activityEventInput).Return(&models.ActivityEvent{}, nil).Maybe()

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, projectID, environmentName).
				Return(test.checkEnvironmentRulesError).Maybe()

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				activityService:       mockActivityEvents,
				environmentGatekeeper: mockEnvironmentGatekeeper,
			}

			pipeline, err := service.DeferPipelineNode(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pipeline, pipeline)
		})
	}
}

func TestUndeferPipelineNode(t *testing.T) {
	pipelineID := "pipeline-1"
	taskPath := "pipeline.stage.s1.task.t1"
	nestedPipelinePath := "pipeline.stage.s1.pipeline.p1"
	projectID := "project-1"
	nestedPipelineID := "nested-pipeline-id"
	environmentName := "environment-1"

	type testCase struct {
		authError                  error
		checkEnvironmentRulesError error
		pipeline                   *models.Pipeline
		input                      *UndeferPipelineNodeInput
		name                       string
		expectErrorCode            errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully undefer a pipeline task",
			input: &UndeferPipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   taskPath,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path:   taskPath,
								Status: statemachine.DeferredNodeStatus,
							},
						},
					},
				},
			},
		},
		{
			name: "successfully undefer a nested pipeline",
			input: &UndeferPipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.PipelineNodeType,
				NodePath:   nestedPipelinePath,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
				Stages: []*models.PipelineStage{
					{
						NestedPipelines: []*models.NestedPipeline{
							{
								Path:             nestedPipelinePath,
								LatestPipelineID: nestedPipelineID,
								Status:           statemachine.DeferredNodeStatus,
							},
						},
					},
				},
			},
		},
		{
			name:            "pipeline not found",
			input:           &UndeferPipelineNodeInput{PipelineID: pipelineID},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to undefer the pipeline task",
			input: &UndeferPipelineNodeInput{
				PipelineID: pipelineID,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "gatekeeper environment protection rule check fails",
			input: &UndeferPipelineNodeInput{
				PipelineID: pipelineID,
				NodeType:   statemachine.TaskNodeType,
				NodePath:   taskPath,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID:       projectID,
				EnvironmentName: &environmentName,
				Stages: []*models.PipelineStage{
					{
						Tasks: []*models.PipelineTask{
							{
								Path: taskPath,
							},
						},
					},
				},
			},
			checkEnvironmentRulesError: errors.New("environment rule check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:            errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUpdater := NewMockUpdater(t)
			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockEnvironmentGatekeeper := NewMockEnvironmentGatekeeper(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ExecutePipeline, mock.Anything, mock.Anything).Return(test.authError)
			}

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			activityEventInput := &activityevent.CreateActivityEventInput{
				ProjectID:  &projectID,
				Action:     models.ActionUndefer,
				TargetType: models.TargetPipeline,
			}

			if test.input.NodeType == statemachine.TaskNodeType {
				activityEventInput.TargetID = &test.pipeline.Metadata.ID
				activityEventInput.Payload = &models.ActivityEventUndeferPipelineNodePayload{
					NodePath: taskPath,
					NodeType: statemachine.TaskNodeType.String(),
				}

				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &UndeferTaskEvent{
					TaskPath: taskPath,
				}).Return(test.pipeline, nil).Maybe()
			}

			if test.input.NodeType == statemachine.PipelineNodeType {
				activityEventInput.TargetID = &test.pipeline.Metadata.ID
				activityEventInput.Payload = &models.ActivityEventUndeferPipelineNodePayload{
					NodePath: nestedPipelinePath,
					NodeType: statemachine.PipelineNodeType.String(),
				}

				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &UndeferNestedPipelineEvent{
					NestedPipelinePath: nestedPipelinePath,
				}).Return(test.pipeline, nil).Maybe()
			}

			mockActivityEvents.On("CreateActivityEvent", mock.Anything, activityEventInput).Return(&models.ActivityEvent{}, nil).Maybe()

			mockEnvironmentGatekeeper.On("checkEnvironmentRules", mock.Anything, projectID, environmentName).
				Return(test.checkEnvironmentRulesError).Maybe()

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				activityService:       mockActivityEvents,
				environmentGatekeeper: mockEnvironmentGatekeeper,
			}

			pipeline, err := service.UndeferPipelineNode(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.pipeline, pipeline)
		})
	}
}

func TestService_verifyDependenciesExist(t *testing.T) {
	type testCase struct {
		name           string
		stage          *models.PipelineStage
		expectCodeType errors.CodeType
	}

	testCases := []testCase{
		{
			name: "valid dependencies",
			stage: &models.PipelineStage{
				Tasks: []*models.PipelineTask{
					{Name: "t1"},
					{Name: "t2", Dependencies: []string{"t1"}},
				},
				NestedPipelines: []*models.NestedPipeline{
					{Name: "p1", Dependencies: []string{"t2"}},
				},
			},
		},
		{
			name: "task has unknown dependency",
			stage: &models.PipelineStage{
				Tasks: []*models.PipelineTask{
					{Name: "t2", Dependencies: []string{"t1"}},
				},
			},
			expectCodeType: errors.EInvalid,
		},
		{
			name: "nested pipeline has unknown dependency",
			stage: &models.PipelineStage{
				NestedPipelines: []*models.NestedPipeline{
					{Name: "p1", Dependencies: []string{"t2"}},
				},
			},
			expectCodeType: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			s := &service{}

			err := s.verifyDependenciesExist(test.stage)

			if test.expectCodeType != "" {
				assert.Equal(t, test.expectCodeType, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
