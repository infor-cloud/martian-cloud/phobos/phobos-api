package pipeline

//go:generate go tool mockery --name Updater --inpackage --case underscore

import (
	"context"
	"fmt"
	"slices"
	"time"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// schdulableStates are the states that a pipeline node can be scheduled in
var schedulableStates = []statemachine.NodeStatus{
	statemachine.CreatedNodeStatus,
	statemachine.ReadyNodeStatus,
	statemachine.ApprovalPendingNodeStatus,
	statemachine.WaitingNodeStatus,
}

// EventHandler is a function that handles pipeline events
type EventHandler func(ctx context.Context, pipelineID string, changes []StatusChange, event any) error

// RunPipelineEvent is an event that starts a pipeline
type RunPipelineEvent struct{}

// ForceNestedPipelineStatusChangeEvent is an event for forcing a nested pipeline status change
type ForceNestedPipelineStatusChangeEvent struct {
	Status statemachine.NodeStatus
}

// ActionStatusChangeEvent is an event that changes the status of a pipeline action
type ActionStatusChangeEvent struct {
	ActionPath string
	Status     statemachine.NodeStatus
}

// JobStatusChangeEvent is an event that changes the pipeline task status based on a job status change
type JobStatusChangeEvent struct {
	Job *models.Job
}

// TaskApprovalEvent is an event that approves a pipeline task
type TaskApprovalEvent struct {
	UserID           *string
	ServiceAccountID *string
	TaskPath         string
}

// TaskRevokeApprovalEvent is an event that revokes approval of a pipeline task
type TaskRevokeApprovalEvent struct {
	UserID           *string
	ServiceAccountID *string
	TaskPath         string
}

// ApprovalEvent is the event that approves a pipeline
type ApprovalEvent struct {
	UserID           *string
	ServiceAccountID *string
}

// RevokeApprovalEvent is the event that revokes approval of a pipeline
type RevokeApprovalEvent struct {
	UserID           *string
	ServiceAccountID *string
}

// RunTaskEvent is an event that indicates a task is ready to be run
type RunTaskEvent struct {
	TaskPath string
}

// RetryTaskEvent is an event that indicates a task should be retried
type RetryTaskEvent struct {
	TaskPath string
}

// CancelPipelineEvent is an event that cancels a pipeline
type CancelPipelineEvent struct {
	Caller  auth.Caller
	Version *int
	Force   bool
}

// RunNestedPipelineEvent is an event that indicates a nested pipeline is ready to be run
type RunNestedPipelineEvent struct {
	NestedPipelinePath string
}

// RetryNestedPipelineEvent is an event that indicates a nested pipeline should be retried
type RetryNestedPipelineEvent struct {
	NestedPipelinePath string
	NestedPipelineID   string
}

// UpdateNestedPipelineEvent is an event that indicates a nested pipeline should be updated
type UpdateNestedPipelineEvent struct {
	NestedPipelinePath   string
	NestedPipelineID     string
	SupersededPipelineID string
}

// NestedPipelineStatusChangeEvent is an event that changes the status of a nested pipeline
type NestedPipelineStatusChangeEvent struct {
	NestedPipelinePath string
	Status             statemachine.NodeStatus
}

// ScheduleNestedPipelineEvent is an event that schedules a nested pipeline
type ScheduleNestedPipelineEvent struct {
	ScheduledStartTime *time.Time
	CronSchedule       *models.CronSchedule
	NestedPipelinePath string
}

// UnscheduleNestedPipelineEvent is an event that removes a schedule from a nested pipeline
type UnscheduleNestedPipelineEvent struct {
	NestedPipelinePath string
}

// ScheduleTaskEvent is an event that schedules a task
type ScheduleTaskEvent struct {
	ScheduledStartTime *time.Time
	CronSchedule       *models.CronSchedule
	TaskPath           string
}

// UnscheduleTaskEvent is an event that removes a schedule from a task
type UnscheduleTaskEvent struct {
	TaskPath string
}

// UpdatePipelineTimestampEvent is an event that updates the timestamps of a pipeline
type UpdatePipelineTimestampEvent struct {
	CompletedTimestamp *time.Time
	StartedTimestamp   *time.Time
}

// InitializeTaskEvent is an event that initializes a task
type InitializeTaskEvent struct {
	Error           error
	TaskPath        string
	ApprovalRuleIDs []string
	MarkAsSkipped   bool
}

// InitializeNestedPipelineEvent is an event that initializes a nested pipeline
type InitializeNestedPipelineEvent struct {
	Error              error
	NestedPipelinePath string
	ApprovalRuleIDs    []string
	MarkAsSkipped      bool
}

// DeferNestedPipelineEvent is an event that defers a nested pipeline
type DeferNestedPipelineEvent struct {
	NestedPipelinePath string
}

// DeferTaskEvent is an event that defers a task
type DeferTaskEvent struct {
	TaskPath string
}

// UndeferNestedPipelineEvent is an event that undefer a nested pipeline
type UndeferNestedPipelineEvent struct {
	NestedPipelinePath string
}

// UndeferTaskEvent is an event that undefer a task
type UndeferTaskEvent struct {
	TaskPath string
}

// StatusChange is a struct that contains the status changes for the specified pipeline
type StatusChange struct {
	PipelineID        string
	NodeStatusChanges []statemachine.NodeStatusChange
}

// Updater manages the execution of a pipeline
type Updater interface {
	RegisterEventHandler(handler EventHandler)
	ProcessEvent(ctx context.Context, pipelineID string, event any) (*models.Pipeline, error)
}

type updater struct {
	logger        logger.Logger
	dbClient      *db.Client
	eventHandlers []EventHandler
}

// NewUpdater creates a pipeline updater to manage the execution of pipelines
func NewUpdater(dbClient *db.Client, logger logger.Logger) Updater {
	return &updater{
		dbClient: dbClient,
		logger:   logger,
	}
}

func (u *updater) ProcessEvent(ctx context.Context, pipelineID string, event any) (*models.Pipeline, error) {
	var (
		updatedPipeline *models.Pipeline
		statusChanges   []StatusChange
	)

	// Wrap all event processing in a transaction
	tx, err := u.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, err
	}

	defer func() {
		if txErr := u.dbClient.Transactions.RollbackTx(tx); txErr != nil {
			u.logger.Errorf("failed to rollback tx in pipeline Updater: %v", txErr)
		}
	}()

	// Wrap the event processing in a retry loop to handle optimistic lock errors
	if rErr := u.dbClient.RetryOnOLE(tx, func() error {
		// Use a child transaction for the event processing incase it fails,
		// so that we can rollback any changes made.
		eventTx, uErr := u.dbClient.Transactions.BeginTx(tx)
		if uErr != nil {
			return errors.Wrap(uErr, "failed to begin event tx")
		}

		defer func() {
			if txErr := u.dbClient.Transactions.RollbackTx(eventTx); txErr != nil {
				u.logger.Errorf("failed to rollback event tx in pipeline Updater: %v", txErr)
			}
		}()

		pipeline, pErr := u.getPipelineByID(eventTx, pipelineID)
		if pErr != nil {
			return pErr
		}

		switch typed := event.(type) {
		case *RunPipelineEvent:
			updatedPipeline, statusChanges, err = u.runPipeline(eventTx, pipeline)
		case *ForceNestedPipelineStatusChangeEvent:
			updatedPipeline, statusChanges, err = u.forceNestedPipelineStatus(eventTx, pipeline, typed)
		case *ActionStatusChangeEvent:
			updatedPipeline, statusChanges, err = u.setPipelineActionStatus(eventTx, pipeline, typed)
		case *JobStatusChangeEvent:
			updatedPipeline, statusChanges, err = u.handleJobStatusChange(eventTx, pipeline, typed)
		case *TaskApprovalEvent:
			updatedPipeline, statusChanges, err = u.approvePipelineTask(eventTx, pipeline, typed)
		case *TaskRevokeApprovalEvent:
			updatedPipeline, statusChanges, err = u.revokePipelineTaskApproval(eventTx, pipeline, typed)
		case *ApprovalEvent:
			updatedPipeline, statusChanges, err = u.approvePipeline(eventTx, pipeline, typed)
		case *RevokeApprovalEvent:
			updatedPipeline, statusChanges, err = u.revokePipelineApproval(eventTx, pipeline, typed)
		case *RunTaskEvent:
			updatedPipeline, statusChanges, err = u.runTask(eventTx, pipeline, typed)
		case *RunNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.runNestedPipeline(eventTx, pipeline, typed)
		case *NestedPipelineStatusChangeEvent:
			updatedPipeline, statusChanges, err = u.setNestedPipelineStatus(eventTx, pipeline, typed)
		case *CancelPipelineEvent:
			updatedPipeline, statusChanges, err = u.handlePipelineCancellation(eventTx, pipeline, typed)
		case *RetryTaskEvent:
			updatedPipeline, statusChanges, err = u.retryTask(eventTx, pipeline, typed)
		case *RetryNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.retryNestedPipeline(eventTx, pipeline, typed)
		case *UpdateNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.updateNestedPipeline(eventTx, pipeline, typed)
		case *ScheduleNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.scheduleNestedPipeline(eventTx, pipeline, typed)
		case *UnscheduleNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.unscheduleNestedPipeline(eventTx, pipeline, typed)
		case *ScheduleTaskEvent:
			updatedPipeline, statusChanges, err = u.scheduleTask(eventTx, pipeline, typed)
		case *UnscheduleTaskEvent:
			updatedPipeline, statusChanges, err = u.unscheduleTask(eventTx, pipeline, typed)
		case *UpdatePipelineTimestampEvent:
			updatedPipeline, statusChanges, err = u.handleUpdatePipelineTimestamps(eventTx, pipeline, typed)
		case *InitializeTaskEvent:
			updatedPipeline, statusChanges, err = u.initializeTask(ctx, pipeline, typed)
		case *InitializeNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.initializeNestedPipeline(ctx, pipeline, typed)
		case *DeferNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.deferNestedPipeline(eventTx, pipeline, typed)
		case *DeferTaskEvent:
			updatedPipeline, statusChanges, err = u.deferTask(eventTx, pipeline, typed)
		case *UndeferNestedPipelineEvent:
			updatedPipeline, statusChanges, err = u.undeferNestedPipeline(eventTx, pipeline, typed)
		case *UndeferTaskEvent:
			updatedPipeline, statusChanges, err = u.undeferTask(eventTx, pipeline, typed)
		default:
			return errors.New("invalid event type")
		}

		if err != nil {
			return err
		}

		if cErr := u.dbClient.Transactions.CommitTx(eventTx); cErr != nil {
			return errors.Wrap(cErr, "failed to commit event tx")
		}

		return nil
	}); rErr != nil {
		return nil, rErr
	}

	// Trigger registered event handlers
	if fErr := u.fireEvents(tx, updatedPipeline.Metadata.ID, statusChanges, event); fErr != nil {
		return nil, fErr
	}

	// Get the updated pipeline again since an event handler could've updated it.
	updatedPipeline, err = u.getPipelineByID(tx, updatedPipeline.Metadata.ID)
	if err != nil {
		return nil, err
	}

	if err := u.dbClient.Transactions.CommitTx(tx); err != nil {
		return nil, err
	}

	return updatedPipeline, nil
}

func (u *updater) RegisterEventHandler(handler EventHandler) {
	u.eventHandlers = append(u.eventHandlers, handler)
}

func (u *updater) handleUpdatePipelineTimestamps(ctx context.Context, pipeline *models.Pipeline, event *UpdatePipelineTimestampEvent) (*models.Pipeline, []StatusChange, error) {
	if event.StartedTimestamp != nil {
		pipeline.Timestamps.StartedTimestamp = event.StartedTimestamp
		// Completed time should be set to nil here in case this pipeline is being restarted
		pipeline.Timestamps.CompletedTimestamp = nil
	}
	if event.CompletedTimestamp != nil {
		pipeline.Timestamps.CompletedTimestamp = event.CompletedTimestamp
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{}, nil
}

func (u *updater) runPipeline(ctx context.Context, pipeline *models.Pipeline) (*models.Pipeline, []StatusChange, error) {
	// Start pipeline
	statusChanges, err := pipeline.SetStatus(statemachine.RunningNodeStatus)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) forceNestedPipelineStatus(ctx context.Context, pipeline *models.Pipeline, event *ForceNestedPipelineStatusChangeEvent) (*models.Pipeline, []StatusChange, error) {
	// Force nested pipeline status
	statusChanges, err := pipeline.SetStatus(event.Status)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) setNestedPipelineStatus(ctx context.Context, pipeline *models.Pipeline, event *NestedPipelineStatusChangeEvent) (*models.Pipeline, []StatusChange, error) {
	nestedPipelinePath := event.NestedPipelinePath

	nestedPipelineNode, ok := pipeline.GetNestedPipeline(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("Child pipeline with path %s not found", nestedPipelinePath)
	}

	if nestedPipelineNode.Status.Equals(event.Status) {
		// Return since the nested pipeline has already been updated
		return pipeline, []StatusChange{}, nil
	}

	statusChanges, err := pipeline.SetNodeStatus(nestedPipelinePath, event.Status)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) runNestedPipeline(ctx context.Context, parent *models.Pipeline, event *RunNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	nestedPipelinePath := event.NestedPipelinePath

	nestedPipelineNode, ok := parent.GetNestedPipeline(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("Child pipeline with path %s not found", nestedPipelinePath)
	}

	if nestedPipelineNode.Status != statemachine.ReadyNodeStatus && nestedPipelineNode.Status != statemachine.WaitingNodeStatus {
		// Return since the nested pipeline has already been updated
		return parent, []StatusChange{}, nil
	}

	// Update nested pipeline state to running
	statusChanges, err := parent.SetNodeStatus(nestedPipelinePath, statemachine.RunningNodeStatus)
	if err != nil {
		return nil, nil, err
	}

	// Update parent pipeline
	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	nestedPipeline, err := u.getPipelineByID(ctx, nestedPipelineNode.LatestPipelineID)
	if err != nil {
		return nil, nil, err
	}

	_, nestedPipelineChanges, err := u.runPipeline(ctx, nestedPipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, append(nestedPipelineChanges, StatusChange{
		PipelineID:        updatedPipeline.Metadata.ID,
		NodeStatusChanges: statusChanges,
	}), nil
}

func (u *updater) updateNestedPipeline(ctx context.Context, parent *models.Pipeline, event *UpdateNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	nestedPipelinePath := event.NestedPipelinePath

	nestedPipelineNode, ok := parent.GetNestedPipeline(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("child pipeline with path %s not found", nestedPipelinePath, errors.WithErrorCode(errors.ENotFound))
	}

	// Must use new nested pipeline to determine the approval status since
	// the nested pipeline node could reflect incorrect approval status.
	newNestedPipeline, err := u.getPipelineByID(ctx, event.NestedPipelineID)
	if err != nil {
		return nil, nil, err
	}

	var nextStatus statemachine.NodeStatus

	switch nestedPipelineNode.Status {
	case statemachine.CreatedNodeStatus,
		statemachine.SkippedNodeStatus:
		// A nested pipeline in the CREATED, or SKIPPED state will remain
		// in the same state after being updated.
		nextStatus = nestedPipelineNode.Status
	case statemachine.ReadyNodeStatus,
		statemachine.SucceededNodeStatus,
		statemachine.FailedNodeStatus,
		statemachine.CanceledNodeStatus,
		statemachine.ApprovalPendingNodeStatus:
		// A nested pipeline in the READY, SUCCEEDED, FAILED, CANCELED, or APPROVAL_PENDING state will be updated to the INITIALIZING state
		nextStatus = statemachine.InitializingNodeStatus
		nestedPipelineNode.ClearSchedule()

		// Reset the approval status if not already.
		if newNestedPipeline.ApprovalStatus != models.ApprovalNotRequired {
			nestedPipelineNode.ApprovalStatus = models.ApprovalNotRequired
			newNestedPipeline.ApprovalStatus = models.ApprovalNotRequired
			newNestedPipeline.ApprovalRuleIDs = []string{}

			if _, uErr := u.dbClient.Pipelines.UpdatePipeline(ctx, newNestedPipeline); uErr != nil {
				return nil, nil, errors.Wrap(err, "failed to update new nested pipeline")
			}
		}
	default:
		// All other states are invalid for updating a nested pipeline.
		return nil, nil, errors.New("nested pipeline cannot be updated when it's in state %s", string(nestedPipelineNode.Status), errors.WithErrorCode(errors.EInvalid))
	}

	// Update nested pipeline status
	statusChanges, err := parent.SetNodeStatus(nestedPipelinePath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	supersededNestedPipeline, err := u.getPipelineByID(ctx, nestedPipelineNode.LatestPipelineID)
	if err != nil {
		return nil, nil, err
	}

	if !supersededNestedPipeline.Superseded {
		supersededNestedPipeline.Superseded = true
		if _, err = u.dbClient.Pipelines.UpdatePipeline(ctx, supersededNestedPipeline); err != nil {
			return nil, nil, errors.Wrap(err, "failed to update superseded nested pipeline")
		}
	}

	// Update parent pipeline node to point to the new latest pipeline
	nestedPipelineNode.LatestPipelineID = event.NestedPipelineID
	nestedPipelineNode.Errors = []string{}

	// Update parent pipeline
	updatedParent, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to update parent pipeline with latest nested pipeline ID")
	}

	return updatedParent, []StatusChange{{PipelineID: updatedParent.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) retryNestedPipeline(ctx context.Context, parent *models.Pipeline, event *RetryNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	nestedPipelinePath := event.NestedPipelinePath

	nestedPipelineNode, ok := parent.GetNestedPipeline(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("Child pipeline with path %s not found", nestedPipelinePath, errors.WithErrorCode(errors.ENotFound))
	}

	// Check if the nested pipeline is in a retryable state
	if !nestedPipelineNode.Status.IsRetryableStatus() {
		return nil, nil, errors.New("nested pipeline is not in a retryable state", errors.WithErrorCode(errors.EInvalid))
	}

	supersededNestedPipeline, err := u.getPipelineByID(ctx, nestedPipelineNode.LatestPipelineID)
	if err != nil {
		return nil, nil, err
	}

	if !supersededNestedPipeline.Superseded {
		supersededNestedPipeline.Superseded = true
		if _, err = u.dbClient.Pipelines.UpdatePipeline(ctx, supersededNestedPipeline); err != nil {
			return nil, nil, errors.Wrap(err, "failed to set nested pipeline to superseded")
		}
	}

	newNestedPipeline, err := u.getPipelineByID(ctx, event.NestedPipelineID)
	if err != nil {
		return nil, nil, err
	}

	// Copy over any approvals that the superseded pipeline had, so we don't
	// require them again for retrying a nested pipeline.
	if supersededNestedPipeline.ApprovalStatus != models.ApprovalNotRequired {
		approvalType := models.ApprovalTypePipeline
		existingPipelineApprovals, pErr := u.dbClient.PipelineApprovals.GetPipelineApprovals(ctx, &db.GetPipelineApprovalsInput{
			Filter: &db.PipelineApprovalFilter{
				PipelineID: &supersededNestedPipeline.Metadata.ID,
				Type:       &approvalType,
			},
		})
		if pErr != nil {
			return nil, nil, errors.Wrap(pErr, "failed to get existing pipeline approvals for superseded pipeline")
		}

		for _, approval := range existingPipelineApprovals.PipelineApprovals {
			if _, cErr := u.dbClient.PipelineApprovals.CreatePipelineApproval(ctx, &models.PipelineApproval{
				UserID:           approval.UserID,
				ServiceAccountID: approval.ServiceAccountID,
				PipelineID:       newNestedPipeline.Metadata.ID,
				Type:             approval.Type,
				ApprovalRuleIDs:  approval.ApprovalRuleIDs,
			}); cErr != nil {
				return nil, nil, errors.Wrap(cErr, "failed to get create pipeline approval for new nested pipeline")
			}
		}
	}

	// Sync the approval status between the superseded and new pipeline.
	newNestedPipeline.ApprovalStatus = supersededNestedPipeline.ApprovalStatus

	// Update the new nested pipeline
	_, err = u.dbClient.Pipelines.UpdatePipeline(ctx, newNestedPipeline)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to set approval status for new nested pipeline")
	}

	nestedPipelineNode.ApprovalStatus = supersededNestedPipeline.ApprovalStatus
	nestedPipelineNode.LatestPipelineID = event.NestedPipelineID
	nestedPipelineNode.ClearSchedule()
	nestedPipelineNode.ClearErrors()

	node, ok := parent.BuildStateMachine().GetNode(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("nested pipeline %s not found in parent pipeline", nestedPipelinePath)
	}

	// Update nested pipeline state to ready
	statusChanges, err := parent.SetNodeStatus(nestedPipelinePath, node.GetInitialStatus())
	if err != nil {
		return nil, nil, err
	}

	// Update parent pipeline
	updatedParent, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to update parent pipeline with latest nested pipeline ID")
	}

	return updatedParent, []StatusChange{{PipelineID: updatedParent.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) runTask(ctx context.Context, pipeline *models.Pipeline, event *RunTaskEvent) (*models.Pipeline, []StatusChange, error) {
	taskPath := event.TaskPath

	task, ok := pipeline.GetTask(taskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", taskPath)
	}

	if task.Status != statemachine.ReadyNodeStatus && task.Status != statemachine.WaitingNodeStatus {
		// Return since the task has already been updated
		return pipeline, []StatusChange{}, nil
	}

	// Increment the attempt count for this task
	task.AttemptCount++

	// There is no need to create a job for this task if it doesn't have any actions or a success condition
	if len(task.Actions) == 0 && !task.HasSuccessCondition {
		statusChanges, err := pipeline.SetNodeStatus(taskPath, statemachine.SucceededNodeStatus)
		if err != nil {
			return nil, nil, err
		}

		task.LastAttemptFinishedAt = ptr.Time(time.Now().UTC())

		updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
		if err != nil {
			return nil, nil, err
		}

		return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
	}

	// Update task state to pending
	statusChanges, err := pipeline.SetNodeStatus(taskPath, statemachine.PendingNodeStatus)
	if err != nil {
		return nil, nil, err
	}

	// Create job
	jobToCreate := &models.Job{
		Type:           models.JobTaskType,
		ProjectID:      pipeline.ProjectID,
		Image:          task.Image,
		MaxJobDuration: int32(models.DefaultMaxJobDuration.Minutes()),
		Timestamps: models.JobTimestamps{
			QueuedTimestamp: ptr.Time(time.Now().UTC()),
		},
		Data: &models.JobTaskData{
			PipelineID: pipeline.Metadata.ID,
			TaskPath:   taskPath,
		},
		Tags: task.AgentTags,
	}

	if err = jobToCreate.SetStatus(models.JobQueued); err != nil {
		return nil, nil, err
	}

	job, err := u.dbClient.Jobs.CreateJob(ctx, jobToCreate)
	if err != nil {
		return nil, nil, err
	}

	// Create log stream
	if _, err = u.dbClient.LogStreams.CreateLogStream(ctx, &models.LogStream{
		JobID: &job.Metadata.ID,
	}); err != nil {
		return nil, nil, err
	}

	task.LatestJobID = &job.Metadata.ID

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	// Add job to pipeline task
	if err = u.dbClient.Jobs.AddJobToPipelineTask(ctx, pipeline.Metadata.ID, taskPath, job); err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) retryTask(ctx context.Context, pipeline *models.Pipeline, event *RetryTaskEvent) (*models.Pipeline, []StatusChange, error) {
	// Verify pipeline task exists.
	task, ok := pipeline.GetTask(event.TaskPath)
	if !ok {
		return nil, nil, errors.New("task %s not found in pipeline", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	// Verify task is in a state that can be retried.
	if !task.Status.IsRetryableStatus() {
		return nil, nil, errors.New("task currently has %s status; only succeeded, failed or canceled tasks can be retried",
			task.Status,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	node, ok := pipeline.BuildStateMachine().GetNode(task.Path)
	if !ok {
		return nil, nil, errors.New("task %s not found in pipeline", task.Path)
	}

	statusChanges, err := pipeline.SetNodeStatus(task.Path, node.GetInitialStatus())
	if err != nil {
		return nil, nil, err
	}

	// Reset attempts count for the task
	task.AttemptCount = 0
	task.LastAttemptFinishedAt = nil
	task.ClearErrors()
	task.ClearSchedule()

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) approvePipelineTask(ctx context.Context, pipeline *models.Pipeline, event *TaskApprovalEvent) (*models.Pipeline, []StatusChange, error) {
	if event.UserID != nil && event.ServiceAccountID != nil {
		return nil, nil, errors.New("only one of user_id or service_account_id can be specified", errors.WithErrorCode(errors.EInvalid))
	}

	task, ok := pipeline.GetTask(event.TaskPath)
	if !ok {
		return nil, nil, errors.New("task %s not found in pipeline", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if task.ApprovalStatus == models.ApprovalNotRequired {
		return nil, nil, errors.New("task %s does not require approval", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if len(task.ApprovalRuleIDs) == 0 {
		return nil, nil, errors.New("task %s cannot be approved because all approval rules were deleted",
			event.TaskPath, errors.WithErrorCode(errors.EForbidden))
	}

	if task.Status != statemachine.ApprovalPendingNodeStatus && task.Status != statemachine.ReadyNodeStatus {
		return nil, nil, errors.New("task %s is not in the waiting state", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	approved, err := u.areApprovalRulesSatisfied(
		ctx,
		pipeline,
		event.UserID,
		event.ServiceAccountID,
		&event.TaskPath,
		task.ApprovalRuleIDs,
		models.ApprovalTypeTask,
	)
	if err != nil {
		return nil, nil, err
	}

	statusChanges := []statemachine.NodeStatusChange{}
	if approved {
		task.ApprovalStatus = models.ApprovalApproved

		nextStatus := statemachine.ReadyNodeStatus
		// If this is a scheduled task, the next status is waiting
		if task.ScheduledStartTime != nil {
			nextStatus = statemachine.WaitingNodeStatus
		}

		statusChanges, err = pipeline.SetNodeStatus(event.TaskPath, nextStatus)
		if err != nil {
			return nil, nil, err
		}

		pipeline, err = u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
		if err != nil {
			return nil, nil, err
		}
	}

	return pipeline, []StatusChange{{PipelineID: pipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) revokePipelineTaskApproval(ctx context.Context, pipeline *models.Pipeline, event *TaskRevokeApprovalEvent) (*models.Pipeline, []StatusChange, error) {
	if event.UserID != nil && event.ServiceAccountID != nil {
		return nil, nil, errors.New("only one of user_id or service_account_id can be specified", errors.WithErrorCode(errors.EInvalid))
	}

	task, ok := pipeline.GetTask(event.TaskPath)
	if !ok {
		return nil, nil, errors.New("task %s not found in pipeline", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if task.ApprovalStatus == models.ApprovalNotRequired {
		return nil, nil, errors.New("task %s does not require approval", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if len(task.ApprovalRuleIDs) == 0 {
		return nil, nil, errors.New("task %s approval cannot be revoked because all approval rules were deleted",
			event.TaskPath, errors.WithErrorCode(errors.EForbidden))
	}

	if task.Status != statemachine.ApprovalPendingNodeStatus && task.Status != statemachine.ReadyNodeStatus {
		return nil, nil, errors.New("task %s is not in the waiting state", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	approvalRevoked, err := u.areApprovalsRevoked(
		ctx,
		pipeline,
		event.UserID,
		event.ServiceAccountID,
		&event.TaskPath,
		task.ApprovalRuleIDs,
		models.ApprovalTypeTask,
	)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to revoke approval")
	}

	// If the task is in ready state but the approval count is dropping
	// below the threshold, the task must be put back to approval pending state.
	statusChanges := []statemachine.NodeStatusChange{}
	if approvalRevoked && task.Status == statemachine.ReadyNodeStatus {
		task.ApprovalStatus = models.ApprovalPending
		statusChanges, err = pipeline.SetNodeStatus(event.TaskPath, statemachine.ApprovalPendingNodeStatus)
		if err != nil {
			return nil, nil, err
		}

		// Write the updated pipeline to the DB and return the updated one.
		pipeline, err = u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
		if err != nil {
			return nil, nil, err
		}
	}

	// The pipeline object itself has either not changed or has been updated in place, so return it.
	return pipeline, []StatusChange{{PipelineID: pipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) approvePipeline(
	ctx context.Context,
	pipeline *models.Pipeline,
	event *ApprovalEvent,
) (
	*models.Pipeline,
	[]StatusChange,
	error,
) {
	if event.UserID != nil && event.ServiceAccountID != nil {
		return nil, nil, errors.New("only one of user_id or service_account_id can be specified", errors.WithErrorCode(errors.EInvalid))
	}

	if pipeline.ApprovalStatus == models.ApprovalNotRequired {
		return nil, nil, errors.New("pipeline does not require approval", errors.WithErrorCode(errors.EInvalid))
	}

	if len(pipeline.ApprovalRuleIDs) == 0 {
		return nil, nil, errors.New("pipeline cannot be approved because the approval rules were deleted", errors.WithErrorCode(errors.EForbidden))
	}

	if pipeline.Superseded {
		return nil, nil, errors.New("pipeline cannot be approved since it has been superseded", errors.WithErrorCode(errors.EInvalid))
	}

	if pipeline.Status != statemachine.ApprovalPendingNodeStatus && pipeline.Status != statemachine.ReadyNodeStatus {
		return nil, nil, errors.New("pipeline is not in the waiting state", errors.WithErrorCode(errors.EInvalid))
	}

	approved, err := u.areApprovalRulesSatisfied(
		ctx,
		pipeline,
		event.UserID,
		event.ServiceAccountID,
		nil,
		pipeline.ApprovalRuleIDs,
		models.ApprovalTypePipeline,
	)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to process pipeline approval")
	}

	statusChanges := []statemachine.NodeStatusChange{}
	if approved {
		pipeline.ApprovalStatus = models.ApprovalApproved

		nextStatus := statemachine.ReadyNodeStatus

		// Sync the nested pipeline's approval status in the parent pipeline.
		if pipeline.ParentPipelineID != nil {
			parentPipeline, pErr := u.getPipelineByID(ctx, *pipeline.ParentPipelineID)
			if pErr != nil {
				return nil, nil, pErr
			}

			nestedPipelineNode, ok := parentPipeline.GetNestedPipeline(*pipeline.ParentPipelineNodePath)
			if !ok {
				return nil, nil, errors.New("failed to get nested pipeline node %s in parent pipeline %s", *pipeline.ParentPipelineNodePath, *pipeline.ParentPipelineID)
			}

			// If this is a scheduled nested pipeline, the next status is waiting
			if nestedPipelineNode.ScheduledStartTime != nil {
				nextStatus = statemachine.WaitingNodeStatus
			}

			if nestedPipelineNode.ApprovalStatus != pipeline.ApprovalStatus {
				nestedPipelineNode.ApprovalStatus = pipeline.ApprovalStatus
				if _, pErr := u.dbClient.Pipelines.UpdatePipeline(ctx, parentPipeline); pErr != nil {
					return nil, nil, err
				}
			}
		}

		statusChanges, err = pipeline.SetStatus(nextStatus)
		if err != nil {
			return nil, nil, err
		}

		pipeline, err = u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
		if err != nil {
			return nil, nil, err
		}
	}

	return pipeline, []StatusChange{{PipelineID: pipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) revokePipelineApproval(
	ctx context.Context,
	pipeline *models.Pipeline,
	event *RevokeApprovalEvent,
) (
	*models.Pipeline,
	[]StatusChange,
	error,
) {
	if event.UserID != nil && event.ServiceAccountID != nil {
		return nil, nil, errors.New("only one of user_id or service_account_id can be specified", errors.WithErrorCode(errors.EInvalid))
	}

	if pipeline.ApprovalStatus == models.ApprovalNotRequired {
		return nil, nil, errors.New("pipeline does not require approval", errors.WithErrorCode(errors.EInvalid))
	}

	if len(pipeline.ApprovalRuleIDs) == 0 {
		return nil, nil, errors.New("pipeline approval cannot be revoked because the approval rule was deleted", errors.WithErrorCode(errors.EForbidden))
	}

	if pipeline.Superseded {
		return nil, nil, errors.New("pipeline approval cannot be revoked since it has been superseded", errors.WithErrorCode(errors.EInvalid))
	}

	if pipeline.Status != statemachine.ApprovalPendingNodeStatus && pipeline.Status != statemachine.ReadyNodeStatus {
		return nil, nil, errors.New("pipeline is not in the waiting state", errors.WithErrorCode(errors.EInvalid))
	}

	approvalRevoked, err := u.areApprovalsRevoked(
		ctx,
		pipeline,
		event.UserID,
		event.ServiceAccountID,
		nil,
		pipeline.ApprovalRuleIDs,
		models.ApprovalTypePipeline,
	)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to revoke approval")
	}

	// If the nested pipeline is in ready state but the approval count is dropping
	// below the threshold, it must be put back to approval pending state.
	statusChanges := []statemachine.NodeStatusChange{}
	if approvalRevoked && pipeline.Status == statemachine.ReadyNodeStatus {
		pipeline.ApprovalStatus = models.ApprovalPending
		statusChanges, err = pipeline.SetStatus(statemachine.ApprovalPendingNodeStatus)
		if err != nil {
			return nil, nil, err
		}

		// Write the updated pipeline to the DB and return the updated one.
		pipeline, err = u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
		if err != nil {
			return nil, nil, err
		}

		// Sync the nested pipeline's approval status in the parent pipeline.
		if pipeline.ParentPipelineID != nil {
			parentPipeline, pErr := u.getPipelineByID(ctx, *pipeline.ParentPipelineID)
			if pErr != nil {
				return nil, nil, pErr
			}

			nestedPipelineNode, ok := parentPipeline.GetNestedPipeline(*pipeline.ParentPipelineNodePath)
			if !ok {
				return nil, nil, errors.New("failed to get nested pipeline node %s in parent pipeline %s", *pipeline.ParentPipelineNodePath, *pipeline.ParentPipelineID)
			}

			if nestedPipelineNode.ApprovalStatus != pipeline.ApprovalStatus {
				nestedPipelineNode.ApprovalStatus = pipeline.ApprovalStatus
				if _, pErr := u.dbClient.Pipelines.UpdatePipeline(ctx, parentPipeline); pErr != nil {
					return nil, nil, err
				}
			}
		}
	}

	// The pipeline object itself has either not changed or has been updated in place, so return it.
	return pipeline, []StatusChange{{PipelineID: pipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) areApprovalRulesSatisfied(
	ctx context.Context,
	pipeline *models.Pipeline,
	userID,
	serviceAccountID,
	nodePath *string,
	approvalRuleIDs []string,
	approvalType models.PipelineApprovalType,
) (bool, error) {
	approvalRules, err := u.dbClient.ApprovalRules.GetApprovalRules(ctx, &db.GetApprovalRulesInput{
		Filter: &db.ApprovalRuleFilter{
			ApprovalRuleIDs: approvalRuleIDs,
		},
	})
	if err != nil {
		return false, errors.Wrap(err, "failed to get approval rules")
	}

	if int(approvalRules.PageInfo.TotalCount) != len(approvalRuleIDs) {
		return false, errors.New("one or more approval rules were not found", errors.WithErrorCode(errors.ENotFound))
	}

	ruleMatches := []*models.ApprovalRule{}
	for _, rule := range approvalRules.ApprovalRules {
		if userID != nil {
			// Check if user is specified in the rule
			if slices.Contains(rule.UserIDs, *userID) {
				ruleMatches = append(ruleMatches, rule)
			} else {
				// User is not specified so check if a team the user is in is specified
				teams, gtErr := u.dbClient.Teams.GetTeams(ctx, &db.GetTeamsInput{
					Filter: &db.TeamFilter{
						UserID: userID,
					},
				})
				if gtErr != nil {
					return false, errors.Wrap(gtErr, "failed to get teams for user")
				}

				for _, team := range teams.Teams {
					if slices.Contains(rule.TeamIDs, team.Metadata.ID) {
						ruleMatches = append(ruleMatches, rule)
						break
					}
				}
			}
		} else if slices.Contains(rule.ServiceAccountIDs, *serviceAccountID) {
			ruleMatches = append(ruleMatches, rule)
		}
	}

	// Return error if no rules were satisfied
	if len(ruleMatches) == 0 {
		if userID != nil {
			return false, errors.New("user is not allowed to approve on this request", errors.WithErrorCode(errors.EForbidden))
		}
		return false, errors.New("service account is not allowed to approve this request", errors.WithErrorCode(errors.EForbidden))
	}

	// If this is a user, resolve the open todo item since it's being approved
	if userID != nil {
		todoItemsInput := &db.GetToDoItemsInput{
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
			Filter: &db.ToDoItemFilter{
				UserID:           userID,
				Resolved:         ptr.Bool(false),
				PipelineTargetID: &pipeline.Metadata.ID,
			},
		}

		switch approvalType {
		case models.ApprovalTypePipeline:
			todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.PipelineApprovalTarget}
		case models.ApprovalTypeTask:
			todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.TaskApprovalTarget}
			todoItemsInput.Filter.PayloadFilter = &db.ToDoItemPayloadFilter{PipelineTaskPath: nodePath}
		}

		itemsResult, iErr := u.dbClient.ToDoItems.GetToDoItems(ctx, todoItemsInput)
		if iErr != nil {
			return false, errors.Wrap(iErr, "failed to get todo items")
		}

		if itemsResult.PageInfo.TotalCount > 0 {
			item := itemsResult.ToDoItems[0]
			item.ResolvedByUsers = append(item.ResolvedByUsers, *userID)

			if _, iErr := u.dbClient.ToDoItems.UpdateToDoItem(ctx, item); iErr != nil {
				return false, errors.Wrap(iErr, "failed to update todo item")
			}
		}
	}

	ruleIDs := make([]string, len(ruleMatches))
	for i, rule := range ruleMatches {
		ruleIDs[i] = rule.Metadata.ID
	}

	// Create approval
	if _, err = u.dbClient.PipelineApprovals.CreatePipelineApproval(ctx, &models.PipelineApproval{
		PipelineID:       pipeline.Metadata.ID,
		Type:             approvalType,
		TaskPath:         nodePath,
		UserID:           userID,
		ServiceAccountID: serviceAccountID,
		ApprovalRuleIDs:  ruleIDs,
	}); err != nil {
		if errors.ErrorCode(err) == errors.EConflict {
			if userID != nil {
				return false, errors.New("user has already approved this request", errors.WithErrorCode(errors.EConflict))
			}
			return false, errors.New("service account has already approved this request", errors.WithErrorCode(errors.EConflict))
		}
		return false, errors.Wrap(err, "failed to create pipeline approval")
	}

	// Check if approval rules are satisfied
	approvalResult, err := u.dbClient.PipelineApprovals.GetPipelineApprovals(ctx, &db.GetPipelineApprovalsInput{
		Filter: &db.PipelineApprovalFilter{
			PipelineID: &pipeline.Metadata.ID,
			TaskPath:   nodePath,
			Type:       &approvalType,
		},
	})
	if err != nil {
		return false, errors.Wrap(err, "failed to get pipeline approvals")
	}

	approvalsPerRule := make(map[string]int)
	for _, approval := range approvalResult.PipelineApprovals {
		for _, id := range approval.ApprovalRuleIDs {
			approvalsPerRule[id]++
		}
	}

	for _, rule := range approvalRules.ApprovalRules {
		if approvalsPerRule[rule.Metadata.ID] < rule.ApprovalsRequired {
			return false, nil
		}
	}

	// If all approval rules are satisfied, only then are we approved.
	return true, nil
}

func (u *updater) areApprovalsRevoked(
	ctx context.Context,
	pipeline *models.Pipeline,
	userID,
	serviceAccountID,
	nodePath *string,
	approvalRuleIDs []string,
	approvalType models.PipelineApprovalType,
) (bool, error) {
	// Get the rules in order to check whether approval status changes.
	approvalRules, err := u.dbClient.ApprovalRules.GetApprovalRules(ctx, &db.GetApprovalRulesInput{
		Filter: &db.ApprovalRuleFilter{
			ApprovalRuleIDs: approvalRuleIDs,
		},
	})
	if err != nil {
		return false, err
	}

	if int(approvalRules.PageInfo.TotalCount) != len(approvalRuleIDs) {
		return false, errors.New("one or more approval rules were not found", errors.WithErrorCode(errors.ENotFound))
	}

	// Since we have multiple approval rules, we must revoke the approval first.
	// Get the matching (same user or service account) approval, if any.
	approvalResult, err := u.dbClient.PipelineApprovals.GetPipelineApprovals(ctx, &db.GetPipelineApprovalsInput{
		Filter: &db.PipelineApprovalFilter{
			PipelineID:       &pipeline.Metadata.ID,
			TaskPath:         nodePath,
			UserID:           userID,
			ServiceAccountID: serviceAccountID,
			Type:             &approvalType,
		},
	})
	if err != nil {
		return false, err
	}

	// If no existing approval was found, that is an error.
	// This checks that the subject who revokes is the same as the subject who created the approval.
	if approvalResult.PageInfo.TotalCount == 0 {
		return false, errors.New("no approval found for user or service account on this pipeline or node", errors.WithErrorCode(errors.EForbidden))
	}

	// At this point, we know that
	// 1. the pipeline is in pending or ready state;
	// 2. and a matching approval was found.
	foundApproval := approvalResult.PipelineApprovals[0]

	// Remove the approval from the DB table.
	if err = u.dbClient.PipelineApprovals.DeletePipelineApproval(ctx, foundApproval); err != nil {
		return false, err
	}

	// un-resolve the todo item if a user revokes their approval.
	if userID != nil {
		todoItemsInput := &db.GetToDoItemsInput{
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
			Filter: &db.ToDoItemFilter{
				UserID:           userID,
				Resolved:         ptr.Bool(true),
				PipelineTargetID: &pipeline.Metadata.ID,
			},
		}

		switch approvalType {
		case models.ApprovalTypePipeline:
			todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.PipelineApprovalTarget}
		case models.ApprovalTypeTask:
			todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.TaskApprovalTarget}
			todoItemsInput.Filter.PayloadFilter = &db.ToDoItemPayloadFilter{PipelineTaskPath: nodePath}
		}

		itemsResult, iErr := u.dbClient.ToDoItems.GetToDoItems(ctx, todoItemsInput)
		if iErr != nil {
			return false, iErr
		}

		if itemsResult.PageInfo.TotalCount > 0 {
			item := itemsResult.ToDoItems[0]

			if index := slices.Index(item.ResolvedByUsers, *userID); index != -1 {
				item.ResolvedByUsers = append(item.ResolvedByUsers[:index], item.ResolvedByUsers[index+1:]...)
			}

			if _, iErr := u.dbClient.ToDoItems.UpdateToDoItem(ctx, item); iErr != nil {
				return false, iErr
			}
		}
	}

	// Get the number of remaining approvals for this nested pipeline.
	newApprovalResult, err := u.dbClient.PipelineApprovals.GetPipelineApprovals(ctx, &db.GetPipelineApprovalsInput{
		PaginationOptions: &pagination.Options{First: ptr.Int32(0)}, // don't return the data, just the count
		Filter: &db.PipelineApprovalFilter{
			PipelineID: &pipeline.Metadata.ID,
			TaskPath:   nodePath,
			Type:       &approvalType,
		},
	})
	if err != nil {
		return false, err
	}

	for _, rule := range approvalRules.ApprovalRules {
		if int(newApprovalResult.PageInfo.TotalCount) < rule.ApprovalsRequired {
			// If approval count is below the requirement, the approval status will change.
			// We only need one approval rule to be below its threshold, hence we can short-circuit here.
			return true, nil
		}
	}

	// This means we likely had an _additional_ approval revoked, so the approval status
	// of the pipeline / node should remain unchanged.
	return false, nil
}

func (u *updater) handleJobStatusChange(ctx context.Context, pipeline *models.Pipeline, event *JobStatusChangeEvent) (*models.Pipeline, []StatusChange, error) {
	taskData, ok := event.Job.Data.(*models.JobTaskData)
	if !ok {
		return nil, nil, errors.New("invalid job data, expected task data by received: %T", event.Job.Data)
	}

	task, ok := pipeline.GetTask(taskData.TaskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", taskData.TaskPath)
	}

	var (
		status        statemachine.NodeStatus
		statusChanges []statemachine.NodeStatusChange
		err           error
	)

	switch event.Job.GetStatus() {
	case models.JobRunning:
		status = statemachine.RunningNodeStatus
	case models.JobSucceeded:
		status = statemachine.SucceededNodeStatus
		task.LastAttemptFinishedAt = ptr.Time(time.Now().UTC())
	case models.JobFailed:
		task.LastAttemptFinishedAt = ptr.Time(time.Now().UTC())

		if !task.IsRetryable() {
			// Task will not be retried if max attempts is one or if the task has already reached the max attempts
			status = statemachine.FailedNodeStatus
		} else {
			// Task will be retried since it still has more attempts left
			status = statemachine.WaitingNodeStatus
		}
	case models.JobCanceling:
		status = statemachine.CancelingNodeStatus

		// Cancel the task
		statusChanges, err = pipeline.CancelNode(task.Path, false)
		if err != nil {
			return nil, nil, err
		}
	case models.JobCanceled:
		task.LastAttemptFinishedAt = ptr.Time(time.Now().UTC())
		status = statemachine.CanceledNodeStatus

		// Force cancel the task incase the job was canceled in queued status
		statusChanges, err = pipeline.CancelNode(task.Path, true)
		if err != nil {
			return nil, nil, err
		}
	default:
		return pipeline, []StatusChange{}, nil
	}

	if !task.Status.Equals(status) {
		// Task status doesn't match the job status so update the task status
		statusChanges, err = pipeline.SetNodeStatus(task.Path, status)
		if err != nil {
			return nil, nil, err
		}
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) setPipelineActionStatus(ctx context.Context, pipeline *models.Pipeline, event *ActionStatusChangeEvent) (*models.Pipeline, []StatusChange, error) {
	node, ok := pipeline.BuildStateMachine().GetNode(event.ActionPath)
	if !ok {
		return nil, nil, errors.New("action with path %s not found", event.ActionPath)
	}

	parent := node.Parent()
	if parent == nil {
		return nil, nil, errors.New("action %s does not have a parent", event.ActionPath)
	}

	taskPath := parent.Path()
	task, ok := pipeline.GetTask(taskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", taskPath)
	}

	nextStatus := event.Status
	if nextStatus == statemachine.FailedNodeStatus && task.IsRetryable() {
		// Instead of failing the action it'll go to a waiting state since it'll be
		// retried after the interval specified in the task.
		nextStatus = statemachine.WaitingNodeStatus
	}

	statusChanges, err := pipeline.SetNodeStatus(event.ActionPath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) handlePipelineCancellation(ctx context.Context, pipeline *models.Pipeline, event *CancelPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	if pipeline.Status.IsFinalStatus() {
		return nil, nil, errors.New(
			"%s pipeline is already in a final state, so it cannot be canceled", pipeline.Type,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	if pipeline.Status.Equals(statemachine.CreatedNodeStatus) {
		return nil, nil, errors.New(
			"%s pipeline has not started yet, so it cannot be canceled", pipeline.Type,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	if event.Version != nil && *event.Version != pipeline.Metadata.Version {
		return nil, nil, errors.New(
			"pipeline has been updated since the cancel request was made, so it cannot be canceled",
			errors.WithErrorCode(errors.EConflict),
		)
	}

	var (
		updatedPipeline *models.Pipeline
		statusChanges   []StatusChange
		err             error
	)

	if event.Force {
		updatedPipeline, statusChanges, err = u.forceCancelPipeline(ctx, pipeline, event.Caller)
	} else {
		updatedPipeline, statusChanges, err = u.gracefullyCancelPipeline(ctx, pipeline)
	}
	if err != nil {
		return nil, nil, err
	}

	// Recursively cancel any nested pipelines currently in progress.
	for _, s := range updatedPipeline.Stages {
		for _, nestedPipeline := range s.NestedPipelines {
			if nestedPipeline.Status.Equals(statemachine.CancelingNodeStatus) || nestedPipeline.Status.Equals(statemachine.CanceledNodeStatus) {
				np, err := u.getPipelineByID(ctx, nestedPipeline.LatestPipelineID)
				if err != nil {
					return nil, nil, err
				}

				if np.Status.IsFinalStatus() {
					// Skip pipelines that are already in a final state incase they were canceled directly.
					continue
				}

				_, changes, err := u.handlePipelineCancellation(ctx, np, &CancelPipelineEvent{
					Caller:  event.Caller,
					Force:   event.Force,
					Version: &np.Metadata.Version,
				})
				if err != nil {
					return nil, nil, err
				}

				statusChanges = append(statusChanges, changes...)
			}
		}
	}

	return updatedPipeline, statusChanges, nil
}

func (u *updater) gracefullyCancelPipeline(ctx context.Context, pipeline *models.Pipeline) (*models.Pipeline, []StatusChange, error) {
	if pipeline.Status.Equals(statemachine.CancelingNodeStatus) {
		return nil, nil, errors.New("%s pipeline cancellation is currently in progress", pipeline.Type, errors.WithErrorCode(errors.EInvalid))
	}

	// Cancel the pipeline.
	statusChanges, err := pipeline.CancelNode("pipeline", false)
	if err != nil {
		return nil, nil, err
	}

	jobs, err := u.getUnfinishedJobsForPipeline(ctx, pipeline.Metadata.ID)
	if err != nil {
		return nil, nil, err
	}

	// Cancel all jobs.
	for ix := range jobs {
		job := &jobs[ix]

		if err = job.GracefullyCancel(); err != nil {
			return nil, nil, err
		}

		updatedJob, uErr := u.dbClient.Jobs.UpdateJob(ctx, job)
		if uErr != nil {
			return nil, nil, uErr
		}

		if updatedJob.GetStatus() == models.JobCanceled {
			// Job (likely in queued status) was canceled, so cancel the associated node.
			switch typed := updatedJob.Data.(type) {
			case *models.JobTaskData:
				changes, cErr := pipeline.CancelNode(typed.TaskPath, false)
				if cErr != nil {
					return nil, nil, cErr
				}
				statusChanges = append(statusChanges, changes...)
			default:
				return nil, nil, errors.New("invalid job data, expected task data by received: %T", updatedJob.Data)
			}
		}
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) forceCancelPipeline(ctx context.Context, pipeline *models.Pipeline, caller auth.Caller) (*models.Pipeline, []StatusChange, error) {
	statusChanges, err := pipeline.CancelNode("pipeline", false)
	if err != nil {
		return nil, nil, err
	}

	jobs, err := u.getUnfinishedJobsForPipeline(ctx, pipeline.Metadata.ID)
	if err != nil {
		return nil, nil, err
	}

	// Set the job status to canceled.
	for ix := range jobs {
		job := &jobs[ix]

		if err = job.ForceCancel(caller.GetSubject()); err != nil {
			return nil, nil, err
		}

		if _, err = u.dbClient.Jobs.UpdateJob(ctx, job); err != nil {
			return nil, nil, err
		}
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) unscheduleTask(ctx context.Context, parent *models.Pipeline, event *UnscheduleTaskEvent) (*models.Pipeline, []StatusChange, error) {
	taskNode, ok := parent.GetTask(event.TaskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if taskNode.Status.Equals(statemachine.WaitingNodeStatus) && taskNode.AttemptCount > 0 {
		return nil, nil, errors.New(
			"task %s schedule cannot be modified since it's waiting to be retried",
			event.TaskPath,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	if !slices.Contains(schedulableStates, taskNode.Status) {
		return nil, nil, errors.New(
			"task %s cannot be unscheduled since it is not in one of the following states: %s",
			event.TaskPath,
			schedulableStates,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	// Clear the task schedule
	taskNode.ClearSchedule()

	nextStatus := taskNode.Status

	if taskNode.Status.Equals(statemachine.WaitingNodeStatus) {
		// If the task is in waiting state, it should go back to ready state
		nextStatus = statemachine.ReadyNodeStatus
	}

	// Update task state
	statusChanges, err := parent.SetNodeStatus(event.TaskPath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	// Update parent pipeline
	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) scheduleTask(ctx context.Context, parent *models.Pipeline, event *ScheduleTaskEvent) (*models.Pipeline, []StatusChange, error) {
	taskNode, ok := parent.GetTask(event.TaskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if taskNode.Status.Equals(statemachine.WaitingNodeStatus) && taskNode.AttemptCount > 0 {
		return nil, nil, errors.New(
			"task %s schedule cannot be modified since it's waiting to be retried",
			event.TaskPath,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	if !slices.Contains(schedulableStates, taskNode.Status) {
		return nil, nil, errors.New(
			"task %s cannot be scheduled since it is not in one of the following states: %s",
			event.TaskPath,
			schedulableStates,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	nextStatus := taskNode.Status

	if (event.ScheduledStartTime != nil && event.CronSchedule != nil) || (event.ScheduledStartTime == nil && event.CronSchedule == nil) {
		return nil, nil, errors.New("only one of scheduled_start_time or cron_schedule can be specified in event")
	} else if event.ScheduledStartTime != nil {
		taskNode.ScheduledStartTime = event.ScheduledStartTime
		taskNode.CronSchedule = nil

		if taskNode.Status.Equals(statemachine.ReadyNodeStatus) {
			nextStatus = statemachine.WaitingNodeStatus
		}
	} else {
		taskNode.CronSchedule = event.CronSchedule

		nextScheduledTime, err := taskNode.CronSchedule.NextTime()
		if err != nil {
			return nil, nil, errors.Wrap(err, "failed to calculate next scheduled time")
		}

		// Check if task is in a state where its schedule can be modified
		switch taskNode.Status {
		case statemachine.ApprovalPendingNodeStatus, statemachine.WaitingNodeStatus:
			taskNode.ScheduledStartTime = nextScheduledTime
		case statemachine.CreatedNodeStatus:
			taskNode.ScheduledStartTime = nil
		case statemachine.ReadyNodeStatus:
			// If the task is in ready state, it should go to waiting state
			nextStatus = statemachine.WaitingNodeStatus
			taskNode.ScheduledStartTime = nextScheduledTime
		}
	}

	// Update task state
	statusChanges, err := parent.SetNodeStatus(event.TaskPath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	// Update parent pipeline
	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) scheduleNestedPipeline(ctx context.Context, parent *models.Pipeline, event *ScheduleNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	pipelineNode, ok := parent.GetNestedPipeline(event.NestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("nested pipeline with path %s not found", event.NestedPipelinePath, errors.WithErrorCode(errors.ENotFound))
	}

	if !slices.Contains(schedulableStates, pipelineNode.Status) {
		return nil, nil, errors.New(
			"nested pipeline %s cannot be scheduled since it is not in one of the following states: %s",
			event.NestedPipelinePath,
			schedulableStates,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	nextStatus := pipelineNode.Status

	if (event.ScheduledStartTime != nil && event.CronSchedule != nil) || (event.ScheduledStartTime == nil && event.CronSchedule == nil) {
		return nil, nil, errors.New("only one of scheduled_start_time or cron_schedule can be specified in event")
	} else if event.ScheduledStartTime != nil {
		pipelineNode.ScheduledStartTime = event.ScheduledStartTime
		pipelineNode.CronSchedule = nil

		if pipelineNode.Status.Equals(statemachine.ReadyNodeStatus) {
			nextStatus = statemachine.WaitingNodeStatus
		}
	} else {
		pipelineNode.CronSchedule = event.CronSchedule

		nextScheduledTime, err := pipelineNode.CronSchedule.NextTime()
		if err != nil {
			return nil, nil, errors.Wrap(err, "failed to calculate next scheduled time")
		}

		switch pipelineNode.Status {
		case statemachine.ApprovalPendingNodeStatus, statemachine.WaitingNodeStatus:
			pipelineNode.ScheduledStartTime = nextScheduledTime
		case statemachine.CreatedNodeStatus:
			pipelineNode.ScheduledStartTime = nil
		case statemachine.ReadyNodeStatus:
			// If the task is in ready state, it should go to waiting state
			nextStatus = statemachine.WaitingNodeStatus
			pipelineNode.ScheduledStartTime = nextScheduledTime
		}
	}

	// Update nested pipeline state to waiting
	statusChanges, err := parent.SetNodeStatus(event.NestedPipelinePath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	// Update parent pipeline
	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) unscheduleNestedPipeline(ctx context.Context, parent *models.Pipeline, event *UnscheduleNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	pipelineNode, ok := parent.GetNestedPipeline(event.NestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("nested pipeline with path %s not found", event.NestedPipelinePath, errors.WithErrorCode(errors.ENotFound))
	}

	if !slices.Contains(schedulableStates, pipelineNode.Status) {
		return nil, nil, errors.New(
			"nested pipeline %s cannot be scheduled since it is not in one of the following states: %s",
			event.NestedPipelinePath,
			schedulableStates,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	// Clear the schedule
	pipelineNode.ClearSchedule()

	nextStatus := pipelineNode.Status

	if pipelineNode.Status.Equals(statemachine.WaitingNodeStatus) {
		// If the task is in waiting state, it should go back to ready state
		nextStatus = statemachine.ReadyNodeStatus
	}

	// Update nested pipeline state to waiting
	statusChanges, err := parent.SetNodeStatus(event.NestedPipelinePath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	// Update parent pipeline
	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) initializeTask(ctx context.Context, pipeline *models.Pipeline, event *InitializeTaskEvent) (*models.Pipeline, []StatusChange, error) {
	taskPath := event.TaskPath

	taskNode, ok := pipeline.GetTask(taskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", taskPath)
	}

	if !taskNode.Status.Equals(statemachine.InitializingNodeStatus) {
		return nil, nil, errors.New("task %s is not in the initializing state", taskPath)
	}

	var newStatus statemachine.NodeStatus

	switch {
	case event.Error != nil:
		// Event contains an initialization error meaning it'll go into 'FAILED' state.
		newStatus = statemachine.FailedNodeStatus
		taskNode.Errors = append(taskNode.Errors, event.Error.Error())
	case event.MarkAsSkipped:
		// Event contains a skip flag meaning it'll go into 'SKIPPED' state.
		newStatus = statemachine.SkippedNodeStatus
	case len(event.ApprovalRuleIDs) > 0 && taskNode.ApprovalStatus != models.ApprovalApproved:
		// Event contains approval rule IDs meaning it'll go into APPROVAL_PENDING state.
		newStatus = statemachine.ApprovalPendingNodeStatus
		taskNode.ApprovalRuleIDs = event.ApprovalRuleIDs
		taskNode.ApprovalStatus = models.ApprovalPending
	case taskNode.CronSchedule != nil || taskNode.ScheduledStartTime != nil:
		// It's important that this check is done after the approval rule check since the status will move to
		// approval pending instead of waiting if approvals are still required.
		newStatus = statemachine.WaitingNodeStatus
	default:
		// We'll default to the 'READY' state when there aren't any errors or approval rule IDs set on the event.
		newStatus = statemachine.ReadyNodeStatus
	}

	// Set scheduled start time if this task has a cron schedule
	if taskNode.CronSchedule != nil && !event.MarkAsSkipped {
		nextTime, err := taskNode.CronSchedule.NextTime()
		if err != nil {
			newStatus = statemachine.FailedNodeStatus
			taskNode.Errors = append(taskNode.Errors, fmt.Sprintf("failed to get next scheduled time from cron expression: %v", err))
		} else {
			taskNode.ScheduledStartTime = nextTime
		}
	}

	// Set last attempt time if this task is being moved to the failed state
	if newStatus.Equals(statemachine.FailedNodeStatus) {
		taskNode.LastAttemptFinishedAt = ptr.Time(time.Now().UTC())
	}

	if !newStatus.Equals(statemachine.FailedNodeStatus) {
		taskNode.Errors = []string{}
	}

	statusChanges, err := pipeline.SetNodeStatus(taskPath, newStatus)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) initializeNestedPipeline(ctx context.Context, parent *models.Pipeline, event *InitializeNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	nestedPipelinePath := event.NestedPipelinePath

	pipelineNode, ok := parent.GetNestedPipeline(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("nested pipeline with path %s not found", nestedPipelinePath)
	}

	if !pipelineNode.Status.Equals(statemachine.InitializingNodeStatus) {
		return nil, nil, errors.New("nested pipeline %s is not in the initializing state", nestedPipelinePath)
	}

	var newStatus statemachine.NodeStatus

	switch {
	case event.Error != nil:
		// Event contains an initialization error meaning it'll go into 'FAILED' state.
		newStatus = statemachine.FailedNodeStatus
		pipelineNode.Errors = append(pipelineNode.Errors, event.Error.Error())
	case event.MarkAsSkipped:
		// Event contains a skip flag meaning it'll go into 'SKIPPED' state.
		newStatus = statemachine.SkippedNodeStatus
	case len(event.ApprovalRuleIDs) > 0 && pipelineNode.ApprovalStatus != models.ApprovalApproved:
		// Event contains approval rule IDs meaning it'll go into APPROVAL_PENDING state.
		newStatus = statemachine.ApprovalPendingNodeStatus

		// Approval rule IDs are stored on the latest pipeline instance, so must look that up.
		nestedPipeline, err := u.getPipelineByID(ctx, pipelineNode.LatestPipelineID)
		if err != nil {
			return nil, nil, err
		}

		// Update approval rule IDs and sync approval status.
		nestedPipeline.ApprovalRuleIDs = event.ApprovalRuleIDs
		nestedPipeline.ApprovalStatus = models.ApprovalPending
		pipelineNode.ApprovalStatus = models.ApprovalPending

		// Update the nested pipeline to sync approval status.
		if _, uErr := u.dbClient.Pipelines.UpdatePipeline(ctx, nestedPipeline); uErr != nil {
			return nil, nil, errors.Wrap(uErr, "failed to update nested pipeline while initializing")
		}
	case pipelineNode.CronSchedule != nil || pipelineNode.ScheduledStartTime != nil:
		// It's important that this check is done after the approval rule check since the status will move to
		// approval pending instead of waiting if approvals are still required.
		newStatus = statemachine.WaitingNodeStatus
	default:
		// We'll default to the 'READY' state when there aren't any errors or approval rule IDs set on the event.
		newStatus = statemachine.ReadyNodeStatus
	}

	// Set scheduled start time if this nested pipeline has a cron schedule
	if pipelineNode.CronSchedule != nil && !event.MarkAsSkipped {
		nextTime, err := pipelineNode.CronSchedule.NextTime()
		if err != nil {
			newStatus = statemachine.FailedNodeStatus
			pipelineNode.Errors = append(pipelineNode.Errors, fmt.Sprintf("failed to get next scheduled time from cron expression: %v", err))
		} else {
			pipelineNode.ScheduledStartTime = nextTime
		}
	}

	if newStatus != statemachine.FailedNodeStatus {
		pipelineNode.Errors = []string{}
	}

	statusChanges, sErr := parent.SetNodeStatus(nestedPipelinePath, newStatus)
	if sErr != nil {
		return nil, nil, sErr
	}

	// Update the parent pipeline.
	updatedParent, uErr := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if uErr != nil {
		return nil, nil, uErr
	}

	return updatedParent, []StatusChange{{PipelineID: updatedParent.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) deferNestedPipeline(ctx context.Context, parent *models.Pipeline, event *DeferNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	nestedPipelinePath := event.NestedPipelinePath

	pipelineNode, ok := parent.GetNestedPipeline(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("nested pipeline with path %s not found", nestedPipelinePath)
	}

	if !pipelineNode.Status.IsDeferrableStatus() {
		return nil, nil, errors.New("nested pipeline %s is not in a deferrable state", nestedPipelinePath, errors.WithErrorCode(errors.EInvalid))
	}

	// Clear the schedule and errors
	pipelineNode.ClearSchedule()
	pipelineNode.ClearErrors()

	statusChanges, err := parent.SetNodeStatus(nestedPipelinePath, statemachine.DeferredNodeStatus)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) deferTask(ctx context.Context, parent *models.Pipeline, event *DeferTaskEvent) (*models.Pipeline, []StatusChange, error) {
	taskNode, ok := parent.GetTask(event.TaskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", event.TaskPath)
	}

	if !taskNode.Status.IsDeferrableStatus() {
		return nil, nil, errors.New("task %s is not in a deferrable state", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if parent.Superseded {
		return nil, nil, errors.New("task %s cannot be deferred since the pipeline is superseded", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	// Clear the schedule and errors
	taskNode.ClearSchedule()
	taskNode.ClearErrors()

	statusChanges, err := parent.SetNodeStatus(event.TaskPath, statemachine.DeferredNodeStatus)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) undeferNestedPipeline(ctx context.Context, parent *models.Pipeline, event *UndeferNestedPipelineEvent) (*models.Pipeline, []StatusChange, error) {
	nestedPipelinePath := event.NestedPipelinePath

	pipelineNode, ok := parent.GetNestedPipeline(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("nested pipeline with path %s not found", nestedPipelinePath)
	}

	if !pipelineNode.Status.Equals(statemachine.DeferredNodeStatus) {
		return nil, nil, errors.New("nested pipeline %s is not in a deferred state", nestedPipelinePath, errors.WithErrorCode(errors.EInvalid))
	}

	node, ok := parent.BuildStateMachine().GetNode(nestedPipelinePath)
	if !ok {
		return nil, nil, errors.New("nested pipeline %s not found in parent pipeline", nestedPipelinePath)
	}

	var nextStatus statemachine.NodeStatus
	if node.(statemachine.StageChildNode).HasUnfinishedDependencies() {
		// If the nested pipeline has unfinished dependencies, it should go back to CREATED state
		// so that it can run when the dependencies are finished.
		nextStatus = statemachine.CreatedNodeStatus
	} else {
		// If the nested pipeline has no unfinished dependencies, it should go back to it's initial state.
		nextStatus = node.GetInitialStatus()
	}

	statusChanges, err := parent.SetNodeStatus(nestedPipelinePath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) undeferTask(ctx context.Context, parent *models.Pipeline, event *UndeferTaskEvent) (*models.Pipeline, []StatusChange, error) {
	taskNode, ok := parent.GetTask(event.TaskPath)
	if !ok {
		return nil, nil, errors.New("task with path %s not found", event.TaskPath)
	}

	if !taskNode.Status.Equals(statemachine.DeferredNodeStatus) {
		return nil, nil, errors.New("task %s is not in a deferred state", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if parent.Status.Equals(statemachine.DeferredNodeStatus) {
		return nil, nil, errors.New("task %s cannot be undeferred since the pipeline is deferred", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	if parent.Superseded {
		return nil, nil, errors.New("task %s cannot be undeferred since the pipeline is superseded", event.TaskPath, errors.WithErrorCode(errors.EInvalid))
	}

	node, ok := parent.BuildStateMachine().GetNode(taskNode.Path)
	if !ok {
		return nil, nil, errors.New("task %s not found in parent pipeline", taskNode.Path)
	}

	var nextStatus statemachine.NodeStatus
	if node.(statemachine.StageChildNode).HasUnfinishedDependencies() {
		// If the task has unfinished dependencies, it should go back to CREATED state
		// so that it can run when the dependencies are finished.
		nextStatus = statemachine.CreatedNodeStatus
	} else {
		// If the task has no unfinished dependencies, it should go back to it's initial state.
		nextStatus = node.GetInitialStatus()
	}

	statusChanges, err := parent.SetNodeStatus(event.TaskPath, nextStatus)
	if err != nil {
		return nil, nil, err
	}

	updatedPipeline, err := u.dbClient.Pipelines.UpdatePipeline(ctx, parent)
	if err != nil {
		return nil, nil, err
	}

	return updatedPipeline, []StatusChange{{PipelineID: updatedPipeline.Metadata.ID, NodeStatusChanges: statusChanges}}, nil
}

func (u *updater) fireEvents(ctx context.Context, pipelineID string, statusChanges []StatusChange, event any) error {
	for _, handler := range u.eventHandlers {
		if err := handler(ctx, pipelineID, statusChanges, event); err != nil {
			return err
		}
	}
	return nil
}

func (u *updater) getUnfinishedJobsForPipeline(ctx context.Context, pipelineID string) ([]models.Job, error) {
	dbResult, err := u.dbClient.Jobs.GetJobs(ctx, &db.GetJobsInput{
		Filter: &db.JobFilter{
			PipelineID: &pipelineID,
			JobStatuses: []models.JobStatus{
				models.JobPending,
				models.JobRunning,
				models.JobQueued,
				models.JobCanceling,
			},
		},
	})
	if err != nil {
		return nil, err
	}

	return dbResult.Jobs, nil
}

func (u *updater) getPipelineByID(ctx context.Context, id string) (*models.Pipeline, error) {
	pipeline, err := u.dbClient.Pipelines.GetPipelineByID(ctx, id)
	if err != nil {
		return nil, err
	}

	if pipeline == nil {
		return nil, errors.New(
			"pipeline with id %s not found", id,
			errors.WithErrorCode(errors.ENotFound))
	}

	return pipeline, nil
}
