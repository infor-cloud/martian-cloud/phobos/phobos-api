package eventhandlers

import (
	"context"
	"time"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// MetricManager manages metrics based on the pipeline's state.
type MetricManager struct {
	logger          logger.Logger
	dbClient        *db.Client
	pipelineUpdater pipeline.Updater
}

// NewMetricManager returns an instance of MetricsManager.
func NewMetricManager(logger logger.Logger, dbClient *db.Client, pipelineUpdater pipeline.Updater) *MetricManager {
	return &MetricManager{
		logger:          logger,
		dbClient:        dbClient,
		pipelineUpdater: pipelineUpdater,
	}
}

// RegisterHandlers registers any handlers with the PipelineUpdater used by the MetricsManager.
func (m *MetricManager) RegisterHandlers() {
	m.pipelineUpdater.RegisterEventHandler(func(ctx context.Context, pipelineID string, changes []pipeline.StatusChange, _ any) error {
		for _, change := range changes {
			if pipelineID == change.PipelineID {
				for _, nodeChange := range change.NodeStatusChanges {
					// We only want to create the metric for the pipeline node and the pipeline is in a final state
					// We're avoiding SKIPPED status and alike since the pipeline never started.
					if nodeChange.NodePath == "pipeline" &&
						(nodeChange.NewStatus.Equals(statemachine.SucceededNodeStatus) ||
							nodeChange.NewStatus.Equals(statemachine.FailedNodeStatus) ||
							nodeChange.NewStatus.Equals(statemachine.CanceledNodeStatus)) {
						return m.createPipelineMetrics(ctx, pipelineID, time.Now())
					}
				}
			}
		}
		return nil
	})

	m.pipelineUpdater.RegisterEventHandler(func(ctx context.Context, _ string, _ []pipeline.StatusChange, event any) error {
		if e, ok := event.(*pipeline.UpdateNestedPipelineEvent); ok {
			return m.createDeploymentUpdatedMetric(ctx, e.SupersededPipelineID)
		}

		return nil
	})

	m.pipelineUpdater.RegisterEventHandler(func(ctx context.Context, pipelineID string, changes []pipeline.StatusChange, _ any) error {
		for _, change := range changes {
			for _, nodeChange := range change.NodeStatusChanges {
				// Check if this node has transitioned from approval pending to ready
				isApprovedChange := nodeChange.OldStatus.Equals(statemachine.ApprovalPendingNodeStatus) && nodeChange.NewStatus.Equals(statemachine.ReadyNodeStatus)
				// Skip this event if it's not for an approved node or a node that has transitioned to approval pending
				if !isApprovedChange && !nodeChange.NewStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
					continue
				}

				// We only want to create the metric if this is a task node type or if it's a top-level pipeline node (i.e. not a nested pipeline node)
				if nodeChange.NodeType == statemachine.TaskNodeType ||
					(nodeChange.NodeType == statemachine.PipelineNodeType && nodeChange.NodePath == "pipeline") {
					// Passing in nil for nodePath since the metric for pipelines is associated with the pipeline object itself.
					nodeChangeCopy := nodeChange
					return m.createApprovalMetrics(ctx, pipelineID, &nodeChangeCopy, time.Now())
				}
			}
		}
		return nil
	})
}

func (m *MetricManager) createApprovalMetrics(
	ctx context.Context,
	pipelineID string,
	nodeChange *statemachine.NodeStatusChange,
	currentTime time.Time,
) error {
	pipeline, err := m.getPipelineByID(ctx, pipelineID)
	if err != nil {
		return err
	}

	project, err := m.getProjectByID(ctx, pipeline.ProjectID)
	if err != nil {
		return err
	}

	tags := map[models.MetricTagName]string{
		models.MetricTagNamePipelineType:       string(pipeline.Type),
		models.MetricTagNameApprovalTargetType: string(nodeChange.NodeType),
	}
	if pipeline.ReleaseID != nil {
		release, err := m.dbClient.Releases.GetReleaseByID(ctx, *pipeline.ReleaseID)
		if err != nil {
			return errors.Wrap(err, "failed to get release")
		}
		if release == nil {
			return errors.New("release with id %s not found", *pipeline.ReleaseID)
		}
		tags[models.MetricTagNameReleaseLifecycle] = release.LifecyclePRN
	}

	if nodeChange.NewStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
		if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
			Name:            models.MetricApprovalRequestCreated,
			PipelineID:      &pipeline.Metadata.ID,
			ReleaseID:       pipeline.ReleaseID,
			ProjectID:       &pipeline.ProjectID,
			OrganizationID:  &project.OrgID,
			EnvironmentName: pipeline.EnvironmentName,
			Value:           1,
			Tags:            tags,
		}); err != nil {
			return err
		}
	} else {
		// Get TODO item associated with approval so that we can determine when this approval was first requested
		sort := db.ToDoItemSortableFieldCreatedAtDesc
		todoItemsInput := &db.GetToDoItemsInput{
			Sort: &sort,
			Filter: &db.ToDoItemFilter{
				PipelineTargetID: &pipeline.Metadata.ID,
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
		}

		switch nodeChange.NodeType {
		case statemachine.TaskNodeType:
			todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.TaskApprovalTarget}
			todoItemsInput.Filter.PayloadFilter = &db.ToDoItemPayloadFilter{PipelineTaskPath: &nodeChange.NodePath}
		case statemachine.PipelineNodeType:
			todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.PipelineApprovalTarget}
		}

		todoItems, err := m.dbClient.ToDoItems.GetToDoItems(ctx, todoItemsInput)
		if err != nil {
			return errors.Wrap(err, "failed to get todo items")
		}

		if len(todoItems.ToDoItems) > 0 {
			approvalWaitTime := currentTime.Sub(*todoItems.ToDoItems[0].Metadata.CreationTimestamp).Seconds()

			if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
				Name:            models.MetricApprovalRequestWaitTime,
				PipelineID:      &pipeline.Metadata.ID,
				ReleaseID:       pipeline.ReleaseID,
				ProjectID:       &pipeline.ProjectID,
				OrganizationID:  &project.OrgID,
				EnvironmentName: pipeline.EnvironmentName,
				Value:           approvalWaitTime,
				Tags:            tags,
			}); err != nil {
				return err
			}

			if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
				Name:            models.MetricApprovalRequestCompleted,
				PipelineID:      &pipeline.Metadata.ID,
				ReleaseID:       pipeline.ReleaseID,
				ProjectID:       &pipeline.ProjectID,
				OrganizationID:  &project.OrgID,
				EnvironmentName: pipeline.EnvironmentName,
				Value:           1,
				Tags:            tags,
			}); err != nil {
				return err
			}
		} else {
			m.logger.Errorf("no todo item found for pipeline approval %s", pipeline.Metadata.ID)
		}
	}

	return nil
}

func (m *MetricManager) createDeploymentUpdatedMetric(ctx context.Context, supersededPipelineID string) error {
	pipeline, err := m.getPipelineByID(ctx, supersededPipelineID)
	if err != nil {
		return err
	}

	// Get the project to find the organization ID
	project, err := m.getProjectByID(ctx, pipeline.ProjectID)
	if err != nil {
		return err
	}

	tags := map[models.MetricTagName]string{}
	if pipeline.ReleaseID != nil {
		release, err := m.dbClient.Releases.GetReleaseByID(ctx, *pipeline.ReleaseID)
		if err != nil {
			return errors.Wrap(err, "failed to get release")
		}
		if release == nil {
			return errors.New("release with id %s not found", *pipeline.ReleaseID)
		}
		tags[models.MetricTagNameReleaseLifecycle] = release.LifecyclePRN
	}

	if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
		Name:            models.MetricDeploymentUpdated,
		PipelineID:      &pipeline.Metadata.ID,
		ReleaseID:       pipeline.ReleaseID,
		ProjectID:       &pipeline.ProjectID,
		OrganizationID:  &project.OrgID,
		EnvironmentName: pipeline.EnvironmentName,
		Tags:            tags,
		Value:           1,
	}); err != nil {
		return errors.Wrap(err, "failed to create %s metric", models.MetricDeploymentUpdated)
	}

	return nil
}

func (m *MetricManager) createPipelineMetrics(ctx context.Context, pipelineID string, currentTime time.Time) error {
	pipeline, err := m.getPipelineByID(ctx, pipelineID)
	if err != nil {
		return err
	}

	if pipeline.Timestamps.StartedTimestamp == nil {
		// If this pipeline never started, we'll skip creating the metrics.
		// For example, a pipeline can fail if it couldn't initialize properly.
		return nil
	}

	// Get the project to find the organization ID
	project, err := m.getProjectByID(ctx, pipeline.ProjectID)
	if err != nil {
		return err
	}

	tags := map[models.MetricTagName]string{
		models.MetricTagNamePipelineType:   string(pipeline.Type),
		models.MetricTagNamePipelineStatus: string(pipeline.Status),
	}

	if pipeline.ReleaseID != nil {
		release, err := m.dbClient.Releases.GetReleaseByID(ctx, *pipeline.ReleaseID)
		if err != nil {
			return errors.Wrap(err, "failed to get release")
		}
		if release == nil {
			return errors.New("release with id %s not found", *pipeline.ReleaseID)
		}
		tags[models.MetricTagNameReleaseLifecycle] = release.LifecyclePRN
	}

	if pipeline.Type == models.DeploymentPipelineType && pipeline.Status.Equals(statemachine.SucceededNodeStatus) {
		/* Create metric for deployment recovery time */

		// Get the metric for the last successful deployment to the same environment
		sort := db.MetricSortableFieldCreatedAtDesc
		metricName := models.MetricPipelineCompleted
		metrics, err := m.dbClient.Metrics.GetMetrics(ctx, &db.GetMetricsInput{
			Sort: &sort,
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
			Filter: &db.MetricFilter{
				ProjectID:       &project.Metadata.ID,
				OrganizationID:  &project.OrgID,
				EnvironmentName: pipeline.EnvironmentName,
				MetricName:      &metricName,
				Tags: map[models.MetricTagName]string{
					models.MetricTagNamePipelineType:   string(models.DeploymentPipelineType),
					models.MetricTagNamePipelineStatus: string(statemachine.SucceededNodeStatus),
				},
			},
		})
		if err != nil {
			return errors.Wrap(err, "failed to get metrics")
		}
		var timeRangeStart *time.Time
		if len(metrics.Metrics) > 0 {
			timeRangeStart = metrics.Metrics[0].Metadata.CreationTimestamp
		}

		// Find first failed deployment after the last successful deployment
		sort = db.MetricSortableFieldCreatedAtAsc
		metrics, err = m.dbClient.Metrics.GetMetrics(ctx, &db.GetMetricsInput{
			Sort: &sort,
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
			Filter: &db.MetricFilter{
				ProjectID:       &project.Metadata.ID,
				OrganizationID:  &project.OrgID,
				EnvironmentName: pipeline.EnvironmentName,
				MetricName:      &metricName,
				TimeRangeStart:  timeRangeStart,
				Tags: map[models.MetricTagName]string{
					models.MetricTagNamePipelineType:   string(models.DeploymentPipelineType),
					models.MetricTagNamePipelineStatus: string(statemachine.FailedNodeStatus),
				},
			},
		})
		if err != nil {
			return errors.Wrap(err, "failed to get metrics")
		}

		if len(metrics.Metrics) > 0 {
			// Add recovery time metric for the deployment
			recoveryTime := currentTime.Sub(*metrics.Metrics[0].Metadata.CreationTimestamp).Seconds()

			if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
				Name:            models.MetricDeploymentRecoveryTime,
				PipelineID:      &pipeline.Metadata.ID,
				ReleaseID:       pipeline.ReleaseID,
				ProjectID:       &pipeline.ProjectID,
				OrganizationID:  &project.OrgID,
				EnvironmentName: pipeline.EnvironmentName,
				Tags:            tags,
				Value:           recoveryTime,
			}); err != nil {
				return errors.Wrap(err, "failed to create %s metric", models.MetricPipelineDuration)
			}
		}
	}

	// Create a metric to track pipeline completion count
	if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
		Name:            models.MetricPipelineCompleted,
		PipelineID:      &pipeline.Metadata.ID,
		ReleaseID:       pipeline.ReleaseID,
		ProjectID:       &project.Metadata.ID,
		OrganizationID:  &project.OrgID,
		EnvironmentName: pipeline.EnvironmentName,
		Tags:            tags,
		Value:           1,
	}); err != nil {
		return errors.Wrap(err, "failed to create %s metric", models.MetricPipelineCompleted)
	}

	// Create metric for pipeline duration
	deploymentTime := currentTime.Sub(*pipeline.Timestamps.StartedTimestamp).Seconds()

	if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
		Name:            models.MetricPipelineDuration,
		PipelineID:      &pipeline.Metadata.ID,
		ReleaseID:       pipeline.ReleaseID,
		ProjectID:       &project.Metadata.ID,
		OrganizationID:  &project.OrgID,
		EnvironmentName: pipeline.EnvironmentName,
		Tags:            tags,
		Value:           deploymentTime,
	}); err != nil {
		return errors.Wrap(err, "failed to create %s metric", models.MetricPipelineDuration)
	}

	// Create metric for pipeline lead time
	pipelineLeadTimeStartTimestamp := pipeline.Metadata.CreationTimestamp

	// Check if this pipeline has any superseded pipelines
	if pipeline.ParentPipelineID != nil {
		// Find any superseded pipelines
		sort := db.PipelineSortableFieldCreatedAtAsc
		supersededPipelines, err := m.dbClient.Pipelines.GetPipelines(ctx, &db.GetPipelinesInput{
			Sort: &sort,
			Filter: &db.PipelineFilter{
				ParentPipelineID:             pipeline.ParentPipelineID,
				ParentNestedPipelineNodePath: pipeline.ParentPipelineNodePath,
				Superseded:                   ptr.Bool(true),
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
		})
		if err != nil {
			return err
		}

		if len(supersededPipelines.Pipelines) > 0 {
			pipelineLeadTimeStartTimestamp = supersededPipelines.Pipelines[0].Metadata.CreationTimestamp
		}
	}

	// Create metric for pipeline lead time
	if _, err := m.dbClient.Metrics.CreateMetric(ctx, &models.Metric{
		Name:            models.MetricPipelineLeadTime,
		PipelineID:      &pipeline.Metadata.ID,
		ReleaseID:       pipeline.ReleaseID,
		ProjectID:       &pipeline.ProjectID,
		OrganizationID:  &project.OrgID,
		EnvironmentName: pipeline.EnvironmentName,
		Tags:            tags,
		Value:           currentTime.Sub(*pipelineLeadTimeStartTimestamp).Seconds(),
	}); err != nil {
		return errors.Wrap(err, "failed to create %s metric", models.MetricPipelineLeadTime)
	}

	return nil
}

// getProjectByID returns a project by its ID.
func (m *MetricManager) getProjectByID(ctx context.Context, projectID string) (*models.Project, error) {
	project, err := m.dbClient.Projects.GetProjectByID(ctx, projectID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project")
	}

	if project == nil {
		return nil, errors.New("project with id %s not found", projectID)
	}

	return project, nil
}

// getPipelineByID returns the pipeline by ID.
func (m *MetricManager) getPipelineByID(ctx context.Context, pipelineID string) (*models.Pipeline, error) {
	pipeline, err := m.dbClient.Pipelines.GetPipelineByID(ctx, pipelineID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline")
	}

	if pipeline == nil {
		return nil, errors.New("pipeline with id %s not found", pipelineID)
	}

	return pipeline, nil
}
