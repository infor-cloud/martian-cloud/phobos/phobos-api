package eventhandlers

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/email"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/email/builder"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	pipelinesvc "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewToDoItemManager(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockPipelineUpdater := pipelinesvc.NewMockUpdater(t)
	mockEmailClient := email.NewMockClient(t)

	expect := &ToDoItemManager{
		logger:          logger,
		dbClient:        dbClient,
		pipelineUpdater: mockPipelineUpdater,
		emailClient:     mockEmailClient,
	}

	assert.Equal(t, expect, NewToDoItemManager(logger, dbClient, mockPipelineUpdater, mockEmailClient))
}

func TestToDoItemManager_RegisterHandlers(t *testing.T) {
	mockPipelineUpdater := pipelinesvc.NewMockUpdater(t)

	mockPipelineUpdater.On("RegisterEventHandler", mock.MatchedBy(func(handler pipelinesvc.EventHandler) bool {
		return handler != nil
	}))

	manager := &ToDoItemManager{
		pipelineUpdater: mockPipelineUpdater,
	}

	manager.RegisterHandlers()
}

func TestToDoItemManager_handleTaskApprovalPendingStatus(t *testing.T) {
	orgID := "org-1"
	approvalRuleIDs := []string{"approval-rule-1", "approval-rule-2"}
	validTaskPath := "pipeline.stage.s1.task.t1"

	pipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID:  "pipeline-1",
			PRN: models.PipelineResource.BuildPRN("test-org", "test-project", "pipeline-1"),
		},
		ProjectID: "project-1",
		Status:    statemachine.RunningNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.PendingNodeStatus,
				Tasks: []*models.PipelineTask{
					{
						Name:            "t1",
						Path:            validTaskPath,
						Status:          statemachine.ApprovalPendingNodeStatus,
						ApprovalStatus:  models.ApprovalNotRequired,
						ApprovalRuleIDs: approvalRuleIDs,
					},
				},
			},
		},
	}

	approvalRules := []*models.ApprovalRule{
		{
			Metadata: models.ResourceMetadata{
				ID: approvalRuleIDs[0],
			},
			OrgID:   orgID,
			UserIDs: []string{"user-1"},
			TeamIDs: []string{"team-1"},
		},
		{
			Metadata: models.ResourceMetadata{
				ID: approvalRuleIDs[1],
			},
			OrgID:   orgID,
			UserIDs: []string{"user-2"},
			TeamIDs: []string{"team-2"},
		},
	}

	createPayload := func(p any) []byte {
		buf, err := json.Marshal(p)
		require.NoError(t, err)
		return buf
	}

	type testCase struct {
		name            string
		statusChange    statemachine.NodeStatusChange
		expectErrorCode errors.CodeType
		todoItemExists  bool
	}

	testCases := []testCase{
		{
			name: "should create a new todo item when task goes into approval pending state from initializing state",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.InitializingNodeStatus,
				NewStatus: statemachine.ApprovalPendingNodeStatus,
				NodePath:  validTaskPath,
				NodeType:  statemachine.TaskNodeType,
			},
		},
		{
			name: "should un-resolve existing todo item when task goes into approval pending state",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.InitializingNodeStatus,
				NewStatus: statemachine.ApprovalPendingNodeStatus,
				NodePath:  validTaskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			todoItemExists: true,
		},
		{
			name: "should resolve todo item when task goes into ready state from approval pending",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.ApprovalPendingNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  validTaskPath,
				NodeType:  statemachine.TaskNodeType,
			},
		},
		{
			name: "should skip non task status changes",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline.stage.s1",
				NodeType:  statemachine.StageNodeType,
			},
		},
		{
			name: "task not found",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.ApprovalPendingNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline.stage.s1.task.unknown",
				NodeType:  statemachine.TaskNodeType,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockToDoItems := db.NewMockToDoItems(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelines := db.NewMockPipelines(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)
			mockUsers := db.NewMockUsers(t)
			mockTeams := db.NewMockTeams(t)

			mockEmailClient := email.NewMockClient(t)

			if test.statusChange.NodePath == validTaskPath {
				todoItem := &models.ToDoItem{
					UserIDs:        append(approvalRules[0].UserIDs, approvalRules[1].UserIDs...),
					TeamIDs:        append(approvalRules[0].TeamIDs, approvalRules[1].TeamIDs...),
					ProjectID:      &pipeline.ProjectID,
					OrganizationID: orgID,
					TargetID:       pipeline.Metadata.ID,
					TargetType:     models.TaskApprovalTarget,
					Payload: createPayload(&models.ToDoItemPipelineTaskApprovalPayload{
						TaskPath: validTaskPath,
					}),
				}

				if test.statusChange.NewStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
					mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
						Filter: &db.ApprovalRuleFilter{
							ApprovalRuleIDs: approvalRuleIDs,
						},
					}).Return(&db.ApprovalRulesResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: int32(len(approvalRules)),
						},
						ApprovalRules: approvalRules,
					}, nil)

					mockUsers.On("GetUsers", mock.Anything, &db.GetUsersInput{
						Filter: &db.UserFilter{
							UserIDs: todoItem.UserIDs,
						},
					}).Return(&db.UsersResult{
						Users: []models.User{},
					}, nil).Maybe()

					mockTeams.On("GetTeams", mock.Anything, &db.GetTeamsInput{
						Filter: &db.TeamFilter{
							TeamIDs: todoItem.TeamIDs,
						},
					}).Return(&db.TeamsResult{
						Teams: []models.Team{},
					}, nil).Maybe()

					mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
						Filter: &db.ToDoItemFilter{
							PipelineTargetID: &pipeline.Metadata.ID,
							TargetTypes: []models.ToDoItemTargetType{
								models.TaskApprovalTarget,
							},
							PayloadFilter: &db.ToDoItemPayloadFilter{
								PipelineTaskPath: &validTaskPath,
							},
						},
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(1),
						},
					}).Return(func(_ context.Context, _ *db.GetToDoItemsInput) (*db.ToDoItemsResult, error) {
						dbResult := &db.ToDoItemsResult{
							PageInfo: &pagination.PageInfo{
								TotalCount: 0,
							},
							ToDoItems: []*models.ToDoItem{},
						}

						if test.todoItemExists {
							dbResult.PageInfo.TotalCount++
							dbResult.ToDoItems = append(dbResult.ToDoItems, todoItem)
						}

						return dbResult, nil
					})

					if test.todoItemExists {
						mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(t *models.ToDoItem) bool {
							return t.AutoResolved == false
						})).Return(todoItem, nil)
					}

					mockProjects.On("GetProjectByID", mock.Anything, pipeline.ProjectID).Return(&models.Project{
						Metadata: models.ResourceMetadata{
							ID: pipeline.ProjectID,
						},
						OrgID: orgID,
					}, nil).Maybe()

					approvalType := models.ApprovalTypeTask
					mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
						Filter: &db.PipelineApprovalFilter{
							PipelineID: &pipeline.Metadata.ID,
							Type:       &approvalType,
							TaskPath:   &validTaskPath,
						},
					}).Return(&db.PipelineApprovalsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
						PipelineApprovals: []*models.PipelineApproval{
							{
								UserID:     &approvalRules[0].UserIDs[0],
								PipelineID: pipeline.Metadata.ID,
								Type:       models.ApprovalTypeTask,
							},
						},
					}, nil).Maybe()

					mockToDoItems.On("CreateToDoItem",
						mock.Anything,
						mock.MatchedBy(func(i *models.ToDoItem) bool {
							return i.ProjectID != nil &&
								*i.ProjectID == pipeline.ProjectID &&
								i.OrganizationID == orgID &&
								i.TargetID == pipeline.Metadata.ID &&
								i.TargetType == models.TaskApprovalTarget &&
								assert.ElementsMatch(t, i.UserIDs, todoItem.UserIDs) &&
								assert.ElementsMatch(t, i.TeamIDs, todoItem.TeamIDs) &&
								len(i.Payload) > 0
						}),
					).Return(todoItem, nil).Maybe()

					mockEmailClient.On("SendMail", mock.Anything, mock.Anything).Maybe()
				}

				if test.statusChange.OldStatus.Equals(statemachine.ApprovalPendingNodeStatus) &&
					(test.statusChange.NewStatus.Equals(statemachine.ReadyNodeStatus) || test.statusChange.NewStatus.Equals(statemachine.CanceledNodeStatus)) {
					mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
						Filter: &db.ToDoItemFilter{
							PipelineTargetID: &pipeline.Metadata.ID,
							Resolved:         ptr.Bool(false),
							TargetTypes: []models.ToDoItemTargetType{
								models.TaskApprovalTarget,
							},
							PayloadFilter: &db.ToDoItemPayloadFilter{
								PipelineTaskPath: &validTaskPath,
							},
						},
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(1),
						},
					}).Return(&db.ToDoItemsResult{
						ToDoItems: []*models.ToDoItem{todoItem},
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
					}, nil)

					mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(item *models.ToDoItem) bool {
						return item.AutoResolved == true
					})).Return(todoItem, nil)

					mockEmailClient.On("SendMail", mock.Anything, mock.Anything).Maybe()
				}

				mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(pipeline, nil).Maybe()

				dbClient := &db.Client{
					ToDoItems:         mockToDoItems,
					ApprovalRules:     mockApprovalRules,
					Pipelines:         mockPipelines,
					Projects:          mockProjects,
					PipelineApprovals: mockPipelineApprovals,
					Users:             mockUsers,
					Teams:             mockTeams,
				}

				manager := &ToDoItemManager{
					dbClient:    dbClient,
					emailClient: mockEmailClient,
				}

				err := manager.handleTaskApprovalPendingStatus(ctx, pipeline.Metadata.ID, []pipelinesvc.StatusChange{{
					PipelineID:        pipeline.Metadata.ID,
					NodeStatusChanges: []statemachine.NodeStatusChange{test.statusChange},
				}}, nil)

				if test.expectErrorCode != "" {
					assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
					return
				}

				require.NoError(t, err)
			}
		})
	}
}

func TestToDoItemManager_handleNestedPipelinePendingStatus(t *testing.T) {
	orgID := "org-1"
	approvalRuleIDs := []string{"approval-rule-1", "approval-rule-2"}
	validNestedPipelinePath := "pipeline.stage.s1.pipeline.p1"
	nestedLatestPipelineID := "nested-latest-pipeline-1"

	parentPipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID:  "pipeline-1",
			PRN: models.PipelineResource.BuildPRN("test-org", "test-project", "pipeline-1"),
		},
		ProjectID: "project-1",
		Status:    statemachine.RunningNodeStatus,
		Stages: []*models.PipelineStage{
			{
				Name:   "s1",
				Path:   "pipeline.stage.s1",
				Status: statemachine.PendingNodeStatus,
				NestedPipelines: []*models.NestedPipeline{
					{
						Name:             "t1",
						Path:             validNestedPipelinePath,
						Status:           statemachine.ApprovalPendingNodeStatus,
						LatestPipelineID: nestedLatestPipelineID,
					},
				},
			},
		},
	}

	approvalRules := []*models.ApprovalRule{
		{
			Metadata: models.ResourceMetadata{
				ID: approvalRuleIDs[0],
			},
			OrgID:   orgID,
			UserIDs: []string{"user-1"},
			TeamIDs: []string{"team-1"},
		},
		{
			Metadata: models.ResourceMetadata{
				ID: approvalRuleIDs[1],
			},
			OrgID:   orgID,
			UserIDs: []string{"user-2"},
			TeamIDs: []string{"team-2"},
		},
	}

	type testCase struct {
		name            string
		statusChange    statemachine.NodeStatusChange
		expectErrorCode errors.CodeType
		todoItemExists  bool
	}

	testCases := []testCase{
		{
			name: "should create new todo item when nested pipeline goes into approval pending state from initializing state",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.InitializingNodeStatus,
				NewStatus: statemachine.ApprovalPendingNodeStatus,
				NodePath:  validNestedPipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
		},
		{
			name: "should un-resolve todo item when nested pipeline goes back into approval pending state from initializing state",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.InitializingNodeStatus,
				NewStatus: statemachine.ApprovalPendingNodeStatus,
				NodePath:  validNestedPipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			todoItemExists: true,
		},
		{
			name: "should resolve todo item when nested pipeline goes into ready state from approval pending",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.ApprovalPendingNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  validNestedPipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
		},
		{
			name: "should skip non nested pipeline status changes",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline.stage.s1",
				NodeType:  statemachine.StageNodeType,
			},
		},
		{
			name: "should skip top-level pipelines",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.RunningNodeStatus,
				NodePath:  "pipeline",
				NodeType:  statemachine.PipelineNodeType,
			},
		},
		{
			name: "nested pipeline not found",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.ApprovalPendingNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline.stage.s1.pipeline.unknown",
				NodeType:  statemachine.PipelineNodeType,
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockToDoItems := db.NewMockToDoItems(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelines := db.NewMockPipelines(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)
			mockUsers := db.NewMockUsers(t)
			mockTeams := db.NewMockTeams(t)

			mockEmailClient := email.NewMockClient(t)

			if test.statusChange.NodePath == validNestedPipelinePath {
				todoItem := &models.ToDoItem{
					UserIDs:        append(approvalRules[0].UserIDs, approvalRules[1].UserIDs...),
					TeamIDs:        append(approvalRules[0].TeamIDs, approvalRules[1].TeamIDs...),
					ProjectID:      &parentPipeline.ProjectID,
					OrganizationID: orgID,
					TargetID:       nestedLatestPipelineID,
					TargetType:     models.PipelineApprovalTarget,
				}

				if test.statusChange.NewStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
					mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
						Filter: &db.ApprovalRuleFilter{
							ApprovalRuleIDs: approvalRuleIDs,
						},
					}).Return(&db.ApprovalRulesResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: int32(len(approvalRules)),
						},
						ApprovalRules: approvalRules,
					}, nil)

					mockUsers.On("GetUsers", mock.Anything, &db.GetUsersInput{
						Filter: &db.UserFilter{
							UserIDs: todoItem.UserIDs,
						},
					}).Return(&db.UsersResult{
						Users: []models.User{},
					}, nil).Maybe()

					mockTeams.On("GetTeams", mock.Anything, &db.GetTeamsInput{
						Filter: &db.TeamFilter{
							TeamIDs: todoItem.TeamIDs,
						},
					}).Return(&db.TeamsResult{
						Teams: []models.Team{},
					}, nil).Maybe()

					mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
						Filter: &db.ToDoItemFilter{
							PipelineTargetID: &nestedLatestPipelineID,
							TargetTypes: []models.ToDoItemTargetType{
								models.PipelineApprovalTarget,
							},
						},
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(1),
						},
					}).Return(func(_ context.Context, _ *db.GetToDoItemsInput) (*db.ToDoItemsResult, error) {
						dbResult := &db.ToDoItemsResult{
							PageInfo: &pagination.PageInfo{
								TotalCount: 0,
							},
							ToDoItems: []*models.ToDoItem{},
						}

						if test.todoItemExists {
							dbResult.PageInfo.TotalCount++
							dbResult.ToDoItems = append(dbResult.ToDoItems, todoItem)
						}

						return dbResult, nil
					})

					if test.todoItemExists {
						mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(t *models.ToDoItem) bool {
							return t.AutoResolved == false
						})).Return(todoItem, nil)
					}

					mockProjects.On("GetProjectByID", mock.Anything, parentPipeline.ProjectID).Return(&models.Project{
						Metadata: models.ResourceMetadata{
							ID: parentPipeline.ProjectID,
						},
						OrgID: orgID,
					}, nil).Maybe()

					approvalType := models.ApprovalTypePipeline
					mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
						Filter: &db.PipelineApprovalFilter{
							PipelineID: &nestedLatestPipelineID,
							Type:       &approvalType,
						},
					}).Return(&db.PipelineApprovalsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
						PipelineApprovals: []*models.PipelineApproval{
							{
								UserID:     &approvalRules[0].UserIDs[0],
								PipelineID: nestedLatestPipelineID,
								Type:       models.ApprovalTypePipeline,
							},
						},
					}, nil).Maybe()

					mockToDoItems.On("CreateToDoItem",
						mock.Anything,
						mock.MatchedBy(func(i *models.ToDoItem) bool {
							return i.ProjectID != nil &&
								*i.ProjectID == parentPipeline.ProjectID &&
								i.OrganizationID == orgID &&
								i.TargetID == nestedLatestPipelineID &&
								i.TargetType == models.PipelineApprovalTarget &&
								assert.ElementsMatch(t, i.UserIDs, todoItem.UserIDs) &&
								assert.ElementsMatch(t, i.TeamIDs, todoItem.TeamIDs)
						}),
					).Return(todoItem, nil).Maybe()

					mockEmailClient.On("SendMail", mock.Anything, mock.Anything).Maybe()
				}

				if test.statusChange.OldStatus.Equals(statemachine.ApprovalPendingNodeStatus) &&
					(test.statusChange.NewStatus.Equals(statemachine.ReadyNodeStatus) || test.statusChange.NewStatus.Equals(statemachine.CanceledNodeStatus)) {
					mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
						Filter: &db.ToDoItemFilter{
							PipelineTargetID: &nestedLatestPipelineID,
							Resolved:         ptr.Bool(false),
							TargetTypes: []models.ToDoItemTargetType{
								models.PipelineApprovalTarget,
							},
						},
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(1),
						},
					}).Return(&db.ToDoItemsResult{
						ToDoItems: []*models.ToDoItem{todoItem},
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
					}, nil)

					mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(item *models.ToDoItem) bool {
						return item.AutoResolved == true
					})).Return(todoItem, nil)

					mockEmailClient.On("SendMail", mock.Anything, mock.Anything).Maybe()
				}

				mockPipelines.On("GetPipelineByID", mock.Anything, parentPipeline.Metadata.ID).Return(parentPipeline, nil).Maybe()

				// For the calls to look up the approval rules on the pipeline instance.
				mockPipelines.On("GetPipelineByID", mock.Anything, nestedLatestPipelineID).Return(&models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID:  nestedLatestPipelineID,
						PRN: models.PipelineResource.BuildPRN("test-org", "test-project", nestedLatestPipelineID),
					},
					ProjectID:        parentPipeline.ProjectID,
					ParentPipelineID: &nestedLatestPipelineID,
					ApprovalRuleIDs:  approvalRuleIDs,
				}, nil).Maybe()

				dbClient := &db.Client{
					ToDoItems:         mockToDoItems,
					ApprovalRules:     mockApprovalRules,
					Pipelines:         mockPipelines,
					Projects:          mockProjects,
					PipelineApprovals: mockPipelineApprovals,
					Users:             mockUsers,
					Teams:             mockTeams,
				}

				manager := &ToDoItemManager{
					dbClient:    dbClient,
					emailClient: mockEmailClient,
				}

				err := manager.handleNestedPipelineApprovalPendingStatus(ctx, parentPipeline.Metadata.ID, []pipelinesvc.StatusChange{{
					PipelineID:        parentPipeline.Metadata.ID,
					NodeStatusChanges: []statemachine.NodeStatusChange{test.statusChange},
				}}, nil)

				if test.expectErrorCode != "" {
					assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
					return
				}

				require.NoError(t, err)
			}
		})
	}
}

func TestToDoItemManager_sendApprovalRequestEmail(t *testing.T) {
	approvalRules := []*models.ApprovalRule{
		{
			Metadata: models.ResourceMetadata{
				ID: "approval-rule-1",
			},
			Name:              "rule-1",
			UserIDs:           []string{"user-a"},
			ApprovalsRequired: 2,
		},
		{
			Metadata: models.ResourceMetadata{
				ID: "approval-rule-2",
			},
			Name:              "rule-2",
			TeamIDs:           []string{"team-a"},
			ApprovalsRequired: 1,
		},
	}
	approvalRuleIDs := []string{}
	for _, rule := range approvalRules {
		approvalRuleIDs = append(approvalRuleIDs, rule.Metadata.ID)
	}

	release := models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		SemanticVersion: "1.0.0",
	}

	type testCase struct {
		name                    string
		pipeline                *models.Pipeline
		todoItem                *models.ToDoItem
		expectSubject           string
		expectReleaseVersion    string
		expectEligibleApprovers []string
		expectNodePath          *string
		expectApprovalRules     []builder.ApprovalRule
	}

	testCases := []testCase{
		{
			name: "approval request email for nested pipeline",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:  "pipeline-1",
					PRN: models.PipelineResource.BuildPRN("test-org", "test-project", "pipeline-1"),
				},
				Type:                   models.DeploymentPipelineType,
				ParentPipelineNodePath: ptr.String("pipeline.stage.s1.pipeline.p1"),
				EnvironmentName:        ptr.String("staging"),
				ReleaseID:              &release.Metadata.ID,
			},
			todoItem: &models.ToDoItem{
				TargetType: models.PipelineApprovalTarget,
				UserIDs:    []string{"user-1"},
				TeamIDs:    []string{"team-1"},
			},
			expectSubject:           "Pipeline Approval Request",
			expectEligibleApprovers: []string{"user-a", "team-a"},
			expectNodePath:          ptr.String("pipeline.stage.s1.pipeline.p1"),
			expectApprovalRules: []builder.ApprovalRule{
				{Name: "rule-1", Required: 2},
				{Name: "rule-2", Required: 1},
			},
			expectReleaseVersion: release.SemanticVersion,
		},
		{
			name: "approval request email for task",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:  "pipeline-1",
					PRN: models.PipelineResource.BuildPRN("test-org", "test-project", "pipeline-1"),
				},
				Type: models.DeploymentPipelineType,
			},
			todoItem: &models.ToDoItem{
				TargetType: models.TaskApprovalTarget,
				UserIDs:    []string{"user-1"},
				TeamIDs:    []string{"team-1"},
				Payload:    []byte(`{"taskPath":"pipeline.stage.s1.task.t1"}`),
			},
			expectSubject:           "Task Approval Request",
			expectEligibleApprovers: []string{"user-a", "team-a"},
			expectNodePath:          ptr.String("pipeline.stage.s1.task.t1"),
			expectApprovalRules: []builder.ApprovalRule{
				{Name: "rule-1", Required: 2},
				{Name: "rule-2", Required: 1},
			},
		},
		{
			name: "approval request email for top-level pipeline",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:  "pipeline-1",
					PRN: models.PipelineResource.BuildPRN("test-org", "test-project", "pipeline-1"),
				},
				Type: models.RunbookPipelineType,
			},
			todoItem: &models.ToDoItem{
				TargetType: models.PipelineApprovalTarget,
				UserIDs:    []string{"user-1"},
				TeamIDs:    []string{"team-1"},
			},
			expectSubject:           "Pipeline Approval Request",
			expectEligibleApprovers: []string{"user-a", "team-a"},
			expectApprovalRules: []builder.ApprovalRule{
				{Name: "rule-1", Required: 2},
				{Name: "rule-2", Required: 1},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// Mock email client
			mockEmailClient := email.NewMockClient(t)
			// DB Mocks
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockUsers := db.NewMockUsers(t)
			mockTeams := db.NewMockTeams(t)
			mockRelease := db.NewMockReleases(t)

			mockEmailClient.On("SendMail", mock.Anything, &email.SendMailInput{
				UsersIDs:       test.todoItem.UserIDs,
				TeamsIDs:       test.todoItem.TeamIDs,
				ExcludeUserIDs: test.todoItem.ResolvedByUsers,
				Subject:        test.expectSubject,
				Builder: &builder.ApprovalRequestEmail{
					OrgName:           test.pipeline.GetOrgName(),
					ProjectName:       test.pipeline.GetProjectName(),
					ApprovalRules:     test.expectApprovalRules,
					PipelineType:      test.pipeline.Type,
					PipelineID:        gid.ToGlobalID(gid.PipelineType, test.pipeline.Metadata.ID),
					TodoTargetType:    test.todoItem.TargetType,
					NodePath:          test.expectNodePath,
					EligibleApprovers: test.expectEligibleApprovers,
					ReleaseVersion:    test.expectReleaseVersion,
					EnvironmentName:   ptr.ToString(test.pipeline.EnvironmentName),
				},
			})

			mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
				Filter: &db.ApprovalRuleFilter{
					ApprovalRuleIDs: approvalRuleIDs,
				},
			}).Return(&db.ApprovalRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(approvalRuleIDs)),
				},
				ApprovalRules: approvalRules,
			}, nil)

			mockUsers.On("GetUsers", mock.Anything, &db.GetUsersInput{
				Filter: &db.UserFilter{
					UserIDs: test.todoItem.UserIDs,
				},
			}).Return(&db.UsersResult{
				Users: []models.User{{Username: "user-a"}},
			}, nil).Maybe()

			mockTeams.On("GetTeams", mock.Anything, &db.GetTeamsInput{
				Filter: &db.TeamFilter{
					TeamIDs: test.todoItem.TeamIDs,
				},
			}).Return(&db.TeamsResult{
				Teams: []models.Team{{Name: "team-a"}},
			}, nil).Maybe()

			if test.pipeline.ReleaseID != nil {
				mockRelease.On("GetReleaseByID", mock.Anything, *test.pipeline.ReleaseID).Return(&release, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				ApprovalRules: mockApprovalRules,
				Users:         mockUsers,
				Teams:         mockTeams,
				Releases:      mockRelease,
			}

			todoItemManager := &ToDoItemManager{
				dbClient:    dbClient,
				logger:      logger,
				emailClient: mockEmailClient,
			}

			err := todoItemManager.sendApprovalRequestEmail(ctx, approvalRuleIDs, test.pipeline, test.todoItem)
			require.NoError(t, err)
		})
	}
}

func TestToDoItemManager_handleSupersededNestedPipelineToDoItem(t *testing.T) {
	projectID := "project-1"
	organizationID := "organization-1"
	nestedPipelinePath := "pipeline.stage.s1.pipeline.p1"
	newNestedPipelineID := "nested-pipeline-1"
	supersededNestedPipelineID := "superseded-pipeline-1"
	approvalRuleIDs := []string{"approval-rule-1", "approval-rule-2"}

	type testCase struct {
		event                     any
		name                      string
		newPipelineApprovalStatus models.PipelineApprovalStatus
		todoItemExists            bool
	}

	testCases := []testCase{
		{
			name: "expect todo item for superseded pipeline to be resolved and a new todo item created",
			event: &pipelinesvc.UpdateNestedPipelineEvent{
				NestedPipelinePath:   nestedPipelinePath,
				NestedPipelineID:     newNestedPipelineID,
				SupersededPipelineID: supersededNestedPipelineID,
			},
			todoItemExists:            true,
			newPipelineApprovalStatus: models.ApprovalPending,
		},
		{
			name: "no todo item found",
			event: &pipelinesvc.UpdateNestedPipelineEvent{
				NestedPipelinePath:   nestedPipelinePath,
				NestedPipelineID:     newNestedPipelineID,
				SupersededPipelineID: supersededNestedPipelineID,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockToDoItems := db.NewMockToDoItems(t)
			mockPipelines := db.NewMockPipelines(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelineApprovals := db.NewMockPipelineApprovals(t)
			mockApprovalRules := db.NewMockApprovalRules(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, newNestedPipelineID).Return(&models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: newNestedPipelineID,
				},
				ProjectID:       projectID,
				ApprovalStatus:  test.newPipelineApprovalStatus,
				ApprovalRuleIDs: approvalRuleIDs,
				Status:          statemachine.CreatedNodeStatus,
			}, nil)

			if test.newPipelineApprovalStatus != models.ApprovalNotRequired {
				mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(1),
					},
					Filter: &db.ToDoItemFilter{
						PipelineTargetID: &supersededNestedPipelineID,
						Resolved:         ptr.Bool(false),
						TargetTypes: []models.ToDoItemTargetType{
							models.PipelineApprovalTarget,
						},
					},
				}).Return(func(_ context.Context, _ *db.GetToDoItemsInput) (*db.ToDoItemsResult, error) {
					todoItemsResult := &db.ToDoItemsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 0,
						},
						ToDoItems: []*models.ToDoItem{},
					}

					if test.todoItemExists {
						todoItemsResult.PageInfo.TotalCount++
						todoItemsResult.ToDoItems = append(todoItemsResult.ToDoItems, &models.ToDoItem{
							Metadata: models.ResourceMetadata{
								ID: "todo-item-1",
							},
						})
					}

					return todoItemsResult, nil
				})

				if test.todoItemExists {
					mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(t *models.ToDoItem) bool {
						return t.Metadata.ID == "todo-item-1" && t.AutoResolved
					})).Return(nil, nil)
				}

				mockApprovalRules.On("GetApprovalRules", mock.Anything, &db.GetApprovalRulesInput{
					Filter: &db.ApprovalRuleFilter{
						ApprovalRuleIDs: approvalRuleIDs,
					},
				}).Return(&db.ApprovalRulesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: int32(len(approvalRuleIDs)),
					},
					ApprovalRules: []*models.ApprovalRule{
						{
							UserIDs:           []string{"user-1"},
							OrgID:             organizationID,
							ApprovalsRequired: 1,
						},
						{
							TeamIDs:           []string{"team-1"},
							OrgID:             organizationID,
							ApprovalsRequired: 1,
						},
					},
				}, nil)

				mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(&models.Project{
					Metadata: models.ResourceMetadata{
						ID: projectID,
					},
					OrgID: organizationID,
				}, nil)

				approvalType := models.ApprovalTypePipeline
				mockPipelineApprovals.On("GetPipelineApprovals", mock.Anything, &db.GetPipelineApprovalsInput{
					Filter: &db.PipelineApprovalFilter{
						PipelineID: &newNestedPipelineID,
						Type:       &approvalType,
					},
				}).Return(&db.PipelineApprovalsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 0,
					},
					PipelineApprovals: []*models.PipelineApproval{},
				}, nil)

				mockToDoItems.On("CreateToDoItem", mock.Anything, &models.ToDoItem{
					UserIDs:        []string{"user-1"},
					TeamIDs:        []string{"team-1"},
					ProjectID:      &projectID,
					OrganizationID: organizationID,
					TargetID:       newNestedPipelineID,
					TargetType:     models.PipelineApprovalTarget,
				}).Return(nil, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				ToDoItems:         mockToDoItems,
				ApprovalRules:     mockApprovalRules,
				Pipelines:         mockPipelines,
				Projects:          mockProjects,
				PipelineApprovals: mockPipelineApprovals,
			}

			todoItemManager := &ToDoItemManager{
				dbClient: dbClient,
				logger:   logger,
			}

			err := todoItemManager.handleSupersededNestedPipelineToDoItem(ctx, "", nil, test.event)
			require.NoError(t, err)
		})
	}
}
