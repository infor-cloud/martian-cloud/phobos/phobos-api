package eventhandlers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
	ctyjson "github.com/zclconf/go-cty/cty/json"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	pipelinesvc "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const testPipelineHCLDir = "../../../../testdata/hcl/pipeline"

func TestNewInitializationHandler(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockPipelineUpdater := pipelinesvc.NewMockUpdater(t)
	mockPipelineTemplateStore := pipelinetemplate.NewMockDataStore(t)
	mockPipelineDataStore := pipelinesvc.NewMockStore(t)

	expect := &InitializationHandler{
		logger:                logger,
		dbClient:              dbClient,
		pipelineUpdater:       mockPipelineUpdater,
		pipelineTemplateStore: mockPipelineTemplateStore,
		pipelineArtifactStore: mockPipelineDataStore,
	}

	assert.Equal(t, expect, NewInitializationHandler(logger, dbClient, mockPipelineUpdater, mockPipelineTemplateStore, mockPipelineDataStore))
}

func TestInitializationHandler_RegisterHandlers(t *testing.T) {
	mockPipelineUpdater := pipelinesvc.NewMockUpdater(t)

	mockPipelineUpdater.On("RegisterEventHandler", mock.MatchedBy(func(handler pipelinesvc.EventHandler) bool {
		return handler != nil
	})).Times(2)

	manager := &InitializationHandler{
		pipelineUpdater: mockPipelineUpdater,
	}

	manager.RegisterHandlers()
}

func TestInitializationHandler_handleTaskInitializing(t *testing.T) {
	orgID := "org-1"

	parsedID, err := gid.ParseGlobalID("GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ")
	require.NoError(t, err)

	approvalRuleID := parsedID.ID

	rawHCLFile := "valid_dynamic_fields.hcl"

	// T0 has no approval rules.
	taskPathT0 := "pipeline.stage.s1.task.t0"

	// T1 uses just a slice of approval rules.
	taskPathT1 := "pipeline.stage.s1.task.t1"

	// T2 uses a variable to determine the approval rules.
	taskPathT2 := "pipeline.stage.s1.task.t2"

	// T3 uses an action output to determine the approval rules.
	taskPathT3 := "pipeline.stage.s1.task.t3"

	// T4 uses an if expression that evaluates to false.
	taskPathT4 := "pipeline.stage.s1.task.t4"

	// T5 uses an action output to determine the if expression.
	taskPathT5 := "pipeline.stage.s1.task.t5"

	// T6 uses a variable to determine the if expression.
	taskPathT6 := "pipeline.stage.s1.task.t6"

	type testCase struct {
		statusChange          statemachine.NodeStatusChange
		name                  string
		expectErrorCode       errors.CodeType
		expectApprovalRuleIDs []string
		expectSkipped         bool
	}

	testCases := []testCase{
		{
			name: "task with no approval rules",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CanceledNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPathT0,
				NodeType:  statemachine.TaskNodeType,
			},
		},
		{
			name: "task with just a list of approval rules",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPathT1,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name: "task uses a variable in approval rules expression",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPathT2,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name: "task uses action outputs in approval rules expression",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPathT3,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name: "task with if expression that evaluates to false should be skipped",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPathT4,
				NodeType:  statemachine.TaskNodeType,
			},
			expectSkipped: true,
		},
		{
			name: "task uses action outputs in if expression that evaluates to false should be skipped",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPathT5,
				NodeType:  statemachine.TaskNodeType,
			},
			expectSkipped: true,
		},
		{
			name: "task uses variable in if expression that evaluates to true should not be skipped",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPathT6,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{},
		},
		{
			name: "tasks not in initializing state should be ignored",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  taskPathT0,
				NodeType:  statemachine.TaskNodeType,
			},
		},
		{
			name: "non task nodes should be ignored",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline.stage.s1",
				NodeType:  statemachine.StageNodeType,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUpdater := pipelinesvc.NewMockUpdater(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockPipelineArtifactStore := pipelinesvc.NewMockStore(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelineActionOutputs := db.NewMockPipelineActionOutputs(t)
			mockPipelineTemplateStore := pipelinetemplate.NewMockDataStore(t)

			template, err := os.Open(filepath.Join(testPipelineHCLDir, rawHCLFile))
			require.NoErrorf(t, err, "failed to open test HCL file %s", rawHCLFile)
			defer template.Close()

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-1",
				},
				Status:             statemachine.RunningNodeStatus,
				PipelineTemplateID: "pipeline-template-1",
				ProjectID:          "project-1",
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:           "t0",
								Path:           "pipeline.stage.s1.task.t0",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
							},
							{
								Name:           "t1",
								Path:           "pipeline.stage.s1.task.t1",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
							},
							{
								Name:           "t2",
								Path:           "pipeline.stage.s1.task.t2",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t1"},
							},
							{
								Name:           "t3",
								Path:           "pipeline.stage.s1.task.t3",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t2"},
							},
							{
								Name:           "t4",
								Path:           "pipeline.stage.s1.task.t4",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t3"},
							},
							{
								Name:           "t5",
								Path:           "pipeline.stage.s1.task.t5",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t4"},
							},
							{
								Name:           "t6",
								Path:           "pipeline.stage.s1.task.t6",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t5"},
							},
						},
					},
				},
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(pipeline, nil).Maybe()

			buf, err := json.Marshal([]pipelinesvc.Variable{{
				Value:    "true",
				Key:      "require_t2_approval",
				Category: models.HCLVariable,
			}})
			require.Nil(t, err)

			mockPipelineArtifactStore.On("GetPipelineVariables", mock.Anything, pipeline.Metadata.ID).Return(io.NopCloser(bytes.NewReader(buf)), nil).Maybe()

			mockPipelineTemplateStore.On("GetData", mock.Anything, pipeline.PipelineTemplateID).Return(io.NopCloser(template), nil).Maybe()

			mockPipelineActionOutputs.On("GetPipelineActionOutputs", mock.Anything, pipeline.Metadata.ID, mock.Anything).Return(
				func(_ context.Context, pipelineID string, actionPaths []string) ([]*models.PipelineActionOutput, error) {
					result := make([]*models.PipelineActionOutput, len(actionPaths))

					jsonVal, err := ctyjson.Marshal(cty.NumberIntVal(0), cty.Number)
					require.NoError(t, err)

					valType, err := ctyjson.MarshalType(cty.Number)
					require.NoError(t, err)

					for ix, actionPath := range actionPaths {
						result[ix] = &models.PipelineActionOutput{
							Name:       "exit_code",
							PipelineID: pipelineID,
							ActionPath: actionPath,
							Value:      jsonVal,
							Type:       valType,
						}
					}

					return result, nil
				},
			).Maybe()

			mockProjects.On("GetProjectByID", mock.Anything, pipeline.ProjectID).Return(&models.Project{
				Metadata: models.ResourceMetadata{
					ID: pipeline.ProjectID,
				},
				OrgID: orgID,
			}, nil).Maybe()

			mockApprovalRules.On("GetApprovalRuleByID", mock.Anything, approvalRuleID).Return(&models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: approvalRuleID,
				},
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				Name:              "martian",
				UserIDs:           []string{"user-1"},
				ApprovalsRequired: 1,
			}, nil).Maybe()

			if test.expectErrorCode == "" {
				mockUpdater.On("ProcessEvent", mock.Anything, pipeline.Metadata.ID, &pipelinesvc.InitializeTaskEvent{
					TaskPath:        test.statusChange.NodePath,
					ApprovalRuleIDs: test.expectApprovalRuleIDs,
					MarkAsSkipped:   test.expectSkipped,
				}).Return(nil, nil).Maybe()
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Projects:              mockProjects,
				Pipelines:             mockPipelines,
				ApprovalRules:         mockApprovalRules,
				PipelineActionOutputs: mockPipelineActionOutputs,
			}

			initializer := &InitializationHandler{
				logger:                logger,
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				pipelineTemplateStore: mockPipelineTemplateStore,
				pipelineArtifactStore: mockPipelineArtifactStore,
			}

			statusChanges := []pipelinesvc.StatusChange{
				{
					PipelineID:        pipeline.Metadata.ID,
					NodeStatusChanges: []statemachine.NodeStatusChange{test.statusChange},
				},
			}

			initializerError := initializer.handleTaskInitializing(ctx, "", statusChanges, nil)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(initializerError))
				return
			}

			require.NoError(t, initializerError)
		})
	}
}

func TestInitializationHandler_handlePipelineInitializing(t *testing.T) {
	orgID := "org-1"
	latestPipelineID := "latest-pipeline-1"

	parsedID, err := gid.ParseGlobalID("GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ")
	require.NoError(t, err)

	approvalRuleID := parsedID.ID

	rawHCLFile := "valid_dynamic_fields.hcl"

	// P0 has no approval rules.
	pipelinePathP0 := "pipeline.stage.s1.pipeline.p0"

	// P1 uses just a slice of approval rules.
	pipelinePathP1 := "pipeline.stage.s1.pipeline.p1"

	// P2 uses a variable to determine the approval rules.
	pipelinePathP2 := "pipeline.stage.s1.pipeline.p2"

	// P3 uses an action output to determine the approval rules.
	pipelinePathP3 := "pipeline.stage.s1.pipeline.p3"

	// P4 uses an if expression that evaluates to false.
	pipelinePathP4 := "pipeline.stage.s1.pipeline.p4"

	// P5 uses an action output to determine the if expression.
	pipelinePathP5 := "pipeline.stage.s1.pipeline.p5"

	// P6 uses a variable to determine the if expression.
	pipelinePathP6 := "pipeline.stage.s1.pipeline.p6"

	type testCase struct {
		statusChange          statemachine.NodeStatusChange
		name                  string
		expectErrorCode       errors.CodeType
		expectApprovalRuleIDs []string
		expectSkipped         bool
	}

	testCases := []testCase{
		{
			name: "pipeline with no approval rules",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CanceledNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePathP0,
				NodeType:  statemachine.PipelineNodeType,
			},
		},
		{
			name: "pipeline with just a list of approval rules",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePathP1,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name: "pipeline uses a variable in approval rules expression",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePathP2,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name: "pipeline uses action outputs in approval rules expression",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePathP3,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name: "pipeline with if expression that evaluates to false should be skipped",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePathP4,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectSkipped: true,
		},
		{
			name: "pipeline uses action outputs in if expression that evaluates to false should be skipped",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePathP5,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectSkipped: true,
		},
		{
			name: "pipeline uses variable in if expression that evaluates to true should not be skipped",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePathP6,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{},
		},
		{
			name: "pipelines not in initializing state should be ignored",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  pipelinePathP0,
				NodeType:  statemachine.PipelineNodeType,
			},
		},
		{
			name: "non pipeline nodes should be ignored",
			statusChange: statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline.stage.s1",
				NodeType:  statemachine.StageNodeType,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUpdater := pipelinesvc.NewMockUpdater(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockPipelineArtifactStore := pipelinesvc.NewMockStore(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelineActionOutputs := db.NewMockPipelineActionOutputs(t)
			mockPipelineTemplateStore := pipelinetemplate.NewMockDataStore(t)

			template, err := os.Open(filepath.Join(testPipelineHCLDir, rawHCLFile))
			require.NoErrorf(t, err, "failed to open test HCL file %s", rawHCLFile)
			defer template.Close()

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-1",
				},
				Status:             statemachine.RunningNodeStatus,
				PipelineTemplateID: "pipeline-template-1",
				ProjectID:          "project-1",
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:           "t0",
								Path:           "pipeline.stage.s1.task.t0",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
							},
							{
								Name:           "t1",
								Path:           "pipeline.stage.s1.task.t1",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
							},
							{
								Name:           "t2",
								Path:           "pipeline.stage.s1.task.t2",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t1"},
							},
							{
								Name:           "t3",
								Path:           "pipeline.stage.s1.task.t3",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t2"},
							},
						},
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "p0",
								Path:             "pipeline.stage.s1.pipeline.p0",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
							},
							{
								Name:             "p1",
								Path:             "pipeline.stage.s1.pipeline.p1",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
							},
							{
								Name:             "p2",
								Path:             "pipeline.stage.s1.pipeline.p2",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
							},
							{
								Name:             "p3",
								Path:             "pipeline.stage.s1.pipeline.p3",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
								Dependencies:     []string{"t3"},
							},
							{
								Name:             "p4",
								Path:             "pipeline.stage.s1.pipeline.p4",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
								Dependencies:     []string{"t3"},
							},
							{
								Name:             "p5",
								Path:             "pipeline.stage.s1.pipeline.p5",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
								Dependencies:     []string{"t4"},
							},
							{
								Name:             "p6",
								Path:             "pipeline.stage.s1.pipeline.p6",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
								Dependencies:     []string{"t5"},
							},
						},
					},
				},
			}
			mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(pipeline, nil).Maybe()

			buf, err := json.Marshal([]pipelinesvc.Variable{{
				Value:    "true",
				Key:      "require_p2_approval",
				Category: models.HCLVariable,
			}})
			require.Nil(t, err)

			mockPipelineArtifactStore.On("GetPipelineVariables", mock.Anything, pipeline.Metadata.ID).Return(io.NopCloser(bytes.NewReader(buf)), nil).Maybe()

			mockPipelineTemplateStore.On("GetData", mock.Anything, pipeline.PipelineTemplateID).Return(io.NopCloser(template), nil).Maybe()

			mockPipelineActionOutputs.On("GetPipelineActionOutputs", mock.Anything, pipeline.Metadata.ID, mock.Anything).Return(
				func(_ context.Context, pipelineID string, actionPaths []string) ([]*models.PipelineActionOutput, error) {
					result := make([]*models.PipelineActionOutput, len(actionPaths))

					jsonVal, err := ctyjson.Marshal(cty.NumberIntVal(0), cty.Number)
					require.NoError(t, err)

					valType, err := ctyjson.MarshalType(cty.Number)
					require.NoError(t, err)

					for ix, actionPath := range actionPaths {
						result[ix] = &models.PipelineActionOutput{
							Name:       "exit_code",
							PipelineID: pipelineID,
							ActionPath: actionPath,
							Value:      jsonVal,
							Type:       valType,
						}
					}

					return result, nil
				},
			).Maybe()

			mockProjects.On("GetProjectByID", mock.Anything, pipeline.ProjectID).Return(&models.Project{
				Metadata: models.ResourceMetadata{
					ID: pipeline.ProjectID,
				},
				OrgID: orgID,
			}, nil).Maybe()

			mockApprovalRules.On("GetApprovalRuleByID", mock.Anything, approvalRuleID).Return(&models.ApprovalRule{
				Metadata: models.ResourceMetadata{
					ID: approvalRuleID,
				},
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				Name:              "martian",
				UserIDs:           []string{"user-1"},
				ApprovalsRequired: 1,
			}, nil).Maybe()

			mockUpdater.On("ProcessEvent", mock.Anything, pipeline.Metadata.ID, &pipelinesvc.InitializeNestedPipelineEvent{
				NestedPipelinePath: test.statusChange.NodePath,
				Error:              nil,
				ApprovalRuleIDs:    test.expectApprovalRuleIDs,
				MarkAsSkipped:      test.expectSkipped,
			}).Return(nil, nil).Maybe()

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Projects:              mockProjects,
				Pipelines:             mockPipelines,
				ApprovalRules:         mockApprovalRules,
				PipelineActionOutputs: mockPipelineActionOutputs,
			}

			initializer := &InitializationHandler{
				logger:                logger,
				dbClient:              dbClient,
				pipelineUpdater:       mockUpdater,
				pipelineTemplateStore: mockPipelineTemplateStore,
				pipelineArtifactStore: mockPipelineArtifactStore,
			}

			statusChanges := []pipelinesvc.StatusChange{
				{
					PipelineID:        pipeline.Metadata.ID,
					NodeStatusChanges: []statemachine.NodeStatusChange{test.statusChange},
				},
			}

			initializerError := initializer.handlePipelineInitializing(ctx, "", statusChanges, nil)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(initializerError))
				return
			}

			require.NoError(t, initializerError)
		})
	}
}

func TestInitializationHandler_evaluateExpressions(t *testing.T) {
	orgID := "org-1"
	latestPipelineID := "latest-pipeline-1"

	parsedID, err := gid.ParseGlobalID("GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ")
	require.NoError(t, err)

	approvalRuleGID := parsedID.String()
	approvalRuleID := parsedID.ID
	taskPath := "pipeline.stage.s1.task.t2"
	pipelinePath := "pipeline.stage.s1.pipeline.p1"

	type testCase struct {
		statusChange          *statemachine.NodeStatusChange
		name                  string
		taskIfExpr            string
		taskApprovalExpr      string
		pipelineIfExpr        string
		pipelineApprovalExpr  string
		expectErrorCode       errors.CodeType
		expectApprovalRuleIDs []string
		expectSkipped         bool
	}

	testCases := []testCase{
		{
			name: "task has no approval rule or if expression",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{},
		},
		{
			name:             "task has approval rule list only",
			taskApprovalExpr: fmt.Sprintf("approval_rules = [\"%s\"]", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name:             "task uses variable in approval rule expression",
			taskApprovalExpr: fmt.Sprintf("approval_rules = var.require_approval ? [\"%s\"]: []", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name:             "task approval rule is not a valid GID or PRN",
			taskApprovalExpr: fmt.Sprintf("approval_rules = [\"%s\"]", "invalid"),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:             "task uses action output in approval rule expression",
			taskApprovalExpr: fmt.Sprintf("approval_rules = action_outputs.stage.s1.task.t1.action.exec_command.exit_code == 0 ? [\"%s\"]: []", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name:       "task uses if expression that evaluates to false",
			taskIfExpr: "if = false",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectSkipped: true,
		},
		{
			name:       "task uses action output in if expression that evaluates to false",
			taskIfExpr: "if = action_outputs.stage.s1.task.t1.action.exec_command.exit_code == 1",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectSkipped: true,
		},
		{
			name:             "task uses variable in if expression that evaluates to true",
			taskIfExpr:       "if = var.require_approval",
			taskApprovalExpr: fmt.Sprintf("approval_rules = [\"%s\"]", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name: "pipeline has no approval rule or if expression",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{},
		},
		{
			name:                 "pipeline has approval rule list only",
			pipelineApprovalExpr: fmt.Sprintf("approval_rules = [\"%s\"]", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name:                 "pipeline uses variable in approval rule expression",
			pipelineApprovalExpr: fmt.Sprintf("approval_rules = var.require_approval ? [\"%s\"]: []", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name:                 "pipeline uses action output in approval rule expression",
			pipelineApprovalExpr: fmt.Sprintf("approval_rules = action_outputs.stage.s1.task.t1.action.exec_command.exit_code == 0 ? [\"%s\"]: []", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name:           "pipeline uses if expression that evaluates to false",
			pipelineIfExpr: "if = false",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectSkipped: true,
		},
		{
			name:           "pipeline uses action output in if expression that evaluates to false",
			pipelineIfExpr: "if = action_outputs.stage.s1.task.t1.action.exec_command.exit_code == 1",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectSkipped: true,
		},
		{
			name:                 "pipeline uses variable in if expression that evaluates to true",
			pipelineIfExpr:       "if = var.require_approval",
			pipelineApprovalExpr: fmt.Sprintf("approval_rules = [\"%s\"]", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{approvalRuleID},
		},
		{
			name:           "pipeline uses 'null' if expression value",
			pipelineIfExpr: "if = null",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{},
		},
		{
			name:           "pipeline uses if expression that doesn't evaluate to a boolean",
			pipelineIfExpr: "if = {}",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:                 "pipeline uses 'null' approval expression value",
			pipelineApprovalExpr: "approval_rules = null",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectApprovalRuleIDs: []string{},
		},
		{
			name: "node type not supported",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  "pipeline.stage.s1",
				NodeType:  statemachine.StageNodeType,
			},
			expectErrorCode: errors.EInternal,
		},
		{
			name:                 "expression doesn't evaluate to a list",
			taskApprovalExpr:     "approval_rules = {}",
			pipelineApprovalExpr: "approval_rules = [{}]",
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:                 "unknown output",
			pipelineApprovalExpr: fmt.Sprintf("approval_rules = action_outputs.stage.s1.task.t1.action.exec_command.unknown == 0 ? [\"%s\"]: []", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  pipelinePath,
				NodeType:  statemachine.PipelineNodeType,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:             "unknown variable",
			taskApprovalExpr: fmt.Sprintf("approval_rules = var.unknown ? [\"%s\"]: []", approvalRuleGID),
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.InitializingNodeStatus,
				NodePath:  taskPath,
				NodeType:  statemachine.TaskNodeType,
			},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockPipelineArtifactStore := pipelinesvc.NewMockStore(t)
			mockApprovalRules := db.NewMockApprovalRules(t)
			mockPipelineActionOutputs := db.NewMockPipelineActionOutputs(t)
			mockPipelineTemplateStore := pipelinetemplate.NewMockDataStore(t)

			hclTemplate := fmt.Sprintf(`
			variable require_approval {
				default = false
			}

			plugin exec {}

			stage s1 {
				task t1 {
					action exec_command {
						command = "echo 'running some command'"
					}
				}
				task t2 {
					%s
					%s
					dependencies   = ["t1"]
				}
				pipeline p1 {
					%s
					%s
					dependencies   = ["t1"]
					template_id = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA"
				}
			}`, test.taskApprovalExpr, test.taskIfExpr, test.pipelineApprovalExpr, test.pipelineIfExpr)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: "pipeline-1",
				},
				Status:             statemachine.RunningNodeStatus,
				PipelineTemplateID: "pipeline-template-1",
				ProjectID:          "project-1",
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: statemachine.RunningNodeStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:           "t1",
								Path:           "pipeline.stage.s1.task.t2",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
							},
							{
								Name:           "t2",
								Path:           "pipeline.stage.s1.task.t2",
								Status:         statemachine.InitializingNodeStatus,
								ApprovalStatus: models.ApprovalNotRequired,
								Dependencies:   []string{"t1"},
							},
						},
						NestedPipelines: []*models.NestedPipeline{
							{
								Name:             "p1",
								Path:             "pipeline.stage.s1.pipeline.p1",
								Status:           statemachine.InitializingNodeStatus,
								LatestPipelineID: latestPipelineID,
								ApprovalStatus:   models.ApprovalNotRequired,
								Dependencies:     []string{"t1"},
							},
						},
					},
				},
			}

			buf, err := json.Marshal([]pipelinesvc.Variable{{
				Value:    "true",
				Key:      "require_approval",
				Category: models.HCLVariable,
			}})
			require.Nil(t, err)

			mockPipelineArtifactStore.On("GetPipelineVariables", mock.Anything, pipeline.Metadata.ID).Return(io.NopCloser(bytes.NewReader(buf)), nil)

			mockPipelineTemplateStore.On("GetData", mock.Anything, pipeline.PipelineTemplateID).Return(io.NopCloser(strings.NewReader(hclTemplate)), nil)

			mockPipelineActionOutputs.On("GetPipelineActionOutputs", mock.Anything, pipeline.Metadata.ID, mock.Anything).Return(
				func(_ context.Context, pipelineID string, actionPaths []string) ([]*models.PipelineActionOutput, error) {
					result := make([]*models.PipelineActionOutput, len(actionPaths))

					jsonVal, err := ctyjson.Marshal(cty.NumberIntVal(0), cty.Number)
					require.NoError(t, err)

					valType, err := ctyjson.MarshalType(cty.Number)
					require.NoError(t, err)

					for ix, actionPath := range actionPaths {
						result[ix] = &models.PipelineActionOutput{
							Name:       "exit_code",
							PipelineID: pipelineID,
							ActionPath: actionPath,
							Value:      jsonVal,
							Type:       valType,
						}
					}

					return result, nil
				},
			).Maybe()

			mockProjects.On("GetProjectByID", mock.Anything, pipeline.ProjectID).Return(&models.Project{
				Metadata: models.ResourceMetadata{
					ID: pipeline.ProjectID,
				},
				OrgID: orgID,
			}, nil).Maybe()

			mockApprovalRules.On("GetApprovalRuleByID", mock.Anything, mock.Anything).Return(
				func(_ context.Context, id string) (*models.ApprovalRule, error) {
					if id != approvalRuleID {
						return nil, errors.New("invalid approval rule ID", errors.EInternal)
					}

					return &models.ApprovalRule{
						Metadata: models.ResourceMetadata{
							ID: approvalRuleID,
						},
						Scope:             models.OrganizationScope,
						OrgID:             orgID,
						Name:              "martian",
						UserIDs:           []string{"user-1"},
						ApprovalsRequired: 1,
					}, nil
				},
			).Maybe()

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Projects:              mockProjects,
				Pipelines:             mockPipelines,
				ApprovalRules:         mockApprovalRules,
				PipelineActionOutputs: mockPipelineActionOutputs,
			}

			initializer := &InitializationHandler{
				logger:                logger,
				dbClient:              dbClient,
				pipelineTemplateStore: mockPipelineTemplateStore,
				pipelineArtifactStore: mockPipelineArtifactStore,
			}

			evaluationResult, evalErr := initializer.evaluateExpressions(ctx, pipeline, test.statusChange)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(evalErr))
				return
			}

			require.NoError(t, evalErr)
			require.NotNil(t, evaluationResult)
			assert.ElementsMatch(t, test.expectApprovalRuleIDs, evaluationResult.approvalRuleIDs)
			assert.Equal(t, test.expectSkipped, evaluationResult.markAsSkipped)
		})
	}
}
