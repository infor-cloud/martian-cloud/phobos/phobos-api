package eventhandlers

import (
	"context"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNewMetricManager(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockPipelineUpdater := pipeline.NewMockUpdater(t)

	expect := &MetricManager{
		logger:          logger,
		dbClient:        dbClient,
		pipelineUpdater: mockPipelineUpdater,
	}

	assert.Equal(t, expect, NewMetricManager(logger, dbClient, mockPipelineUpdater))
}

func TestRegisterHandlers(t *testing.T) {
	mockPipelineUpdater := pipeline.NewMockUpdater(t)

	mockPipelineUpdater.On("RegisterEventHandler", mock.MatchedBy(func(handler pipeline.EventHandler) bool {
		return handler != nil
	})).Times(3)

	manager := &MetricManager{
		pipelineUpdater: mockPipelineUpdater,
	}

	manager.RegisterHandlers()
}

func TestMetricManager_createApprovalMetrics(t *testing.T) {
	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: "project-1",
		},
		Name:  "sample-project",
		OrgID: "organization-1",
	}

	release := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		LifecyclePRN: "release-lifecycle-1",
	}

	currentTime := time.Now().UTC()
	startTime := currentTime.Add(5 * -time.Minute)

	type testCase struct {
		name           string
		pipelineStatus statemachine.NodeStatus
		pipelineType   models.PipelineType
		statusChange   *statemachine.NodeStatusChange
		todoItems      []*models.ToDoItem
	}

	testCases := []testCase{
		{
			name:           "runbook approved",
			pipelineStatus: statemachine.ReadyNodeStatus,
			pipelineType:   models.RunbookPipelineType,
			todoItems: []*models.ToDoItem{
				{
					Metadata: models.ResourceMetadata{
						CreationTimestamp: &startTime,
					},
				},
			},
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.ApprovalPendingNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline",
				NodeType:  statemachine.PipelineNodeType,
			},
		},
		{
			name:           "task approved",
			pipelineStatus: statemachine.ReadyNodeStatus,
			pipelineType:   models.RunbookPipelineType,
			todoItems: []*models.ToDoItem{
				{
					Metadata: models.ResourceMetadata{
						CreationTimestamp: &startTime,
					},
				},
			},
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.ApprovalPendingNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline.stage.s1.task.t1",
				NodeType:  statemachine.TaskNodeType,
			},
		},
		{
			name:           "task approval request created",
			pipelineStatus: statemachine.RunningNodeStatus,
			pipelineType:   models.RunbookPipelineType,
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.CreatedNodeStatus,
				NewStatus: statemachine.ApprovalPendingNodeStatus,
				NodePath:  "pipeline.stage.s1.task.t1",
				NodeType:  statemachine.TaskNodeType,
			},
		},
		{
			name:           "todo item not found",
			pipelineStatus: statemachine.ReadyNodeStatus,
			pipelineType:   models.RunbookPipelineType,
			todoItems:      []*models.ToDoItem{},
			statusChange: &statemachine.NodeStatusChange{
				OldStatus: statemachine.ApprovalPendingNodeStatus,
				NewStatus: statemachine.ReadyNodeStatus,
				NodePath:  "pipeline",
				NodeType:  statemachine.PipelineNodeType,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMetrics := db.NewMockMetrics(t)
			mockProjects := db.NewMockProjects(t)
			mockToDoItems := db.NewMockToDoItems(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                "pipeline-1",
					CreationTimestamp: &startTime,
				},
				ProjectID:              project.Metadata.ID,
				ReleaseID:              &release.Metadata.ID,
				ParentPipelineID:       ptr.String("parent-pipeline-1"),
				ParentPipelineNodePath: ptr.String("pipeline.stage.s1.pipeline.p1"),
				EnvironmentName:        ptr.String("testing"),
				Type:                   test.pipelineType,
				Status:                 test.pipelineStatus,
				Stages: []*models.PipelineStage{
					{
						Name:   "s1",
						Path:   "pipeline.stage.s1",
						Status: test.pipelineStatus,
						Tasks: []*models.PipelineTask{
							{
								Name:   "t1",
								Path:   "pipeline.stage.s1.task.t1",
								Status: test.pipelineStatus,
							},
						},
					},
				},
				Timestamps: models.PipelineTimestamps{
					StartedTimestamp:   &startTime,
					CompletedTimestamp: &currentTime,
				},
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(pipeline, nil)

			mockProjects.On("GetProjectByID", mock.Anything, project.Metadata.ID).Return(project, nil).Maybe()
			mockReleases.On("GetReleaseByID", mock.Anything, release.Metadata.ID).Return(release, nil).Maybe()

			if test.todoItems != nil {
				mockToDoItems.On("GetToDoItems", mock.Anything, mock.Anything).Return(&db.ToDoItemsResult{
					ToDoItems: test.todoItems,
				}, nil)
			}

			mockMetrics.On("CreateMetric", mock.Anything, mock.Anything).Return(nil, nil).Maybe()

			dbClient := &db.Client{
				Metrics:   mockMetrics,
				Projects:  mockProjects,
				ToDoItems: mockToDoItems,
				Releases:  mockReleases,
				Pipelines: mockPipelines,
			}

			testLogger, _ := logger.NewForTest()
			metricManager := &MetricManager{
				dbClient: dbClient,
				logger:   testLogger,
			}

			err := metricManager.createApprovalMetrics(ctx, pipeline.Metadata.ID, test.statusChange, currentTime)

			require.NoError(t, err)

			if test.statusChange.NewStatus == statemachine.ApprovalPendingNodeStatus {
				mockMetrics.AssertCalled(t, "CreateMetric", mock.Anything, &models.Metric{
					Name:            models.MetricApprovalRequestCreated,
					PipelineID:      &pipeline.Metadata.ID,
					ReleaseID:       pipeline.ReleaseID,
					ProjectID:       &pipeline.ProjectID,
					OrganizationID:  &project.OrgID,
					EnvironmentName: pipeline.EnvironmentName,
					Value:           1,
					Tags: map[models.MetricTagName]string{
						models.MetricTagNamePipelineType:       string(pipeline.Type),
						models.MetricTagNameReleaseLifecycle:   release.LifecyclePRN,
						models.MetricTagNameApprovalTargetType: string(test.statusChange.NodeType),
					},
				})
			} else if len(test.todoItems) > 0 {
				mockMetrics.AssertCalled(t, "CreateMetric", mock.Anything, &models.Metric{
					Name:            models.MetricApprovalRequestWaitTime,
					PipelineID:      &pipeline.Metadata.ID,
					ReleaseID:       pipeline.ReleaseID,
					ProjectID:       &pipeline.ProjectID,
					OrganizationID:  &project.OrgID,
					EnvironmentName: pipeline.EnvironmentName,
					Value:           currentTime.Sub(startTime).Seconds(),
					Tags: map[models.MetricTagName]string{
						models.MetricTagNamePipelineType:       string(pipeline.Type),
						models.MetricTagNameReleaseLifecycle:   release.LifecyclePRN,
						models.MetricTagNameApprovalTargetType: string(test.statusChange.NodeType),
					},
				})
				mockMetrics.AssertCalled(t, "CreateMetric", mock.Anything, &models.Metric{
					Name:            models.MetricApprovalRequestCompleted,
					PipelineID:      &pipeline.Metadata.ID,
					ReleaseID:       pipeline.ReleaseID,
					ProjectID:       &pipeline.ProjectID,
					OrganizationID:  &project.OrgID,
					EnvironmentName: pipeline.EnvironmentName,
					Value:           1,
					Tags: map[models.MetricTagName]string{
						models.MetricTagNamePipelineType:       string(pipeline.Type),
						models.MetricTagNameReleaseLifecycle:   release.LifecyclePRN,
						models.MetricTagNameApprovalTargetType: string(test.statusChange.NodeType),
					},
				})
			}
		})
	}
}

func TestMetricManager_createDeploymentUpdatedMetric(t *testing.T) {
	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: "project-1",
		},
		Name:  "sample-project",
		OrgID: "organization-1",
	}

	release := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		LifecyclePRN: "release-lifecycle-1",
	}

	supersededPipelineID := "superseded-pipeline-1"

	currentTime := time.Now().UTC()
	startTime := currentTime.Add(5 * -time.Minute)

	type testCase struct {
		name string
	}

	testCases := []testCase{
		{
			name: "deployment updated",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMetrics := db.NewMockMetrics(t)
			mockProjects := db.NewMockProjects(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                supersededPipelineID,
					CreationTimestamp: &startTime,
				},
				ProjectID:              project.Metadata.ID,
				ReleaseID:              &release.Metadata.ID,
				ParentPipelineID:       ptr.String("parent-pipeline-1"),
				ParentPipelineNodePath: ptr.String("pipeline.stage.s1.pipeline.p1"),
				EnvironmentName:        ptr.String("testing"),
				Type:                   models.DeploymentPipelineType,
				Timestamps: models.PipelineTimestamps{
					StartedTimestamp:   &startTime,
					CompletedTimestamp: &currentTime,
				},
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(pipeline, nil)
			mockProjects.On("GetProjectByID", mock.Anything, project.Metadata.ID).Return(project, nil).Maybe()
			mockReleases.On("GetReleaseByID", mock.Anything, release.Metadata.ID).Return(release, nil).Maybe()
			mockMetrics.On("CreateMetric", mock.Anything, mock.Anything).Return(nil, nil).Maybe()

			dbClient := &db.Client{
				Metrics:   mockMetrics,
				Projects:  mockProjects,
				Releases:  mockReleases,
				Pipelines: mockPipelines,
			}

			testLogger, _ := logger.NewForTest()
			metricManager := &MetricManager{
				dbClient: dbClient,
				logger:   testLogger,
			}

			err := metricManager.createDeploymentUpdatedMetric(ctx, supersededPipelineID)

			require.NoError(t, err)

			mockMetrics.AssertCalled(t, "CreateMetric", mock.Anything, &models.Metric{
				Name:            models.MetricDeploymentUpdated,
				PipelineID:      &supersededPipelineID,
				ReleaseID:       pipeline.ReleaseID,
				ProjectID:       &pipeline.ProjectID,
				OrganizationID:  &project.OrgID,
				EnvironmentName: pipeline.EnvironmentName,
				Value:           1,
				Tags: map[models.MetricTagName]string{
					models.MetricTagNameReleaseLifecycle: release.LifecyclePRN,
				},
			})
		})
	}
}

func TestMetricManager_createPipelineMetrics(t *testing.T) {
	release := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		LifecyclePRN: "release-lifecycle-1",
	}

	project := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: "project-1",
		},
		Name:  "sample-project",
		OrgID: "organization-1",
	}

	currentTime := time.Now().UTC()
	startTime := currentTime.Add(5 * -time.Minute)

	type testCase struct {
		name                    string
		pipelineStatus          statemachine.NodeStatus
		pipelineType            models.PipelineType
		failureDeploymentMetric *models.Metric
		superseededPipeline     *models.Pipeline
		expectedMetrics         []struct {
			name  models.MetricName
			value float64
		}
	}

	testCases := []testCase{
		{
			name:           "runbook succeeded",
			pipelineStatus: statemachine.SucceededNodeStatus,
			pipelineType:   models.RunbookPipelineType,
			expectedMetrics: []struct {
				name  models.MetricName
				value float64
			}{
				{models.MetricPipelineCompleted, 1},
				{models.MetricPipelineDuration, currentTime.Sub(startTime).Seconds()},
				{models.MetricPipelineLeadTime, currentTime.Sub(startTime).Seconds()},
			},
		},
		{
			name:           "deployment failed",
			pipelineStatus: statemachine.FailedNodeStatus,
			pipelineType:   models.DeploymentPipelineType,
			expectedMetrics: []struct {
				name  models.MetricName
				value float64
			}{
				{models.MetricPipelineCompleted, 1},
				{models.MetricPipelineDuration, currentTime.Sub(startTime).Seconds()},
				{models.MetricPipelineLeadTime, currentTime.Sub(startTime).Seconds()},
			},
		},
		{
			name:           "deployment succeeded",
			pipelineStatus: statemachine.SucceededNodeStatus,
			pipelineType:   models.DeploymentPipelineType,
			expectedMetrics: []struct {
				name  models.MetricName
				value float64
			}{
				{models.MetricPipelineCompleted, 1},
				{models.MetricPipelineDuration, currentTime.Sub(startTime).Seconds()},
				{models.MetricPipelineLeadTime, currentTime.Sub(startTime).Seconds()},
			},
		},
		{
			name:           "deployment succeeded with superseded pipeline",
			pipelineStatus: statemachine.SucceededNodeStatus,
			pipelineType:   models.DeploymentPipelineType,
			superseededPipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					CreationTimestamp: ptr.Time(startTime.Add(-10 * time.Minute)),
				},
			},
			expectedMetrics: []struct {
				name  models.MetricName
				value float64
			}{
				{models.MetricPipelineCompleted, 1},
				{models.MetricPipelineDuration, currentTime.Sub(startTime).Seconds()},
				{models.MetricPipelineLeadTime, currentTime.Sub(startTime.Add(-10 * time.Minute)).Seconds()},
			},
		},
		{
			name:           "deployment succeeded with recovery",
			pipelineStatus: statemachine.SucceededNodeStatus,
			pipelineType:   models.DeploymentPipelineType,
			failureDeploymentMetric: &models.Metric{
				Metadata: models.ResourceMetadata{
					CreationTimestamp: ptr.Time(startTime.Add(time.Minute)),
				},
			},
			expectedMetrics: []struct {
				name  models.MetricName
				value float64
			}{
				{models.MetricDeploymentRecoveryTime, currentTime.Sub(startTime.Add(time.Minute)).Seconds()},
				{models.MetricPipelineCompleted, 1},
				{models.MetricPipelineDuration, currentTime.Sub(startTime).Seconds()},
				{models.MetricPipelineLeadTime, currentTime.Sub(startTime).Seconds()},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockMetrics := db.NewMockMetrics(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelines := db.NewMockPipelines(t)
			mockReleases := db.NewMockReleases(t)

			pipeline := &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID:                "pipeline-1",
					CreationTimestamp: &startTime,
				},
				ProjectID:              project.Metadata.ID,
				ReleaseID:              &release.Metadata.ID,
				ParentPipelineID:       ptr.String("parent-pipeline-1"),
				ParentPipelineNodePath: ptr.String("pipeline.stage.s1.pipeline.p1"),
				EnvironmentName:        ptr.String("testing"),
				Type:                   test.pipelineType,
				Status:                 test.pipelineStatus,
				Timestamps: models.PipelineTimestamps{
					StartedTimestamp:   &startTime,
					CompletedTimestamp: &currentTime,
				},
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(pipeline, nil)

			mockProjects.On("GetProjectByID", mock.Anything, project.Metadata.ID).Return(project, nil).Maybe()
			mockReleases.On("GetReleaseByID", mock.Anything, release.Metadata.ID).Return(release, nil).Maybe()

			mockMetrics.On("CreateMetric", mock.Anything, mock.Anything).Return(nil, nil)

			if pipeline.Status.Equals(statemachine.SucceededNodeStatus) && pipeline.Type == models.DeploymentPipelineType {
				mockMetrics.On("GetMetrics", mock.Anything, mock.Anything).Return(&db.MetricsResult{
					Metrics: []*models.Metric{},
				}, nil).Once()

				if test.failureDeploymentMetric != nil {
					mockMetrics.On("GetMetrics", mock.Anything, mock.Anything).Return(&db.MetricsResult{
						Metrics: []*models.Metric{test.failureDeploymentMetric},
					}, nil).Once()
				} else {
					mockMetrics.On("GetMetrics", mock.Anything, mock.Anything).Return(&db.MetricsResult{
						Metrics: []*models.Metric{},
					}, nil).Once()
				}
			}

			if pipeline.ParentPipelineID != nil {
				if test.superseededPipeline != nil {
					mockPipelines.On("GetPipelines", mock.Anything, mock.Anything).Return(&db.PipelinesResult{
						Pipelines: []models.Pipeline{*test.superseededPipeline},
					}, nil).Once()
				} else {
					mockPipelines.On("GetPipelines", mock.Anything, mock.Anything).Return(&db.PipelinesResult{
						Pipelines: []models.Pipeline{},
					}, nil).Once()
				}
			}

			dbClient := &db.Client{
				Metrics:   mockMetrics,
				Projects:  mockProjects,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			metricManager := &MetricManager{
				dbClient: dbClient,
			}

			err := metricManager.createPipelineMetrics(ctx, pipeline.Metadata.ID, currentTime)
			require.NoError(t, err)

			for _, expectedMetric := range test.expectedMetrics {
				mockMetrics.AssertCalled(t, "CreateMetric", mock.Anything, &models.Metric{
					Name:            expectedMetric.name,
					PipelineID:      &pipeline.Metadata.ID,
					ReleaseID:       pipeline.ReleaseID,
					ProjectID:       &pipeline.ProjectID,
					OrganizationID:  &project.OrgID,
					EnvironmentName: pipeline.EnvironmentName,
					Value:           expectedMetric.value,
					Tags: map[models.MetricTagName]string{
						models.MetricTagNamePipelineStatus:   string(pipeline.Status),
						models.MetricTagNamePipelineType:     string(pipeline.Type),
						models.MetricTagNameReleaseLifecycle: release.LifecyclePRN,
					},
				})
			}
		})
	}
}
