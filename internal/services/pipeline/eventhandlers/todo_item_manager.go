package eventhandlers

import (
	"context"
	"encoding/json"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/email"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/email/builder"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

// ToDoItemManager manages todo items based on a pipeline's state.
type ToDoItemManager struct {
	logger          logger.Logger
	dbClient        *db.Client
	emailClient     email.Client
	pipelineUpdater pipeline.Updater
}

// NewToDoItemManager returns an instance of ToDoItemManager.
func NewToDoItemManager(logger logger.Logger, dbClient *db.Client, pipelineUpdater pipeline.Updater, emailClient email.Client) *ToDoItemManager {
	return &ToDoItemManager{
		logger:          logger,
		dbClient:        dbClient,
		pipelineUpdater: pipelineUpdater,
		emailClient:     emailClient,
	}
}

// RegisterHandlers registers any handlers with the PipelineUpdater used by the ToDoItemManager.
func (t *ToDoItemManager) RegisterHandlers() {
	t.pipelineUpdater.RegisterEventHandler(t.handleTaskApprovalPendingStatus)
	t.pipelineUpdater.RegisterEventHandler(t.handleNestedPipelineApprovalPendingStatus)
	t.pipelineUpdater.RegisterEventHandler(t.handleSupersededNestedPipelineToDoItem)
}

// handleTaskApprovalPendingStatus handles task status going to and from approval pending.
func (t *ToDoItemManager) handleTaskApprovalPendingStatus(ctx context.Context, _ string, changes []pipeline.StatusChange, _ any) error {
	for _, change := range changes {
		for ix := range change.NodeStatusChanges {
			targetStatusChange := change.NodeStatusChanges[ix]

			if targetStatusChange.NodeType != statemachine.TaskNodeType {
				// We're only interested in task nodes.
				continue
			}

			if !targetStatusChange.NewStatus.Equals(statemachine.ApprovalPendingNodeStatus) && !targetStatusChange.OldStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
				// We're only interested in transitions to and from ApprovalPending.
				continue
			}

			// Get the pipeline associated with the status change.
			pipeline, err := t.getPipelineByID(ctx, change.PipelineID)
			if err != nil {
				return errors.Wrap(err, "failed to get pipeline in todo manager event handler")
			}

			// Find the task in current pipeline.
			task, ok := pipeline.GetTask(targetStatusChange.NodePath)
			if !ok {
				return errors.New("failed to find task with path %s", targetStatusChange.NodePath)
			}

			if err := t.updateToDoItemsForPipelineApprovals(
				ctx,
				pipeline,
				&targetStatusChange,
				task.ApprovalRuleIDs,
			); err != nil {
				return errors.Wrap(err, "failed to process task node in todo manager")
			}
		}
	}

	return nil
}

// handleNestedPipelineApprovalPendingStatus handles nested pipelines status going to and from approval pending.
func (t *ToDoItemManager) handleNestedPipelineApprovalPendingStatus(ctx context.Context, _ string, changes []pipeline.StatusChange, _ any) error {
	for _, change := range changes {
		for ix := range change.NodeStatusChanges {
			targetStatusChange := change.NodeStatusChanges[ix]

			if targetStatusChange.NodeType != statemachine.PipelineNodeType || targetStatusChange.NodePath == "pipeline" {
				// We're only interested in nested pipeline nodes.
				continue
			}

			if !targetStatusChange.NewStatus.Equals(statemachine.ApprovalPendingNodeStatus) && !targetStatusChange.OldStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
				// We're only interested in transitions to and from ApprovalPending.
				continue
			}

			// Get the pipeline associated with the status change.
			pipeline, err := t.getPipelineByID(ctx, change.PipelineID)
			if err != nil {
				return errors.Wrap(err, "failed to get pipeline in todo manager event handler")
			}

			// Find the nestedPipeline in current pipeline.
			nestedPipelineNode, ok := pipeline.GetNestedPipeline(targetStatusChange.NodePath)
			if !ok {
				return errors.New("failed to find nested pipeline with path %s", targetStatusChange.NodePath)
			}

			// Approval rule IDs are stored on the latest pipeline instance, so must look that up.
			nestedPipeline, err := t.getPipelineByID(ctx, nestedPipelineNode.LatestPipelineID)
			if err != nil {
				return errors.New("failed to get nested pipeline in todo manager event handler")
			}

			if err := t.updateToDoItemsForPipelineApprovals(
				ctx,
				nestedPipeline,
				&targetStatusChange,
				nestedPipeline.ApprovalRuleIDs,
			); err != nil {
				return errors.Wrap(err, "failed to process nested pipeline node in todo manager")
			}
		}
	}

	return nil
}

func (t *ToDoItemManager) handleSupersededNestedPipelineToDoItem(ctx context.Context, _ string, changes []pipeline.StatusChange, event any) error {
	updateEvent, ok := event.(*pipeline.UpdateNestedPipelineEvent)
	if !ok {
		// We're only interested in update nested pipeline event.
		return nil
	}

	newNestedPipeline, err := t.getPipelineByID(ctx, updateEvent.NestedPipelineID)
	if err != nil {
		return errors.Wrap(err, "failed to get new nested pipeline")
	}

	// Resolve the ToDo item for the superseded pipeline.
	if err = t.resolveToDoItem(ctx, updateEvent.SupersededPipelineID, statemachine.PipelineNodeType, nil); err != nil {
		return errors.Wrap(err, "failed to resolve todo item for superseded pipeline")
	}

	for _, change := range changes {
		if len(change.NodeStatusChanges) > 0 {
			// Only create a new todo item if nothing has changed between the superseded
			// and new pipeline. Otherwise, the todo item should be created automatically
			// for the status change and we can just return here.
			return nil
		}
	}

	userIDs, teamIDs, err := t.getSubjectsForApprovalRules(ctx, newNestedPipeline.ApprovalRuleIDs)
	if err != nil {
		return errors.Wrap(err, "failed to get subjects for approval rule")
	}

	return t.createPipelineApprovalToDoItem(ctx, userIDs, teamIDs, newNestedPipeline, nil, statemachine.PipelineNodeType, newNestedPipeline.ApprovalRuleIDs, false)
}

func (t *ToDoItemManager) updateToDoItemsForPipelineApprovals(
	ctx context.Context,
	pipeline *models.Pipeline,
	statusChange *statemachine.NodeStatusChange,
	approvalRuleIDs []string,
) error {
	// Handle going into approval pending status.
	if statusChange.NewStatus.Equals(statemachine.ApprovalPendingNodeStatus) {
		userIDs, teamIDs, err := t.getSubjectsForApprovalRules(ctx, approvalRuleIDs)
		if err != nil {
			return errors.Wrap(err, "failed to get approval rules for pipeline %s", pipeline.Metadata.ID)
		}

		// Create a TODO item for the users / teams, so they know they have to approve this pipeline node.
		if len(userIDs) > 0 || len(teamIDs) > 0 {
			// For retries, we must un-resolve the previous ToDo item so only users who haven't
			// yet approved are notified instead of creating a new one.
			todoItemsInput := &db.GetToDoItemsInput{
				Filter: &db.ToDoItemFilter{
					PipelineTargetID: &pipeline.Metadata.ID,
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			}

			switch statusChange.NodeType {
			case statemachine.TaskNodeType:
				todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.TaskApprovalTarget}
				todoItemsInput.Filter.PayloadFilter = &db.ToDoItemPayloadFilter{PipelineTaskPath: &statusChange.NodePath}
			case statemachine.PipelineNodeType:
				todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.PipelineApprovalTarget}
			}

			todoItems, err := t.dbClient.ToDoItems.GetToDoItems(ctx, todoItemsInput)
			if err != nil {
				return errors.Wrap(err, "failed to get todo items")
			}

			if todoItems.PageInfo.TotalCount > 0 {
				// We have a todo item associated with this node, so we can just un-resolve it.
				todoItem := todoItems.ToDoItems[0]
				todoItem.AutoResolved = false

				updatedToDoItem, err := t.dbClient.ToDoItems.UpdateToDoItem(ctx, todoItem)
				if err != nil {
					return err
				}

				if err = t.sendApprovalRequestEmail(ctx, approvalRuleIDs, pipeline, updatedToDoItem); err != nil {
					return errors.Wrap(err, "failed to send approval request email")
				}

				return nil
			}

			// Create a new pipeline approval, this will handle nodes going into approval pending
			// for the first time or if they're updated.
			if err := t.createPipelineApprovalToDoItem(
				ctx,
				userIDs,
				teamIDs,
				pipeline,
				&statusChange.NodePath,
				statusChange.NodeType,
				approvalRuleIDs,
				true,
			); err != nil {
				return err
			}
		}
	}

	// Handle pipeline node approved.
	if statusChange.OldStatus.Equals(statemachine.ApprovalPendingNodeStatus) &&
		(statusChange.NewStatus.Equals(statemachine.ReadyNodeStatus) || statusChange.NewStatus.Equals(statemachine.CanceledNodeStatus)) {
		if err := t.resolveToDoItem(ctx, pipeline.Metadata.ID, statusChange.NodeType, &statusChange.NodePath); err != nil {
			return err
		}
	}

	return nil
}

// resolveToDoItem finds and resolves a todo item.
func (t *ToDoItemManager) resolveToDoItem(
	ctx context.Context,
	pipelineID string,
	nodeType statemachine.NodeType,
	nodePath *string,
) error {
	todoItemsInput := &db.GetToDoItemsInput{
		Filter: &db.ToDoItemFilter{
			PipelineTargetID: &pipelineID,
			Resolved:         ptr.Bool(false),
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
	}

	switch nodeType {
	case statemachine.TaskNodeType:
		todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.TaskApprovalTarget}
		todoItemsInput.Filter.PayloadFilter = &db.ToDoItemPayloadFilter{PipelineTaskPath: nodePath}
	case statemachine.PipelineNodeType:
		todoItemsInput.Filter.TargetTypes = []models.ToDoItemTargetType{models.PipelineApprovalTarget}
	}

	itemsResult, err := t.dbClient.ToDoItems.GetToDoItems(ctx, todoItemsInput)
	if err != nil {
		return errors.Wrap(err, "failed to get todo items")
	}

	if itemsResult.PageInfo.TotalCount > 0 {
		item := itemsResult.ToDoItems[0]

		// Resolve the ToDo item.
		item.AutoResolved = true

		if _, err := t.dbClient.ToDoItems.UpdateToDoItem(ctx, item); err != nil {
			return errors.Wrap(err, "failed to update todo item")
		}
	}

	return nil
}

// createPipelineApprovalToDoItem creates a ToDo item for a pipeline approval.
func (t *ToDoItemManager) createPipelineApprovalToDoItem(
	ctx context.Context,
	userIDs,
	teamIDs []string,
	pipeline *models.Pipeline,
	nodePath *string,
	nodeType statemachine.NodeType,
	approvalRuleIDs []string,
	sendMail bool,
) error {
	// Get the project to find the organization id.
	project, err := t.dbClient.Projects.GetProjectByID(ctx, pipeline.ProjectID)
	if err != nil {
		return errors.Wrap(err, "failed to get project associated with pipeline %s", pipeline.Metadata.ID)
	}

	if project == nil {
		return errors.New("project with id %s not found", pipeline.ProjectID)
	}

	todoItemToCreateInput := &models.ToDoItem{
		UserIDs:        userIDs,
		TeamIDs:        teamIDs,
		ProjectID:      &pipeline.ProjectID,
		OrganizationID: project.OrgID,
		TargetID:       pipeline.Metadata.ID,
	}

	pipelineApprovalsInput := &db.GetPipelineApprovalsInput{
		Filter: &db.PipelineApprovalFilter{
			PipelineID: &pipeline.Metadata.ID,
		},
	}

	switch nodeType {
	case statemachine.TaskNodeType:
		payload := &models.ToDoItemPipelineTaskApprovalPayload{
			TaskPath: ptr.ToString(nodePath),
		}

		payloadBuffer, jErr := json.Marshal(payload)
		if jErr != nil {
			return errors.Wrap(jErr, "failed to marshal event payload")
		}

		todoItemToCreateInput.Payload = payloadBuffer
		todoItemToCreateInput.TargetType = models.TaskApprovalTarget

		approvalTypeTask := models.ApprovalTypeTask
		pipelineApprovalsInput.Filter.Type = &approvalTypeTask
		pipelineApprovalsInput.Filter.TaskPath = nodePath
	case statemachine.PipelineNodeType:
		todoItemToCreateInput.TargetType = models.PipelineApprovalTarget

		approvalTypePipeline := models.ApprovalTypePipeline
		pipelineApprovalsInput.Filter.Type = &approvalTypePipeline
	}

	// Get the pipeline approvals, so todo item is automatically resolved for users who have given approvals.
	pipelineApprovalsResult, err := t.dbClient.PipelineApprovals.GetPipelineApprovals(ctx, pipelineApprovalsInput)
	if err != nil {
		return errors.Wrap(err, "failed to get pipeline approvals for pipeline %s", pipeline.Metadata.ID)
	}

	for _, approval := range pipelineApprovalsResult.PipelineApprovals {
		if approval.UserID != nil {
			todoItemToCreateInput.ResolvedByUsers = append(todoItemToCreateInput.ResolvedByUsers, *approval.UserID)
		}
	}

	createdToDoItem, err := t.dbClient.ToDoItems.CreateToDoItem(ctx, todoItemToCreateInput)
	if err != nil {
		return errors.Wrap(err, "failed to create pipeline approval todo item")
	}

	if sendMail {
		if err = t.sendApprovalRequestEmail(ctx, approvalRuleIDs, pipeline, createdToDoItem); err != nil {
			return errors.Wrap(err, "failed to send approval request email")
		}
	}

	return nil
}

func (t *ToDoItemManager) sendApprovalRequestEmail(ctx context.Context, approvalRuleIDs []string, pipeline *models.Pipeline, todoItem *models.ToDoItem) error {
	approvalRulesResp, err := t.dbClient.ApprovalRules.GetApprovalRules(ctx, &db.GetApprovalRulesInput{
		Filter: &db.ApprovalRuleFilter{
			ApprovalRuleIDs: approvalRuleIDs,
		},
	})
	if err != nil {
		return err
	}

	approvalRules := []builder.ApprovalRule{}
	for _, approvalRule := range approvalRulesResp.ApprovalRules {
		approvalRules = append(approvalRules, builder.ApprovalRule{
			Name:     approvalRule.Name,
			Required: approvalRule.ApprovalsRequired,
		})
	}

	eligibleApprovers := []string{}

	if len(todoItem.UserIDs) > 0 {
		usersResp, err := t.dbClient.Users.GetUsers(ctx, &db.GetUsersInput{
			Filter: &db.UserFilter{
				UserIDs: todoItem.UserIDs,
			},
		})
		if err != nil {
			return err
		}

		for _, user := range usersResp.Users {
			eligibleApprovers = append(eligibleApprovers, user.Username)
		}
	}

	if len(todoItem.TeamIDs) > 0 {
		teamsResp, err := t.dbClient.Teams.GetTeams(ctx, &db.GetTeamsInput{
			Filter: &db.TeamFilter{
				TeamIDs: todoItem.TeamIDs,
			},
		})
		if err != nil {
			return err
		}
		for _, team := range teamsResp.Teams {
			eligibleApprovers = append(eligibleApprovers, team.Name)
		}
	}

	releaseVersion := ""
	if pipeline.ReleaseID != nil {
		release, err := t.dbClient.Releases.GetReleaseByID(ctx, *pipeline.ReleaseID)
		if err != nil {
			return errors.Wrap(err, "failed to get release")
		}
		if release == nil {
			return errors.New("release with id %s not found", *pipeline.ReleaseID)
		}
		releaseVersion = release.SemanticVersion
	}

	environmentName := ""
	if pipeline.EnvironmentName != nil {
		environmentName = *pipeline.EnvironmentName
	}

	var nodePath *string
	var subject string

	switch todoItem.TargetType {
	case models.TaskApprovalTarget:
		subject = "Task Approval Request"

		var payload models.ToDoItemPipelineTaskApprovalPayload
		if err := json.Unmarshal(todoItem.Payload, &payload); err != nil {
			return errors.Wrap(err, "failed to unmarshal todo item payload")
		}

		nodePath = &payload.TaskPath
	case models.PipelineApprovalTarget:
		subject = "Pipeline Approval Request"

		nodePath = pipeline.ParentPipelineNodePath
	default:
		return errors.New("failed to build email template: invalid todo item target type: %s", todoItem.TargetType)
	}

	// Send emails for approval request
	t.emailClient.SendMail(ctx, &email.SendMailInput{
		UsersIDs:       todoItem.UserIDs,
		TeamsIDs:       todoItem.TeamIDs,
		ExcludeUserIDs: todoItem.ResolvedByUsers,
		Subject:        subject,
		Builder: &builder.ApprovalRequestEmail{
			OrgName:           pipeline.GetOrgName(),
			ProjectName:       pipeline.GetProjectName(),
			ApprovalRules:     approvalRules,
			PipelineType:      pipeline.Type,
			PipelineID:        gid.ToGlobalID(gid.PipelineType, pipeline.Metadata.ID),
			TodoTargetType:    todoItem.TargetType,
			NodePath:          nodePath,
			EligibleApprovers: eligibleApprovers,
			ReleaseVersion:    releaseVersion,
			EnvironmentName:   environmentName,
		},
	})
	return nil
}

func (t *ToDoItemManager) getSubjectsForApprovalRules(
	ctx context.Context,
	approvalRuleIDs []string,
) ([]string, []string, error) {
	approvalRulesResult, err := t.dbClient.ApprovalRules.GetApprovalRules(ctx, &db.GetApprovalRulesInput{
		Filter: &db.ApprovalRuleFilter{
			ApprovalRuleIDs: approvalRuleIDs,
		},
	})
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to get approval rules associated with pipeline")
	}

	if int(approvalRulesResult.PageInfo.TotalCount) != len(approvalRuleIDs) {
		return nil, nil, errors.New("one or more approval rules associated with pipeline were not found")
	}

	var (
		userIDMap = map[string]struct{}{}
		teamIDMap = map[string]struct{}{}
		userIDs   = []string{}
		teamIDs   = []string{}
	)

	// Only add unique subjects from all the approval rules.
	for _, approvalRule := range approvalRulesResult.ApprovalRules {
		for _, userID := range approvalRule.UserIDs {
			if _, ok := userIDMap[userID]; !ok {
				userIDs = append(userIDs, userID)
				userIDMap[userID] = struct{}{}
			}

		}

		for _, teamID := range approvalRule.TeamIDs {
			if _, ok := teamIDMap[teamID]; !ok {
				teamIDs = append(teamIDs, teamID)
				teamIDMap[teamID] = struct{}{}
			}
		}
	}

	return userIDs, teamIDs, nil
}

func (t *ToDoItemManager) getPipelineByID(ctx context.Context, id string) (*models.Pipeline, error) {
	pipeline, err := t.dbClient.Pipelines.GetPipelineByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline")
	}

	if pipeline == nil {
		return nil, errors.New("pipeline with id %s not found")
	}

	return pipeline, nil
}
