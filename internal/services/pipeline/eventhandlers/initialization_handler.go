// Package eventhandlers provides event handlers for pipeline events.
package eventhandlers

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/zclconf/go-cty/cty"
	ctyjson "github.com/zclconf/go-cty/cty/json"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipelinetemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// expressionEvaluationResult represents the result of evaluating an approval rule expression.
type expressionEvaluationResult struct {
	approvalRuleIDs []string
	markAsSkipped   bool
}

// InitializationHandler dictates if a pipeline node requires initialization.
type InitializationHandler struct {
	logger                logger.Logger
	dbClient              *db.Client
	pipelineUpdater       pipeline.Updater
	pipelineTemplateStore pipelinetemplate.DataStore
	pipelineArtifactStore pipeline.Store
}

// NewInitializationHandler returns an instance of InitializationHandler.
func NewInitializationHandler(
	logger logger.Logger,
	dbClient *db.Client,
	pipelineUpdater pipeline.Updater,
	pipelineTemplateStore pipelinetemplate.DataStore,
	pipelineArtifactStore pipeline.Store,
) *InitializationHandler {
	return &InitializationHandler{
		logger:                logger,
		dbClient:              dbClient,
		pipelineUpdater:       pipelineUpdater,
		pipelineTemplateStore: pipelineTemplateStore,
		pipelineArtifactStore: pipelineArtifactStore,
	}
}

// RegisterHandlers registers any handlers with the PipelineUpdater used by the InitializationHandler.
func (i *InitializationHandler) RegisterHandlers() {
	i.pipelineUpdater.RegisterEventHandler(i.handleTaskInitializing)
	i.pipelineUpdater.RegisterEventHandler(i.handlePipelineInitializing)
}

// handleTaskInitializing handles a task in the 'INITIALIZING' state.
func (i *InitializationHandler) handleTaskInitializing(ctx context.Context, _ string, changes []pipeline.StatusChange, _ any) error {
	for _, change := range changes {
		for ix := range change.NodeStatusChanges {
			targetStatusChange := change.NodeStatusChanges[ix]

			if targetStatusChange.NodeType != statemachine.TaskNodeType {
				// We're only interested in task nodes.
				continue
			}

			if !targetStatusChange.NewStatus.Equals(statemachine.InitializingNodeStatus) {
				// We're only worried about task nodes that are going into 'INITIALIZING' status.
				continue
			}

			// Get the pipeline associated with the status change.
			pipelineModel, err := i.getPipelineByID(ctx, change.PipelineID)
			if err != nil {
				return err
			}

			evaluationResult, evaluationErr := i.evaluateExpressions(ctx, pipelineModel, &targetStatusChange)
			if evaluationErr != nil {
				if errors.ErrorCode(evaluationErr) == errors.EInvalid {
					// If the evaluation error is because of bad user input, we'll initialize the task with the error.
					if _, eventErr := i.pipelineUpdater.ProcessEvent(ctx, change.PipelineID, &pipeline.InitializeTaskEvent{
						TaskPath: targetStatusChange.NodePath,
						Error:    evaluationErr,
					}); eventErr != nil {
						return errors.Wrap(eventErr, "failed to set task to error state while initializing")
					}

					continue
				}

				// Return any other error as-is since it's likely a bug.
				return evaluationErr
			}

			// Update the task to the new status.
			if _, err := i.pipelineUpdater.ProcessEvent(ctx, change.PipelineID, &pipeline.InitializeTaskEvent{
				TaskPath:        targetStatusChange.NodePath,
				ApprovalRuleIDs: evaluationResult.approvalRuleIDs,
				MarkAsSkipped:   evaluationResult.markAsSkipped,
			}); err != nil {
				return errors.Wrap(err, "failed to process initialize task event")
			}
		}
	}

	return nil
}

// handlePipelineInitializing handles a pipeline in the 'INITIALIZING' state.
func (i *InitializationHandler) handlePipelineInitializing(ctx context.Context, _ string, changes []pipeline.StatusChange, _ any) error {
	for _, change := range changes {
		for ix := range change.NodeStatusChanges {
			targetStatusChange := change.NodeStatusChanges[ix]

			if targetStatusChange.NodeType != statemachine.PipelineNodeType || targetStatusChange.NodePath == "pipeline" {
				// We're only interested in nested pipeline nodes.
				continue
			}

			if !targetStatusChange.NewStatus.Equals(statemachine.InitializingNodeStatus) {
				// We're only worried about nested pipeline nodes that are going into 'INITIALIZING' status.
				continue
			}

			// Get the parent associated with the status change.
			parent, err := i.getPipelineByID(ctx, change.PipelineID)
			if err != nil {
				return errors.Wrap(err, "failed to get pipeline in todo manager event handler")
			}

			// Get the pipeline associated with the status change.
			evaluationResult, evaluationErr := i.evaluateExpressions(ctx, parent, &targetStatusChange)
			if evaluationErr != nil {
				if errors.ErrorCode(evaluationErr) == errors.EInvalid {
					// If the evaluation error is because of bad user input, we'll initialize the nested pipeline with the error.
					if _, eventErr := i.pipelineUpdater.ProcessEvent(ctx, parent.Metadata.ID, &pipeline.InitializeNestedPipelineEvent{
						NestedPipelinePath: targetStatusChange.NodePath,
						Error:              evaluationErr,
					}); eventErr != nil {
						return errors.Wrap(eventErr, "failed to set nested pipeline to error state while initializing")
					}

					continue
				}

				// Return any other error as-is since it's likely a bug.
				return evaluationErr
			}

			// Initialize the nested pipeline.
			if _, err := i.pipelineUpdater.ProcessEvent(ctx, parent.Metadata.ID, &pipeline.InitializeNestedPipelineEvent{
				NestedPipelinePath: targetStatusChange.NodePath,
				ApprovalRuleIDs:    evaluationResult.approvalRuleIDs,
				MarkAsSkipped:      evaluationResult.markAsSkipped,
			}); err != nil {
				return errors.Wrap(err, "failed to process initialize nested pipeline event")
			}
		}
	}

	return nil
}

func (i *InitializationHandler) evaluateExpressions(
	ctx context.Context,
	pipeline *models.Pipeline,
	targetStatusChange *statemachine.NodeStatusChange,
) (*expressionEvaluationResult, error) {
	hclVariables, err := i.getPipelineVariables(ctx, pipeline.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline variables")
	}

	templateReader, err := i.pipelineTemplateStore.GetData(ctx, pipeline.PipelineTemplateID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get template data")
	}

	hclConfig, _, err := config.NewPipelineTemplate(templateReader, hclVariables)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse pipeline template")
	}

	// SkipCondition takes precedence over ApprovalRules.
	var ifExpression, approvalRulesExpression hcl.Expression
	switch targetStatusChange.NodeType {
	case statemachine.TaskNodeType:
		hclTask, ok := hclConfig.GetTask(targetStatusChange.NodePath)
		if !ok {
			return nil, errors.New("task with path %s not found", targetStatusChange.NodePath)
		}

		ifExpression = hclTask.If
		approvalRulesExpression = hclTask.ApprovalRules
	case statemachine.PipelineNodeType:
		hclPipeline, ok := hclConfig.GetNestedPipeline(targetStatusChange.NodePath)
		if !ok {
			return nil, errors.New("pipeline with path %s not found", targetStatusChange.NodePath)
		}

		ifExpression = hclPipeline.If
		approvalRulesExpression = hclPipeline.ApprovalRules
	default:
		return nil, errors.New("unexpected node type. Cannot initialize node type %s", targetStatusChange.NodeType)
	}

	if ifExpression.Range().Empty() && approvalRulesExpression.Range().Empty() {
		// No expressions to evaluate.
		return &expressionEvaluationResult{}, nil
	}

	variables, err := variable.GetCtyValuesForVariables(hclConfig.GetVariableBodies(), hclVariables, false)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get cty values for variables")
	}

	evalCtx := hclctx.New(map[string]cty.Value{
		"var": cty.ObjectVal(variables),
	})

	sm, err := hclConfig.BuildStateMachine()
	if err != nil {
		return nil, errors.Wrap(err, "failed to build state machine")
	}

	traversalCtx, err := sm.GetTraversalContextForNode(targetStatusChange.NodePath)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get traversal context for node")
	}

	actionPaths := []string{}
	for _, node := range traversalCtx.VisitedActions() {
		actionPaths = append(actionPaths, node.Path())
	}

	availableOutputs, err := i.getOutputsForActions(ctx, pipeline.Metadata.ID, actionPaths)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline action outputs")
	}

	if !ifExpression.Range().Empty() {
		val, diag := ifExpression.Value(evalCtx.NewActionContext(availableOutputs).Context())
		if diag.HasErrors() {
			return nil, errors.New("invalid if expression: %v", diag.Error(), errors.WithErrorCode(errors.EInvalid))
		}

		if !val.IsNull() {
			// Ensure the value is a boolean.
			if !val.Type().Equals(cty.Bool) {
				return nil, errors.New("if expression must evaluate to a boolean value", errors.WithErrorCode(errors.EInvalid))
			}

			if val.False() {
				// Skip the node if the condition is false.
				return &expressionEvaluationResult{markAsSkipped: true}, nil
			}
		}
	}

	if approvalRulesExpression.Range().Empty() {
		// No approval rules expression.
		return &expressionEvaluationResult{approvalRuleIDs: []string{}}, nil
	}

	val, diag := approvalRulesExpression.Value(evalCtx.NewActionContext(availableOutputs).Context())
	if diag.HasErrors() {
		return nil, errors.New("invalid approval rule expression: %v", diag.Error(), errors.WithErrorCode(errors.EInvalid))
	}

	if val.IsNull() {
		// We'll treat a 'null' value as an empty list '[]'.
		return &expressionEvaluationResult{approvalRuleIDs: []string{}}, nil
	}

	// A list in an HCL expression sometimes gets treated as a tuple,
	// so we'll check both types.
	if !val.CanIterateElements() && !val.Type().IsListType() && !val.Type().IsTupleType() {
		return nil, errors.New(
			"approval rule expression must evaluate to a list or tuple of strings",
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	// Convert all approval rule cty values into strings.
	approvalRules := []string{}
	for _, rule := range val.AsValueSlice() {
		if !rule.Type().Equals(cty.String) {
			return nil, errors.New(
				"approval rule expression must be a list of strings, unknown value type %s",
				rule.Type().FriendlyName(),
				errors.WithErrorCode(errors.EInvalid),
			)
		}

		approvalRules = append(approvalRules, rule.AsString())
	}

	// Enforce a limit on the number of approval rules.
	if len(approvalRules) > config.MaximumAllowedNodes {
		return nil, errors.New("cannot have more than %d approval rules for a %s", config.MaximumAllowedNodes, targetStatusChange.NodeType)
	}

	appRuleIDs, err := i.getApprovalRuleIDs(ctx, approvalRules, pipeline.ProjectID)
	if err != nil {
		return nil, err
	}

	return &expressionEvaluationResult{approvalRuleIDs: appRuleIDs}, nil
}

// getOutputsForActions returns outputs for actions that have already executed.
func (i *InitializationHandler) getOutputsForActions(ctx context.Context, pipelineID string, actionPaths []string) (map[string]map[string]cty.Value, error) {
	response := map[string]map[string]cty.Value{}

	if len(actionPaths) > 0 {
		actionOutputs, err := i.dbClient.PipelineActionOutputs.GetPipelineActionOutputs(ctx, pipelineID, actionPaths)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get pipeline action outputs")
		}

		for _, output := range actionOutputs {
			if _, ok := response[output.ActionPath]; !ok {
				response[output.ActionPath] = map[string]cty.Value{}
			}

			typ, err := ctyjson.UnmarshalType(output.Type)
			if err != nil {
				return nil, fmt.Errorf("failed to unmarshal type for output %s: %w", output.Name, err)
			}

			val, err := ctyjson.Unmarshal(output.Value, typ)
			if err != nil {
				return nil, fmt.Errorf("failed to unmarshal value for output %s: %w", output.Name, err)
			}

			response[output.ActionPath][output.Name] = val
		}
	}

	return response, nil
}

// getApprovalRuleIDs fetches approval rules by either PRN or GID, verifies they
// are usable (checks if they're within the same org and/or project) and returning the IDs.
func (i *InitializationHandler) getApprovalRuleIDs(
	ctx context.Context,
	approvalRuleIDs []string,
	projectID string,
) ([]string, error) {
	if len(approvalRuleIDs) == 0 {
		return []string{}, nil
	}

	result := []string{}

	project, err := i.getProjectByID(ctx, projectID)
	if err != nil {
		return nil, err
	}

	for _, ruleID := range approvalRuleIDs {
		var (
			rule *models.ApprovalRule
			err  error
		)

		if strings.HasPrefix(ruleID, models.PRNPrefix) {
			rule, err = i.dbClient.ApprovalRules.GetApprovalRuleByPRN(ctx, ruleID)
		} else {
			// Parse the ID incase it's malformed.
			parsedID, gErr := gid.ParseGlobalID(ruleID)
			if gErr != nil {
				return nil, errors.Wrap(gErr, "failed to parse approval rule ID %s", ruleID)
			}

			rule, err = i.dbClient.ApprovalRules.GetApprovalRuleByID(ctx, parsedID.ID)
		}

		if err != nil {
			return nil, errors.Wrap(err, "failed to get approval rule %s", ruleID)
		}

		if rule == nil {
			return nil, errors.New("approval rule %s does not exist", ruleID, errors.WithErrorCode(errors.EInvalid))
		}

		switch rule.Scope {
		case models.OrganizationScope:
			if rule.OrgID != project.OrgID {
				return nil, errors.New("approval rule %s is outside the target organization", rule.Name, errors.WithErrorCode(errors.EInvalid))
			}
		case models.ProjectScope:
			if *rule.ProjectID != project.Metadata.ID {
				return nil, errors.New("approval rule %s is outside the target project", rule.Name, errors.WithErrorCode(errors.EInvalid))
			}
		}

		result = append(result, rule.Metadata.ID)
	}

	return result, nil
}

// getPipelineVariables returns HCL variables for a pipeline as a map.
func (i *InitializationHandler) getPipelineVariables(ctx context.Context, pipelineID string) (map[string]string, error) {
	reader, err := i.pipelineArtifactStore.GetPipelineVariables(ctx, pipelineID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline variables")
	}

	defer reader.Close()

	var variables []pipeline.Variable
	if err := json.NewDecoder(reader).Decode(&variables); err != nil {
		return nil, errors.Wrap(err, "failed to decode pipeline variables")
	}

	// Convert HCL variables to map for easier processing.
	hclVariables := make(map[string]string, len(variables))
	for _, variable := range variables {
		hclVariables[variable.Key] = variable.Value
	}

	return hclVariables, nil
}

// getPipelineByID returns a pipeline by ID.
func (i *InitializationHandler) getPipelineByID(ctx context.Context, id string) (*models.Pipeline, error) {
	pipeline, err := i.dbClient.Pipelines.GetPipelineByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline")
	}

	if pipeline == nil {
		return nil, errors.New("pipeline with id %s not found", id)
	}

	return pipeline, nil
}

// getProjectByID returns a project by id.
func (i *InitializationHandler) getProjectByID(ctx context.Context, id string) (*models.Project, error) {
	project, err := i.dbClient.Projects.GetProjectByID(ctx, id)
	if err != nil {
		return nil, err
	}

	if project == nil {
		return nil, errors.New("project with id %s not found", id)
	}

	return project, nil
}
