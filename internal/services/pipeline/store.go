package pipeline

//go:generate go tool mockery --name Store --inpackage --case underscore

import (
	context "context"
	"fmt"
	"io"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/objectstore"
)

// Store interface encapsulates the logic for saving pipeline artifacts
type Store interface {
	UploadPipelineVariables(ctx context.Context, pipelineID string, body io.Reader) error
	GetPipelineVariables(ctx context.Context, pipelineID string) (io.ReadCloser, error)
}

type store struct {
	objectStore objectstore.ObjectStore
}

// NewStore creates an instance of the Store interface
func NewStore(objectStore objectstore.ObjectStore) Store {
	return &store{objectStore: objectStore}
}

// GetPipelineVariables returns the pipeline variables for the given pipeline
func (a *store) GetPipelineVariables(ctx context.Context, pipelineID string) (io.ReadCloser, error) {
	return a.objectStore.GetObjectStream(ctx, getPipelineVariablesObjectKey(pipelineID), nil)
}

// UploadPipelineVariables uploads the pipeline variables for the given pipeline
func (a *store) UploadPipelineVariables(ctx context.Context, pipelineID string, body io.Reader) error {
	return a.upload(ctx, getPipelineVariablesObjectKey(pipelineID), body)
}

func (a *store) upload(ctx context.Context, key string, body io.Reader) error {
	return a.objectStore.UploadObject(ctx, key, body)
}

func getPipelineVariablesObjectKey(pipelineID string) string {
	return fmt.Sprintf("pipelines/%s/variables.json", pipelineID)
}
