// Package activityevent provides functionality for the activity events
// which are used to track changes to Phobos resources.
package activityevent

//go:generate go tool mockery --name Service --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
)

// GetActivityEventsInput represents the input for getting activity events.
type GetActivityEventsInput struct {
	Sort              *db.ActivityEventSortableField
	PaginationOptions *pagination.Options
	OrganizationID    *string
	ProjectID         *string
	UserID            *string
	ServiceAccountID  *string
	ReleaseTargetID   *string
	TimeRangeStart    *time.Time
	TimeRangeEnd      *time.Time
	Actions           []models.ActivityEventAction
	TargetTypes       []models.ActivityEventTargetType
}

// CreateActivityEventInput represents the input for creating an activity event.
type CreateActivityEventInput struct {
	Payload        any
	OrganizationID *string
	ProjectID      *string
	TargetID       *string
	Action         models.ActivityEventAction
	TargetType     models.ActivityEventTargetType
}

// SubscribeToActivityEventsInput represents the input for subscribing to activity events.
type SubscribeToActivityEventsInput struct {
	OrganizationID *string
	ProjectID      *string
}

// Service encapsulates the logic for interfacing with the activity event service.
type Service interface {
	GetActivityEvents(ctx context.Context, input *GetActivityEventsInput) (*db.ActivityEventsResult, error)
	CreateActivityEvent(ctx context.Context, input *CreateActivityEventInput) (*models.ActivityEvent, error)
	SubscribeToActivityEvents(ctx context.Context, input *SubscribeToActivityEventsInput) (<-chan *models.ActivityEvent, error)
}

type service struct {
	logger       logger.Logger
	dbClient     *db.Client
	eventManager *events.EventManager
}

// NewService creates a new activity event service.
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	eventManager *events.EventManager,
) Service {
	return &service{
		logger:       logger,
		dbClient:     dbClient,
		eventManager: eventManager,
	}
}

func (s *service) GetActivityEvents(ctx context.Context, input *GetActivityEventsInput) (*db.ActivityEventsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetActivityEvents")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var membershipRequirement *db.ActivityEventMembershipRequirement

	if !caller.IsAdmin() {
		switch c := caller.(type) {
		case *auth.UserCaller:
			membershipRequirement = &db.ActivityEventMembershipRequirement{UserID: &c.User.Metadata.ID}
		case *auth.ServiceAccountCaller:
			membershipRequirement = &db.ActivityEventMembershipRequirement{ServiceAccountID: &c.ServiceAccountID}
		default:
			return nil, errors.New("invalid caller type", errors.WithErrorCode(errors.EUnauthorized))
		}
	}

	dbInput := &db.GetActivityEventsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ActivityEventFilter{
			UserID:           input.UserID,
			ServiceAccountID: input.ServiceAccountID,
			OrganizationID:   input.OrganizationID,
			ProjectID:        input.ProjectID,
			ReleaseTargetID:  input.ReleaseTargetID,
			TimeRangeStart:   input.TimeRangeStart,
			TimeRangeEnd:     input.TimeRangeEnd,
			Actions:          input.Actions,
			TargetTypes:      input.TargetTypes,
			// MembershipRequirement will only return activity events from
			// organizations / projects that the caller is a member of.
			MembershipRequirement: membershipRequirement,
		},
	}

	return s.dbClient.ActivityEvents.GetActivityEvents(ctx, dbInput)
}

func (s *service) CreateActivityEvent(ctx context.Context, input *CreateActivityEventInput) (*models.ActivityEvent, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateActivityEvent")
	span.SetAttributes(attribute.String("target_type", string(input.TargetType)))
	span.SetAttributes(attribute.String("action", string(input.Action)))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var userID, serviceAccountID *string
	switch c := caller.(type) {
	case *auth.UserCaller:
		userID = &c.User.Metadata.ID
	case *auth.ServiceAccountCaller:
		serviceAccountID = &c.ServiceAccountID
	default:
		// If the caller is not a user or service account, do not create an activity event.
		return nil, nil
	}

	var payloadBuffer []byte
	if input.Payload != nil {
		payloadBuffer, err = json.Marshal(input.Payload)
		if err != nil {
			return nil, errors.Wrap(err, "failed to marshal event payload", errors.WithSpan(span))
		}
	}

	toCreate := &models.ActivityEvent{
		UserID:           userID,
		ServiceAccountID: serviceAccountID,
		OrganizationID:   input.OrganizationID,
		ProjectID:        input.ProjectID,
		Payload:          payloadBuffer,
		TargetID:         input.TargetID,
		Action:           input.Action,
		TargetType:       input.TargetType,
	}

	return s.dbClient.ActivityEvents.CreateActivityEvent(ctx, toCreate)
}

func (s *service) SubscribeToActivityEvents(ctx context.Context, input *SubscribeToActivityEventsInput) (<-chan *models.ActivityEvent, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToActivityEvents")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if input.OrganizationID != nil && input.ProjectID != nil {
		// We only need to check that the caller can view the organization since
		// an org membership will give them access to all projects under it.
		if err = caller.RequirePermission(ctx, models.ViewOrganization, auth.WithOrganizationID(*input.OrganizationID)); err != nil {
			return nil, err
		}

		project, gErr := s.dbClient.Projects.GetProjectByID(ctx, *input.ProjectID)
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get project", errors.WithSpan(span))
		}

		if project == nil {
			return nil, errors.New("project not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
		}

		if project.OrgID != *input.OrganizationID {
			return nil, errors.New("cannot subscribe to activity events for a project outside of specified organization", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	} else if input.OrganizationID != nil {
		if err = caller.RequirePermission(ctx, models.ViewOrganization, auth.WithOrganizationID(*input.OrganizationID)); err != nil {
			return nil, err
		}
	} else if input.ProjectID != nil {
		if err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(*input.ProjectID)); err != nil {
			return nil, err
		}
	}
	// If not using any filters, we will authorizer caller has access to each activity event later.

	subscription := events.Subscription{
		Type: events.ActivityEventSubscription,
		Actions: []events.SubscriptionAction{
			events.CreateAction,
			events.UpdateAction,
		},
	}

	subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})

	outgoing := make(chan *models.ActivityEvent)
	go func() {
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) {
					s.logger.Errorf("Error occurred while waiting for activity events: %v", err)
				}
				return
			}

			activityEvent, err := s.getActivityEvent(ctx, event.ID, input.OrganizationID, input.ProjectID)
			if err != nil {
				s.logger.Errorf("Error occurred while getting activity event: %v", err)
				continue
			}

			if activityEvent == nil {
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- activityEvent:
			}
		}
	}()

	return outgoing, nil
}

// getActivityEvent returns the activity event for the given subscription event.
// Authorizes that the caller can view the activity event. Returns nil errors in
// certain cases to avoid noise in the logs.
func (s *service) getActivityEvent(
	ctx context.Context,
	eventID string,
	organizationID *string,
	projectID *string,
) (*models.ActivityEvent, error) {
	activityEvent, err := s.dbClient.ActivityEvents.GetActivityEventByID(ctx, eventID)
	if err != nil {
		return nil, fmt.Errorf("error querying for activity event for subscription event %s: %s", eventID, err)
	}
	if activityEvent == nil {
		return nil, fmt.Errorf("received event for activity event that does not exist %s", eventID)
	}

	// Filtering by organization.
	if activityEvent.OrganizationID != nil && organizationID != nil && *activityEvent.OrganizationID != *organizationID {
		return nil, nil
	}

	if activityEvent.ProjectID != nil {
		// Filtering by project.
		if projectID != nil && *activityEvent.ProjectID != *projectID {
			// Not for the target project.
			return nil, nil
		}

		// Filtering by organization, but we have a project activity event.
		// So, we need to check that the project is in the organization.
		if organizationID != nil {
			project, err := s.dbClient.Projects.GetProjectByID(ctx, *activityEvent.ProjectID)
			if err != nil {
				return nil, fmt.Errorf("error querying for project for activity event subscription: %s", err)
			}

			if project == nil {
				return nil, fmt.Errorf("project associated with activity event does not exist: %w", err)
			}

			if project.OrgID != *organizationID {
				return nil, nil
			}
		}
	}

	// If not filtering by organization or project, must authorize that caller can view each activity event.
	if organizationID == nil && projectID == nil {
		caller, err := auth.AuthorizeCaller(ctx)
		if err != nil {
			return nil, err
		}

		switch {
		case activityEvent.OrganizationID != nil:
			if err := caller.RequirePermission(ctx, models.ViewOrganization, auth.WithOrganizationID(*activityEvent.OrganizationID)); err != nil {
				return nil, nil
			}
		case activityEvent.ProjectID != nil:
			if err := caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(*activityEvent.ProjectID)); err != nil {
				return nil, nil
			}
		default:
			// If the activity event is not associated with an organization or project,
			// it is a global event and the caller must be an admin.
			if !caller.IsAdmin() {
				return nil, nil
			}
		}
	}

	return activityEvent, nil
}
