package activityevent

import (
	context "context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	eventManager := events.NewEventManager(dbClient, logger)

	expect := &service{
		logger:       logger,
		dbClient:     dbClient,
		eventManager: eventManager,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, eventManager))
}

func TestGetActivityEvents(t *testing.T) {
	serviceAccountID := "sample-service-account-id"
	userID := "sample-user-id"

	type testCase struct {
		name            string
		caller          auth.Caller
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:   "user queries for activity events",
			caller: &auth.UserCaller{User: &models.User{Metadata: models.ResourceMetadata{ID: userID}}},
		},
		{
			name:   "service account queries for activity events",
			caller: &auth.ServiceAccountCaller{ServiceAccountID: serviceAccountID},
		},
		{
			name:   "admin queries for activity events",
			caller: &auth.UserCaller{User: &models.User{Admin: true}},
		},
		{
			name:            "invalid caller type",
			caller:          &auth.JobCaller{},
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "no caller",
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockActivityEvents := db.NewMockActivityEvents(t)

			serviceInput := &GetActivityEventsInput{}

			if test.expectErrorCode == "" {
				dbInput := &db.GetActivityEventsInput{Filter: &db.ActivityEventFilter{}}

				switch c := test.caller.(type) {
				case *auth.UserCaller:
					if !c.User.Admin {
						dbInput.Filter.UserID = &c.User.Metadata.ID
						dbInput.Filter.MembershipRequirement = &db.ActivityEventMembershipRequirement{
							UserID: &c.User.Metadata.ID,
						}
						serviceInput.UserID = &c.User.Metadata.ID
					}
				case *auth.ServiceAccountCaller:
					dbInput.Filter.ServiceAccountID = &c.ServiceAccountID
					dbInput.Filter.MembershipRequirement = &db.ActivityEventMembershipRequirement{
						ServiceAccountID: &c.ServiceAccountID,
					}
					serviceInput.ServiceAccountID = &c.ServiceAccountID
				}

				mockActivityEvents.On("GetActivityEvents", mock.Anything, dbInput).Return(&db.ActivityEventsResult{}, nil)
			}

			dbClient := &db.Client{
				ActivityEvents: mockActivityEvents,
			}

			service := &service{
				dbClient: dbClient,
			}

			result, err := service.GetActivityEvents(auth.WithCaller(ctx, test.caller), serviceInput)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			assert.NotNil(t, result)
		})
	}
}

func TestCreateActivityEvent(t *testing.T) {
	serviceAccountID := "sample-service-account-id"
	userID := "sample-user-id"

	type testCase struct {
		caller          auth.Caller
		expectErrorCode errors.CodeType
		name            string
		expectCreated   bool
	}

	testCases := []testCase{
		{
			name:          "user creates activity event",
			caller:        &auth.UserCaller{User: &models.User{Metadata: models.ResourceMetadata{ID: userID}}},
			expectCreated: true,
		},
		{
			name:          "service account creates activity event",
			caller:        &auth.ServiceAccountCaller{ServiceAccountID: serviceAccountID},
			expectCreated: true,
		},
		{
			name:   "invalid caller type",
			caller: &auth.JobCaller{},
			// Invalid caller shouldn't return an error.
		},
		{
			name:            "no caller",
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockActivityEvents := db.NewMockActivityEvents(t)

			if test.expectCreated {
				toCreate := &models.ActivityEvent{}

				switch c := test.caller.(type) {
				case *auth.UserCaller:
					toCreate.UserID = &c.User.Metadata.ID
				case *auth.ServiceAccountCaller:
					toCreate.ServiceAccountID = &c.ServiceAccountID
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, toCreate).Return(toCreate, nil)
			}

			dbClient := &db.Client{
				ActivityEvents: mockActivityEvents,
			}

			service := &service{
				dbClient: dbClient,
			}

			created, err := service.CreateActivityEvent(auth.WithCaller(ctx, test.caller), &CreateActivityEventInput{})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectCreated {
				assert.NotNil(t, created)
			} else {
				assert.Nil(t, created)
			}
		})
	}
}

func TestSubscribeToActivityEvents(t *testing.T) {
	organizationID := "sample-organization-id"
	projectID := "sample-project-id"

	type testCase struct {
		authError       error
		input           *SubscribeToActivityEventsInput
		project         *models.Project
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "user subscribes to organization activity events",
			input: &SubscribeToActivityEventsInput{
				OrganizationID: &organizationID,
			},
		},
		{
			name: "user subscribes to project activity events",
			input: &SubscribeToActivityEventsInput{
				ProjectID: &projectID,
			},
		},
		{
			name: "user does not have access to organization",
			input: &SubscribeToActivityEventsInput{
				OrganizationID: &organizationID,
			},
			authError:       errors.New("unauthorized", errors.WithErrorCode(errors.EUnauthorized)),
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name: "user does not have access to project",
			input: &SubscribeToActivityEventsInput{
				ProjectID: &projectID,
			},
			authError:       errors.New("unauthorized", errors.WithErrorCode(errors.EUnauthorized)),
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:  "user subscribes to all activity events",
			input: &SubscribeToActivityEventsInput{},
		},
		{
			name: "specified project is not associated with the organization",
			input: &SubscribeToActivityEventsInput{
				ProjectID:      &projectID,
				OrganizationID: &organizationID,
			},
			project:         &models.Project{OrgID: "unknown"},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "specified project does not exist",
			input: &SubscribeToActivityEventsInput{
				ProjectID:      &projectID,
				OrganizationID: &organizationID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "specified both project and organization",
			input: &SubscribeToActivityEventsInput{
				ProjectID:      &projectID,
				OrganizationID: &organizationID,
			},
			project: &models.Project{OrgID: organizationID},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)

			if test.input.OrganizationID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewOrganization, mock.Anything).Return(test.authError)
			}

			if test.input.OrganizationID != nil && test.input.ProjectID != nil {
				mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(test.project, nil)
			} else if test.input.ProjectID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).Return(test.authError)
			}

			logger, _ := logger.NewForTest()
			dbClient := &db.Client{
				Projects: mockProjects,
			}

			service := &service{
				logger:       logger,
				dbClient:     dbClient,
				eventManager: events.NewEventManager(dbClient, logger),
			}

			resultChan, err := service.SubscribeToActivityEvents(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			assert.NotNil(t, resultChan)
		})
	}
}

func TestGetActivityEvent(t *testing.T) {
	organizationID := "sample-organization-id"
	projectID := "sample-project-id"
	eventID := "sample-event-id"

	type testCase struct {
		authError           error
		organizationID      *string
		projectID           *string
		activityEvent       *models.ActivityEvent
		project             *models.Project
		name                string
		expectErrorCode     errors.CodeType
		expectActivityEvent bool
		isAdmin             bool
	}

	testCases := []testCase{
		{
			name: "activity event is for the organization",
			activityEvent: &models.ActivityEvent{
				OrganizationID: &organizationID,
			},
			organizationID:      &organizationID,
			expectActivityEvent: true,
		},
		{
			name: "activity event is for the project",
			activityEvent: &models.ActivityEvent{
				ProjectID: &projectID,
			},
			projectID:           &projectID,
			expectActivityEvent: true,
		},
		{
			name: "activity event is for a project that belongs to the organization",
			activityEvent: &models.ActivityEvent{
				ProjectID: &projectID,
			},
			project: &models.Project{
				Metadata: models.ResourceMetadata{
					ID: projectID,
				},
				OrgID: organizationID,
			},
			organizationID:      &organizationID,
			expectActivityEvent: true,
		},
		{
			name: "project associated with activity event does not exist",
			activityEvent: &models.ActivityEvent{
				ProjectID: &projectID,
			},
			organizationID:  &organizationID,
			expectErrorCode: errors.EInternal,
		},
		{
			name: "activity event is not for the organization",
			activityEvent: &models.ActivityEvent{
				OrganizationID: ptr.String("unknown"),
			},
			organizationID: &organizationID,
		},
		{
			name: "activity event is not for the project",
			activityEvent: &models.ActivityEvent{
				ProjectID: ptr.String("unknown"),
			},
			projectID: &projectID,
		},
		{
			name: "no filter, caller has access to see organization activity event",
			activityEvent: &models.ActivityEvent{
				OrganizationID: &organizationID,
			},
			expectActivityEvent: true,
		},
		{
			name: "no filter, caller has access to see project activity event",
			activityEvent: &models.ActivityEvent{
				ProjectID: &projectID,
			},
			expectActivityEvent: true,
		},
		{
			name: "no filter, caller does not have access to see organization activity event",
			activityEvent: &models.ActivityEvent{
				OrganizationID: &organizationID,
			},
			authError: errors.New("unauthorized", errors.WithErrorCode(errors.EUnauthorized)),
		},
		{
			name: "no filter, caller does not have access to see project activity event",
			activityEvent: &models.ActivityEvent{
				ProjectID: &projectID,
			},
			authError: errors.New("unauthorized", errors.WithErrorCode(errors.EUnauthorized)),
		},
		{
			name:          "no filter, caller does not have access to see global event",
			activityEvent: &models.ActivityEvent{},
		},
		{
			name:                "admin can see global event",
			activityEvent:       &models.ActivityEvent{},
			isAdmin:             true,
			expectActivityEvent: true,
		},
		{
			name:            "activity event does not exist",
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockActivityEvents := db.NewMockActivityEvents(t)
			mockProjects := db.NewMockProjects(t)

			if test.activityEvent != nil && test.organizationID == nil && test.projectID == nil {
				if test.activityEvent.OrganizationID != nil {
					mockCaller.On("RequirePermission", mock.Anything, models.ViewOrganization, mock.Anything).Return(test.authError)
				} else if test.activityEvent.ProjectID != nil {
					mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			mockActivityEvents.On("GetActivityEventByID", mock.Anything, eventID).Return(test.activityEvent, nil)

			mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(test.project, nil).Maybe()

			dbClient := &db.Client{
				Projects:       mockProjects,
				ActivityEvents: mockActivityEvents,
			}

			service := &service{
				dbClient: dbClient,
			}

			result, err := service.getActivityEvent(auth.WithCaller(ctx, mockCaller), eventID, test.organizationID, test.projectID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			if test.expectActivityEvent {
				assert.NotNil(t, result)
			} else {
				assert.Nil(t, result)
			}
		})
	}
}
