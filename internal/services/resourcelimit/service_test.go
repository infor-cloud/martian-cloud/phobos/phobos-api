package resourcelimit

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}

	expect := &service{
		logger:   logger,
		dbClient: dbClient,
	}

	assert.Equal(t, expect, NewService(logger, dbClient))
}

func TestUpdateResourceLimit(t *testing.T) {
	userMemberID := "this-is-a-fake-user-member-ID"
	testBadInput := &UpdateResourceLimitInput{
		Name:  "not-a-valid-resource-limit-name",
		Value: 56,
	}

	testGoodInput := &UpdateResourceLimitInput{
		Name:  string(limits.ResourceLimitTotalRoles),
		Value: 47,
	}

	testUpdatedInput := &UpdateResourceLimitInput{
		Name:  string(limits.ResourceLimitTotalRoles),
		Value: 91,
	}

	testGoodLimit := &models.ResourceLimit{
		Name:  testGoodInput.Name,
		Value: testGoodInput.Value,
	}

	testUpdatedLimit := &models.ResourceLimit{
		Name:  testUpdatedInput.Name,
		Value: testUpdatedInput.Value,
	}

	type testCase struct {
		input           *UpdateResourceLimitInput
		injectOldLimit  *models.ResourceLimit
		injectNewLimit  *models.ResourceLimit
		expectLimit     *models.ResourceLimit
		name            string
		callerType      string // "admin", "user"
		expectErrorCode errors.CodeType
		willGetLimit    bool
	}

	/*
		template test case:
		{
			name            string
			callerType      string // "admin", "user"
			input           *UpdateResourceLimitInput
			willGetLimit    bool
			injectOldLimit  *models.ResourceLimit
			injectNewLimit  *models.ResourceLimit
			expectLimit     *models.ResourceLimit
			expectErrorCode errors.CodeType
		}
	*/

	// Test cases
	testCases := []testCase{
		{
			name:            "non-admin user cannot create/update limits",
			callerType:      "user",
			input:           testGoodInput,
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "admin user cannot create/update a limit with a bad name",
			callerType:      "admin",
			input:           testBadInput,
			willGetLimit:    true,
			injectOldLimit:  nil,
			expectErrorCode: errors.EInvalid,
		},
		// There is no such thing as creating a new limit in the DB, because they are all pre-populated.
		{
			name:           "admin user can update an existing limit",
			callerType:     "admin",
			input:          testUpdatedInput,
			willGetLimit:   true,
			injectOldLimit: testGoodLimit,
			injectNewLimit: testUpdatedLimit,
			expectLimit:    testUpdatedLimit,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockTransactions := db.NewMockTransactions(t)
			mockTransactions.Test(t)

			mockResourceLimits := db.NewMockResourceLimits(t)
			mockResourceLimits.Test(t)

			if test.willGetLimit {
				mockResourceLimits.On("GetResourceLimit", mock.Anything, test.input.Name).Return(test.injectOldLimit, nil)
			}

			if (test.expectErrorCode == "") && (test.injectOldLimit != nil) {
				// for the update existing limit case
				mockResourceLimits.On("UpdateResourceLimit", mock.Anything, mock.Anything).Return(test.injectNewLimit, nil)
			}

			dbClient := &db.Client{
				Transactions:   mockTransactions,
				ResourceLimits: mockResourceLimits,
			}

			var testCaller auth.Caller
			switch test.callerType {
			case "admin":
				testCaller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userMemberID,
						},
						Admin:    true,
						Username: "user1",
					},
					nil,
					dbClient,
				)
			case "user":
				testCaller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userMemberID,
						},
						Admin:    false,
						Username: "user1",
					},
					nil,
					dbClient,
				)
			default:
				assert.Fail(t, "invalid caller type in test")
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient: dbClient,
				logger:   logger,
			}

			// Call the service function.
			actualOutput, actualError := service.UpdateResourceLimit(auth.WithCaller(ctx, testCaller), test.input)

			assert.Equal(t, (test.expectErrorCode == ""), (actualError == nil))
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(actualError))
			}

			assert.Equal(t, test.expectLimit, actualOutput)
		})
	}
}
