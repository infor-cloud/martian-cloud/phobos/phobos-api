package organization

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	membershipService := membership.NewMockService(t)
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:            logger,
		dbClient:          dbClient,
		membershipService: membershipService,
		activityService:   activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, membershipService, activityService))
}

func TestGetOrganizationByID(t *testing.T) {
	organizationID := "organization-1"

	type testCase struct {
		authError          error
		expectOrganization *models.Organization
		name               string
		expectErrorCode    errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return an organization",
			expectOrganization: &models.Organization{
				Metadata: models.ResourceMetadata{
					ID: organizationID,
				},
			},
		},
		{
			name:            "organization does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "caller does not have permission to view organizations",
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockOrganizations := db.NewMockOrganizations(t)
			mockCaller := auth.NewMockCaller(t)

			mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.OrganizationResource, mock.Anything).Return(test.authError)

			if test.authError == nil {
				mockOrganizations.On("GetOrganizationByID", mock.Anything, organizationID).Return(test.expectOrganization, nil)
			}

			dbClient := &db.Client{
				Organizations: mockOrganizations,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualOrganization, err := service.GetOrganizationByID(auth.WithCaller(ctx, mockCaller), organizationID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectOrganization, actualOrganization)
		})
	}
}

func TestGetOrganizationByPRN(t *testing.T) {
	organizationPRN := "prn:organization:organization-1"

	type testCase struct {
		authError          error
		expectOrganization *models.Organization
		name               string
		expectErrorCode    errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return an organization",
			expectOrganization: &models.Organization{
				Metadata: models.ResourceMetadata{
					PRN: organizationPRN,
				},
			},
		},
		{
			name:            "organization does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "caller does not have permission to view organizations",
			expectOrganization: &models.Organization{
				Metadata: models.ResourceMetadata{
					PRN: organizationPRN,
				},
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockOrganizations := db.NewMockOrganizations(t)
			mockCaller := auth.NewMockCaller(t)

			if test.expectOrganization != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.OrganizationResource, mock.Anything).Return(test.authError)
			}

			mockOrganizations.On("GetOrganizationByPRN", mock.Anything, organizationPRN).Return(test.expectOrganization, nil)

			dbClient := &db.Client{
				Organizations: mockOrganizations,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualOrganization, err := service.GetOrganizationByPRN(auth.WithCaller(ctx, mockCaller), organizationPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectOrganization, actualOrganization)
		})
	}
}

func TestGetOrganizationsByIDs(t *testing.T) {
	inputList := []string{"org-1"}

	type testCase struct {
		name                string
		expectErrorCode     errors.CodeType
		authError           error
		expectOrganizations []models.Organization
	}

	testCases := []testCase{
		{
			name:                "successfully return a list of organizations",
			expectOrganizations: []models.Organization{{Metadata: models.ResourceMetadata{ID: "org-1"}}},
		},
		{
			name:                "caller does not have permission to view organizations",
			expectOrganizations: []models.Organization{{Metadata: models.ResourceMetadata{ID: "org-1"}}},
			authError:           errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:     errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockOrganizations := db.NewMockOrganizations(t)

			mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.OrganizationResource, mock.Anything).Return(test.authError)

			dbInput := &db.GetOrganizationsInput{
				Filter: &db.OrganizationFilter{
					OrganizationIDs: inputList,
				},
			}
			dbResult := &db.OrganizationsResult{
				Organizations: test.expectOrganizations,
			}
			mockOrganizations.On("GetOrganizations", mock.Anything, dbInput).Return(dbResult, nil)

			dbClient := &db.Client{
				Organizations: mockOrganizations,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualOrganizations, err := service.GetOrganizationsByIDs(auth.WithCaller(ctx, mockCaller), inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectOrganizations, actualOrganizations)
		})
	}
}

func TestGetOrganizations(t *testing.T) {
	sort := db.OrganizationSortableFieldUpdatedAtAsc
	userMemberID := "user-member-id"
	search := "some-search"
	paginationFirst := int32(10)

	type testCase struct {
		input      *GetOrganizationsInput
		dbInput    *db.GetOrganizationsInput
		name       string
		callerType string
	}

	testCases := []testCase{
		{
			name:       "admin user caller with empty input",
			callerType: "admin",
			input:      &GetOrganizationsInput{},
			dbInput: &db.GetOrganizationsInput{
				Filter: &db.OrganizationFilter{
					// Filter shouldn't be added for an admin.
				},
			},
		},
		{
			name:       "admin user caller with fully populated input",
			callerType: "admin",
			input: &GetOrganizationsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: &paginationFirst,
				},
				Search: &search,
			},
			dbInput: &db.GetOrganizationsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: &paginationFirst,
				},
				Filter: &db.OrganizationFilter{
					Search: &search,
				},
			},
		},
		{
			name:       "non-admin user caller with empty input",
			callerType: "user",
			input:      &GetOrganizationsInput{},
			dbInput: &db.GetOrganizationsInput{
				Filter: &db.OrganizationFilter{
					UserMemberID: &userMemberID,
				},
			},
		},
		{
			name:       "non-admin user caller with fully populated input",
			callerType: "user",
			input: &GetOrganizationsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: &paginationFirst,
				},
				Search: &search,
			},
			dbInput: &db.GetOrganizationsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: &paginationFirst,
				},
				Filter: &db.OrganizationFilter{
					Search:       &search,
					UserMemberID: &userMemberID,
				},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockOrganizations := db.NewMockOrganizations(t)

			dbResult := &db.OrganizationsResult{
				Organizations: []models.Organization{{}},
			}

			mockOrganizations.On("GetOrganizations", mock.Anything, test.dbInput).Return(dbResult, nil)

			dbClient := &db.Client{
				Organizations: mockOrganizations,
			}

			var testCaller auth.Caller
			switch test.callerType {
			case "admin":
				testCaller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userMemberID,
						},
						Admin: true,
					},
					nil,
					nil,
				)
			case "user":

				testCaller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userMemberID,
						},
						Admin: false,
					},
					nil,
					nil,
				)
			default:
				assert.Fail(t, "invalid caller type in test")
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetOrganizations(auth.WithCaller(ctx, testCaller), test.input)

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, dbResult, actualResult)
		})
	}
}

func TestCreateOrganization(t *testing.T) {
	sampleUser := &models.User{
		Metadata: models.ResourceMetadata{
			ID: "user-1",
		},
		Email: "sample-user@domain.tld",
	}

	sampleOrganization := &models.Organization{
		Name:        "some-organization",
		Description: "This is a new organization",
		CreatedBy:   sampleUser.Email,
	}

	input := &CreateOrganizationInput{
		Name:        sampleOrganization.Name,
		Description: sampleOrganization.Description,
	}

	type testCase struct {
		expectCreated   *models.Organization
		name            string
		expectErrorCode errors.CodeType
		isAdmin         bool
	}

	testCases := []testCase{
		{
			name:          "successfully create a new organization",
			expectCreated: sampleOrganization,
			isAdmin:       true,
		},
		{
			name:            "user is not an admin",
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			sampleUser.Admin = test.isAdmin
			ctx = auth.WithCaller(ctx, &auth.UserCaller{User: sampleUser})

			mockOrganizations := db.NewMockOrganizations(t)
			mockTransactions := db.NewMockTransactions(t)
			mockMemberships := membership.NewMockService(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.expectCreated != nil {
				sampleMembership := &models.Membership{
					UserID:         &sampleUser.Metadata.ID,
					RoleID:         models.OwnerRoleID.String(),
					OrganizationID: &test.expectCreated.Metadata.ID,
					Scope:          models.OrganizationScope,
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockOrganizations.On("CreateOrganization", mock.Anything, test.expectCreated).Return(test.expectCreated, nil)

				membershipInput := &membership.CreateOrganizationMembershipInput{
					OrganizationID: test.expectCreated.Metadata.ID,
					User:           sampleUser,
					RoleID:         models.OwnerRoleID.String(),
				}

				mockMemberships.On("CreateOrganizationMembership", mock.Anything, membershipInput).Return(sampleMembership, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: &test.expectCreated.Metadata.ID,
						Action:         models.ActionCreate,
						TargetType:     models.TargetOrganization,
						TargetID:       &test.expectCreated.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Organizations: mockOrganizations,
				Transactions:  mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:          dbClient,
				membershipService: mockMemberships,
				activityService:   mockActivityEvents,
				logger:            logger,
			}

			actualCreated, err := service.CreateOrganization(ctx, input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreated, actualCreated)
		})
	}
}

func TestUpdateOrganization(t *testing.T) {
	input := &UpdateOrganizationInput{
		ID:          "organization-id",
		Description: ptr.String("This is the new description"),
		Version:     ptr.Int(5),
	}

	type testCase struct {
		existingOrganization *models.Organization
		name                 string
		expectErrorCode      errors.CodeType
		isAdmin              bool
	}

	testCases := []testCase{
		{
			name: "successfully update an organization",
			existingOrganization: &models.Organization{
				Metadata: models.ResourceMetadata{
					ID: input.ID,
				},
				Name:        "organization-name",
				Description: "This is the old description",
			},
			isAdmin: true,
		},
		{
			name:            "organization does not exist",
			expectErrorCode: errors.ENotFound,
			isAdmin:         true,
		},
		{
			name:            "user it not an admin",
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ctx = auth.WithCaller(ctx, &auth.UserCaller{
				User: &models.User{
					Admin: test.isAdmin,
					Email: "user@domain.tld",
				},
			})

			mockOrganizations := db.NewMockOrganizations(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.isAdmin {
				mockOrganizations.On("GetOrganizationByID", mock.Anything, input.ID).Return(test.existingOrganization, nil)

				if test.existingOrganization != nil {
					mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
					mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
					mockTransactions.On("CommitTx", mock.Anything).Return(nil)

					mockOrganizations.On("UpdateOrganization", mock.Anything, test.existingOrganization).Return(test.existingOrganization, nil)

					mockActivityEvents.On("CreateActivityEvent", mock.Anything,
						&activityevent.CreateActivityEventInput{
							OrganizationID: &test.existingOrganization.Metadata.ID,
							Action:         models.ActionUpdate,
							TargetType:     models.TargetOrganization,
							TargetID:       &test.existingOrganization.Metadata.ID,
						},
					).Return(&models.ActivityEvent{}, nil)
				}
			}

			dbClient := &db.Client{
				Organizations: mockOrganizations,
				Transactions:  mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			actualUpdated, err := service.UpdateOrganization(ctx, input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingOrganization, actualUpdated)
		})
	}
}

func TestDeleteOrganization(t *testing.T) {
	organizationID := "organization-1"

	input := &DeleteOrganizationInput{
		ID: organizationID,
	}

	sampleOrganization := &models.Organization{
		Metadata: models.ResourceMetadata{
			ID: organizationID,
		},
	}

	type testCase struct {
		existingOrganization *models.Organization
		name                 string
		expectErrorCode      errors.CodeType
		isAdmin              bool
	}

	testCases := []testCase{
		{
			name:                 "successfully delete an organization",
			existingOrganization: sampleOrganization,
			isAdmin:              true,
		},
		{
			name:            "organization does not exist",
			expectErrorCode: errors.ENotFound,
			isAdmin:         true,
		},
		{
			name:                 "user is not an admin",
			existingOrganization: sampleOrganization,
			expectErrorCode:      errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ctx = auth.WithCaller(ctx, &auth.UserCaller{
				User: &models.User{
					Admin: test.isAdmin,
					Email: "user@domain.tld",
				},
			})

			mockOrganizations := db.NewMockOrganizations(t)

			if test.isAdmin {
				mockOrganizations.On("GetOrganizationByID", mock.Anything, input.ID).Return(test.existingOrganization, nil)

				if test.existingOrganization != nil {
					mockOrganizations.On("DeleteOrganization", mock.Anything, test.existingOrganization).Return(nil)
				}
			}

			dbClient := &db.Client{
				Organizations: mockOrganizations,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient: dbClient,
				logger:   logger,
			}

			err := service.DeleteOrganization(ctx, input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
