// Package organization implements functionality related to Phobos orgs.
package organization

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/membership"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetOrganizationsInput is the input for querying a list of organizations
type GetOrganizationsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.OrganizationSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Search is used to search for a organization by name
	Search *string
}

// CreateOrganizationInput is the input for creating an organization
type CreateOrganizationInput struct {
	Name        string
	Description string
}

// UpdateOrganizationInput is the input for updating an organization
type UpdateOrganizationInput struct {
	Version     *int
	Description *string
	ID          string
}

// DeleteOrganizationInput is the input for deleting an organization
type DeleteOrganizationInput struct {
	Version *int
	ID      string
}

// Service implements all organization related functionality
type Service interface {
	GetOrganizationByID(ctx context.Context, id string) (*models.Organization, error)
	GetOrganizationByPRN(ctx context.Context, prn string) (*models.Organization, error)
	GetOrganizationsByIDs(ctx context.Context, idList []string) ([]models.Organization, error)
	GetOrganizations(ctx context.Context, input *GetOrganizationsInput) (*db.OrganizationsResult, error)
	CreateOrganization(ctx context.Context, input *CreateOrganizationInput) (*models.Organization, error)
	UpdateOrganization(ctx context.Context, input *UpdateOrganizationInput) (*models.Organization, error)
	DeleteOrganization(ctx context.Context, input *DeleteOrganizationInput) error
}

type service struct {
	logger            logger.Logger
	dbClient          *db.Client
	membershipService membership.Service
	activityService   activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	membershipService membership.Service,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:            logger,
		dbClient:          dbClient,
		membershipService: membershipService,
		activityService:   activityService,
	}
}

func (s *service) GetOrganizationByID(ctx context.Context, id string) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "svc.GetOrganizationByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = caller.RequireAccessToInheritableResource(ctx, models.OrganizationResource, auth.WithOrganizationID(id))
	if err != nil {
		return nil, err
	}

	return s.getOrganizationByID(ctx, span, id)
}

func (s *service) GetOrganizationByPRN(ctx context.Context, prn string) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "svc.GetOrganizationByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	organization, err := s.dbClient.Organizations.GetOrganizationByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get organization by PRN", errors.WithSpan(span))
	}

	if organization == nil {
		return nil, errors.New(
			"organization with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	err = caller.RequireAccessToInheritableResource(ctx, models.OrganizationResource, auth.WithOrganizationID(organization.Metadata.ID))
	if err != nil {
		return nil, err
	}

	return organization, nil
}

func (s *service) GetOrganizationsByIDs(ctx context.Context, idList []string) ([]models.Organization, error) {
	ctx, span := tracer.Start(ctx, "svc.GetOrganizationsByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetOrganizationsInput{
		Filter: &db.OrganizationFilter{OrganizationIDs: idList},
	}

	resp, err := s.dbClient.Organizations.GetOrganizations(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get organizations", errors.WithSpan(span))
	}

	orgIDs := make([]string, 0, len(resp.Organizations))
	for _, org := range resp.Organizations {
		orgIDs = append(orgIDs, org.Metadata.ID)
	}

	if len(orgIDs) > 0 {
		err = caller.RequireAccessToInheritableResource(ctx, models.OrganizationResource, auth.WithOrganizationIDs(orgIDs))
		if err != nil {
			return nil, err
		}
	}

	return resp.Organizations, nil
}

func (s *service) GetOrganizations(ctx context.Context, input *GetOrganizationsInput) (*db.OrganizationsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetOrganizations")
	if input.Search != nil {
		span.SetAttributes(attribute.String("search", *input.Search))
	}
	defer span.End()

	_, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetOrganizationsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.OrganizationFilter{
			Search: input.Search,
		},
	}

	// Only return organizations that the caller is a member of.
	if err = auth.HandleCaller(
		ctx,
		func(_ context.Context, c *auth.UserCaller) error {
			if !c.User.Admin {
				dbInput.Filter.UserMemberID = &c.User.Metadata.ID
			}
			return nil
		},
		func(_ context.Context, c *auth.ServiceAccountCaller) error {
			dbInput.Filter.ServiceAccountMemberID = &c.ServiceAccountID
			return nil
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to handle caller", errors.WithSpan(span))
	}

	resp, err := s.dbClient.Organizations.GetOrganizations(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get organizations", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) CreateOrganization(ctx context.Context, input *CreateOrganizationInput) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateOrganization")
	span.SetAttributes(attribute.String("name", input.Name))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	userCaller, ok := caller.(*auth.UserCaller)
	if !ok {
		return nil, errors.New("Unsupported caller type, only users are allowed to create organizations", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	// Only system admins are allowed to create organizations
	if !userCaller.User.Admin {
		return nil, errors.New("Only system admins can create organizations", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	toCreate := &models.Organization{
		Name:        input.Name,
		Description: input.Description,
		CreatedBy:   caller.GetSubject(),
	}

	// Validate model
	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate a organization model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin a DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for CreateOrganization: %v", txErr)
		}
	}()

	organization, err := s.dbClient.Organizations.CreateOrganization(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create organization", errors.WithSpan(span))
	}

	membershipInput := &membership.CreateOrganizationMembershipInput{
		OrganizationID: organization.Metadata.ID,
		RoleID:         models.OwnerRoleID.String(),
		User:           userCaller.User,
	}

	// Add user as organization owner.
	_, err = s.membershipService.CreateOrganizationMembership(txContext, membershipInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create membership", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &organization.Metadata.ID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetOrganization,
			TargetID:       &organization.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit a DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created an organization.",
		"caller", caller.GetSubject(),
		"name", organization.Name,
		"organizationID", organization.Metadata.ID,
	)

	return organization, nil
}

func (s *service) UpdateOrganization(ctx context.Context, input *UpdateOrganizationInput) (*models.Organization, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateOrganization")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// Only system admins are allowed to update organizations
	if !caller.IsAdmin() {
		return nil, errors.New("Only system admins can update organizations", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	gotOrg, err := s.getOrganizationByID(ctx, span, input.ID)
	if err != nil {
		return nil, err
	}

	if input.Description != nil {
		gotOrg.Description = *input.Description
	}

	if input.Version != nil {
		gotOrg.Metadata.Version = *input.Version
	}

	if err = gotOrg.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate organization model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin a DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for UpdateOrganization: %v", txErr)
		}
	}()

	updatedOrg, err := s.dbClient.Organizations.UpdateOrganization(txContext, gotOrg)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &updatedOrg.Metadata.ID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetOrganization,
			TargetID:       &updatedOrg.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit a DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated an organization.",
		"caller", caller.GetSubject(),
		"organizationName", gotOrg.Name,
		"organizationID", gotOrg.Metadata.ID,
	)

	return updatedOrg, nil
}

func (s *service) DeleteOrganization(ctx context.Context, input *DeleteOrganizationInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteOrganization")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	// Only system admins are allowed to delete organizations
	if !caller.IsAdmin() {
		return errors.New("Only system admins can delete organizations", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	gotOrg, err := s.getOrganizationByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	if input.Version != nil {
		gotOrg.Metadata.Version = *input.Version
	}

	if err = s.dbClient.Organizations.DeleteOrganization(ctx, gotOrg); err != nil {
		return err
	}

	s.logger.Infow("Deleted an organization.",
		"caller", caller.GetSubject(),
		"organizationName", gotOrg.Name,
		"organizationID", gotOrg.Metadata.ID,
	)

	return nil
}

func (s *service) getOrganizationByID(ctx context.Context, span trace.Span, id string) (*models.Organization, error) {
	organization, err := s.dbClient.Organizations.GetOrganizationByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get organization by ID", errors.WithSpan(span))
	}

	if organization == nil {
		return nil, errors.New(
			"organization with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return organization, nil
}
