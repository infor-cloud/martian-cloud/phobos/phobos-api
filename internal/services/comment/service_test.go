package comment

import (
	"context"
	"encoding/json"
	"slices"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockLimitChecker := limits.NewMockLimitChecker(t)
	mockActivityEvents := activityevent.NewMockService(t)
	eventManager := events.NewEventManager(dbClient, logger)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    mockLimitChecker,
		eventManager:    eventManager,
		activityService: mockActivityEvents,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, mockLimitChecker, eventManager, mockActivityEvents))
}

func TestSubscribeToComments(t *testing.T) {
	comment := &models.Comment{
		Metadata: models.ResourceMetadata{
			ID: "comment-1",
		},
		ThreadID: "thread-1",
	}

	pipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: "pipeline-1",
		},
		ProjectID: "project-1",
	}

	release := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		ProjectID: "project-1",
	}

	type testCase struct {
		authError       error
		pipeline        *models.Pipeline
		release         *models.Release
		input           *SubscribeToCommentsInput
		name            string
		expectErrorCode errors.CodeType
		sendEventData   []*db.CommentEventData
		expectedEvents  []Event
	}

	testCases := []testCase{
		{
			name: "successfully subscribe to pipeline comments",
			input: &SubscribeToCommentsInput{
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline: pipeline,
			sendEventData: []*db.CommentEventData{
				{
					ID:       comment.Metadata.ID,
					ThreadID: comment.ThreadID,
				},
			},
			expectedEvents: []Event{
				{
					Comment: comment,
					Action:  string(events.CreateAction),
				},
			},
		},
		{
			name: "successfully subscribe to release comments",
			input: &SubscribeToCommentsInput{
				ReleaseID: &release.Metadata.ID,
			},
			release: release,
			sendEventData: []*db.CommentEventData{
				{
					ID:       comment.Metadata.ID,
					ThreadID: comment.ThreadID,
				},
			},
			expectedEvents: []Event{
				{
					Comment: comment,
					Action:  string(events.CreateAction),
				},
			},
		},
		{
			name: "should skip comment events not for the target resource",
			input: &SubscribeToCommentsInput{
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline: pipeline,
			sendEventData: []*db.CommentEventData{
				{
					ID:       "comment-2",
					ThreadID: "thread-2",
				},
				{
					ID:       comment.Metadata.ID,
					ThreadID: comment.ThreadID,
				},
			},
			expectedEvents: []Event{
				{
					Comment: comment,
					Action:  string(events.CreateAction),
				},
			},
		},
		{
			name: "pipeline not found",
			input: &SubscribeToCommentsInput{
				PipelineID: &pipeline.Metadata.ID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "release not found",
			input: &SubscribeToCommentsInput{
				ReleaseID: &release.Metadata.ID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view pipeline comments",
			input: &SubscribeToCommentsInput{
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline:        pipeline,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view release comments",
			input: &SubscribeToCommentsInput{
				ReleaseID: &release.Metadata.ID,
			},
			release:         release,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()

			mockEvents := db.NewMockEvents(t)
			mockThreads := db.NewMockThreads(t)
			mockCaller := auth.NewMockCaller(t)
			mockComments := db.NewMockComments(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			expectEvents := len(test.expectedEvents)

			mockEventChannel := make(chan db.Event, 1)
			var roEventChan <-chan db.Event = mockEventChannel
			mockEvents.On("Listen", mock.Anything).Return(roEventChan, make(<-chan error)).Maybe()

			for _, d := range test.sendEventData {
				mockComments.On("GetCommentByID", mock.Anything, d.ID).
					Return(&models.Comment{
						Metadata: models.ResourceMetadata{
							ID: d.ID,
						},
						ThreadID: d.ThreadID,
					}, nil).Maybe()
				mockThreads.On("GetThreads", mock.Anything, &db.GetThreadsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
					Filter: &db.ThreadFilter{
						PipelineID: test.input.PipelineID,
						ReleaseID:  test.input.ReleaseID,
						ThreadIDs:  []string{d.ThreadID},
					},
				}).Return(func(_ context.Context, input *db.GetThreadsInput) (*db.ThreadsResult, error) {
					dbResult := &db.ThreadsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 0,
						},
					}

					if slices.Contains(input.Filter.ThreadIDs, comment.ThreadID) {
						// We're incrementing this totalCount to indicate
						// the thread is for the pipeline / release we're interested in.
						dbResult.PageInfo.TotalCount++
					}

					return dbResult, nil
				}).Maybe()
			}

			if test.input.PipelineID != nil {
				mockPipelines.On("GetPipelineByID", mock.Anything, *test.input.PipelineID).Return(test.pipeline, nil)
			}

			if test.input.ReleaseID != nil {
				mockReleases.On("GetReleaseByID", mock.Anything, *test.input.ReleaseID).Return(test.release, nil)
			}

			mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError).Maybe()

			dbClient := &db.Client{
				Events:    mockEvents,
				Threads:   mockThreads,
				Comments:  mockComments,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			logger, _ := logger.NewForTest()
			eventManager := events.NewEventManager(dbClient, logger)
			eventManager.Start(ctx)

			service := &service{
				dbClient:     dbClient,
				eventManager: eventManager,
				logger:       logger,
			}

			actualEvents, err := service.SubscribeToComments(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			go func() {
				for _, d := range test.sendEventData {
					encoded, err := json.Marshal(d)
					require.Nil(t, err)

					mockEventChannel <- db.Event{
						Table:  "comments",
						Action: string(events.CreateAction),
						ID:     d.ID,
						Data:   encoded,
					}
				}
			}()

			receivedEvents := []*Event{}

			if expectEvents > 0 {
				for e := range actualEvents {
					receivedEvents = append(receivedEvents, e)

					if len(receivedEvents) == expectEvents {
						break
					}
				}
			}

			require.Len(t, receivedEvents, expectEvents)
			for i, e := range test.expectedEvents {
				assert.Equal(t, e, *receivedEvents[i])
			}
		})
	}
}

func TestGetCommentByID(t *testing.T) {
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"

	comment := &models.Comment{
		Metadata: models.ResourceMetadata{
			ID: "comment-1",
		},
		ThreadID: "thread-1",
	}

	type testCase struct {
		name            string
		authError       error
		thread          *models.Thread
		comment         *models.Comment
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully get pipeline comment by id",
			comment: comment,
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
		},
		{
			name:    "successfully get release comment by id",
			comment: comment,
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
		},
		{
			name:            "comment not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:    "subject is not authorized to view pipeline comment",
			comment: comment,
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:    "subject is not authorized to view release comment",
			comment: comment,
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockComments := db.NewMockComments(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			mockComments.On("GetCommentByID", mock.Anything, comment.Metadata.ID).Return(test.comment, nil)

			if test.comment != nil {
				mockThreads.On("GetThreadByID", mock.Anything, comment.ThreadID).Return(test.thread, nil)

				if test.thread.PipelineID != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.thread.PipelineID).Return(&models.Pipeline{
						ProjectID: projectID,
					}, nil)
				}

				if test.thread.ReleaseID != nil {
					mockReleases.On("GetReleaseByID", mock.Anything, *test.thread.ReleaseID).Return(&models.Release{
						ProjectID: projectID,
					}, nil)
				}
			}

			mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError).Maybe()

			dbClient := &db.Client{
				Threads:   mockThreads,
				Comments:  mockComments,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualComment, err := service.GetCommentByID(auth.WithCaller(ctx, mockCaller), comment.Metadata.ID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.comment, actualComment)
		})
	}
}

func TestGetCommentByPRN(t *testing.T) {
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"

	comment := &models.Comment{
		Metadata: models.ResourceMetadata{
			ID:  "comment-1",
			PRN: models.CommentResource.BuildPRN("gid-comment-1"),
		},
		ThreadID: "thread-1",
	}

	type testCase struct {
		name            string
		authError       error
		thread          *models.Thread
		comment         *models.Comment
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:    "successfully get pipeline comment by prn",
			comment: comment,
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
		},
		{
			name:    "successfully get release comment by prn",
			comment: comment,
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
		},
		{
			name:            "comment not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:    "subject is not authorized to view pipeline comment",
			comment: comment,
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:    "subject is not authorized to view release comment",
			comment: comment,
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockComments := db.NewMockComments(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			mockComments.On("GetCommentByPRN", mock.Anything, comment.Metadata.PRN).Return(test.comment, nil)

			if test.comment != nil {
				mockThreads.On("GetThreadByID", mock.Anything, comment.ThreadID).Return(test.thread, nil)

				if test.thread.PipelineID != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.thread.PipelineID).Return(&models.Pipeline{
						ProjectID: projectID,
					}, nil)
				}

				if test.thread.ReleaseID != nil {
					mockReleases.On("GetReleaseByID", mock.Anything, *test.thread.ReleaseID).Return(&models.Release{
						ProjectID: projectID,
					}, nil)
				}
			}

			mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError).Maybe()

			dbClient := &db.Client{
				Threads:   mockThreads,
				Comments:  mockComments,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualComment, err := service.GetCommentByPRN(auth.WithCaller(ctx, mockCaller), comment.Metadata.PRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.comment, actualComment)
		})
	}
}

func TestGetCommentsByIDs(t *testing.T) {
	projectID := "project-1"
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	idList := []string{"comment-1", "comment-2"}

	type testCase struct {
		authError       error
		name            string
		expectErrorCode errors.CodeType
		threads         []models.Thread
	}

	testCases := []testCase{
		{
			name: "successfully get comments by ids",
			threads: []models.Thread{
				{
					Metadata: models.ResourceMetadata{
						ID: "thread-1",
					},
					PipelineID: &pipelineID,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: "thread-2",
					},
					ReleaseID: &releaseID,
				},
			},
		},
		{
			name: "subject does not have permission to view comments",
			threads: []models.Thread{
				{
					Metadata: models.ResourceMetadata{
						ID: "thread-1",
					},
					PipelineID: &pipelineID,
				},
				{
					Metadata: models.ResourceMetadata{
						ID: "thread-2",
					},
					ReleaseID: &releaseID,
				},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockComments := db.NewMockComments(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			mockComments.On("GetComments", mock.Anything, &db.GetCommentsInput{
				Filter: &db.CommentFilter{
					CommentIDs: idList,
				},
			}).Return(func(_ context.Context, input *db.GetCommentsInput) (*db.CommentsResult, error) {
				dbResult := &db.CommentsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 0,
					},
					Comments: []models.Comment{},
				}

				for ix, id := range input.Filter.CommentIDs {
					dbResult.PageInfo.TotalCount++
					dbResult.Comments = append(dbResult.Comments, models.Comment{
						Metadata: models.ResourceMetadata{
							ID: id,
						},
						ThreadID: test.threads[ix].Metadata.ID,
					})
				}

				return dbResult, nil
			})

			mockThreads.On("GetThreads", mock.Anything, &db.GetThreadsInput{
				Filter: &db.ThreadFilter{
					ThreadIDs: []string{test.threads[0].Metadata.ID, test.threads[1].Metadata.ID},
				},
			}).Return(&db.ThreadsResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				Threads: test.threads,
			}, nil)

			mockPipelines.On("GetPipelines", mock.Anything, &db.GetPipelinesInput{
				Filter: &db.PipelineFilter{
					PipelineIDs: []string{pipelineID},
				},
			}).Return(&db.PipelinesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				Pipelines: []models.Pipeline{
					{
						Metadata: models.ResourceMetadata{
							ID: pipelineID,
						},
						ProjectID: projectID,
					},
				},
			}, nil).Maybe()

			mockReleases.On("GetReleases", mock.Anything, &db.GetReleasesInput{
				Filter: &db.ReleaseFilter{
					ReleaseIDs: []string{releaseID},
				},
			}).Return(&db.ReleasesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				Releases: []*models.Release{
					{
						Metadata: models.ResourceMetadata{
							ID: releaseID,
						},
						ProjectID: projectID,
					},
				},
			}, nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError).Maybe()

			dbClient := &db.Client{
				Threads:   mockThreads,
				Comments:  mockComments,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualComments, err := service.GetCommentsByIDs(auth.WithCaller(ctx, mockCaller), idList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, len(idList), len(actualComments))
		})
	}
}

func TestGetComments(t *testing.T) {
	threadID := "thread-1"
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"

	type testCase struct {
		name            string
		thread          *models.Thread
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get comments for pipeline thread",
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
		},
		{
			name: "successfully get comments for release thread",
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
		},
		{
			name:            "thread not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view pipeline comments",
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view release comments",
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockComments := db.NewMockComments(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			mockThreads.On("GetThreadByID", mock.Anything, threadID).Return(test.thread, nil)

			if test.thread != nil {
				if test.thread.PipelineID != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.thread.PipelineID).Return(&models.Pipeline{
						Metadata: models.ResourceMetadata{
							ID: "pipeline-1",
						},
						ProjectID: projectID,
					}, nil)
				}

				if test.thread.ReleaseID != nil {
					mockReleases.On("GetReleaseByID", mock.Anything, *test.thread.ReleaseID).Return(&models.Release{
						Metadata: models.ResourceMetadata{
							ID: "release-1",
						},
						ProjectID: projectID,
					}, nil)
				}

				mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockComments.On("GetComments", mock.Anything, &db.GetCommentsInput{
					Filter: &db.CommentFilter{
						ThreadID: &threadID,
					},
				}).Return(&db.CommentsResult{}, nil)
			}

			dbClient := &db.Client{
				Threads:   mockThreads,
				Comments:  mockComments,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			comments, err := service.GetComments(auth.WithCaller(ctx, mockCaller), &GetCommentsInput{ThreadID: threadID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, comments)
		})
	}
}

func TestGetThreadByID(t *testing.T) {
	threadID := "thread-1"
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"

	type testCase struct {
		name            string
		thread          *models.Thread
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get a pipeline thread by id",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipelineID,
			},
		},
		{
			name: "successfully get a release thread by id",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &releaseID,
			},
		},
		{
			name:            "thread not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view pipeline thread",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipelineID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view release thread",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &releaseID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			mockThreads.On("GetThreadByID", mock.Anything, threadID).Return(test.thread, nil)

			if test.thread != nil {
				if test.thread.PipelineID != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.thread.PipelineID).Return(&models.Pipeline{
						Metadata: models.ResourceMetadata{
							ID: pipelineID,
						},
						ProjectID: projectID,
					}, nil)
				}

				if test.thread.ReleaseID != nil {
					mockReleases.On("GetReleaseByID", mock.Anything, *test.thread.ReleaseID).Return(&models.Release{
						Metadata: models.ResourceMetadata{
							ID: releaseID,
						},
						ProjectID: projectID,
					}, nil)
				}

				mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Threads:   mockThreads,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualThread, err := service.GetThreadByID(auth.WithCaller(ctx, mockCaller), threadID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.thread, actualThread)
		})
	}
}

func TestGetThreadByPRN(t *testing.T) {
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"
	threadPRN := models.ThreadResource.BuildPRN("gid-thread-1")

	type testCase struct {
		name            string
		thread          *models.Thread
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get a pipeline thread by id",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					PRN: threadPRN,
				},
				PipelineID: &pipelineID,
			},
		},
		{
			name: "successfully get a release thread by id",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					PRN: threadPRN,
				},
				ReleaseID: &releaseID,
			},
		},
		{
			name:            "thread not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view pipeline thread",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					PRN: threadPRN,
				},
				PipelineID: &pipelineID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view release thread",
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					PRN: threadPRN,
				},
				ReleaseID: &releaseID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			mockThreads.On("GetThreadByPRN", mock.Anything, threadPRN).Return(test.thread, nil)

			if test.thread != nil {
				if test.thread.PipelineID != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.thread.PipelineID).Return(&models.Pipeline{
						Metadata: models.ResourceMetadata{
							ID: pipelineID,
						},
						ProjectID: projectID,
					}, nil)
				}

				if test.thread.ReleaseID != nil {
					mockReleases.On("GetReleaseByID", mock.Anything, *test.thread.ReleaseID).Return(&models.Release{
						Metadata: models.ResourceMetadata{
							ID: releaseID,
						},
						ProjectID: projectID,
					}, nil)
				}

				mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Threads:   mockThreads,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualThread, err := service.GetThreadByPRN(auth.WithCaller(ctx, mockCaller), threadPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.thread, actualThread)
		})
	}
}

func TestGetThreadsByIDs(t *testing.T) {
	idList := []string{"thread-1", "thread-2"}
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"

	type testCase struct {
		authError       error
		name            string
		expectErrorCode errors.CodeType
		threads         []models.Thread
	}

	testCases := []testCase{
		{
			name: "successfully get threads by ids",
			threads: []models.Thread{
				{
					PipelineID: &pipelineID,
				},
				{
					ReleaseID: &releaseID,
				},
			},
		},
		{
			name:            "subject is not authorized to view thread",
			threads:         []models.Thread{{PipelineID: &pipelineID}},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			mockThreads.On("GetThreads", mock.Anything, &db.GetThreadsInput{
				Filter: &db.ThreadFilter{
					ThreadIDs: idList,
				},
			}).Return(&db.ThreadsResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(test.threads)),
				},
				Threads: test.threads,
			}, nil)

			mockPipelines.On("GetPipelines", mock.Anything, &db.GetPipelinesInput{
				Filter: &db.PipelineFilter{
					PipelineIDs: []string{pipelineID},
				},
			}).Return(&db.PipelinesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				Pipelines: []models.Pipeline{
					{
						Metadata: models.ResourceMetadata{
							ID: pipelineID,
						},
						ProjectID: projectID,
					},
				},
			}, nil).Maybe()

			mockReleases.On("GetReleases", mock.Anything, &db.GetReleasesInput{
				Filter: &db.ReleaseFilter{
					ReleaseIDs: []string{releaseID},
				},
			}).Return(&db.ReleasesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				Releases: []*models.Release{
					{
						Metadata: models.ResourceMetadata{
							ID: releaseID,
						},
						ProjectID: projectID,
					},
				},
			}, nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError)

			dbClient := &db.Client{
				Threads:   mockThreads,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			threads, err := service.GetThreadsByIDs(auth.WithCaller(ctx, mockCaller), idList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, len(test.threads), len(threads))
		})
	}
}

func TestGetThreads(t *testing.T) {
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"

	type testCase struct {
		name            string
		input           *GetThreadsInput
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:  "successfully get pipeline threads",
			input: &GetThreadsInput{PipelineID: &pipelineID},
		},
		{
			name:  "successfully get release threads",
			input: &GetThreadsInput{ReleaseID: &releaseID},
		},
		{
			name:            "subject is not authorized to view pipeline threads",
			input:           &GetThreadsInput{PipelineID: &pipelineID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "subject is not authorized to view release threads",
			input:           &GetThreadsInput{ReleaseID: &releaseID},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "no pipeline or release id provided",
			input:           &GetThreadsInput{},
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockThreads := db.NewMockThreads(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)

			if test.input.PipelineID != nil {
				mockPipelines.On("GetPipelineByID", mock.Anything, *test.input.PipelineID).Return(&models.Pipeline{
					Metadata: models.ResourceMetadata{
						ID: pipelineID,
					},
					ProjectID: projectID,
				}, nil)
			}

			if test.input.ReleaseID != nil {
				mockReleases.On("GetReleaseByID", mock.Anything, *test.input.ReleaseID).Return(&models.Release{
					Metadata: models.ResourceMetadata{
						ID: releaseID,
					},
					ProjectID: projectID,
				}, nil)
			}

			mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError).Maybe()

			if test.expectErrorCode == "" {
				mockThreads.On("GetThreads", mock.Anything, &db.GetThreadsInput{
					Filter: &db.ThreadFilter{
						PipelineID: test.input.PipelineID,
						ReleaseID:  test.input.ReleaseID,
					},
				}).Return(&db.ThreadsResult{}, nil)
			}

			dbClient := &db.Client{
				Threads:   mockThreads,
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			threadResult, err := service.GetThreads(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.NotNil(t, threadResult)
		})
	}
}

func TestCreateComment(t *testing.T) {
	userID := "user-1"
	projectID := "project-1"
	threadID := "thread-1"
	serviceAccountID := "service-account-1"

	pipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: "pipeline-1",
		},
		ProjectID: projectID,
	}

	release := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: "release-1",
		},
		ProjectID: projectID,
	}

	sampleCommentText := "this is a comment"

	type testCase struct {
		name              string
		input             *CreateCommentInput
		thread            *models.Thread
		pipeline          *models.Pipeline
		release           *models.Release
		expectCreated     *models.Comment
		callerType        string
		authError         error
		threadLimitError  error
		commentLimitError error
		expectErrorCode   errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "user successfully starts a new pipeline thread",
			callerType: "user",
			input: &CreateCommentInput{
				PipelineID: &pipeline.Metadata.ID,
				Text:       sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline: pipeline,
			expectCreated: &models.Comment{
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
		},
		{
			name:       "user successfully starts a new release thread",
			callerType: "user",
			input: &CreateCommentInput{
				ReleaseID: &release.Metadata.ID,
				Text:      sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &release.Metadata.ID,
			},
			release: release,
			expectCreated: &models.Comment{
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
		},
		{
			name:       "user successfully comments on an existing pipeline thread",
			callerType: "user",
			input: &CreateCommentInput{
				ThreadID: &threadID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline: pipeline,
			expectCreated: &models.Comment{
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
		},
		{
			name:       "user successfully comments on an existing release thread",
			callerType: "user",
			input: &CreateCommentInput{
				ThreadID: &threadID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &release.Metadata.ID,
			},
			release: release,
			expectCreated: &models.Comment{
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
		},
		{
			name:       "service account successfully starts a new pipeline thread",
			callerType: "serviceAccount",
			input: &CreateCommentInput{
				PipelineID: &pipeline.Metadata.ID,
				Text:       sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline: pipeline,
			expectCreated: &models.Comment{
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
				Text:             sampleCommentText,
			},
		},
		{
			name:       "service account successfully starts a new release thread",
			callerType: "serviceAccount",
			input: &CreateCommentInput{
				ReleaseID: &release.Metadata.ID,
				Text:      sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &release.Metadata.ID,
			},
			release: release,
			expectCreated: &models.Comment{
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
				Text:             sampleCommentText,
			},
		},
		{
			name:       "service account successfully comments on an existing pipeline thread",
			callerType: "serviceAccount",
			input: &CreateCommentInput{
				ThreadID: &threadID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline: pipeline,
			expectCreated: &models.Comment{
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
				Text:             sampleCommentText,
			},
		},
		{
			name:       "service account successfully comments on an existing release thread",
			callerType: "serviceAccount",
			input: &CreateCommentInput{
				ThreadID: &threadID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &release.Metadata.ID,
			},
			release: release,
			expectCreated: &models.Comment{
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
				Text:             sampleCommentText,
			},
		},
		{
			name:       "thread not found",
			callerType: "user",
			input: &CreateCommentInput{
				ThreadID: &threadID,
				Text:     sampleCommentText,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:       "pipeline not found",
			callerType: "user",
			input: &CreateCommentInput{
				PipelineID: &pipeline.Metadata.ID,
				Text:       sampleCommentText,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:       "release not found",
			callerType: "user",
			input: &CreateCommentInput{
				ReleaseID: &release.Metadata.ID,
				Text:      sampleCommentText,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:       "subject does not have permission to start a pipeline thread",
			callerType: "user",
			input: &CreateCommentInput{
				PipelineID: &pipeline.Metadata.ID,
				Text:       sampleCommentText,
			},
			pipeline:        pipeline,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "subject does not have permission to start a release thread",
			callerType: "user",
			input: &CreateCommentInput{
				ReleaseID: &release.Metadata.ID,
				Text:      sampleCommentText,
			},
			release:         release,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "subject does not have permission to comment on existing pipeline thread",
			callerType: "user",
			input: &CreateCommentInput{
				ThreadID: &threadID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline:        pipeline,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "subject does not have permission to comment on existing release thread",
			callerType: "user",
			input: &CreateCommentInput{
				ThreadID: &threadID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &release.Metadata.ID,
			},
			release:         release,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "pipeline thread limit exceeded",
			callerType: "user",
			input: &CreateCommentInput{
				PipelineID: &pipeline.Metadata.ID,
				Text:       sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipeline.Metadata.ID,
			},
			pipeline:         pipeline,
			threadLimitError: errors.New("limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode:  errors.EInvalid,
		},
		{
			name:       "release thread limit exceeded",
			callerType: "user",
			input: &CreateCommentInput{
				ReleaseID: &release.Metadata.ID,
				Text:      sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &release.Metadata.ID,
			},
			release:          release,
			threadLimitError: errors.New("limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode:  errors.EInvalid,
		},
		{
			name:       "comments per thread limit exceeded",
			callerType: "user",
			input: &CreateCommentInput{
				ReleaseID: &release.Metadata.ID,
				Text:      sampleCommentText,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &release.Metadata.ID,
			},
			release: release,
			expectCreated: &models.Comment{
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
			commentLimitError: errors.New("limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode:   errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockThreads := db.NewMockThreads(t)
			mockReleases := db.NewMockReleases(t)
			mockComments := db.NewMockComments(t)
			mockPipelines := db.NewMockPipelines(t)
			mockAuthorizer := auth.NewMockAuthorizer(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.input.ThreadID != nil {
				mockThreads.On("GetThreadByID", mock.Anything, *test.input.ThreadID).Return(test.thread, nil)
			}

			mockPipelines.On("GetPipelineByID", mock.Anything, pipeline.Metadata.ID).Return(test.pipeline, nil).Maybe()

			mockReleases.On("GetReleaseByID", mock.Anything, release.Metadata.ID).Return(test.release, nil).Maybe()

			mockAuthorizer.On("RequirePermissions", mock.Anything, []models.Permission{models.CreateComment}, mock.Anything).Return(test.authError).Maybe()

			if test.expectErrorCode != errors.ENotFound && test.authError == nil {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

				if test.input.ThreadID == nil {
					mockThreads.On("CreateThread", mock.Anything, &models.Thread{
						PipelineID: test.input.PipelineID,
						ReleaseID:  test.input.ReleaseID,
					}).Return(test.thread, nil)

					mockThreads.On("GetThreads", mock.Anything, &db.GetThreadsInput{
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(0),
						},
						Filter: &db.ThreadFilter{
							PipelineID: test.input.PipelineID,
							ReleaseID:  test.input.ReleaseID,
						},
					}).Return(&db.ThreadsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
					}, nil)

					mockLimitChecker.On("CheckLimit", mock.Anything, mock.MatchedBy(func(l limits.ResourceLimitName) bool {
						// Ensure right resource limit name is passed in.
						if test.input.PipelineID != nil {
							return l == limits.ResourceLimitThreadsPerPipeline
						}

						return l == limits.ResourceLimitThreadsPerRelease
					}), int32(1)).Return(test.threadLimitError)
				}

				if test.threadLimitError == nil {
					mockComments.On("CreateComment", mock.Anything, test.expectCreated).Return(test.expectCreated, nil)

					mockComments.On("GetComments", mock.Anything, &db.GetCommentsInput{
						PaginationOptions: &pagination.Options{
							First: ptr.Int32(0), // Don't return the data, because we only want the total count.
						},
						Filter: &db.CommentFilter{
							ThreadID: &threadID,
						},
					}).Return(&db.CommentsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
					}, nil)

					mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitCommentsPerThread, int32(1)).Return(test.commentLimitError)
				}

				if test.expectErrorCode == "" {
					mockActivityEvents.On("CreateActivityEvent", mock.Anything, mock.MatchedBy(func(i *activityevent.CreateActivityEventInput) bool {
						if test.input.ThreadID == nil {
							return i.Action == models.ActionCreate &&
								ptr.ToString(i.TargetID) == threadID &&
								ptr.ToString(i.ProjectID) == projectID &&
								i.TargetType == models.TargetThread
						}

						return i.Action == models.ActionCreate &&
							ptr.ToString(i.TargetID) == test.expectCreated.Metadata.ID &&
							ptr.ToString(i.ProjectID) == projectID &&
							i.TargetType == models.TargetComment
					})).Return(nil, nil)

					mockTransactions.On("CommitTx", mock.Anything).Return(nil)
				}
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Threads:      mockThreads,
				Comments:     mockComments,
				Releases:     mockReleases,
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
			}

			// Determine the type of caller we'll use.
			var testCaller auth.Caller
			switch test.callerType {
			case "user":
				testCaller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userID,
						},
						Username: "test-user",
					},
					mockAuthorizer,
					dbClient,
				)
			case "serviceAccount":
				testCaller = auth.NewServiceAccountCaller(
					serviceAccountID,
					models.ServiceAccountResource.BuildPRN("test-sa"),
					mockAuthorizer,
					dbClient,
				)
			default:
				t.Fatalf("unknown test caller type %q", test.callerType)
			}

			createdComment, err := service.CreateComment(auth.WithCaller(ctx, testCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectCreated, createdComment)
		})
	}
}

func TestUpdateComment(t *testing.T) {
	userID := "user-1"
	threadID := "thread-1"
	commentID := "comment-1"
	projectID := "project-1"
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	serviceAccountID := "service-account-1"
	sampleCommentText := "this is a sample comment"

	type testCase struct {
		name            string
		callerType      string
		comment         *models.Comment
		thread          *models.Thread
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "user successfully updates pipeline thread comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
		},
		{
			name:       "user successfully updates release thread comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
		},
		{
			name:       "service account successfully updates pipeline thread comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
				Text:             sampleCommentText,
			},
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
		},
		{
			name:       "service account successfully updates release thread comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
				Text:             sampleCommentText,
			},
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
		},
		{
			name:       "a user cannot update another user's comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   ptr.String("user-2"),
				Text:     sampleCommentText,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "a user cannot update a service account's comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: ptr.String("service-account-2"),
				Text:             sampleCommentText,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "a service account cannot update another service account's comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: ptr.String("service-account-2"),
				Text:             sampleCommentText,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "a service account cannot update a user's comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   ptr.String("user-2"),
				Text:     sampleCommentText,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "subject is not authorized to update pipeline thread comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				PipelineID: &pipelineID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "subject is not authorized to update release thread comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
				Text:     sampleCommentText,
			},
			thread: &models.Thread{
				ReleaseID: &releaseID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockThreads := db.NewMockThreads(t)
			mockComments := db.NewMockComments(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)
			mockAuthorizer := auth.NewMockAuthorizer(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockComments.On("GetCommentByID", mock.Anything, commentID).Return(test.comment, nil)

			if test.comment != nil && (ptr.ToString(test.comment.UserID) == userID || ptr.ToString(test.comment.ServiceAccountID) == serviceAccountID) {
				mockThreads.On("GetThreadByID", mock.Anything, threadID).Return(test.thread, nil)

				if test.thread.PipelineID != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.thread.PipelineID).Return(&models.Pipeline{
						Metadata: models.ResourceMetadata{
							ID: *test.thread.PipelineID,
						},
						ProjectID: projectID,
					}, nil)
				}

				if test.thread.ReleaseID != nil {
					mockReleases.On("GetReleaseByID", mock.Anything, *test.thread.ReleaseID).Return(&models.Release{
						Metadata: models.ResourceMetadata{
							ID: *test.thread.ReleaseID,
						},
						ProjectID: projectID,
					}, nil)
				}

				mockAuthorizer.On("RequirePermissions", mock.Anything, []models.Permission{models.CreateComment}, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockComments.On("UpdateComment", mock.Anything, test.comment).Return(test.comment, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionUpdate,
					TargetID:   &commentID,
					TargetType: models.TargetComment,
				}).Return(nil, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Threads:      mockThreads,
				Comments:     mockComments,
				Releases:     mockReleases,
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			// Determine the type of caller we'll use.
			var testCaller auth.Caller
			switch test.callerType {
			case "user":
				testCaller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userID,
						},
						Username: "test-user",
					},
					mockAuthorizer,
					dbClient,
				)
			case "serviceAccount":
				testCaller = auth.NewServiceAccountCaller(
					serviceAccountID,
					models.ServiceAccountResource.BuildPRN("test-sa"),
					mockAuthorizer,
					dbClient,
				)
			default:
				t.Fatalf("unknown test caller type %q", test.callerType)
			}

			updatedComment, err := service.UpdateComment(auth.WithCaller(ctx, testCaller), &UpdateCommentInput{
				ID: commentID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.comment, updatedComment)
		})
	}
}

func TestDeleteComment(t *testing.T) {
	userID := "user-1"
	threadID := "thread-1"
	commentID := "comment-1"
	projectID := "project-1"
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	serviceAccountID := "service-account-1"

	type testCase struct {
		authError              error
		comment                *models.Comment
		thread                 *models.Thread
		name                   string
		callerType             string
		expectErrorCode        errors.CodeType
		remainingCommentsCount int32
	}

	testCases := []testCase{
		{
			name:       "user successfully deletes their pipeline comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipelineID,
			},
			remainingCommentsCount: 2,
		},
		{
			name:       "user successfully deletes their release comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &releaseID,
			},
			remainingCommentsCount: 2,
		},
		{
			name:       "user successfully deletes their pipeline thread",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipelineID,
			},
		},
		{
			name:       "user successfully deletes their release thread",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &releaseID,
			},
		},
		{
			name:       "service account successfully deletes their pipeline comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipelineID,
			},
			remainingCommentsCount: 2,
		},
		{
			name:       "service account successfully deletes their release comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &releaseID,
			},
			remainingCommentsCount: 2,
		},
		{
			name:       "service account successfully deletes their pipeline thread",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipelineID,
			},
		},
		{
			name:       "service account successfully deletes their release thread",
			callerType: "serviceAccount",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID:         threadID,
				ServiceAccountID: &serviceAccountID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &releaseID,
			},
		},
		{
			name:            "comment not found",
			callerType:      "user",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:       "user cannot delete another user's comment",
			callerType: "user",
			comment: &models.Comment{
				ThreadID: threadID,
				UserID:   ptr.String("user-2"),
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "a user cannot delete a service account's comment",
			callerType: "user",
			comment: &models.Comment{
				ThreadID:         threadID,
				ServiceAccountID: ptr.String("service-account-2"),
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "service account cannot delete another service account's comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				ThreadID:         threadID,
				ServiceAccountID: ptr.String("service-account-2"),
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "a service account cannot delete a user's comment",
			callerType: "serviceAccount",
			comment: &models.Comment{
				ThreadID: threadID,
				UserID:   ptr.String("user-2"),
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "subject is not authorized to update pipeline thread comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				PipelineID: &pipelineID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:       "subject is not authorized to update release thread comment",
			callerType: "user",
			comment: &models.Comment{
				Metadata: models.ResourceMetadata{
					ID: commentID,
				},
				ThreadID: threadID,
				UserID:   &userID,
			},
			thread: &models.Thread{
				Metadata: models.ResourceMetadata{
					ID: threadID,
				},
				ReleaseID: &releaseID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockThreads := db.NewMockThreads(t)
			mockComments := db.NewMockComments(t)
			mockReleases := db.NewMockReleases(t)
			mockPipelines := db.NewMockPipelines(t)
			mockAuthorizer := auth.NewMockAuthorizer(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockComments.On("GetCommentByID", mock.Anything, commentID).Return(test.comment, nil)

			if test.comment != nil && (ptr.ToString(test.comment.UserID) == userID || ptr.ToString(test.comment.ServiceAccountID) == serviceAccountID) {
				mockThreads.On("GetThreadByID", mock.Anything, threadID).Return(test.thread, nil)

				if test.thread.PipelineID != nil {
					mockPipelines.On("GetPipelineByID", mock.Anything, *test.thread.PipelineID).Return(&models.Pipeline{
						Metadata: models.ResourceMetadata{
							ID: *test.thread.PipelineID,
						},
						ProjectID: projectID,
					}, nil)
				}

				if test.thread.ReleaseID != nil {
					mockReleases.On("GetReleaseByID", mock.Anything, *test.thread.ReleaseID).Return(&models.Release{
						Metadata: models.ResourceMetadata{
							ID: *test.thread.ReleaseID,
						},
						ProjectID: projectID,
					}, nil)
				}

				mockAuthorizer.On("RequirePermissions", mock.Anything, []models.Permission{models.CreateComment}, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockComments.On("DeleteComment", mock.Anything, test.comment).Return(nil)
				mockComments.On("GetComments", mock.Anything, &db.GetCommentsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
					Filter: &db.CommentFilter{
						ThreadID: &threadID,
					},
				}).Return(&db.CommentsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: test.remainingCommentsCount,
					},
				}, nil)

				var (
					targetID     = test.thread.PipelineID
					targetType   = models.TargetPipeline
					resourceID   = test.comment.Metadata.ID
					resourceType = models.TargetComment
				)

				if test.thread.ReleaseID != nil {
					targetID = test.thread.ReleaseID
					targetType = models.TargetRelease
				}

				if test.remainingCommentsCount == 0 {
					resourceID = test.thread.Metadata.ID
					resourceType = models.TargetThread
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					TargetID:   targetID,
					TargetType: targetType,
					Action:     models.ActionDelete,
					Payload: &models.ActivityEventDeleteResourcePayload{
						ID:   resourceID,
						Type: resourceType.String(),
					},
				}).Return(nil, nil)

				if test.remainingCommentsCount == 0 {
					mockThreads.On("DeleteThread", mock.Anything, test.thread).Return(nil)
				}
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Threads:      mockThreads,
				Comments:     mockComments,
				Releases:     mockReleases,
				Pipelines:    mockPipelines,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			// Determine the type of caller we'll use.
			var testCaller auth.Caller
			switch test.callerType {
			case "user":
				testCaller = auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: userID,
						},
						Username: "test-user",
					},
					mockAuthorizer,
					dbClient,
				)
			case "serviceAccount":
				testCaller = auth.NewServiceAccountCaller(
					serviceAccountID,
					models.ServiceAccountResource.BuildPRN("test-sa"),
					mockAuthorizer,
					dbClient,
				)
			default:
				t.Fatalf("unknown test caller type %q", test.callerType)
			}

			err := service.DeleteComment(auth.WithCaller(ctx, testCaller), &DeleteCommentInput{
				ID: commentID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestComments_processReleaseCommentMentions(t *testing.T) {
	projectID := "project-1"

	// Give our sample users and teams some memberships.
	participantsWithMembership := []string{"user-id-1", "user-id-2", "team-id-1", "team-id-2"}

	type testCase struct {
		usernameToID    map[string]string
		teamNameToID    map[string]string
		name            string
		text            string
		authError       error
		expectErrorCode errors.CodeType
		isAdmin         bool
	}

	tests := []testCase{
		{
			name: "no mentions",
			text: "Comment without mentions",
		},
		{
			name: "mentions users",
			text: "Comment with @user1 and @user2",
			usernameToID: map[string]string{
				"user1": "user-id-1",
				"user2": "user-id-2",
			},
		},
		{
			name: "mentions teams",
			text: "Comment with @team1 and @team2",
			teamNameToID: map[string]string{
				"team1": "team-id-1",
				"team2": "team-id-2",
			},
		},
		{
			name: "admin mentions users and teams",
			text: "Comment with @user1 and @team1 and @team_2 and @user.3, and @user-4",
			usernameToID: map[string]string{
				"user1":  "user-id-1",
				"user.3": "user-id-3",
				"user-4": "user-id-4",
			},
			teamNameToID: map[string]string{
				"team1":  "team-id-1",
				"team_2": "team-id-2",
			},
			isAdmin: true,
		},
		{
			name: "characters after mention are ignored",
			text: "Comment with @user-1, @team-2, and @user-3. And @user-4, and @team-5",
			usernameToID: map[string]string{
				"user-1": "user-id-1",
				"user-3": "user-id-3",
				"user-4": "user-id-4",
			},
			teamNameToID: map[string]string{
				"team-2": "team-id-2",
				"team-5": "team-id-5",
			},
		},
		{
			name: "user not found",
			text: "Comment with @user-1 and @user-2",
			usernameToID: map[string]string{
				"user-1": "user-id-1",
			},
		},
		{
			name: "team not found",
			text: "Comment with @team-1 and @team-2",
			teamNameToID: map[string]string{
				"team-1": "team-id-1",
			},
		},
		{
			name:            "subject is not authorized to update release participants",
			text:            "Comment with @user1 and @user2",
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockMemberships := db.NewMockMemberships(t)
			mockUsers := db.NewMockUsers(t)
			mockTeams := db.NewMockTeams(t)
			mockReleases := db.NewMockReleases(t)

			expectUserMentions := len(test.usernameToID)
			expectTeamMentions := len(test.teamNameToID)

			// Sample release we'll be creating comments for.
			release := &models.Release{
				Metadata: models.ResourceMetadata{
					ID: "release-1",
				},
				ProjectID: projectID,
			}

			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.UpdateRelease, mock.Anything).Return(test.authError).Maybe()

			if expectUserMentions > 0 || expectTeamMentions > 0 {
				mockMemberships.On("GetMemberships", mock.Anything, &db.GetMembershipsInput{
					Filter: &db.MembershipFilter{
						ProjectID: &projectID,
						MembershipScopes: []models.ScopeType{
							models.OrganizationScope,
							models.ProjectScope,
							models.GlobalScope,
						},
					},
				}).Return(func(_ context.Context, _ *db.GetMembershipsInput) (*db.MembershipsResult, error) {
					memberships := make([]models.Membership, 0, len(participantsWithMembership))
					for ix := range participantsWithMembership {
						memberships = append(memberships, models.Membership{
							UserID: &participantsWithMembership[ix],
						})
					}
					return &db.MembershipsResult{Memberships: memberships}, nil
				})

				mockUsers.On("GetUsers", mock.Anything, mock.Anything).Return(
					func(_ context.Context, input *db.GetUsersInput) (*db.UsersResult, error) {
						require.NotNil(t, input.Filter)
						require.NotEmpty(t, input.Filter.Usernames)

						users := make([]models.User, 0, len(input.Filter.Usernames))
						for _, username := range input.Filter.Usernames {
							id, ok := test.usernameToID[username]
							if !ok {
								// User not found, continue
								continue
							}

							users = append(users, models.User{
								Metadata: models.ResourceMetadata{
									ID: id,
								},
								Username: username,
							})
						}

						return &db.UsersResult{Users: users}, nil
					},
				)
			}

			mockTeams.On("GetTeams", mock.Anything, mock.Anything).Return(
				func(_ context.Context, input *db.GetTeamsInput) (*db.TeamsResult, error) {
					require.NotNil(t, input.Filter)
					require.NotEmpty(t, input.Filter.TeamNames)

					teams := make([]models.Team, 0, len(input.Filter.TeamNames))
					for _, name := range input.Filter.TeamNames {
						id, ok := test.teamNameToID[name]
						if !ok {
							// Team not found, continue
							continue
						}

						teams = append(teams, models.Team{
							Metadata: models.ResourceMetadata{
								ID: id,
							},
							Name: name,
						})
					}

					return &db.TeamsResult{Teams: teams}, nil
				},
			).Maybe()

			mockReleases.On("UpdateRelease", mock.Anything, mock.Anything).Return(
				func(_ context.Context, input *models.Release) (*models.Release, error) {
					// Ensure only participants with memberships are added to the release.
					for _, userID := range input.UserParticipantIDs {
						require.Contains(t, participantsWithMembership, userID)
					}

					for _, teamID := range input.TeamParticipantIDs {
						require.Contains(t, participantsWithMembership, teamID)
					}

					return input, nil
				},
			).Maybe()

			dbClient := &db.Client{
				Memberships: mockMemberships,
				Users:       mockUsers,
				Teams:       mockTeams,
				Releases:    mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			err := service.processReleaseCommentMentions(auth.WithCaller(ctx, mockCaller), release, test.text)
			assert.NoError(t, err)
		})
	}
}

func TestComments_requireViewerAccessToComment(t *testing.T) {
	pipelineID := "pipeline-1"
	releaseID := "release-1"
	projectID := "project-1"

	pipeline := &models.Pipeline{
		Metadata: models.ResourceMetadata{
			ID: pipelineID,
		},
		ProjectID: projectID,
	}

	release := &models.Release{
		Metadata: models.ResourceMetadata{
			ID: releaseID,
		},
		ProjectID: projectID,
	}

	type testCase struct {
		name            string
		pipelineID      *string
		releaseID       *string
		pipeline        *models.Pipeline
		release         *models.Release
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:       "subject is authorized to view pipeline comment",
			pipelineID: &pipelineID,
			pipeline:   pipeline,
		},
		{
			name:      "subject is authorized to view release comment",
			releaseID: &releaseID,
			release:   release,
		},
		{
			name:            "subject is not authorized to view pipeline comment",
			pipelineID:      &pipelineID,
			pipeline:        pipeline,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "subject is not authorized to view release comment",
			releaseID:       &releaseID,
			release:         release,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "pipeline not found",
			pipelineID:      &pipelineID,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "release not found",
			releaseID:       &releaseID,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "required pipeline or release id not provided",
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockReleases := db.NewMockReleases(t)

			if test.pipelineID != nil {
				mockPipelines.On("GetPipelineByID", mock.Anything, *test.pipelineID).Return(test.pipeline, nil)
			}

			if test.releaseID != nil {
				mockReleases.On("GetReleaseByID", mock.Anything, *test.releaseID).Return(test.release, nil)
			}

			if test.pipeline != nil || test.release != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewComment, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Pipelines: mockPipelines,
				Releases:  mockReleases,
			}

			service := &service{
				dbClient: dbClient,
			}

			err := service.requireViewerAccessToComment(auth.WithCaller(ctx, mockCaller), test.pipelineID, test.releaseID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			assert.NoError(t, err)
		})
	}
}
