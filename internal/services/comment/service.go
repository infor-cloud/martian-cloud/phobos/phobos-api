// Package comment implements functionality related to Phobos comments.
package comment

import (
	"context"
	"regexp"
	"slices"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// mentionRegex is the regular expression used to find mentions in a comment.
var mentionRegex = regexp.MustCompile(`@[a-z0-9]+(?:[_.-][a-z0-9]+)*`)

// GetCommentsInput is the input for querying a list of comments
type GetCommentsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.CommentSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Search is used to search for a comment
	Search *string
	// ThreadID to filter comments by
	ThreadID string
}

// GetThreadsInput is the input for querying a list of threads
type GetThreadsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ThreadSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// PipelineID filters the threads by the specified pipeline
	PipelineID *string
	// ReleaseID filters the threads by the specified release
	ReleaseID *string
}

// CreateCommentInput is the input for creating a comment
type CreateCommentInput struct {
	ThreadID   *string
	PipelineID *string
	ReleaseID  *string
	Text       string
}

// UpdateCommentInput is the input for updating a comment
type UpdateCommentInput struct {
	Version *int
	Text    string
	ID      string
}

// DeleteCommentInput is the input for deleting a comment
type DeleteCommentInput struct {
	Version *int
	ID      string
}

// SubscribeToCommentsInput is the input for subscribing to a single comment
type SubscribeToCommentsInput struct {
	PipelineID *string
	ReleaseID  *string
}

// Event is a comment event
type Event struct {
	Comment *models.Comment
	Action  string
}

// Service implements all comment related functionality
type Service interface {
	GetCommentByID(ctx context.Context, id string) (*models.Comment, error)
	GetCommentsByIDs(ctx context.Context, idList []string) ([]models.Comment, error)
	GetCommentByPRN(ctx context.Context, prn string) (*models.Comment, error)
	GetComments(ctx context.Context, input *GetCommentsInput) (*db.CommentsResult, error)
	GetThreadByID(ctx context.Context, id string) (*models.Thread, error)
	GetThreadByPRN(ctx context.Context, prn string) (*models.Thread, error)
	GetThreadsByIDs(ctx context.Context, idList []string) ([]models.Thread, error)
	GetThreads(ctx context.Context, input *GetThreadsInput) (*db.ThreadsResult, error)
	CreateComment(ctx context.Context, input *CreateCommentInput) (*models.Comment, error)
	UpdateComment(ctx context.Context, input *UpdateCommentInput) (*models.Comment, error)
	DeleteComment(ctx context.Context, input *DeleteCommentInput) error
	SubscribeToComments(ctx context.Context, options *SubscribeToCommentsInput) (<-chan *Event, error)
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	limitChecker    limits.LimitChecker
	eventManager    *events.EventManager
	activityService activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	eventManager *events.EventManager,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		eventManager:    eventManager,
		activityService: activityService,
	}
}

func (s *service) SubscribeToComments(ctx context.Context, options *SubscribeToCommentsInput) (<-chan *Event, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToComments")
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	// Check whether the caller is allowed to view/subscribe to this comment.
	if err := s.requireViewerAccessToComment(ctx, options.PipelineID, options.ReleaseID); err != nil {
		return nil, err
	}

	// Don't pass an ID to the event manager.
	subscriber := s.eventManager.Subscribe([]events.Subscription{
		{
			Type: events.CommentSubscription,
			// Only subscribe to create and update since the filtering logic for delete won't
			// currently work because when it queries for the comment it'll be deleted.
			Actions: []events.SubscriptionAction{events.CreateAction, events.UpdateAction},
		},
	})

	outgoing := make(chan *Event)
	go func() {
		// Defer close of outgoing channel
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		// Wait for updates
		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) {
					s.logger.Errorf("Error occurred while waiting for comment events: %v", err)
				}
				return
			}

			eventData, err := event.ToCommentEventData()
			if err != nil {
				s.logger.Errorf("failed to get comment event data in comment subscription: %v", err)
				continue
			}

			// Must filter by pipeline/release ID.
			thread, err := s.dbClient.Threads.GetThreads(ctx, &db.GetThreadsInput{
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
				Filter: &db.ThreadFilter{
					PipelineID: options.PipelineID,
					ReleaseID:  options.ReleaseID,
					ThreadIDs:  []string{eventData.ThreadID},
				},
			})
			if err != nil {
				s.logger.Errorf("Error querying for thread associated with comment event: %v", err)
				continue
			}

			if thread.PageInfo.TotalCount == 0 {
				// Ignore this comment, because it does not match what we are filtering for.
				continue
			}

			comment, err := s.dbClient.Comments.GetCommentByID(ctx, event.ID)
			if err != nil {
				s.logger.Errorf("Error querying for comment in comment subscription goroutine: %v", err)
				continue
			}
			if comment == nil {
				s.logger.Errorf("Received event for comment that does not exist %s", event.ID)
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- &Event{Comment: comment, Action: event.Action}:
			}
		}
	}()

	return outgoing, nil
}

func (s *service) GetCommentByID(ctx context.Context, id string) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "svc.GetCommentByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	comment, err := s.getCommentByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	thread, err := s.getThreadByID(ctx, span, comment.ThreadID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this comment.
	if err = s.requireViewerAccessToComment(ctx, thread.PipelineID, thread.ReleaseID); err != nil {
		return nil, err
	}

	return comment, nil
}

func (s *service) GetCommentByPRN(ctx context.Context, prn string) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "svc.GetCommentByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	comment, err := s.dbClient.Comments.GetCommentByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get comment by PRN", errors.WithSpan(span))
	}

	if comment == nil {
		return nil, errors.New(
			"comment with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	thread, err := s.getThreadByID(ctx, span, comment.ThreadID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this comment.
	if err = s.requireViewerAccessToComment(ctx, thread.PipelineID, thread.ReleaseID); err != nil {
		return nil, err
	}

	return comment, nil
}

func (s *service) GetCommentsByIDs(ctx context.Context, idList []string) ([]models.Comment, error) {
	ctx, span := tracer.Start(ctx, "svc.GetCommentsByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetCommentsInput{
		Filter: &db.CommentFilter{
			CommentIDs: idList,
		},
	}

	resp, err := s.dbClient.Comments.GetComments(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get comments", errors.WithSpan(span))
	}

	if resp.PageInfo.TotalCount == 0 {
		// Just return since no comments were found.
		return []models.Comment{}, nil
	}

	threadIDs := []string{}
	for _, comment := range resp.Comments {
		threadIDs = append(threadIDs, comment.ThreadID)
	}

	threads, err := s.dbClient.Threads.GetThreads(ctx, &db.GetThreadsInput{
		Filter: &db.ThreadFilter{
			ThreadIDs: threadIDs,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get threads", errors.WithSpan(span))
	}

	if threads.PageInfo.TotalCount == 0 {
		// Make sure we have some threads otherwise this is an error.
		return nil, errors.New("threads query returned no results", errors.WithSpan(span))
	}

	var (
		pipelineIDs = []string{}
		releaseIDs  = []string{}
		projectIDs  = map[string]struct{}{}
	)

	for _, thread := range threads.Threads {
		switch {
		case thread.PipelineID != nil:
			pipelineIDs = append(pipelineIDs, *thread.PipelineID)
		case thread.ReleaseID != nil:
			releaseIDs = append(releaseIDs, *thread.ReleaseID)
		default:
			return nil, errors.New("thread is associated with an unknown resource", errors.WithSpan(span))
		}
	}

	if len(pipelineIDs) > 0 {
		pipelines, pErr := s.dbClient.Pipelines.GetPipelines(ctx, &db.GetPipelinesInput{
			Filter: &db.PipelineFilter{
				PipelineIDs: pipelineIDs,
			},
		})
		if pErr != nil {
			return nil, errors.Wrap(pErr, "failed to get pipelines", errors.WithSpan(span))
		}

		if pipelines.PageInfo.TotalCount == 0 {
			// Make sure we have some pipelines otherwise this is an error.
			return nil, errors.New("pipelines query returned no results", errors.WithSpan(span))
		}

		for _, pipeline := range pipelines.Pipelines {
			projectIDs[pipeline.ProjectID] = struct{}{}
		}
	}

	if len(releaseIDs) > 0 {
		releases, rErr := s.dbClient.Releases.GetReleases(ctx, &db.GetReleasesInput{
			Filter: &db.ReleaseFilter{
				ReleaseIDs: releaseIDs,
			},
		})
		if rErr != nil {
			return nil, errors.Wrap(rErr, "failed to get releases", errors.WithSpan(span))
		}

		if releases.PageInfo.TotalCount == 0 {
			// Make sure we have some releases otherwise this is an error.
			return nil, errors.New("releases query returned no results", errors.WithSpan(span))
		}

		for _, release := range releases.Releases {
			projectIDs[release.ProjectID] = struct{}{}
		}
	}

	// Check whether the caller is allowed to view these comments.
	for id := range projectIDs {
		if err = caller.RequirePermission(ctx, models.ViewComment, auth.WithProjectID(id)); err != nil {
			return nil, err
		}
	}

	return resp.Comments, nil
}

func (s *service) GetComments(ctx context.Context, input *GetCommentsInput) (*db.CommentsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetComments")
	span.SetAttributes(attribute.String("threadID", input.ThreadID))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	thread, err := s.getThreadByID(ctx, span, input.ThreadID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view comments in this thread.
	if err = s.requireViewerAccessToComment(ctx, thread.PipelineID, thread.ReleaseID); err != nil {
		return nil, err
	}

	dbInput := &db.GetCommentsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.CommentFilter{
			ThreadID: &input.ThreadID,
			Search:   input.Search,
		},
	}

	resp, err := s.dbClient.Comments.GetComments(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get comments", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) GetThreadByID(ctx context.Context, id string) (*models.Thread, error) {
	ctx, span := tracer.Start(ctx, "svc.GetThreadByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	thread, err := s.getThreadByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	// Check whether the caller is allowed to view this thread.
	if err = s.requireViewerAccessToComment(ctx, thread.PipelineID, thread.ReleaseID); err != nil {
		return nil, err
	}

	return thread, nil
}

func (s *service) GetThreadByPRN(ctx context.Context, prn string) (*models.Thread, error) {
	ctx, span := tracer.Start(ctx, "svc.GetThreadByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	thread, err := s.dbClient.Threads.GetThreadByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get thread by prn", errors.WithSpan(span))
	}

	if thread == nil {
		return nil, errors.New("thread with prn %s not found", prn,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	// Check whether the caller is allowed to view this thread.
	if err := s.requireViewerAccessToComment(ctx, thread.PipelineID, thread.ReleaseID); err != nil {
		return nil, err
	}

	return thread, nil
}

func (s *service) GetThreadsByIDs(ctx context.Context, idList []string) ([]models.Thread, error) {
	ctx, span := tracer.Start(ctx, "svc.GetThreadsByIDs")
	span.SetAttributes(attribute.StringSlice("id list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetThreadsInput{
		Filter: &db.ThreadFilter{
			ThreadIDs: idList,
		},
	}

	resp, err := s.dbClient.Threads.GetThreads(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get threads", errors.WithSpan(span))
	}

	var (
		pipelineIDs = []string{}
		releaseIDs  = []string{}
		projectIDs  = map[string]struct{}{}
	)

	// Ensure caller can view thread(s).
	for _, thread := range resp.Threads {
		switch {
		case thread.PipelineID != nil:
			pipelineIDs = append(pipelineIDs, *thread.PipelineID)
		case thread.ReleaseID != nil:
			releaseIDs = append(releaseIDs, *thread.ReleaseID)
		default:
			return nil, errors.New("thread is associated with an unknown resource", errors.WithSpan(span))
		}
	}

	if len(pipelineIDs) > 0 {
		pipelines, pErr := s.dbClient.Pipelines.GetPipelines(ctx, &db.GetPipelinesInput{
			Filter: &db.PipelineFilter{
				PipelineIDs: pipelineIDs,
			},
		})
		if pErr != nil {
			return nil, errors.Wrap(pErr, "failed to get pipelines", errors.WithSpan(span))
		}

		if pipelines.PageInfo.TotalCount == 0 {
			// Make sure we have some pipelines otherwise this is an error.
			return nil, errors.New("pipelines query returned no results", errors.WithSpan(span))
		}

		for _, pipeline := range pipelines.Pipelines {
			projectIDs[pipeline.ProjectID] = struct{}{}
		}
	}

	if len(releaseIDs) > 0 {
		releases, rErr := s.dbClient.Releases.GetReleases(ctx, &db.GetReleasesInput{
			Filter: &db.ReleaseFilter{
				ReleaseIDs: releaseIDs,
			},
		})
		if rErr != nil {
			return nil, errors.Wrap(rErr, "failed to get releases", errors.WithSpan(span))
		}

		if releases.PageInfo.TotalCount == 0 {
			// Make sure we have some releases otherwise this is an error.
			return nil, errors.New("releases query returned no results", errors.WithSpan(span))
		}

		for _, release := range releases.Releases {
			projectIDs[release.ProjectID] = struct{}{}
		}
	}

	// Check whether the caller is allowed to view these comments.
	for id := range projectIDs {
		if err = caller.RequirePermission(ctx, models.ViewComment, auth.WithProjectID(id)); err != nil {
			return nil, err
		}
	}

	return resp.Threads, nil
}

func (s *service) GetThreads(ctx context.Context, input *GetThreadsInput) (*db.ThreadsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetThreads")
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	// Check whether the caller is allowed to view threads for the pipeline / release.
	if err := s.requireViewerAccessToComment(ctx, input.PipelineID, input.ReleaseID); err != nil {
		return nil, err
	}

	dbInput := &db.GetThreadsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ThreadFilter{
			PipelineID: input.PipelineID,
			ReleaseID:  input.ReleaseID,
		},
	}

	return s.dbClient.Threads.GetThreads(ctx, dbInput)
}

func (s *service) CreateComment(ctx context.Context, input *CreateCommentInput) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateComment")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var (
		pipelineID    *string
		releaseID     *string
		threadRelease *models.Release
		projectID     string
	)

	if input.ThreadID != nil {
		// This is a part of an on-going thread, retrieve it to get the pipeline / release ID.
		thread, tErr := s.getThreadByID(ctx, span, *input.ThreadID)
		if tErr != nil {
			return nil, tErr
		}

		// Incase a pipeline or release ID was also passed in, make sure we're looking at the intended resource.
		switch {
		case thread.PipelineID != nil:
			if input.ReleaseID != nil || (input.PipelineID != nil && *thread.PipelineID != *input.PipelineID) {
				return nil, errors.New("thread does not belong to target resource",
					errors.WithErrorCode(errors.EInvalid),
					errors.WithSpan(span),
				)
			}
		case thread.ReleaseID != nil:
			if input.PipelineID != nil || (input.ReleaseID != nil && *thread.ReleaseID != *input.ReleaseID) {
				return nil, errors.New("thread does not belong to target resource",
					errors.WithErrorCode(errors.EInvalid),
					errors.WithSpan(span),
				)
			}
		}

		pipelineID = thread.PipelineID
		releaseID = thread.ReleaseID
	} else {
		// Use what's provided as the input as this will be a new thread.
		pipelineID = input.PipelineID
		releaseID = input.ReleaseID
	}

	switch {
	case pipelineID != nil:
		pipeline, pErr := s.getPipelineByID(ctx, span, *pipelineID)
		if pErr != nil {
			return nil, pErr
		}

		projectID = pipeline.ProjectID
	case releaseID != nil:
		release, rErr := s.getReleaseByID(ctx, span, *releaseID)
		if rErr != nil {
			return nil, rErr
		}

		threadRelease = release
		projectID = release.ProjectID
	default:
		return nil, errors.New("must specify either a thread, pipeline or release id when creating a comment",
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	// Check whether the caller is allowed to create comments.
	if err = caller.RequirePermission(ctx, models.CreateComment, auth.WithProjectID(projectID)); err != nil {
		return nil, err
	}

	toCreate := &models.Comment{
		Text: input.Text,
	}

	if err = auth.HandleCaller(ctx,
		func(_ context.Context, caller *auth.UserCaller) error {
			toCreate.UserID = &caller.User.Metadata.ID
			return nil
		},
		func(_ context.Context, caller *auth.ServiceAccountCaller) error {
			toCreate.ServiceAccountID = &caller.ServiceAccountID
			return nil
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to handle caller", errors.WithSpan(span))
	}

	// Validate model
	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate a comment model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateComment: %v", txErr)
		}
	}()

	if input.ThreadID == nil {
		// Comment is not part of an existing thread; create a new one.
		thread, tErr := s.dbClient.Threads.CreateThread(txContext, &models.Thread{
			PipelineID: pipelineID,
			ReleaseID:  releaseID,
		})
		if tErr != nil {
			return nil, errors.Wrap(tErr, "failed to create thread", errors.WithSpan(span))
		}

		// Get the current thread count for this resource, we must make sure it doesn't exceed the limit.
		threadsResult, tErr := s.dbClient.Threads.GetThreads(txContext, &db.GetThreadsInput{
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
			Filter: &db.ThreadFilter{
				PipelineID: pipelineID,
				ReleaseID:  releaseID,
			},
		})
		if tErr != nil {
			return nil, errors.Wrap(tErr, "failed to get threads", errors.WithSpan(span))
		}

		// Discern what limit we're supposed to check based on the resource ID.
		limitToCheck := limits.ResourceLimitThreadsPerPipeline
		if releaseID != nil {
			limitToCheck = limits.ResourceLimitThreadsPerRelease
		}

		if lErr := s.limitChecker.CheckLimit(txContext, limitToCheck, threadsResult.PageInfo.TotalCount); lErr != nil {
			return nil, errors.Wrap(lErr, "limit check failed", errors.WithSpan(span))
		}

		// Associate the comment with the new thread.
		toCreate.ThreadID = thread.Metadata.ID
	} else {
		// Comment is part of an existing thread, so use the ID passed in.
		toCreate.ThreadID = *input.ThreadID
	}

	comment, err := s.dbClient.Comments.CreateComment(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create comment", errors.WithSpan(span))
	}

	// Get the total count of comments on this thread so we can check whether we just exceeded the limit.
	resp, err := s.dbClient.Comments.GetComments(txContext, &db.GetCommentsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0), // Don't return the data, because we only want the total count.
		},
		Filter: &db.CommentFilter{
			ThreadID: &comment.ThreadID,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get comments", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitCommentsPerThread, resp.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "limit check failed", errors.WithSpan(span))
	}

	if releaseID != nil {
		if err = s.processReleaseCommentMentions(txContext, threadRelease, comment.Text); err != nil {
			return nil, errors.Wrap(err, "failed to process release comment mentions", errors.WithSpan(span))
		}
	}

	var (
		targetID   string
		targetType models.ActivityEventTargetType
	)

	if input.ThreadID != nil {
		// Comment is part of an on-going thread, create an event for just the comment.
		targetID = comment.Metadata.ID
		targetType = models.TargetComment
	} else {
		// This is a new thread, create an event for just the thread.
		targetID = comment.ThreadID
		targetType = models.TargetThread
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, &activityevent.CreateActivityEventInput{
		ProjectID:  &projectID,
		Action:     models.ActionCreate,
		TargetID:   &targetID,
		TargetType: targetType,
	}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a comment.",
		"caller", caller.GetSubject(),
		"commentID", comment.Metadata.ID,
		"threadID", comment.ThreadID,
	)

	return comment, nil
}

func (s *service) UpdateComment(ctx context.Context, input *UpdateCommentInput) (*models.Comment, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateComment")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	comment, err := s.getCommentByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Verify that this caller (user or service account) is the same as the one who created the comment.
	var userID, serviceAccountID *string
	if err = auth.HandleCaller(ctx,
		func(_ context.Context, caller *auth.UserCaller) error {
			userID = ptr.String(caller.User.Metadata.ID)
			return nil
		},
		func(_ context.Context, caller *auth.ServiceAccountCaller) error {
			serviceAccountID = ptr.String(caller.ServiceAccountID)
			return nil
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to handle caller", errors.WithSpan(span))
	}
	if (ptr.ToString(userID) != ptr.ToString(comment.UserID)) ||
		(ptr.ToString(serviceAccountID) != ptr.ToString(comment.ServiceAccountID)) {
		return nil, errors.New(
			"only the creator of a comment is allowed to update it",
			errors.WithErrorCode(errors.EForbidden))
	}

	thread, err := s.getThreadByID(ctx, span, comment.ThreadID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	var (
		threadRelease *models.Release
		projectID     string
	)

	switch {
	case thread.PipelineID != nil:
		pipeline, pErr := s.getPipelineByID(ctx, span, *thread.PipelineID)
		if pErr != nil {
			return nil, pErr
		}

		projectID = pipeline.ProjectID
	case thread.ReleaseID != nil:
		release, rErr := s.getReleaseByID(ctx, span, *thread.ReleaseID)
		if rErr != nil {
			return nil, rErr
		}

		threadRelease = release
		projectID = release.ProjectID
	default:
		return nil, errors.New("thread is associated with an unexpected resource", errors.WithSpan(span))
	}

	// Check whether the caller is allowed to Create comments (consequently update them).
	if err = caller.RequirePermission(ctx, models.CreateComment, auth.WithProjectID(projectID)); err != nil {
		return nil, err
	}

	// Update the comment's text field and validate the updated model.
	comment.Text = input.Text
	if err = comment.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate the updated comment model", errors.WithSpan(span))
	}

	if input.Version != nil {
		comment.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateComment: %v", txErr)
		}
	}()

	updatedComment, err := s.dbClient.Comments.UpdateComment(txContext, comment)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update comment", errors.WithSpan(span))
	}

	if thread.ReleaseID != nil {
		if err = s.processReleaseCommentMentions(txContext, threadRelease, updatedComment.Text); err != nil {
			return nil, errors.Wrap(err, "failed to process release comment mentions", errors.WithSpan(span))
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &projectID,
			Action:     models.ActionUpdate,
			TargetID:   &comment.Metadata.ID,
			TargetType: models.TargetComment,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a comment.",
		"caller", caller.GetSubject(),
		"commentID", comment.Metadata.ID,
		"threadID", thread.Metadata.ID,
	)

	return updatedComment, nil
}

func (s *service) DeleteComment(ctx context.Context, input *DeleteCommentInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteComment")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	comment, err := s.getCommentByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	// Verify that this caller (user or service account) is the same as the one who created the comment.
	var userID, serviceAccountID *string
	if err = auth.HandleCaller(ctx,
		func(_ context.Context, caller *auth.UserCaller) error {
			userID = ptr.String(caller.User.Metadata.ID)
			return nil
		},
		func(_ context.Context, caller *auth.ServiceAccountCaller) error {
			serviceAccountID = ptr.String(caller.ServiceAccountID)
			return nil
		},
	); err != nil {
		return errors.Wrap(err, "failed to handle caller", errors.WithSpan(span))
	}
	if (ptr.ToString(userID) != ptr.ToString(comment.UserID)) ||
		(ptr.ToString(serviceAccountID) != ptr.ToString(comment.ServiceAccountID)) {
		return errors.New(
			"only the creator of a comment is allowed to delete it",
			errors.WithErrorCode(errors.EForbidden))
	}

	thread, err := s.getThreadByID(ctx, span, comment.ThreadID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	var projectID string
	switch {
	case thread.PipelineID != nil:
		pipeline, pErr := s.getPipelineByID(ctx, span, *thread.PipelineID)
		if pErr != nil {
			return pErr
		}

		projectID = pipeline.ProjectID
	case thread.ReleaseID != nil:
		release, rErr := s.getReleaseByID(ctx, span, *thread.ReleaseID)
		if rErr != nil {
			return rErr
		}

		projectID = release.ProjectID
	default:
		return errors.New("thread is associated with an unexpected resource", errors.WithSpan(span))
	}

	// Check whether the caller is allowed to Create comments (consequently delete them).
	if err = caller.RequirePermission(ctx, models.CreateComment, auth.WithProjectID(projectID)); err != nil {
		return err
	}

	if input.Version != nil {
		comment.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteComment: %v", txErr)
		}
	}()

	if err = s.dbClient.Comments.DeleteComment(txContext, comment); err != nil {
		return errors.Wrap(err, "failed to delete comment", errors.WithSpan(span))
	}

	// Get the remaining comments count in this thread.
	// We need to delete the thread if this was the last comment.
	commentsResult, err := s.dbClient.Comments.GetComments(txContext, &db.GetCommentsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
		Filter: &db.CommentFilter{
			ThreadID: &thread.Metadata.ID,
		},
	})
	if err != nil {
		return errors.Wrap(err, "failed to get comments for thread", errors.WithSpan(span))
	}

	// Associate activity event with either pipeline or release parent resource.
	var (
		targetID            = thread.PipelineID
		targetType          = models.TargetPipeline
		deletedResourceID   = comment.Metadata.ID
		deletedResourceType = models.TargetComment
	)

	if commentsResult.PageInfo.TotalCount == 0 {
		deletedResourceID = thread.Metadata.ID
		deletedResourceType = models.TargetThread

		// No more comments; delete the thread.
		if err = s.dbClient.Threads.DeleteThread(txContext, thread); err != nil {
			return errors.Wrap(err, "failed to delete thread", errors.WithSpan(span))
		}
	}

	if thread.ReleaseID != nil {
		targetID = thread.ReleaseID
		targetType = models.TargetRelease
	}

	if _, err = s.activityService.CreateActivityEvent(txContext, &activityevent.CreateActivityEventInput{
		ProjectID:  &projectID,
		TargetID:   targetID,
		TargetType: targetType,
		Action:     models.ActionDelete,
		Payload: &models.ActivityEventDeleteResourcePayload{
			ID:   deletedResourceID,
			Type: deletedResourceType.String(),
		},
	}); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a comment.",
		"caller", caller.GetSubject(),
		"commentID", comment.Metadata.ID,
		"threadID", thread.Metadata.ID,
	)

	return nil
}

// processReleaseCommentMentions processes "@" mentions in a release comment and
// attempts to add the mentioned users and teams to the release's participants.
// Only considers mentions that are not already participants of the release, and
// are members of the project. If new participants are added, the release is updated
// otherwise the mentions are ignored.
func (s *service) processReleaseCommentMentions(ctx context.Context, release *models.Release, commentText string) error {
	matches := mentionRegex.FindAllString(commentText, -1)

	if len(matches) == 0 {
		// No mentions found.
		return nil
	}

	caller := auth.GetCaller(ctx)

	if caller == nil {
		// Should never happen.
		return errors.New("no caller found on context")
	}

	// Check perms for non admins.
	if !caller.IsAdmin() {
		if err := caller.RequirePermission(ctx, models.UpdateRelease, auth.WithProjectID(release.ProjectID)); err != nil {
			// If caller doesn't have perms to UpdateRelease (therefore add participants),
			// we'll just say goodbye here since this isn't an error.
			return nil
		}
	}

	mentions := map[string]struct{}{}
	for _, match := range matches {
		// Remove the '@' from the mention.
		mentions[match[1:]] = struct{}{}
	}

	// Helper function to get the keys of a map.
	getMapKeys := func(m map[string]struct{}) []string {
		keys := make([]string, 0, len(m))
		for k := range m {
			keys = append(keys, k)
		}
		return keys
	}

	// Get all project memberships.
	membershipsResult, err := s.dbClient.Memberships.GetMemberships(ctx, &db.GetMembershipsInput{
		Filter: &db.MembershipFilter{
			ProjectID: &release.ProjectID,
			MembershipScopes: []models.ScopeType{
				models.OrganizationScope,
				models.ProjectScope,
				models.GlobalScope,
			},
		},
	})
	if err != nil {
		return errors.Wrap(err, "failed to get memberships")
	}

	// To easily check if a user or team is a member of the project.
	memberships := map[string]struct{}{}
	for _, membership := range membershipsResult.Memberships {
		if membership.UserID != nil {
			memberships[*membership.UserID] = struct{}{}
		} else if membership.TeamID != nil {
			memberships[*membership.TeamID] = struct{}{}
		}
	}

	usersResult, err := s.dbClient.Users.GetUsers(ctx, &db.GetUsersInput{
		Filter: &db.UserFilter{
			Usernames: getMapKeys(mentions),
		},
	})
	if err != nil {
		return errors.Wrap(err, "failed to get users")
	}

	hasNewParticipants := false
	for _, user := range usersResult.Users {
		// Remove the user from the mentions map, so we can track any missing mentions.
		delete(mentions, user.Username)

		if _, ok := memberships[user.Metadata.ID]; !ok {
			// User is not a member of the project.
			continue
		}

		// Add only new participants to the release.
		if !slices.Contains(release.UserParticipantIDs, user.Metadata.ID) {
			hasNewParticipants = true
			release.UserParticipantIDs = append(release.UserParticipantIDs, user.Metadata.ID)
		}
	}

	if len(mentions) > 0 {
		// Some mentioned users were not found, check if they are teams.
		teamsResult, tErr := s.dbClient.Teams.GetTeams(ctx, &db.GetTeamsInput{
			Filter: &db.TeamFilter{
				TeamNames: getMapKeys(mentions),
			},
		})
		if tErr != nil {
			return errors.Wrap(tErr, "failed to get teams")
		}

		for _, team := range teamsResult.Teams {
			if _, ok := memberships[team.Metadata.ID]; !ok {
				// Team is not a member of the project.
				continue
			}

			// Add only new participants to the release.
			if !slices.Contains(release.TeamParticipantIDs, team.Metadata.ID) {
				hasNewParticipants = true
				release.TeamParticipantIDs = append(release.TeamParticipantIDs, team.Metadata.ID)
			}
		}
	}

	if hasNewParticipants {
		if _, err = s.dbClient.Releases.UpdateRelease(ctx, release); err != nil && errors.ErrorCode(err) != errors.EConflict {
			// Ignore conflict errors, since participants may have been added elsewhere.
			return errors.Wrap(err, "failed to update release")
		}
	}

	return nil
}

func (s *service) getCommentByID(ctx context.Context, span trace.Span, id string) (*models.Comment, error) {
	gotComment, err := s.dbClient.Comments.GetCommentByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get comment by ID", errors.WithSpan(span))
	}

	if gotComment == nil {
		return nil, errors.New(
			"comment with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return gotComment, nil
}

func (s *service) getPipelineByID(ctx context.Context, span trace.Span, id string) (*models.Pipeline, error) {
	pipeline, err := s.dbClient.Pipelines.GetPipelineByID(ctx, id)
	if err != nil {
		return nil, err
	}
	if pipeline == nil {
		return nil, errors.New(
			"pipeline with id %s not found", id, errors.WithSpan(span), errors.WithErrorCode(errors.ENotFound))
	}

	return pipeline, nil
}

func (s *service) getReleaseByID(ctx context.Context, span trace.Span, id string) (*models.Release, error) {
	release, err := s.dbClient.Releases.GetReleaseByID(ctx, id)
	if err != nil {
		return nil, err
	}
	if release == nil {
		return nil, errors.New(
			"release with id %s not found", id, errors.WithSpan(span), errors.WithErrorCode(errors.ENotFound))
	}

	return release, nil
}

func (s *service) getThreadByID(ctx context.Context, span trace.Span, id string) (*models.Thread, error) {
	thread, err := s.dbClient.Threads.GetThreadByID(ctx, id)
	if err != nil {
		return nil, err
	}

	if thread == nil {
		return nil, errors.New("thread with id %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return thread, nil
}

func (s *service) requireViewerAccessToComment(
	ctx context.Context,
	pipelineID,
	releaseID *string,
) error {
	ctx, span := tracer.Start(ctx, "svc.requireViewerAccessToComment")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	// Check whether the caller is allowed to view this comment.
	var projectID string
	switch {
	case pipelineID != nil:
		pipeline, err := s.getPipelineByID(ctx, span, *pipelineID)
		if err != nil {
			return err
		}
		projectID = pipeline.ProjectID
	case releaseID != nil:
		release, err := s.getReleaseByID(ctx, span, *releaseID)
		if err != nil {
			return err
		}
		projectID = release.ProjectID
	default:
		return errors.New("either a pipeline or release id must be specified",
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	return caller.RequirePermission(ctx, models.ViewComment, auth.WithProjectID(projectID))
}
