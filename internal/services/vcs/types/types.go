// Package types package
package types

import (
	"net/url"
	"time"
)

// Request method and content types, headers, etc. mainly for convenience.
const (
	JSONContentType = "application/json"

	BearerAuthPrefix  = "Bearer "
	V1WebhookEndpoint = "v1/vcs/events"
)

// BuildOAuthAuthorizationURLInput is the input for building an
// authorization code URL that can be used to complete OAuth flow.
type BuildOAuthAuthorizationURLInput struct {
	ProviderURL   url.URL
	OAuthClientID string
	OAuthState    string
	RedirectURL   string
	OAuthScopes   []string
}

// BuildRepositoryURLInput is the input for building a repository URL.
type BuildRepositoryURLInput struct {
	ProviderURL    url.URL
	RepositoryPath string
}

// TestConnectionInput is the input for testing a connection with a provider.
type TestConnectionInput struct {
	ProviderURL url.URL
	AccessToken string
}

// CreateAccessTokenInput is the input for creating an access token from a provider.
type CreateAccessTokenInput struct {
	ProviderURL       url.URL
	ClientID          string
	ClientSecret      string
	AuthorizationCode string
	RedirectURI       string
	RefreshToken      string // Required when renewing a token, only for GitLab.
}

// GetArchiveInput is the input for downloading a source archive.
type GetArchiveInput struct {
	Ref            *string
	ProviderURL    url.URL
	AccessToken    string
	RepositoryPath string
}

// AccessTokenPayload is the payload returned for creating /
// renewing an access token.
type AccessTokenPayload struct {
	ExpirationTimestamp *time.Time
	AccessToken         string
	RefreshToken        string
}
