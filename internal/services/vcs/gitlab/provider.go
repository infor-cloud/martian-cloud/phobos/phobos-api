// Package gitlab package
package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"slices"
	"strings"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const (
	// V4 API endpoint for GitLab.
	apiV4Endpoint = "api/v4"
)

var (
	// defaultURL is the default API URL for this provider type.
	defaultURL = url.URL{
		Scheme: "https",
		Host:   "gitlab.com",
	}

	// gitlabRequiredOAuthScopes are the required scopes for GitLab OAuth to
	// verify the token and to maintain base functionality.
	gitlabRequiredOAuthScopes = []string{"read_user", "read_api"}
)

// createAccessTokenResponse is the response struct for creating an access token.
type createAccessTokenResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int64  `json:"expires_in"`
	CreatedAt    int64  `json:"created_at"`
}

// Provider represents a particular VCS provider.
type Provider struct {
	logger    logger.Logger
	client    *http.Client
	phobosURL string
}

// New creates a new Provider instance.
func New(
	logger logger.Logger,
	client *http.Client,
	phobosURL string,
) (*Provider, error) {
	return &Provider{
		logger,
		client,
		phobosURL,
	}, nil
}

// DefaultURL returns the default API URL for this provider.
func (p *Provider) DefaultURL() url.URL {
	return defaultURL
}

// BuildOAuthAuthorizationURL build the authorization code URL which is
// used to redirect the user to the VCS provider to complete OAuth flow.
func (p *Provider) BuildOAuthAuthorizationURL(input *types.BuildOAuthAuthorizationURLInput) (string, error) {
	// Add any additional scopes.
	oAuthScopes := gitlabRequiredOAuthScopes
	if len(input.OAuthScopes) > 0 {
		oAuthScopes = append(gitlabRequiredOAuthScopes, input.OAuthScopes...)
		slices.Sort(oAuthScopes)
		oAuthScopes = slices.Compact(oAuthScopes)
	}

	// Add queries.
	queries := input.ProviderURL.Query()
	queries.Add("client_id", input.OAuthClientID)
	queries.Add("redirect_uri", input.RedirectURL)
	queries.Add("response_type", "code")
	queries.Add("state", input.OAuthState)
	queries.Add("scope", strings.Join(oAuthScopes, " "))
	input.ProviderURL.RawQuery = queries.Encode()

	return url.JoinPath(input.ProviderURL.String(), "oauth/authorize")
}

// BuildRepositoryURL returns the repository URL associated with the provider.
func (p *Provider) BuildRepositoryURL(input *types.BuildRepositoryURLInput) (string, error) {
	return url.JoinPath(input.ProviderURL.String(), input.RepositoryPath)
}

// TestConnection simply queries for the user metadata that's
// associated with the access token to verify validity.
// https://docs.gitlab.com/ee/api/users.html#for-normal-users-1
func (p *Provider) TestConnection(ctx context.Context, input *types.TestConnectionInput) error {
	endpoint, err := url.JoinPath(input.ProviderURL.String(), apiV4Endpoint, "user")
	if err != nil {
		return err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil) // nosemgrep: G107-1
	if err != nil {
		return fmt.Errorf("failed to prepare HTTP request: %v", err)
	}

	// Add request headers.
	request.Header.Add("Accept", types.JSONContentType)
	request.Header.Add("Authorization", types.BearerAuthPrefix+input.AccessToken)

	// Make the request.
	resp, err := p.client.Do(request)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to connect to VCS provider. Response status: %s", resp.Status)
	}

	return nil
}

// GetArchive downloads the entire repository archive for a branch or tag.
// https://docs.gitlab.com/ee/api/repositories.html#get-file-archive
func (p *Provider) GetArchive(ctx context.Context, input *types.GetArchiveInput) (*http.Response, error) {
	// Build the request URL.
	rawPath := strings.Join([]string{
		apiV4Endpoint,
		"projects",
		url.PathEscape(input.RepositoryPath),
		"repository",
		"archive.tar.gz", // Default is tar.gz, but incase it changes.
	}, "/")

	// Add queries.
	if input.Ref != nil {
		queries := input.ProviderURL.Query()
		queries.Add("sha", *input.Ref)
		input.ProviderURL.RawQuery = queries.Encode()
	}

	endpoint, err := url.JoinPath(input.ProviderURL.String(), rawPath)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil) // nosemgrep: G107-1
	if err != nil {
		return nil, fmt.Errorf("failed to prepare HTTP request: %v", err)
	}

	// Add request headers.
	request.Header.Add("Accept", "application/octet-stream")
	request.Header.Add("Authorization", types.BearerAuthPrefix+input.AccessToken)

	resp, err := p.client.Do(request)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("failed to get repository archive. Response status: %s", resp.Status)
	}

	return resp, nil
}

// CreateAccessToken sends a POST request to the provider to create
// an access and refresh tokens that can be used to further interact
// with the provider's API.
// https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-flow
func (p *Provider) CreateAccessToken(ctx context.Context, input *types.CreateAccessTokenInput) (*types.AccessTokenPayload, error) {
	path := strings.Join([]string{
		"oauth",
		"token",
	}, "/")

	// Add queries.
	queries := input.ProviderURL.Query()
	queries.Add("client_id", input.ClientID)
	queries.Add("client_secret", input.ClientSecret)
	queries.Add("redirect_uri", input.RedirectURI)

	// Add appropriate params for renewing an access token.
	if input.RefreshToken != "" {
		queries.Add("refresh_token", input.RefreshToken)
		queries.Add("grant_type", "refresh_token")
	} else {
		queries.Add("code", input.AuthorizationCode)
		queries.Add("grant_type", "authorization_code")
	}
	input.ProviderURL.RawQuery = queries.Encode()

	endpoint, err := url.JoinPath(input.ProviderURL.String(), path)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, endpoint, nil) // nosemgrep: G107-1
	if err != nil {
		return nil, fmt.Errorf("failed to prepare HTTP request: %v", err)
	}

	// Add request headers.
	request.Header.Add("Accept", types.JSONContentType)

	// Make the request.
	resp, err := p.client.Do(request)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = resp.Body.Close()
		if err != nil {
			p.logger.Errorf("failed to close response body in CreateAccessToken: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to create access token. Response status: %s", resp.Status)
	}

	tokenResp := createAccessTokenResponse{}
	if err = json.NewDecoder(resp.Body).Decode(&tokenResp); err != nil {
		return nil, err
	}

	// Parse timestamps.
	createdAtUnix := time.Unix(tokenResp.CreatedAt, 0)
	expiresAtDuration := time.Duration(tokenResp.ExpiresIn) // GitLab's expiration is 7200s.
	expirationTimestamp := createdAtUnix.Add(time.Second * expiresAtDuration)

	return &types.AccessTokenPayload{
		AccessToken:         tokenResp.AccessToken,
		RefreshToken:        tokenResp.RefreshToken,
		ExpirationTimestamp: &expirationTimestamp,
	}, nil
}
