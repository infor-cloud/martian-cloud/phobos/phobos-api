package vcs

import (
	"context"
	"fmt"
	"io"
	http "net/http"
	"net/url"
	"slices"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	mock "github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	types "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	mockLimitChecker := limits.NewMockLimitChecker(t)
	mockActivityEvents := activityevent.NewMockService(t)
	mockHTTPClient := &http.Client{}
	phobosURL := "http://phobos.tld"

	providerMap, err := NewVCSProviderMap(logger, mockHTTPClient, phobosURL)
	require.NotEmpty(t, providerMap)
	require.NoError(t, err)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		activityService: mockActivityEvents,
		limitChecker:    mockLimitChecker,
		phobosURL:       phobosURL,
		vcsProviderMap:  providerMap,
	}

	// Assert does not support function equality so pass in nil for oAuthStateGenerator.
	actual := newService(logger, dbClient, mockActivityEvents, mockLimitChecker, providerMap, nil, phobosURL)
	require.NoError(t, err)
	assert.Equal(t, expect, actual)
}

func TestGetVCSProviderByID(t *testing.T) {
	providerID := "vcs-provider-1"
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		name            string
		provider        *models.VCSProvider
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "get an org provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				OrganizationID: orgID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "get a project provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name:            "provider not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view org provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				OrganizationID: orgID,
				Scope:          models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockVCSProviders.On("GetProviderByID", mock.Anything, providerID).Return(test.provider, nil)

			if test.provider != nil {
				switch test.provider.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.VCSProviderResource, mock.Anything).Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewVCSProvider, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualProvider, err := service.GetVCSProviderByID(auth.WithCaller(ctx, mockCaller), providerID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.provider, actualProvider)
		})
	}
}

func TestGetVCSProviderByPRN(t *testing.T) {
	projProviderPRN := models.VCSProviderResource.BuildPRN("test-org", "test-project", "test-provider")
	orgProviderPRN := models.VCSProviderResource.BuildPRN("test-org", "test-provider")
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		name            string
		prn             string
		provider        *models.VCSProvider
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "get an org provider",
			prn:  orgProviderPRN,
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					PRN: orgProviderPRN,
				},
				OrganizationID: orgID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "get a project provider",
			prn:  projProviderPRN,
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					PRN: projProviderPRN,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name:            "provider not found",
			prn:             orgProviderPRN,
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view org provider",
			prn:  orgProviderPRN,
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					PRN: orgProviderPRN,
				},
				OrganizationID: orgID,
				Scope:          models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project provider",
			prn:  projProviderPRN,
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					PRN: projProviderPRN,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockVCSProviders.On("GetProviderByPRN", mock.Anything, test.prn).Return(test.provider, nil)

			if test.provider != nil {
				switch test.provider.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.VCSProviderResource, mock.Anything).Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewVCSProvider, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualProvider, err := service.GetVCSProviderByPRN(auth.WithCaller(ctx, mockCaller), test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.provider, actualProvider)
		})
	}
}

func TestGetVCSProviders(t *testing.T) {
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		name            string
		organizationID  *string
		projectID       *string
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:           "get org providers",
			organizationID: &orgID,
		},
		{
			name:      "get project providers",
			projectID: &projID,
		},
		{
			name:            "get will fail without organization or project id",
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "subject is not authorized to get org provider",
			organizationID:  &orgID,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "subject is not authorized to get project provider",
			projectID:       &projID,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			if test.organizationID != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.VCSProviderResource, mock.Anything).Return(test.authError)
			} else if test.projectID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewVCSProvider, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockVCSProviders.On("GetProviders", mock.Anything, mock.MatchedBy(func(input *db.GetVCSProvidersInput) bool {
					if test.organizationID != nil {
						return input.Filter.OrganizationID == test.organizationID &&
							slices.Compare(input.Filter.VCSProviderScopes, []models.ScopeType{models.OrganizationScope}) == 0
					} else if test.projectID != nil {
						return input.Filter.ProjectID == test.projectID &&
							slices.Compare(input.Filter.VCSProviderScopes, []models.ScopeType{models.OrganizationScope, models.ProjectScope}) == 0
					}

					return false
				})).Return(&db.VCSProvidersResult{}, nil)
			}

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient: dbClient,
			}

			result, err := service.GetVCSProviders(auth.WithCaller(ctx, mockCaller), &GetVCSProvidersInput{
				OrganizationID: test.organizationID,
				ProjectID:      test.projectID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, result)
		})
	}
}

func TestGetVCSProvidersByIDs(t *testing.T) {
	idList := []string{"provider-1"}
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		authError       error
		name            string
		expectErrorCode errors.CodeType
		providers       []models.VCSProvider
	}

	testCases := []testCase{
		{
			name: "get org provider",
			providers: []models.VCSProvider{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					OrganizationID: orgID,
					Scope:          models.OrganizationScope,
				},
			},
		},
		{
			name: "get proj provider",
			providers: []models.VCSProvider{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					ProjectID: &projID,
					Scope:     models.ProjectScope,
				},
			},
		},
		{
			name:      "providers not found",
			providers: []models.VCSProvider{},
		},
		{
			name: "subject is not authorized to view org provider",
			providers: []models.VCSProvider{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					OrganizationID: orgID,
					Scope:          models.OrganizationScope,
				},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project provider",
			providers: []models.VCSProvider{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					ProjectID: &projID,
					Scope:     models.ProjectScope,
				},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockVCSProviders.On("GetProviders", mock.Anything, &db.GetVCSProvidersInput{
				Filter: &db.VCSProviderFilter{
					VCSProviderIDs: idList,
				},
			}).Return(&db.VCSProvidersResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(test.providers)),
				},
				VCSProviders: test.providers,
			}, nil)

			for _, provider := range test.providers {
				switch provider.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.VCSProviderResource, mock.Anything).Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewVCSProvider, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient: dbClient,
			}

			result, err := service.GetVCSProvidersByIDs(auth.WithCaller(ctx, mockCaller), idList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.Len(t, result, len(test.providers))
		})
	}
}

func TestCreateOrganizationScope(t *testing.T) {
	orgID := "org-1"
	providerID := "provider-1"
	oAuthClientID := "client-id"
	testSubject := "testSubject"
	phobosURL := "http://phobos.tld"
	redirectURL := "http://"
	oAuthClientSecret := "client-secret"
	personalAccessToken := "personal-access-token"
	oAuthScopes := []string{"read:user", "repo"}
	githubAPIUrl := url.URL{Scheme: "https", Host: "api.github.com"}

	sampleOAuthState, err := uuid.NewRandom()
	require.NoError(t, err)

	type testCase struct {
		name            string
		input           *CreateOrganizationVCSProviderInput
		toCreate        *models.VCSProvider
		limitError      error
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "create a vcs provider with OAuth type",
			input: &CreateOrganizationVCSProviderInput{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				ExtraOAuthScopes:  oAuthScopes,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               ptr.String("https://api.github.com"),
			},
			toCreate: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				ExtraOAuthScopes:  oAuthScopes,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
				CreatedBy:         testSubject,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
		},
		{
			name: "create a vcs provider with access token type",
			input: &CreateOrganizationVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String(githubAPIUrl.String()),
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.OrganizationScope,
			},
		},
		{
			name: "should use provider's default api url when nothing is supplied",
			input: &CreateOrganizationVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.OrganizationScope,
			},
		},
		{
			name: "should normalize provided url",
			input: &CreateOrganizationVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String("https://api.github.com/"),
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.OrganizationScope,
			},
		},
		{
			name: "invalid url",
			input: &CreateOrganizationVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String("api.github.com"),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "subject is not authorized to create a vcs provider",
			input: &CreateOrganizationVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "provider type is not supported",
			input: &CreateOrganizationVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.VCSProviderType("unknown"),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "no oauth fields provided when using oauth scheme",
			input: &CreateOrganizationVCSProviderInput{
				Name:           "test-provider",
				Description:    "a test provider",
				OrganizationID: orgID,
				AuthType:       models.OAuthType,
				Type:           models.GitHubType,
				URL:            ptr.String(githubAPIUrl.String()),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "no personal token provided when using access token scheme",
			input: &CreateOrganizationVCSProviderInput{
				Name:           "test-provider",
				Description:    "a test provider",
				OrganizationID: orgID,
				AuthType:       models.AccessTokenType,
				Type:           models.GitHubType,
				URL:            ptr.String(githubAPIUrl.String()),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "limit exceeded",
			input: &CreateOrganizationVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String("https://api.github.com"),
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.OrganizationScope,
			},
			limitError:      errors.New("Limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProvider := NewMockProvider(t)
			mockCaller := auth.NewMockCaller(t)
			mockVCSProviders := db.NewMockVCSProviders(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.CreateVCSProvider, mock.Anything).Return(test.authError)

			mockProvider.On("DefaultURL").Return(githubAPIUrl).Maybe()

			if test.toCreate != nil {
				if test.toCreate.AuthType == models.AccessTokenType {
					mockProvider.On("TestConnection", mock.Anything, &types.TestConnectionInput{
						ProviderURL: test.toCreate.URL,
						AccessToken: ptr.ToString(test.toCreate.PersonalAccessToken),
					}).Return(nil)
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

				mockVCSProviders.On("CreateProvider", mock.Anything, test.toCreate).Return(
					func(_ context.Context, input *models.VCSProvider) (*models.VCSProvider, error) {
						// Assign an ID and version to the record. Rest should remain the same.
						input.Metadata.ID = providerID
						input.Metadata.Version = 1
						return input, nil
					},
				)

				mockVCSProviders.On("GetProviders", mock.Anything, &db.GetVCSProvidersInput{
					Filter: &db.VCSProviderFilter{
						OrganizationID:    &orgID,
						VCSProviderScopes: []models.ScopeType{models.OrganizationScope},
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.VCSProvidersResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil)

				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitVCSProvidersPerOrganization, int32(1)).Return(test.limitError)
			}

			if test.expectErrorCode == "" {
				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					OrganizationID: &orgID,
					Action:         models.ActionCreate,
					TargetType:     models.TargetVCSProvider,
					TargetID:       &providerID,
				}).Return(nil, nil)

				if test.input.AuthType == models.OAuthType {
					// Use mock.MatchedBy since we don't have control over oAuth state.
					mockProvider.On("BuildOAuthAuthorizationURL", &types.BuildOAuthAuthorizationURLInput{
						ProviderURL:   test.toCreate.URL,
						OAuthClientID: *test.toCreate.OAuthClientID,
						OAuthState:    *test.toCreate.OAuthState,
						OAuthScopes:   oAuthScopes,
						RedirectURL:   fmt.Sprintf("%s/%s", phobosURL, oAuthCallBackEndpoint),
					}).Return(redirectURL, nil)
				}

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
				phobosURL:       phobosURL,
				oAuthStateGenerator: func() (uuid.UUID, error) {
					return sampleOAuthState, nil
				},
				vcsProviderMap: map[models.VCSProviderType]Provider{
					models.GitHubType: mockProvider,
					models.GitLabType: mockProvider,
				},
			}

			response, err := service.CreateOrganizationVCSProvider(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, response)
			require.NotNil(t, response.VCSProvider)

			// Make sure a redirect url is provided for OAuth type.
			if test.input.AuthType == models.OAuthType {
				require.NotNil(t, response.OAuthAuthorizationURL)
				assert.Equal(t, redirectURL, *response.OAuthAuthorizationURL)
			}
		})
	}
}

func TestCreateProjectScope(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	providerID := "provider-1"
	oAuthClientID := "client-id"
	testSubject := "testSubject"
	phobosURL := "http://phobos.tld"
	redirectURL := "http://github.com"
	oAuthClientSecret := "client-secret"
	personalAccessToken := "personal-access-token"
	oAuthScopes := []string{"read:user", "repo"}
	githubAPIUrl := url.URL{Scheme: "https", Host: "api.github.com"}

	sampleOAuthState, err := uuid.NewRandom()
	require.NoError(t, err)

	type testCase struct {
		name            string
		input           *CreateProjectVCSProviderInput
		toCreate        *models.VCSProvider
		limitError      error
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "create a vcs provider with OAuth type",
			input: &CreateProjectVCSProviderInput{
				Name:              "test-provider",
				Description:       "a test provider",
				ProjectID:         projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				ExtraOAuthScopes:  oAuthScopes,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               ptr.String("https://api.github.com"),
			},
			toCreate: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				ExtraOAuthScopes:  oAuthScopes,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.ProjectScope,
				CreatedBy:         testSubject,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
		},
		{
			name: "create a vcs provider with access token type",
			input: &CreateProjectVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				ProjectID:           projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String(githubAPIUrl.String()),
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				ProjectID:           &projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.ProjectScope,
			},
		},
		{
			name: "should use provider's default api url when nothing is supplied",
			input: &CreateProjectVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				ProjectID:           projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				ProjectID:           &projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.ProjectScope,
			},
		},
		{
			name: "should normalize provided url",
			input: &CreateProjectVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				ProjectID:           projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String("https://api.github.com/"),
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				ProjectID:           &projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.ProjectScope,
			},
		},
		{
			name: "invalid url",
			input: &CreateProjectVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				ProjectID:           projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String("api.github.com"),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "subject is not authorized to create a vcs provider",
			input: &CreateProjectVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				ProjectID:           projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "provider type is not supported",
			input: &CreateProjectVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				ProjectID:           projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.VCSProviderType("unknown"),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "no oauth fields provided when using oauth scheme",
			input: &CreateProjectVCSProviderInput{
				Name:        "test-provider",
				Description: "a test provider",
				ProjectID:   projectID,
				AuthType:    models.OAuthType,
				Type:        models.GitHubType,
				URL:         ptr.String(githubAPIUrl.String()),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "no personal token provided when using access token scheme",
			input: &CreateProjectVCSProviderInput{
				Name:        "test-provider",
				Description: "a test provider",
				ProjectID:   projectID,
				AuthType:    models.AccessTokenType,
				Type:        models.GitHubType,
				URL:         ptr.String(githubAPIUrl.String()),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "limit exceeded",
			input: &CreateProjectVCSProviderInput{
				Name:                "test-provider",
				Description:         "a test provider",
				ProjectID:           projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 ptr.String("https://api.github.com"),
			},
			toCreate: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				ProjectID:           &projectID,
				PersonalAccessToken: &personalAccessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				CreatedBy:           testSubject,
				Scope:               models.ProjectScope,
			},
			limitError:      errors.New("Limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProvider := NewMockProvider(t)
			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockVCSProviders := db.NewMockVCSProviders(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.CreateVCSProvider, mock.Anything).Return(test.authError)

			mockProvider.On("DefaultURL").Return(githubAPIUrl).Maybe()

			mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(&models.Project{
				Metadata: models.ResourceMetadata{
					ID: projectID,
				},
				OrgID: orgID,
			}, nil).Maybe()

			if test.toCreate != nil {
				if test.toCreate.AuthType == models.AccessTokenType {
					mockProvider.On("TestConnection", mock.Anything, &types.TestConnectionInput{
						ProviderURL: test.toCreate.URL,
						AccessToken: ptr.ToString(test.toCreate.PersonalAccessToken),
					}).Return(nil)
				}

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

				mockVCSProviders.On("CreateProvider", mock.Anything, test.toCreate).Return(
					func(_ context.Context, input *models.VCSProvider) (*models.VCSProvider, error) {
						// Assign an ID and version to the record. Rest should remain the same.
						input.Metadata.ID = providerID
						input.Metadata.Version = 1
						return input, nil
					},
				)

				mockVCSProviders.On("GetProviders", mock.Anything, &db.GetVCSProvidersInput{
					Filter: &db.VCSProviderFilter{
						ProjectID:         &projectID,
						VCSProviderScopes: []models.ScopeType{models.ProjectScope},
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.VCSProvidersResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil)

				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitVCSProvidersPerProject, int32(1)).Return(test.limitError)
			}

			if test.expectErrorCode == "" {
				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionCreate,
					TargetType: models.TargetVCSProvider,
					TargetID:   &providerID,
				}).Return(nil, nil)

				if test.input.AuthType == models.OAuthType {
					// Use mock.MatchedBy since we don't have control over oAuth state.
					mockProvider.On("BuildOAuthAuthorizationURL", &types.BuildOAuthAuthorizationURLInput{
						ProviderURL:   test.toCreate.URL,
						OAuthClientID: *test.toCreate.OAuthClientID,
						OAuthState:    *test.toCreate.OAuthState,
						OAuthScopes:   oAuthScopes,
						RedirectURL:   fmt.Sprintf("%s/%s", phobosURL, oAuthCallBackEndpoint),
					}).Return(redirectURL, nil)
				}

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Projects:     mockProjects,
				VCSProviders: mockVCSProviders,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
				phobosURL:       phobosURL,
				oAuthStateGenerator: func() (uuid.UUID, error) {
					return sampleOAuthState, nil
				},
				vcsProviderMap: map[models.VCSProviderType]Provider{
					models.GitHubType: mockProvider,
					models.GitLabType: mockProvider,
				},
			}

			response, err := service.CreateProjectVCSProvider(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, response)
			require.NotNil(t, response.VCSProvider)

			// Make sure a redirect url is provided for OAuth type.
			if test.input.AuthType == models.OAuthType {
				require.NotNil(t, response.OAuthAuthorizationURL)
				assert.Equal(t, redirectURL, *response.OAuthAuthorizationURL)
			}
		})
	}
}

func TestUpdateVCSProvider(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	providerID := "provider-1"
	githubAPIUrl := url.URL{Scheme: "https", Host: "api.github.com"}

	type testCase struct {
		name            string
		input           *UpdateVCSProviderInput
		provider        *models.VCSProvider
		expectUpdated   *models.VCSProvider
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "update an org provider's description and access token",
			input: &UpdateVCSProviderInput{
				ID:                  providerID,
				Version:             ptr.Int(1),
				Description:         ptr.String("new description"),
				PersonalAccessToken: ptr.String("new-token"),
			},
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: ptr.String("original-access-token"),
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				Scope:               models.OrganizationScope,
			},
			expectUpdated: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID:      providerID,
					Version: 1,
				},
				Name:                "test-provider",
				Description:         "new description",
				OrganizationID:      orgID,
				PersonalAccessToken: ptr.String("new-token"),
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				Scope:               models.OrganizationScope,
			},
		},
		{
			name: "update an org provider's oauth settings",
			input: &UpdateVCSProviderInput{
				ID:                providerID,
				Version:           ptr.Int(1),
				OAuthClientID:     ptr.String("new-client-id"),
				OAuthClientSecret: ptr.String("new-client-secret"),
			},
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				ExtraOAuthScopes:  []string{},
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
			expectUpdated: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID:      providerID,
					Version: 1,
				},
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     ptr.String("new-client-id"),
				OAuthClientSecret: ptr.String("new-client-secret"),
				ExtraOAuthScopes:  []string{},
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
		},
		{
			name: "update a project provider's description and access token",
			input: &UpdateVCSProviderInput{
				ID:                  providerID,
				Version:             ptr.Int(1),
				Description:         ptr.String("new description"),
				PersonalAccessToken: ptr.String("new-token"),
			},
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				ProjectID:           &projectID,
				PersonalAccessToken: ptr.String("original-access-token"),
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				Scope:               models.OrganizationScope,
			},
			expectUpdated: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID:      providerID,
					Version: 1,
				},
				Name:                "test-provider",
				Description:         "new description",
				OrganizationID:      orgID,
				ProjectID:           &projectID,
				PersonalAccessToken: ptr.String("new-token"),
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				Scope:               models.OrganizationScope,
			},
		},
		{
			name: "update a project provider's oauth settings",
			input: &UpdateVCSProviderInput{
				ID:                providerID,
				Version:           ptr.Int(1),
				OAuthClientID:     ptr.String("new-client-id"),
				OAuthClientSecret: ptr.String("new-client-secret"),
				ExtraOAuthScopes:  []string{"admin:org"},
			},
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				ExtraOAuthScopes:  []string{"admin:org"},
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
			expectUpdated: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID:      providerID,
					Version: 1,
				},
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     ptr.String("new-client-id"),
				OAuthClientSecret: ptr.String("new-client-secret"),
				ExtraOAuthScopes:  []string{"admin:org"},
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
		},
		{
			name: "cannot update oauth settings if provider uses access token auth type",
			input: &UpdateVCSProviderInput{
				ID:                providerID,
				Version:           ptr.Int(1),
				OAuthClientID:     ptr.String("new-client-id"),
				OAuthClientSecret: ptr.String("new-client-secret"),
			},
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: ptr.String("access-token"),
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 githubAPIUrl,
				Scope:               models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "cannot update personal access token if provider uses oauth auth type",
			input: &UpdateVCSProviderInput{
				ID:                  providerID,
				Version:             ptr.Int(1),
				PersonalAccessToken: ptr.String("new-access-token"),
			},
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				ExtraOAuthScopes:  []string{},
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "subject is not authorized to update vcs provider",
			input: &UpdateVCSProviderInput{
				ID:                  providerID,
				Version:             ptr.Int(1),
				PersonalAccessToken: ptr.String("new-access-token"),
			},
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     ptr.String("client-id"),
				OAuthClientSecret: ptr.String("client-secret"),
				ExtraOAuthScopes:  []string{},
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "provider not found",
			input: &UpdateVCSProviderInput{
				ID: providerID,
			},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockRemoteProvider := NewMockProvider(t)
			mockVCSProviders := db.NewMockVCSProviders(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return("testSubject").Maybe()

			mockVCSProviders.On("GetProviderByID", mock.Anything, providerID).Return(test.provider, nil)

			if test.provider != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateVCSProvider, mock.Anything).Return(test.authError)

				mockRemoteProvider.On("TestConnection", mock.Anything, &types.TestConnectionInput{
					ProviderURL: githubAPIUrl,
					AccessToken: ptr.ToString(test.input.PersonalAccessToken),
				}).Return(nil).Maybe()
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockVCSProviders.On("UpdateProvider", mock.Anything, test.expectUpdated).Return(test.expectUpdated, nil)

				var organizationID *string
				if test.provider.Scope == models.OrganizationScope {
					organizationID = &orgID
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					OrganizationID: organizationID,
					ProjectID:      test.provider.ProjectID,
					Action:         models.ActionUpdate,
					TargetType:     models.TargetVCSProvider,
					TargetID:       &providerID,
				}).Return(nil, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
				vcsProviderMap: map[models.VCSProviderType]Provider{
					models.GitHubType: mockRemoteProvider,
					models.GitLabType: mockRemoteProvider,
				},
			}

			updatedProvider, err := service.UpdateVCSProvider(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectUpdated, updatedProvider)
		})
	}
}

func TestDeleteVCSProvider(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	providerID := "provider-1"

	type testCase struct {
		name            string
		provider        *models.VCSProvider
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "delete an org provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:           "test-provider",
				OrganizationID: orgID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "delete a project provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:           "test-provider",
				OrganizationID: orgID,
				ProjectID:      &projectID,
				Scope:          models.ProjectScope,
			},
		},
		{
			name: "subject is not authorized to delete org provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:           "test-provider",
				OrganizationID: orgID,
				Scope:          models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to delete project provider",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:           "test-provider",
				OrganizationID: orgID,
				ProjectID:      &projectID,
				Scope:          models.ProjectScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "provider not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockVCSProviders := db.NewMockVCSProviders(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return("testSubject").Maybe()

			mockVCSProviders.On("GetProviderByID", mock.Anything, providerID).Return(test.provider, nil)

			if test.provider != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.DeleteVCSProvider, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockVCSProviders.On("DeleteProvider", mock.Anything, test.provider).Return(nil)

				var organizationID *string
				var targetType models.ActivityEventTargetType
				var targetID *string
				if test.provider.Scope == models.OrganizationScope {
					organizationID = &orgID
					targetID = &orgID
					targetType = models.TargetOrganization
				} else {
					targetID = &projectID
					targetType = models.TargetProject
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					OrganizationID: organizationID,
					ProjectID:      test.provider.ProjectID,
					Action:         models.ActionDelete,
					TargetType:     targetType,
					TargetID:       targetID,
					Payload: &models.ActivityEventDeleteResourcePayload{
						Name: ptr.String(test.provider.Name),
						ID:   test.provider.Metadata.ID,
						Type: string(models.TargetVCSProvider),
					},
				}).Return(nil, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
				Transactions: mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			err := service.DeleteVCSProvider(auth.WithCaller(ctx, mockCaller), &DeleteVCSProviderInput{
				ID:      providerID,
				Version: ptr.Int(1),
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestResetVCSProviderOAuthToken(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	providerID := "provider-1"
	oAuthClientID := "client-id"
	oAuthClientSecret := "client-secret"
	phobosURL := "http://phobos.tld"
	redirectURL := "http://github.com"
	githubAPIUrl := url.URL{Scheme: "https", Host: "api.github.com"}

	sampleOAuthState, err := uuid.NewRandom()
	require.NoError(t, err)

	type testCase struct {
		name            string
		provider        *models.VCSProvider
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "reset org provider token",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
		},
		{
			name: "reset project provider token",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.ProjectScope,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProvider := NewMockProvider(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockVCSProviders.On("GetProviderByID", mock.Anything, providerID).Return(test.provider, nil)

			if test.provider != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateVCSProvider, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockVCSProviders.On("UpdateProvider", mock.Anything, mock.MatchedBy(func(vp *models.VCSProvider) bool {
					return vp.OAuthAccessToken == nil &&
						vp.OAuthAccessTokenExpiresAt == nil &&
						vp.OAuthRefreshToken == nil &&
						ptr.ToString(vp.OAuthState) == sampleOAuthState.String()
				})).Return(func(_ context.Context, provider *models.VCSProvider) (*models.VCSProvider, error) {
					// Just return the updated provider.
					return provider, nil
				})

				// Use mock.MatchedBy since we don't have control over oAuth state.
				mockProvider.On("BuildOAuthAuthorizationURL", &types.BuildOAuthAuthorizationURLInput{
					ProviderURL:   test.provider.URL,
					OAuthClientID: *test.provider.OAuthClientID,
					OAuthState:    *test.provider.OAuthState,
					RedirectURL:   fmt.Sprintf("%s/%s", phobosURL, oAuthCallBackEndpoint),
				}).Return(redirectURL, nil)
			}

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient:  dbClient,
				phobosURL: phobosURL,
				oAuthStateGenerator: func() (uuid.UUID, error) {
					return sampleOAuthState, nil
				},
				vcsProviderMap: map[models.VCSProviderType]Provider{
					models.GitHubType: mockProvider,
					models.GitLabType: mockProvider,
				},
			}

			response, err := service.ResetVCSProviderOAuthToken(auth.WithCaller(ctx, mockCaller), &ResetVCSProviderOAuthTokenInput{
				ProviderID: providerID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, response)
			require.NotNil(t, response.VCSProvider)
			require.NotNil(t, response.VCSProvider.OAuthState)
			assert.Equal(t, sampleOAuthState.String(), *response.VCSProvider.OAuthState)
			assert.Nil(t, response.VCSProvider.OAuthAccessToken)
			assert.Nil(t, response.VCSProvider.OAuthAccessTokenExpiresAt)
			assert.Nil(t, response.VCSProvider.OAuthRefreshToken)
			assert.Equal(t, redirectURL, response.OAuthAuthorizationURL)
		})
	}
}

func TestProcessOAuth(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	authorizationCode := "auth-code"
	oAuthClientID := "client-id"
	oAuthClientSecret := "client-secret"
	phobosURL := "http://phobos.tld"
	accessToken := "access-token"
	githubAPIUrl := url.URL{Scheme: "https", Host: "api.github.com"}
	gitlabAPIUrl := url.URL{Scheme: "https", Host: "gitlab.com"}

	sampleOAuthState, err := uuid.NewRandom()
	assert.Nil(t, err)

	type testCase struct {
		name               string
		provider           *models.VCSProvider
		accessTokenPayload *types.AccessTokenPayload
		authError          error
		expectErrorCode    errors.CodeType
	}

	testCases := []testCase{
		{
			name: "process org provider oauth",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
			accessTokenPayload: &types.AccessTokenPayload{
				AccessToken: accessToken,
			},
		},
		{
			name: "process project provider oauth",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.ProjectScope,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
			accessTokenPayload: &types.AccessTokenPayload{
				AccessToken: accessToken,
			},
		},
		{
			name: "provider uses refresh token",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitLabType,
				URL:               gitlabAPIUrl,
				Scope:             models.OrganizationScope,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
			accessTokenPayload: &types.AccessTokenPayload{
				AccessToken:         accessToken,
				ExpirationTimestamp: ptr.Time(time.Now().UTC().Add(5 * time.Minute)),
				RefreshToken:        "refresh-token",
			},
		},
		{
			name:            "provider not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to update org provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to update project provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.ProjectScope,
				OAuthState:        ptr.String(sampleOAuthState.String()),
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProvider := NewMockProvider(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockVCSProviders.On("GetProviderByOAuthState", mock.Anything, sampleOAuthState.String()).Return(test.provider, nil)

			if test.provider != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateVCSProvider, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockProvider.On("CreateAccessToken", mock.Anything, &types.CreateAccessTokenInput{
					ProviderURL:       test.provider.URL,
					ClientID:          *test.provider.OAuthClientID,
					ClientSecret:      *test.provider.OAuthClientSecret,
					AuthorizationCode: authorizationCode,
					RedirectURI:       fmt.Sprintf("%s/%s", phobosURL, oAuthCallBackEndpoint),
				}).Return(test.accessTokenPayload, nil)

				mockProvider.On("TestConnection", mock.Anything, &types.TestConnectionInput{
					ProviderURL: test.provider.URL,
					AccessToken: test.accessTokenPayload.AccessToken,
				}).Return(nil)

				mockVCSProviders.On("UpdateProvider", mock.Anything, mock.MatchedBy(func(vp *models.VCSProvider) bool {
					if test.accessTokenPayload.RefreshToken != "" {
						return ptr.ToString(vp.OAuthRefreshToken) == test.accessTokenPayload.RefreshToken &&
							vp.OAuthState == nil &&
							ptr.ToString(vp.OAuthAccessToken) == test.accessTokenPayload.AccessToken &&
							vp.OAuthAccessTokenExpiresAt == test.accessTokenPayload.ExpirationTimestamp
					}

					return vp.OAuthRefreshToken == nil &&
						vp.OAuthAccessTokenExpiresAt == nil &&
						vp.OAuthState == nil &&
						ptr.ToString(vp.OAuthAccessToken) == test.accessTokenPayload.AccessToken
				})).Return(func(_ context.Context, provider *models.VCSProvider) (*models.VCSProvider, error) {
					// Just return the updated provider.
					return provider, nil
				})
			}

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient:  dbClient,
				phobosURL: phobosURL,
				oAuthStateGenerator: func() (uuid.UUID, error) {
					return sampleOAuthState, nil
				},
				vcsProviderMap: map[models.VCSProviderType]Provider{
					models.GitHubType: mockProvider,
					models.GitLabType: mockProvider,
				},
			}

			err := service.ProcessOAuth(auth.WithCaller(ctx, mockCaller), &ProcessOAuthInput{
				AuthorizationCode: authorizationCode,
				State:             sampleOAuthState.String(),
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}

func TestGetRepositoryArchive(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	providerID := "provider-1"
	oAuthAccessToken := "access-token"
	oAuthClientID := "client-id"
	oAuthClientSecret := "client-secret"
	ref := "main"
	repositoryPath := "some-repository"
	githubAPIUrl := url.URL{Scheme: "https", Host: "api.github.com"}
	sampleArchiveData := io.NopCloser(strings.NewReader("some-archive-data"))

	type testCase struct {
		name            string
		provider        *models.VCSProvider
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "get repository archive with org provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				OAuthAccessToken:  &oAuthAccessToken,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
		},
		{
			name: "get repository archive using project provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				OAuthAccessToken:  &oAuthAccessToken,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.ProjectScope,
			},
		},
		{
			name:            "provider not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view org provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         &projectID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               githubAPIUrl,
				Scope:             models.ProjectScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProvider := NewMockProvider(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockVCSProviders.On("GetProviderByID", mock.Anything, providerID).Return(test.provider, nil)

			if test.provider != nil {
				switch test.provider.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.VCSProviderResource, mock.Anything).Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewVCSProvider, mock.Anything).Return(test.authError)
				}
			}

			if test.expectErrorCode == "" {
				mockProvider.On("GetArchive", mock.Anything, &types.GetArchiveInput{
					ProviderURL:    test.provider.URL,
					AccessToken:    oAuthAccessToken,
					RepositoryPath: repositoryPath,
					Ref:            &ref,
				}).Return(&http.Response{
					Body: sampleArchiveData,
				}, nil)
			}

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient: dbClient,
				vcsProviderMap: map[models.VCSProviderType]Provider{
					models.GitHubType: mockProvider,
					models.GitLabType: mockProvider,
				},
			}

			response, err := service.GetRepositoryArchive(auth.WithCaller(ctx, mockCaller), &GetRepositoryArchiveInput{
				Ref:            &ref,
				ProviderID:     providerID,
				RepositoryPath: repositoryPath,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, sampleArchiveData, response)
		})
	}
}

func Test_getAuthToken(t *testing.T) {
	orgID := "org-1"
	providerID := "provider-1"
	accessToken := "access-token"
	oAuthClientID := "client-id"
	oAuthClientSecret := "client-secret"
	oAuthRefreshToken := "refresh-token"
	phobosURL := "http://phobos.tld"

	type testCase struct {
		name               string
		provider           *models.VCSProvider
		accessTokenPayload *types.AccessTokenPayload
		expectAuthToken    string
		expectErrorCode    errors.CodeType
	}

	testCases := []testCase{
		{
			name: "access token auth type",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &accessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				Scope:               models.OrganizationScope,
			},
			expectAuthToken: accessToken,
		},
		{
			name: "oauth auth type",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &oAuthClientID,
				OAuthClientSecret: &oAuthClientSecret,
				OAuthAccessToken:  &accessToken,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				Scope:             models.OrganizationScope,
			},
			expectAuthToken: accessToken,
		},
		{
			name: "current oauth token is not yet expired, expect same token",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                      "test-provider",
				Description:               "a test provider",
				OrganizationID:            orgID,
				OAuthClientID:             &oAuthClientID,
				OAuthClientSecret:         &oAuthClientSecret,
				OAuthAccessToken:          &accessToken,
				OAuthRefreshToken:         &oAuthRefreshToken,
				OAuthAccessTokenExpiresAt: ptr.Time(time.Now().UTC().Add(5 * time.Minute)),
				AuthType:                  models.OAuthType,
				Type:                      models.GitLabType,
				Scope:                     models.OrganizationScope,
			},
			expectAuthToken: accessToken,
		},
		{
			name: "current oauth token is not yet expired but expect a new token due to leeway",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                      "test-provider",
				Description:               "a test provider",
				OrganizationID:            orgID,
				OAuthClientID:             &oAuthClientID,
				OAuthClientSecret:         &oAuthClientSecret,
				OAuthAccessToken:          &accessToken,
				OAuthRefreshToken:         &oAuthRefreshToken,
				OAuthAccessTokenExpiresAt: ptr.Time(time.Now().UTC().Add(55 * time.Second)),
				AuthType:                  models.OAuthType,
				Type:                      models.GitLabType,
				Scope:                     models.OrganizationScope,
			},
			accessTokenPayload: &types.AccessTokenPayload{
				AccessToken:         "new-access-token",
				ExpirationTimestamp: ptr.Time(time.Now().UTC().Add(5 * time.Hour)),
				RefreshToken:        "a-refresh-token",
			},
			expectAuthToken: "new-access-token",
		},
		{
			name: "current oauth token is expired, expect a new token",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                      "test-provider",
				Description:               "a test provider",
				OrganizationID:            orgID,
				OAuthClientID:             &oAuthClientID,
				OAuthClientSecret:         &oAuthClientSecret,
				OAuthAccessToken:          &accessToken,
				OAuthRefreshToken:         &oAuthRefreshToken,
				OAuthAccessTokenExpiresAt: ptr.Time(time.Now().UTC().Add(-5 * time.Minute)),
				AuthType:                  models.OAuthType,
				Type:                      models.GitLabType,
				Scope:                     models.OrganizationScope,
			},
			accessTokenPayload: &types.AccessTokenPayload{
				AccessToken:         "new-access-token",
				ExpirationTimestamp: ptr.Time(time.Now().UTC().Add(5 * time.Hour)),
				RefreshToken:        "a-refresh-token",
			},
			expectAuthToken: "new-access-token",
		},
		{
			name: "oauth auth type but oauth flow is incomplete",
			provider: &models.VCSProvider{
				Metadata: models.ResourceMetadata{
					ID: providerID,
				},
				Name:                      "test-provider",
				Description:               "a test provider",
				OrganizationID:            orgID,
				OAuthClientID:             &oAuthClientID,
				OAuthClientSecret:         &oAuthClientSecret,
				OAuthAccessTokenExpiresAt: ptr.Time(time.Now().UTC().Add(-5 * time.Minute)),
				AuthType:                  models.OAuthType,
				Type:                      models.GitHubType,
				Scope:                     models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "auth type not supported",
			provider: &models.VCSProvider{
				AuthType: models.VCSProviderAuthType("unknown"),
			},
			expectErrorCode: errors.EInternal,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProvider := NewMockProvider(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockProvider.On("CreateAccessToken", mock.Anything, &types.CreateAccessTokenInput{
				ProviderURL:  test.provider.URL,
				ClientID:     oAuthClientID,
				ClientSecret: oAuthClientSecret,
				RedirectURI:  fmt.Sprintf("%s/%s", phobosURL, oAuthCallBackEndpoint),
				RefreshToken: oAuthRefreshToken, // We're renewing the access token.
			}).Return(test.accessTokenPayload, nil).Maybe()

			mockVCSProviders.On("UpdateProvider", mock.Anything, mock.MatchedBy(func(vp *models.VCSProvider) bool {
				return ptr.ToString(vp.OAuthAccessToken) == test.accessTokenPayload.AccessToken &&
					ptr.ToString(vp.OAuthRefreshToken) == test.accessTokenPayload.RefreshToken &&
					vp.OAuthAccessTokenExpiresAt == test.accessTokenPayload.ExpirationTimestamp
			})).Return(func(_ context.Context, provider *models.VCSProvider) (*models.VCSProvider, error) {
				// Just return the updated provider.
				return provider, nil
			}).Maybe()

			dbClient := &db.Client{
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient:  dbClient,
				phobosURL: phobosURL,
			}

			authToken, err := service.getAuthToken(ctx, mockProvider, test.provider)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectAuthToken, authToken)
		})
	}
}

func TestCreateVCSTokenForPipeline(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	vcsProviderID := "provider-1"
	pipelineID := "pipeline-1"
	clientID := "client-id"
	clientSecret := "client-secret"
	phobosURL := "http://phobos.tld"
	redirectURI := "http://phobos.tld/v1/vcs/auth/callback"
	accessToken := "access-token"
	refreshToken := "refresh-token"
	expirationTimestamp := time.Now().UTC().Add(5 * time.Hour)

	type testCase struct {
		name            string
		provider        *models.VCSProvider
		pipeline        *models.Pipeline
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "create VCS token for pipeline with org provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &clientID,
				OAuthClientSecret: &clientSecret,
				OAuthAccessToken:  &accessToken,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               url.URL{Scheme: "https", Host: "api.github.com"},
				Scope:             models.OrganizationScope,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
		},
		{
			name: "create VCS token for pipeline with project provider",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         ptr.String(projectID),
				OAuthClientID:     &clientID,
				OAuthClientSecret: &clientSecret,
				OAuthAccessToken:  &accessToken,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               url.URL{Scheme: "https", Host: "api.github.com"},
				Scope:             models.ProjectScope,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
		},
		{
			name: "provider uses refresh token",
			provider: &models.VCSProvider{
				Name:                      "test-provider",
				Description:               "a test provider",
				OrganizationID:            orgID,
				ProjectID:                 ptr.String(projectID),
				OAuthClientID:             &clientID,
				OAuthClientSecret:         &clientSecret,
				OAuthAccessToken:          &accessToken,
				OAuthRefreshToken:         &refreshToken,
				OAuthAccessTokenExpiresAt: &expirationTimestamp,
				AuthType:                  models.OAuthType,
				Type:                      models.GitLabType,
				URL:                       url.URL{Scheme: "https", Host: "gitlab.com"},
				Scope:                     models.OrganizationScope,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
		},
		{
			name: "provider not found",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "pipeline not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to update pipeline state",
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "provider uses access token auth type",
			provider: &models.VCSProvider{
				Name:                "test-provider",
				Description:         "a test provider",
				OrganizationID:      orgID,
				PersonalAccessToken: &accessToken,
				AuthType:            models.AccessTokenType,
				Type:                models.GitHubType,
				URL:                 url.URL{Scheme: "https", Host: "api.github.com"},
				Scope:               models.OrganizationScope,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
		},
		{
			name: "provider uses OAuth but OAuth flow is incomplete",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				OAuthClientID:     &clientID,
				OAuthClientSecret: &clientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               url.URL{Scheme: "https", Host: "api.github.com"},
				Scope:             models.OrganizationScope,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "vcs provider does not belong to the same org as the pipeline",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    "org-2",
				OAuthClientID:     &clientID,
				OAuthClientSecret: &clientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               url.URL{Scheme: "https", Host: "api.github.com"},
				Scope:             models.OrganizationScope,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "vcs provider does not belong to the same project as the pipeline",
			provider: &models.VCSProvider{
				Name:              "test-provider",
				Description:       "a test provider",
				OrganizationID:    orgID,
				ProjectID:         ptr.String("project-2"),
				OAuthClientID:     &clientID,
				OAuthClientSecret: &clientSecret,
				AuthType:          models.OAuthType,
				Type:              models.GitHubType,
				URL:               url.URL{Scheme: "https", Host: "api.github.com"},
				Scope:             models.ProjectScope,
			},
			pipeline: &models.Pipeline{
				Metadata: models.ResourceMetadata{
					ID: pipelineID,
				},
				ProjectID: projectID,
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockPipelines := db.NewMockPipelines(t)
			mockProjects := db.NewMockProjects(t)
			mockRemoteProvider := NewMockProvider(t)
			mockVCSProviders := db.NewMockVCSProviders(t)

			mockPipelines.On("GetPipelineByID", mock.Anything, pipelineID).Return(test.pipeline, nil)

			if test.pipeline != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdatePipelineState, mock.Anything, mock.Anything).Return(test.authError)

				if test.authError == nil {
					mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(&models.Project{
						Metadata: models.ResourceMetadata{
							ID: projectID,
						},
						OrgID: orgID,
					}, nil)

					mockVCSProviders.On("GetProviderByID", mock.Anything, vcsProviderID).Return(test.provider, nil)
				}
			}

			if test.provider != nil {
				mockRemoteProvider.On("CreateAccessToken", mock.Anything, &types.CreateAccessTokenInput{
					ProviderURL:  test.provider.URL,
					ClientID:     clientID,
					ClientSecret: clientSecret,
					RedirectURI:  redirectURI,
					RefreshToken: refreshToken,
				}).Return(&types.AccessTokenPayload{
					AccessToken:         accessToken,
					ExpirationTimestamp: &expirationTimestamp,
					RefreshToken:        refreshToken,
				}, nil).Maybe()

				mockVCSProviders.On("UpdateProvider", mock.Anything, mock.MatchedBy(func(vp *models.VCSProvider) bool {
					return ptr.ToString(vp.OAuthAccessToken) == accessToken &&
						ptr.ToString(vp.OAuthRefreshToken) == refreshToken &&
						vp.OAuthAccessTokenExpiresAt.Equal(expirationTimestamp)
				})).Return(func(_ context.Context, provider *models.VCSProvider) (*models.VCSProvider, error) {
					// Just return the updated provider.
					return provider, nil
				}).Maybe()
			}

			dbClient := &db.Client{
				Pipelines:    mockPipelines,
				Projects:     mockProjects,
				VCSProviders: mockVCSProviders,
			}

			service := &service{
				dbClient: dbClient,
				vcsProviderMap: map[models.VCSProviderType]Provider{
					models.GitHubType: mockRemoteProvider,
					models.GitLabType: mockRemoteProvider,
				},
				phobosURL: phobosURL,
			}

			response, err := service.CreateVCSTokenForPipeline(auth.WithCaller(ctx, mockCaller), &CreateVCSTokenForPipelineInput{
				PipelineID: pipelineID,
				ProviderID: vcsProviderID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, response)
			assert.Equal(t, accessToken, response.AccessToken)

			if response.ExpiresAt != nil {
				assert.Equal(t, expirationTimestamp, *response.ExpiresAt)
			}
		})
	}
}
