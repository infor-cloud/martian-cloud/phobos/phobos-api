// Package vcs package
package vcs

//go:generate go tool mockery --name Provider --inpackage --case underscore

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/github"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/gitlab"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// Provider handles the logic for a specific type of vcs provider.
type Provider interface {
	DefaultURL() url.URL
	BuildOAuthAuthorizationURL(input *types.BuildOAuthAuthorizationURLInput) (string, error)
	BuildRepositoryURL(input *types.BuildRepositoryURLInput) (string, error)
	TestConnection(ctx context.Context, input *types.TestConnectionInput) error
	GetArchive(ctx context.Context, input *types.GetArchiveInput) (*http.Response, error)
	CreateAccessToken(ctx context.Context, input *types.CreateAccessTokenInput) (*types.AccessTokenPayload, error)
}

// NewVCSProviderMap returns a map containing a handler for each VCS provider type.
func NewVCSProviderMap(
	logger logger.Logger,
	client *http.Client,
	phobosURL string,
) (map[models.VCSProviderType]Provider, error) {
	gitLabHandler, err := gitlab.New(logger, client, phobosURL)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize %s vcs provider handler %v", models.GitLabType, err)
	}

	gitHubHandler, err := github.New(logger, client, phobosURL)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize %s vcs provider handler %v", models.GitHubType, err)
	}

	return map[models.VCSProviderType]Provider{
		models.GitLabType: gitLabHandler,
		models.GitHubType: gitHubHandler,
	}, nil
}
