// Package github package
package github

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"slices"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const (
	// Content-Type specific to GitHub.
	jsonContentType = "application/vnd.github+json"
)

var (
	// defaultURL is the default API URL for this provider type.
	defaultURL = url.URL{
		Scheme: "https",
		Host:   "api.github.com",
	}

	// gitHubRequiredOAuthScopes are the required scopes for GitHub OAuth
	// to verify the token and to maintain base functionality.
	// NOTE: GitHub does not seem to support read-only 'repo' scope.
	// https://docs.github.com/en/developers/apps/building-oauth-apps/scopes-for-oauth-apps#available-scopes
	gitHubRequiredOAuthScopes = []string{"read:user", "repo"}
)

// createAccessTokenResponse is the response struct for creating an access token.
type createAccessTokenResponse struct {
	AccessToken string `json:"access_token"`
}

// Provider represents a particular VCS provider.
type Provider struct {
	logger    logger.Logger
	client    *http.Client
	phobosURL string
}

// New creates a new Provider instance.
func New(
	logger logger.Logger,
	client *http.Client,
	phobosURL string,
) (*Provider, error) {
	return &Provider{
		logger,
		client,
		phobosURL,
	}, nil
}

// DefaultURL returns the default API URL for this provider.
func (p *Provider) DefaultURL() url.URL {
	return defaultURL
}

// BuildOAuthAuthorizationURL build the authorization code URL which is
// used to redirect the user to the VCS provider to complete OAuth flow.
func (p *Provider) BuildOAuthAuthorizationURL(input *types.BuildOAuthAuthorizationURLInput) (string, error) {
	// Add additional scopes if provided.
	oAuthScopes := gitHubRequiredOAuthScopes
	if len(input.OAuthScopes) > 0 {
		oAuthScopes = append(gitHubRequiredOAuthScopes, input.OAuthScopes...)
		slices.Sort(oAuthScopes)
		oAuthScopes = slices.Compact(oAuthScopes)
	}

	// Add queries.
	queries := input.ProviderURL.Query()
	queries.Add("client_id", input.OAuthClientID)
	queries.Add("redirect_uri", input.RedirectURL)
	queries.Add("state", input.OAuthState)
	queries.Add("scope", strings.Join(oAuthScopes, " "))
	input.ProviderURL.RawQuery = queries.Encode()

	// Can't use GitHub's API here. Must remove ".api" prefix.
	input.ProviderURL.Host = p.stripAPIPrefix(input.ProviderURL.Host)
	return url.JoinPath(input.ProviderURL.String(), "login/oauth/authorize")
}

// BuildRepositoryURL returns the repository URL associated with the provider.
func (p *Provider) BuildRepositoryURL(input *types.BuildRepositoryURLInput) (string, error) {
	input.ProviderURL.Host = p.stripAPIPrefix(input.ProviderURL.Host)
	return url.JoinPath(input.ProviderURL.String(), input.RepositoryPath)
}

// TestConnection simply queries for the user metadata that's
// associated with the access token to verify validity.
// https://docs.github.com/en/rest/users/users#get-the-authenticated-user
func (p *Provider) TestConnection(ctx context.Context, input *types.TestConnectionInput) error {
	endpoint, err := url.JoinPath(input.ProviderURL.String(), "user")
	if err != nil {
		return err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil) // nosemgrep: G107-1
	if err != nil {
		return fmt.Errorf("failed to prepare HTTP request: %v", err)
	}

	// Add the headers.
	request.Header.Add("Accept", jsonContentType)
	request.Header.Add("Authorization", types.BearerAuthPrefix+input.AccessToken)

	// Make the request.
	resp, err := p.client.Do(request)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to connect to VCS provider. Response status: %s", resp.Status)
	}

	return nil
}

// GetArchive downloads the entire repository archive for a branch or tag.
// https://docs.github.com/en/rest/repos/contents#download-a-repository-archive-tar
func (p *Provider) GetArchive(ctx context.Context, input *types.GetArchiveInput) (*http.Response, error) {
	pathParts := []string{
		"repos",
		input.RepositoryPath,
		"tarball",
	}

	if input.Ref != nil {
		pathParts = append(pathParts, *input.Ref)
	}

	path := strings.Join(pathParts, "/")

	endpoint, err := url.JoinPath(input.ProviderURL.String(), path)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil) // nosemgrep: G107-1
	if err != nil {
		return nil, fmt.Errorf("failed to prepare HTTP request: %v", err)
	}

	// Add the headers.
	request.Header.Add("Accept", jsonContentType)
	request.Header.Add("Authorization", types.BearerAuthPrefix+input.AccessToken)

	// Make the request.
	resp, err := p.client.Do(request)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		return nil, fmt.Errorf("failed to get repository archive. Response status: %s", resp.Status)
	}

	return resp, nil
}

// CreateAccessToken sends a POST request to the provider to create
// an access and refresh tokens that can be used to further interact
// with the provider's API.
// https://docs.github.com/en/developers/apps/building-oauth-apps/authorizing-oauth-apps#web-application-flow
func (p *Provider) CreateAccessToken(ctx context.Context, input *types.CreateAccessTokenInput) (*types.AccessTokenPayload, error) {
	path := strings.Join([]string{
		"login",
		"oauth",
		"access_token",
	}, "/")

	// Add queries.
	queries := input.ProviderURL.Query()
	queries.Add("client_id", input.ClientID)
	queries.Add("client_secret", input.ClientSecret)
	queries.Add("code", input.AuthorizationCode)
	queries.Add("redirect_uri", input.RedirectURI)
	input.ProviderURL.RawQuery = queries.Encode()

	// Cannot use GitHub's API hostname here. Must trim "api." prefix here.
	input.ProviderURL.Host = p.stripAPIPrefix(input.ProviderURL.Host)

	endpoint, err := url.JoinPath(input.ProviderURL.String(), path)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, endpoint, nil) // nosemgrep: G107-1
	if err != nil {
		return nil, fmt.Errorf("failed to prepare HTTP request: %v", err)
	}

	// Add request headers.
	request.Header.Add("Accept", types.JSONContentType)

	// Make the request.
	resp, err := p.client.Do(request)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err = resp.Body.Close(); err != nil {
			p.logger.Errorf("failed to close response body in CreateAccessToken: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to create access token. Response status: %s", resp.Status)
	}

	tokenResp := createAccessTokenResponse{}
	if err = json.NewDecoder(resp.Body).Decode(&tokenResp); err != nil {
		return nil, err
	}

	return &types.AccessTokenPayload{
		AccessToken: tokenResp.AccessToken,
	}, nil
}

// stripAPIPrefix removes the API prefix if using the provider's default URL.
// This is necessary for any user-facing actions, such as, OAuth flow,
// repository URL etc. i.e. actions that take place in the browser.
func (p *Provider) stripAPIPrefix(host string) string {
	if host == defaultURL.Host {
		host = strings.TrimPrefix(host, "api.")
	}

	return host
}
