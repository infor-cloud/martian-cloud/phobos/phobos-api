package github

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// Various constant values to make testing easier.
const (
	authorizationHeader = "Authorization"
	sampleValidToken    = types.BearerAuthPrefix + "an-access-token"
)

// customProviderURL is a url.URL for a custom provider instance.
var customProviderURL = url.URL{Scheme: "https", Host: "example.com", Path: "/instances/github"}

// roundTripFunc implements the RoundTripper interface.
type roundTripFunc func(r *http.Request) *http.Response

// RoundTrip executes a single HTTP transaction, returning
// a Response for the provided Request.
func (f roundTripFunc) RoundTrip(r *http.Request) (*http.Response, error) {
	return f(r), nil
}

// newTestClient returns *http.Client with Transport replaced to avoid making real calls.
func newTestClient(fn roundTripFunc) *http.Client {
	return &http.Client{
		Transport: roundTripFunc(fn),
	}
}

func TestDefaultURL(t *testing.T) {
	provider := &Provider{}

	assert.Equal(t, defaultURL, provider.DefaultURL())
}

func TestBuildOAuthAuthorizationURL(t *testing.T) {
	// URL should be sample for both test cases since GitHub
	// doesn't support read-only scopes.
	expectedAuthorizationCodeURL := "https://github.com/login/oauth/authorize?client_id=an-oauth-client-id" +
		"&redirect_uri=https%3A%2F%2Fphobos.domain%2Fv1%2Fvcs%2Fauth%2Fcallback" +
		"&scope=read%3Auser+repo&state=an-oauth-state"

	testCases := []struct {
		input       *types.BuildOAuthAuthorizationURLInput
		expectedURL string
		name        string
	}{
		{
			name: "positive: valid input scopes; expect authorization URL",
			input: &types.BuildOAuthAuthorizationURLInput{
				ProviderURL:   defaultURL,
				OAuthClientID: "an-oauth-client-id",
				OAuthState:    "an-oauth-state",
				RedirectURL:   "https://phobos.domain/v1/vcs/auth/callback",
				OAuthScopes:   gitHubRequiredOAuthScopes,
			},
			expectedURL: expectedAuthorizationCodeURL,
		},
		{
			name: "positive: valid input with custom provider URL; expect authorization URL",
			input: &types.BuildOAuthAuthorizationURLInput{
				ProviderURL:   customProviderURL, // Theoretical GitHub instance hosted under a path.
				OAuthClientID: "an-oauth-client-id",
				OAuthState:    "an-oauth-state",
				RedirectURL:   "https://phobos.domain/v1/vcs/auth/callback",
				OAuthScopes:   gitHubRequiredOAuthScopes,
			},
			expectedURL: customProviderURL.String() + "/login/oauth/authorize?client_id=an-oauth-client-id" +
				"&redirect_uri=https%3A%2F%2Fphobos.domain%2Fv1%2Fvcs%2Fauth%2Fcallback" +
				"&scope=read%3Auser+repo&state=an-oauth-state",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			provider := &Provider{}

			actualURL, err := provider.BuildOAuthAuthorizationURL(test.input)
			assert.Nil(t, err)
			assert.Equal(t, test.expectedURL, actualURL)
		})
	}
}

func TestBuildRepositoryURL(t *testing.T) {
	repositoryPath := "/owner/repository"

	testCases := []struct {
		name        string
		input       *types.BuildRepositoryURLInput
		expectedURL string
	}{
		{
			name: "repository URL with provider's default API URL",
			input: &types.BuildRepositoryURLInput{
				ProviderURL:    defaultURL,
				RepositoryPath: repositoryPath,
			},
			expectedURL: "https://github.com" + repositoryPath, // "api." prefix should be stripped.
		},
		{
			name: "repository URL with custom API URL",
			input: &types.BuildRepositoryURLInput{
				ProviderURL:    customProviderURL,
				RepositoryPath: repositoryPath,
			},
			expectedURL: customProviderURL.String() + repositoryPath,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			provider := &Provider{}

			actualURL, err := provider.BuildRepositoryURL(test.input)
			assert.Nil(t, err)
			assert.Equal(t, test.expectedURL, actualURL)
		})
	}
}

func TestTestConnection(t *testing.T) {
	ctx := context.Background()

	testCases := []struct {
		expectedError error
		input         *types.TestConnectionInput
		name          string
	}{
		{
			name: "positive: token and URL are valid; expect no errors",
			input: &types.TestConnectionInput{
				ProviderURL: defaultURL,
				AccessToken: "an-access-token",
			},
		},
		{
			name: "positive: token and URL are valid for a custom instance; expect no errors",
			input: &types.TestConnectionInput{
				ProviderURL: customProviderURL,
				AccessToken: "an-access-token",
			},
		},
		{
			name: "negative: token or URL is invalid; expect error",
			input: &types.TestConnectionInput{
				ProviderURL: defaultURL,
				AccessToken: "an-invalid-access-token",
			},
			expectedError: fmt.Errorf("failed to connect to VCS provider. Response status: %s", "401"),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			client := newTestClient(func(r *http.Request) *http.Response {
				assert.Equal(t, test.input.ProviderURL.Scheme, r.URL.Scheme)
				assert.Equal(t, test.input.ProviderURL.Host, r.URL.Host)
				assert.Equal(t, test.input.ProviderURL.Path+"/user", r.URL.Path)

				if r.Header.Get(authorizationHeader) != sampleValidToken {
					return &http.Response{
						StatusCode: http.StatusUnauthorized,
						Body:       nil,
						Status:     "401",
						Header:     make(http.Header),
					}
				}

				return &http.Response{
					StatusCode: http.StatusOK,
					Body:       nil,
					Status:     "200",
					Header:     make(http.Header),
				}
			})

			provider := &Provider{
				client: client,
			}

			err := provider.TestConnection(ctx, test.input)
			if test.expectedError != nil {
				assert.Equal(t, test.expectedError, err)
			} else if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestGetArchive(t *testing.T) {
	ctx := context.Background()

	testCases := []struct {
		expectedError error
		input         *types.GetArchiveInput
		name          string
	}{
		{
			name: "positive: input is valid; expect no errors",
			input: &types.GetArchiveInput{
				ProviderURL:    defaultURL,
				AccessToken:    "an-access-token",
				RepositoryPath: "owner/repository",
				Ref:            ptr.String("main"), // Attempting to download main branch.
			},
		},
		{
			name: "positive: input is valid with custom provider URL; expect no errors",
			input: &types.GetArchiveInput{
				ProviderURL:    customProviderURL,
				AccessToken:    "an-access-token",
				RepositoryPath: "owner/repository",
				Ref:            ptr.String("main"), // Attempting to download main branch.
			},
		},
		{
			name: "negative: input is invalid; expect error",
			input: &types.GetArchiveInput{
				ProviderURL:    defaultURL,
				AccessToken:    "some-token",
				RepositoryPath: "owner/repo",
				Ref:            ptr.String("feature/branch"),
			},
			expectedError: fmt.Errorf("failed to get repository archive. Response status: %s", "401"),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			client := newTestClient(func(r *http.Request) *http.Response {
				parts := []string{
					test.input.ProviderURL.Path,
					"repos",
					test.input.RepositoryPath,
					"tarball",
				}

				if test.input.Ref != nil {
					parts = append(parts, *test.input.Ref)
				}

				expectedPath := strings.Join(parts, "/")
				assert.Equal(t, test.input.ProviderURL.Scheme, r.URL.Scheme)
				assert.Equal(t, test.input.ProviderURL.Host, r.URL.Host)
				assert.Equal(t, expectedPath, r.URL.Path)

				if r.Header.Get(authorizationHeader) != sampleValidToken {
					return &http.Response{
						StatusCode: http.StatusUnauthorized,
						Body:       nil,
						Status:     "401",
						Header:     make(http.Header),
					}
				}

				return &http.Response{
					StatusCode: http.StatusOK,
					Body:       nil, // Payload won't matter since it never uses it.
					Status:     "200",
					Header:     make(http.Header),
				}
			})

			logger, _ := logger.NewForTest()
			provider := &Provider{
				logger: logger,
				client: client,
			}

			_, err := provider.GetArchive(ctx, test.input)
			if test.expectedError != nil {
				assert.Equal(t, test.expectedError, err)
			} else if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestCreateAccessToken(t *testing.T) {
	ctx := context.Background()

	testCases := []struct {
		expectedError   error
		input           *types.CreateAccessTokenInput
		response        *createAccessTokenResponse
		expectedPayload *types.AccessTokenPayload
		name            string
	}{
		{
			name: "positive: input is valid; expect no errors",
			input: &types.CreateAccessTokenInput{
				ProviderURL:       defaultURL,
				ClientID:          "some-client-id",
				ClientSecret:      "some-client-secret",
				AuthorizationCode: "some-authorization-code",
				RedirectURI:       "https://phobos.domain/v1/vcs/auth/callback",
				// Other fields aren't used for GitHub.
			},
			response: &createAccessTokenResponse{
				AccessToken: "some-access-token",
			},
			expectedPayload: &types.AccessTokenPayload{
				AccessToken: "some-access-token",
				// RefreshToken isn't used for GitHub.
			},
		},
		{
			name: "positive: input is valid with custom provider URL; expect no errors",
			input: &types.CreateAccessTokenInput{
				ProviderURL:       customProviderURL,
				ClientID:          "some-client-id",
				ClientSecret:      "some-client-secret",
				AuthorizationCode: "some-authorization-code",
				RedirectURI:       "https://phobos.domain/v1/vcs/auth/callback",
				// Other fields aren't used for GitHub.
			},
			response: &createAccessTokenResponse{
				AccessToken: "some-access-token",
			},
			expectedPayload: &types.AccessTokenPayload{
				AccessToken: "some-access-token",
				// RefreshToken isn't used for GitHub.
			},
		},
		{
			name: "negative: input is invalid; expect error",
			input: &types.CreateAccessTokenInput{
				ProviderURL:       defaultURL,
				ClientID:          "invalid",
				ClientSecret:      "invalid",
				AuthorizationCode: "invalid",
				RedirectURI:       "https://phobos.domain/v1/vcs/auth/callback",
			},
			expectedError: fmt.Errorf("failed to create access token. Response status: %s", "400"),
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			client := newTestClient(func(r *http.Request) *http.Response {
				expectedPath := path.Join(
					test.input.ProviderURL.Path,
					"/",
					"login",
					"oauth",
					"access_token",
				)

				// Host will be without 'api.' prefix.
				assert.Equal(t, test.input.ProviderURL.Scheme, r.URL.Scheme)
				assert.Equal(t, strings.TrimPrefix(test.input.ProviderURL.Host, "api."), r.URL.Host)
				assert.Equal(t, expectedPath, r.URL.Path)

				// Parse the queries.
				queries, err := url.ParseQuery(r.URL.RawQuery)
				assert.Nil(t, err)

				// Validate the values.
				assert.Equal(t, test.input.ClientID, queries.Get("client_id"))
				assert.Equal(t, test.input.ClientSecret, queries.Get("client_secret"))
				assert.Equal(t, test.input.AuthorizationCode, queries.Get("code"))
				assert.Equal(t, test.input.RedirectURI, queries.Get("redirect_uri"))

				// Emulate malformed input.
				if queries.Get("client_id") == "invalid" {
					return &http.Response{
						StatusCode: http.StatusBadRequest,
						Body:       nil,
						Status:     "400",
						Header:     make(http.Header),
					}
				}

				// Marshal the response payload.
				responsePayload, err := json.Marshal(test.response)
				assert.Nil(t, err)

				return &http.Response{
					StatusCode: http.StatusOK,
					Body:       io.NopCloser(bytes.NewBuffer(responsePayload)),
					Status:     "200",
					Header:     make(http.Header),
				}
			})

			logger, _ := logger.NewForTest()
			provider := &Provider{
				logger:    logger,
				client:    client,
				phobosURL: "http://phobos.domain",
			}

			payload, err := provider.CreateAccessToken(ctx, test.input)
			if test.expectedError != nil {
				assert.Equal(t, test.expectedError, err)
			} else if err != nil {
				t.Fatal(err)
			} else {
				assert.NotNil(t, payload)
				assert.Equal(t, test.expectedPayload.AccessToken, payload.AccessToken)
			}
		})
	}
}
