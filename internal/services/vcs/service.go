package vcs

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"slices"
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/google/uuid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/vcs/types"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const (
	// tokenExpirationLeeway is the headroom given to renew an
	// OAuth access token before it expires.
	tokenExpirationLeeway = time.Minute

	// oAuthCallBackEndpoint is the Phobos endpoint used
	// as a callback for completing the OAuth flow.
	oAuthCallBackEndpoint = "v1/vcs/auth/callback"
)

// GetVCSProvidersInput is the input for listing VCS providers.
type GetVCSProvidersInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.VCSProviderSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Search returns only the VCS providers with a name that
	// starts with the value of search
	Search *string
	// OrganizationID is the organization ID to filter VCS providers on
	OrganizationID *string
	// ProjectID is the project ID to filter VCS provider on
	ProjectID *string
}

// CreateOrganizationVCSProviderInput is the input for creating an organization VCS provider.
type CreateOrganizationVCSProviderInput struct {
	URL                 *string
	OAuthClientID       *string
	OAuthClientSecret   *string
	PersonalAccessToken *string
	OrganizationID      string
	Name                string
	Description         string
	Type                models.VCSProviderType
	AuthType            models.VCSProviderAuthType
	ExtraOAuthScopes    []string
}

// CreateProjectVCSProviderInput is the input for creating a project VCS provider.
type CreateProjectVCSProviderInput struct {
	URL                 *string
	OAuthClientID       *string
	OAuthClientSecret   *string
	PersonalAccessToken *string
	ProjectID           string
	Name                string
	Description         string
	Type                models.VCSProviderType
	AuthType            models.VCSProviderAuthType
	ExtraOAuthScopes    []string
}

// CreateVCSProviderResponse is the response for creating a VCS provider
type CreateVCSProviderResponse struct {
	VCSProvider           *models.VCSProvider
	OAuthAuthorizationURL *string
}

// UpdateVCSProviderInput is the input for updating a VCS provider.
type UpdateVCSProviderInput struct {
	Version             *int
	Description         *string
	OAuthClientID       *string
	OAuthClientSecret   *string
	PersonalAccessToken *string
	ID                  string
	ExtraOAuthScopes    []string
}

// DeleteVCSProviderInput is the input for deleting a VCS provider.
type DeleteVCSProviderInput struct {
	Version *int
	ID      string
}

// ResetVCSProviderOAuthTokenInput is the input for
type ResetVCSProviderOAuthTokenInput struct {
	ProviderID string
}

// ResetVCSProviderOAuthTokenResponse is the response for resetting a VCS OAuth token.
type ResetVCSProviderOAuthTokenResponse struct {
	VCSProvider           *models.VCSProvider
	OAuthAuthorizationURL string
}

// ProcessOAuthInput is the input for processing OAuth callback.
type ProcessOAuthInput struct {
	AuthorizationCode string
	State             string
}

// GetRepositoryArchiveInput is the input for retrieving a repository archive.
type GetRepositoryArchiveInput struct {
	Ref            *string
	ProviderID     string
	RepositoryPath string
}

// CreateVCSTokenForPipelineInput is the input for creating a VCS token.
type CreateVCSTokenForPipelineInput struct {
	ProviderID string
	PipelineID string
}

// CreateVCSTokenForPipelineResponse is the response for creating a VCS token.
type CreateVCSTokenForPipelineResponse struct {
	ExpiresAt   *time.Time
	AccessToken string
}

// Service implements all the functionality related to VCSProviders.
type Service interface {
	GetVCSProviderByID(ctx context.Context, id string) (*models.VCSProvider, error)
	GetVCSProviderByPRN(ctx context.Context, prn string) (*models.VCSProvider, error)
	GetVCSProviders(ctx context.Context, input *GetVCSProvidersInput) (*db.VCSProvidersResult, error)
	GetVCSProvidersByIDs(ctx context.Context, idList []string) ([]models.VCSProvider, error)
	CreateOrganizationVCSProvider(ctx context.Context, input *CreateOrganizationVCSProviderInput) (*CreateVCSProviderResponse, error)
	CreateProjectVCSProvider(ctx context.Context, input *CreateProjectVCSProviderInput) (*CreateVCSProviderResponse, error)
	UpdateVCSProvider(ctx context.Context, input *UpdateVCSProviderInput) (*models.VCSProvider, error)
	DeleteVCSProvider(ctx context.Context, input *DeleteVCSProviderInput) error
	ResetVCSProviderOAuthToken(ctx context.Context, input *ResetVCSProviderOAuthTokenInput) (*ResetVCSProviderOAuthTokenResponse, error)
	ProcessOAuth(ctx context.Context, input *ProcessOAuthInput) error
	GetRepositoryArchive(ctx context.Context, input *GetRepositoryArchiveInput) (io.ReadCloser, error)
	CreateVCSTokenForPipeline(ctx context.Context, input *CreateVCSTokenForPipelineInput) (*CreateVCSTokenForPipelineResponse, error)
}

type service struct {
	logger              logger.Logger
	dbClient            *db.Client
	activityService     activityevent.Service
	limitChecker        limits.LimitChecker
	vcsProviderMap      map[models.VCSProviderType]Provider
	oAuthStateGenerator func() (uuid.UUID, error) // Overriding for unit tests.
	phobosURL           string
}

// NewService returns an instance of Service.
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	activityService activityevent.Service,
	httpClient *http.Client,
	phobosURL string,
) (Service, error) {
	vcsProviderMap, err := NewVCSProviderMap(logger, httpClient, phobosURL)
	if err != nil {
		return nil, fmt.Errorf("failed to initialize vcs provider map %v", err)
	}

	return newService(
		logger,
		dbClient,
		activityService,
		limitChecker,
		vcsProviderMap,
		uuid.NewRandom,
		phobosURL,
	), nil
}

func newService(
	logger logger.Logger,
	dbClient *db.Client,
	activityService activityevent.Service,
	limitChecker limits.LimitChecker,
	vcsProviderMap map[models.VCSProviderType]Provider,
	oAuthStateGenerator func() (uuid.UUID, error),
	phobosURL string,
) Service {
	return &service{
		logger:              logger,
		dbClient:            dbClient,
		limitChecker:        limitChecker,
		activityService:     activityService,
		oAuthStateGenerator: oAuthStateGenerator,
		vcsProviderMap:      vcsProviderMap,
		phobosURL:           phobosURL,
	}
}

func (s *service) GetVCSProviderByID(ctx context.Context, id string) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "svc.GetVCSProviderByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	provider, err := s.getVCSProviderByID(ctx, id, span)
	if err != nil {
		return nil, err
	}

	switch provider.Scope {
	case models.OrganizationScope:
		if err := caller.RequireAccessToInheritableResource(ctx, models.VCSProviderResource, auth.WithOrganizationID(provider.OrganizationID)); err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if err := caller.RequirePermission(ctx, models.ViewVCSProvider, auth.WithProjectID(*provider.ProjectID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unexpected provider scope %s", provider.Scope)
	}

	return provider, nil
}

func (s *service) GetVCSProviderByPRN(ctx context.Context, prn string) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "svc.GetVCSProviderByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	provider, err := s.dbClient.VCSProviders.GetProviderByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get provider", errors.WithSpan(span))
	}

	if provider == nil {
		return nil, errors.New("vcs provider not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	switch provider.Scope {
	case models.OrganizationScope:
		if err := caller.RequireAccessToInheritableResource(ctx, models.VCSProviderResource, auth.WithOrganizationID(provider.OrganizationID)); err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if err := caller.RequirePermission(ctx, models.ViewVCSProvider, auth.WithProjectID(*provider.ProjectID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unexpected provider scope %s", provider.Scope)
	}

	return provider, nil
}

func (s *service) GetVCSProviders(ctx context.Context, input *GetVCSProvidersInput) (*db.VCSProvidersResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetVCSProviders")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetVCSProvidersInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.VCSProviderFilter{
			Search: input.Search,
		},
	}

	switch {
	case input.OrganizationID != nil:
		if err := caller.RequireAccessToInheritableResource(ctx, models.VCSProviderResource, auth.WithOrganizationID(*input.OrganizationID)); err != nil {
			return nil, err
		}

		// Filter to just organization vcs provider scopes.
		dbInput.Filter.OrganizationID = input.OrganizationID
		dbInput.Filter.VCSProviderScopes = []models.ScopeType{models.OrganizationScope}
	case input.ProjectID != nil:
		if err := caller.RequirePermission(ctx, models.ViewVCSProvider, auth.WithProjectID(*input.ProjectID)); err != nil {
			return nil, err
		}

		// Include both organization and project vcs provider scopes (allow inheritance).
		dbInput.Filter.ProjectID = input.ProjectID
		dbInput.Filter.VCSProviderScopes = []models.ScopeType{models.OrganizationScope, models.ProjectScope}
	default:
		return nil, errors.New("either an organization or project id must be specified",
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	return s.dbClient.VCSProviders.GetProviders(ctx, dbInput)
}

func (s *service) GetVCSProvidersByIDs(ctx context.Context, idList []string) ([]models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "svc.GetVCSProviders")
	span.SetAttributes(attribute.StringSlice("idList", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	result, err := s.dbClient.VCSProviders.GetProviders(ctx, &db.GetVCSProvidersInput{
		Filter: &db.VCSProviderFilter{
			VCSProviderIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get providers", errors.WithSpan(span))
	}

	for _, provider := range result.VCSProviders {
		switch provider.Scope {
		case models.OrganizationScope:
			if err := caller.RequireAccessToInheritableResource(ctx, models.VCSProviderResource, auth.WithOrganizationID(provider.OrganizationID)); err != nil {
				return nil, err
			}
		case models.ProjectScope:
			if err := caller.RequirePermission(ctx, models.ViewVCSProvider, auth.WithProjectID(*provider.ProjectID)); err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("unexpected provider scope %s", provider.Scope)
		}
	}

	return result.VCSProviders, nil
}

func (s *service) CreateOrganizationVCSProvider(ctx context.Context, input *CreateOrganizationVCSProviderInput) (*CreateVCSProviderResponse, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateOrganizationVCSProvider")
	span.SetAttributes(attribute.String("name", input.Name))
	span.SetAttributes(attribute.String("organizationId", input.OrganizationID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.CreateVCSProvider, auth.WithOrganizationID(input.OrganizationID)); err != nil {
		return nil, err
	}

	// Check if provider is supported.
	provider, err := s.getRemoteProvider(input.Type)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get remote provider instance", errors.WithSpan(span))
	}

	var providerURL url.URL
	if input.URL == nil {
		// Use the default providerURL if nothing provided.
		providerURL = provider.DefaultURL()
	} else {
		// Otherwise parse and attempt to "normalize" it.
		normalizedURL, uErr := s.normalizeProviderURL(*input.URL)
		if uErr != nil {
			return nil, errors.Wrap(uErr, "failed to normalize provider url", errors.WithSpan(span))
		}

		providerURL = *normalizedURL
	}

	if len(input.ExtraOAuthScopes) > 0 {
		slices.Sort(input.ExtraOAuthScopes)
		input.ExtraOAuthScopes = slices.Compact(input.ExtraOAuthScopes)
	}

	toCreate := &models.VCSProvider{
		Name:                input.Name,
		Description:         input.Description,
		CreatedBy:           caller.GetSubject(),
		OrganizationID:      input.OrganizationID,
		URL:                 providerURL,
		OAuthClientID:       input.OAuthClientID,
		OAuthClientSecret:   input.OAuthClientSecret,
		ExtraOAuthScopes:    input.ExtraOAuthScopes,
		PersonalAccessToken: input.PersonalAccessToken,
		Scope:               models.OrganizationScope,
		Type:                input.Type,
		AuthType:            input.AuthType,
	}

	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate VCS provider model", errors.WithSpan(span))
	}

	switch input.AuthType {
	case models.OAuthType:
		// Only generate state if using OAuth.
		oAuthState, uErr := s.oAuthStateGenerator()
		if uErr != nil {
			return nil, errors.Wrap(uErr, "failed to generate OAuth state", errors.WithSpan(span))
		}

		toCreate.OAuthState = ptr.String(oAuthState.String())
	case models.AccessTokenType:
		// Make sure the personal access token is valid.
		if tErr := provider.TestConnection(ctx, &types.TestConnectionInput{
			ProviderURL: toCreate.URL,
			AccessToken: ptr.ToString(toCreate.PersonalAccessToken),
		}); tErr != nil {
			return nil, errors.Wrap(tErr, "failed to validate personal access token",
				errors.WithErrorCode(errors.EInvalid),
				errors.WithSpan(span),
			)
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateOrganizationVCSProvider: %v", txErr)
		}
	}()

	createdProvider, err := s.dbClient.VCSProviders.CreateProvider(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create vcs provider", errors.WithSpan(span))
	}

	// Get the number of VCS providers in the organization to check whether we just violated the limit.
	newVCSProviders, err := s.dbClient.VCSProviders.GetProviders(txContext, &db.GetVCSProvidersInput{
		Filter: &db.VCSProviderFilter{
			OrganizationID:    &input.OrganizationID,
			VCSProviderScopes: []models.ScopeType{models.OrganizationScope},
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get VCS providers", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(
		txContext,
		limits.ResourceLimitVCSProvidersPerOrganization,
		newVCSProviders.PageInfo.TotalCount,
	); err != nil {
		return nil, errors.Wrap(err, "failed to check limit", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &input.OrganizationID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetVCSProvider,
			TargetID:       &createdProvider.Metadata.ID,
		}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	var authorizationURL *string
	if createdProvider.AuthType == models.OAuthType {
		authURL, cErr := s.getOAuthAuthorizationURL(createdProvider)
		if cErr != nil {
			return nil, errors.Wrap(cErr, "failed to get authorization URL", errors.WithSpan(span))
		}

		authorizationURL = &authURL
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a VCS provider.",
		"caller", caller.GetSubject(),
		"name", createdProvider.Name,
		"organizationId", input.OrganizationID,
		"type", input.Type,
		"authType", input.AuthType,
	)

	return &CreateVCSProviderResponse{
		VCSProvider:           createdProvider,
		OAuthAuthorizationURL: authorizationURL,
	}, nil
}

func (s *service) CreateProjectVCSProvider(ctx context.Context, input *CreateProjectVCSProviderInput) (*CreateVCSProviderResponse, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateProjectVCSProvider")
	span.SetAttributes(attribute.String("name", input.Name))
	span.SetAttributes(attribute.String("projectId", input.ProjectID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.CreateVCSProvider, auth.WithProjectID(input.ProjectID)); err != nil {
		return nil, err
	}

	// Check if provider is supported.
	provider, err := s.getRemoteProvider(input.Type)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get remote provider instance", errors.WithSpan(span))
	}

	var providerURL url.URL
	if input.URL == nil {
		// Use the default providerURL if nothing provided.
		providerURL = provider.DefaultURL()
	} else {
		// Otherwise parse and attempt to "normalize" it.
		normalizedURL, uErr := s.normalizeProviderURL(*input.URL)
		if uErr != nil {
			return nil, errors.Wrap(uErr, "failed to normalize provider url", errors.WithSpan(span))
		}

		providerURL = *normalizedURL
	}

	// We'll need the project to fetch the organization id.
	project, err := s.dbClient.Projects.GetProjectByID(ctx, input.ProjectID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project by id", errors.WithSpan(span))
	}

	if project == nil {
		return nil, errors.New("project with id %s not found", input.ProjectID, errors.WithSpan(span))
	}

	if len(input.ExtraOAuthScopes) > 0 {
		slices.Sort(input.ExtraOAuthScopes)
		input.ExtraOAuthScopes = slices.Compact(input.ExtraOAuthScopes)
	}

	toCreate := &models.VCSProvider{
		Name:                input.Name,
		Description:         input.Description,
		CreatedBy:           caller.GetSubject(),
		OrganizationID:      project.OrgID,
		ProjectID:           &input.ProjectID,
		URL:                 providerURL,
		OAuthClientID:       input.OAuthClientID,
		OAuthClientSecret:   input.OAuthClientSecret,
		ExtraOAuthScopes:    input.ExtraOAuthScopes,
		PersonalAccessToken: input.PersonalAccessToken,
		Scope:               models.ProjectScope,
		Type:                input.Type,
		AuthType:            input.AuthType,
	}

	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate VCS provider model", errors.WithSpan(span))
	}

	switch input.AuthType {
	case models.OAuthType:
		// Only generate state if using OAuth.
		oAuthState, uErr := s.oAuthStateGenerator()
		if uErr != nil {
			return nil, errors.Wrap(uErr, "failed to generate OAuth state", errors.WithSpan(span))
		}

		toCreate.OAuthState = ptr.String(oAuthState.String())
	case models.AccessTokenType:
		// Make sure the personal access token is valid.
		if tErr := provider.TestConnection(ctx, &types.TestConnectionInput{
			ProviderURL: toCreate.URL,
			AccessToken: ptr.ToString(toCreate.PersonalAccessToken),
		}); tErr != nil {
			return nil, errors.Wrap(tErr, "failed to validate personal access token",
				errors.WithErrorCode(errors.EInvalid),
				errors.WithSpan(span),
			)
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateProjectVCSProvider: %v", txErr)
		}
	}()

	createdProvider, err := s.dbClient.VCSProviders.CreateProvider(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create vcs provider", errors.WithSpan(span))
	}

	// Get the number of VCS providers in the project to check whether we just violated the limit.
	newVCSProviders, err := s.dbClient.VCSProviders.GetProviders(txContext, &db.GetVCSProvidersInput{
		Filter: &db.VCSProviderFilter{
			ProjectID:         &input.ProjectID,
			VCSProviderScopes: []models.ScopeType{models.ProjectScope},
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get VCS providers", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(
		txContext,
		limits.ResourceLimitVCSProvidersPerProject,
		newVCSProviders.PageInfo.TotalCount,
	); err != nil {
		return nil, errors.Wrap(err, "failed to check limit", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &input.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetVCSProvider,
			TargetID:   &createdProvider.Metadata.ID,
		}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	var authorizationURL *string
	if createdProvider.AuthType == models.OAuthType {
		authURL, cErr := s.getOAuthAuthorizationURL(createdProvider)
		if cErr != nil {
			return nil, errors.Wrap(cErr, "failed to get authorization URL", errors.WithSpan(span))
		}

		authorizationURL = &authURL
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a VCS provider.",
		"caller", caller.GetSubject(),
		"name", createdProvider.Name,
		"projectId", input.ProjectID,
		"type", input.Type,
		"authType", input.AuthType,
	)

	return &CreateVCSProviderResponse{
		VCSProvider:           createdProvider,
		OAuthAuthorizationURL: authorizationURL,
	}, nil
}

func (s *service) UpdateVCSProvider(ctx context.Context, input *UpdateVCSProviderInput) (*models.VCSProvider, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateVCSProvider")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	provider, err := s.getVCSProviderByID(ctx, input.ID, span)
	if err != nil {
		return nil, err
	}

	var organizationID *string
	switch provider.Scope {
	case models.OrganizationScope:
		if err = caller.RequirePermission(ctx, models.UpdateVCSProvider, auth.WithOrganizationID(provider.OrganizationID)); err != nil {
			return nil, err
		}

		organizationID = &provider.OrganizationID
	case models.ProjectScope:
		if err = caller.RequirePermission(ctx, models.UpdateVCSProvider, auth.WithProjectID(*provider.ProjectID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unexpected VCS provider scope %s", provider.Scope, errors.WithSpan(span))
	}

	remoteProvider, pErr := s.getRemoteProvider(provider.Type)
	if pErr != nil {
		return nil, errors.Wrap(pErr, "failed to get remote provider instance", errors.WithSpan(span))
	}

	if input.Version != nil {
		provider.Metadata.Version = *input.Version
	}

	if input.Description != nil {
		provider.Description = *input.Description
	}

	if input.OAuthClientID != nil {
		provider.OAuthClientID = input.OAuthClientID
	}

	if input.OAuthClientSecret != nil {
		provider.OAuthClientSecret = input.OAuthClientSecret
	}

	if input.PersonalAccessToken != nil {
		// Make sure the personal access token is valid.
		if tErr := remoteProvider.TestConnection(ctx, &types.TestConnectionInput{
			ProviderURL: provider.URL,
			AccessToken: *input.PersonalAccessToken,
		}); tErr != nil {
			return nil, errors.Wrap(tErr, "failed to validate personal access token",
				errors.WithErrorCode(errors.EInvalid),
				errors.WithSpan(span),
			)
		}

		provider.PersonalAccessToken = input.PersonalAccessToken
	}

	if input.ExtraOAuthScopes != nil {
		slices.Sort(input.ExtraOAuthScopes)
		provider.ExtraOAuthScopes = slices.Compact(input.ExtraOAuthScopes)
	}

	if err = provider.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate VCS provider model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateVCSProvider: %v", txErr)
		}
	}()

	updatedProvider, err := s.dbClient.VCSProviders.UpdateProvider(txContext, provider)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update provider", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: organizationID,
			ProjectID:      provider.ProjectID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetVCSProvider,
			TargetID:       &updatedProvider.Metadata.ID,
		}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a VCS provider.",
		"caller", caller.GetSubject(),
		"id", updatedProvider.Metadata.ID,
	)

	return updatedProvider, nil
}

func (s *service) DeleteVCSProvider(ctx context.Context, input *DeleteVCSProviderInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteVCSProvider")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	provider, err := s.getVCSProviderByID(ctx, input.ID, span)
	if err != nil {
		return err
	}

	var (
		targetType     models.ActivityEventTargetType
		targetID       *string
		organizationID *string // Only needed for org providers.
	)

	switch provider.Scope {
	case models.OrganizationScope:
		if err = caller.RequirePermission(ctx, models.DeleteVCSProvider, auth.WithOrganizationID(provider.OrganizationID)); err != nil {
			return err
		}

		organizationID = &provider.OrganizationID
		targetID = &provider.OrganizationID
		targetType = models.TargetOrganization
	case models.ProjectScope:
		if err = caller.RequirePermission(ctx, models.DeleteVCSProvider, auth.WithProjectID(*provider.ProjectID)); err != nil {
			return err
		}

		targetID = provider.ProjectID
		targetType = models.TargetProject
	default:
		return errors.New("unexpected VCS provider scope %s", provider.Scope, errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteVCSProvider: %v", txErr)
		}
	}()

	if err = s.dbClient.VCSProviders.DeleteProvider(txContext, provider); err != nil {
		return errors.Wrap(err, "failed to delete provider", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: organizationID,
			ProjectID:      provider.ProjectID,
			Action:         models.ActionDelete,
			TargetType:     targetType,
			TargetID:       targetID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &provider.Name,
				ID:   provider.Metadata.ID,
				Type: string(models.TargetVCSProvider),
			},
		}); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit transactions", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a VCS provider.",
		"caller", caller.GetSubject(),
		"name", provider.Name,
		"targetId", targetID,
		"scope", provider.Scope,
	)

	return nil
}

func (s *service) ResetVCSProviderOAuthToken(ctx context.Context, input *ResetVCSProviderOAuthTokenInput) (*ResetVCSProviderOAuthTokenResponse, error) {
	ctx, span := tracer.Start(ctx, "svc.ResetVCSProviderOAuthToken")
	span.SetAttributes(attribute.String("providerID", input.ProviderID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	provider, err := s.getVCSProviderByID(ctx, input.ProviderID, span)
	if err != nil {
		return nil, err
	}

	switch provider.Scope {
	case models.OrganizationScope:
		if err = caller.RequirePermission(ctx, models.UpdateVCSProvider, auth.WithOrganizationID(provider.OrganizationID)); err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if err = caller.RequirePermission(ctx, models.UpdateVCSProvider, auth.WithProjectID(*provider.ProjectID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unexpected VCS provider scope %s", provider.Scope, errors.WithSpan(span))
	}

	if provider.AuthType != models.OAuthType {
		return nil, errors.New("oauth token can only be reset for providers using %s auth type", models.OAuthType)
	}

	// Use a UUID for the state.
	oAuthState, err := s.oAuthStateGenerator()
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate an OAuth state", errors.WithSpan(span))
	}

	// Update fields with state value / reset fields.
	provider.OAuthAccessToken = nil
	provider.OAuthRefreshToken = nil
	provider.OAuthAccessTokenExpiresAt = nil
	provider.OAuthState = ptr.String(oAuthState.String())

	updatedProvider, err := s.dbClient.VCSProviders.UpdateProvider(ctx, provider)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update provider", errors.WithSpan(span))
	}

	authorizationURL, err := s.getOAuthAuthorizationURL(provider)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get OAuth authorization URL", errors.WithSpan(span))
	}

	return &ResetVCSProviderOAuthTokenResponse{
		VCSProvider:           updatedProvider,
		OAuthAuthorizationURL: authorizationURL,
	}, nil
}

func (s *service) ProcessOAuth(ctx context.Context, input *ProcessOAuthInput) error {
	ctx, span := tracer.Start(ctx, "svc.ProcessOAuth")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	// Make sure the state value if a valid UUID. Avoids
	// a DB query for random calls to the endpoint.
	if _, err = uuid.Parse(input.State); err != nil {
		return errors.Wrap(err, "failed to parse state value", errors.WithSpan(span))
	}

	// Validate the state value.
	vp, err := s.dbClient.VCSProviders.GetProviderByOAuthState(ctx, input.State)
	if err != nil {
		return errors.Wrap(err, "failed to get provider by OAuth state", errors.WithSpan(span))
	}

	if vp == nil {
		return errors.New("VCS provider not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	// Require UpdateVCSProvider since we're updating the provider's values.
	switch vp.Scope {
	case models.OrganizationScope:
		if err = caller.RequirePermission(ctx, models.UpdateVCSProvider, auth.WithOrganizationID(vp.OrganizationID)); err != nil {
			return err
		}
	case models.ProjectScope:
		if err = caller.RequirePermission(ctx, models.UpdateVCSProvider, auth.WithProjectID(*vp.ProjectID)); err != nil {
			return err
		}
	default:
		return errors.New("unexpected VCS provider scope %s", vp.Scope, errors.WithSpan(span))
	}

	provider, err := s.getRemoteProvider(vp.Type)
	if err != nil {
		return errors.Wrap(err, "failed to get remote provider instance", errors.WithSpan(span))
	}

	redirectURL, err := s.getOAuthCallBackURL()
	if err != nil {
		return errors.Wrap(err, "failed to get Phobos OAuth callback URL", errors.WithSpan(span))
	}

	// Create the access token with the provider.
	payload, err := provider.CreateAccessToken(ctx, &types.CreateAccessTokenInput{
		ProviderURL:       vp.URL,
		ClientID:          *vp.OAuthClientID,
		ClientSecret:      *vp.OAuthClientSecret,
		AuthorizationCode: input.AuthorizationCode,
		RedirectURI:       redirectURL,
	})
	if err != nil {
		return errors.Wrap(err, "failed to create access token", errors.WithSpan(span))
	}

	// Test the access token incase the value wasn't retrieved for some reason.
	if err = provider.TestConnection(ctx, &types.TestConnectionInput{
		ProviderURL: vp.URL,
		AccessToken: payload.AccessToken,
	}); err != nil {
		return errors.Wrap(err, "could not validate provider issued access token", errors.WithSpan(span))
	}

	// Reset OAuthState value in provider.
	vp.OAuthState = nil
	vp.OAuthAccessToken = &payload.AccessToken

	// Not all provider's (e.g. GitHub) support refresh tokens for OAuth apps.
	if payload.RefreshToken != "" {
		vp.OAuthRefreshToken = &payload.RefreshToken
		vp.OAuthAccessTokenExpiresAt = payload.ExpirationTimestamp
	}

	// Update the provider.
	if _, err = s.dbClient.VCSProviders.UpdateProvider(ctx, vp); err != nil {
		return errors.Wrap(err, "failed to update VCS provider", errors.WithSpan(span))
	}

	return nil
}

func (s *service) GetRepositoryArchive(ctx context.Context, input *GetRepositoryArchiveInput) (io.ReadCloser, error) {
	ctx, span := tracer.Start(ctx, "svc.GetRepositoryArchive")
	span.SetAttributes(attribute.String("providerId", input.ProviderID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	vp, err := s.getVCSProviderByID(ctx, input.ProviderID, span)
	if err != nil {
		return nil, err
	}

	switch vp.Scope {
	case models.OrganizationScope:
		if err = caller.RequireAccessToInheritableResource(ctx, models.VCSProviderResource, auth.WithOrganizationID(vp.OrganizationID)); err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if err = caller.RequirePermission(ctx, models.ViewVCSProvider, auth.WithProjectID(*vp.ProjectID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unexpected VCS provider scope %s", vp.Scope, errors.WithSpan(span))
	}

	provider, err := s.getRemoteProvider(vp.Type)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get remote provider instance", errors.WithSpan(span))
	}

	// Get auth token.
	accessToken, err := s.getAuthToken(ctx, provider, vp)
	if err != nil {
		return nil, errors.Wrap(err, "failed to refresh OAuth token", errors.WithSpan(span))
	}

	// Get the archive with our options.
	archiveResponse, err := provider.GetArchive(ctx, &types.GetArchiveInput{
		ProviderURL:    vp.URL,
		AccessToken:    accessToken,
		RepositoryPath: input.RepositoryPath,
		Ref:            input.Ref,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get repository archive", errors.WithSpan(span))
	}

	return archiveResponse.Body, nil
}

func (s *service) CreateVCSTokenForPipeline(ctx context.Context, input *CreateVCSTokenForPipelineInput) (*CreateVCSTokenForPipelineResponse, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateVCSTokenForPipeline")
	span.SetAttributes(attribute.String("providerId", input.ProviderID))
	span.SetAttributes(attribute.String("pipelineId", input.PipelineID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipeline, err := s.dbClient.Pipelines.GetPipelineByID(ctx, input.PipelineID)
	if err != nil {
		return nil, err
	}

	if pipeline == nil {
		return nil, errors.New(
			"pipeline with id %s not found",
			input.PipelineID,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	// Check whether the caller is allowed to update this pipeline.
	if err = caller.RequirePermission(
		ctx,
		models.UpdatePipelineState,
		auth.WithProjectID(pipeline.ProjectID),
		auth.WithPipelineID(pipeline.Metadata.ID),
	); err != nil {
		return nil, err
	}

	project, err := s.dbClient.Projects.GetProjectByID(ctx, pipeline.ProjectID)
	if err != nil {
		return nil, err
	}

	if project == nil {
		return nil, errors.New(
			"project with id %s associated with pipeline not found",
			pipeline.ProjectID,
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	vp, err := s.getVCSProviderByID(ctx, input.ProviderID, span)
	if err != nil {
		return nil, err
	}

	switch vp.Scope {
	case models.OrganizationScope:
		if vp.OrganizationID != project.OrgID {
			return nil, errors.New(
				"VCS provider %s is not associated with organization %s",
				vp.Metadata.ID,
				project.OrgID,
				errors.WithErrorCode(errors.EForbidden),
				errors.WithSpan(span),
			)
		}
	case models.ProjectScope:
		if *vp.ProjectID != pipeline.ProjectID {
			return nil, errors.New(
				"VCS provider %s is not associated with project %s",
				vp.Metadata.ID,
				pipeline.ProjectID,
				errors.WithErrorCode(errors.EForbidden),
				errors.WithSpan(span),
			)
		}
	default:
		return nil, errors.New("unexpected VCS provider scope %s", vp.Scope, errors.WithSpan(span))
	}

	provider, err := s.getRemoteProvider(vp.Type)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get remote provider instance", errors.WithSpan(span))
	}

	// Get auth token.
	accessToken, err := s.getAuthToken(ctx, provider, vp)
	if err != nil {
		return nil, errors.Wrap(err, "failed to refresh OAuth token", errors.WithSpan(span))
	}

	// Note: Only OAuth tokens can expire and be renewed.
	return &CreateVCSTokenForPipelineResponse{
		AccessToken: accessToken,
		ExpiresAt:   vp.OAuthAccessTokenExpiresAt,
	}, nil
}

func (s *service) getRemoteProvider(typ models.VCSProviderType) (Provider, error) {
	provider, ok := s.vcsProviderMap[typ]
	if !ok {
		return nil, errors.New("VCS type %s is not supported", typ, errors.WithErrorCode(errors.EInvalid))
	}

	return provider, nil
}

func (s *service) getOAuthAuthorizationURL(vcsProvider *models.VCSProvider) (string, error) {
	// Check if a valid state value is available.
	if vcsProvider.OAuthState == nil {
		return "", errors.New("oauth state is not set")
	}

	redirectURL, err := s.getOAuthCallBackURL()
	if err != nil {
		return "", err
	}

	provider, err := s.getRemoteProvider(vcsProvider.Type)
	if err != nil {
		return "", err
	}

	authorizationURLInput := &types.BuildOAuthAuthorizationURLInput{
		ProviderURL:   vcsProvider.URL,
		OAuthClientID: *vcsProvider.OAuthClientID,
		OAuthState:    *vcsProvider.OAuthState,
		OAuthScopes:   vcsProvider.ExtraOAuthScopes,
		RedirectURL:   redirectURL,
	}

	// Build authorization code URL for the provider which
	// identity provider can use to complete OAuth flow.
	return provider.BuildOAuthAuthorizationURL(authorizationURLInput)
}

// getAuthToken returns the auth token the provider uses based on the auth scheme.
// Renews the OAuth token if applicable.
func (s *service) getAuthToken(ctx context.Context, provider Provider, vp *models.VCSProvider) (string, error) {
	switch vp.AuthType {
	case models.AccessTokenType:
		// Use the personal access token.
		return *vp.PersonalAccessToken, nil
	case models.OAuthType:
		if vp.OAuthAccessToken == nil {
			// OAuthAccessToken could be nil if OAuth token has been reset, but OAuth flow hasn't been completed yet.
			return "", errors.New("No available access token, please complete OAuth flow first", errors.WithErrorCode(errors.EInvalid))
		}

		if vp.OAuthRefreshToken == nil {
			// Since no refresh token is available, use the token on the provider.
			return *vp.OAuthAccessToken, nil
		}

		if vp.OAuthAccessTokenExpiresAt != nil && vp.OAuthAccessTokenExpiresAt.After(time.Now().Add(tokenExpirationLeeway)) {
			// Since the access token hasn't expired yet, continue to use it.
			return *vp.OAuthAccessToken, nil
		}

		redirectURI, err := s.getOAuthCallBackURL()
		if err != nil {
			return "", err
		}

		// Renew the access token.
		payload, err := provider.CreateAccessToken(ctx, &types.CreateAccessTokenInput{
			ProviderURL:  vp.URL,
			ClientID:     *vp.OAuthClientID,
			ClientSecret: *vp.OAuthClientSecret,
			RedirectURI:  redirectURI,
			RefreshToken: *vp.OAuthRefreshToken, // We're renewing the access token.
		})
		if err != nil {
			return "", err
		}

		// Update fields.
		vp.OAuthAccessToken = &payload.AccessToken
		vp.OAuthRefreshToken = &payload.RefreshToken
		vp.OAuthAccessTokenExpiresAt = payload.ExpirationTimestamp

		// Update provider.
		if _, err = s.dbClient.VCSProviders.UpdateProvider(ctx, vp); err != nil {
			return "", err
		}

		return payload.AccessToken, nil
	default:
		return "", errors.New("unsupported vcs provider auth type %s", vp.AuthType)
	}
}

// normalizeProviderURL parses inputURL and normalizes it by removing some unneeded parts.
func (s *service) normalizeProviderURL(inputURL string) (*url.URL, error) {
	parsedURL, err := url.ParseRequestURI(inputURL)
	if err != nil || (parsedURL.Scheme == "") || (parsedURL.Host == "") {
		return nil, errors.New("invalid provider URL", errors.WithErrorCode(errors.EInvalid))
	}

	// Remove any trailing backslash.
	parsedURL.Path = strings.TrimSuffix(parsedURL.Path, "/")

	return parsedURL, nil
}

func (s *service) getOAuthCallBackURL() (string, error) {
	phobosURL, err := url.Parse(s.phobosURL)
	if err != nil {
		return "", fmt.Errorf("failed to parse phobos URL: %v", err)
	}

	// Add the callback endpoint to the URL.
	phobosURL.Path = oAuthCallBackEndpoint

	return phobosURL.String(), nil
}

func (s *service) getVCSProviderByID(ctx context.Context, id string, span trace.Span) (*models.VCSProvider, error) {
	provider, err := s.dbClient.VCSProviders.GetProviderByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get provider", errors.WithSpan(span))
	}

	if provider == nil {
		return nil, errors.New("VCS provider not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return provider, nil
}
