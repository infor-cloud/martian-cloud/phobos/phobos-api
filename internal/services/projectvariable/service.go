// Package projectvariable set implements functionality related to project variables
package projectvariable

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// maxProjectVariablesPerProjectVariableSet is the maximum number of variables
// that can be associated with a project variable set
const maxProjectVariablesPerProjectVariableSet = 500

// GetProjectVariableSetsInput is the input for querying a list of variable sets
type GetProjectVariableSetsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ProjectVariableSetSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// The project ID to filter by
	ProjectID string
	// Latest specifies if the latest variable set should be returned
	Latest *bool
	// VariableID is the ID of the variable to filter by
	VariableID *string
	// RevisionSearch is the revision to search for
	RevisionSearch *string
}

// GetProjectVariablesInput is the input for querying a list of variables
type GetProjectVariablesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ProjectVariableSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// ProjectID is the project ID to filter by
	ProjectID string
	// The project variable set ID to filter by
	ProjectVariableSetID *string
	// PipelineType is the pipeline type to filter by
	PipelineType *models.PipelineType
	// VariableKey is the key of the variable to filter by
	VariableKey *string
	// EnvironmentScopes are the environments to filter by
	EnvironmentScopes []string
	// CreatedAtTimeRangeEnd is the end of the created at time range to filter by (inclusive)
	CreatedAtTimeRangeEnd *time.Time
	// ExcludeVariableIDs is a list of variable IDs to exclude
	ExcludeVariableIDs []string
	// Search is used to search for a variable by key
	Search *string
}

// SetProjectVariablesInput is the input for replacing all the variables in a single operation
type SetProjectVariablesInput struct {
	ProjectID string
	Variables []*models.ProjectVariable
}

func (s *SetProjectVariablesInput) validate() error {
	if s.ProjectID == "" {
		return errors.New("project ID is required", errors.WithErrorCode(errors.EInvalid))
	}

	if len(s.Variables) > maxProjectVariablesPerProjectVariableSet {
		return errors.New(
			"a project variable set cannot have more than %d variables",
			maxProjectVariablesPerProjectVariableSet,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	variableMapByKey := map[string]bool{}

	for _, variable := range s.Variables {
		if err := variable.Validate(); err != nil {
			return err
		}

		// Check if variable with key already exists in the set
		key := variable.FQN()
		if _, ok := variableMapByKey[key]; ok {
			return errors.New(
				"variable with key %s already exists in the set", key,
				errors.WithErrorCode(errors.EInvalid))
		}

		variableMapByKey[key] = true
	}

	// Check if variable scope is conflicting with existing variable
	for _, variable := range s.Variables {
		if variable.PipelineType == models.DeploymentPipelineType {
			if variable.EnvironmentScope != models.ProjectVariableEnvironmentScopeAll {
				// Verify that there is not a variable with the same key set for all environments
				if _, ok := variableMapByKey[fmt.Sprintf("%s:%s:%s", string(variable.PipelineType), "*", variable.Key)]; ok {
					return errors.New(
						"cannot set deployment variable with name %s because a variable with the same name is set for all environments",
						variable.Key,
						errors.WithErrorCode(errors.EInvalid),
					)
				}
			}
		}
	}

	return nil
}

// PatchProjectVariablesInput is the input for modifying project variables
type PatchProjectVariablesInput struct {
	ProjectID         string
	RemoveVariableIDs []string
	CreateVariables   []*models.ProjectVariable
	ReplaceVariables  []*models.ProjectVariable
}

func (p *PatchProjectVariablesInput) validate() error {
	if p.ProjectID == "" {
		return errors.New("project ID is required", errors.WithErrorCode(errors.EInvalid))
	}

	for _, variable := range p.CreateVariables {
		if err := variable.Validate(); err != nil {
			return err
		}
	}

	replaceIDMap := map[string]bool{}
	for _, variable := range p.ReplaceVariables {
		if err := variable.Validate(); err != nil {
			return err
		}
		replaceIDMap[variable.Metadata.ID] = true
	}

	// Check if a variable is being removed and replaced at the same time
	for _, id := range p.RemoveVariableIDs {
		if _, ok := replaceIDMap[id]; ok {
			return errors.New(
				"cannot remove and replace variable with id %q at the same time",
				gid.ToGlobalID(gid.ProjectVariableType, id),
				errors.WithErrorCode(errors.EInvalid),
			)
		}
	}

	return nil
}

// Service implements all variable set related functionality
type Service interface {
	GetProjectVariableSetByID(ctx context.Context, id string) (*models.ProjectVariableSet, error)
	GetProjectVariableSetByPRN(ctx context.Context, prn string) (*models.ProjectVariableSet, error)
	GetProjectVariableSetsByIDs(ctx context.Context, idList []string) ([]*models.ProjectVariableSet, error)
	GetProjectVariableSets(ctx context.Context, input *GetProjectVariableSetsInput) (*db.ProjectVariableSetsResult, error)
	GetProjectVariableByID(ctx context.Context, id string) (*models.ProjectVariable, error)
	GetProjectVariableByPRN(ctx context.Context, prn string) (*models.ProjectVariable, error)
	GetProjectVariablesByIDs(ctx context.Context, idList []string) ([]*models.ProjectVariable, error)
	GetProjectVariables(ctx context.Context, input *GetProjectVariablesInput) (*db.ProjectVariablesResult, error)
	SetProjectVariables(ctx context.Context, input *SetProjectVariablesInput) (*models.ProjectVariableSet, error)
	PatchProjectVariables(ctx context.Context, input *PatchProjectVariablesInput) (*models.ProjectVariableSet, error)
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	activityService activityevent.Service
	limitChecker    limits.LimitChecker
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	activityService activityevent.Service,
	limitChecker limits.LimitChecker,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		activityService: activityService,
		limitChecker:    limitChecker,
	}
}

func (s *service) GetProjectVariableSetByID(ctx context.Context, id string) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariableSetByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	set, err := s.getProjectVariableSetByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(set.ProjectID))
	if err != nil {
		return nil, err
	}

	return set, nil
}

func (s *service) GetProjectVariableSetByPRN(ctx context.Context, prn string) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariableSetByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	variableSet, err := s.dbClient.ProjectVariableSets.GetProjectVariableSetByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variable set by PRN", errors.WithSpan(span))
	}

	if variableSet == nil {
		return nil, errors.New(
			"variable set with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(variableSet.ProjectID))
	if err != nil {
		return nil, err
	}

	return variableSet, nil
}

func (s *service) GetProjectVariableSetsByIDs(ctx context.Context, idList []string) ([]*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariableSetsByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetProjectVariableSetsInput{
		Filter: &db.ProjectVariableSetFilter{VariableSetIDs: idList},
	}

	resp, err := s.dbClient.ProjectVariableSets.GetProjectVariableSets(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variable sets", errors.WithSpan(span))
	}

	projectIDs := map[string]struct{}{}
	for _, set := range resp.ProjectVariableSets {
		if _, ok := projectIDs[set.ProjectID]; ok {
			// Skip since this project has already been checked
			continue
		}

		projectIDs[set.ProjectID] = struct{}{}
		err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(set.ProjectID))
		if err != nil {
			return nil, err
		}
	}

	return resp.ProjectVariableSets, nil
}

func (s *service) GetProjectVariableSets(ctx context.Context, input *GetProjectVariableSetsInput) (*db.ProjectVariableSetsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariableSets")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(input.ProjectID)); err != nil {
		return nil, err
	}

	dbInput := &db.GetProjectVariableSetsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ProjectVariableSetFilter{
			ProjectID:                 &input.ProjectID,
			Latest:                    input.Latest,
			VariableID:                input.VariableID,
			VariableSetRevisionSearch: input.RevisionSearch,
		},
	}

	resp, err := s.dbClient.ProjectVariableSets.GetProjectVariableSets(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variable sets", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) SetProjectVariables(ctx context.Context, input *SetProjectVariablesInput) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "svc.SetProjectVariables")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.UpdateProject, auth.WithProjectID(input.ProjectID)); err != nil {
		return nil, err
	}

	if err = input.validate(); err != nil {
		return nil, err
	}

	// Find latest variable set
	resp, err := s.dbClient.ProjectVariableSets.GetProjectVariableSets(ctx, &db.GetProjectVariableSetsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
		Filter: &db.ProjectVariableSetFilter{
			ProjectID: &input.ProjectID,
			Latest:    ptr.Bool(true),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get latest variable set", errors.WithSpan(span))
	}

	var latestVariableSet *models.ProjectVariableSet
	var currentVariables map[string]*models.ProjectVariable

	latestRevision := 0
	if len(resp.ProjectVariableSets) > 0 {
		latestVariableSet = resp.ProjectVariableSets[0]

		latestRevision, err = strconv.Atoi(latestVariableSet.Revision)
		if err != nil {
			return nil, errors.Wrap(err, "failed to convert revision to int", errors.WithSpan(span))
		}

		// Get all variables in latest set
		currentVariablesResp, err := s.dbClient.ProjectVariables.GetProjectVariables(ctx, &db.GetProjectVariablesInput{
			Filter: &db.ProjectVariableFilter{
				ProjectVariableSetID: &latestVariableSet.Metadata.ID,
			},
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to get variables in latest set", errors.WithSpan(span))
		}

		// Build map of current variables
		currentVariables = make(map[string]*models.ProjectVariable, len(currentVariablesResp.ProjectVariables))
		for _, variable := range currentVariablesResp.ProjectVariables {
			variable := variable
			currentVariables[variable.FQN()] = variable
		}
	} else {
		currentVariables = map[string]*models.ProjectVariable{}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin a DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for CreateProjectVariableSet: %v", txErr)
		}
	}()

	// Create new variables
	variablesInNewSet := make([]*models.ProjectVariable, 0, len(input.Variables))
	for _, variable := range input.Variables {
		// Create new variable is it doesn't already exist in the set
		existingVar := currentVariables[variable.FQN()]
		if existingVar != nil && existingVar.Value == variable.Value {
			variablesInNewSet = append(variablesInNewSet, currentVariables[variable.FQN()])
		} else {
			v, err := s.dbClient.ProjectVariables.CreateProjectVariable(txContext, &models.ProjectVariable{
				ProjectID:        input.ProjectID,
				EnvironmentScope: variable.EnvironmentScope,
				Key:              variable.Key,
				Value:            variable.Value,
				PipelineType:     variable.PipelineType,
				CreatedBy:        caller.GetSubject(),
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to create variable", errors.WithSpan(span))
			}
			variablesInNewSet = append(variablesInNewSet, v)
		}
	}

	if latestVariableSet != nil {
		latestVariableSet.Latest = false
		// Update existing set to no longer be the latest
		if _, err = s.dbClient.ProjectVariableSets.UpdateProjectVariableSet(txContext, latestVariableSet); err != nil {
			return nil, errors.Wrap(err, "failed to update latest variable set", errors.WithSpan(span))
		}
	}

	// Create new variable set
	variableSet, err := s.dbClient.ProjectVariableSets.CreateProjectVariableSet(txContext, &models.ProjectVariableSet{
		ProjectID: input.ProjectID,
		Latest:    true,
		Revision:  strconv.Itoa(latestRevision + 1),
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create variable set", errors.WithSpan(span))
	}

	// Get variable set count so we can check the limit
	variableSetsResp, err := s.dbClient.ProjectVariableSets.GetProjectVariableSets(txContext, &db.GetProjectVariableSetsInput{
		Filter: &db.ProjectVariableSetFilter{ProjectID: &input.ProjectID},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project variable sets for limit check", errors.WithSpan(span))
	}

	// Verify we're not exceeding the limit
	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitVariableSetsPerProject,
		variableSetsResp.PageInfo.TotalCount); err != nil {
		return nil, err
	}

	if len(variablesInNewSet) > 0 {
		// Add variables to variable set
		if err = s.dbClient.ProjectVariableSets.AddProjectVariablesToSet(txContext, variableSet, variablesInNewSet); err != nil {
			return nil, errors.Wrap(err, "failed to add variable to variable set", errors.WithSpan(span))
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &input.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetProjectVariableSet,
			TargetID:   &variableSet.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit a DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Set project variables.",
		"caller", caller.GetSubject(),
		"project_id", input.ProjectID,
		"variable_revision", variableSet.Revision,
	)

	return variableSet, nil
}

func (s *service) PatchProjectVariables(ctx context.Context, input *PatchProjectVariablesInput) (*models.ProjectVariableSet, error) {
	ctx, span := tracer.Start(ctx, "svc.PatchProjectVariableSet")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.UpdateProject, auth.WithProjectID(input.ProjectID)); err != nil {
		return nil, err
	}

	if err = input.validate(); err != nil {
		return nil, err
	}

	// Find latest variable set
	resp, err := s.dbClient.ProjectVariableSets.GetProjectVariableSets(ctx, &db.GetProjectVariableSetsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
		Filter: &db.ProjectVariableSetFilter{
			ProjectID: &input.ProjectID,
			Latest:    ptr.Bool(true),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get latest variable set", errors.WithSpan(span))
	}

	var latestVariableSet *models.ProjectVariableSet
	var currentVariablesByFQN, currentVariablesByID map[string]*models.ProjectVariable
	latestRevision := 0
	if len(resp.ProjectVariableSets) > 0 {
		latestVariableSet = resp.ProjectVariableSets[0]

		latestRevision, err = strconv.Atoi(latestVariableSet.Revision)
		if err != nil {
			return nil, errors.Wrap(err, "failed to convert revision to int", errors.WithSpan(span))
		}

		// Get all variables in latest set
		currentVariablesResp, err := s.dbClient.ProjectVariables.GetProjectVariables(ctx, &db.GetProjectVariablesInput{
			Filter: &db.ProjectVariableFilter{
				ProjectVariableSetID: &latestVariableSet.Metadata.ID,
			},
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to get variables in latest set", errors.WithSpan(span))
		}

		// Build map of current variables
		currentVariablesByFQN = make(map[string]*models.ProjectVariable, len(currentVariablesResp.ProjectVariables))
		currentVariablesByID = make(map[string]*models.ProjectVariable, len(currentVariablesResp.ProjectVariables))
		for _, variable := range currentVariablesResp.ProjectVariables {
			variable := variable
			currentVariablesByFQN[variable.FQN()] = variable
			currentVariablesByID[variable.Metadata.ID] = variable
		}
	} else {
		currentVariablesByFQN = map[string]*models.ProjectVariable{}
		currentVariablesByID = map[string]*models.ProjectVariable{}
	}

	variablesToRemove := make(map[string]bool, len(input.RemoveVariableIDs)+len(input.ReplaceVariables))
	for _, id := range input.RemoveVariableIDs {
		if _, ok := currentVariablesByID[id]; !ok {
			return nil, errors.New(
				"cannot delete variable with id %q because it's not in set with revision %d",
				gid.ToGlobalID(gid.ProjectVariableType, id),
				latestRevision,
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EInvalid),
			)
		}
		variablesToRemove[id] = true
	}
	for _, variable := range input.ReplaceVariables {
		if _, ok := currentVariablesByID[variable.Metadata.ID]; !ok {
			return nil, errors.New(
				"cannot update variable with id %q because it's not in set with revision %d",
				gid.ToGlobalID(gid.ProjectVariableType, variable.Metadata.ID),
				latestRevision,
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EInvalid),
			)
		}
		variablesToRemove[variable.Metadata.ID] = true
	}

	variablesInNewSet := make([]*models.ProjectVariable, 0, len(input.CreateVariables))

	// Copy over existing variables that aren't being removed
	for _, variable := range currentVariablesByFQN {
		if _, ok := variablesToRemove[variable.Metadata.ID]; !ok {
			variablesInNewSet = append(variablesInNewSet, variable)
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin a DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for CreateProjectVariableSet: %v", txErr)
		}
	}()

	// Create new variables
	for _, variable := range input.CreateVariables {
		// Create new variable is it doesn't already exist in the set
		if _, ok := currentVariablesByFQN[variable.FQN()]; ok {
			return nil, errors.New(
				"%s variable with key %s and environment scope %s already exists in set",
				variable.PipelineType,
				variable.Key,
				variable.EnvironmentScope,
				errors.WithErrorCode(errors.EInvalid),
				errors.WithSpan(span),
			)
		}

		v, err := s.dbClient.ProjectVariables.CreateProjectVariable(txContext, &models.ProjectVariable{
			ProjectID:        input.ProjectID,
			EnvironmentScope: variable.EnvironmentScope,
			Key:              variable.Key,
			Value:            variable.Value,
			PipelineType:     variable.PipelineType,
			CreatedBy:        caller.GetSubject(),
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to create variable", errors.WithSpan(span))
		}
		variablesInNewSet = append(variablesInNewSet, v)
	}

	// Create new variables for updated variables
	for _, variable := range input.ReplaceVariables {
		// Create new variable is it doesn't already exist in the set
		existingVar := currentVariablesByFQN[variable.FQN()]
		if existingVar != nil && existingVar.Value == variable.Value {
			variablesInNewSet = append(variablesInNewSet, currentVariablesByFQN[variable.FQN()])
		} else {
			v, err := s.dbClient.ProjectVariables.CreateProjectVariable(txContext, &models.ProjectVariable{
				ProjectID:        input.ProjectID,
				EnvironmentScope: variable.EnvironmentScope,
				Key:              variable.Key,
				Value:            variable.Value,
				PipelineType:     variable.PipelineType,
				CreatedBy:        caller.GetSubject(),
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to create variable", errors.WithSpan(span))
			}
			variablesInNewSet = append(variablesInNewSet, v)
		}
	}

	if len(variablesInNewSet) > maxProjectVariablesPerProjectVariableSet {
		return nil, errors.New(
			"a project variable set cannot have more than %d variables",
			maxProjectVariablesPerProjectVariableSet,
			errors.WithErrorCode(errors.EInvalid),
		)
	}

	variableMapByKey := map[string]bool{}

	// Validate variables in new set
	for _, variable := range variablesInNewSet {
		// Check if variable with key already exists in the set
		key := variable.FQN()
		if _, ok := variableMapByKey[key]; ok {
			return nil, errors.New(
				"variable with key %s already exists in the set", key,
				errors.WithErrorCode(errors.EInvalid))
		}

		variableMapByKey[key] = true
	}

	// Check if variable scope is conflicting with existing variable
	for _, variable := range variablesInNewSet {
		if variable.PipelineType == models.DeploymentPipelineType {
			if variable.EnvironmentScope != models.ProjectVariableEnvironmentScopeAll {
				// Verify that there is not a variable with the same key set for all environments
				if _, ok := variableMapByKey[fmt.Sprintf("%s:%s:%s", string(variable.PipelineType), "*", variable.Key)]; ok {
					return nil, errors.New(
						"cannot set deployment variable with name %s because a variable with the same name is set for all environments",
						variable.Key,
						errors.WithErrorCode(errors.EInvalid),
					)
				}
			}
		}
	}

	if latestVariableSet != nil {
		// Update existing latest variable set to no longer be the latest
		latestVariableSet.Latest = false
		if _, err = s.dbClient.ProjectVariableSets.UpdateProjectVariableSet(txContext, latestVariableSet); err != nil {
			return nil, errors.Wrap(err, "failed to update latest variable set", errors.WithSpan(span))
		}
	}

	// Create new variable set
	variableSet, err := s.dbClient.ProjectVariableSets.CreateProjectVariableSet(txContext, &models.ProjectVariableSet{
		ProjectID: input.ProjectID,
		Latest:    true,
		Revision:  strconv.Itoa(latestRevision + 1),
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create variable set", errors.WithSpan(span))
	}

	// Get variable set count so we can check the limit
	variableSetsResp, err := s.dbClient.ProjectVariableSets.GetProjectVariableSets(txContext, &db.GetProjectVariableSetsInput{
		Filter: &db.ProjectVariableSetFilter{ProjectID: &input.ProjectID},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project variable sets for limit check", errors.WithSpan(span))
	}

	// Verify we're not exceeding the limit
	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitVariableSetsPerProject,
		variableSetsResp.PageInfo.TotalCount); err != nil {
		return nil, err
	}

	if len(variablesInNewSet) > 0 {
		// Add variables to variable set
		if err = s.dbClient.ProjectVariableSets.AddProjectVariablesToSet(txContext, variableSet, variablesInNewSet); err != nil {
			return nil, errors.Wrap(err, "failed to add variable to variable set", errors.WithSpan(span))
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &input.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetProjectVariableSet,
			TargetID:   &variableSet.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit a DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Modify project variables.",
		"caller", caller.GetSubject(),
		"project_id", input.ProjectID,
		"variable_revision", variableSet.Revision,
	)

	return variableSet, nil
}

func (s *service) GetProjectVariableByID(ctx context.Context, id string) (*models.ProjectVariable, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariableByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	variable, err := s.getProjectVariableByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(variable.ProjectID))
	if err != nil {
		return nil, err
	}

	return variable, nil
}

func (s *service) GetProjectVariableByPRN(ctx context.Context, prn string) (*models.ProjectVariable, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariableByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	variable, err := s.dbClient.ProjectVariables.GetProjectVariableByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variable by PRN", errors.WithSpan(span))
	}

	if variable == nil {
		return nil, errors.New(
			"variable with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(variable.ProjectID))
	if err != nil {
		return nil, err
	}

	return variable, nil
}

func (s *service) GetProjectVariablesByIDs(ctx context.Context, idList []string) ([]*models.ProjectVariable, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariableByIDs")
	span.SetAttributes(attribute.StringSlice("ids", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetProjectVariablesInput{
		Filter: &db.ProjectVariableFilter{ProjectVariableIDs: idList},
	}

	resp, err := s.dbClient.ProjectVariables.GetProjectVariables(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variables", errors.WithSpan(span))
	}

	projectIDs := map[string]struct{}{}
	for _, v := range resp.ProjectVariables {
		if _, ok := projectIDs[v.ProjectID]; ok {
			// Skip since this project has already been checked
			continue
		}

		projectIDs[v.ProjectID] = struct{}{}
		err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(v.ProjectID))
		if err != nil {
			return nil, err
		}
	}

	return resp.ProjectVariables, nil
}

func (s *service) GetProjectVariables(ctx context.Context, input *GetProjectVariablesInput) (*db.ProjectVariablesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectVariables")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(input.ProjectID)); err != nil {
		return nil, err
	}

	dbInput := &db.GetProjectVariablesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ProjectVariableFilter{
			ProjectID:                 &input.ProjectID,
			ProjectVariableSetID:      input.ProjectVariableSetID,
			VariableKey:               input.VariableKey,
			EnvironmentScopes:         input.EnvironmentScopes,
			PipelineType:              input.PipelineType,
			CreatedAtTimeRangeEnd:     input.CreatedAtTimeRangeEnd,
			ExcludeProjectVariableIDs: input.ExcludeVariableIDs,
			Search:                    input.Search,
		},
	}

	resp, err := s.dbClient.ProjectVariables.GetProjectVariables(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variables", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) getProjectVariableSetByID(ctx context.Context, span trace.Span, id string) (*models.ProjectVariableSet, error) {
	variableSet, err := s.dbClient.ProjectVariableSets.GetProjectVariableSetByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variable set by ID", errors.WithSpan(span))
	}

	if variableSet == nil {
		return nil, errors.New(
			"variable set with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return variableSet, nil
}

func (s *service) getProjectVariableByID(ctx context.Context, span trace.Span, id string) (*models.ProjectVariable, error) {
	variable, err := s.dbClient.ProjectVariables.GetProjectVariableByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get variable by ID", errors.WithSpan(span))
	}

	if variable == nil {
		return nil, errors.New(
			"variable with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return variable, nil
}
