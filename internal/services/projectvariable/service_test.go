package projectvariable

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	activityService := activityevent.NewMockService(t)
	limitChecker := limits.NewMockLimitChecker(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		activityService: activityService,
		limitChecker:    limitChecker,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, activityService, limitChecker))
}

func TestGetProjectVariableSetByID(t *testing.T) {
	projectID := "project-1"
	variableSetID := "var-set-1"

	type testCase struct {
		injectPermissionError error
		projectVariableSet    *models.ProjectVariableSet
		name                  string
		expectErrorCode       errors.CodeType
	}

	testCases := []testCase{
		{
			name:            "project variable set not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "caller not authorized",
			projectVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: variableSetID,
				},
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name: "successfully return a project variable",
			projectVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: variableSetID,
				},
				ProjectID: projectID,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjectVariableSets := db.NewMockProjectVariableSets(t)
			mockCaller := auth.NewMockCaller(t)

			ctx = auth.WithCaller(ctx, mockCaller)

			mockProjectVariableSets.On("GetProjectVariableSetByID", mock.Anything, variableSetID).
				Return(test.projectVariableSet, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			dbClient := &db.Client{
				ProjectVariableSets: mockProjectVariableSets,
			}

			service := &service{
				dbClient: dbClient,
			}

			variable, err := service.GetProjectVariableSetByID(ctx, variableSetID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.projectVariableSet, variable)
		})
	}
}

func TestGetProjectVariableSetByPRN(t *testing.T) {
	projectID := "project-1"
	variableSetID := "var-set-1"
	variableSetPRN := models.ProjectVariableSetResource.BuildPRN("org-1", "project-1", "1")

	type testCase struct {
		injectPermissionError error
		projectVariableSet    *models.ProjectVariableSet
		name                  string
		expectErrorCode       errors.CodeType
	}

	testCases := []testCase{
		{
			name:            "project variable set not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "caller not authorized",
			projectVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: variableSetID,
				},
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name: "successfully return project variable set",
			projectVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: variableSetID,
				},
				ProjectID: projectID,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjectVariableSets := db.NewMockProjectVariableSets(t)
			mockCaller := auth.NewMockCaller(t)

			ctx = auth.WithCaller(ctx, mockCaller)

			mockProjectVariableSets.On("GetProjectVariableSetByPRN", mock.Anything, variableSetPRN).
				Return(test.projectVariableSet, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			dbClient := &db.Client{
				ProjectVariableSets: mockProjectVariableSets,
			}

			service := &service{
				dbClient: dbClient,
			}

			variableSet, err := service.GetProjectVariableSetByPRN(ctx, variableSetPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.projectVariableSet, variableSet)
		})
	}
}

func TestGetProjectVariableSetsByIDs(t *testing.T) {
	type testCase struct {
		injectPermissionError error
		name                  string
		expectErrorCode       errors.CodeType
		expectVariableSets    []*models.ProjectVariableSet
	}

	testCases := []testCase{
		{
			name: "successfully return variable sets",
			expectVariableSets: []*models.ProjectVariableSet{
				{Metadata: models.ResourceMetadata{ID: "var-1"}, ProjectID: "project-1"},
				{Metadata: models.ResourceMetadata{ID: "var-1"}, ProjectID: "project-2"},
			},
		},
		{
			name:                  "caller does not have permission to view project variable",
			injectPermissionError: errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
			expectVariableSets: []*models.ProjectVariableSet{
				{Metadata: models.ResourceMetadata{ID: "var-1"}, ProjectID: "project-1"},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjectVariableSets := db.NewMockProjectVariableSets(t)

			idList := []string{}
			for _, variable := range test.expectVariableSets {
				idList = append(idList, variable.Metadata.ID)
			}

			dbInput := &db.GetProjectVariableSetsInput{
				Filter: &db.ProjectVariableSetFilter{
					VariableSetIDs: idList,
				},
			}
			dbResult := &db.ProjectVariableSetsResult{
				ProjectVariableSets: test.expectVariableSets,
			}

			mockProjectVariableSets.On("GetProjectVariableSets", mock.Anything, dbInput).Return(dbResult, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			dbClient := &db.Client{
				ProjectVariableSets: mockProjectVariableSets,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			variableSets, err := service.GetProjectVariableSetsByIDs(auth.WithCaller(ctx, mockCaller), idList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.ElementsMatch(t, test.expectVariableSets, variableSets)
		})
	}
}

func TestGetProjectVariableSets(t *testing.T) {
	projectID := "project-1"

	type testCase struct {
		input                 *GetProjectVariableSetsInput
		expectResult          *db.ProjectVariableSetsResult
		name                  string
		injectPermissionError error
		expectErrorCode       errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return project variable sets",
			input: &GetProjectVariableSetsInput{
				ProjectID: projectID,
			},
			expectResult: &db.ProjectVariableSetsResult{
				ProjectVariableSets: []*models.ProjectVariableSet{
					{
						Metadata: models.ResourceMetadata{ID: "var-1"},
					},
				},
			},
		},
		{
			name: "caller not authorized",
			input: &GetProjectVariableSetsInput{
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
	}

	for _, test := range testCases {
		test := test
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			mockProjectVariableSets := db.NewMockProjectVariableSets(t)

			if test.expectResult != nil {
				dbInput := &db.GetProjectVariableSetsInput{
					Sort:              test.input.Sort,
					PaginationOptions: test.input.PaginationOptions,
					Filter: &db.ProjectVariableSetFilter{
						ProjectID: &test.input.ProjectID,
					},
				}
				mockProjectVariableSets.On("GetProjectVariableSets", mock.Anything, dbInput).Return(test.expectResult, nil)
			}

			dbClient := &db.Client{
				ProjectVariableSets: mockProjectVariableSets,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetProjectVariableSets(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestGetProjectVariableByID(t *testing.T) {
	projectID := "project-1"
	variableID := "var-1"

	type testCase struct {
		injectPermissionError error
		projectVariable       *models.ProjectVariable
		name                  string
		expectErrorCode       errors.CodeType
	}

	testCases := []testCase{
		{
			name:            "project variable not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "caller not authorized",
			projectVariable: &models.ProjectVariable{
				Metadata: models.ResourceMetadata{
					ID: variableID,
				},
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name: "successfully return a project variable",
			projectVariable: &models.ProjectVariable{
				Metadata: models.ResourceMetadata{
					ID: variableID,
				},
				ProjectID: projectID,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjectVariables := db.NewMockProjectVariables(t)
			mockCaller := auth.NewMockCaller(t)

			ctx = auth.WithCaller(ctx, mockCaller)

			mockProjectVariables.On("GetProjectVariableByID", mock.Anything, variableID).
				Return(test.projectVariable, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			dbClient := &db.Client{
				ProjectVariables: mockProjectVariables,
			}

			service := &service{
				dbClient: dbClient,
			}

			variable, err := service.GetProjectVariableByID(ctx, variableID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.projectVariable, variable)
		})
	}
}

func TestGetProjectVariableByPRN(t *testing.T) {
	projectID := "project-1"
	variableID := "var-1"
	variablePRN := models.ProjectVariableResource.BuildPRN("org-1", "project-1", variableID)

	type testCase struct {
		injectPermissionError error
		projectVariable       *models.ProjectVariable
		name                  string
		expectErrorCode       errors.CodeType
	}

	testCases := []testCase{
		{
			name:            "project variable not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "caller not authorized",
			projectVariable: &models.ProjectVariable{
				Metadata: models.ResourceMetadata{
					ID: variableID,
				},
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name: "successfully return project variable",
			projectVariable: &models.ProjectVariable{
				Metadata: models.ResourceMetadata{
					ID: variableID,
				},
				ProjectID: projectID,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjectVariables := db.NewMockProjectVariables(t)
			mockCaller := auth.NewMockCaller(t)

			ctx = auth.WithCaller(ctx, mockCaller)

			mockProjectVariables.On("GetProjectVariableByPRN", mock.Anything, variablePRN).
				Return(test.projectVariable, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			dbClient := &db.Client{
				ProjectVariables: mockProjectVariables,
			}

			service := &service{
				dbClient: dbClient,
			}

			variable, err := service.GetProjectVariableByPRN(ctx, variablePRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.projectVariable, variable)
		})
	}
}

func TestGetProjectVariablesByIDs(t *testing.T) {
	type testCase struct {
		injectPermissionError error
		name                  string
		expectErrorCode       errors.CodeType
		expectVariables       []*models.ProjectVariable
	}

	testCases := []testCase{
		{
			name: "successfully return variables",
			expectVariables: []*models.ProjectVariable{
				{Metadata: models.ResourceMetadata{ID: "var-1"}, ProjectID: "project-1"},
				{Metadata: models.ResourceMetadata{ID: "var-1"}, ProjectID: "project-2"},
			},
		},
		{
			name:                  "caller does not have permission to view project variable",
			injectPermissionError: errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
			expectVariables: []*models.ProjectVariable{
				{Metadata: models.ResourceMetadata{ID: "var-1"}, ProjectID: "project-1"},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjectVariables := db.NewMockProjectVariables(t)

			idList := []string{}
			for _, variable := range test.expectVariables {
				idList = append(idList, variable.Metadata.ID)
			}

			dbInput := &db.GetProjectVariablesInput{
				Filter: &db.ProjectVariableFilter{
					ProjectVariableIDs: idList,
				},
			}
			dbResult := &db.ProjectVariablesResult{
				ProjectVariables: test.expectVariables,
			}

			mockProjectVariables.On("GetProjectVariables", mock.Anything, dbInput).Return(dbResult, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			dbClient := &db.Client{
				ProjectVariables: mockProjectVariables,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualProjectVariables, err := service.GetProjectVariablesByIDs(auth.WithCaller(ctx, mockCaller), idList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.ElementsMatch(t, test.expectVariables, actualProjectVariables)
		})
	}
}

func TestGetProjectVariables(t *testing.T) {
	projectID := "project-1"

	type testCase struct {
		input                 *GetProjectVariablesInput
		expectResult          *db.ProjectVariablesResult
		name                  string
		injectPermissionError error
		expectErrorCode       errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return project variables",
			input: &GetProjectVariablesInput{
				ProjectID: projectID,
			},
			expectResult: &db.ProjectVariablesResult{
				ProjectVariables: []*models.ProjectVariable{
					{
						Metadata: models.ResourceMetadata{ID: "var-1"},
					},
				},
			},
		},
		{
			name: "caller not authorized",
			input: &GetProjectVariablesInput{
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
	}

	for _, test := range testCases {
		test := test
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			mockProjectVariables := db.NewMockProjectVariables(t)

			if test.expectResult != nil {
				dbInput := &db.GetProjectVariablesInput{
					Sort:              test.input.Sort,
					PaginationOptions: test.input.PaginationOptions,
					Filter: &db.ProjectVariableFilter{
						ProjectID: &test.input.ProjectID,
					},
				}
				mockProjectVariables.On("GetProjectVariables", mock.Anything, dbInput).Return(test.expectResult, nil)
			}

			dbClient := &db.Client{
				ProjectVariables: mockProjectVariables,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetProjectVariables(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestSetProjectVariables(t *testing.T) {
	projectID := "project-1"
	mockSubject := "mock-caller-subject"

	type testCase struct {
		injectPermissionError    error
		input                    *SetProjectVariablesInput
		currentVariables         []*models.ProjectVariable
		currentLatestVariableSet *models.ProjectVariableSet
		expectCreatedVariableSet *models.ProjectVariableSet
		expectCreatedVariables   []*models.ProjectVariable
		name                     string
		expectErrorCode          errors.CodeType
		limitExceeded            bool
	}

	testCases := []testCase{
		{
			name: "caller not authorized",
			input: &SetProjectVariablesInput{
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name: "duplicate variable key",
			input: &SetProjectVariablesInput{
				ProjectID: projectID,
				Variables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "environment scope not defined for deployment variable",
			input: &SetProjectVariablesInput{
				ProjectID: projectID,
				Variables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: ""},
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "conflicting variable scope",
			input: &SetProjectVariablesInput{
				ProjectID: projectID,
				Variables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: models.ProjectVariableEnvironmentScopeAll},
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
				},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "limit exceeded",
			input: &SetProjectVariablesInput{
				ProjectID: projectID,
				Variables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1"},
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test2"},
				},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1", CreatedBy: mockSubject, ProjectID: projectID},
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test2", CreatedBy: mockSubject, ProjectID: projectID},
			},
			expectCreatedVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-1",
				},
				ProjectID: projectID,
				Revision:  "1",
				Latest:    true,
			},
			limitExceeded:   true,
			expectErrorCode: errors.EConflict,
		},
		{
			name: "successfully set project variables when no latest variable set exists",
			input: &SetProjectVariablesInput{
				ProjectID: projectID,
				Variables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1"},
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test2"},
				},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1", CreatedBy: mockSubject, ProjectID: projectID},
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test2", CreatedBy: mockSubject, ProjectID: projectID},
			},
			expectCreatedVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-1",
				},
				ProjectID: projectID,
				Revision:  "1",
				Latest:    true,
			},
		},
		{
			name: "successfully set project variables when a latest variable set exists",
			input: &SetProjectVariablesInput{
				ProjectID: projectID,
				Variables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
					{PipelineType: models.DeploymentPipelineType, Key: "key-2", Value: "value-1", EnvironmentScope: "test"},
				},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
			},
			currentVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-2", Value: "value-1", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
			},
			currentLatestVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-1",
				},
				ProjectID: projectID,
				Revision:  "1",
				Latest:    true,
			},
			expectCreatedVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-2",
				},
				ProjectID: projectID,
				Revision:  "2",
				Latest:    true,
			},
		},
	}

	for _, test := range testCases {
		test := test
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjectVariables := db.NewMockProjectVariables(t)
			mockProjectVariableSets := db.NewMockProjectVariableSets(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			mockCaller.On("GetSubject").Return(mockSubject).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			// Mock out call to get latest project variable set
			mockProjectVariableSets.On("GetProjectVariableSets", mock.Anything, &db.GetProjectVariableSetsInput{
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Filter: &db.ProjectVariableSetFilter{
					ProjectID: &test.input.ProjectID,
					Latest:    ptr.Bool(true),
				},
			}).
				Return(func(_ context.Context, _ *db.GetProjectVariableSetsInput) *db.ProjectVariableSetsResult {
					if test.currentLatestVariableSet == nil {
						return &db.ProjectVariableSetsResult{}
					}
					return &db.ProjectVariableSetsResult{
						ProjectVariableSets: []*models.ProjectVariableSet{test.currentLatestVariableSet},
					}
				}, nil).Maybe()

			if test.currentLatestVariableSet != nil {
				mockProjectVariables.On("GetProjectVariables", mock.Anything, &db.GetProjectVariablesInput{
					Filter: &db.ProjectVariableFilter{
						ProjectVariableSetID: &test.currentLatestVariableSet.Metadata.ID,
					},
				}).Return(&db.ProjectVariablesResult{
					ProjectVariables: test.currentVariables,
				}, nil)
			}

			for _, variable := range test.expectCreatedVariables {
				mockProjectVariables.On("CreateProjectVariable", mock.Anything, variable).
					Return(variable, nil)
			}

			if test.currentLatestVariableSet != nil {
				mockProjectVariableSets.On("UpdateProjectVariableSet", mock.Anything, &models.ProjectVariableSet{
					Metadata:  test.currentLatestVariableSet.Metadata,
					ProjectID: projectID,
					Latest:    false,
					Revision:  test.currentLatestVariableSet.Revision,
				}).Return(nil, nil)
			}

			if test.expectCreatedVariableSet != nil {
				mockProjectVariableSets.On("CreateProjectVariableSet", mock.Anything, &models.ProjectVariableSet{
					ProjectID: projectID,
					Latest:    true,
					Revision:  test.expectCreatedVariableSet.Revision,
				}).Return(test.expectCreatedVariableSet, nil)

				matcher := mock.MatchedBy(func(variables []*models.ProjectVariable) bool {
					// Build map of expected variables
					expectedVariables := map[string]*models.ProjectVariable{}
					for _, variable := range test.input.Variables {
						expectedVariables[variable.FQN()] = variable
					}
					for _, variable := range variables {
						if expectedVariables[variable.FQN()] == nil || expectedVariables[variable.FQN()].Value != variable.Value {
							return false
						}
					}
					return true
				})

				totalCountForLimitCheck := 10

				// Mock out call to get total count of project variable sets for limit check
				mockProjectVariableSets.On("GetProjectVariableSets", mock.Anything, &db.GetProjectVariableSetsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
					Filter: &db.ProjectVariableSetFilter{
						ProjectID: &test.input.ProjectID,
					},
				}).
					Return(&db.ProjectVariableSetsResult{PageInfo: &pagination.PageInfo{TotalCount: int32(totalCountForLimitCheck)}}, nil).Maybe()

				// Mock out limit check
				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitVariableSetsPerProject, int32(totalCountForLimitCheck)).
					Return(func(_ context.Context, _ limits.ResourceLimitName, _ int32) error {
						if test.limitExceeded {
							return errors.New("limit exceeded", errors.WithErrorCode(errors.EConflict))
						}
						return nil
					}).Maybe()

				if !test.limitExceeded {
					mockProjectVariableSets.On("AddProjectVariablesToSet", mock.Anything, test.expectCreatedVariableSet, matcher).Return(nil)

					mockActivityEvents.On("CreateActivityEvent", mock.Anything,
						&activityevent.CreateActivityEventInput{
							ProjectID:  &projectID,
							Action:     models.ActionCreate,
							TargetType: models.TargetProjectVariableSet,
							TargetID:   &test.expectCreatedVariableSet.Metadata.ID,
						},
					).Return(&models.ActivityEvent{}, nil)

					mockTransactions.On("CommitTx", mock.Anything).Return(nil)
				}
			}

			dbClient := &db.Client{
				ProjectVariables:    mockProjectVariables,
				ProjectVariableSets: mockProjectVariableSets,
				Transactions:        mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
				limitChecker:    mockLimitChecker,
			}

			variableSet, err := service.SetProjectVariables(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreatedVariableSet, variableSet)
		})
	}
}

func TestPatchProjectVariables(t *testing.T) {
	projectID := "project-1"
	mockSubject := "mock-caller-subject"

	type testCase struct {
		injectPermissionError    error
		input                    *PatchProjectVariablesInput
		currentVariables         []*models.ProjectVariable
		currentLatestVariableSet *models.ProjectVariableSet
		expectCreatedVariableSet *models.ProjectVariableSet
		expectCreatedVariables   []*models.ProjectVariable
		expectVariablesInNewSet  []*models.ProjectVariable
		name                     string
		expectErrorCode          errors.CodeType
		limitExceeded            bool
	}

	testCases := []testCase{
		{
			name: "caller not authorized",
			input: &PatchProjectVariablesInput{
				ProjectID: projectID,
			},
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name: "duplicate variable key",
			input: &PatchProjectVariablesInput{
				ProjectID: projectID,
				CreateVariables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
				},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "conflicting variable scope",
			input: &PatchProjectVariablesInput{
				ProjectID: projectID,
				CreateVariables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: models.ProjectVariableEnvironmentScopeAll},
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
				},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: models.ProjectVariableEnvironmentScopeAll, CreatedBy: mockSubject, ProjectID: projectID},
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "limit exceeded error",
			input: &PatchProjectVariablesInput{
				ProjectID: projectID,
				CreateVariables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1"},
				},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1", CreatedBy: mockSubject, ProjectID: projectID},
			},
			expectCreatedVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-1",
				},
				ProjectID: projectID,
				Revision:  "1",
				Latest:    true,
			},
			limitExceeded:   true,
			expectErrorCode: errors.EConflict,
		},
		{
			name: "successfully patch project variables when no latest variable set exists",
			input: &PatchProjectVariablesInput{
				ProjectID: projectID,
				CreateVariables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1"},
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test2"},
				},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1", CreatedBy: mockSubject, ProjectID: projectID},
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test2", CreatedBy: mockSubject, ProjectID: projectID},
			},
			expectVariablesInNewSet: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test1"},
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test2"},
			},
			expectCreatedVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-1",
				},
				ProjectID: projectID,
				Revision:  "1",
				Latest:    true,
			},
		},
		{
			name: "successfully patch project variables when a latest variable set exists",
			input: &PatchProjectVariablesInput{
				ProjectID: projectID,
				CreateVariables: []*models.ProjectVariable{
					{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
				},
				ReplaceVariables: []*models.ProjectVariable{
					{Metadata: models.ResourceMetadata{ID: "var-1"}, PipelineType: models.DeploymentPipelineType, Key: "key-2", Value: "value-1-updated", EnvironmentScope: "test"},
				},
				RemoveVariableIDs: []string{"var-3"},
			},
			expectCreatedVariables: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
				{PipelineType: models.DeploymentPipelineType, Key: "key-2", Value: "value-1-updated", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
			},
			currentVariables: []*models.ProjectVariable{
				{Metadata: models.ResourceMetadata{ID: "var-1"}, PipelineType: models.DeploymentPipelineType, Key: "key-2", Value: "value-1", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
				{Metadata: models.ResourceMetadata{ID: "var-3"}, PipelineType: models.DeploymentPipelineType, Key: "key-3", Value: "value-3", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
				{Metadata: models.ResourceMetadata{ID: "var-4"}, PipelineType: models.DeploymentPipelineType, Key: "key-4", Value: "value-4", EnvironmentScope: "test", CreatedBy: mockSubject, ProjectID: projectID},
			},
			expectVariablesInNewSet: []*models.ProjectVariable{
				{PipelineType: models.DeploymentPipelineType, Key: "key-1", Value: "value-1", EnvironmentScope: "test"},
				{PipelineType: models.DeploymentPipelineType, Key: "key-2", Value: "value-1-updated", EnvironmentScope: "test"},
				{PipelineType: models.DeploymentPipelineType, Key: "key-4", Value: "value-4", EnvironmentScope: "test"},
			},
			currentLatestVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-1",
				},
				ProjectID: projectID,
				Revision:  "1",
				Latest:    true,
			},
			expectCreatedVariableSet: &models.ProjectVariableSet{
				Metadata: models.ResourceMetadata{
					ID: "var-set-2",
				},
				ProjectID: projectID,
				Revision:  "2",
				Latest:    true,
			},
		},
	}

	for _, test := range testCases {
		test := test
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjectVariables := db.NewMockProjectVariables(t)
			mockProjectVariableSets := db.NewMockProjectVariableSets(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateProject, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			mockCaller.On("GetSubject").Return(mockSubject).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			// Mock out call to get latest project variable set
			mockProjectVariableSets.On("GetProjectVariableSets", mock.Anything, &db.GetProjectVariableSetsInput{
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Filter: &db.ProjectVariableSetFilter{
					ProjectID: &test.input.ProjectID,
					Latest:    ptr.Bool(true),
				},
			}).
				Return(func(_ context.Context, _ *db.GetProjectVariableSetsInput) *db.ProjectVariableSetsResult {
					if test.currentLatestVariableSet == nil {
						return &db.ProjectVariableSetsResult{}
					}
					return &db.ProjectVariableSetsResult{
						ProjectVariableSets: []*models.ProjectVariableSet{test.currentLatestVariableSet},
					}
				}, nil).Maybe()

			if test.currentLatestVariableSet != nil {
				mockProjectVariables.On("GetProjectVariables", mock.Anything, &db.GetProjectVariablesInput{
					Filter: &db.ProjectVariableFilter{
						ProjectVariableSetID: &test.currentLatestVariableSet.Metadata.ID,
					},
				}).Return(&db.ProjectVariablesResult{
					ProjectVariables: test.currentVariables,
				}, nil)
			}

			for _, variable := range test.expectCreatedVariables {
				mockProjectVariables.On("CreateProjectVariable", mock.Anything, variable).
					Return(variable, nil)
			}

			if test.currentLatestVariableSet != nil {
				mockProjectVariableSets.On("UpdateProjectVariableSet", mock.Anything, &models.ProjectVariableSet{
					Metadata:  test.currentLatestVariableSet.Metadata,
					ProjectID: projectID,
					Latest:    false,
					Revision:  test.currentLatestVariableSet.Revision,
				}).Return(nil, nil)
			}

			if test.expectCreatedVariableSet != nil {
				mockProjectVariableSets.On("CreateProjectVariableSet", mock.Anything, &models.ProjectVariableSet{
					ProjectID: projectID,
					Latest:    true,
					Revision:  test.expectCreatedVariableSet.Revision,
				}).Return(test.expectCreatedVariableSet, nil)

				matcher := mock.MatchedBy(func(variables []*models.ProjectVariable) bool {
					// Build map of expected variables
					expectedVariables := map[string]*models.ProjectVariable{}
					for _, variable := range test.expectVariablesInNewSet {
						expectedVariables[variable.FQN()] = variable
					}
					for _, variable := range variables {
						if expectedVariables[variable.FQN()] == nil || expectedVariables[variable.FQN()].Value != variable.Value {
							return false
						}
					}
					return true
				})

				totalCountForLimitCheck := 10

				// Mock out call to get total count of project variable sets for limit check
				mockProjectVariableSets.On("GetProjectVariableSets", mock.Anything, &db.GetProjectVariableSetsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
					Filter: &db.ProjectVariableSetFilter{
						ProjectID: &test.input.ProjectID,
					},
				}).
					Return(&db.ProjectVariableSetsResult{PageInfo: &pagination.PageInfo{TotalCount: int32(totalCountForLimitCheck)}}, nil).Maybe()

				// Mock out limit check
				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitVariableSetsPerProject, int32(totalCountForLimitCheck)).
					Return(func(_ context.Context, _ limits.ResourceLimitName, _ int32) error {
						if test.limitExceeded {
							return errors.New("limit exceeded", errors.WithErrorCode(errors.EConflict))
						}
						return nil
					}).Maybe()

				if !test.limitExceeded {
					mockProjectVariableSets.On("AddProjectVariablesToSet", mock.Anything, test.expectCreatedVariableSet, matcher).Return(nil)

					mockActivityEvents.On("CreateActivityEvent", mock.Anything,
						&activityevent.CreateActivityEventInput{
							ProjectID:  &projectID,
							Action:     models.ActionCreate,
							TargetType: models.TargetProjectVariableSet,
							TargetID:   &test.expectCreatedVariableSet.Metadata.ID,
						},
					).Return(&models.ActivityEvent{}, nil)

					mockTransactions.On("CommitTx", mock.Anything).Return(nil)
				}
			}

			dbClient := &db.Client{
				ProjectVariables:    mockProjectVariables,
				ProjectVariableSets: mockProjectVariableSets,
				Transactions:        mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
				limitChecker:    mockLimitChecker,
			}

			variableSet, err := service.PatchProjectVariables(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreatedVariableSet, variableSet)
		})
	}
}
