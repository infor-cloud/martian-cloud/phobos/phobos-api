package pipelinetemplate

import (
	"bytes"
	"context"
	"io"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	limitChecker := limits.NewMockLimitChecker(t)
	dataStore := NewMockDataStore(t)
	activityEvents := activityevent.NewMockService(t)

	service := &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		dataStore:       dataStore,
		activityService: activityEvents,
	}

	assert.Equal(t, service, NewService(logger, dbClient, limitChecker, dataStore, activityEvents))
}

func TestGetPipelineTemplateByID(t *testing.T) {
	pipelineTemplateID := "pipeline-template-1"

	goodPipelineTemplate := &models.PipelineTemplate{
		Metadata: models.ResourceMetadata{
			ID: pipelineTemplateID,
		},
	}

	type testCase struct {
		injectPermissionError  error
		findTemplate           *models.PipelineTemplate
		expectPipelineTemplate *models.PipelineTemplate
		name                   string
		expectErrorCode        errors.CodeType
		withCaller             bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "template not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			findTemplate:          goodPipelineTemplate,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:                   "successfully return a pipeline template",
			withCaller:             true,
			findTemplate:           goodPipelineTemplate,
			expectPipelineTemplate: goodPipelineTemplate,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, pipelineTemplateID).
					Return(test.findTemplate, nil)

				if test.findTemplate != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				Projects:          mockProjects,
				PipelineTemplates: mockPipelineTemplates,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPipelineTemplate, err := service.GetPipelineTemplateByID(ctx, pipelineTemplateID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectPipelineTemplate, actualPipelineTemplate)
		})
	}
}

func TestGetPipelineTemplateByPRN(t *testing.T) {
	pipelineTemplatePRN := "prn:pipeline_template:some-org/some-project/some-pipeline-template"

	goodPipelineTemplate := &models.PipelineTemplate{
		Metadata: models.ResourceMetadata{
			PRN: pipelineTemplatePRN,
		},
	}

	type testCase struct {
		injectPermissionError  error
		findTemplate           *models.PipelineTemplate
		expectPipelineTemplate *models.PipelineTemplate
		name                   string
		expectErrorCode        errors.CodeType
		withCaller             bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "template not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			findTemplate:          goodPipelineTemplate,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:                   "successfully return a pipeline template",
			withCaller:             true,
			findTemplate:           goodPipelineTemplate,
			expectPipelineTemplate: goodPipelineTemplate,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockPipelineTemplates.On("GetPipelineTemplateByPRN", mock.Anything, pipelineTemplatePRN).
					Return(test.findTemplate, nil)

				if test.findTemplate != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				PipelineTemplates: mockPipelineTemplates,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPipelineTemplate, err := service.GetPipelineTemplateByPRN(ctx, pipelineTemplatePRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectPipelineTemplate, actualPipelineTemplate)
		})
	}
}

func TestGetPipelineTemplatesByIDs(t *testing.T) {
	inputList := []string{"pt-1", "pt-2"}

	goodTemplates := []models.PipelineTemplate{
		{CreatedBy: "some-pt-1"},
		{CreatedBy: "some-pt-2"},
	}

	type testCase struct {
		name                    string
		expectErrorCode         errors.CodeType
		expectPipelineTemplates []models.PipelineTemplate
		withCaller              bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "templates not found, possibly due to lack of permissions",
			withCaller: true,
		},
		{
			name:                    "successfully return pipeline templates",
			withCaller:              true,
			expectPipelineTemplates: goodTemplates,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelineTemplates := db.NewMockPipelineTemplates(t)

			if test.withCaller {
				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)

				dbInput := &db.GetPipelineTemplatesInput{
					Filter: &db.PipelineTemplateFilter{
						PipelineTemplateIDs: inputList,
					},
				}
				dbResult := &db.PipelineTemplatesResult{
					PipelineTemplates: test.expectPipelineTemplates,
				}

				mockPipelineTemplates.On("GetPipelineTemplates", mock.Anything, dbInput).
					Return(dbResult, nil)
			}

			dbClient := &db.Client{
				PipelineTemplates: mockPipelineTemplates,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualPipelineTemplates, err := service.GetPipelineTemplatesByIDs(ctx, inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectPipelineTemplates, actualPipelineTemplates)
		})
	}
}

func TestGetPipelineTemplates(t *testing.T) {
	sort := db.PipelineTemplateSortableFieldUpdatedAtAsc

	input := &GetPipelineTemplatesInput{
		Sort: &sort,
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
		ProjectID: "proj-id-1",
	}

	type testCase struct {
		expectResult    *db.PipelineTemplatesResult
		name            string
		expectErrorCode errors.CodeType
		withCaller      bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "templates not found, possibly due to lack of permissions",
			withCaller: true,
		},
		{
			name: "successfully return pipeline templates result, by project, positive",
			expectResult: &db.PipelineTemplatesResult{
				PipelineTemplates: []models.PipelineTemplate{{CreatedBy: "pt-1"}},
			},
			withCaller: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelineTemplates := db.NewMockPipelineTemplates(t)

			dbInput := &db.GetPipelineTemplatesInput{
				Sort:              input.Sort,
				PaginationOptions: input.PaginationOptions,
				Filter: &db.PipelineTemplateFilter{
					ProjectID: &input.ProjectID,
				},
			}

			if test.withCaller {
				mockPipelineTemplates.On("GetPipelineTemplates", mock.Anything, dbInput).Return(test.expectResult, nil)

				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)
			}

			dbClient := &db.Client{
				PipelineTemplates: mockPipelineTemplates,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetPipelineTemplates(ctx, input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestGetPipelineTemplateData(t *testing.T) {
	pipelineTemplateID := "pipeline-template-1"

	goodPipelineTemplate := &models.PipelineTemplate{
		Metadata: models.ResourceMetadata{
			ID: pipelineTemplateID,
		},
	}

	goodData := []byte("this is good pipeline template data")

	type testCase struct {
		injectPermissionOrDataError error
		injectDownloadError         error
		findTemplate                *models.PipelineTemplate
		name                        string
		expectErrorCode             errors.CodeType
		expectData                  []byte
		withCaller                  bool
		hasBeenUploaded             bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "template not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                        "permission check failed",
			withCaller:                  true,
			findTemplate:                goodPipelineTemplate,
			injectPermissionOrDataError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:             errors.EForbidden,
		},
		{
			name:                        "data has not yet been uploaded",
			withCaller:                  true,
			findTemplate:                goodPipelineTemplate,
			injectPermissionOrDataError: errors.New("no pipeline template data has been uploaded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode:             errors.EInvalid,
		},
		{
			name:                "download failed",
			withCaller:          true,
			findTemplate:        goodPipelineTemplate,
			hasBeenUploaded:     true,
			injectDownloadError: errors.New("error while downloading pipeline template data"),
			expectErrorCode:     errors.EInternal,
		},
		{
			name:            "successfully return data",
			withCaller:      true,
			findTemplate:    goodPipelineTemplate,
			hasBeenUploaded: true,
			expectData:      goodData,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockCaller := auth.NewMockCaller(t)
			mockDataStore := NewMockDataStore(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				// Make a (possibly modified) copy of test.findTemplate.
				var copyFindTemplate *models.PipelineTemplate
				if test.findTemplate != nil {
					copyFoundTemplate := *test.findTemplate
					if test.hasBeenUploaded {
						copyFoundTemplate.Status = models.PipelineTemplateUploaded
					}
					copyFindTemplate = &copyFoundTemplate
				}

				mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, pipelineTemplateID).
					Return(copyFindTemplate, nil)

				if copyFindTemplate != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionOrDataError)

					if test.injectPermissionOrDataError == nil {

						mockDataStore.On("GetData", mock.Anything, mock.Anything).
							Return(func(_ context.Context, _ string) (io.ReadCloser, error) {
								copyGoodData := goodData
								return io.NopCloser(bytes.NewReader(copyGoodData)), test.injectDownloadError
							})
					}
				}
			}

			dbClient := &db.Client{
				Projects:          mockProjects,
				PipelineTemplates: mockPipelineTemplates,
			}

			service := &service{
				dbClient:  dbClient,
				dataStore: mockDataStore,
			}

			actualReader, err := service.GetPipelineTemplateData(ctx, pipelineTemplateID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			actualData, err := io.ReadAll(actualReader)
			assert.Nil(t, err)
			actualReader.Close()

			assert.Equal(t, test.expectData, actualData)
		})
	}
}

func TestCreatePipelineTemplate(t *testing.T) {
	pipelineTemplateID := "pipeline-template-1"
	currentTime := time.Now().UTC()

	parentProj := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: "proj-1",
		},
	}

	samplePipelineTemplate := &models.PipelineTemplate{
		Metadata: models.ResourceMetadata{
			ID:                pipelineTemplateID,
			CreationTimestamp: &currentTime,
		},
		CreatedBy: "sample-user@domain.tld",
		ProjectID: parentProj.Metadata.ID,
	}

	input := &CreatePipelineTemplateInput{
		ProjectID: parentProj.Metadata.ID,
	}

	type testCase struct {
		injectProjectError    error
		injectPermissionError error
		injectCreateError     error
		limitError            error
		findProject           *models.Project
		input                 *CreatePipelineTemplateInput
		expectCreated         *models.PipelineTemplate
		injectUpdate          *models.PipelineTemplate
		name                  string
		expectErrorCode       errors.CodeType
		injectGet             []models.PipelineTemplate
		injectCount           int32
	}

	/*
		Test case template:

		type testCase struct {
			name                  string
			input                 *models.CreatePipelineTemplateInput
			findProject           *models.Project
			injectProjectError    error
			injectPermissionError error
			injectCreateError     error
			injectCount           int32
			limitError            error
			injectGet             []models.PipelineTemplate
			injectUpdate          *models.PipelineTemplate
			expectErrorCode       errors.CodeType
			expectCreated         *models.PipelineTemplate
		}
	*/

	testCases := []testCase{
		{
			name:               "project not found",
			input:              input,
			injectProjectError: errors.New("project not found", errors.WithErrorCode(errors.ENotFound)),
			expectErrorCode:    errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			input:                 input,
			findProject:           parentProj,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name: "validation failed",
			input: &CreatePipelineTemplateInput{
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       false,                                      // when versioned is false,
				Name:            ptr.String("not-allowed-name"),             // name and
				SemanticVersion: ptr.String("not-allowed-semantic-version"), // semantic version are not allowed
			},
			findProject:     parentProj,
			expectErrorCode: errors.EInvalid,
		},
		{
			name:              "creation failed due to unspecified internal problem",
			input:             input,
			findProject:       parentProj,
			injectCreateError: errors.New("injected error, creation failed"),
			expectErrorCode:   errors.EInternal,
		},
		{
			name:            "exceeds limit",
			input:           input,
			findProject:     parentProj,
			injectCount:     5,
			expectCreated:   samplePipelineTemplate,
			limitError:      errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
		},
		{
			name:          "successfully create a new pipeline template",
			input:         input,
			findProject:   parentProj,
			injectCount:   4,
			expectCreated: samplePipelineTemplate,
		},
		{
			name: "existing latest is a pre-release and new version is not a pre-release",
			input: &CreatePipelineTemplateInput{
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("0.1.0"),
			},
			findProject: parentProj,
			injectCount: 4,
			injectGet: []models.PipelineTemplate{{
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
				Latest:          true,
			}},
			injectUpdate: &models.PipelineTemplate{
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
				Latest:          false,
			},
			expectCreated: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:                pipelineTemplateID,
					CreationTimestamp: &currentTime,
				},
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("0.1.0"),
				Latest:          true,
			},
		},
		{
			name: "existing latest is not a pre-release and new version is a pre-release",
			input: &CreatePipelineTemplateInput{
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
			},
			findProject: parentProj,
			injectCount: 4,
			injectGet: []models.PipelineTemplate{{
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("0.0.1"),
				Latest:          true,
			}},
			expectCreated: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:                pipelineTemplateID,
					CreationTimestamp: &currentTime,
				},
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
				Latest:          false,
			},
		},
		{
			name: "existing latest is a pre-release and new version is a pre-release",
			input: &CreatePipelineTemplateInput{
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
			},
			findProject: parentProj,
			injectCount: 4,
			injectGet: []models.PipelineTemplate{{
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("0.0.0-pre"),
				Latest:          true,
			}},
			injectUpdate: &models.PipelineTemplate{
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("0.0.0-pre"),
				Latest:          false,
			},
			expectCreated: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:                pipelineTemplateID,
					CreationTimestamp: &currentTime,
				},
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
				Latest:          true,
			},
		},
		{
			name: "existing latest is not a pre-release and new version is not a pre-release",
			input: &CreatePipelineTemplateInput{
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0"),
			},
			findProject: parentProj,
			injectCount: 4,
			injectGet: []models.PipelineTemplate{{
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("0.0.1"),
				Latest:          true,
			}},
			injectUpdate: &models.PipelineTemplate{
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("0.0.1"),
				Latest:          false,
			},
			expectCreated: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:                pipelineTemplateID,
					CreationTimestamp: &currentTime,
				},
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0"),
				Latest:          true,
			},
		},
		{
			name: "no current latest and new version is not a pre-release",
			input: &CreatePipelineTemplateInput{
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0"),
			},
			findProject: parentProj,
			injectCount: 4,
			injectGet:   []models.PipelineTemplate{},
			expectCreated: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:                pipelineTemplateID,
					CreationTimestamp: &currentTime,
				},
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0"),
				Latest:          true,
			},
		},
		{
			name: "no current latest and new version is a pre-release",
			input: &CreatePipelineTemplateInput{
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
			},
			findProject: parentProj,
			injectCount: 4,
			injectGet:   []models.PipelineTemplate{},
			expectCreated: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID:                pipelineTemplateID,
					CreationTimestamp: &currentTime,
				},
				CreatedBy:       "sample-user@domain.tld",
				ProjectID:       parentProj.Metadata.ID,
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0-pre"),
				Latest:          true,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			ctx = auth.WithCaller(ctx, mockCaller)

			mockProjects.On("GetProjectByID", mock.Anything, test.input.ProjectID).
				Return(test.findProject, test.injectProjectError)

			mockCaller.On("RequirePermission", mock.Anything, models.CreatePipelineTemplate, mock.Anything, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			mockCaller.On("GetSubject").Return("mock-caller-subject").Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			mockPipelineTemplates.On("CreatePipelineTemplate", mock.Anything, mock.Anything).
				Return(test.expectCreated, test.injectCreateError).Maybe()

			mockPipelineTemplates.On("GetPipelineTemplates", mock.Anything, mock.Anything).
				Return(&db.PipelineTemplatesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: test.injectCount,
					},
					PipelineTemplates: test.injectGet,
				}, nil).
				Maybe()

			mockPipelineTemplates.On("UpdatePipelineTemplate", mock.Anything, test.injectUpdate).
				Return(test.injectUpdate, nil).Maybe()

			mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitPipelineTemplatesPerProjectPerTimePeriod, test.injectCount).
				Return(test.limitError).Maybe()

			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			mockActivityEvents.On("CreateActivityEvent", mock.Anything,
				&activityevent.CreateActivityEventInput{
					ProjectID:  &test.input.ProjectID,
					Action:     models.ActionCreate,
					TargetType: models.TargetPipelineTemplate,
					TargetID:   &pipelineTemplateID,
				},
			).Return(&models.ActivityEvent{}, nil).Maybe()

			dbClient := &db.Client{
				Projects:          mockProjects,
				PipelineTemplates: mockPipelineTemplates,
				Transactions:      mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
				logger:          logger,
			}

			actualCreated, err := service.CreatePipelineTemplate(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreated, actualCreated)
		})
	}
}

func TestUploadPipelineTemplate(t *testing.T) {
	pipelineTemplateID := "pipeline-template-1"
	projectID := "proj-1"
	sampleReader := strings.NewReader("this is sample pipeline template data")

	type testCase struct {
		name                     string
		existingPipelineTemplate *models.PipelineTemplate
		authError                error
		expectErrorCode          errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully upload pipeline template",
			existingPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: pipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplatePending,
			},
		},
		{
			name: "cannot upload pipeline template since it was already uploaded",
			existingPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: pipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplateUploaded,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "pipeline template not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to upload pipeline template",
			existingPipelineTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: pipelineTemplateID,
				},
				ProjectID: projectID,
				Status:    models.PipelineTemplatePending,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockDataStore := NewMockDataStore(t)
			mockTransactions := db.NewMockTransactions(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)

			mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, pipelineTemplateID).Return(test.existingPipelineTemplate, nil)

			if test.existingPipelineTemplate != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.CreatePipelineTemplate, mock.Anything).Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockPipelineTemplates.On("UpdatePipelineTemplate", mock.Anything, mock.Anything).Return(test.existingPipelineTemplate, nil)

				mockDataStore.On("UploadData", mock.Anything, pipelineTemplateID, sampleReader).Return(nil)

				mockCaller.On("GetSubject").Return("testSubject")
			}

			dbClient := &db.Client{
				Transactions:      mockTransactions,
				PipelineTemplates: mockPipelineTemplates,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:  dbClient,
				dataStore: mockDataStore,
				logger:    logger,
			}

			err := service.UploadPipelineTemplate(auth.WithCaller(ctx, mockCaller), &UploadPipelineTemplateInput{
				Reader: sampleReader,
				ID:     pipelineTemplateID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestDeletePipelineTemplate(t *testing.T) {
	pipelineTemplateID := "pipeline-template-1"

	goodPipelineTemplate := &models.PipelineTemplate{
		Metadata: models.ResourceMetadata{
			ID: pipelineTemplateID,
		},
	}

	input := &DeletePipelineTemplateInput{
		ID: pipelineTemplateID,
	}

	type testCase struct {
		injectPermissionError error
		injectDeleteError     error
		findTemplate          *models.PipelineTemplate
		injectUpdate          *models.PipelineTemplate
		name                  string
		expectErrorCode       errors.CodeType
		injectGet             []models.PipelineTemplate
	}

	/*
		Test case template:

		type testCase struct {
			name                  string
			findTemplate          *models.PipelineTemplate
			injectPermissionError error
			injectDeleteError     error
			injectGet             []models.PipelineTemplate
			injectUpdate          *models.PipelineTemplate
			expectErrorCode       errors.CodeType
		}
	*/

	testCases := []testCase{

		{
			name:            "template not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			findTemplate:          goodPipelineTemplate,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:              "delete failed for unspecified internal problem",
			findTemplate:      goodPipelineTemplate,
			injectDeleteError: errors.New("injected error, delete failed"),
			expectErrorCode:   errors.EInternal,
		},
		{
			name:         "successfully delete a pipeline template",
			findTemplate: goodPipelineTemplate,
		},
		{
			name: "version to delete is the latest version",
			findTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: pipelineTemplateID,
				},
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.1"),
				Latest:          true,
			},
			injectGet: []models.PipelineTemplate{{
				CreatedBy:       "sample-user@domain.tld",
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0"),
				Latest:          false,
			}},
			injectUpdate: &models.PipelineTemplate{
				CreatedBy:       "sample-user@domain.tld",
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0"),
				Latest:          true,
			},
		},
		{
			name: "version to delete is not the latest version",
			findTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: pipelineTemplateID,
				},
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.0"),
				Latest:          false,
			},
		},
		{
			name: "version to delete is the latest version and a pre-release",
			findTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: pipelineTemplateID,
				},
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.1-pre.2"),
				Latest:          true,
			},
			injectGet: []models.PipelineTemplate{{
				CreatedBy:       "sample-user@domain.tld",
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.1-pre.1"),
				Latest:          false,
			}},
			injectUpdate: &models.PipelineTemplate{
				CreatedBy:       "sample-user@domain.tld",
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.1-pre.1"),
				Latest:          true,
			},
		},
		{
			name: "version to delete is the only version",
			findTemplate: &models.PipelineTemplate{
				Metadata: models.ResourceMetadata{
					ID: pipelineTemplateID,
				},
				Versioned:       true,
				Name:            ptr.String("pt-01"),
				SemanticVersion: ptr.String("1.0.1"),
				Latest:          true,
			},
			injectGet: []models.PipelineTemplate{},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockPipelineTemplates := db.NewMockPipelineTemplates(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			ctx = auth.WithCaller(ctx, mockCaller)

			mockPipelineTemplates.On("GetPipelineTemplateByID", mock.Anything, pipelineTemplateID).
				Return(test.findTemplate, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.DeletePipelineTemplate, mock.Anything, mock.Anything).
				Return(test.injectPermissionError).Maybe()

			mockCaller.On("GetSubject").Return("mock-caller-subject").Maybe()

			if test.findTemplate != nil {
				mockPipelineTemplates.On("GetPipelineTemplates", mock.Anything, &db.GetPipelineTemplatesInput{
					Filter: &db.PipelineTemplateFilter{
						ProjectID: &test.findTemplate.ProjectID,
						Versioned: ptr.Bool(true),
						Name:      test.findTemplate.Name,
					},
				}).
					Return(&db.PipelineTemplatesResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
						PipelineTemplates: test.injectGet,
					}, nil).
					Maybe()

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &test.findTemplate.ProjectID,
						Action:     models.ActionDelete,
						TargetType: models.TargetProject,
						TargetID:   &test.findTemplate.ProjectID,
						Payload: &models.ActivityEventDeleteResourcePayload{
							Name: test.findTemplate.Name,
							ID:   test.findTemplate.Metadata.ID,
							Type: models.TargetPipelineTemplate.String(),
						},
					},
				).Return(&models.ActivityEvent{}, nil).Maybe()
			}

			mockPipelineTemplates.On("UpdatePipelineTemplate", mock.Anything, test.injectUpdate).
				Return(test.injectUpdate, nil).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			mockPipelineTemplates.On("DeletePipelineTemplate", mock.Anything, test.findTemplate).
				Return(test.injectDeleteError).Maybe()

			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			dbClient := &db.Client{
				Projects:          mockProjects,
				PipelineTemplates: mockPipelineTemplates,
				Transactions:      mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			err := service.DeletePipelineTemplate(ctx, input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
