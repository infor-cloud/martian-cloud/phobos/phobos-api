package pipelinetemplate

//go:generate go tool mockery --name DataStore --inpackage --case underscore

import (
	"context"
	"io"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/objectstore"
)

const (
	objectStoreKeyPrefix = "pipeline_templates/"
)

// DataStore interface encapsulates the logic for saving and retrieving pipeline template data
type DataStore interface {
	UploadData(ctx context.Context, inputID string, inputReader io.Reader) error
	GetData(ctx context.Context, inputID string) (io.ReadCloser, error)
}

type dataStore struct {
	objectStore objectstore.ObjectStore
}

// NewDataStore creates an instance of the DataStore interface
func NewDataStore(objectStore objectstore.ObjectStore) DataStore {
	return &dataStore{objectStore: objectStore}
}

func (ds *dataStore) UploadData(ctx context.Context, inputID string, inputReader io.Reader) error {
	return ds.objectStore.UploadObject(ctx, ds.objectStoreKey(inputID), inputReader)
}

func (ds *dataStore) GetData(ctx context.Context, inputID string) (io.ReadCloser, error) {
	return ds.objectStore.GetObjectStream(ctx, ds.objectStoreKey(inputID), nil)
}

func (ds *dataStore) objectStoreKey(id string) string {
	return objectStoreKeyPrefix + id
}
