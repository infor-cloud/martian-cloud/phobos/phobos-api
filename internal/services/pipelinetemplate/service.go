// Package pipelinetemplate implements functionality related to Phobos pipeline templates.
package pipelinetemplate

//go:generate go tool mockery --name Service --inpackage --case underscore

import (
	"context"
	"io"

	msemver "github.com/Masterminds/semver/v3"
	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-version"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/semver"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetPipelineTemplatesInput is the input for querying a list of pipeline templates
type GetPipelineTemplatesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.PipelineTemplateSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Versioned filters the pipeline templates by whether they are versioned.
	Versioned *bool
	// Latest filters the pipeline templates by whether they are the latest version.
	Latest *bool
	// Name filters the pipeline templates by exact name (which implies versioned=true)
	Name *string
	// Search filters the pipeline templates by name prefix (which implies versioned=true)
	Search *string
	// ProjectID filters the pipeline templates by the specified project
	ProjectID string
}

// CreatePipelineTemplateInput is the input for creating a pipeline template
// Name and SemanticVersion are required if Versioned is true, prohibited if Versioned is false.
type CreatePipelineTemplateInput struct {
	Name            *string
	SemanticVersion *string
	ProjectID       string
	Versioned       bool
}

// UploadPipelineTemplateInput is the input for uploading to a pipeline template
type UploadPipelineTemplateInput struct {
	Reader io.Reader
	ID     string
}

// DeletePipelineTemplateInput is the input for deleting a pipeline template.
type DeletePipelineTemplateInput struct {
	Version *int
	ID      string
}

// Service implements all pipeline template related functionality
// Please note the HCL data is NOT returned by the Get... methods other than GetPipelineTemplateData.
type Service interface {
	GetPipelineTemplateByID(ctx context.Context, id string) (*models.PipelineTemplate, error)
	GetPipelineTemplateByPRN(ctx context.Context, prn string) (*models.PipelineTemplate, error)
	GetPipelineTemplatesByIDs(ctx context.Context, idList []string) ([]models.PipelineTemplate, error)
	GetPipelineTemplates(ctx context.Context, input *GetPipelineTemplatesInput) (*db.PipelineTemplatesResult, error)
	GetPipelineTemplateData(ctx context.Context, id string) (io.ReadCloser, error)
	CreatePipelineTemplate(ctx context.Context, input *CreatePipelineTemplateInput) (*models.PipelineTemplate, error)
	UploadPipelineTemplate(ctx context.Context, input *UploadPipelineTemplateInput) error
	DeletePipelineTemplate(ctx context.Context, input *DeletePipelineTemplateInput) error
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	limitChecker    limits.LimitChecker
	dataStore       DataStore
	activityService activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	dataStore DataStore,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		dataStore:       dataStore,
		limitChecker:    limitChecker,
		activityService: activityService,
	}
}

// Please note the HCL data is NOT returned by this method.
func (s *service) GetPipelineTemplateByID(ctx context.Context, id string) (*models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineTemplateByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipelineTemplate, err := s.getPipelineTemplateByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this pipeline template.
	err = caller.RequirePermission(ctx, models.ViewPipelineTemplate, auth.WithProjectID(pipelineTemplate.ProjectID))
	if err != nil {
		return nil, err
	}

	return pipelineTemplate, nil
}

func (s *service) GetPipelineTemplateByPRN(ctx context.Context, prn string) (*models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineTemplateByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipelineTemplate, err := s.dbClient.PipelineTemplates.GetPipelineTemplateByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline template by PRN", errors.WithSpan(span))
	}

	if pipelineTemplate == nil {
		return nil, errors.New(
			"pipeline template with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	err = caller.RequirePermission(ctx, models.ViewPipelineTemplate, auth.WithProjectID(pipelineTemplate.ProjectID))
	if err != nil {
		return nil, err
	}

	return pipelineTemplate, nil
}

// Please note the HCL data is NOT returned by this method.
func (s *service) GetPipelineTemplatesByIDs(ctx context.Context, idList []string) ([]models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineTemplatesByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetPipelineTemplatesInput{
		Filter: &db.PipelineTemplateFilter{
			PipelineTemplateIDs: idList,
		},
	}

	resp, err := s.dbClient.PipelineTemplates.GetPipelineTemplates(ctx,
		dbInput,
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline templates", errors.WithSpan(span))
	}

	// Make sure caller has permission to view pipeline templates in all the parent projects.
	for _, pt := range resp.PipelineTemplates {
		err = caller.RequirePermission(ctx, models.ViewPipelineTemplate, auth.WithProjectID(pt.ProjectID))
		if err != nil {
			return nil, err
		}
	}

	return resp.PipelineTemplates, nil
}

// Please note the HCL data is NOT returned by this method.
func (s *service) GetPipelineTemplates(ctx context.Context, input *GetPipelineTemplatesInput) (*db.PipelineTemplatesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineTemplates")
	span.SetAttributes(attribute.String("project", input.ProjectID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// Permission checks by project.
	err = caller.RequirePermission(ctx, models.ViewPipelineTemplate, auth.WithProjectID(input.ProjectID))
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetPipelineTemplatesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.PipelineTemplateFilter{
			ProjectID: &input.ProjectID,
			Versioned: input.Versioned,
			Name:      input.Name,
			Search:    input.Search,
			Latest:    input.Latest,
		},
	}

	resp, err := s.dbClient.PipelineTemplates.GetPipelineTemplates(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline templates", errors.WithSpan(span))
	}

	return resp, nil
}

// Please note this method is the only Get... method that returns HCL data.
func (s *service) GetPipelineTemplateData(ctx context.Context, id string) (io.ReadCloser, error) {
	ctx, span := tracer.Start(ctx, "svc.GetPipelineTemplateData")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	pipelineTemplate, err := s.getPipelineTemplateByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this pipeline template.
	err = caller.RequirePermission(ctx, models.ViewPipelineTemplate, auth.WithProjectID(pipelineTemplate.ProjectID))
	if err != nil {
		return nil, err
	}

	// Return an error if no HCL data has been uploaded.
	if pipelineTemplate.Status != models.PipelineTemplateUploaded {
		err = errors.New("no pipeline template data has been uploaded", errors.WithErrorCode(errors.EInvalid))
		return nil, errors.Wrap(err, "", errors.WithSpan(span))
	}

	// Get the HCL data from the data store.
	rdr, err := s.dataStore.GetData(ctx, pipelineTemplate.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "error downloading pipeline template data")
	}

	return rdr, nil
}

func (s *service) CreatePipelineTemplate(ctx context.Context, input *CreatePipelineTemplateInput) (*models.PipelineTemplate, error) {
	ctx, span := tracer.Start(ctx, "svc.CreatePipelineTemplate")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// Must get the project in order to check permissions, etc.
	project, err := s.getProjectByID(ctx, span, input.ProjectID)
	if err != nil {
		// Error has already been recorded in span.
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CreatePipelineTemplate, auth.WithProjectID(project.Metadata.ID))
	if err != nil {
		return nil, err
	}

	toCreate := &models.PipelineTemplate{
		ProjectID: input.ProjectID,
		CreatedBy: caller.GetSubject(),
		Status:    models.PipelineTemplatePending,
		Versioned: input.Versioned,
		Name:      input.Name,
	}

	if input.SemanticVersion != nil {
		// This will attempt to parse the semantic version and convert it to a semver version if possible
		parsedVersion, err := msemver.NewVersion(*input.SemanticVersion)
		if err != nil {
			return nil, errors.Wrap(err, "invalid semantic version", errors.WithErrorCode(errors.EInvalid))
		}
		toCreate.SemanticVersion = ptr.String(parsedVersion.String())
	}

	// Validate model
	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate a pipeline template model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// If versioned, check if this version is greater than the previous latest
	var pipelineTemplatesResp *db.PipelineTemplatesResult
	if input.Versioned {
		pipelineTemplatesResp, err = s.dbClient.PipelineTemplates.GetPipelineTemplates(ctx, &db.GetPipelineTemplatesInput{
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
			Filter: &db.PipelineTemplateFilter{
				ProjectID: &input.ProjectID,
				Versioned: ptr.Bool(true),
				Name:      input.Name,
				Latest:    ptr.Bool(true),
			},
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to get pipeline templates", errors.WithSpan(span))
		}
	}

	// Must do a transaction in order to roll back if the limit was exceeded.
	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreatePipelineTemplate: %v", txErr)
		}
	}()

	// If versioned, determine whether this is a new latest version.
	if input.Versioned {
		semanticVersion, sErr := version.NewSemver(*input.SemanticVersion)
		if sErr != nil {
			return nil, errors.Wrap(sErr, "failed to validate a pipeline template semantic version", errors.WithSpan(span))
		}

		if len(pipelineTemplatesResp.PipelineTemplates) > 0 {
			prevLatest := pipelineTemplatesResp.PipelineTemplates[0]
			// We know prevLatest is versioned, so SemanticVersion is not nil.
			prevSemanticVersion, sErr := version.NewSemver(*prevLatest.SemanticVersion)
			if sErr != nil {
				return nil, errors.Wrap(sErr, "semantic version validation failed", errors.WithSpan(span))
			}
			if semver.IsSemverGreaterThan(semanticVersion, prevSemanticVersion) {
				toCreate.Latest = true
				// Remove latest from version
				prevLatest.Latest = false
				if _, uErr := s.dbClient.PipelineTemplates.UpdatePipelineTemplate(txContext, &prevLatest); uErr != nil {
					return nil, uErr
				}
			}
		} else {
			// New version is the latest since it is the only version
			toCreate.Latest = true
		}
	}

	pipelineTemplate, err := s.dbClient.PipelineTemplates.CreatePipelineTemplate(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create pipeline template", errors.WithSpan(span))
	}

	// Get the number of pipeline templates in this project to check whether we just violated the limit.
	pipelineTemplatesResult, err := s.dbClient.PipelineTemplates.GetPipelineTemplates(txContext, &db.GetPipelineTemplatesInput{
		Filter: &db.PipelineTemplateFilter{
			TimeRangeStart: ptr.Time(pipelineTemplate.Metadata.CreationTimestamp.Add(-limits.ResourceLimitTimePeriod)),
			ProjectID:      &input.ProjectID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline templates", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitPipelineTemplatesPerProjectPerTimePeriod,
		pipelineTemplatesResult.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "limit check failed", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &pipelineTemplate.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetPipelineTemplate,
			TargetID:   &pipelineTemplate.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a pipeline template.",
		"caller", caller.GetSubject(),
		"pipelineTemplateID", pipelineTemplate.Metadata.ID,
	)

	return pipelineTemplate, nil
}

func (s *service) UploadPipelineTemplate(ctx context.Context, input *UploadPipelineTemplateInput) error {
	ctx, span := tracer.Start(ctx, "svc.UploadPipelineTemplate")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pt, err := s.getPipelineTemplateByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	err = caller.RequirePermission(ctx, models.CreatePipelineTemplate, auth.WithProjectID(pt.ProjectID))
	if err != nil {
		return err
	}

	if pt.Status == models.PipelineTemplateUploaded {
		// Uploading again (aka updating) is not allowed.
		return errors.New("pipeline template has already been uploaded; updating is not allowed", errors.WithErrorCode(errors.EInvalid))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UploadPipelineTemplate: %v", txErr)
		}
	}()

	// Update status of the pipeline template to uploaded before uploading the HCL file since
	// the HCL file is not guaranteed to be uploaded successfully.
	pt.Status = models.PipelineTemplateUploaded
	if _, err = s.dbClient.PipelineTemplates.UpdatePipelineTemplate(txContext, pt); err != nil {
		return err
	}

	if err = s.dataStore.UploadData(ctx, input.ID, input.Reader); err != nil {
		return errors.Wrap(err, "Failed to write pipeline template HCL file to object storage", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Uploaded a pipeline template.",
		"caller", caller.GetSubject(),
		"pipelineTemplateID", input.ID,
	)

	return nil
}

func (s *service) DeletePipelineTemplate(ctx context.Context, input *DeletePipelineTemplateInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeletePipelineTemplate")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	pipelineTemplate, err := s.getPipelineTemplateByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	err = caller.RequirePermission(ctx, models.DeletePipelineTemplate, auth.WithProjectID(pipelineTemplate.ProjectID))
	if err != nil {
		return err
	}

	if input.Version != nil {
		pipelineTemplate.Metadata.Version = *input.Version
	}

	// Reset latest flag if we're deleting the latest version
	var newLatestVersion *models.PipelineTemplate
	if pipelineTemplate.Latest {
		pipelineTemplatesResp, gpErr := s.dbClient.PipelineTemplates.GetPipelineTemplates(ctx, &db.GetPipelineTemplatesInput{
			Filter: &db.PipelineTemplateFilter{
				ProjectID: &pipelineTemplate.ProjectID,
				Versioned: ptr.Bool(true),
				Name:      pipelineTemplate.Name,
			},
		})
		if gpErr != nil {
			return errors.Wrap(err, "failed to get pipeline template", errors.WithSpan(span))
		}

		for _, pt := range pipelineTemplatesResp.PipelineTemplates {
			vCopy := pt

			// Skip if this is the pipeline template we're deleting
			if pt.Metadata.ID == pipelineTemplate.Metadata.ID {
				continue
			}

			if newLatestVersion == nil {
				newLatestVersion = &vCopy
				continue
			}

			// We know the pipeline template is versioned, so SemanticVersion is not nil.
			latestSemanticVersion, lsErr := version.NewSemver(*newLatestVersion.SemanticVersion)
			if lsErr != nil {
				return errors.Wrap(lsErr, "failed to validate semantic version", errors.WithSpan(span))
			}

			currentSemanticVersion, csErr := version.NewSemver(*vCopy.SemanticVersion)
			if csErr != nil {
				return errors.Wrap(csErr, "failed to validate semantic version", errors.WithSpan(span))
			}

			if semver.IsSemverGreaterThan(currentSemanticVersion, latestSemanticVersion) {
				newLatestVersion = &vCopy
			}
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for DeletePipelineTemplate: %v", txErr)
		}
	}()

	// Delete the target pipeline template from the DB.
	if err = s.dbClient.PipelineTemplates.DeletePipelineTemplate(txContext, pipelineTemplate); err != nil {
		return err
	}

	if newLatestVersion != nil {
		s.logger.Infof(
			"Deleted latest pipeline template, latest flag is being set to latest version %s for siblings of the late %s",
			newLatestVersion.SemanticVersion,
			pipelineTemplate.Metadata.PRN,
		)
		newLatestVersion.Latest = true
		if _, err = s.dbClient.PipelineTemplates.UpdatePipelineTemplate(txContext, newLatestVersion); err != nil {
			return err
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &pipelineTemplate.ProjectID,
			Action:     models.ActionDelete,
			TargetType: models.TargetProject,
			TargetID:   &pipelineTemplate.ProjectID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: pipelineTemplate.Name,
				ID:   pipelineTemplate.Metadata.ID,
				Type: models.TargetPipelineTemplate.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a pipeline template.",
		"caller", caller.GetSubject(),
		"pipelineTemplateName", pipelineTemplate.Name,
		"pipelineTemplateID", pipelineTemplate.Metadata.ID,
	)

	return nil
}

// getPipelineTemplateByID returns a non-nil pipeline template.
// If there is an error or no such template exists, it records the error in the span.
func (s *service) getPipelineTemplateByID(ctx context.Context, span trace.Span, id string) (*models.PipelineTemplate, error) {
	gotPipelineTemplate, err := s.dbClient.PipelineTemplates.GetPipelineTemplateByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pipeline template by ID", errors.WithSpan(span))
	}

	if gotPipelineTemplate == nil {
		return nil, errors.New(
			"pipeline template with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return gotPipelineTemplate, nil
}

// getProjectByID returns a non-nil project.
// If there is an error or no such project exists, it records the error in the span.
func (s *service) getProjectByID(ctx context.Context, span trace.Span, id string) (*models.Project, error) {
	gotProject, err := s.dbClient.Projects.GetProjectByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project by ID", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	if gotProject == nil {
		return nil, errors.New(
			"project with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return gotProject, nil
}
