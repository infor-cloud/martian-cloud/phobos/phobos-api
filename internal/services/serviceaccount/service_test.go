package serviceaccount

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jws"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	jwsprovider "gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/jws"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

type keyPair struct {
	priv jwk.Key
	pub  jwk.Key
}

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	limitChecker := limits.NewMockLimitChecker(t)
	activityService := activityevent.NewMockService(t)
	identityProvider := auth.NewIdentityProvider(jwsprovider.NewMockProvider(t), "https://test.phobos")
	configFetcher := auth.NewOpenIDConfigFetcher()

	expect := &service{
		logger:              logger,
		dbClient:            dbClient,
		limitChecker:        limitChecker,
		idp:                 identityProvider,
		openIDConfigFetcher: configFetcher,
		activityService:     activityService,
	}

	// Function equality won't work with assert.Equal, hence a "nil" is passed in for getKeySetFunc.
	assert.Equal(t, expect, newService(logger, dbClient, limitChecker, identityProvider, configFetcher, nil, activityService))
}

func TestGetServiceAccountByID(t *testing.T) {
	type testCase struct {
		name           string
		serviceAccount *models.ServiceAccount
		authError      error
		expectErrCode  errors.CodeType
		isAdmin        bool
	}

	tests := []testCase{
		{
			name: "admin user gets a global service account",
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name: "get an organization service account",
			serviceAccount: &models.ServiceAccount{
				OrganizationID: ptr.String("org123"),
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "get a project service account",
			serviceAccount: &models.ServiceAccount{
				Scope:          models.ProjectScope,
				OrganizationID: ptr.String("org123"),
				ProjectID:      ptr.String("proj456"),
			},
		},
		{
			name: "subject does not have permission to view project service account",
			serviceAccount: &models.ServiceAccount{
				Scope:          models.ProjectScope,
				OrganizationID: ptr.String("org123"),
				ProjectID:      ptr.String("456"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to view organization service account",
			serviceAccount: &models.ServiceAccount{
				OrganizationID: ptr.String("org123"),
				Scope:          models.OrganizationScope,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "service account does not exist",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "non admin user cannot get a global service account",
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			expectErrCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockServiceAccounts.On("GetServiceAccountByID", mock.Anything, mock.Anything).Return(test.serviceAccount, nil)

			if test.serviceAccount != nil {
				switch test.serviceAccount.Scope {
				case models.GlobalScope:
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ServiceAccountResource, mock.Anything).
						Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewServiceAccount, mock.Anything).
						Return(test.authError)
				}
			}

			dbClient := &db.Client{
				ServiceAccounts: mockServiceAccounts,
			}

			service := &service{
				dbClient: dbClient,
			}

			serviceAccount, err := service.GetServiceAccountByID(auth.WithCaller(ctx, mockCaller), "serviceAccountID")

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.serviceAccount, serviceAccount)
		})
	}
}

func TestGetServiceAccountByPRN(t *testing.T) {
	serviceAccountPRN := "prn:service-account:serviceAccountID" // Note: PRN is not reflective of service account type.

	type testCase struct {
		name           string
		serviceAccount *models.ServiceAccount
		authError      error
		expectErrCode  errors.CodeType
		isAdmin        bool
	}

	tests := []testCase{
		{
			name: "admin user gets a global service account",
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name: "get an organization service account",
			serviceAccount: &models.ServiceAccount{
				OrganizationID: ptr.String("org123"),
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "get a project service account",
			serviceAccount: &models.ServiceAccount{
				Scope:          models.OrganizationScope,
				OrganizationID: ptr.String("org123"),
				ProjectID:      ptr.String("456"),
			},
		},
		{
			name: "subject does not have permission to view organization service account",
			serviceAccount: &models.ServiceAccount{
				OrganizationID: ptr.String("org123"),
				Scope:          models.OrganizationScope,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to view project service account",
			serviceAccount: &models.ServiceAccount{
				Scope:          models.OrganizationScope,
				OrganizationID: ptr.String("org123"),
				ProjectID:      ptr.String("456"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "service account does not exist",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "non admin user cannot get a global service account",
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			expectErrCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockServiceAccounts.On("GetServiceAccountByPRN", mock.Anything, serviceAccountPRN).Return(test.serviceAccount, nil)

			if test.serviceAccount != nil {
				if test.serviceAccount.Scope.Equals(models.OrganizationScope) {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ServiceAccountResource, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			dbClient := &db.Client{
				ServiceAccounts: mockServiceAccounts,
			}

			service := &service{
				dbClient: dbClient,
			}

			serviceAccount, err := service.GetServiceAccountByPRN(auth.WithCaller(ctx, mockCaller), serviceAccountPRN)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.serviceAccount, serviceAccount)
		})
	}
}

func TestGetServiceAccounts(t *testing.T) {
	organizationID := "org123"
	agentID := "agentID"
	projectID := "project123"
	searchPrefix := "search-prefix-1"

	type testCase struct {
		name          string
		input         *GetServiceAccountsInput
		org           *models.Organization
		agent         *models.Agent
		authError     error
		expectErrCode errors.CodeType
		isAdmin       bool
	}

	tests := []testCase{
		{
			name: "get service accounts by search prefix",
			input: &GetServiceAccountsInput{
				Search: &searchPrefix,
			},
			isAdmin: true,
		},
		{
			name: "get service accounts by organization ID, any scope",
			input: &GetServiceAccountsInput{
				OrganizationID: &organizationID,
			},
		},
		{
			name: "get service accounts by organization ID, organization scope only",
			input: &GetServiceAccountsInput{
				OrganizationID:       &organizationID,
				ServiceAccountScopes: []models.ScopeType{models.OrganizationScope},
			},
		},
		{
			name: "get service accounts by project ID, any scope",
			input: &GetServiceAccountsInput{
				ProjectID: &projectID,
			},
		},
		{
			name: "get service accounts by project ID, project scope only",
			input: &GetServiceAccountsInput{
				ProjectID:            &projectID,
				ServiceAccountScopes: []models.ScopeType{models.ProjectScope},
			},
		},
		{
			name: "get service accounts by project ID, organization scope only",
			input: &GetServiceAccountsInput{
				ProjectID:            &projectID,
				ServiceAccountScopes: []models.ScopeType{models.OrganizationScope},
			},
		},
		{
			name: "get service accounts for organization agent",
			input: &GetServiceAccountsInput{
				AgentID: &agentID,
			},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
		},
		{
			name: "admin user queries for shared agent service accounts",
			input: &GetServiceAccountsInput{
				AgentID: &agentID,
			},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type: models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name: "admin user queries for global service accounts",
			input: &GetServiceAccountsInput{
				ServiceAccountScopes: []models.ScopeType{models.GlobalScope},
			},
			isAdmin: true,
		},
		{
			name: "agent does not exist",
			input: &GetServiceAccountsInput{
				AgentID: &agentID,
			},
			expectErrCode: errors.ENotFound,
		},
		{
			name: "subject does not have permission to view organization service account",
			input: &GetServiceAccountsInput{
				OrganizationID: &organizationID,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to view organization agent's service account",
			input: &GetServiceAccountsInput{
				AgentID: &agentID,
			},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "non admin user cannot get shared agent's service accounts",
			input: &GetServiceAccountsInput{
				AgentID: &agentID,
			},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type: models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name: "non admin user cannot get global service accounts",
			input: &GetServiceAccountsInput{
				ServiceAccountScopes: []models.ScopeType{models.GlobalScope},
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name:    "only admin can query without any filters",
			input:   &GetServiceAccountsInput{},
			isAdmin: true,
		},
		{
			name:          "non admin cannot query without any filters",
			input:         &GetServiceAccountsInput{},
			expectErrCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)
			mockAgents := db.NewMockAgents(t)

			if test.input.AgentID != nil {
				mockAgents.On("GetAgentByID", mock.Anything, mock.Anything).Return(test.agent, nil)
			}

			mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ServiceAccountResource, mock.Anything).
				Return(test.authError).Maybe()
			if test.input.ProjectID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewServiceAccount, mock.Anything).
					Return(test.authError)
			}
			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()

			if test.expectErrCode == "" {
				dbInput := &db.GetServiceAccountsInput{
					Filter: &db.ServiceAccountFilter{
						Search:               test.input.Search,
						OrganizationID:       test.input.OrganizationID,
						ProjectID:            test.input.ProjectID,
						AgentID:              test.input.AgentID,
						ServiceAccountScopes: test.input.ServiceAccountScopes,
					},
				}

				if test.org != nil {
					dbInput.Filter.OrganizationID = &test.org.Metadata.ID
				}

				mockServiceAccounts.On("GetServiceAccounts", mock.Anything, dbInput).Return(&db.ServiceAccountsResult{
					ServiceAccounts: []*models.ServiceAccount{
						{
							Name:           "returned-service-account",
							Scope:          models.OrganizationScope,
							OrganizationID: ptr.String("returned-sa-org"),
						},
					},
				}, nil)
			}

			dbClient := &db.Client{
				ServiceAccounts: mockServiceAccounts,
				Agents:          mockAgents,
			}

			service := &service{
				dbClient: dbClient,
			}

			serviceAccounts, err := service.GetServiceAccounts(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.NotNil(t, serviceAccounts)
			assert.Len(t, serviceAccounts.ServiceAccounts, 1)
		})
	}
}

func TestGetServiceAccountsByIDs(t *testing.T) {
	idList := []string{"serviceAccountID1"}
	organizationID := "org123"
	projectID := "proj456"

	type testCase struct {
		name           string
		serviceAccount *models.ServiceAccount
		authError      error
		expectErrCode  errors.CodeType
		isAdmin        bool
	}

	tests := []testCase{
		{
			name: "get organization service accounts",
			serviceAccount: &models.ServiceAccount{
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "get project service accounts",
			serviceAccount: &models.ServiceAccount{
				Scope:          models.ProjectScope,
				OrganizationID: &organizationID,
				ProjectID:      &projectID,
			},
		},
		{
			name: "admin queries for global service account",
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name: "non admin user cannot get global service account",
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to view organization service account",
			serviceAccount: &models.ServiceAccount{
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to view project service account",
			serviceAccount: &models.ServiceAccount{
				Scope:          models.OrganizationScope,
				OrganizationID: &organizationID,
				ProjectID:      &projectID,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockServiceAccounts.On("GetServiceAccounts", mock.Anything, &db.GetServiceAccountsInput{
				Filter: &db.ServiceAccountFilter{
					ServiceAccountIDs: idList,
				},
			}).Return(&db.ServiceAccountsResult{
				ServiceAccounts: []*models.ServiceAccount{test.serviceAccount},
			}, nil)

			if test.serviceAccount != nil {
				switch test.serviceAccount.Scope {
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewServiceAccount, mock.Anything).
						Return(test.authError)
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ServiceAccountResource, mock.Anything).
						Return(test.authError)
				case models.GlobalScope:
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			dbClient := &db.Client{
				ServiceAccounts: mockServiceAccounts,
			}

			service := &service{
				dbClient: dbClient,
			}

			serviceAccounts, err := service.GetServiceAccountsByIDs(auth.WithCaller(ctx, mockCaller), idList)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Len(t, serviceAccounts, 1)
			assert.Equal(t, test.serviceAccount, serviceAccounts[0])
		})
	}
}

func TestCreateServiceAccount(t *testing.T) {
	organizationID := "org-1"
	projectID := "proj-1"
	testSubject := "testSubject1"
	serviceAccountID := "serviceAccountID"

	sampleIssuer := "https://test.phobos"
	sampleBoundClaims := map[string]string{
		"sub": "testSubject1",
		"aud": "phobos",
	}

	type testCase struct {
		authError          error
		limitExceededError error
		input              *CreateServiceAccountInput
		expectCreated      *models.ServiceAccount
		name               string
		expectErrCode      errors.CodeType
		isAdmin            bool
	}

	tests := []testCase{
		{
			name: "successfully create project service account",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				ProjectID:   &projectID,
				Scope:       models.ProjectScope,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
			expectCreated: &models.ServiceAccount{
				Metadata:       models.ResourceMetadata{ID: serviceAccountID},
				Name:           "service-account-1",
				Description:    "service account description",
				OrganizationID: &organizationID,
				ProjectID:      &projectID,
				Scope:          models.ProjectScope,
				CreatedBy:      testSubject,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
		},
		{
			name: "successfully create organization service account",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				OrgID:       &organizationID,
				Scope:       models.OrganizationScope,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
			expectCreated: &models.ServiceAccount{
				Metadata:       models.ResourceMetadata{ID: serviceAccountID},
				Name:           "service-account-1",
				Description:    "service account description",
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
				CreatedBy:      testSubject,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
		},
		{
			name: "successfully create global service account",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				Scope:       models.GlobalScope,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
			expectCreated: &models.ServiceAccount{
				Metadata:    models.ResourceMetadata{ID: serviceAccountID},
				Name:        "service-account-1",
				Description: "service account description",
				Scope:       models.GlobalScope,
				CreatedBy:   testSubject,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
			isAdmin: true,
		},
		{
			name: "project service accounts must be associated with a project",
			input: &CreateServiceAccountInput{
				Scope: models.ProjectScope,
				OrgID: &organizationID,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name: "organization service accounts must be associated with an organization",
			input: &CreateServiceAccountInput{
				Scope: models.OrganizationScope,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name: "global service accounts cannot be associated with an organization",
			input: &CreateServiceAccountInput{
				Scope: models.GlobalScope,
				OrgID: &organizationID,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name: "global service accounts cannot be associated with a project",
			input: &CreateServiceAccountInput{
				Scope:     models.GlobalScope,
				ProjectID: &projectID,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name: "subject does not have permission to create a project service account",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				ProjectID:   &projectID,
				Scope:       models.ProjectScope,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to create organization service account",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				OrgID:       &organizationID,
				Scope:       models.OrganizationScope,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "only system admins can create global service accounts",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				Scope:       models.GlobalScope,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name: "organization service account limit exceeded",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				OrgID:       &organizationID,
				Scope:       models.OrganizationScope,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
			expectCreated: &models.ServiceAccount{
				Metadata:       models.ResourceMetadata{ID: serviceAccountID},
				Name:           "service-account-1",
				Description:    "service account description",
				Scope:          models.OrganizationScope,
				OrganizationID: &organizationID,
				CreatedBy:      testSubject,
				OIDCTrustPolicies: []models.OIDCTrustPolicy{
					{
						Issuer:          sampleIssuer,
						BoundClaimsType: models.BoundClaimsTypeString,
						BoundClaims:     sampleBoundClaims,
					},
				},
			},
			limitExceededError: errors.New("Limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrCode:      errors.EInvalid,
		},
		{
			name: "model validation error",
			input: &CreateServiceAccountInput{
				Name:        "service-account-1",
				Description: "service account description",
				OrgID:       &organizationID,
				Scope:       models.OrganizationScope,
				// Every service account must have at least one OIDC trust policy.
			},
			expectErrCode: errors.EInvalid,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)
			mockProjects := db.NewMockProjects(t)
			mockLimits := limits.NewMockLimitChecker(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.CreateServiceAccount, mock.Anything).
				Return(test.authError).Maybe()

			if (test.input.Scope.Equals(models.GlobalScope)) &&
				(test.input.OrgID == nil) &&
				(test.input.ProjectID == nil) {
				mockCaller.On("IsAdmin").Return(test.isAdmin)
			}

			mockProjects.On("GetProjectByID", mock.Anything, mock.Anything).
				Return(&models.Project{
					Metadata: models.ResourceMetadata{
						ID: projectID,
					},
					OrgID: organizationID,
				}, nil).Maybe()

			if test.expectCreated != nil && test.expectErrCode != errors.EForbidden {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

				toCreate := &models.ServiceAccount{
					Name:              test.input.Name,
					Description:       test.input.Description,
					OrganizationID:    test.expectCreated.OrganizationID,
					ProjectID:         test.expectCreated.ProjectID,
					Scope:             test.input.Scope,
					CreatedBy:         testSubject,
					OIDCTrustPolicies: test.expectCreated.OIDCTrustPolicies,
				}

				mockServiceAccounts.On("CreateServiceAccount", mock.Anything, toCreate).Return(test.expectCreated, nil)
			}

			if (test.expectCreated != nil) && (test.expectCreated.Scope != models.GlobalScope) {
				filter := &db.ServiceAccountFilter{
					OrganizationID: test.expectCreated.OrganizationID,
					ProjectID:      test.expectCreated.ProjectID,
				}
				filterScope := models.OrganizationScope
				limitToCheck := limits.ResourceLimitServiceAccountsPerOrganization
				if test.expectCreated.Scope == models.ProjectScope {
					filter.OrganizationID = nil
					filterScope = models.ProjectScope
					limitToCheck = limits.ResourceLimitServiceAccountsPerProject
				}
				filter.ServiceAccountScopes = []models.ScopeType{filterScope}
				mockServiceAccounts.On("GetServiceAccounts", mock.Anything, &db.GetServiceAccountsInput{
					Filter: filter,
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.ServiceAccountsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil)

				mockLimits.On("CheckLimit", mock.Anything, limitToCheck, int32(1)).
					Return(test.limitExceededError)
			}

			if test.expectErrCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: test.expectCreated.OrganizationID,
						ProjectID:      test.expectCreated.ProjectID,
						Action:         models.ActionCreate,
						TargetType:     models.TargetServiceAccount,
						TargetID:       &test.expectCreated.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				ServiceAccounts: mockServiceAccounts,
				Projects:        mockProjects,
				Transactions:    mockTransactions,
			}

			testLogger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          testLogger,
				limitChecker:    mockLimits,
				activityService: mockActivityEvents,
			}

			serviceAccount, err := service.CreateServiceAccount(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreated, serviceAccount)
		})
	}
}

func TestUpdateServiceAccount(t *testing.T) {
	serviceAccountID := "serviceAccountID"
	serviceAccountName := "service-account-1"
	organizationID := "org123"
	projectID := "proj123"
	updatedDescription := "updated description"
	testSubject := "testSubject1"

	sampleOIDCTrustPolicy := []models.OIDCTrustPolicy{
		{
			Issuer:          "https://test.phobos",
			BoundClaimsType: models.BoundClaimsTypeGlob,
			BoundClaims: map[string]string{
				"sub": "testSubject1",
				"aud": "phobos",
			},
		},
	}

	type testCase struct {
		name           string
		input          *UpdateServiceAccountInput
		serviceAccount *models.ServiceAccount
		authError      error
		expectErrCode  errors.CodeType
		isAdmin        bool
		expectUpdated  bool
	}

	tests := []testCase{
		{
			name: "successfully update project service account",
			input: &UpdateServiceAccountInput{
				ID:          serviceAccountID,
				Description: &updatedDescription,
			},
			serviceAccount: &models.ServiceAccount{
				Metadata:          models.ResourceMetadata{ID: serviceAccountID},
				Name:              serviceAccountName,
				Scope:             models.OrganizationScope,
				OrganizationID:    &organizationID,
				ProjectID:         &projectID,
				Description:       "original description",
				OIDCTrustPolicies: sampleOIDCTrustPolicy,
			},
			expectUpdated: true,
		},
		{
			name: "successfully update organization service account",
			input: &UpdateServiceAccountInput{
				ID:          serviceAccountID,
				Description: &updatedDescription,
			},
			serviceAccount: &models.ServiceAccount{
				Metadata:          models.ResourceMetadata{ID: serviceAccountID},
				Name:              serviceAccountName,
				Scope:             models.OrganizationScope,
				OrganizationID:    &organizationID,
				Description:       "original description",
				OIDCTrustPolicies: sampleOIDCTrustPolicy,
			},
			expectUpdated: true,
		},
		{
			name: "successfully update global service account",
			input: &UpdateServiceAccountInput{
				ID:          serviceAccountID,
				Description: &updatedDescription,
			},
			serviceAccount: &models.ServiceAccount{
				Metadata:          models.ResourceMetadata{ID: serviceAccountID},
				Name:              serviceAccountName,
				Description:       "original description",
				Scope:             models.GlobalScope,
				OIDCTrustPolicies: sampleOIDCTrustPolicy,
			},
			expectUpdated: true,
			isAdmin:       true,
		},
		{
			name: "subject does not have permission to update organization service account",
			input: &UpdateServiceAccountInput{
				ID:          serviceAccountID,
				Description: &updatedDescription,
			},
			serviceAccount: &models.ServiceAccount{
				Metadata:          models.ResourceMetadata{ID: serviceAccountID},
				Name:              serviceAccountName,
				OrganizationID:    &organizationID,
				Description:       "original description",
				Scope:             models.OrganizationScope,
				OIDCTrustPolicies: sampleOIDCTrustPolicy,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "only system admins can update global service accounts",
			input: &UpdateServiceAccountInput{
				ID:          serviceAccountID,
				Description: &updatedDescription,
			},
			serviceAccount: &models.ServiceAccount{
				Metadata:          models.ResourceMetadata{ID: serviceAccountID},
				Name:              serviceAccountName,
				Description:       "original description",
				Scope:             models.GlobalScope,
				OIDCTrustPolicies: sampleOIDCTrustPolicy,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name: "service account does not exist",
			input: &UpdateServiceAccountInput{
				ID:          serviceAccountID,
				Description: &updatedDescription,
			},
			expectErrCode: errors.ENotFound,
		},
		{
			name: "model validation error",
			input: &UpdateServiceAccountInput{
				ID:          serviceAccountID,
				Description: ptr.String(strings.Repeat("invalid", 100)),
			},
			serviceAccount: &models.ServiceAccount{
				Metadata:          models.ResourceMetadata{ID: serviceAccountID},
				Name:              serviceAccountName,
				Description:       "original description",
				Scope:             models.GlobalScope,
				OIDCTrustPolicies: sampleOIDCTrustPolicy,
			},
			expectErrCode: errors.EInvalid,
			isAdmin:       true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			mockServiceAccounts.On("GetServiceAccountByID", mock.Anything, serviceAccountID).Return(test.serviceAccount, nil)

			if test.serviceAccount != nil {
				if test.serviceAccount.Scope.Equals(models.OrganizationScope) {
					mockCaller.On("RequirePermission", mock.Anything, models.UpdateServiceAccount, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			if test.expectUpdated {
				test.serviceAccount.Description = updatedDescription
				mockServiceAccounts.On("UpdateServiceAccount", mock.Anything, test.serviceAccount).Return(test.serviceAccount, nil)

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: test.serviceAccount.OrganizationID,
						ProjectID:      test.serviceAccount.ProjectID,
						Action:         models.ActionUpdate,
						TargetType:     models.TargetServiceAccount,
						TargetID:       &test.serviceAccount.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				ServiceAccounts: mockServiceAccounts,
				Transactions:    mockTransactions,
			}

			testLogger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          testLogger,
				activityService: mockActivityEvents,
			}

			serviceAccount, err := service.UpdateServiceAccount(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.serviceAccount, serviceAccount)
		})
	}
}

func TestDeleteServiceAccount(t *testing.T) {
	organizationID := "org123"
	projectID := "proj456"
	testSubject := "testSubject1"
	serviceAccountID := "serviceAccountID"

	type testCase struct {
		name           string
		serviceAccount *models.ServiceAccount
		authError      error
		expectErrCode  errors.CodeType
		isAdmin        bool
	}

	tests := []testCase{
		{
			name: "successfully delete project service account",
			serviceAccount: &models.ServiceAccount{
				Metadata:       models.ResourceMetadata{ID: serviceAccountID},
				Scope:          models.OrganizationScope,
				OrganizationID: &organizationID,
				ProjectID:      &projectID,
			},
		},
		{
			name: "successfully delete organization service account",
			serviceAccount: &models.ServiceAccount{
				Metadata:       models.ResourceMetadata{ID: serviceAccountID},
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "successfully delete global service account",
			serviceAccount: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{ID: serviceAccountID},
				Scope:    models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name: "subject does not have permission to delete project service account",
			serviceAccount: &models.ServiceAccount{
				Metadata:       models.ResourceMetadata{ID: serviceAccountID},
				Scope:          models.OrganizationScope,
				OrganizationID: &organizationID,
				ProjectID:      &projectID,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to delete organization service account",
			serviceAccount: &models.ServiceAccount{
				Metadata:       models.ResourceMetadata{ID: serviceAccountID},
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "only system admins can delete global service accounts",
			serviceAccount: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{ID: serviceAccountID},
				Scope:    models.GlobalScope,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "service account does not exist",
			expectErrCode: errors.ENotFound,
		},
	}

	for _, test := range tests {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		mockCaller := auth.NewMockCaller(t)
		mockServiceAccounts := db.NewMockServiceAccounts(t)
		mockTransactions := db.NewMockTransactions(t)
		mockActivityEvents := activityevent.NewMockService(t)

		mockCaller.On("GetSubject").Return(testSubject).Maybe()

		mockServiceAccounts.On("GetServiceAccountByID", mock.Anything, serviceAccountID).Return(test.serviceAccount, nil)

		if test.serviceAccount != nil {
			if test.serviceAccount.Scope.Equals(models.OrganizationScope) {
				mockCaller.On("RequirePermission", mock.Anything, models.DeleteServiceAccount, mock.Anything).Return(test.authError)
			} else {
				mockCaller.On("IsAdmin").Return(test.isAdmin)
			}
		}

		if test.expectErrCode == "" {
			mockServiceAccounts.On("DeleteServiceAccount", mock.Anything, test.serviceAccount).Return(nil)

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
			mockTransactions.On("CommitTx", mock.Anything).Return(nil)

			var targetType models.ActivityEventTargetType
			switch test.serviceAccount.Scope {
			case models.OrganizationScope:
				targetType = models.TargetOrganization
			case models.GlobalScope:
				targetType = models.TargetGlobal
			}

			mockActivityEvents.On("CreateActivityEvent", mock.Anything,
				&activityevent.CreateActivityEventInput{
					OrganizationID: test.serviceAccount.OrganizationID,
					ProjectID:      test.serviceAccount.ProjectID,
					Action:         models.ActionDelete,
					TargetType:     targetType,
					TargetID:       test.serviceAccount.OrganizationID,
					Payload: &models.ActivityEventDeleteResourcePayload{
						Name: &test.serviceAccount.Name,
						ID:   test.serviceAccount.Metadata.ID,
						Type: models.TargetServiceAccount.String(),
					},
				},
			).Return(&models.ActivityEvent{}, nil)
		}

		dbClient := &db.Client{
			ServiceAccounts: mockServiceAccounts,
			Transactions:    mockTransactions,
		}

		testLogger, _ := logger.NewForTest()
		service := &service{
			dbClient:        dbClient,
			logger:          testLogger,
			activityService: mockActivityEvents,
		}

		err := service.DeleteServiceAccount(auth.WithCaller(ctx, mockCaller), &DeleteServiceAccountInput{
			ID: serviceAccountID,
		})

		if test.expectErrCode != "" {
			assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
			return
		}

		if err != nil {
			t.Fatal(err)
		}
	}
}

func TestCreateToken(t *testing.T) {
	validKeyPair := createKeyPair(t)
	invalidKeyPair := createKeyPair(t)

	keyID := validKeyPair.pub.KeyID()
	serviceAccountID := "d4a94ff5-154e-4758-8039-55e2147fa154"
	issuer := "https://test.phobos"
	sub := "testSubject1"
	organizationID := "org123"

	basicPolicy := []models.OIDCTrustPolicy{
		{
			Issuer: issuer,
			BoundClaims: map[string]string{
				"sub": sub,
				"aud": "phobos",
			},
		},
	}

	// Test cases
	tests := []struct {
		expectErrCode  errors.CodeType
		name           string
		serviceAccount string
		policy         []models.OIDCTrustPolicy
		token          []byte
	}{
		{
			name:           "create service account token with service account ID",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, sub, time.Now().Add(time.Minute)),
			policy:         basicPolicy,
		},
		{
			name:           "subject claim doesn't match",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, "invalidsubject", time.Now().Add(time.Minute)),
			policy:         basicPolicy,
			expectErrCode:  errors.EUnauthorized,
		},
		{
			name:           "expired token",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, sub, time.Now().Add(-time.Minute)),
			policy:         basicPolicy,
			expectErrCode:  errors.EUnauthorized,
		},
		{
			name:           "no matching trust policy",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, sub, time.Now().Add(time.Minute)),
			policy: []models.OIDCTrustPolicy{
				{
					Issuer:      "https://notavalidissuer",
					BoundClaims: map[string]string{},
				},
			},
			expectErrCode: errors.EUnauthorized,
		},
		{
			name:           "empty trust policy",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, sub, time.Now().Add(time.Minute)),
			policy:         []models.OIDCTrustPolicy{},
			expectErrCode:  errors.EUnauthorized,
		},
		{
			name:           "invalid token",
			serviceAccount: serviceAccountID,
			token:          []byte("invalidtoken"),
			policy:         basicPolicy,
			expectErrCode:  errors.EUnauthorized,
		},
		{
			name:           "missing issuer",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, "", sub, time.Now().Add(time.Minute)),
			policy:         basicPolicy,
			expectErrCode:  errors.EUnauthorized,
		},
		{
			name:           "invalid token signature",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, invalidKeyPair.priv, keyID, issuer, sub, time.Now().Add(time.Minute)),
			policy:         basicPolicy,
			expectErrCode:  errors.EUnauthorized,
		},
		{
			name:           "negative: multiple trust policies with same issuer: all mismatch",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, "noMatchSubject", time.Now().Add(time.Minute)),
			policy: []models.OIDCTrustPolicy{
				{
					Issuer:      issuer,
					BoundClaims: map[string]string{"sub": "firstSubject"},
				},
				{
					Issuer:      issuer,
					BoundClaims: map[string]string{"sub": "secondSubject"},
				},
			},
			expectErrCode: errors.EUnauthorized,
		},
		{
			name:           "positive: multiple trust policies with same issuer: match first",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, "firstSubject", time.Now().Add(time.Minute)),
			policy: []models.OIDCTrustPolicy{
				{
					Issuer:      issuer,
					BoundClaims: map[string]string{"sub": "firstSubject"},
				},
				{
					Issuer:      issuer,
					BoundClaims: map[string]string{"sub": "secondSubject"},
				},
			},
		},
		{
			name:           "positive: multiple trust policies with same issuer: match second",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, issuer, "secondSubject", time.Now().Add(time.Minute)),
			policy: []models.OIDCTrustPolicy{
				{
					Issuer:      issuer,
					BoundClaims: map[string]string{"sub": "firstSubject"},
				},
				{
					Issuer:      issuer,
					BoundClaims: map[string]string{"sub": "secondSubject"},
				},
			},
		},
		{
			name:           "positive: trust policy issuer has forward slash, token issuer does not",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, "https://test.phobos", sub, time.Now().Add(time.Minute)),
			policy: []models.OIDCTrustPolicy{
				{
					Issuer:      "https://test.phobos/",
					BoundClaims: map[string]string{},
				},
			},
		},
		{
			name:           "positive: token issuer has forward slash, trust policy issuer does not",
			serviceAccount: serviceAccountID,
			token:          createJWT(t, validKeyPair.priv, keyID, "https://test.phobos/", sub, time.Now().Add(time.Minute)),
			policy: []models.OIDCTrustPolicy{
				{
					Issuer:      "https://test.phobos",
					BoundClaims: map[string]string{},
				},
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			sa := models.ServiceAccount{
				Metadata:          models.ResourceMetadata{ID: serviceAccountID},
				Name:              "serviceAccount1",
				OIDCTrustPolicies: test.policy,
				OrganizationID:    &organizationID,
				Scope:             models.OrganizationScope,
			}

			mockServiceAccounts := db.MockServiceAccounts{}
			mockServiceAccounts.Test(t)

			mockServiceAccounts.On("GetServiceAccountByID", mock.Anything, test.serviceAccount).Return(&sa, nil)

			mockJWSProvider := jwsprovider.MockProvider{}
			mockJWSProvider.Test(t)

			mockJWSProvider.On("Sign", mock.Anything, mock.MatchedBy(func(payload []byte) bool {
				parsedToken, err := jwt.Parse(payload, jwt.WithVerify(false))
				if err != nil {
					t.Fatal(err)
				}
				if parsedToken.Subject() != sa.Metadata.ID {
					return false
				}
				privClaims := parsedToken.PrivateClaims()

				return privClaims["service_account_id"] == gid.ToGlobalID(gid.ServiceAccountType, sa.Metadata.ID) &&
					privClaims["service_account_scope"] == string(sa.Scope) &&
					privClaims["type"] == auth.ServiceAccountTokenType
			})).Return([]byte("signedtoken"), nil)

			dbClient := &db.Client{
				ServiceAccounts: &mockServiceAccounts,
			}

			serviceAccountAuth := auth.NewIdentityProvider(&mockJWSProvider, "https://phobos.io")

			configFetcher := auth.NewOpenIDConfigFetcher()

			getKeySetFunc := func(_ context.Context, _ string, _ *auth.OpenIDConfigFetcher) (jwk.Set, error) {
				set := jwk.NewSet()
				set.AddKey(validKeyPair.pub)
				return set, nil
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				dbClient:            dbClient,
				logger:              testLogger,
				idp:                 serviceAccountAuth,
				openIDConfigFetcher: configFetcher,
				getKeySetFunc:       getKeySetFunc,
			}

			resp, err := service.CreateToken(ctx, &CreateTokenInput{ServiceAccountID: serviceAccountID, Token: test.token})
			if err != nil && test.expectErrCode == "" {
				t.Fatal(err)
			}

			if test.expectErrCode == "" {
				expected := CreateTokenResponse{
					Token:     []byte("signedtoken"),
					ExpiresIn: int32(serviceAccountLoginDuration / time.Second),
				}

				assert.Equal(t, &expected, resp)
			} else {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
			}
		})
	}
}

func createKeyPair(t *testing.T) keyPair {
	rsaPrivKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Fatal(err)
	}

	privKey, err := jwk.FromRaw(rsaPrivKey)
	if err != nil {
		t.Fatal(err)
	}

	pubKey, err := jwk.FromRaw(rsaPrivKey.PublicKey)
	if err != nil {
		t.Fatal(err)
	}

	if err := jwk.AssignKeyID(pubKey); err != nil {
		t.Fatal(err)
	}

	return keyPair{priv: privKey, pub: pubKey}
}

func createJWT(t *testing.T, key jwk.Key, keyID string, issuer string, sub string, exp time.Time) []byte {
	token := jwt.New()

	_ = token.Set(jwt.ExpirationKey, exp.Unix())
	_ = token.Set(jwt.SubjectKey, sub)
	_ = token.Set(jwt.AudienceKey, "phobos")
	if issuer != "" {
		_ = token.Set(jwt.IssuerKey, issuer)
	}

	hdrs := jws.NewHeaders()
	_ = hdrs.Set(jws.TypeKey, "JWT")
	_ = hdrs.Set(jws.KeyIDKey, keyID)

	signed, err := jwt.Sign(token, jwt.WithKey(jwa.RS256, key, jws.WithProtectedHeaders(hdrs)))
	if err != nil {
		t.Fatal(err)
	}

	return signed
}
