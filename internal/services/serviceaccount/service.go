// Package serviceaccount package
package serviceaccount

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const (
	failedToVerifyJWSSignature = "failed to verify jws signature"
	expiredTokenDetector       = "failed to verify token exp not satisfied"
)

var (
	serviceAccountLoginDuration = 1 * time.Hour

	errFailedCreateToken = errors.New(
		"Failed to create service account token due to one of the "+
			"following reasons: the service account does not exist; the JWT token used as input is invalid; the issuer "+
			"for the token is not a valid issuer.",
		errors.WithErrorCode(errors.EUnauthorized),
	)

	errExpiredToken = errors.New(
		"failed to create service account token due to an expired token",
		errors.WithErrorCode(errors.EUnauthorized))
)

// GetServiceAccountsInput is the input for querying a list of service accounts
type GetServiceAccountsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ServiceAccountSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// AgentID will filter service accounts that are assigned to the specified agent
	AgentID *string
	// OrganizationID will filter service accounts that are assigned to the specified organization
	OrganizationID *string
	// ProjectID will filter service accounts that are assigned to the specified project
	ProjectID *string
	// Search will filter service accounts that match the specified search string
	Search *string
	// ServiceAccountScopes will filter service accounts that are scope to specific level
	ServiceAccountScopes []models.ScopeType
}

// CreateServiceAccountInput is the input for creating a service account
type CreateServiceAccountInput struct {
	Scope             models.ScopeType
	OrgID             *string
	ProjectID         *string
	Name              string
	Description       string
	OIDCTrustPolicies []models.OIDCTrustPolicy
}

// UpdateServiceAccountInput is the input for updating a service account
type UpdateServiceAccountInput struct {
	ID                string
	Description       *string
	MetadataVersion   *int
	OIDCTrustPolicies []models.OIDCTrustPolicy
}

// DeleteServiceAccountInput is the input for deleting a service account
type DeleteServiceAccountInput struct {
	MetadataVersion *int
	ID              string
}

// CreateTokenInput for logging into a service account
type CreateTokenInput struct {
	ServiceAccountID string
	Token            []byte
}

// CreateTokenResponse returned after logging into a service account
type CreateTokenResponse struct {
	Token     []byte
	ExpiresIn int32 // seconds
}

// Service implements all service account related functionality
type Service interface {
	GetServiceAccountByID(ctx context.Context, id string) (*models.ServiceAccount, error)
	GetServiceAccountByPRN(ctx context.Context, prn string) (*models.ServiceAccount, error)
	GetServiceAccounts(ctx context.Context, input *GetServiceAccountsInput) (*db.ServiceAccountsResult, error)
	GetServiceAccountsByIDs(ctx context.Context, idList []string) ([]*models.ServiceAccount, error)
	CreateServiceAccount(ctx context.Context, input *CreateServiceAccountInput) (*models.ServiceAccount, error)
	UpdateServiceAccount(ctx context.Context, input *UpdateServiceAccountInput) (*models.ServiceAccount, error)
	DeleteServiceAccount(ctx context.Context, input *DeleteServiceAccountInput) error
	CreateToken(ctx context.Context, input *CreateTokenInput) (*CreateTokenResponse, error)
}

type service struct {
	logger              logger.Logger
	dbClient            *db.Client
	limitChecker        limits.LimitChecker
	idp                 *auth.IdentityProvider
	openIDConfigFetcher *auth.OpenIDConfigFetcher
	getKeySetFunc       func(ctx context.Context, issuer string, configFetcher *auth.OpenIDConfigFetcher) (jwk.Set, error)
	activityService     activityevent.Service
}

// NewService creates an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	idp *auth.IdentityProvider,
	openIDConfigFetcher *auth.OpenIDConfigFetcher,
	activityService activityevent.Service,
) Service {
	return newService(
		logger,
		dbClient,
		limitChecker,
		idp,
		openIDConfigFetcher,
		getKeySet,
		activityService,
	)
}

func newService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	idp *auth.IdentityProvider,
	openIDConfigFetcher *auth.OpenIDConfigFetcher,
	getKeySetFunc func(ctx context.Context, issuer string, configFetcher *auth.OpenIDConfigFetcher) (jwk.Set, error),
	activityService activityevent.Service,
) Service {
	return &service{
		logger:              logger,
		dbClient:            dbClient,
		limitChecker:        limitChecker,
		idp:                 idp,
		openIDConfigFetcher: openIDConfigFetcher,
		getKeySetFunc:       getKeySetFunc,
		activityService:     activityService,
	}
}

func (s *service) GetServiceAccountByID(ctx context.Context, id string) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "svc.GetServiceAccountByID")
	span.SetAttributes(attribute.String("service account id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	serviceAccount, err := s.getServiceAccountByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	switch serviceAccount.Scope {
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewServiceAccount, auth.WithProjectID(*serviceAccount.ProjectID))
		if err != nil {
			return nil, err
		}
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.ServiceAccountResource, auth.WithOrganizationID(*serviceAccount.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.GlobalScope:
		if !caller.IsAdmin() {
			return nil, errors.New(
				"Only system admins can get global service accounts",
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	default:
		return nil, errors.New("invalid service account scope: %s", serviceAccount.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return serviceAccount, nil
}

func (s *service) GetServiceAccountByPRN(ctx context.Context, prn string) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "svc.GetServiceAccount")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	serviceAccount, err := s.dbClient.ServiceAccounts.GetServiceAccountByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get service account", errors.WithSpan(span))
	}

	if serviceAccount == nil {
		return nil, errors.New("service account not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	switch serviceAccount.Scope {
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewServiceAccount, auth.WithProjectID(*serviceAccount.ProjectID))
		if err != nil {
			return nil, err
		}
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.ServiceAccountResource, auth.WithOrganizationID(*serviceAccount.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.GlobalScope:
		if !caller.IsAdmin() {
			return nil, errors.New(
				"Only system admins can get global service accounts",
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	default:
		return nil, errors.New("invalid service account scope: %s", serviceAccount.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return serviceAccount, nil
}

func (s *service) GetServiceAccounts(ctx context.Context, input *GetServiceAccountsInput) (*db.ServiceAccountsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetServiceAccounts")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	filter := &db.ServiceAccountFilter{
		OrganizationID:       input.OrganizationID,
		ProjectID:            input.ProjectID,
		Search:               input.Search,
		ServiceAccountScopes: input.ServiceAccountScopes,
	}

	switch {
	case input.AgentID != nil:
		agent, aErr := s.dbClient.Agents.GetAgentByID(ctx, *input.AgentID)
		if aErr != nil {
			return nil, errors.Wrap(aErr, "failed to get agent", errors.WithSpan(span))
		}

		if agent == nil {
			return nil, errors.New("agent not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
		}

		filter.AgentID = &agent.Metadata.ID

		// Require permissions based on the agent type.
		if agent.Type.Equals(models.OrganizationAgent) {
			err = caller.RequireAccessToInheritableResource(ctx, models.ServiceAccountResource,
				auth.WithOrganizationID(*agent.OrganizationID))
			if err != nil {
				return nil, err
			}

		} else {
			if !caller.IsAdmin() {
				return nil, errors.New(
					"Only system admins can get global service accounts",
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EForbidden),
				)
			}
		}
	case input.OrganizationID != nil:
		err = caller.RequireAccessToInheritableResource(ctx, models.ServiceAccountResource,
			auth.WithOrganizationID(*input.OrganizationID))
		if err != nil {
			return nil, err
		}
	case input.ProjectID != nil:
		if pErr := caller.RequirePermission(ctx, models.ViewServiceAccount,
			auth.WithProjectID(*input.ProjectID)); pErr != nil {
			return nil, pErr
		}
	default:
		if !caller.IsAdmin() {
			return nil, errors.New(
				"Only system admins can get all service accounts",
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	}

	result, err := s.dbClient.ServiceAccounts.GetServiceAccounts(ctx, &db.GetServiceAccountsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter:            filter,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get service accounts", errors.WithSpan(span))
	}

	return result, nil
}

func (s *service) GetServiceAccountsByIDs(ctx context.Context, idList []string) ([]*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "svc.GetServiceAccountsByIDs")
	span.SetAttributes(attribute.StringSlice("serviceAccountIDs", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	result, err := s.dbClient.ServiceAccounts.GetServiceAccounts(ctx, &db.GetServiceAccountsInput{
		Filter: &db.ServiceAccountFilter{
			ServiceAccountIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get service accounts", errors.WithSpan(span))
	}

	for _, sa := range result.ServiceAccounts {
		switch sa.Scope {
		case models.GlobalScope:
			if !caller.IsAdmin() {
				return nil, errors.New(
					"Only system admins can get global service accounts",
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EForbidden),
				)
			}
		case models.OrganizationScope:
			err = caller.RequireAccessToInheritableResource(ctx, models.ServiceAccountResource, auth.WithOrganizationID(*sa.OrganizationID))
			if err != nil {
				return nil, err
			}
		case models.ProjectScope:
			err = caller.RequirePermission(ctx, models.ViewServiceAccount, auth.WithProjectID(*sa.ProjectID))
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("invalid service account scope: %s", sa.Scope,
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	return result.ServiceAccounts, nil
}

func (s *service) CreateServiceAccount(ctx context.Context, input *CreateServiceAccountInput) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateOrganizationServiceAccount")
	span.SetAttributes(attribute.String("name", input.Name))
	span.SetAttributes(attribute.String("scope", string(input.Scope)))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var orgID *string
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrgID == nil {
			return nil, errors.New("to create a service account of organization scope, must supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.ProjectID != nil {
			return nil, errors.New("to create a service account of organization scope, not allowed to supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		orgID = input.OrgID
		err = caller.RequirePermission(ctx, models.CreateServiceAccount, auth.WithOrganizationID(*orgID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("to create a service account of project scope, must supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.OrgID != nil {
			return nil, errors.New("to create a service account of project scope, not allowed to supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		projectID := input.ProjectID
		err = caller.RequirePermission(ctx, models.CreateServiceAccount, auth.WithProjectID(*projectID))
		if err != nil {
			return nil, err
		}

		// Get the project in order to have the organization ID.
		project, pErr := s.dbClient.Projects.GetProjectByID(ctx, *projectID)
		if pErr != nil {
			return nil, pErr
		}
		if project == nil {
			return nil, errors.New("failed to find project with ID %s", *projectID, errors.WithSpan(span))
		}

		orgID = &project.OrgID
	case models.GlobalScope:
		if input.ProjectID != nil {
			return nil, errors.New("to create a service account of global scope, not allowed to supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.OrgID != nil {
			return nil, errors.New("to create a service account of global scope, not allowed to supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if !caller.IsAdmin() {
			return nil, errors.New(
				"Only system admins can create global service accounts",
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	default:
		return nil, errors.New("not allowed to create a service account with %s scope",
			input.Scope, errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	toCreate := &models.ServiceAccount{
		Name:              input.Name,
		Scope:             input.Scope,
		OrganizationID:    orgID,
		ProjectID:         input.ProjectID,
		Description:       input.Description,
		CreatedBy:         caller.GetSubject(),
		OIDCTrustPolicies: input.OIDCTrustPolicies,
	}

	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate service account model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateServiceAccount: %v", txErr)
		}
	}()

	createdServiceAccount, err := s.dbClient.ServiceAccounts.CreateServiceAccount(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create service account", errors.WithSpan(span))
	}

	// Check limit if an org service account.
	if input.Scope.Equals(models.OrganizationScope) {
		newServiceAccounts, sErr := s.dbClient.ServiceAccounts.GetServiceAccounts(txContext, &db.GetServiceAccountsInput{
			Filter: &db.ServiceAccountFilter{
				OrganizationID:       orgID,
				ServiceAccountScopes: []models.ScopeType{models.OrganizationScope},
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
		})
		if sErr != nil {
			return nil, errors.Wrap(sErr, "failed to get organization's service accounts", errors.WithSpan(span))
		}

		if err = s.limitChecker.CheckLimit(
			txContext,
			limits.ResourceLimitServiceAccountsPerOrganization,
			newServiceAccounts.PageInfo.TotalCount,
		); err != nil {
			return nil, err
		}
	}

	// Check limit if a project service account.
	if input.Scope.Equals(models.ProjectScope) {
		newServiceAccounts, sErr := s.dbClient.ServiceAccounts.GetServiceAccounts(txContext, &db.GetServiceAccountsInput{
			Filter: &db.ServiceAccountFilter{
				ProjectID:            createdServiceAccount.ProjectID,
				ServiceAccountScopes: []models.ScopeType{models.ProjectScope},
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
		})
		if sErr != nil {
			return nil, errors.Wrap(sErr, "failed to get project's service accounts", errors.WithSpan(span))
		}

		if err = s.limitChecker.CheckLimit(
			txContext,
			limits.ResourceLimitServiceAccountsPerProject,
			newServiceAccounts.PageInfo.TotalCount,
		); err != nil {
			return nil, err
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: createdServiceAccount.OrganizationID,
			ProjectID:      createdServiceAccount.ProjectID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetServiceAccount,
			TargetID:       &createdServiceAccount.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a service account.",
		"caller", caller.GetSubject(),
		"serviceAccountName", createdServiceAccount.Name,
		"scope", createdServiceAccount.Scope,
	)

	return createdServiceAccount, nil
}

func (s *service) UpdateServiceAccount(ctx context.Context, input *UpdateServiceAccountInput) (*models.ServiceAccount, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateServiceAccount")
	span.SetAttributes(attribute.String("service account id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	serviceAccount, err := s.getServiceAccountByID(ctx, span, input.ID)
	if err != nil {
		return nil, err
	}

	switch serviceAccount.Scope {
	case models.OrganizationScope:
		err = caller.RequirePermission(ctx, models.UpdateServiceAccount,
			auth.WithOrganizationID(*serviceAccount.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.UpdateServiceAccount,
			auth.WithProjectID(*serviceAccount.ProjectID))
		if err != nil {
			return nil, err
		}
	case models.GlobalScope:
		if !caller.IsAdmin() {
			return nil, errors.New(
				"Only system admins can update global service accounts",
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	default:
		return nil, errors.New("invalid service account scope: %s", serviceAccount.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if input.Description != nil {
		serviceAccount.Description = *input.Description
	}

	if input.MetadataVersion != nil {
		serviceAccount.Metadata.Version = *input.MetadataVersion
	}

	if len(input.OIDCTrustPolicies) > 0 {
		serviceAccount.OIDCTrustPolicies = input.OIDCTrustPolicies
	}

	if err = serviceAccount.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate service account model", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateServiceAccount: %v", txErr)
		}
	}()

	updatedServiceAccount, err := s.dbClient.ServiceAccounts.UpdateServiceAccount(txContext, serviceAccount)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: updatedServiceAccount.OrganizationID,
			ProjectID:      updatedServiceAccount.ProjectID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetServiceAccount,
			TargetID:       &updatedServiceAccount.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a service account.",
		"caller", caller.GetSubject(),
		"serviceAccountID", serviceAccount.Metadata.ID,
		"scope", serviceAccount.Scope,
	)

	return updatedServiceAccount, nil
}

func (s *service) DeleteServiceAccount(ctx context.Context, input *DeleteServiceAccountInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteServiceAccount")
	span.SetAttributes(attribute.String("service account id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	serviceAccount, err := s.getServiceAccountByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	switch serviceAccount.Scope {
	case models.OrganizationScope:
		err = caller.RequirePermission(ctx, models.DeleteServiceAccount,
			auth.WithOrganizationID(*serviceAccount.OrganizationID))
		if err != nil {
			return err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.DeleteServiceAccount,
			auth.WithProjectID(*serviceAccount.ProjectID))
		if err != nil {
			return err
		}
	case models.GlobalScope:
		if !caller.IsAdmin() {
			return errors.New(
				"Only system admins can delete global service accounts",
				errors.WithSpan(span),
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	default:
		return errors.New("invalid service account scope: %s", serviceAccount.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if input.MetadataVersion != nil {
		serviceAccount.Metadata.Version = *input.MetadataVersion
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteServiceAccount: %v", txErr)
		}
	}()

	err = s.dbClient.ServiceAccounts.DeleteServiceAccount(txContext, serviceAccount)
	if err != nil {
		return err
	}

	var targetType models.ActivityEventTargetType
	var targetID *string
	switch serviceAccount.Scope {
	case models.ProjectScope:
		targetType = models.TargetProject
		targetID = serviceAccount.ProjectID
	case models.OrganizationScope:
		targetType = models.TargetOrganization
		targetID = serviceAccount.OrganizationID
	case models.GlobalScope:
		targetType = models.TargetGlobal
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: serviceAccount.OrganizationID,
			ProjectID:      serviceAccount.ProjectID,
			Action:         models.ActionDelete,
			TargetType:     targetType,
			TargetID:       targetID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &serviceAccount.Name,
				ID:   serviceAccount.Metadata.ID,
				Type: models.TargetServiceAccount.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a service account.",
		"caller", caller.GetSubject(),
		"serviceAccountID", serviceAccount.Metadata.ID,
		"scope", serviceAccount.Scope,
	)

	return nil
}

func (s *service) CreateToken(ctx context.Context, input *CreateTokenInput) (*CreateTokenResponse, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateToken")
	span.SetAttributes(attribute.String("service account id", input.ServiceAccountID))
	defer span.End()

	// Parse token
	token, err := jwt.Parse(input.Token, jwt.WithVerify(false))
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode token", errors.WithErrorCode(errors.EUnauthorized), errors.WithSpan(span))
	}

	// Check if token is from a valid issuer associated with the service account
	issuer := token.Issuer()
	if issuer == "" {
		return nil, errors.New("JWT is missing issuer claim", errors.WithErrorCode(errors.EUnauthorized), errors.WithSpan(span))
	}

	// Get service account
	serviceAccount, err := s.getServiceAccountByID(ctx, span, input.ServiceAccountID)
	if err != nil {
		s.logger.Infof("Failed to create token for service account; service account %s does not exist", input.ServiceAccountID)
		return nil, errFailedCreateToken
	}

	trustPolicies := s.findMatchingTrustPolicies(issuer, serviceAccount.OIDCTrustPolicies)
	if len(trustPolicies) == 0 {
		s.logger.Infof("Failed to create token for service account %s; issuer %s not found in trust policy", serviceAccount.Metadata.ID, issuer)
		return nil, errors.Wrap(
			errFailedCreateToken,
			"failed to create token for service account; issuer not found in trust policy",
			errors.WithSpan(span),
			errors.WithErrorCode(errors.EUnauthorized),
		)
	}

	// One satisfied trust policy is sufficient for service account token creation.
	// However, must keep all the failures incase everything fails.
	mismatchesFound := []string{}
	for _, trustPolicy := range trustPolicies {
		err := s.verifyOneTrustPolicy(ctx, input.Token, trustPolicy)
		if err != nil {

			// Catch bubbled-up invalid token signature errors here.
			if strings.Contains(err.Error(), failedToVerifyJWSSignature) {
				s.logger.Infof("Failed to create token for service account %s due to invalid token signature",
					serviceAccount.Metadata.ID)
				return nil, errors.Wrap(
					errFailedCreateToken,
					"failed to create token for service account; invalid token signature",
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EUnauthorized),
				)
			}

			// Catch token expiration here.  An expired token will be expired for all trust policies.
			if strings.Contains(err.Error(), expiredTokenDetector) {
				s.logger.Infof("Failed to create token for service account %s due to expired token",
					serviceAccount.Metadata.ID)
				return nil, errors.Wrap(
					errExpiredToken,
					"failed to create token for service account; expired token",
					errors.WithSpan(span),
					errors.WithErrorCode(errors.EUnauthorized),
				)
			}

			// Record this claim mismatch in case no other, later trust policy is satisfied
			mismatchesFound = append(mismatchesFound, err.Error())
		} else {
			// The input token satisfied this trust policy, so the service account token creation succeeded.

			// Generate service account token
			expiration := time.Now().Add(serviceAccountLoginDuration)
			serviceAccountToken, err := s.idp.GenerateToken(ctx, &auth.TokenInput{
				Expiration: &expiration,
				Subject:    serviceAccount.Metadata.ID,
				Audience:   "phobos",
				Claims: map[string]interface{}{
					"service_account_id":    gid.ToGlobalID(gid.ServiceAccountType, serviceAccount.Metadata.ID),
					"service_account_prn":   serviceAccount.Metadata.PRN,
					"service_account_scope": string(serviceAccount.Scope),
					"type":                  auth.ServiceAccountTokenType,
				},
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to generate token for service account", errors.WithSpan(span))
			}

			return &CreateTokenResponse{
				Token:     serviceAccountToken,
				ExpiresIn: int32(serviceAccountLoginDuration / time.Second),
			}, nil
		}
	}

	// Log all the mismatches found so we can look them up if needed.
	s.logger.Infof("failed to create service account token for issuer %s; %s", issuer, strings.Join(mismatchesFound, "; "))

	// We know there was at least one trust policy checked, otherwise we would have returned before the for loop.
	// To get here, all of the trust policies that were checked must have failed.
	return nil, errors.New("of the trust policies for issuer %s, none was satisfied", issuer, errors.WithErrorCode(errors.EUnauthorized), errors.WithSpan(span))
}

// findMatchingTrustPolicies returns a slice of the policies that have a matching issuer.
// If no match is found, it returns an empty slice.
// Trailing forward slashes are ignored on both sides of the comparison.
// Claims are not checked.
func (s *service) findMatchingTrustPolicies(issuer string, policies []models.OIDCTrustPolicy) []models.OIDCTrustPolicy {
	result := []models.OIDCTrustPolicy{}
	normalizedIssuer := issuer
	if !strings.HasPrefix(issuer, "https://") {
		normalizedIssuer = fmt.Sprintf("https://%s", issuer)
	}

	normalizedIssuer = strings.TrimSuffix(normalizedIssuer, "/")
	for _, p := range policies {
		if normalizedIssuer == strings.TrimSuffix(p.Issuer, "/") {
			result = append(result, p)
		}
	}

	return result
}

// verifyOneTrustPolicy verifies a token vs. one trust policy.
func (s *service) verifyOneTrustPolicy(ctx context.Context, inputToken []byte, trustPolicy models.OIDCTrustPolicy) error {
	// Get issuer JWK response
	keySet, err := s.getKeySetFunc(ctx, trustPolicy.Issuer, s.openIDConfigFetcher)
	if err != nil {
		return err
	}

	// Set default key to RS256 if it's not specified in JWK set
	iter := keySet.Keys(ctx)
	for iter.Next(ctx) {
		key := iter.Pair().Value.(jwk.Key)
		if err = key.Set(jwk.AlgorithmKey, jwa.RS256); err != nil {
			return err
		}
	}

	options := []jwt.ParseOption{
		jwt.WithVerify(true),
		jwt.WithKeySet(keySet),
		jwt.WithValidate(true),
	}

	for k, v := range trustPolicy.BoundClaims {
		options = append(options, jwt.WithValidator(newClaimValueValidator(k, v, trustPolicy.BoundClaimsType == models.BoundClaimsTypeGlob)))
	}

	// Parse and Verify token
	if _, err := jwt.Parse(inputToken, options...); err != nil {
		return fmt.Errorf("failed to verify token %w", err)
	}

	return nil
}

func (s *service) getServiceAccountByID(ctx context.Context, span trace.Span, id string) (*models.ServiceAccount, error) {
	serviceAccount, err := s.dbClient.ServiceAccounts.GetServiceAccountByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get service account by ID", errors.WithSpan(span))
	}

	if serviceAccount == nil {
		return nil, errors.New("service account with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return serviceAccount, nil
}

func getKeySet(ctx context.Context, issuer string, configFetcher *auth.OpenIDConfigFetcher) (jwk.Set, error) {
	fetchCtx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	oidcConfig, err := configFetcher.GetOpenIDConfig(fetchCtx, issuer)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get OIDC discovery document for issuer %s", issuer)
	}

	fetchCtx, cancel = context.WithTimeout(ctx, 30*time.Second)
	defer cancel()

	// Get issuer JWK response
	keySet, err := jwk.Fetch(fetchCtx, oidcConfig.JwksURI)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to query JWK URL %s", oidcConfig.JwksURI)
	}

	return keySet, nil
}
