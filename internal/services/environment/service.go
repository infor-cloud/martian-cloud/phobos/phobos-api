// Package environment contains all functionalities related to Phobos environments.
package environment

//go:generate go tool mockery --name Service --inpackage --case underscore

import (
	"context"
	"strings"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetEnvironmentsInput is the input for querying a list of environments
type GetEnvironmentsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.EnvironmentSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// ProjectID is the project to return environments for
	ProjectID *string
	// OrganizationID is the organization to return environments for
	OrganizationID *string
}

// CreateEnvironmentInput is the input for creating a new environment
type CreateEnvironmentInput struct {
	ProjectID   string
	Name        string
	Description string
}

// UpdateEnvironmentInput is the input for updating an existing environment
type UpdateEnvironmentInput struct {
	Description     *string
	MetadataVersion *int
	ID              string
}

// DeleteEnvironmentInput is the input for deleting an existing environment
type DeleteEnvironmentInput struct {
	MetadataVersion *int
	ID              string
}

// GetEnvironmentRulesInput is the input for querying a list of environment protection rules
type GetEnvironmentRulesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.EnvironmentRuleSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// OrganizationID filters the environment protection rules by the specified organization.
	OrganizationID *string
	// ProjectID filters the environment protection rules by the specified project.
	ProjectID *string
	// EnvironmentName filters the environment protection rules by the specified environment.
	EnvironmentName *string
}

// CreateOrganizationEnvironmentRuleInput is the input for creating an environment protection rule
type CreateOrganizationEnvironmentRuleInput struct {
	OrganizationID    string
	EnvironmentName   string
	UserIDs           []string
	TeamIDs           []string
	ServiceAccountIDs []string
	RoleIDs           []string
}

// CreateProjectEnvironmentRuleInput is the input for creating an environment protection rule
type CreateProjectEnvironmentRuleInput struct {
	ProjectID         string
	EnvironmentName   string
	UserIDs           []string
	TeamIDs           []string
	ServiceAccountIDs []string
	RoleIDs           []string
}

// UpdateEnvironmentRuleInput is the input for updating an environment protection rule
type UpdateEnvironmentRuleInput struct {
	Version           *int
	ID                string
	UserIDs           []string
	TeamIDs           []string
	ServiceAccountIDs []string
	RoleIDs           []string
}

// DeleteEnvironmentRuleInput is the input for deleting an environment protection rule
type DeleteEnvironmentRuleInput struct {
	Version *int
	ID      string
}

// Service implements all environment related functionality
type Service interface {
	GetEnvironmentByID(ctx context.Context, id string) (*models.Environment, error)
	GetEnvironmentByPRN(ctx context.Context, prn string) (*models.Environment, error)
	GetEnvironmentsByIDs(ctx context.Context, idList []string) ([]models.Environment, error)
	GetEnvironments(ctx context.Context, input *GetEnvironmentsInput) (*db.EnvironmentsResult, error)
	CreateEnvironment(ctx context.Context, input *CreateEnvironmentInput) (*models.Environment, error)
	UpdateEnvironment(ctx context.Context, input *UpdateEnvironmentInput) (*models.Environment, error)
	DeleteEnvironment(ctx context.Context, input *DeleteEnvironmentInput) error
	GetEnvironmentRuleByID(ctx context.Context, id string) (*models.EnvironmentRule, error)
	GetEnvironmentRuleByPRN(ctx context.Context, prn string) (*models.EnvironmentRule, error)
	GetEnvironmentRules(ctx context.Context, input *GetEnvironmentRulesInput) (*db.EnvironmentRulesResult, error)
	GetEnvironmentRulesByIDs(ctx context.Context, ids []string) ([]*models.EnvironmentRule, error)
	CreateOrganizationEnvironmentRule(ctx context.Context,
		input *CreateOrganizationEnvironmentRuleInput) (*models.EnvironmentRule, error)
	CreateProjectEnvironmentRule(ctx context.Context,
		input *CreateProjectEnvironmentRuleInput) (*models.EnvironmentRule, error)
	UpdateEnvironmentRule(ctx context.Context,
		input *UpdateEnvironmentRuleInput) (*models.EnvironmentRule, error)
	DeleteEnvironmentRule(ctx context.Context, input *DeleteEnvironmentRuleInput) error
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	limitChecker    limits.LimitChecker
	activityService activityevent.Service
}

// NewService creates an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		activityService: activityService,
	}
}

func (s *service) GetEnvironmentByID(ctx context.Context, id string) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironmentByID")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	environment, err := s.getEnvironmentByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	// Every environment is tied to a specific project, so check permissions based on that project.
	err = caller.RequirePermission(ctx, models.ViewEnvironment, auth.WithProjectID(environment.ProjectID))
	if err != nil {
		return nil, err
	}

	return environment, nil
}

func (s *service) GetEnvironmentByPRN(ctx context.Context, prn string) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironmentByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	environment, err := s.dbClient.Environments.GetEnvironmentByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environment by PRN", errors.WithSpan(span))
	}

	if environment == nil {
		return nil, errors.New("environment with PRN %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	// Every environment is tied to a specific project, so check permissions based on that project.
	err = caller.RequirePermission(ctx, models.ViewEnvironment, auth.WithProjectID(environment.ProjectID))
	if err != nil {
		return nil, err
	}

	return environment, nil
}

func (s *service) GetEnvironmentsByIDs(ctx context.Context, idList []string) ([]models.Environment, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironmentsByIDs")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	result, err := s.dbClient.Environments.GetEnvironments(ctx, &db.GetEnvironmentsInput{
		Filter: &db.EnvironmentFilter{
			EnvironmentIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environments", errors.WithSpan(span))
	}

	// Every environment is tied to a specific project, so check permissions based on that project.
	for _, env := range result.Environments {
		err = caller.RequirePermission(ctx, models.ViewEnvironment, auth.WithProjectID(env.ProjectID))
		if err != nil {
			return nil, err
		}
	}

	return result.Environments, nil
}

func (s *service) GetEnvironments(ctx context.Context, input *GetEnvironmentsInput) (*db.EnvironmentsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironments")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	switch {
	case input.ProjectID != nil:
		if err = caller.RequirePermission(ctx, models.ViewEnvironment, auth.WithProjectID(*input.ProjectID)); err != nil {
			return nil, err
		}
	case input.OrganizationID != nil:
		if err = caller.RequirePermission(ctx, models.ViewEnvironment, auth.WithOrganizationID(*input.OrganizationID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("either projectID or organizationID must be provided", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	result, err := s.dbClient.Environments.GetEnvironments(ctx, &db.GetEnvironmentsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.EnvironmentFilter{
			ProjectID:      input.ProjectID,
			OrganizationID: input.OrganizationID,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environments", errors.WithSpan(span))
	}

	return result, nil
}

func (s *service) CreateEnvironment(ctx context.Context, input *CreateEnvironmentInput) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateEnvironment")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CreateEnvironment, auth.WithProjectID(input.ProjectID))
	if err != nil {
		return nil, err
	}

	environmentToCreate := &models.Environment{
		Name:        input.Name,
		Description: input.Description,
		ProjectID:   input.ProjectID,
		CreatedBy:   caller.GetSubject(),
	}

	// Validate model
	if err = environmentToCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate environment model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateEnvironment: %v", txErr)
		}
	}()

	// Store environment in DB
	createdEnvironment, err := s.dbClient.Environments.CreateEnvironment(txContext, environmentToCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create environment", errors.WithSpan(span))
	}

	// Get the number of environments for the project to check whether we just violated the limit.
	newEnvironments, err := s.dbClient.Environments.GetEnvironments(txContext, &db.GetEnvironmentsInput{
		Filter: &db.EnvironmentFilter{
			ProjectID: &createdEnvironment.ProjectID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project's environments", errors.WithSpan(span))
	}
	if err = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitEnvironmentsPerProject,
		newEnvironments.PageInfo.TotalCount); err != nil {
		return nil, errors.Wrap(err, "limit check failed", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &createdEnvironment.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetEnvironment,
			TargetID:   &createdEnvironment.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created an environment.",
		"caller", caller.GetSubject(),
		"projectID", input.ProjectID,
		"environmentName", input.Name,
	)

	return createdEnvironment, nil
}

func (s *service) UpdateEnvironment(ctx context.Context, input *UpdateEnvironmentInput) (*models.Environment, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateEnvironment")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	environment, err := s.getEnvironmentByID(ctx, span, input.ID)
	if err != nil {
		return nil, err
	}

	// Every environment is tied to a specific project, so check permissions based on that project.
	err = caller.RequirePermission(ctx, models.UpdateEnvironment, auth.WithProjectID(environment.ProjectID))
	if err != nil {
		return nil, err
	}

	if input.Description != nil {
		environment.Description = *input.Description
	}

	if input.MetadataVersion != nil {
		environment.Metadata.Version = *input.MetadataVersion
	}

	// Validate model
	if err = environment.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate environment model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateEnvironment: %v", txErr)
		}
	}()

	updatedEnvironment, err := s.dbClient.Environments.UpdateEnvironment(txContext, environment)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &updatedEnvironment.ProjectID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetEnvironment,
			TargetID:   &updatedEnvironment.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated an environment.",
		"caller", caller.GetSubject(),
		"environmentName", environment.Name,
		"environmentID", environment.Metadata.ID,
	)

	return updatedEnvironment, nil
}

func (s *service) DeleteEnvironment(ctx context.Context, input *DeleteEnvironmentInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteEnvironment")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	environment, err := s.getEnvironmentByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	// Every environment is tied to a specific project, so check permissions based on that project.
	err = caller.RequirePermission(ctx, models.DeleteEnvironment, auth.WithProjectID(environment.ProjectID))
	if err != nil {
		return err
	}

	if input.MetadataVersion != nil {
		environment.Metadata.Version = *input.MetadataVersion
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteEnvironment: %v", txErr)
		}
	}()

	err = s.dbClient.Environments.DeleteEnvironment(txContext, environment)
	if err != nil {
		return err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &environment.ProjectID,
			Action:     models.ActionDelete,
			TargetType: models.TargetProject,
			TargetID:   &environment.ProjectID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &environment.Name,
				ID:   environment.Metadata.ID,
				Type: models.TargetEnvironment.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted an environment.",
		"caller", caller.GetSubject(),
		"environmentName", environment.Name,
		"environmentID", environment.Metadata.ID,
	)

	return nil
}

func (s *service) getEnvironmentByID(ctx context.Context, span trace.Span, id string) (*models.Environment, error) {
	environment, err := s.dbClient.Environments.GetEnvironmentByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environment by ID", errors.WithSpan(span))
	}

	if environment == nil {
		return nil, errors.New("environment with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return environment, nil
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Environment protection rules:

func (s *service) GetEnvironmentRuleByID(ctx context.Context, id string) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironmentRuleByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	rule, err := s.getEnvironmentRuleByID(ctx, id, span)
	if err != nil {
		return nil, err
	}

	switch rule.Scope {
	case models.OrganizationScope:
		if err := caller.RequireAccessToInheritableResource(ctx, models.EnvironmentRuleResource,
			auth.WithOrganizationID(rule.OrgID)); err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if err := caller.RequirePermission(ctx, models.ViewEnvironmentRule,
			auth.WithProjectID(*rule.ProjectID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unexpected environment protection rule scope %s", rule.Scope)
	}

	return rule, nil
}

func (s *service) GetEnvironmentRuleByPRN(ctx context.Context, prn string) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironmentRuleByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	rule, err := s.dbClient.EnvironmentRules.GetEnvironmentRuleByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environment protection rule", errors.WithSpan(span))
	}

	if rule == nil {
		return nil, errors.New("environment protection rule not found",
			errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	switch rule.Scope {
	case models.OrganizationScope:
		if err := caller.RequireAccessToInheritableResource(ctx, models.EnvironmentRuleResource,
			auth.WithOrganizationID(rule.OrgID)); err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if err := caller.RequirePermission(ctx, models.ViewEnvironmentRule,
			auth.WithProjectID(*rule.ProjectID)); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unexpected environment protection rule scope %s", rule.Scope)
	}

	return rule, nil
}

func (s *service) GetEnvironmentRules(ctx context.Context,
	input *GetEnvironmentRulesInput,
) (*db.EnvironmentRulesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironmentRules")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetEnvironmentRulesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.EnvironmentRuleFilter{
			EnvironmentName: input.EnvironmentName,
		},
	}

	switch {
	case input.OrganizationID != nil:
		if err := caller.RequireAccessToInheritableResource(ctx, models.EnvironmentRuleResource,
			auth.WithOrganizationID(*input.OrganizationID)); err != nil {
			return nil, err
		}

		// Filter to just organization environment protection rule scopes.
		dbInput.Filter.OrgID = input.OrganizationID
		dbInput.Filter.EnvironmentRuleScopes = []models.ScopeType{models.OrganizationScope}
	case input.ProjectID != nil:
		if err := caller.RequirePermission(ctx, models.ViewEnvironmentRule,
			auth.WithProjectID(*input.ProjectID)); err != nil {
			return nil, err
		}

		// Include only project environment protection rules.
		dbInput.Filter.ProjectID = input.ProjectID
		dbInput.Filter.EnvironmentRuleScopes = []models.ScopeType{models.ProjectScope}
	default:
		return nil, errors.New("either an organization or project id must be specified",
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	return s.dbClient.EnvironmentRules.GetEnvironmentRules(ctx, dbInput)
}

func (s *service) GetEnvironmentRulesByIDs(ctx context.Context, idList []string) ([]*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "svc.GetEnvironmentRulesByIDs")
	span.SetAttributes(attribute.StringSlice("idList", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	result, err := s.dbClient.EnvironmentRules.GetEnvironmentRules(ctx, &db.GetEnvironmentRulesInput{
		Filter: &db.EnvironmentRuleFilter{
			EnvironmentRuleIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environment protection rules", errors.WithSpan(span))
	}

	for _, r := range result.EnvironmentRules {
		switch r.Scope {
		case models.OrganizationScope:
			if err := caller.RequireAccessToInheritableResource(ctx,
				models.EnvironmentRuleResource, auth.WithOrganizationID(r.OrgID)); err != nil {
				return nil, err
			}
		case models.ProjectScope:
			if err := caller.RequirePermission(ctx,
				models.ViewEnvironmentRule, auth.WithProjectID(*r.ProjectID)); err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("unexpected environment protection rule scope %s", r.Scope)
		}
	}

	return result.EnvironmentRules, nil
}

func (s *service) CreateOrganizationEnvironmentRule(
	ctx context.Context,
	input *CreateOrganizationEnvironmentRuleInput,
) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateOrganizationEnvironmentRule")
	span.SetAttributes(attribute.String("organizationId", input.OrganizationID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(
		ctx,
		models.CreateEnvironmentRule,
		auth.WithOrganizationID(input.OrganizationID),
	); err != nil {
		return nil, err
	}

	toCreate := &models.EnvironmentRule{
		Scope:             models.OrganizationScope,
		OrgID:             input.OrganizationID,
		EnvironmentName:   input.EnvironmentName,
		UserIDs:           input.UserIDs,
		TeamIDs:           input.TeamIDs,
		ServiceAccountIDs: input.ServiceAccountIDs,
		RoleIDs:           input.RoleIDs,
	}

	createdEnvironmentRule, err := s.createEnvironmentRule(ctx, span, toCreate,
		&db.EnvironmentRuleFilter{
			OrgID:                 &input.OrganizationID,
			EnvironmentRuleScopes: []models.ScopeType{models.OrganizationScope},
		},
		limits.ResourceLimitEnvironmentRulesPerOrganization,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &input.OrganizationID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetEnvironmentRule,
			// TargetID will be assigned inside the function.
		},
	)
	if err != nil {
		return nil, err
	}

	s.logger.Infow("Created an environment protection rule.",
		"caller", caller.GetSubject(),
		"organizationId", input.OrganizationID,
		"environmentName", input.EnvironmentName,
	)

	return createdEnvironmentRule, nil
}

func (s *service) CreateProjectEnvironmentRule(
	ctx context.Context,
	input *CreateProjectEnvironmentRuleInput,
) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateProjectEnvironmentRule")
	span.SetAttributes(attribute.String("projectId", input.ProjectID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if err = caller.RequirePermission(ctx, models.CreateEnvironmentRule, auth.WithProjectID(input.ProjectID)); err != nil {
		return nil, err
	}

	// We'll need to fetch the project to have the organization id.
	project, err := s.dbClient.Projects.GetProjectByID(ctx, input.ProjectID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project by id", errors.WithSpan(span))
	}

	if project == nil {
		return nil, errors.New("project with id %s not found", input.ProjectID, errors.WithSpan(span))
	}

	toCreate := &models.EnvironmentRule{
		Scope:             models.ProjectScope,
		OrgID:             project.OrgID,
		ProjectID:         &input.ProjectID,
		EnvironmentName:   input.EnvironmentName,
		UserIDs:           input.UserIDs,
		TeamIDs:           input.TeamIDs,
		ServiceAccountIDs: input.ServiceAccountIDs,
		RoleIDs:           input.RoleIDs,
	}

	createdEnvironmentRule, err := s.createEnvironmentRule(ctx, span, toCreate,
		&db.EnvironmentRuleFilter{
			ProjectID:             &input.ProjectID,
			EnvironmentRuleScopes: []models.ScopeType{models.ProjectScope},
		},
		limits.ResourceLimitEnvironmentsPerProject,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &input.ProjectID,
			Action:     models.ActionCreate,
			TargetType: models.TargetEnvironmentRule,
			// TargetID will be assigned inside the function.
		},
	)
	if err != nil {
		return nil, err
	}

	s.logger.Infow("Created an environment protection rule.",
		"caller", caller.GetSubject(),
		"projectId", input.ProjectID,
		"environmentName", input.EnvironmentName,
	)

	return createdEnvironmentRule, nil
}

func (s *service) createEnvironmentRule(
	ctx context.Context,
	span trace.Span,
	toCreate *models.EnvironmentRule,
	getRulesFilter *db.EnvironmentRuleFilter,
	limitName limits.ResourceLimitName,
	activityEventInput *activityevent.CreateActivityEventInput,
) (*models.EnvironmentRule, error) {

	if err := toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate environment protection rule model", errors.WithSpan(span))
	}

	if err := s.verifyServiceAccountsInOrg(ctx, toCreate.OrgID, toCreate.ServiceAccountIDs); err != nil {
		return nil, err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer createEnvironmentRule: %v", txErr)
		}
	}()

	createdEnvironmentRule, err := s.dbClient.EnvironmentRules.CreateEnvironmentRule(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create environment protection rule", errors.WithSpan(span))
	}

	// Get the number of environment protection rules in the organization
	// or project to check whether we just violated the limit.
	newEnvironmentRules, err := s.dbClient.EnvironmentRules.GetEnvironmentRules(txContext, &db.GetEnvironmentRulesInput{
		Filter: getRulesFilter,
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environment protection rules", errors.WithSpan(span))
	}

	if err = s.limitChecker.CheckLimit(
		txContext,
		limitName,
		newEnvironmentRules.PageInfo.TotalCount,
	); err != nil {
		return nil, errors.Wrap(err, "failed to check limit", errors.WithSpan(span))
	}

	activityEventInput.TargetID = &createdEnvironmentRule.Metadata.ID
	if _, err = s.activityService.CreateActivityEvent(txContext, activityEventInput); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return createdEnvironmentRule, nil
}

func (s *service) UpdateEnvironmentRule(ctx context.Context,
	input *UpdateEnvironmentRuleInput) (*models.EnvironmentRule, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateEnvironmentRule")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	rule, err := s.getEnvironmentRuleByID(ctx, input.ID, span)
	if err != nil {
		return nil, err
	}

	var (
		targetType     models.ActivityEventTargetType
		targetID       *string
		organizationID *string // Only needed for org environment protection rules.
	)

	switch rule.Scope {
	case models.OrganizationScope:
		if err = caller.RequirePermission(ctx, models.UpdateEnvironmentRule, auth.WithOrganizationID(rule.OrgID)); err != nil {
			return nil, err
		}

		organizationID = &rule.OrgID
		targetID = &rule.OrgID
		targetType = models.TargetOrganization
	case models.ProjectScope:
		if err = caller.RequirePermission(ctx, models.UpdateEnvironmentRule, auth.WithProjectID(*rule.ProjectID)); err != nil {
			return nil, err
		}

		targetID = rule.ProjectID
		targetType = models.TargetProject
	default:
		return nil, errors.New("unexpected environment protection rule scope %s", rule.Scope, errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateEnvironmentRule: %v", txErr)
		}
	}()

	// Conditionally modify the rule's users, etc.
	if input.UserIDs != nil {
		rule.UserIDs = input.UserIDs
	}
	if input.TeamIDs != nil {
		rule.TeamIDs = input.TeamIDs
	}
	if input.ServiceAccountIDs != nil {
		rule.ServiceAccountIDs = input.ServiceAccountIDs
	}
	if input.RoleIDs != nil {
		rule.RoleIDs = input.RoleIDs
	}

	// Validate the to-update model.
	if err = rule.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate environment updated protection rule model", errors.WithSpan(span))
	}

	if err = s.verifyServiceAccountsInOrg(ctx, rule.OrgID, rule.ServiceAccountIDs); err != nil {
		return nil, err
	}

	updatedRule, err := s.dbClient.EnvironmentRules.UpdateEnvironmentRule(txContext, rule)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update environment protection rule", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: organizationID,
			ProjectID:      rule.ProjectID,
			Action:         models.ActionUpdate,
			TargetType:     targetType,
			TargetID:       targetID,
		}); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit transactions", errors.WithSpan(span))
	}

	s.logger.Infow("Updated an environment protection rule.",
		"caller", caller.GetSubject(),
		"targetId", targetID,
		"scope", rule.Scope,
	)

	return updatedRule, err
}

func (s *service) DeleteEnvironmentRule(ctx context.Context, input *DeleteEnvironmentRuleInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteEnvironmentRule")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	originalRule, err := s.getEnvironmentRuleByID(ctx, input.ID, span)
	if err != nil {
		return err
	}

	var (
		targetType     models.ActivityEventTargetType
		targetID       *string
		organizationID *string // Only needed for org environment protection rules.
	)

	switch originalRule.Scope {
	case models.OrganizationScope:
		if err = caller.RequirePermission(ctx, models.DeleteEnvironmentRule, auth.WithOrganizationID(originalRule.OrgID)); err != nil {
			return err
		}

		organizationID = &originalRule.OrgID
		targetID = &originalRule.OrgID
		targetType = models.TargetOrganization
	case models.ProjectScope:
		if err = caller.RequirePermission(ctx, models.DeleteEnvironmentRule, auth.WithProjectID(*originalRule.ProjectID)); err != nil {
			return err
		}

		targetID = originalRule.ProjectID
		targetType = models.TargetProject
	default:
		return errors.New("unexpected environment protection rule scope %s", originalRule.Scope, errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteEnvironmentRule: %v", txErr)
		}
	}()

	if err = s.dbClient.EnvironmentRules.DeleteEnvironmentRule(txContext, originalRule); err != nil {
		return errors.Wrap(err, "failed to delete environment protection rule", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: organizationID,
			ProjectID:      originalRule.ProjectID,
			Action:         models.ActionDelete,
			TargetType:     targetType,
			TargetID:       targetID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				ID:   originalRule.Metadata.ID,
				Type: string(models.TargetEnvironmentRule),
			},
		}); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit transactions", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted an environment protection rule.",
		"caller", caller.GetSubject(),
		"targetId", targetID,
		"scope", originalRule.Scope,
	)

	return nil
}

func (s *service) getEnvironmentRuleByID(ctx context.Context, id string, span trace.Span) (*models.EnvironmentRule, error) {
	rule, err := s.dbClient.EnvironmentRules.GetEnvironmentRuleByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get environment protection rule", errors.WithSpan(span))
	}

	if rule == nil {
		return nil, errors.New("environment protection rule not found",
			errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return rule, nil
}

func (s *service) verifyServiceAccountsInOrg(ctx context.Context, orgID string, serviceAccountIDs []string) error {
	if len(serviceAccountIDs) == 0 {
		return nil
	}

	response, err := s.dbClient.ServiceAccounts.GetServiceAccounts(ctx, &db.GetServiceAccountsInput{
		Filter: &db.ServiceAccountFilter{
			ServiceAccountIDs: serviceAccountIDs,
			OrganizationID:    &orgID,
		},
	})
	if err != nil {
		return err
	}
	if int(response.PageInfo.TotalCount) != len(serviceAccountIDs) {
		return errors.New("one or more service accounts not found", errors.WithErrorCode(errors.EInvalid))
	}

	foundMap := map[string]bool{}
	for _, sa := range response.ServiceAccounts {
		foundMap[sa.Metadata.ID] = true
	}

	notFound := []string{}
	for _, id := range serviceAccountIDs {
		if _, ok := foundMap[id]; !ok {
			notFound = append(notFound, id)
		}
	}

	if len(notFound) > 0 {
		return errors.New("the following service accounts do not exist in org %s: %s", orgID, strings.Join(notFound, ","), errors.WithErrorCode(errors.EInvalid))
	}

	return nil
}
