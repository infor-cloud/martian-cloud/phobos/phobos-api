package environment

import "go.opentelemetry.io/otel"

var tracer = otel.Tracer("environment")
