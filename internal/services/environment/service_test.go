package environment

import (
	"context"
	"fmt"
	"slices"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	limitChecker := limits.NewMockLimitChecker(t)
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		limitChecker:    limitChecker,
		activityService: activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, limitChecker, activityService))
}

func TestGetEnvironmentByID(t *testing.T) {
	environmentID := "environment-1"
	projectID := "project-1"
	// Test cases
	tests := []struct {
		expectEnvironment *models.Environment
		name              string
		authError         error
		expectErrCode     errors.CodeType
	}{
		{
			name: "successfully get project environment",
			expectEnvironment: &models.Environment{
				Metadata:  models.ResourceMetadata{ID: environmentID},
				ProjectID: projectID,
				Name:      "test-environment",
			},
		},
		{
			name: "subject does not have access to project environment",
			expectEnvironment: &models.Environment{
				Metadata:  models.ResourceMetadata{ID: environmentID},
				ProjectID: projectID,
				Name:      "test-environment",
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "environment not found",
			expectErrCode: errors.ENotFound,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironments := db.NewMockEnvironments(t)

			if test.expectEnvironment != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironment, mock.Anything).Return(test.authError)
			}

			mockEnvironments.On("GetEnvironmentByID", mock.Anything, environmentID).Return(test.expectEnvironment, nil)

			dbClient := &db.Client{
				Environments: mockEnvironments,
			}

			service := &service{
				dbClient: dbClient,
			}

			environment, err := service.GetEnvironmentByID(auth.WithCaller(ctx, mockCaller), environmentID)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectEnvironment, environment)
		})
	}
}

func TestGetEnvironmentByPRN(t *testing.T) {
	environmentPRN := "prn:environment:project-1/environment-1"
	projectID := "project-1"
	// Test cases
	tests := []struct {
		expectEnvironment *models.Environment
		name              string
		authError         error
		expectErrCode     errors.CodeType
	}{
		{
			name: "successfully get project environment",
			expectEnvironment: &models.Environment{
				Metadata:  models.ResourceMetadata{PRN: environmentPRN},
				ProjectID: projectID,
				Name:      "test-environment",
			},
		},
		{
			name: "subject does not have access to project environment",
			expectEnvironment: &models.Environment{
				Metadata:  models.ResourceMetadata{PRN: environmentPRN},
				ProjectID: projectID,
				Name:      "test-environment",
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "environment not found",
			expectErrCode: errors.ENotFound,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironments := db.NewMockEnvironments(t)

			if test.expectEnvironment != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironment, mock.Anything).Return(test.authError)
			}

			mockEnvironments.On("GetEnvironmentByPRN", mock.Anything, environmentPRN).Return(test.expectEnvironment, nil)

			dbClient := &db.Client{
				Environments: mockEnvironments,
			}

			service := &service{
				dbClient: dbClient,
			}

			environment, err := service.GetEnvironmentByPRN(auth.WithCaller(ctx, mockCaller), environmentPRN)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectEnvironment, environment)
		})
	}
}

func TestGetEnvironmentsByIDs(t *testing.T) {
	environmentID := "environment-1"
	projectID := "project-1"
	// Test cases
	tests := []struct {
		expectEnvironment *models.Environment
		name              string
		authError         error
		expectErrCode     errors.CodeType
	}{
		{
			name: "successfully get project environment",
			expectEnvironment: &models.Environment{
				Metadata:  models.ResourceMetadata{ID: environmentID},
				ProjectID: projectID,
				Name:      "test-environment",
			},
		},
		{
			name: "subject does not have access to project environment",
			expectEnvironment: &models.Environment{
				Metadata:  models.ResourceMetadata{ID: environmentID},
				ProjectID: projectID,
				Name:      "test-environment",
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "environment not found",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironments := db.NewMockEnvironments(t)

			if test.expectEnvironment != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironment, mock.Anything).Return(test.authError)
			}

			getEnvironmentsResponse := db.EnvironmentsResult{
				Environments: []models.Environment{},
			}

			if test.expectEnvironment != nil {
				getEnvironmentsResponse.Environments = append(getEnvironmentsResponse.Environments, *test.expectEnvironment)
			}

			mockEnvironments.On("GetEnvironments", mock.Anything, &db.GetEnvironmentsInput{
				Filter: &db.EnvironmentFilter{
					EnvironmentIDs: []string{environmentID},
				},
			}).Return(&getEnvironmentsResponse, nil)

			dbClient := &db.Client{
				Environments: mockEnvironments,
			}

			service := &service{
				dbClient: dbClient,
			}

			environments, err := service.GetEnvironmentsByIDs(auth.WithCaller(ctx, mockCaller), []string{environmentID})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			if test.expectEnvironment != nil {
				assert.Equal(t, 1, len(environments))
				assert.Equal(t, test.expectEnvironment, &environments[0])
			} else {
				assert.Equal(t, 0, len(environments))
			}
		})
	}
}

func TestGetEnvironments(t *testing.T) {
	organizationID := "organization-1"
	projectID := "project-1"

	// Test cases
	tests := []struct {
		authError          error
		input              *GetEnvironmentsInput
		name               string
		expectErrCode      errors.CodeType
		expectEnvironments int
	}{
		{
			name: "successfully get environments for an project",
			input: &GetEnvironmentsInput{
				ProjectID: &projectID,
			},
			expectEnvironments: 1,
		},
		{
			name: "successfully get environments for an organization",
			input: &GetEnvironmentsInput{
				OrganizationID: &organizationID,
			},
			expectEnvironments: 1,
		},
		{
			name:          "subject is not a member of the project",
			input:         &GetEnvironmentsInput{ProjectID: &projectID},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "subject is not a member of the organization",
			input:         &GetEnvironmentsInput{OrganizationID: &organizationID},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "no filter returns an error",
			input:         &GetEnvironmentsInput{},
			expectErrCode: errors.EInvalid,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironments := db.NewMockEnvironments(t)
			mockProjects := db.NewMockProjects(t)

			if test.input.ProjectID != nil || test.input.OrganizationID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironment, mock.Anything).Return(test.authError)
			}

			if test.expectErrCode == "" {
				mockEnvironments.On("GetEnvironments", mock.Anything, mock.Anything).Return(
					func(_ context.Context, _ *db.GetEnvironmentsInput) (*db.EnvironmentsResult, error) {
						environments := []models.Environment{}
						for i := 0; i < test.expectEnvironments; i++ {
							environments = append(environments, models.Environment{
								Name:        fmt.Sprintf("environment-%d", i),
								ProjectID:   projectID,
								Description: "test description",
							})
						}

						return &db.EnvironmentsResult{Environments: environments}, nil
					},
				)
			}

			dbClient := &db.Client{
				Environments: mockEnvironments,
				Projects:     mockProjects,
			}

			service := &service{
				dbClient: dbClient,
			}

			resp, err := service.GetEnvironments(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Len(t, resp.Environments, test.expectEnvironments)
		})
	}
}

func TestCreateEnvironment(t *testing.T) {
	environmentName := "test-environment"
	testSubject := "test-subject"
	projectID := "project-1"

	// Test cases
	tests := []struct {
		authError                    error
		limitError                   error
		input                        *CreateEnvironmentInput
		expectCreatedEnvironment     *models.Environment
		name                         string
		expectErrCode                errors.CodeType
		injectEnvironmentsPerProject int32
	}{
		{
			name: "create project environment",
			input: &CreateEnvironmentInput{
				Name:        environmentName,
				ProjectID:   projectID,
				Description: "test description",
			},
			expectCreatedEnvironment: &models.Environment{
				Name:        environmentName,
				ProjectID:   projectID,
				CreatedBy:   testSubject,
				Description: "test description",
			},
			injectEnvironmentsPerProject: 5,
		},
		{
			name:          "subject does not have permissions to create an environment",
			input:         &CreateEnvironmentInput{Name: environmentName, ProjectID: projectID},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:  "exceeds limit",
			input: &CreateEnvironmentInput{Name: environmentName, ProjectID: projectID},
			expectCreatedEnvironment: &models.Environment{
				Name:      environmentName,
				ProjectID: projectID,
				CreatedBy: testSubject,
			},
			limitError:                   errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			injectEnvironmentsPerProject: 6,
			expectErrCode:                errors.EInvalid,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironments := db.NewMockEnvironments(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockProjects := db.NewMockProjects(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("RequirePermission", mock.Anything, models.CreateEnvironment, mock.Anything).Return(test.authError)

			if test.authError == nil {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

				mockEnvironments.On("GetEnvironments", mock.Anything, &db.GetEnvironmentsInput{
					Filter: &db.EnvironmentFilter{
						ProjectID: &projectID,
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.EnvironmentsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: test.injectEnvironmentsPerProject,
					},
				}, nil)

				mockLimitChecker.On("CheckLimit", mock.Anything,
					limits.ResourceLimitEnvironmentsPerProject, test.injectEnvironmentsPerProject).
					Return(test.limitError)

				if test.limitError == nil {
					mockTransactions.On("CommitTx", mock.Anything).Return(nil)

					mockActivityEvents.On("CreateActivityEvent", mock.Anything,
						&activityevent.CreateActivityEventInput{
							ProjectID:  &test.expectCreatedEnvironment.ProjectID,
							Action:     models.ActionCreate,
							TargetType: models.TargetEnvironment,
							TargetID:   &test.expectCreatedEnvironment.Metadata.ID,
						},
					).Return(&models.ActivityEvent{}, nil)
				}
			}

			if test.expectCreatedEnvironment != nil {
				mockCaller.On("GetSubject").Return("mockSubject")

				mockEnvironments.On("CreateEnvironment", mock.Anything, mock.Anything).Return(test.expectCreatedEnvironment, nil)
			}

			dbClient := &db.Client{
				Transactions: mockTransactions,
				Environments: mockEnvironments,
				Projects:     mockProjects,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				logger:          testLogger,
				activityService: mockActivityEvents,
			}

			environment, err := service.CreateEnvironment(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreatedEnvironment, environment)
		})
	}
}

func TestUpdateEnvironment(t *testing.T) {
	environmentID := "environment123"
	projectID := "project123"

	sampleEnvironment := &models.Environment{
		Metadata: models.ResourceMetadata{
			ID: environmentID,
		},
		Name:        "environment1",
		ProjectID:   projectID,
		Description: "test description",
	}

	// Test cases
	tests := []struct {
		authError           error
		existingEnvironment *models.Environment
		expectUpdated       *models.Environment
		name                string
		expectErrCode       errors.CodeType
	}{
		{
			name:                "successfully update an environment",
			existingEnvironment: sampleEnvironment,
			expectUpdated: &models.Environment{
				Metadata: models.ResourceMetadata{
					ID: environmentID,
				},
				Name:        "environment1",
				ProjectID:   projectID,
				Description: "updated description",
			},
		},
		{
			name:          "environment not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name:                "subject does not have permissions to update an environment",
			existingEnvironment: sampleEnvironment,
			authError:           errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode:       errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironments := db.NewMockEnvironments(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockEnvironments.On("GetEnvironmentByID", mock.Anything, environmentID).Return(test.existingEnvironment, nil)

			if test.existingEnvironment != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateEnvironment, mock.Anything).Return(test.authError)
			}

			if test.expectUpdated != nil {
				mockCaller.On("GetSubject").Return("mockSubject")

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockEnvironments.On("UpdateEnvironment", mock.Anything, test.expectUpdated).Return(test.expectUpdated, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &test.expectUpdated.ProjectID,
						Action:     models.ActionUpdate,
						TargetType: models.TargetEnvironment,
						TargetID:   &test.expectUpdated.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Environments: mockEnvironments,
				Transactions: mockTransactions,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				dbClient:        dbClient,
				logger:          testLogger,
				activityService: mockActivityEvents,
			}

			environment, err := service.UpdateEnvironment(auth.WithCaller(ctx, mockCaller), &UpdateEnvironmentInput{
				ID:          environmentID,
				Description: ptr.String("updated description"),
			})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingEnvironment, environment)
		})
	}
}

func TestDeleteEnvironment(t *testing.T) {
	projectID := "project123"
	environmentID := "environment123"

	sampleEnvironment := &models.Environment{
		Metadata: models.ResourceMetadata{
			ID: environmentID,
		},
		Name:        "environment1",
		ProjectID:   projectID,
		Description: "test description",
	}

	// Test cases
	tests := []struct {
		authError           error
		existingEnvironment *models.Environment
		name                string
		expectErrCode       errors.CodeType
		deployments         int
	}{
		{
			name:                "successfully delete an environment with no deployments",
			existingEnvironment: sampleEnvironment,
		},
		{
			name:          "environment not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name:                "subject does not have permissions to delete an environment",
			existingEnvironment: sampleEnvironment,
			authError:           errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode:       errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironments := db.NewMockEnvironments(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockEnvironments.On("GetEnvironmentByID", mock.Anything, environmentID).Return(test.existingEnvironment, nil)

			if test.existingEnvironment != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.DeleteEnvironment, mock.Anything).Return(test.authError)
			}

			if test.expectErrCode == "" {
				mockCaller.On("GetSubject").Return("mockSubject")

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockEnvironments.On("DeleteEnvironment", mock.Anything, test.existingEnvironment).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &test.existingEnvironment.ProjectID,
						Action:     models.ActionDelete,
						TargetType: models.TargetProject,
						TargetID:   &test.existingEnvironment.ProjectID,
						Payload: &models.ActivityEventDeleteResourcePayload{
							Name: &test.existingEnvironment.Name,
							ID:   test.existingEnvironment.Metadata.ID,
							Type: models.TargetEnvironment.String(),
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Environments: mockEnvironments,
				Transactions: mockTransactions,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				dbClient:        dbClient,
				logger:          testLogger,
				activityService: mockActivityEvents,
			}

			err := service.DeleteEnvironment(auth.WithCaller(ctx, mockCaller), &DeleteEnvironmentInput{ID: environmentID})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestGetEnvironmentRuleByID(t *testing.T) {
	ruleID := "environment-rule-1"
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		name            string
		environmentRule *models.EnvironmentRule
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "get an org rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				OrgID: orgID,
				Scope: models.OrganizationScope,
			},
		},
		{
			name: "get a project rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name:            "rule not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view org rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				OrgID: orgID,
				Scope: models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)

			mockEnvironmentRules.On("GetEnvironmentRuleByID", mock.Anything, ruleID).Return(test.environmentRule, nil)

			if test.environmentRule != nil {
				switch test.environmentRule.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.EnvironmentRuleResource, mock.Anything).Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironmentRule, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				EnvironmentRules: mockEnvironmentRules,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualEnvironmentRule, err := service.GetEnvironmentRuleByID(auth.WithCaller(ctx, mockCaller), ruleID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.environmentRule, actualEnvironmentRule)
		})
	}
}

func TestGetEnvironmentRuleByPRN(t *testing.T) {
	projEnvironmentRulePRN := models.EnvironmentRuleResource.BuildPRN("test-org", "test-project", "test-rule")
	orgEnvironmentRulePRN := models.EnvironmentRuleResource.BuildPRN("test-org", "test-rule")
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		name            string
		prn             string
		environmentRule *models.EnvironmentRule
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "get an org rule",
			prn:  orgEnvironmentRulePRN,
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					PRN: orgEnvironmentRulePRN,
				},
				OrgID: orgID,
				Scope: models.OrganizationScope,
			},
		},
		{
			name: "get a project rule",
			prn:  projEnvironmentRulePRN,
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					PRN: projEnvironmentRulePRN,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name:            "rule not found",
			prn:             orgEnvironmentRulePRN,
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view org rule",
			prn:  orgEnvironmentRulePRN,
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					PRN: orgEnvironmentRulePRN,
				},
				OrgID: orgID,
				Scope: models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project rule",
			prn:  projEnvironmentRulePRN,
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					PRN: projEnvironmentRulePRN,
				},
				ProjectID: &projID,
				Scope:     models.ProjectScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)

			mockEnvironmentRules.On("GetEnvironmentRuleByPRN", mock.Anything, test.prn).Return(test.environmentRule, nil)

			if test.environmentRule != nil {
				switch test.environmentRule.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.EnvironmentRuleResource, mock.Anything).Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironmentRule, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				EnvironmentRules: mockEnvironmentRules,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualEnvironmentRule, err := service.GetEnvironmentRuleByPRN(auth.WithCaller(ctx, mockCaller), test.prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.environmentRule, actualEnvironmentRule)
		})
	}
}

func TestGetEnvironmentRules(t *testing.T) {
	orgID := "org-1"
	projID := "proj-1"
	envName := "env-1"
	userID := "user-1"
	serviceAccountID := "service-account-1"
	teamID := "team-1"
	roleID := "role-1"

	type testCase struct {
		name            string
		organizationID  *string
		projectID       *string
		environmentName *string
		authError       error
		rulesResult     *db.EnvironmentRulesResult
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:           "filter by org, with user ID",
			organizationID: &orgID,
			rulesResult: &db.EnvironmentRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				EnvironmentRules: []*models.EnvironmentRule{
					{
						Scope:           models.OrganizationScope,
						OrgID:           orgID,
						EnvironmentName: envName,
						UserIDs:         []string{userID},
					},
				},
			},
		},
		{
			name:      "filter by proj, with service account ID",
			projectID: &projID,
			rulesResult: &db.EnvironmentRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				EnvironmentRules: []*models.EnvironmentRule{
					{
						Scope:             models.OrganizationScope,
						OrgID:             orgID,
						ProjectID:         &projID,
						EnvironmentName:   envName,
						ServiceAccountIDs: []string{serviceAccountID},
					},
				},
			},
		},
		{
			name:      "filter by proj, with team ID",
			projectID: &projID,
			rulesResult: &db.EnvironmentRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				EnvironmentRules: []*models.EnvironmentRule{
					{
						Scope:           models.OrganizationScope,
						OrgID:           orgID,
						ProjectID:       &projID,
						EnvironmentName: envName,
						TeamIDs:         []string{teamID},
					},
				},
			},
		},
		{
			name:            "filter by environment ID, with role ID",
			projectID:       &projID,
			environmentName: &envName,
			rulesResult: &db.EnvironmentRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: 1,
				},
				EnvironmentRules: []*models.EnvironmentRule{
					{
						Scope:           models.OrganizationScope,
						OrgID:           orgID,
						ProjectID:       &projID,
						EnvironmentName: envName,
						RoleIDs:         []string{roleID},
					},
				},
			},
		},
		{
			name:            "get will fail without organization or project id",
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "subject is not authorized to get org rule",
			organizationID:  &orgID,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "subject is not authorized to get project rule",
			projectID:       &projID,
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)

			if test.organizationID != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.EnvironmentRuleResource, mock.Anything).
					Return(test.authError)
			} else if test.projectID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironmentRule, mock.Anything).
					Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockEnvironmentRules.On("GetEnvironmentRules", mock.Anything, mock.MatchedBy(
					func(input *db.GetEnvironmentRulesInput) bool {
						switch {
						case test.organizationID != nil:
							return input.Filter.OrgID == test.organizationID &&
								slices.Compare(input.Filter.EnvironmentRuleScopes,
									[]models.ScopeType{models.OrganizationScope},
								) == 0
						case test.projectID != nil:
							return input.Filter.ProjectID == test.projectID &&
								slices.Compare(input.Filter.EnvironmentRuleScopes,
									[]models.ScopeType{models.ProjectScope},
								) == 0
						}

						return false
					})).Return(test.rulesResult, nil)
			}

			dbClient := &db.Client{
				EnvironmentRules: mockEnvironmentRules,
			}

			service := &service{
				dbClient: dbClient,
			}

			result, err := service.GetEnvironmentRules(auth.WithCaller(ctx, mockCaller), &GetEnvironmentRulesInput{
				OrganizationID:  test.organizationID,
				ProjectID:       test.projectID,
				EnvironmentName: test.environmentName,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, result)
			assert.Equal(t, test.rulesResult.PageInfo, result.PageInfo)
			assert.ElementsMatch(t, test.rulesResult.EnvironmentRules, result.EnvironmentRules)
		})
	}
}

func TestGetEnvironmentRulesByIDs(t *testing.T) {
	idList := []string{"rule-1"}
	orgID := "org-1"
	projID := "proj-1"

	type testCase struct {
		authError        error
		name             string
		expectErrorCode  errors.CodeType
		environmentRules []*models.EnvironmentRule
	}

	testCases := []testCase{
		{
			name: "get org rule",
			environmentRules: []*models.EnvironmentRule{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					OrgID: orgID,
					Scope: models.OrganizationScope,
				},
			},
		},
		{
			name: "get proj environment protection rule",
			environmentRules: []*models.EnvironmentRule{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					ProjectID: &projID,
					Scope:     models.ProjectScope,
				},
			},
		},
		{
			name:             "environment protection rules not found",
			environmentRules: []*models.EnvironmentRule{},
		},
		{
			name: "subject is not authorized to view org rule",
			environmentRules: []*models.EnvironmentRule{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					OrgID: orgID,
					Scope: models.OrganizationScope,
				},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to view project rule",
			environmentRules: []*models.EnvironmentRule{
				{
					Metadata: models.ResourceMetadata{
						ID: idList[0],
					},
					ProjectID: &projID,
					Scope:     models.ProjectScope,
				},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)

			mockEnvironmentRules.On("GetEnvironmentRules", mock.Anything, &db.GetEnvironmentRulesInput{
				Filter: &db.EnvironmentRuleFilter{
					EnvironmentRuleIDs: idList,
				},
			}).Return(&db.EnvironmentRulesResult{
				PageInfo: &pagination.PageInfo{
					TotalCount: int32(len(test.environmentRules)),
				},
				EnvironmentRules: test.environmentRules,
			}, nil)

			for _, environmentRule := range test.environmentRules {
				switch environmentRule.Scope {
				case models.OrganizationScope:
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.EnvironmentRuleResource, mock.Anything).Return(test.authError)
				case models.ProjectScope:
					mockCaller.On("RequirePermission", mock.Anything, models.ViewEnvironmentRule, mock.Anything).Return(test.authError)
				}
			}

			dbClient := &db.Client{
				EnvironmentRules: mockEnvironmentRules,
			}

			service := &service{
				dbClient: dbClient,
			}

			result, err := service.GetEnvironmentRulesByIDs(auth.WithCaller(ctx, mockCaller), idList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.Len(t, result, len(test.environmentRules))
		})
	}
}

func TestCreateOrganizationEnvironmentRule(t *testing.T) {
	orgID := "org-1"
	userID := "user-1"
	serviceAccountID := "service-account-1"
	teamID := "team-1"
	roleID := "role-1"
	ruleID := "rule-1"
	testSubject := "testSubject"
	envName := "environment-name-1"

	type testCase struct {
		name              string
		input             *CreateOrganizationEnvironmentRuleInput
		toCreate          *models.EnvironmentRule
		limitError        error
		authError         error
		getServiceAccount *models.ServiceAccount
		expectErrorCode   errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create an organization environment protection rule with a user ID",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:  orgID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.OrganizationScope,
				OrgID:           orgID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
		},
		{
			name: "successfully create an organization environment protection rule with a service account ID",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:    orgID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
			getServiceAccount: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID: serviceAccountID,
				},
				OrganizationID: &orgID,
			},
			toCreate: &models.EnvironmentRule{
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
		},
		{
			name: "successfully create an organization environment protection rule with a team ID",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:  orgID,
				EnvironmentName: envName,
				TeamIDs:         []string{teamID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.OrganizationScope,
				OrgID:           orgID,
				EnvironmentName: envName,
				TeamIDs:         []string{teamID},
			},
		},
		{
			name: "successfully create an organization environment protection rule with a role ID",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:  orgID,
				EnvironmentName: envName,
				RoleIDs:         []string{roleID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.OrganizationScope,
				OrgID:           orgID,
				EnvironmentName: envName,
				RoleIDs:         []string{roleID},
			},
		},
		{
			name: "fail to create an organization environment protection rule with zero users, etc.",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:  orgID,
				EnvironmentName: envName,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "subject is not authorized to create an environment protection rule",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:  orgID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "service account is outside the organization",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:    orgID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
			getServiceAccount: &models.ServiceAccount{},
			toCreate: &models.EnvironmentRule{
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "limit exceeded",
			input: &CreateOrganizationEnvironmentRuleInput{
				OrganizationID:  orgID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.OrganizationScope,
				OrgID:           orgID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			limitError:      errors.New("Limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
		},
		// Because of the separation between CreateOrg... and CreateProj...,
		// it's not possible to attempt to create inconsistent scope vs. project ID.
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// mockEnvironmentRule := db.NewMockEnvironmentRules(t)
			mockCaller := auth.NewMockCaller(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.CreateEnvironmentRule, mock.Anything).
				Return(test.authError).Maybe()

			if test.toCreate != nil {

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

				mockEnvironmentRules.On("CreateEnvironmentRule", mock.Anything, test.toCreate).Return(
					func(_ context.Context, input *models.EnvironmentRule) (*models.EnvironmentRule, error) {
						copyInput := *input

						// Assign an ID and version to the record. Rest should remain the same.
						copyInput.Metadata.ID = ruleID
						copyInput.Metadata.Version = 1
						return &copyInput, nil
					},
				).Maybe()

				mockEnvironmentRules.On("GetEnvironmentRules", mock.Anything, &db.GetEnvironmentRulesInput{
					Filter: &db.EnvironmentRuleFilter{
						OrgID:                 &orgID,
						EnvironmentRuleScopes: []models.ScopeType{models.OrganizationScope},
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.EnvironmentRulesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil).Maybe()

				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitEnvironmentRulesPerOrganization, int32(1)).
					Return(test.limitError).Maybe()
			}

			if test.getServiceAccount != nil {
				mockServiceAccounts.On("GetServiceAccounts", mock.Anything, mock.Anything).
					Return(&db.ServiceAccountsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
						ServiceAccounts: []*models.ServiceAccount{
							test.getServiceAccount,
						},
					}, nil)
			}

			if test.expectErrorCode == "" {
				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					OrganizationID: &orgID,
					Action:         models.ActionCreate,
					TargetType:     models.TargetEnvironmentRule,
					TargetID:       &ruleID,
				}).Return(nil, nil)

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				EnvironmentRules: mockEnvironmentRules,
				Transactions:     mockTransactions,
				ServiceAccounts:  mockServiceAccounts,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
			}

			response, err := service.CreateOrganizationEnvironmentRule(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, response)
			require.NotNil(t, response)
		})
	}
}

func TestCreateProjectEnvironmentRule(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	ruleID := "rule-1"
	testSubject := "testSubject"
	userID := "user-id-1"
	serviceAccountID := "service-account-1"
	teamID := "team-id-1"
	roleID := "role-id-1"
	envName := "environment-name-1"

	type testCase struct {
		name              string
		input             *CreateProjectEnvironmentRuleInput
		toCreate          *models.EnvironmentRule
		limitError        error
		authError         error
		getServiceAccount *models.ServiceAccount
		expectErrorCode   errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully create a project environment protection rule with a user ID",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:       projectID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.ProjectScope,
				OrgID:           orgID,
				ProjectID:       &projectID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
		},
		{
			name: "successfully create a project environment protection rule with a service account ID",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:         projectID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
			getServiceAccount: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID: serviceAccountID,
				},
				OrganizationID: &orgID,
			},
			toCreate: &models.EnvironmentRule{
				Scope:             models.ProjectScope,
				OrgID:             orgID,
				ProjectID:         &projectID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
		},
		{
			name: "successfully create a project environment protection rule with a team ID",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:       projectID,
				EnvironmentName: envName,
				TeamIDs:         []string{teamID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.ProjectScope,
				OrgID:           orgID,
				ProjectID:       &projectID,
				EnvironmentName: envName,
				TeamIDs:         []string{teamID},
			},
		},
		{
			name: "successfully create a project environment protection rule with a role ID",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:       projectID,
				EnvironmentName: envName,
				RoleIDs:         []string{roleID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.ProjectScope,
				OrgID:           orgID,
				ProjectID:       &projectID,
				EnvironmentName: envName,
				RoleIDs:         []string{roleID},
			},
		},
		{
			name: "fail to create a project environment protection rule with zero users, etc.",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:       projectID,
				EnvironmentName: envName,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "subject is not authorized to create an environment protection rule",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:       projectID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "service account is outside the organization",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:         projectID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
			getServiceAccount: &models.ServiceAccount{},
			toCreate: &models.EnvironmentRule{
				Scope:             models.ProjectScope,
				OrgID:             orgID,
				ProjectID:         &projectID,
				EnvironmentName:   envName,
				ServiceAccountIDs: []string{serviceAccountID},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "limit exceeded",
			input: &CreateProjectEnvironmentRuleInput{
				ProjectID:       projectID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			toCreate: &models.EnvironmentRule{
				Scope:           models.ProjectScope,
				OrgID:           orgID,
				ProjectID:       &projectID,
				EnvironmentName: envName,
				UserIDs:         []string{userID},
			},
			limitError:      errors.New("Limit exceeded", errors.WithErrorCode(errors.EInvalid)),
			expectErrorCode: errors.EInvalid,
		},
		// Because of the separation between CreateOrg... and CreateProj...,
		// it's not possible to attempt to create inconsistent scope vs. project ID.
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.CreateEnvironmentRule, mock.Anything).
				Return(test.authError).Maybe()

			mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(&models.Project{
				Metadata: models.ResourceMetadata{
					ID: projectID,
				},
				OrgID: orgID,
			}, nil).Maybe()

			if test.toCreate != nil {

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

				mockEnvironmentRules.On("CreateEnvironmentRule", mock.Anything, test.toCreate).Return(
					func(_ context.Context, input *models.EnvironmentRule) (*models.EnvironmentRule, error) {
						copyInput := *input

						// Assign an ID and version to the record. Rest should remain the same.
						copyInput.Metadata.ID = ruleID
						copyInput.Metadata.Version = 1
						return &copyInput, nil
					},
				).Maybe()

				mockEnvironmentRules.On("GetEnvironmentRules", mock.Anything, &db.GetEnvironmentRulesInput{
					Filter: &db.EnvironmentRuleFilter{
						ProjectID:             &projectID,
						EnvironmentRuleScopes: []models.ScopeType{models.ProjectScope},
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
				}).Return(&db.EnvironmentRulesResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 1,
					},
				}, nil).Maybe()

				mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitEnvironmentsPerProject, int32(1)).
					Return(test.limitError).Maybe()
			}

			if test.getServiceAccount != nil {
				mockServiceAccounts.On("GetServiceAccounts", mock.Anything, mock.Anything).
					Return(&db.ServiceAccountsResult{
						PageInfo: &pagination.PageInfo{
							TotalCount: 1,
						},
						ServiceAccounts: []*models.ServiceAccount{
							test.getServiceAccount,
						},
					}, nil)
			}

			if test.expectErrorCode == "" {
				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					ProjectID:  &projectID,
					Action:     models.ActionCreate,
					TargetType: models.TargetEnvironmentRule,
					TargetID:   &ruleID,
				}).Return(nil, nil)

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				Projects:         mockProjects,
				EnvironmentRules: mockEnvironmentRules,
				Transactions:     mockTransactions,
				ServiceAccounts:  mockServiceAccounts,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
			}

			response, err := service.CreateProjectEnvironmentRule(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			require.NotNil(t, response)
			require.NotNil(t, response)
		})
	}
}

func TestUpdateEnvironmentRule(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	ruleID := "rule-1"
	createdBy := "created-by-1"
	envName := "environment-1"
	userID := "user-1"
	serviceAccountID := "service-account-1"
	teamID := "team-1"
	roleID := "role-1"

	//

	type testCase struct {
		name              string
		seekID            string
		originalRule      *models.EnvironmentRule
		overrideUserIDs   []string
		expectUpdated     *models.EnvironmentRule
		authError         error
		getServiceAccount *models.ServiceAccount
		expectErrorCode   errors.CodeType
	}

	testCases := []testCase{
		{
			name:   "update an org rule",
			seekID: ruleID,
			originalRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 1,
				},
				CreatedBy:         createdBy,
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				UserIDs:           []string{userID},
				ServiceAccountIDs: []string{serviceAccountID},
				TeamIDs:           []string{},
				RoleIDs:           []string{},
			},
			expectUpdated: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 2,
				},
				CreatedBy:         createdBy,
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				UserIDs:           []string{},
				ServiceAccountIDs: []string{},
				TeamIDs:           []string{teamID},
				RoleIDs:           []string{roleID},
			},
		},
		{
			name:   "update a project rule",
			seekID: ruleID,
			originalRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 1,
				},
				CreatedBy:         createdBy,
				Scope:             models.ProjectScope,
				OrgID:             orgID,
				ProjectID:         &projectID,
				EnvironmentName:   envName,
				UserIDs:           []string{},
				ServiceAccountIDs: []string{},
				TeamIDs:           []string{teamID},
				RoleIDs:           []string{roleID},
			},
			getServiceAccount: &models.ServiceAccount{
				Metadata: models.ResourceMetadata{
					ID: serviceAccountID,
				},
				OrganizationID: &orgID,
			},
			expectUpdated: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 2,
				},
				CreatedBy:         createdBy,
				Scope:             models.ProjectScope,
				OrgID:             orgID,
				ProjectID:         &projectID,
				EnvironmentName:   envName,
				UserIDs:           []string{userID},
				ServiceAccountIDs: []string{serviceAccountID},
				TeamIDs:           []string{},
				RoleIDs:           []string{},
			},
		},
		{
			name:   "fail to update an rule to zero users, etc.",
			seekID: ruleID,
			originalRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 1,
				},
				CreatedBy:         createdBy,
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				UserIDs:           []string{userID},
				ServiceAccountIDs: []string{serviceAccountID},
				TeamIDs:           []string{},
				RoleIDs:           []string{},
			},
			expectUpdated: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 2,
				},
				CreatedBy:         createdBy,
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				UserIDs:           []string{},
				ServiceAccountIDs: []string{},
				TeamIDs:           []string{},
				RoleIDs:           []string{},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:   "subject is not authorized to update org rule",
			seekID: ruleID,
			originalRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 1,
				},
				CreatedBy:         createdBy,
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				UserIDs:           []string{userID},
				ServiceAccountIDs: []string{serviceAccountID},
				TeamIDs:           []string{},
				RoleIDs:           []string{},
			},
			overrideUserIDs: []string{"a-test-user"},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:   "subject is not authorized to update project rule",
			seekID: ruleID,
			originalRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 1,
				},
				CreatedBy:         createdBy,
				Scope:             models.ProjectScope,
				OrgID:             orgID,
				ProjectID:         &projectID,
				EnvironmentName:   envName,
				UserIDs:           []string{},
				ServiceAccountIDs: []string{},
				TeamIDs:           []string{teamID},
				RoleIDs:           []string{roleID},
			},
			overrideUserIDs: []string{"a-test-user"},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:   "service account is outside the organization",
			seekID: ruleID,
			originalRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 1,
				},
				CreatedBy:         createdBy,
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				UserIDs:           []string{userID},
				ServiceAccountIDs: []string{serviceAccountID},
				TeamIDs:           []string{},
				RoleIDs:           []string{},
			},
			getServiceAccount: &models.ServiceAccount{},
			expectUpdated: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID:      ruleID,
					Version: 2,
				},
				CreatedBy:         createdBy,
				Scope:             models.OrganizationScope,
				OrgID:             orgID,
				EnvironmentName:   envName,
				UserIDs:           []string{},
				ServiceAccountIDs: []string{},
				TeamIDs:           []string{teamID},
				RoleIDs:           []string{roleID},
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:            "rule not found",
			seekID:          "non-existent-id",
			overrideUserIDs: []string{"a-test-user"},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockCaller.On("GetSubject").Return("testSubject").Maybe()

			mockEnvironmentRules.On("GetEnvironmentRuleByID", mock.Anything, ruleID).Return(test.originalRule, nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateEnvironmentRule, mock.Anything).
				Return(test.authError).Maybe()

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			mockEnvironmentRules.On("UpdateEnvironmentRule", mock.Anything, test.originalRule).
				Return(func(_ context.Context, _ *models.EnvironmentRule) (*models.EnvironmentRule, error) {
					if test.expectErrorCode == "" {
						// return an updated rule with no error
						return test.expectUpdated, nil
					}

					// return an error with no rule
					return nil, errors.New("", errors.WithErrorCode(test.expectErrorCode))
				}).Maybe()

			if test.expectErrorCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				var organizationID *string
				var targetType models.ActivityEventTargetType
				var targetID *string
				if test.originalRule.Scope == models.OrganizationScope {
					organizationID = &orgID
					targetID = &orgID
					targetType = models.TargetOrganization
				} else {
					targetID = &projectID
					targetType = models.TargetProject
				}

				var eventProjectID *string
				if test.originalRule.Scope == models.ProjectScope {
					eventProjectID = test.originalRule.ProjectID
				}
				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					OrganizationID: organizationID,
					ProjectID:      eventProjectID,
					Action:         models.ActionUpdate,
					TargetType:     targetType,
					TargetID:       targetID,
				}).Return(nil, nil)

				if test.getServiceAccount != nil {
					mockServiceAccounts.On("GetServiceAccounts", mock.Anything, mock.Anything).
						Return(&db.ServiceAccountsResult{
							PageInfo: &pagination.PageInfo{
								TotalCount: 1,
							},
							ServiceAccounts: []*models.ServiceAccount{
								test.getServiceAccount,
							},
						}, nil)
				}
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				EnvironmentRules: mockEnvironmentRules,
				Transactions:     mockTransactions,
				ServiceAccounts:  mockServiceAccounts,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			input := &UpdateEnvironmentRuleInput{
				ID:      ruleID,
				Version: ptr.Int(1),
			}

			if test.expectUpdated != nil {
				input.UserIDs = test.expectUpdated.UserIDs
				input.TeamIDs = test.expectUpdated.TeamIDs
				input.ServiceAccountIDs = test.expectUpdated.ServiceAccountIDs
				input.RoleIDs = test.expectUpdated.RoleIDs
			}

			if test.overrideUserIDs != nil {
				input.UserIDs = test.overrideUserIDs
			}

			actualUpdated, err := service.UpdateEnvironmentRule(auth.WithCaller(ctx, mockCaller), input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectUpdated, actualUpdated)
		})
	}
}

func TestDeleteEnvironmentRule(t *testing.T) {
	orgID := "org-1"
	projectID := "project-1"
	ruleID := "rule-1"

	type testCase struct {
		name            string
		environmentRule *models.EnvironmentRule
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "delete an org rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				OrgID: orgID,
				Scope: models.OrganizationScope,
			},
		},
		{
			name: "delete a project rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				OrgID:     orgID,
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
		},
		{
			name: "subject is not authorized to delete org rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				OrgID: orgID,
				Scope: models.OrganizationScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "subject is not authorized to delete project rule",
			environmentRule: &models.EnvironmentRule{
				Metadata: models.ResourceMetadata{
					ID: ruleID,
				},
				OrgID:     orgID,
				ProjectID: &projectID,
				Scope:     models.ProjectScope,
			},
			authError:       errors.New("Forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "rule not found",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockEnvironmentRules := db.NewMockEnvironmentRules(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockCaller.On("GetSubject").Return("testSubject").Maybe()

			mockEnvironmentRules.On("GetEnvironmentRuleByID", mock.Anything, ruleID).Return(test.environmentRule, nil)

			if test.environmentRule != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.DeleteEnvironmentRule, mock.Anything).
					Return(test.authError)
			}

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockEnvironmentRules.On("DeleteEnvironmentRule", mock.Anything, test.environmentRule).Return(nil)

				var organizationID *string
				var targetType models.ActivityEventTargetType
				var targetID *string
				if test.environmentRule.Scope == models.OrganizationScope {
					organizationID = &orgID
					targetID = &orgID
					targetType = models.TargetOrganization
				} else {
					targetID = &projectID
					targetType = models.TargetProject
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything, &activityevent.CreateActivityEventInput{
					OrganizationID: organizationID,
					ProjectID:      test.environmentRule.ProjectID,
					Action:         models.ActionDelete,
					TargetType:     targetType,
					TargetID:       targetID,
					Payload: &models.ActivityEventDeleteResourcePayload{
						ID:   test.environmentRule.Metadata.ID,
						Type: string(models.TargetEnvironmentRule),
					},
				}).Return(nil, nil)
			}

			logger, _ := logger.NewForTest()

			dbClient := &db.Client{
				EnvironmentRules: mockEnvironmentRules,
				Transactions:     mockTransactions,
			}

			service := &service{
				logger:          logger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			err := service.DeleteEnvironmentRule(auth.WithCaller(ctx, mockCaller), &DeleteEnvironmentRuleInput{
				ID:      ruleID,
				Version: ptr.Int(1),
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
		})
	}
}
