// Package agent contains all functionalities related to Phobos agents,
// which are responsible for claiming and running jobs.
package agent

import (
	"context"
	"fmt"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/fatih/color"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const agentErrorLogsBytesLimit = 2 * 1024 * 1024 // 2MiB

var agentLogsTimestampColor = color.New(color.FgMagenta)

// GetAgentsInput is the input for querying a list of agents
type GetAgentsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.AgentSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// OrganizationID is the organization to return agents for
	OrganizationID *string
	// AgentType is the type of agent to return
	AgentType *models.AgentType
}

// GetAgentSessionsInput is the input for querying a list of agent sessions
type GetAgentSessionsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.AgentSessionSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// AgentID is the agent to return sessions for
	AgentID string
}

// CreateAgentInput is the input for creating a new agent
type CreateAgentInput struct {
	OrganizationID  *string
	Type            models.AgentType
	Name            string
	Description     string
	Tags            []string
	Disabled        bool
	RunUntaggedJobs bool
}

// UpdateAgentInput is the input for updating an existing agent
type UpdateAgentInput struct {
	Disabled        *bool
	Description     *string
	MetadataVersion *int
	RunUntaggedJobs *bool
	ID              string
	Tags            []string
}

// DeleteAgentInput is the input for deleting an existing agent
type DeleteAgentInput struct {
	MetadataVersion *int
	ID              string
}

// AssignServiceAccountToAgentInput is the input for assigning a service account to an agent
type AssignServiceAccountToAgentInput struct {
	ID               string
	ServiceAccountID string
}

// UnassignServiceAccountFromAgentInput is the input for un-assigning a service account from an agent
type UnassignServiceAccountFromAgentInput struct {
	ID               string
	ServiceAccountID string
}

// CreateAgentSessionInput is the input for creating a new agent session
type CreateAgentSessionInput struct {
	AgentID  string
	Internal bool
}

// SubscribeToAgentSessionErrorLogInput includes options for setting up a log event subscription
type SubscribeToAgentSessionErrorLogInput struct {
	LastSeenLogSize *int
	AgentSessionID  string
}

// SubscribeToAgentSessionsInput is the input for subscribing to agent sessions
type SubscribeToAgentSessionsInput struct {
	OrganizationID *string
	AgentID        *string
	AgentType      *models.AgentType
}

// SessionEvent is an agent session event
type SessionEvent struct {
	AgentSession *models.AgentSession
	Action       string
}

// Service implements all agent related functionality
type Service interface {
	GetAgentByID(ctx context.Context, id string) (*models.Agent, error)
	GetAgentByPRN(ctx context.Context, prn string) (*models.Agent, error)
	GetAgentsByIDs(ctx context.Context, idList []string) ([]models.Agent, error)
	GetAgents(ctx context.Context, input *GetAgentsInput) (*db.AgentsResult, error)
	CreateAgent(ctx context.Context, input *CreateAgentInput) (*models.Agent, error)
	UpdateAgent(ctx context.Context, input *UpdateAgentInput) (*models.Agent, error)
	DeleteAgent(ctx context.Context, input *DeleteAgentInput) error
	AssignServiceAccountToAgent(ctx context.Context, input *AssignServiceAccountToAgentInput) error
	UnassignServiceAccountFromAgent(ctx context.Context, input *UnassignServiceAccountFromAgentInput) error
	CreateAgentSession(ctx context.Context, input *CreateAgentSessionInput) (*models.AgentSession, error)
	GetAgentSessions(ctx context.Context, input *GetAgentSessionsInput) (*db.AgentSessionsResult, error)
	GetAgentSessionByID(ctx context.Context, id string) (*models.AgentSession, error)
	GetAgentSessionByPRN(ctx context.Context, prn string) (*models.AgentSession, error)
	AcceptAgentSessionHeartbeat(ctx context.Context, sessionID string) error
	CreateAgentSessionError(ctx context.Context, agentSessionID string, message string) error
	ReadAgentSessionErrorLog(ctx context.Context, agentSessionID string, startOffset int, limit int) ([]byte, error)
	SubscribeToAgentSessionErrorLog(ctx context.Context, options *SubscribeToAgentSessionErrorLogInput) (<-chan *logstream.LogEvent, error)
	GetLogStreamsByAgentSessionIDs(ctx context.Context, idList []string) ([]models.LogStream, error)
	SubscribeToAgentSessions(ctx context.Context, options *SubscribeToAgentSessionsInput) (<-chan *SessionEvent, error)
}

type service struct {
	logger           logger.Logger
	dbClient         *db.Client
	limitChecker     limits.LimitChecker
	logStreamManager logstream.Manager
	eventManager     *events.EventManager
	activityService  activityevent.Service
}

// NewService creates an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	limitChecker limits.LimitChecker,
	logStreamManager logstream.Manager,
	eventManager *events.EventManager,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:           logger,
		dbClient:         dbClient,
		limitChecker:     limitChecker,
		logStreamManager: logStreamManager,
		eventManager:     eventManager,
		activityService:  activityService,
	}
}

func (s *service) GetAgentSessionByID(ctx context.Context, id string) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgentSessionByID")
	span.SetAttributes(attribute.String("agentSessionID", id))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	session, err := s.dbClient.AgentSessions.GetAgentSessionByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agent session by ID", errors.WithSpan(span))
	}

	if session == nil {
		return nil, errors.New("agent session with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	agent, err := s.getAgentByID(ctx, span, session.AgentID)
	if err != nil {
		return nil, err
	}

	if err := RequireViewerAccessToAgentResource(ctx, agent); err != nil {
		return nil, err
	}

	return session, nil
}

func (s *service) GetAgentSessionByPRN(ctx context.Context, prn string) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgentSessionByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	session, err := s.dbClient.AgentSessions.GetAgentSessionByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agent session by PRN", errors.WithSpan(span))
	}

	if session == nil {
		return nil, errors.New("agent session with PRN %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	agent, err := s.getAgentByID(ctx, span, session.AgentID)
	if err != nil {
		return nil, err
	}

	if err := RequireViewerAccessToAgentResource(ctx, agent); err != nil {
		return nil, err
	}

	return session, nil
}

func (s *service) GetAgentSessions(ctx context.Context, input *GetAgentSessionsInput) (*db.AgentSessionsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgentSessions")
	span.SetAttributes(attribute.String("agentID", input.AgentID))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	agent, err := s.getAgentByID(ctx, span, input.AgentID)
	if err != nil {
		return nil, err
	}

	if err = RequireViewerAccessToAgentResource(ctx, agent); err != nil {
		return nil, err
	}

	result, err := s.dbClient.AgentSessions.GetAgentSessions(ctx, &db.GetAgentSessionsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.AgentSessionFilter{
			AgentID: &input.AgentID,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agents", errors.WithSpan(span))
	}

	return result, nil
}

func (s *service) AcceptAgentSessionHeartbeat(ctx context.Context, sessionID string) error {
	ctx, span := tracer.Start(ctx, "svc.AcceptAgentSessionHeartbeat")
	span.SetAttributes(attribute.String("sessionID", sessionID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	return s.dbClient.RetryOnOLE(ctx, func() error {
		session, err := s.dbClient.AgentSessions.GetAgentSessionByID(ctx, sessionID)
		if err != nil {
			return errors.Wrap(err, "failed to get agent session by ID", errors.WithSpan(span))
		}

		if session == nil {
			return errors.New("agent session not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
		}

		err = caller.RequirePermission(ctx, models.UpdateAgentSession, auth.WithAgentID(session.AgentID))
		if err != nil {
			return err
		}

		session.LastContactTimestamp = time.Now().UTC()

		_, err = s.dbClient.AgentSessions.UpdateAgentSession(ctx, session)
		if err != nil {
			return err
		}

		return nil
	})
}

func (s *service) CreateAgentSession(ctx context.Context, input *CreateAgentSessionInput) (*models.AgentSession, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateAgentSession")
	span.SetAttributes(attribute.String("agentID", input.AgentID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	agent, err := s.getAgentByID(ctx, span, input.AgentID)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CreateAgentSession, auth.WithAgentID(agent.Metadata.ID))
	if err != nil {
		return nil, err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateAgentSession: %v", txErr)
		}
	}()

	// Execute create first to ensure that no other agents can be created while the transaction is in progress
	session, err := s.dbClient.AgentSessions.CreateAgentSession(txContext, &models.AgentSession{
		AgentID:              agent.Metadata.ID,
		LastContactTimestamp: time.Now().UTC(),
		Internal:             input.Internal,
	})
	if err != nil {
		return nil, err
	}

	// Create log stream for session
	_, err = s.dbClient.LogStreams.CreateLogStream(txContext, &models.LogStream{
		AgentSessionID: &session.Metadata.ID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to create log stream", errors.WithSpan(span))
	}

	// Check how many sessions are currently active for the agent
	activeSessionsResponse, err := s.dbClient.AgentSessions.GetAgentSessions(txContext, &db.GetAgentSessionsInput{
		Filter: &db.AgentSessionFilter{
			AgentID: &agent.Metadata.ID,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agent sessions", errors.WithSpan(span))
	}

	// Check if the sessions per agent limit has been exceeded
	if err := s.limitChecker.CheckLimit(ctx, limits.ResourceLimitSessionsPerAgent, activeSessionsResponse.PageInfo.TotalCount); err != nil {
		if errors.ErrorCode(err) != errors.EInvalid {
			return nil, errors.Wrap(err, "failed to check limit", errors.WithSpan(span))
		}

		// Remove the oldest session
		sortBy := db.AgentSessionSortableFieldLastContactedAtAsc
		oldestSessionResponse, err := s.dbClient.AgentSessions.GetAgentSessions(txContext, &db.GetAgentSessionsInput{
			Sort: &sortBy,
			Filter: &db.AgentSessionFilter{
				AgentID: &agent.Metadata.ID,
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(1),
			},
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to get agent sessions", errors.WithSpan(span))
		}

		if len(oldestSessionResponse.AgentSessions) > 0 {
			oldestSession := oldestSessionResponse.AgentSessions[0]

			// If the oldest session is still active then no more sessions can be created until it's
			// not active anymore
			if oldestSession.Active() {
				return nil, errors.New("too many active sessions", errors.WithSpan(span), errors.WithErrorCode(errors.ETooManyRequests))
			}

			if err = s.dbClient.AgentSessions.DeleteAgentSession(txContext, &oldestSession); err != nil {
				return nil, err
			}
		}
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	return session, nil
}

func (s *service) GetAgentByID(ctx context.Context, id string) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgentByID")
	span.SetAttributes(attribute.String("agentID", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	agent, err := s.getAgentByID(ctx, span, id)
	if err != nil {
		return nil, err
	}

	switch agent.Type {
	case models.OrganizationAgent:
		if aErr := caller.RequireAccessToInheritableResource(ctx, models.AgentResource,
			auth.WithOrganizationID(*agent.OrganizationID),
			auth.WithAgentID(agent.Metadata.ID),
		); aErr != nil {
			return nil, aErr
		}
	case models.SharedAgent:
		// Any authenciated caller can view basic information about an agent.
	default:
		return nil, errors.New("unknown agent type %s", agent.Type)
	}

	return agent, nil
}

func (s *service) GetAgentByPRN(ctx context.Context, prn string) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgentByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	agent, err := s.dbClient.Agents.GetAgentByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agent by PRN", errors.WithSpan(span))
	}

	if agent == nil {
		return nil, errors.New("agent with PRN %s not found", prn, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	switch agent.Type {
	case models.OrganizationAgent:
		if aErr := caller.RequireAccessToInheritableResource(ctx, models.AgentResource,
			auth.WithOrganizationID(*agent.OrganizationID),
			auth.WithAgentID(agent.Metadata.ID),
		); aErr != nil {
			return nil, aErr
		}
	case models.SharedAgent:
		// Any authenciated caller can view basic information about an agent.
	default:
		return nil, errors.New("unknown agent type %s", agent.Type)
	}

	return agent, nil
}

func (s *service) GetAgentsByIDs(ctx context.Context, idList []string) ([]models.Agent, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgentsByIDs")
	span.SetAttributes(attribute.StringSlice("id list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	result, err := s.dbClient.Agents.GetAgents(ctx, &db.GetAgentsInput{
		Filter: &db.AgentFilter{
			AgentIDs: idList,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agents", errors.WithSpan(span))
	}

	for ix := range result.Agents {
		agent := result.Agents[ix]

		switch agent.Type {
		case models.OrganizationAgent:
			if aErr := caller.RequireAccessToInheritableResource(ctx, models.AgentResource,
				auth.WithOrganizationID(*agent.OrganizationID),
				auth.WithAgentID(agent.Metadata.ID),
			); aErr != nil {
				return nil, aErr
			}
		case models.SharedAgent:
			// Any authenciated caller can view basic information about an agent.
		default:
			return nil, errors.New("unknown agent type %s", agent.Type)
		}
	}

	return result.Agents, nil
}

func (s *service) GetAgents(ctx context.Context, input *GetAgentsInput) (*db.AgentsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgents")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if input.OrganizationID != nil {
		err = caller.RequireAccessToInheritableResource(ctx, models.AgentResource, auth.WithOrganizationID(*input.OrganizationID))
		if err != nil {
			return nil, err
		}
	} else if !caller.IsAdmin() {
		// Non admin caller shouldn't be able to access all agents, so require a type.
		if input.AgentType == nil {
			return nil, errors.New(
				"only system admins can view all agents",
				errors.WithErrorCode(errors.EForbidden),
			)
		}

		// Non admin can't retrieve agents for all orgs, so require an organization ID.
		if input.AgentType.Equals(models.OrganizationAgent) {
			return nil, errors.New(
				"an organization ID is required when filtering for organization agents",
				errors.WithErrorCode(errors.EInvalid),
			)
		}
	}

	result, err := s.dbClient.Agents.GetAgents(ctx, &db.GetAgentsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.AgentFilter{
			OrganizationID: input.OrganizationID,
			AgentType:      input.AgentType,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agents", errors.WithSpan(span))
	}

	return result, nil
}

func (s *service) CreateAgent(ctx context.Context, input *CreateAgentInput) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateAgent")
	span.SetAttributes(attribute.String("agentName", input.Name))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	switch input.Type {
	case models.OrganizationAgent:
		if input.OrganizationID == nil {
			return nil, errors.New("organization ID is required for organization agents", errors.WithErrorCode(errors.EInvalid))
		}
		err = caller.RequirePermission(ctx, models.CreateAgent, auth.WithOrganizationID(*input.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.SharedAgent:
		if !caller.IsAdmin() {
			return nil, errors.New("Only system admins can create shared agents", errors.WithErrorCode(errors.EForbidden))
		}
	default:
		return nil, errors.New("invalid agent type", errors.WithErrorCode(errors.EInvalid))
	}

	agentToCreate := models.Agent{
		Type:            input.Type,
		Name:            input.Name,
		Description:     input.Description,
		OrganizationID:  input.OrganizationID,
		RunUntaggedJobs: input.RunUntaggedJobs,
		Tags:            input.Tags,
		Disabled:        input.Disabled,
		CreatedBy:       caller.GetSubject(),
	}

	// Validate model
	if err = agentToCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate agent model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateAgent: %v", txErr)
		}
	}()

	// Store agent in DB
	createdAgent, err := s.dbClient.Agents.CreateAgent(txContext, &agentToCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create agent", errors.WithSpan(span))
	}

	// Only check limit if this is an organization agent
	if input.Type == models.OrganizationAgent {
		// Get the number of agents for the organization to check whether we just violated the limit.
		newAgents, aErr := s.dbClient.Agents.GetAgents(txContext, &db.GetAgentsInput{
			Filter: &db.AgentFilter{
				OrganizationID: createdAgent.OrganizationID,
			},
			PaginationOptions: &pagination.Options{
				First: ptr.Int32(0),
			},
		})
		if aErr != nil {
			return nil, errors.Wrap(aErr, "failed to get organization's agents", errors.WithSpan(span))
		}
		if aErr = s.limitChecker.CheckLimit(txContext, limits.ResourceLimitAgentsPerOrganization, newAgents.PageInfo.TotalCount); aErr != nil {
			return nil, errors.Wrap(aErr, "failed to check limit", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: createdAgent.OrganizationID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetAgent,
			TargetID:       &createdAgent.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created an agent.",
		"caller", caller.GetSubject(),
		"agentType", input.Type,
		"agentName", input.Name,
	)

	return createdAgent, nil
}

func (s *service) UpdateAgent(ctx context.Context, input *UpdateAgentInput) (*models.Agent, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateAgent")
	span.SetAttributes(attribute.String("agentID", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	agent, err := s.getAgentByID(ctx, span, input.ID)
	if err != nil {
		return nil, err
	}

	switch agent.Type {
	case models.OrganizationAgent:
		err = caller.RequirePermission(ctx, models.UpdateAgent, auth.WithOrganizationID(*agent.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.SharedAgent:
		if !caller.IsAdmin() {
			return nil, errors.New(
				"Only system admins can update shared agents",
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	}

	if input.Description != nil {
		agent.Description = *input.Description
	}

	if input.MetadataVersion != nil {
		agent.Metadata.Version = *input.MetadataVersion
	}

	if input.Disabled != nil {
		agent.Disabled = *input.Disabled
	}

	if input.RunUntaggedJobs != nil {
		agent.RunUntaggedJobs = *input.RunUntaggedJobs
	}

	if input.Tags != nil {
		agent.Tags = input.Tags
	}

	// Validate model
	if err = agent.Validate(); err != nil {
		return nil, err
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateAgent: %v", txErr)
		}
	}()

	updatedAgent, err := s.dbClient.Agents.UpdateAgent(txContext, agent)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: updatedAgent.OrganizationID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetAgent,
			TargetID:       &updatedAgent.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated an agent.",
		"caller", caller.GetSubject(),
		"agentName", agent.Name,
		"agentID", agent.Metadata.ID,
	)

	return updatedAgent, nil
}

func (s *service) DeleteAgent(ctx context.Context, input *DeleteAgentInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteAgent")
	span.SetAttributes(attribute.String("agentID", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	agent, err := s.getAgentByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	switch agent.Type {
	case models.OrganizationAgent:
		err = caller.RequirePermission(ctx, models.DeleteAgent, auth.WithOrganizationID(*agent.OrganizationID))
		if err != nil {
			return err
		}
	case models.SharedAgent:
		if !caller.IsAdmin() {
			return errors.New(
				"Only system admins can delete shared agents",
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	}

	if input.MetadataVersion != nil {
		agent.Metadata.Version = *input.MetadataVersion
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteAgent: %v", txErr)
		}
	}()

	err = s.dbClient.Agents.DeleteAgent(txContext, agent)
	if err != nil {
		return err
	}

	var targetType models.ActivityEventTargetType
	switch agent.Type {
	case models.OrganizationAgent:
		targetType = models.TargetOrganization
	case models.SharedAgent:
		targetType = models.TargetGlobal
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: agent.OrganizationID,
			Action:         models.ActionDelete,
			TargetType:     targetType,
			TargetID:       agent.OrganizationID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &agent.Name,
				ID:   agent.Metadata.ID,
				Type: models.TargetAgent.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted an agent.",
		"caller", caller.GetSubject(),
		"agentName", agent.Name,
		"agentID", agent.Metadata.ID,
	)

	return nil
}

func (s *service) AssignServiceAccountToAgent(ctx context.Context, input *AssignServiceAccountToAgentInput) error {
	ctx, span := tracer.Start(ctx, "svc.AssignServiceAccountToAgent")
	span.SetAttributes(attribute.String("serviceAccountID", input.ServiceAccountID))
	span.SetAttributes(attribute.String("agentID", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	agent, err := s.getAgentByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	sa, err := s.dbClient.ServiceAccounts.GetServiceAccountByID(ctx, input.ServiceAccountID)
	if err != nil {
		return errors.Wrap(err, "failed to get service account by ID", errors.WithSpan(span))
	}

	if sa == nil {
		return errors.New("service account not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	// Validate input.
	switch sa.Scope {
	case models.GlobalScope:
		if agent.Type.Equals(models.OrganizationAgent) {
			return errors.New("service account of type global cannot be assigned to organization agent",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	default:
		if agent.Type.Equals(models.SharedAgent) {
			return errors.New("service account of type %s cannot be assigned to shared agent",
				sa.Scope, errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
		if sa.Scope.Equals(models.ProjectScope) {
			return errors.New("a project scoped service account cannot be assigned to an organization agent",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
		if *sa.OrganizationID != *agent.OrganizationID {
			return errors.New("service account does not belong to the same organization as the agent",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	if agent.Type.Equals(models.OrganizationAgent) {
		err = caller.RequirePermission(ctx, models.UpdateAgent, auth.WithOrganizationID(*agent.OrganizationID))
		if err != nil {
			return err
		}
	} else {
		if !caller.IsAdmin() {
			return errors.New(
				"Only system admins can assign service accounts to shared agents",
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	}

	err = s.dbClient.ServiceAccounts.AssignServiceAccountToAgent(ctx, input.ServiceAccountID, input.ID)
	if err != nil {
		return errors.Wrap(err, "failed to assign service account to agent", errors.WithSpan(span))
	}

	return nil
}

func (s *service) UnassignServiceAccountFromAgent(ctx context.Context, input *UnassignServiceAccountFromAgentInput) error {
	ctx, span := tracer.Start(ctx, "svc.UnassignServiceAccountFromAgent")
	span.SetAttributes(attribute.String("serviceAccountID", input.ServiceAccountID))
	span.SetAttributes(attribute.String("agentID", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	agent, err := s.getAgentByID(ctx, span, input.ID)
	if err != nil {
		return err
	}

	if agent.Type.Equals(models.OrganizationAgent) {
		err = caller.RequirePermission(ctx, models.UpdateAgent, auth.WithOrganizationID(*agent.OrganizationID))
		if err != nil {
			return err
		}
	} else {
		if !caller.IsAdmin() {
			return errors.New(
				"Only system admins can un-assign service accounts from shared agents",
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	}

	err = s.dbClient.ServiceAccounts.UnassignServiceAccountFromAgent(ctx, input.ServiceAccountID, input.ID)
	if err != nil {
		return errors.Wrap(err, "failed to unassign service account from agent", errors.WithSpan(span))
	}

	return nil
}

func (s *service) CreateAgentSessionError(ctx context.Context, agentSessionID string, message string) error {
	ctx, span := tracer.Start(ctx, "svc.CreateAgentSessionError")
	span.SetAttributes(attribute.String("agent_session_id", agentSessionID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	return s.dbClient.RetryOnOLE(ctx, func() error {
		session, err := s.getAgentSessionByID(ctx, span, agentSessionID)
		if err != nil {
			return err
		}

		err = caller.RequirePermission(ctx, models.UpdateAgentSession, auth.WithAgentID(session.AgentID))
		if err != nil {
			return err
		}

		// Find log stream associated with agent session
		stream, err := s.dbClient.LogStreams.GetLogStreamByAgentSessionID(ctx, agentSessionID)
		if err != nil {
			return err
		}
		if stream == nil {
			return errors.New("log stream not found for agent session: %s", agentSessionID)
		}

		timestamp := agentLogsTimestampColor.Sprintf("%s:", time.Now().UTC().Format(time.RFC3339Nano))

		buf := []byte(fmt.Sprintf("%s %s\n", timestamp, message))

		// Check if the new logs will exceed the limit
		if (stream.Size + len(buf)) > agentErrorLogsBytesLimit {
			return errors.New("agent session error log size limit exceeded", errors.WithErrorCode(errors.ETooLarge))
		}

		txContext, err := s.dbClient.Transactions.BeginTx(ctx)
		if err != nil {
			return err
		}

		defer func() {
			if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
				s.logger.Errorf("failed to rollback tx: %v", txErr)
			}
		}()

		// Update session error count
		session.ErrorCount++
		_, err = s.dbClient.AgentSessions.UpdateAgentSession(txContext, session)
		if err != nil {
			return err
		}

		// Write logs to store
		_, err = s.logStreamManager.WriteLogs(txContext, stream.Metadata.ID, stream.Size, buf)
		if err != nil {
			return err
		}

		return s.dbClient.Transactions.CommitTx(txContext)
	})
}

func (s *service) ReadAgentSessionErrorLog(ctx context.Context, agentSessionID string, startOffset int, limit int) ([]byte, error) {
	ctx, span := tracer.Start(ctx, "svc.ReadAgentSessionErrorLog")
	span.SetAttributes(attribute.String("agent_session_id", agentSessionID))
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	session, err := s.getAgentSessionByID(ctx, span, agentSessionID)
	if err != nil {
		return nil, err
	}

	agent, err := s.getAgentByID(ctx, span, session.AgentID)
	if err != nil {
		return nil, err
	}

	if err = RequireViewerAccessToAgentResource(ctx, agent); err != nil {
		return nil, err
	}

	// Find log stream associated with agent session
	stream, err := s.dbClient.LogStreams.GetLogStreamByAgentSessionID(ctx, agentSessionID)
	if err != nil {
		return nil, err
	}
	if stream == nil {
		return nil, errors.New("log stream not found for agent session: %s", agentSessionID)
	}

	return s.logStreamManager.ReadLogs(ctx, stream.Metadata.ID, startOffset, limit)
}

func (s *service) SubscribeToAgentSessionErrorLog(ctx context.Context, options *SubscribeToAgentSessionErrorLogInput) (<-chan *logstream.LogEvent, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToAgentSessionErrorLog")
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	session, err := s.getAgentSessionByID(ctx, span, options.AgentSessionID)
	if err != nil {
		return nil, err
	}

	agent, err := s.getAgentByID(ctx, span, session.AgentID)
	if err != nil {
		return nil, err
	}

	if err = RequireViewerAccessToAgentResource(ctx, agent); err != nil {
		return nil, err
	}

	logStream, err := s.dbClient.LogStreams.GetLogStreamByAgentSessionID(ctx, session.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get log stream by agent session ID", errors.WithSpan(span))
	}

	if logStream == nil {
		return nil, fmt.Errorf("log stream not found for agent session %s", session.Metadata.ID)
	}

	return s.logStreamManager.Subscribe(ctx, &logstream.SubscriptionOptions{
		LastSeenLogSize: options.LastSeenLogSize,
		LogStreamID:     logStream.Metadata.ID,
	})
}

func (s *service) GetLogStreamsByAgentSessionIDs(ctx context.Context, idList []string) ([]models.LogStream, error) {
	ctx, span := tracer.Start(ctx, "svc.GetAgentLogStreamsByIDs")
	defer span.End()

	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	if len(idList) == 0 {
		return []models.LogStream{}, nil
	}

	agentSessionsResp, err := s.dbClient.AgentSessions.GetAgentSessions(ctx, &db.GetAgentSessionsInput{
		Filter: &db.AgentSessionFilter{
			AgentSessionIDs: idList,
		},
	})
	if err != nil {
		return nil, err
	}

	agentIDMap := map[string]struct{}{}
	for _, session := range agentSessionsResp.AgentSessions {
		agentIDMap[session.AgentID] = struct{}{}
	}

	agentIDs := make([]string, 0, len(agentIDMap))
	for agentID := range agentIDMap {
		agentIDs = append(agentIDs, agentID)
	}

	agentsResp, err := s.dbClient.Agents.GetAgents(ctx, &db.GetAgentsInput{
		Filter: &db.AgentFilter{
			AgentIDs: agentIDs,
		},
	})
	if err != nil {
		return nil, err
	}

	// Verify caller has access to all agents
	for ix := range agentsResp.Agents {
		if err = RequireViewerAccessToAgentResource(ctx, &agentsResp.Agents[ix]); err != nil {
			return nil, err
		}
	}

	result, err := s.dbClient.LogStreams.GetLogStreams(ctx, &db.GetLogStreamsInput{
		Filter: &db.LogStreamFilter{
			AgentSessionIDs: idList,
		},
	})
	if err != nil {
		return nil, err
	}

	return result.LogStreams, nil
}

func (s *service) SubscribeToAgentSessions(ctx context.Context, options *SubscribeToAgentSessionsInput) (<-chan *SessionEvent, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToAgentSessions")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if options.OrganizationID != nil {
		err = caller.RequireAccessToInheritableResource(ctx, models.AgentResource, auth.WithOrganizationID(*options.OrganizationID))
		if err != nil {
			return nil, err
		}
	} else if options.AgentID != nil {
		agent, err := s.getAgentByID(ctx, span, *options.AgentID)
		if err != nil {
			return nil, err
		}
		if err = RequireViewerAccessToAgentResource(ctx, agent); err != nil {
			return nil, err
		}
	} else if !caller.IsAdmin() {
		return nil, errors.New(
			"Only system admins can subscribe to all agent sessions",
			errors.WithErrorCode(errors.EForbidden),
		)
	}

	subscription := events.Subscription{
		Type: events.AgentSessionSubscription,
		Actions: []events.SubscriptionAction{
			events.CreateAction,
			events.UpdateAction,
		},
	}

	subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})

	outgoing := make(chan *SessionEvent)
	go func() {
		// Defer close of outgoing channel
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		// Wait for agent session updates
		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) {
					s.logger.Errorf("Error occurred while waiting for agent session events: %v", err)
				}
				return
			}

			eventData, err := event.ToAgentSessionEventData()
			if err != nil {
				s.logger.Errorf("failed to get agent session event data in agent session subscription: %v", err)
				continue
			}

			// Check if this event is for the agent we're interested in
			if options.AgentID != nil && *options.AgentID != eventData.AgentID {
				continue
			}

			if options.OrganizationID != nil || options.AgentID != nil {
				// We need to query the agent to check if it belongs to the organization
				agent, gErr := s.getAgentByID(ctx, span, eventData.AgentID)
				if gErr != nil {
					s.logger.Errorf("Error querying for agent in subscription goroutine: %v", err)
					continue
				}
				if options.OrganizationID != nil && *agent.OrganizationID != *options.OrganizationID {
					continue
				}
				if options.AgentType != nil && agent.Type != *options.AgentType {
					continue
				}
			}

			session, err := s.dbClient.AgentSessions.GetAgentSessionByID(ctx, event.ID)
			if err != nil {
				s.logger.Errorf("Error querying for agent session in subscription goroutine: %v", err)
				continue
			}
			if session == nil {
				s.logger.Errorf("Received event for agent session that does not exist %s", event.ID)
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- &SessionEvent{AgentSession: session, Action: event.Action}:
			}
		}
	}()

	return outgoing, nil
}

func (s *service) getAgentByID(ctx context.Context, span trace.Span, id string) (*models.Agent, error) {
	agent, err := s.dbClient.Agents.GetAgentByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agent by ID", errors.WithSpan(span))
	}

	if agent == nil {
		return nil, errors.New("agent with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return agent, nil
}

func (s *service) getAgentSessionByID(ctx context.Context, span trace.Span, id string) (*models.AgentSession, error) {
	session, err := s.dbClient.AgentSessions.GetAgentSessionByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agent session by ID", errors.WithSpan(span))
	}

	if session == nil {
		return nil, errors.New("agent session with ID %s not found", id, errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	return session, nil
}

// RequireViewerAccessToAgentResource checks if the caller has viewer access to an
// agent's resource like jobs, sessions, error logs, etc.
func RequireViewerAccessToAgentResource(ctx context.Context, agent *models.Agent) error {
	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	switch agent.Type {
	case models.OrganizationAgent:
		err := caller.RequireAccessToInheritableResource(ctx, models.AgentResource,
			auth.WithOrganizationID(*agent.OrganizationID),
			auth.WithAgentID(agent.Metadata.ID),
		)
		if err != nil {
			return err
		}
	case models.SharedAgent:
		if !caller.IsAdmin() {
			return errors.New(
				"Only system admins can access shared agent's resources like jobs, sessions and logs",
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	default:
		return errors.New("unknown agent type %s", agent.Type)
	}

	return nil
}
