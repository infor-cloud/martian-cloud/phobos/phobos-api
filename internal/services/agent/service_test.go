package agent

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	limitChecker := limits.NewMockLimitChecker(t)
	logStreamManager := logstream.NewMockManager(t)
	eventManager := events.NewEventManager(dbClient, logger)
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:           logger,
		dbClient:         dbClient,
		limitChecker:     limitChecker,
		logStreamManager: logStreamManager,
		eventManager:     eventManager,
		activityService:  activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, limitChecker, logStreamManager, eventManager, activityService))
}

func TestGetAgentByID(t *testing.T) {
	agentID := "agent-1"
	organizationID := "organization-1"
	// Test cases
	tests := []struct {
		expectAgent   *models.Agent
		name          string
		authError     error
		expectErrCode errors.CodeType
	}{
		{
			name: "any user can query for a shared agent",
			expectAgent: &models.Agent{
				Metadata: models.ResourceMetadata{ID: agentID},
				Name:     "test-agent",
				Type:     models.SharedAgent,
			},
		},
		{
			name: "get organization agent",
			expectAgent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: agentID},
				OrganizationID: &organizationID,
				Name:           "test-agent",
				Type:           models.OrganizationAgent,
			},
		},
		{
			name: "subject does not have access to organization agent",
			expectAgent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: agentID},
				OrganizationID: &organizationID,
				Name:           "test-agent",
				Type:           models.OrganizationAgent,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)

			if test.expectAgent != nil {
				if test.expectAgent.Type.Equals(models.OrganizationAgent) {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				}
			}

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.expectAgent, nil)

			dbClient := db.Client{
				Agents: mockAgents,
			}

			service := &service{
				dbClient: &dbClient,
			}

			agent, err := service.GetAgentByID(auth.WithCaller(ctx, mockCaller), agentID)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectAgent, agent)
		})
	}
}

func TestGetAgentByPRN(t *testing.T) {
	agentPRN := "prn:agent:some-agent" // Note: PRN not necessarily reflective of agent type.
	organizationID := "organization-1"
	// Test cases
	tests := []struct {
		expectAgent   *models.Agent
		name          string
		authError     error
		expectErrCode errors.CodeType
	}{
		{
			name: "any user can query for a shared agent",
			expectAgent: &models.Agent{
				Metadata: models.ResourceMetadata{PRN: agentPRN},
				Name:     "test-agent",
				Type:     models.SharedAgent,
			},
		},
		{
			name: "get organization agent",
			expectAgent: &models.Agent{
				Metadata:       models.ResourceMetadata{PRN: agentPRN},
				OrganizationID: &organizationID,
				Name:           "test-agent",
				Type:           models.OrganizationAgent,
			},
		},
		{
			name: "subject does not have access to organization agent",
			expectAgent: &models.Agent{
				Metadata:       models.ResourceMetadata{PRN: agentPRN},
				OrganizationID: &organizationID,
				Name:           "test-agent",
				Type:           models.OrganizationAgent,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)

			if test.expectAgent != nil {

				if test.expectAgent.Type.Equals(models.OrganizationAgent) {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				}
			}

			mockAgents.On("GetAgentByPRN", mock.Anything, agentPRN).Return(test.expectAgent, nil)

			dbClient := db.Client{
				Agents: mockAgents,
			}

			service := &service{
				dbClient: &dbClient,
			}

			agent, err := service.GetAgentByPRN(auth.WithCaller(ctx, mockCaller), agentPRN)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectAgent, agent)
		})
	}
}

func TestGetAgentsByIDs(t *testing.T) {
	agentID := "agent-1"
	organizationID := "organization-1"
	// Test cases
	tests := []struct {
		expectAgent   *models.Agent
		name          string
		authError     error
		expectErrCode errors.CodeType
	}{
		{
			name: "any user can query for a shared agent",
			expectAgent: &models.Agent{
				Metadata: models.ResourceMetadata{ID: agentID},
				Name:     "test-agent",
				Type:     models.SharedAgent,
			},
		},
		{
			name: "get organization agent",
			expectAgent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: agentID},
				OrganizationID: &organizationID,
				Name:           "test-agent",
				Type:           models.OrganizationAgent,
			},
		},
		{
			name: "subject does not have access to organization agent",
			expectAgent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: agentID},
				OrganizationID: &organizationID,
				Name:           "test-agent",
				Type:           models.OrganizationAgent,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "agent not found",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)

			if test.expectAgent != nil {
				if test.expectAgent.Type.Equals(models.OrganizationAgent) {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				}
			}

			getAgentsResponse := db.AgentsResult{
				Agents: []models.Agent{},
			}

			if test.expectAgent != nil {
				getAgentsResponse.Agents = append(getAgentsResponse.Agents, *test.expectAgent)
			}

			mockAgents.On("GetAgents", mock.Anything, &db.GetAgentsInput{
				Filter: &db.AgentFilter{
					AgentIDs: []string{agentID},
				},
			}).Return(&getAgentsResponse, nil)

			dbClient := db.Client{
				Agents: mockAgents,
			}

			service := &service{
				dbClient: &dbClient,
			}

			agents, err := service.GetAgentsByIDs(auth.WithCaller(ctx, mockCaller), []string{agentID})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			if test.expectAgent != nil {
				assert.Equal(t, 1, len(agents))
				assert.Equal(t, test.expectAgent, &agents[0])
			} else {
				assert.Equal(t, 0, len(agents))
			}
		})
	}
}

func TestGetAgents(t *testing.T) {
	sampleOrganization := &models.Organization{
		Metadata: models.ResourceMetadata{ID: "organization-1"},
	}

	// Test cases
	tests := []struct {
		expectAgent   *models.Agent
		input         *GetAgentsInput
		name          string
		authError     error
		expectErrCode errors.CodeType
		isAdmin       bool
	}{
		{
			name:  "successfully get agents for an organization",
			input: &GetAgentsInput{OrganizationID: &sampleOrganization.Metadata.ID},
			expectAgent: &models.Agent{
				Type:           models.OrganizationAgent,
				Metadata:       models.ResourceMetadata{ID: "agent-1"},
				OrganizationID: &sampleOrganization.Metadata.ID,
				Name:           "test-agent",
			},
		},
		{
			name:  "successfully get all agents",
			input: &GetAgentsInput{},
			expectAgent: &models.Agent{
				Type:     models.SharedAgent,
				Metadata: models.ResourceMetadata{ID: "agent-1"},
				Name:     "test-agent",
			},
			isAdmin: true,
		},
		{
			name:          "subject is not a member of the organization or project",
			input:         &GetAgentsInput{OrganizationID: &sampleOrganization.Metadata.ID},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "cannot query for all agents since subject is not an admin",
			input:         &GetAgentsInput{},
			expectErrCode: errors.EForbidden,
		},
		{
			name:  "no agents matching filters",
			input: &GetAgentsInput{OrganizationID: &sampleOrganization.Metadata.ID},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)

			if test.input.OrganizationID != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
					Return(test.authError)
			} else {
				mockCaller.On("IsAdmin").Return(test.isAdmin)
			}

			getAgentsResponse := db.AgentsResult{
				Agents: []models.Agent{},
			}

			if test.expectAgent != nil {
				getAgentsResponse.Agents = append(getAgentsResponse.Agents, *test.expectAgent)
			}

			mockAgents.On("GetAgents", mock.Anything, &db.GetAgentsInput{
				Filter: &db.AgentFilter{
					OrganizationID: test.input.OrganizationID,
					AgentType:      test.input.AgentType,
				},
			}).Return(&getAgentsResponse, nil).Maybe()

			dbClient := db.Client{
				Agents: mockAgents,
			}

			service := &service{
				dbClient: &dbClient,
			}

			resp, err := service.GetAgents(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			if test.expectAgent != nil {
				assert.Equal(t, 1, len(resp.Agents))
				assert.Equal(t, test.expectAgent, &resp.Agents[0])
			} else {
				assert.Equal(t, 0, len(resp.Agents))
			}
		})
	}
}

func TestCreateAgent(t *testing.T) {
	agentName := "test-agent"
	testSubject := "test-subject"

	// Test cases
	tests := []struct {
		authError          error
		limitError         error
		input              *CreateAgentInput
		expectCreatedAgent *models.Agent
		name               string
		expectErrCode      errors.CodeType
		isAdmin            bool
	}{
		{
			name: "create organization agent",
			input: &CreateAgentInput{
				Type:            models.OrganizationAgent,
				Name:            agentName,
				OrganizationID:  ptr.String("organization-1"),
				Description:     "test description",
				Tags:            []string{"golang"},
				RunUntaggedJobs: true,
			},
			expectCreatedAgent: &models.Agent{
				Type:            models.OrganizationAgent,
				Name:            agentName,
				OrganizationID:  ptr.String("organization-1"),
				CreatedBy:       testSubject,
				Description:     "test description",
				Tags:            []string{"golang"},
				RunUntaggedJobs: true,
			},
		},
		{
			name: "create shared agent",
			input: &CreateAgentInput{
				Type:            models.SharedAgent,
				Name:            agentName,
				Description:     "test description",
				RunUntaggedJobs: true,
			},
			expectCreatedAgent: &models.Agent{
				Type:            models.SharedAgent,
				Name:            agentName,
				CreatedBy:       testSubject,
				Description:     "test description",
				RunUntaggedJobs: true,
			},
			isAdmin: true,
		},
		{
			name: "subject does not have permissions to create an organization agent",
			input: &CreateAgentInput{
				Type:            models.OrganizationAgent,
				Name:            agentName,
				OrganizationID:  ptr.String("organization-1"),
				RunUntaggedJobs: true,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permission to create shared agent",
			input: &CreateAgentInput{
				Type:            models.SharedAgent,
				Name:            agentName,
				Description:     "test description",
				RunUntaggedJobs: true,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name: "exceeds limit",
			input: &CreateAgentInput{
				Type:            models.OrganizationAgent,
				Name:            agentName,
				OrganizationID:  ptr.String("organization-1"),
				RunUntaggedJobs: true,
			},
			expectCreatedAgent: &models.Agent{
				Type:            models.OrganizationAgent,
				Name:            agentName,
				OrganizationID:  ptr.String("organization-1"),
				CreatedBy:       testSubject,
				RunUntaggedJobs: true,
			},
			limitError:    errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			expectErrCode: errors.EInvalid,
		},
		{
			name: "runUntaggedJobs cannot be false when no tags are specified",
			input: &CreateAgentInput{
				Type:            models.OrganizationAgent,
				Name:            agentName,
				OrganizationID:  ptr.String("organization-1"),
				RunUntaggedJobs: false,
			},
			expectErrCode: errors.EInvalid,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.CreateAgent, mock.Anything).Return(test.authError).Maybe()
			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()

			mockAgents.On("GetAgents", mock.Anything, &db.GetAgentsInput{
				Filter: &db.AgentFilter{
					OrganizationID: test.input.OrganizationID,
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
			}).Return(&db.AgentsResult{
				PageInfo: &pagination.PageInfo{},
			}, nil).Maybe()

			mockLimitChecker.On("CheckLimit", mock.Anything,
				limits.ResourceLimitAgentsPerOrganization, int32(0)).
				Return(test.limitError).Maybe()

			if test.expectErrCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: test.expectCreatedAgent.OrganizationID,
						Action:         models.ActionCreate,
						TargetType:     models.TargetAgent,
						TargetID:       &test.expectCreatedAgent.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			mockCaller.On("GetSubject").Return("mockSubject").Maybe()

			if test.expectCreatedAgent != nil {
				mockAgents.On("CreateAgent", mock.Anything, mock.Anything).Return(test.expectCreatedAgent, nil)
			}

			dbClient := &db.Client{
				Transactions: mockTransactions,
				Agents:       mockAgents,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				logger:          testLogger,
				dbClient:        dbClient,
				limitChecker:    mockLimitChecker,
				activityService: mockActivityEvents,
			}

			agent, err := service.CreateAgent(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreatedAgent, agent)
		})
	}
}

func TestUpdateAgent(t *testing.T) {
	agentID := "agent123"
	organizationID := "organization123"

	// Test cases
	tests := []struct {
		authError     error
		updateInput   *UpdateAgentInput
		existingAgent *models.Agent
		expectUpdated *models.Agent
		name          string
		expectErrCode errors.CodeType
	}{
		{
			name: "successfully update an agent",
			updateInput: &UpdateAgentInput{
				ID:          agentID,
				Description: ptr.String("updated description"),
				Disabled:    ptr.Bool(true),
				Tags:        []string{"golang"},
			},
			existingAgent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type:            models.OrganizationAgent,
				Name:            "agent1",
				OrganizationID:  &organizationID,
				Description:     "test description",
				Disabled:        false,
				RunUntaggedJobs: true,
			},
			expectUpdated: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type:            models.OrganizationAgent,
				Name:            "agent1",
				OrganizationID:  &organizationID,
				Description:     "updated description",
				Disabled:        true,
				RunUntaggedJobs: true,
				Tags:            []string{"golang"},
			},
		},
		{
			name: "agent not found",
			updateInput: &UpdateAgentInput{
				ID: agentID,
			},
			expectErrCode: errors.ENotFound,
		},
		{
			name: "runUntaggedJobs cannot be false if no tags are specified",
			updateInput: &UpdateAgentInput{
				ID:              agentID,
				RunUntaggedJobs: ptr.Bool(false),
			},
			existingAgent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type:            models.OrganizationAgent,
				Name:            "agent1",
				OrganizationID:  &organizationID,
				Description:     "test description",
				Disabled:        false,
				RunUntaggedJobs: true,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name: "subject does not have permissions to update an agent",
			updateInput: &UpdateAgentInput{
				ID: agentID,
			},
			existingAgent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type:            models.OrganizationAgent,
				Name:            "agent1",
				OrganizationID:  &organizationID,
				Description:     "test description",
				Disabled:        false,
				RunUntaggedJobs: true,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.existingAgent, nil)

			if test.existingAgent != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.UpdateAgent, mock.Anything).Return(test.authError)
			}

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			if test.expectUpdated != nil {
				mockCaller.On("GetSubject").Return("mockSubject")
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockAgents.On("UpdateAgent", mock.Anything, test.expectUpdated).Return(test.expectUpdated, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: test.expectUpdated.OrganizationID,
						Action:         models.ActionUpdate,
						TargetType:     models.TargetAgent,
						TargetID:       &test.expectUpdated.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := db.Client{
				Agents:       mockAgents,
				Transactions: mockTransactions,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				logger:          testLogger,
				dbClient:        &dbClient,
				activityService: mockActivityEvents,
			}

			agent, err := service.UpdateAgent(auth.WithCaller(ctx, mockCaller), test.updateInput)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingAgent, agent)
		})
	}
}

func TestDeleteAgent(t *testing.T) {
	organizationID := "organization123"
	agentID := "agent123"

	sampleAgent := &models.Agent{
		Metadata: models.ResourceMetadata{
			ID: agentID,
		},
		Type:           models.OrganizationAgent,
		Name:           "agent1",
		OrganizationID: &organizationID,
		Description:    "test description",
		Disabled:       false,
	}

	// Test cases
	tests := []struct {
		authError     error
		existingAgent *models.Agent
		name          string
		expectErrCode errors.CodeType
	}{
		{
			name:          "successfully delete an agent",
			existingAgent: sampleAgent,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name:          "subject does not have permissions to delete an agent",
			existingAgent: sampleAgent,
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.existingAgent, nil)

			if test.existingAgent != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.DeleteAgent, mock.Anything).Return(test.authError)
			}

			if test.expectErrCode == "" {
				mockCaller.On("GetSubject").Return("mockSubject")

				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockAgents.On("DeleteAgent", mock.Anything, test.existingAgent).Return(nil)

				var targetType models.ActivityEventTargetType
				switch test.existingAgent.Type {
				case models.OrganizationAgent:
					targetType = models.TargetOrganization
				case models.SharedAgent:
					targetType = models.TargetGlobal
				}

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: test.existingAgent.OrganizationID,
						Action:         models.ActionDelete,
						TargetType:     targetType,
						TargetID:       test.existingAgent.OrganizationID,
						Payload: &models.ActivityEventDeleteResourcePayload{
							Name: &test.existingAgent.Name,
							ID:   test.existingAgent.Metadata.ID,
							Type: models.TargetAgent.String(),
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Agents:       mockAgents,
				Transactions: mockTransactions,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				logger:          testLogger,
				dbClient:        dbClient,
				activityService: mockActivityEvents,
			}

			err := service.DeleteAgent(auth.WithCaller(ctx, mockCaller), &DeleteAgentInput{ID: agentID})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestAssignServiceAccountToAgent(t *testing.T) {
	organizationID := "organization123"
	projectID := "project456"

	type testCase struct {
		name           string
		agent          *models.Agent
		serviceAccount *models.ServiceAccount
		authError      error
		expectErrCode  errors.CodeType
		isAdmin        bool
	}

	testCases := []testCase{
		{
			name: "successfully assign organization service account to organization agent",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			serviceAccount: &models.ServiceAccount{
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
		},
		{
			name: "successfully assign global service account to shared agent",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			isAdmin: true,
		},
		{
			name: "cannot assign organization service account to shared agent",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			serviceAccount: &models.ServiceAccount{
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name: "cannot assign global service account to organization agent",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name: "organization service account and organization agent are not in the same organization",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			serviceAccount: &models.ServiceAccount{
				OrganizationID: ptr.String("organization456"),
				Scope:          models.OrganizationScope,
			},
			expectErrCode: errors.EInvalid,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "service account not found",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			expectErrCode: errors.ENotFound,
		},
		{
			name: "global service account cannot be assigned to a shared agent, since caller is not an admin",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subject does not have permissions to assign a organization service account to a organization agent",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			serviceAccount: &models.ServiceAccount{
				OrganizationID: &organizationID,
				Scope:          models.OrganizationScope,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "global service account cannot be assigned to a shared agent, since caller is not a user",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			serviceAccount: &models.ServiceAccount{
				Scope: models.GlobalScope,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name: "project-scoped service account cannot be assigned to an organization agent",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			serviceAccount: &models.ServiceAccount{
				Scope:          models.ProjectScope,
				OrganizationID: &organizationID,
				ProjectID:      &projectID,
			},
			expectErrCode: errors.EInvalid,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockAgents.On("GetAgentByID", mock.Anything, mock.Anything).Return(test.agent, nil)

			if test.agent != nil {
				mockServiceAccounts.On("GetServiceAccountByID", mock.Anything, mock.Anything).Return(test.serviceAccount, nil)
			}

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateAgent, mock.Anything).Return(test.authError).Maybe()
			mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()

			if test.expectErrCode == "" {
				mockServiceAccounts.On("AssignServiceAccountToAgent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			}

			dbClient := &db.Client{
				Agents:          mockAgents,
				ServiceAccounts: mockServiceAccounts,
			}

			service := &service{
				dbClient: dbClient,
			}

			err := service.AssignServiceAccountToAgent(auth.WithCaller(ctx, mockCaller), &AssignServiceAccountToAgentInput{ID: "agent-1", ServiceAccountID: "sa-1"})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestUnassignServiceAccountFromAgent(t *testing.T) {
	organizationID := "organization123"

	type testCase struct {
		name          string
		agent         *models.Agent
		authError     error
		expectErrCode errors.CodeType
		isAdmin       bool
	}

	testCases := []testCase{
		{
			name: "successfully unassign service account from shared agent",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name: "successfully unassign service account from organization agent",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
		},
		{
			name: "subject does not have permissions to unassign a service account from a organization agent",
			agent: &models.Agent{
				OrganizationID: &organizationID,
				Type:           models.OrganizationAgent,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "global service account cannot be unassigned from a shared agent, since caller is not an admin",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
			isAdmin:       false,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockServiceAccounts := db.NewMockServiceAccounts(t)

			mockAgents.On("GetAgentByID", mock.Anything, mock.Anything).Return(test.agent, nil)

			if test.agent != nil {
				if test.agent.Type.Equals(models.OrganizationAgent) {
					mockCaller.On("RequirePermission", mock.Anything, models.UpdateAgent, mock.Anything).Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			if test.expectErrCode == "" {
				mockServiceAccounts.On("UnassignServiceAccountFromAgent", mock.Anything, mock.Anything, mock.Anything).Return(nil)
			}

			dbClient := &db.Client{
				Agents:          mockAgents,
				ServiceAccounts: mockServiceAccounts,
			}

			service := &service{
				dbClient: dbClient,
			}

			err := service.UnassignServiceAccountFromAgent(auth.WithCaller(ctx, mockCaller), &UnassignServiceAccountFromAgentInput{ID: "agent-1", ServiceAccountID: "sa-1"})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestCreateAgentSession(t *testing.T) {
	agentID := "agent123"
	agentSessionID := "agent-session-123"

	// Test cases
	tests := []struct {
		authError                 error
		limitError                error
		input                     *CreateAgentSessionInput
		createdAgentSession       *models.AgentSession
		name                      string
		expectErrCode             errors.CodeType
		allExistingSessionsActive bool
	}{
		{
			name: "create agent session",
			input: &CreateAgentSessionInput{
				AgentID: agentID,
			},
			createdAgentSession: &models.AgentSession{
				Metadata: models.ResourceMetadata{ID: agentSessionID},
				AgentID:  agentID,
			},
		},
		{
			name: "subject does not have permissions to create an agent",
			input: &CreateAgentSessionInput{
				AgentID: agentID,
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "oldest session should be deleted when limit is reached",
			input: &CreateAgentSessionInput{
				AgentID: agentID,
			},
			createdAgentSession: &models.AgentSession{
				Metadata: models.ResourceMetadata{ID: agentSessionID},
				AgentID:  agentID,
			},
			limitError: errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
		},
		{
			name: "create session should fail because limit is reached and all sessions are currently active",
			input: &CreateAgentSessionInput{
				AgentID: agentID,
			},
			createdAgentSession: &models.AgentSession{
				Metadata: models.ResourceMetadata{ID: agentSessionID},
				AgentID:  agentID,
			},
			allExistingSessionsActive: true,
			limitError:                errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			expectErrCode:             errors.ETooManyRequests,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)
			mockLogStreams := db.NewMockLogStreams(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)

			mockCaller.On("RequirePermission", mock.Anything, models.CreateAgentSession, mock.Anything).Return(test.authError)

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(&models.Agent{
				Metadata: models.ResourceMetadata{ID: agentID},
			}, nil)

			mockAgentSessions.On("GetAgentSessions", mock.Anything, &db.GetAgentSessionsInput{
				Filter: &db.AgentSessionFilter{
					AgentID: &agentID,
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
			}).Return(&db.AgentSessionsResult{
				PageInfo: &pagination.PageInfo{},
			}, nil).Maybe()

			mockLimitChecker.On("CheckLimit", mock.Anything,
				limits.ResourceLimitSessionsPerAgent, int32(0)).
				Return(test.limitError).Maybe()

			if test.createdAgentSession != nil {
				mockAgentSessions.On("CreateAgentSession", mock.Anything, mock.Anything).Return(test.createdAgentSession, nil)

				sessionID := test.createdAgentSession.Metadata.ID
				mockLogStreams.On("CreateLogStream", mock.Anything, &models.LogStream{
					AgentSessionID: &sessionID,
				}).Return(nil, nil)
			}

			if test.limitError != nil {
				existingSession := models.AgentSession{
					Metadata: models.ResourceMetadata{ID: "existing-session"},
					AgentID:  agentID,
				}

				if test.allExistingSessionsActive {
					existingSession.LastContactTimestamp = time.Now().UTC()
				}

				sortBy := db.AgentSessionSortableFieldLastContactedAtAsc
				mockAgentSessions.On("GetAgentSessions", mock.Anything, &db.GetAgentSessionsInput{
					Sort: &sortBy,
					Filter: &db.AgentSessionFilter{
						AgentID: &agentID,
					},
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(1),
					},
				}).Return(&db.AgentSessionsResult{
					PageInfo:      &pagination.PageInfo{},
					AgentSessions: []models.AgentSession{existingSession},
				}, nil)

				if !test.allExistingSessionsActive {
					mockAgentSessions.On("DeleteAgentSession", mock.Anything, &existingSession).Return(nil)
				}
			}

			if test.authError == nil && test.expectErrCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			dbClient := &db.Client{
				Transactions:  mockTransactions,
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
				LogStreams:    mockLogStreams,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				logger:       testLogger,
				dbClient:     dbClient,
				limitChecker: mockLimitChecker,
			}

			session, err := service.CreateAgentSession(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.createdAgentSession, session)
		})
	}
}

func TestGetAgentSessions(t *testing.T) {
	agentID := "agent123"

	// Test cases
	tests := []struct {
		input         *GetAgentSessionsInput
		agent         *models.Agent
		name          string
		authError     error
		expectErrCode errors.CodeType
		isAdmin       bool
	}{
		{
			name:  "successfully get sessions for an organization agent",
			input: &GetAgentSessionsInput{AgentID: agentID},
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
		},
		{
			name:  "successfully get sessiosn for a shared agent",
			input: &GetAgentSessionsInput{AgentID: agentID},
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name:  "subject is not authorized to query sessions for an organization agent",
			input: &GetAgentSessionsInput{AgentID: agentID},
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			input:         &GetAgentSessionsInput{AgentID: agentID},
			expectErrCode: errors.ENotFound,
		},
		{
			name:  "cannot query sessions for a shared agent because subject is not an admin",
			input: &GetAgentSessionsInput{AgentID: agentID},
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.agent, nil)

			if test.agent != nil {
				if test.agent.Type == models.OrganizationAgent {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			mockAgentSessions.On("GetAgentSessions", mock.Anything, &db.GetAgentSessionsInput{
				Filter: &db.AgentSessionFilter{
					AgentID: &agentID,
				},
			}).Return(&db.AgentSessionsResult{}, nil).Maybe()

			dbClient := &db.Client{
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
			}

			service := &service{
				dbClient: dbClient,
			}

			resp, err := service.GetAgentSessions(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, resp)
		})
	}
}

func TestGetAgentSessionByID(t *testing.T) {
	agentID := "agent123"
	agentSessionID := "agent-session-123"

	// Test cases
	tests := []struct {
		agent         *models.Agent
		name          string
		authError     error
		expectErrCode errors.CodeType
		isAdmin       bool
	}{
		{
			name: "successfully get session by ID for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
		},
		{
			name: "successfully get session for a shared agent",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name: "subject is not authorized to query session for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "cannot query session for a shared agent because subject is not an admin",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.agent, nil)

			if test.agent != nil {
				if test.agent.Type == models.OrganizationAgent {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			mockAgentSessions.On("GetAgentSessionByID", mock.Anything, agentSessionID).Return(&models.AgentSession{
				Metadata: models.ResourceMetadata{ID: agentSessionID},
				AgentID:  agentID,
			}, nil).Maybe()

			dbClient := &db.Client{
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
			}

			service := &service{
				dbClient: dbClient,
			}

			resp, err := service.GetAgentSessionByID(auth.WithCaller(ctx, mockCaller), agentSessionID)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, resp)
		})
	}
}

func TestGetAgentSessionByPRN(t *testing.T) {
	agentID := "agent123"
	agentSessionID := "agent-session-123"
	agentSessionPRN := models.AgentSessionResource.BuildPRN(agentSessionID)

	// Test cases
	tests := []struct {
		agent         *models.Agent
		name          string
		authError     error
		expectErrCode errors.CodeType
		isAdmin       bool
	}{
		{
			name: "successfully get session for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
		},
		{
			name: "successfully get session for a shared agent",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name: "subject is not authorized to query session for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "cannot query session for a shared agent because subject is not an admin",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.agent, nil)

			if test.agent != nil {
				if test.agent.Type == models.OrganizationAgent {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			mockAgentSessions.On("GetAgentSessionByPRN", mock.Anything, agentSessionPRN).Return(&models.AgentSession{
				Metadata: models.ResourceMetadata{ID: agentSessionID},
				AgentID:  agentID,
			}, nil).Maybe()

			dbClient := &db.Client{
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
			}

			service := &service{
				dbClient: dbClient,
			}

			resp, err := service.GetAgentSessionByPRN(auth.WithCaller(ctx, mockCaller), agentSessionPRN)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, resp)
		})
	}
}

func TestAcceptAgentSessionHeartbeat(t *testing.T) {
	agentSessionID := "agent-session-123"

	agentSession := models.AgentSession{
		Metadata: models.ResourceMetadata{ID: agentSessionID},
		AgentID:  "agent123",
	}

	// Test cases
	tests := []struct {
		name          string
		authError     error
		expectErrCode errors.CodeType
	}{
		{
			name: "successfully accept heartbeat for an agent session",
		},
		{
			name:          "subject is not authorized",
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgentSessions := db.NewMockAgentSessions(t)

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateAgentSession, mock.Anything).Return(test.authError)

			mockAgentSessions.On("GetAgentSessionByID", mock.Anything, agentSessionID).Return(&agentSession, nil)

			if test.authError == nil {
				matcher := mock.MatchedBy(func(session *models.AgentSession) bool {
					return session.LastContactTimestamp.After(time.Now().UTC().Add(-time.Minute))
				})
				mockAgentSessions.On("UpdateAgentSession", mock.Anything, matcher).Return(nil, nil)
			}

			dbClient := &db.Client{
				AgentSessions: mockAgentSessions,
			}

			service := &service{
				dbClient: dbClient,
			}

			err := service.AcceptAgentSessionHeartbeat(auth.WithCaller(ctx, mockCaller), agentSessionID)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestCreateAgentSessionError(t *testing.T) {
	agentID := "agent1"
	agentSessionID := "agent-session-1"
	logStreamID := "log-stream-1"
	errorCount := 1
	message := "agent failed to claim job"

	// Test cases
	tests := []struct {
		authError     error
		name          string
		expectErrCode errors.CodeType
		logStreamSize int
	}{
		{
			name:          "create agent session error",
			logStreamSize: 100,
		},
		{
			name:          "subject does not have permission to create session error",
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "fail to create error because error log has exceeded the limit",
			logStreamSize: agentErrorLogsBytesLimit,
			expectErrCode: errors.ETooLarge,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgentSessions := db.NewMockAgentSessions(t)
			mockLogStreams := db.NewMockLogStreams(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockLogStreamManager := logstream.NewMockManager(t)

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateAgentSession, mock.Anything).Return(test.authError)

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			mockLogStreams.On("GetLogStreamByAgentSessionID", mock.Anything, agentSessionID).Return(&models.LogStream{
				Metadata: models.ResourceMetadata{ID: logStreamID},
				Size:     test.logStreamSize,
			}, nil).Maybe()

			mockAgentSessions.On("GetAgentSessionByID", mock.Anything, agentSessionID).Return(&models.AgentSession{
				Metadata:   models.ResourceMetadata{ID: agentSessionID},
				AgentID:    agentID,
				ErrorCount: errorCount,
			}, nil).Maybe()

			mockAgentSessions.On("UpdateAgentSession",
				mock.Anything,
				mock.MatchedBy(func(session *models.AgentSession) bool {
					// Verify error count was updated
					return session.ErrorCount > errorCount
				})).Return(nil, nil).Maybe()

			mockLogStreamManager.On("WriteLogs",
				mock.Anything,
				logStreamID,
				test.logStreamSize,
				mock.MatchedBy(func(buf []byte) bool {
					return strings.Contains(string(buf), message)
				}),
			).Return(nil, nil).Maybe()

			if test.authError == nil && test.expectErrCode == "" {
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			dbClient := &db.Client{
				Transactions:  mockTransactions,
				AgentSessions: mockAgentSessions,
				LogStreams:    mockLogStreams,
			}

			testLogger, _ := logger.NewForTest()

			service := &service{
				logger:           testLogger,
				dbClient:         dbClient,
				limitChecker:     mockLimitChecker,
				logStreamManager: mockLogStreamManager,
			}

			err := service.CreateAgentSessionError(auth.WithCaller(ctx, mockCaller), agentSessionID, message)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestReadAgentSessionErrorLog(t *testing.T) {
	agentID := "agent123"
	agentSessionID := "agent-session-123"
	logStreamID := "log-stream-1"

	// Test cases
	tests := []struct {
		agent         *models.Agent
		name          string
		authError     error
		expectErrCode errors.CodeType
		isAdmin       bool
	}{
		{
			name: "successfully read logs for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
		},
		{
			name: "successfully read logs for a shared agent",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name: "subject is not authorized to read logs for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "cannot read logs for a shared agent because subject is not an admin",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)
			mockLogStream := db.NewMockLogStreams(t)
			mockLogStreamManager := logstream.NewMockManager(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.agent, nil)

			if test.agent != nil {
				if test.agent.Type == models.OrganizationAgent {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			mockAgentSessions.On("GetAgentSessionByID", mock.Anything, agentSessionID).Return(&models.AgentSession{
				Metadata: models.ResourceMetadata{ID: agentSessionID},
				AgentID:  agentID,
			}, nil).Maybe()

			mockLogStream.On("GetLogStreamByAgentSessionID", mock.Anything, agentSessionID).Return(&models.LogStream{
				Metadata: models.ResourceMetadata{ID: logStreamID},
				Size:     100,
			}, nil).Maybe()

			mockLogStreamManager.On("ReadLogs",
				mock.Anything,
				logStreamID,
				0,
				100,
			).Return([]byte("hello"), nil).Maybe()

			dbClient := db.Client{
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
				LogStreams:    mockLogStream,
			}

			service := &service{
				dbClient:         &dbClient,
				logStreamManager: mockLogStreamManager,
			}

			resp, err := service.ReadAgentSessionErrorLog(auth.WithCaller(ctx, mockCaller), agentSessionID, 0, 100)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, resp)
		})
	}
}

func TestGetLogStreamsByAgentSessionIDs(t *testing.T) {
	agentID := "agent123"

	// Test cases
	tests := []struct {
		authError     error
		agent         *models.Agent
		name          string
		expectErrCode errors.CodeType
		sessionIDs    []string
		isAdmin       bool
	}{
		{
			name:       "get log streams for an organization agent",
			sessionIDs: []string{"session-1", "session-2"},
			agent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: agentID},
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
		},
		{
			name:       "get log streams for a shared agent",
			sessionIDs: []string{"session-1", "session-2"},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{ID: agentID},
				Type:     models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name:       "caller is not authorized to get log streams for organization agent",
			sessionIDs: []string{"session-1", "session-2"},
			agent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: agentID},
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:       "caller is not authorized to get log streams for shared agent",
			sessionIDs: []string{"session-1", "session-2"},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{ID: agentID},
				Type:     models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name:       "handle empty session ID list",
			sessionIDs: []string{},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)
			mockLogStream := db.NewMockLogStreams(t)

			if test.agent != nil {
				if test.agent.Type == models.OrganizationAgent {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			logStreams := []models.LogStream{}

			if len(test.sessionIDs) > 0 {
				agentSessions := []models.AgentSession{}
				for _, sessionID := range test.sessionIDs {
					agentSessions = append(agentSessions, models.AgentSession{
						Metadata: models.ResourceMetadata{ID: sessionID},
						AgentID:  agentID,
					})
				}

				mockAgentSessions.On("GetAgentSessions", mock.Anything, &db.GetAgentSessionsInput{
					Filter: &db.AgentSessionFilter{
						AgentSessionIDs: test.sessionIDs,
					},
				}).Return(&db.AgentSessionsResult{
					AgentSessions: agentSessions,
				}, nil)

				mockAgents.On("GetAgents", mock.Anything, &db.GetAgentsInput{
					Filter: &db.AgentFilter{
						AgentIDs: []string{agentID},
					},
				}).Return(&db.AgentsResult{
					Agents: []models.Agent{*test.agent},
				}, nil)

				for idx, sessionID := range test.sessionIDs {
					sessionIDCopy := sessionID
					logStreams = append(logStreams, models.LogStream{
						Metadata:       models.ResourceMetadata{ID: fmt.Sprintf("log-stream-%d", idx)},
						AgentSessionID: &sessionIDCopy,
					})
				}

				mockLogStream.On("GetLogStreams", mock.Anything, &db.GetLogStreamsInput{
					Filter: &db.LogStreamFilter{
						AgentSessionIDs: test.sessionIDs,
					},
				}).Return(&db.LogStreamsResult{
					LogStreams: logStreams,
				}, nil).Maybe()
			}

			dbClient := &db.Client{
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
				LogStreams:    mockLogStream,
			}

			service := &service{
				dbClient: dbClient,
			}

			resp, err := service.GetLogStreamsByAgentSessionIDs(auth.WithCaller(ctx, mockCaller), test.sessionIDs)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, resp)
			assert.Equal(t, logStreams, resp)
		})
	}
}

func TestSubscribeToAgentSessionErrorLog(t *testing.T) {
	agentID := "agent123"
	agentSessionID := "agent-session-123"
	logStreamID := "log-stream-1"

	// Test cases
	tests := []struct {
		agent           *models.Agent
		lastSeenLogSize *int
		name            string
		authError       error
		expectErrCode   errors.CodeType
		isAdmin         bool
	}{
		{
			name: "successfully subscribe to log stream events for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
			lastSeenLogSize: ptr.Int(100),
		},
		{
			name: "successfully subscribe to log stream events for a shared agent",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name: "subject is not authorized to subscribe to events for an organization agent",
			agent: &models.Agent{
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("organization123"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name:          "agent not found",
			expectErrCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to subscribe to events for a shared agent because subject is not an admin",
			agent: &models.Agent{
				Type: models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)
			mockLogStream := db.NewMockLogStreams(t)
			mockLogStreamManager := logstream.NewMockManager(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.agent, nil)

			if test.agent != nil {
				if test.agent.Type == models.OrganizationAgent {
					mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
						Return(test.authError)
				} else {
					mockCaller.On("IsAdmin").Return(test.isAdmin)
				}
			}

			mockAgentSessions.On("GetAgentSessionByID", mock.Anything, agentSessionID).Return(&models.AgentSession{
				Metadata: models.ResourceMetadata{ID: agentSessionID},
				AgentID:  agentID,
			}, nil).Maybe()

			mockLogStream.On("GetLogStreamByAgentSessionID", mock.Anything, agentSessionID).Return(&models.LogStream{
				Metadata: models.ResourceMetadata{ID: logStreamID},
				Size:     100,
			}, nil).Maybe()

			eventChan := make(<-chan *logstream.LogEvent)
			mockLogStreamManager.On("Subscribe",
				mock.Anything,
				&logstream.SubscriptionOptions{
					LastSeenLogSize: test.lastSeenLogSize,
					LogStreamID:     logStreamID,
				},
			).Return(eventChan, nil).Maybe()

			dbClient := db.Client{
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
				LogStreams:    mockLogStream,
			}

			service := &service{
				dbClient:         &dbClient,
				logStreamManager: mockLogStreamManager,
			}

			resp, err := service.SubscribeToAgentSessionErrorLog(auth.WithCaller(ctx, mockCaller), &SubscribeToAgentSessionErrorLogInput{
				AgentSessionID:  agentSessionID,
				LastSeenLogSize: test.lastSeenLogSize,
			})

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			require.NotNil(t, resp)
		})
	}
}

func TestSubscribeToAgentSessions(t *testing.T) {
	// Test cases
	tests := []struct {
		authError      error
		input          *SubscribeToAgentSessionsInput
		name           string
		expectErrCode  errors.CodeType
		agents         []models.Agent
		sendEventData  []*db.AgentSessionEventData
		expectedEvents []SessionEvent
		isAdmin        bool
	}{
		{
			name: "subscribe to agent session events for an organization",
			input: &SubscribeToAgentSessionsInput{
				OrganizationID: ptr.String("organization1"),
			},
			sendEventData: []*db.AgentSessionEventData{
				{
					ID:      "session1",
					AgentID: "agent1",
				},
				{
					ID:      "session2",
					AgentID: "agent2",
				},
			},
			expectedEvents: []SessionEvent{
				{
					AgentSession: &models.AgentSession{
						Metadata: models.ResourceMetadata{ID: "session1"},
						AgentID:  "agent1",
					},
					Action: "UPDATE",
				},
			},
			agents: []models.Agent{
				{
					Metadata:       models.ResourceMetadata{ID: "agent1"},
					Type:           models.OrganizationAgent,
					OrganizationID: ptr.String("organization1"),
				},
				{
					Metadata:       models.ResourceMetadata{ID: "agent2"},
					Type:           models.OrganizationAgent,
					OrganizationID: ptr.String("organization2"),
				},
			},
		},
		{
			name: "not authorized to subscribe to events for an organization",
			input: &SubscribeToAgentSessionsInput{
				OrganizationID: ptr.String("organization1"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subscribe to events for an organization agent",
			input: &SubscribeToAgentSessionsInput{
				AgentID: ptr.String("agent1"),
			},
			sendEventData: []*db.AgentSessionEventData{
				{
					ID:      "session1",
					AgentID: "agent1",
				},
				{
					ID:      "session2",
					AgentID: "agent2",
				},
			},
			expectedEvents: []SessionEvent{
				{
					AgentSession: &models.AgentSession{
						Metadata: models.ResourceMetadata{ID: "session1"},
						AgentID:  "agent1",
					},
					Action: "UPDATE",
				},
			},
			agents: []models.Agent{
				{
					Metadata:       models.ResourceMetadata{ID: "agent1"},
					Type:           models.OrganizationAgent,
					OrganizationID: ptr.String("organization1"),
				},
				{
					Metadata:       models.ResourceMetadata{ID: "agent2"},
					Type:           models.OrganizationAgent,
					OrganizationID: ptr.String("organization1"),
				},
			},
		},
		{
			name: "not authorized to subscribe to events for an organization agent",
			input: &SubscribeToAgentSessionsInput{
				AgentID: ptr.String("agent1"),
			},
			agents: []models.Agent{
				{
					Metadata:       models.ResourceMetadata{ID: "agent1"},
					Type:           models.OrganizationAgent,
					OrganizationID: ptr.String("organization1"),
				},
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subscribe to events for a shared agent",
			input: &SubscribeToAgentSessionsInput{
				AgentID: ptr.String("agent1"),
			},
			agents: []models.Agent{
				{
					Metadata: models.ResourceMetadata{ID: "agent1"},
					Type:     models.SharedAgent,
				},
			},
			isAdmin: true,
		},
		{
			name: "not authorized to subscribe to events for a shared agent",
			input: &SubscribeToAgentSessionsInput{
				AgentID: ptr.String("agent1"),
			},
			agents: []models.Agent{
				{
					Metadata: models.ResourceMetadata{ID: "agent1"},
					Type:     models.SharedAgent,
				},
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name:    "subscribe to all agent session events",
			input:   &SubscribeToAgentSessionsInput{},
			isAdmin: true,
			sendEventData: []*db.AgentSessionEventData{
				{
					ID:      "session1",
					AgentID: "agent1",
				},
				{
					ID:      "session2",
					AgentID: "agent2",
				},
			},
			expectedEvents: []SessionEvent{
				{
					AgentSession: &models.AgentSession{
						Metadata: models.ResourceMetadata{ID: "session1"},
						AgentID:  "agent1",
					},
					Action: "UPDATE",
				},
				{
					AgentSession: &models.AgentSession{
						Metadata: models.ResourceMetadata{ID: "session2"},
						AgentID:  "agent2",
					},
					Action: "UPDATE",
				},
			},
			agents: []models.Agent{
				{
					Metadata:       models.ResourceMetadata{ID: "agent1"},
					Type:           models.OrganizationAgent,
					OrganizationID: ptr.String("organization1"),
				},
				{
					Metadata:       models.ResourceMetadata{ID: "agent2"},
					Type:           models.OrganizationAgent,
					OrganizationID: ptr.String("organization2"),
				},
			},
		},
		{
			name:          "not authorized to subscribe to all agent session events",
			input:         &SubscribeToAgentSessionsInput{},
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockAgentSessions := db.NewMockAgentSessions(t)
			mockEvents := db.NewMockEvents(t)

			mockEventChannel := make(chan db.Event, 1)
			var roEventChan <-chan db.Event = mockEventChannel
			mockEvents.On("Listen", mock.Anything).Return(roEventChan, make(<-chan error)).Maybe()

			for _, agent := range test.agents {
				agentCopy := agent
				mockAgents.On("GetAgentByID", mock.Anything, agent.Metadata.ID).Return(&agentCopy, nil).Maybe()
			}

			if test.input.OrganizationID != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
					Return(test.authError)
			} else if test.input.AgentID != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
					Return(test.authError).Maybe()
				mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()
			} else {
				mockCaller.On("IsAdmin").Return(test.isAdmin)
			}

			for _, d := range test.sendEventData {
				mockAgentSessions.On("GetAgentSessionByID", mock.Anything, d.ID).
					Return(&models.AgentSession{
						Metadata: models.ResourceMetadata{
							ID: d.ID,
						},
						AgentID: d.AgentID,
					}, nil).Maybe()
			}

			dbClient := db.Client{
				Agents:        mockAgents,
				AgentSessions: mockAgentSessions,
				Events:        mockEvents,
			}

			logger, _ := logger.NewForTest()
			eventManager := events.NewEventManager(&dbClient, logger)
			eventManager.Start(ctx)

			service := &service{
				dbClient:     &dbClient,
				eventManager: eventManager,
				logger:       logger,
			}

			eventChannel, err := service.SubscribeToAgentSessions(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			receivedEvents := []*SessionEvent{}

			go func() {
				for _, d := range test.sendEventData {
					encoded, err := json.Marshal(d)
					require.Nil(t, err)

					mockEventChannel <- db.Event{
						Table:  "agent_sessions",
						Action: string(events.UpdateAction),
						ID:     d.ID,
						Data:   encoded,
					}
				}
			}()

			if len(test.expectedEvents) > 0 {
				for e := range eventChannel {
					eCopy := e
					receivedEvents = append(receivedEvents, eCopy)

					if len(receivedEvents) == len(test.expectedEvents) {
						break
					}
				}
			}

			require.Equal(t, len(test.expectedEvents), len(receivedEvents))
			for i, e := range test.expectedEvents {
				assert.Equal(t, e, *receivedEvents[i])
			}
		})
	}
}
