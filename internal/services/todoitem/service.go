// Package todoitem contains the core business logic for the todoitem service.
package todoitem

import (
	"context"
	"slices"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
)

// GetToDoItemsInput represents the input for getting todo items.
type GetToDoItemsInput struct {
	Sort              *db.ToDoItemSortableField
	PaginationOptions *pagination.Options
	OrganizationID    *string
	ProjectID         *string
	Resolved          *bool
	TargetTypes       []models.ToDoItemTargetType
}

// Event is the output of subscribing to ToDoItems.
type Event struct {
	ToDoItem        *models.ToDoItem
	Action          string
	TotalUnresolved int32
}

// UpdateToDoItemInput represents the input for updating a todo item.
type UpdateToDoItemInput struct {
	Resolved *bool
	Version  *int
	ID       string
}

// Service encapsulates the core business logic for the todo item service.
type Service interface {
	GetToDoItemByPRN(ctx context.Context, prn string) (*models.ToDoItem, error)
	CreateToDoItemSubscription(ctx context.Context) (<-chan *Event, error)
	GetToDoItems(ctx context.Context, input *GetToDoItemsInput) (*db.ToDoItemsResult, error)
	UpdateToDoItem(ctx context.Context, input *UpdateToDoItemInput) (*models.ToDoItem, error)
}

type service struct {
	logger       logger.Logger
	dbClient     *db.Client
	eventManager *events.EventManager
}

// NewService creates a new todo item service.
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	eventManager *events.EventManager,
) Service {
	return &service{
		logger:       logger,
		dbClient:     dbClient,
		eventManager: eventManager,
	}
}

func (s *service) GetToDoItemByPRN(ctx context.Context, prn string) (*models.ToDoItem, error) {
	ctx, span := tracer.Start(ctx, "svc.GetToDoItemByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// ToDo items are only applicable to users / teams (a collection of users).
	userCaller, ok := caller.(*auth.UserCaller)
	if !ok {
		return nil, errors.New("unauthorized caller type", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	toDoItem, err := s.dbClient.ToDoItems.GetToDoItemByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get todo item by prn", errors.WithSpan(span))
	}

	if toDoItem == nil {
		return nil, errors.New("todo item not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	// Add a membership requirement for a non-admin user.
	var membershipRequirement *db.ToDoItemMembershipRequirement
	if !userCaller.User.Admin {
		membershipRequirement = &db.ToDoItemMembershipRequirement{
			UserID: &userCaller.User.Metadata.ID,
		}
	}

	itemsResult, err := s.dbClient.ToDoItems.GetToDoItems(ctx, &db.GetToDoItemsInput{
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(0),
		},
		Filter: &db.ToDoItemFilter{
			UserID:                &userCaller.User.Metadata.ID,
			ToDoItemIDs:           []string{toDoItem.Metadata.ID},
			MembershipRequirement: membershipRequirement,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get todo item", errors.WithSpan(span))
	}

	if itemsResult.PageInfo.TotalCount == 0 {
		return nil, errors.New("todo item not found or user is not authorized to view it",
			errors.WithErrorCode(errors.ENotFound),
			errors.WithSpan(span),
		)
	}

	return toDoItem, nil
}

func (s *service) CreateToDoItemSubscription(ctx context.Context) (<-chan *Event, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateToDoItemSubscription")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// ToDo items are only applicable to users / teams (a collection of users).
	userCaller, ok := caller.(*auth.UserCaller)
	if !ok {
		return nil, errors.New("unauthorized caller type", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	var membershipRequirement *db.ToDoItemMembershipRequirement
	if !userCaller.User.Admin {
		membershipRequirement = &db.ToDoItemMembershipRequirement{
			UserID: &userCaller.User.Metadata.ID,
		}
	}

	subscription := events.Subscription{
		Type: events.ToDoItemSubscription,
		Actions: []events.SubscriptionAction{
			events.CreateAction,
			events.UpdateAction,
		},
	}

	subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})

	outgoing := make(chan *Event)
	go func() {
		// Defer close of outgoing channel
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		// Wait for todo item event
		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) {
					s.logger.Errorf("Error occurred while waiting for todo list events: %v", err)
				}
				return
			}

			result, err := s.dbClient.ToDoItems.GetToDoItems(ctx, &db.GetToDoItemsInput{
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Filter: &db.ToDoItemFilter{
					UserID:                &userCaller.User.Metadata.ID,
					ToDoItemIDs:           []string{event.ID},
					MembershipRequirement: membershipRequirement,
				},
			})
			if err != nil {
				s.logger.Errorf("Error querying for todo item in todo item subscription goroutine: %v", err)
				continue
			}

			if result.PageInfo.TotalCount == 0 {
				// Not for our target user.
				continue
			}

			// Get the totalCount of unresolved items.
			unResolvedItems, err := s.dbClient.ToDoItems.GetToDoItems(ctx, &db.GetToDoItemsInput{
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
				Filter: &db.ToDoItemFilter{
					Resolved:              ptr.Bool(false),
					UserID:                &userCaller.User.Metadata.ID,
					MembershipRequirement: membershipRequirement,
				},
			})
			if err != nil {
				s.logger.Error("Error querying for total unresolved todo items in subscription goroutine: %v", err)
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- &Event{ToDoItem: result.ToDoItems[0], Action: event.Action, TotalUnresolved: unResolvedItems.PageInfo.TotalCount}:
			}
		}
	}()

	return outgoing, nil
}

func (s *service) GetToDoItems(ctx context.Context, input *GetToDoItemsInput) (*db.ToDoItemsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetToDoItems")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// ToDo items are only applicable to users / teams (a collection of users).
	userCaller, ok := caller.(*auth.UserCaller)
	if !ok {
		return nil, errors.New("unauthorized caller type", errors.WithErrorCode(errors.EForbidden), errors.WithSpan(span))
	}

	dbInput := &db.GetToDoItemsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ToDoItemFilter{
			UserID:         &userCaller.User.Metadata.ID,
			OrganizationID: input.OrganizationID,
			ProjectID:      input.ProjectID,
			TargetTypes:    input.TargetTypes,
			Resolved:       input.Resolved,
		},
	}

	if !userCaller.User.Admin {
		// Add a membership requirement for a non-admin.
		dbInput.Filter.MembershipRequirement = &db.ToDoItemMembershipRequirement{
			UserID: &userCaller.User.Metadata.ID,
		}
	}

	return s.dbClient.ToDoItems.GetToDoItems(ctx, dbInput)
}

func (s *service) UpdateToDoItem(ctx context.Context, input *UpdateToDoItemInput) (*models.ToDoItem, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateToDoItem")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	// ToDo items are only applicable to users / teams (a collection of users).
	userCaller, ok := caller.(*auth.UserCaller)
	if !ok {
		return nil, errors.New("unauthorized caller type", errors.WithErrorCode(errors.EForbidden))
	}

	var membershipRequirement *db.ToDoItemMembershipRequirement
	if !userCaller.User.Admin {
		membershipRequirement = &db.ToDoItemMembershipRequirement{
			UserID: &userCaller.User.Metadata.ID,
		}
	}

	// This check verifies that the user is authorized to modify the todo item (i.e. they're listed as an assignee).
	itemsResult, err := s.dbClient.ToDoItems.GetToDoItems(ctx, &db.GetToDoItemsInput{
		Filter: &db.ToDoItemFilter{
			UserID:                &userCaller.User.Metadata.ID,
			ToDoItemIDs:           []string{input.ID},
			MembershipRequirement: membershipRequirement,
		},
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
	})
	if err != nil {
		return nil, err
	}

	if itemsResult.PageInfo.TotalCount == 0 {
		return nil, errors.New("todo item not found", errors.WithErrorCode(errors.ENotFound))
	}

	item := itemsResult.ToDoItems[0]

	if !item.Resolvable {
		return nil, errors.New("todo item can only be resolved by the system", errors.WithErrorCode(errors.EForbidden))
	}

	if input.Resolved == nil {
		return item, nil
	}

	if input.Version != nil {
		item.Metadata.Version = *input.Version
	}

	if *input.Resolved {
		item.ResolvedByUsers = append(item.ResolvedByUsers, userCaller.User.Metadata.ID)
	} else if index := slices.Index(item.ResolvedByUsers, userCaller.User.Metadata.ID); index != -1 {
		item.ResolvedByUsers = append(item.ResolvedByUsers[:index], item.ResolvedByUsers[index+1:]...)
	}

	return s.dbClient.ToDoItems.UpdateToDoItem(ctx, item)
}
