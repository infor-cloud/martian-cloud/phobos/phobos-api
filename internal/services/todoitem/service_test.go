package todoitem

import (
	"context"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	eventManager := events.NewEventManager(dbClient, logger)

	expect := &service{
		logger:       logger,
		dbClient:     dbClient,
		eventManager: eventManager,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, eventManager))
}

func TestGetToDoItemByPRN(t *testing.T) {
	userID := "user-1"
	prn := models.ToDoItemResource.BuildPRN("todo-item-gid")

	type testCase struct {
		item            *models.ToDoItem
		name            string
		expectErrorCode errors.CodeType
		totalCount      int32
		isAdmin         bool
	}

	testCases := []testCase{
		{
			name: "user successfully gets a todo item",
			item: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID:  "todo-item-1",
					PRN: prn,
				},
			},
			totalCount: 1,
		},
		{
			name: "admin user successfully gets a todo item",
			item: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID:  "todo-item-1",
					PRN: prn,
				},
			},
			totalCount: 1,
			isAdmin:    true,
		},
		{
			name:            "todo item not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "user subject is not authorized to view todo item",
			item: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID:  "todo-item-1",
					PRN: prn,
				},
			},
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockToDoItems := db.NewMockToDoItems(t)

			mockToDoItems.On("GetToDoItemByPRN", mock.Anything, prn).Return(test.item, nil)

			if test.item != nil {
				var membershipRequirement *db.ToDoItemMembershipRequirement
				if !test.isAdmin {
					membershipRequirement = &db.ToDoItemMembershipRequirement{
						UserID: &userID,
					}
				}

				mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
					Filter: &db.ToDoItemFilter{
						UserID:                &userID,
						ToDoItemIDs:           []string{test.item.Metadata.ID},
						MembershipRequirement: membershipRequirement,
					},
				}).Return(&db.ToDoItemsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: test.totalCount,
					},
				}, nil)
			}

			dbClient := &db.Client{
				ToDoItems: mockToDoItems,
			}

			service := &service{
				dbClient: dbClient,
			}

			var mockCaller = &auth.UserCaller{
				User: &models.User{
					Metadata: models.ResourceMetadata{
						ID: userID,
					},
					Email: "some-user@domain.tld",
					Admin: test.isAdmin,
				},
			}

			item, err := service.GetToDoItemByPRN(auth.WithCaller(ctx, mockCaller), prn)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.item, item)
		})
	}
}

func TestGetToDoItems(t *testing.T) {
	userID := "user-1"

	item := &models.ToDoItem{
		Metadata: models.ResourceMetadata{
			ID: "todo-item-1",
		},
		OrganizationID: "organization-1",
		ProjectID:      ptr.String("project-1"),
	}

	type testCase struct {
		name    string
		isAdmin bool
	}

	testCases := []testCase{
		{
			name: "user gets todo items",
		},
		{
			name:    "admin user gets todo items",
			isAdmin: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockToDoItems := db.NewMockToDoItems(t)

			mockToDoItems.On("GetToDoItems",
				mock.Anything,
				mock.MatchedBy(func(input *db.GetToDoItemsInput) bool {
					if test.isAdmin {
						return input.Filter.MembershipRequirement == nil
					}

					// Make sure we have a membership requirement for non admins.
					return *input.Filter.UserID == userID && *input.Filter.MembershipRequirement.UserID == userID
				}),
			).Return(&db.ToDoItemsResult{
				ToDoItems: []*models.ToDoItem{item},
			}, nil)

			dbClient := &db.Client{
				ToDoItems: mockToDoItems,
			}

			service := &service{
				dbClient: dbClient,
			}

			var mockCaller = &auth.UserCaller{
				User: &models.User{
					Metadata: models.ResourceMetadata{
						ID: userID,
					},
					Email: "some-user@domain.tld",
					Admin: test.isAdmin,
				},
			}

			result, err := service.GetToDoItems(auth.WithCaller(ctx, mockCaller), &GetToDoItemsInput{})

			require.NoError(t, err)
			require.NotNil(t, result)
			assert.Len(t, result.ToDoItems, 1)
		})
	}
}

func TestCreateToDoItemSubscription(t *testing.T) {
	userID := "user-1"

	type testCase struct {
		todoItem       *models.ToDoItem
		name           string
		sendEvents     []Event
		expectedEvents []Event
		isAdmin        bool
	}

	testCases := []testCase{
		{
			name: "user successfully subscribes to todo item events",
			sendEvents: []Event{
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action: string(events.CreateAction),
				},
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action: string(events.UpdateAction),
				},
			},
			todoItem: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID: "todo-item-1",
				},
				UserIDs: []string{userID},
			},
			expectedEvents: []Event{
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action:          string(events.CreateAction),
					TotalUnresolved: 5,
				},
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action:          string(events.UpdateAction),
					TotalUnresolved: 5,
				},
			},
		},
		{
			name: "admin user successfully subscribes to todo item events",
			sendEvents: []Event{
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action: string(events.CreateAction),
				},
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action: string(events.UpdateAction),
				},
			},
			todoItem: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID: "todo-item-1",
				},
				UserIDs: []string{userID},
			},
			expectedEvents: []Event{
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action:          string(events.CreateAction),
					TotalUnresolved: 5,
				},
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action:          string(events.UpdateAction),
					TotalUnresolved: 5,
				},
			},
			isAdmin: true,
		},
		{
			name: "todo item associated with event not found",
			sendEvents: []Event{
				{
					ToDoItem: &models.ToDoItem{
						Metadata: models.ResourceMetadata{
							ID: "todo-item-1",
						},
						UserIDs: []string{userID},
					},
					Action: string(events.CreateAction),
				},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()

			mockToDoItems := db.NewMockToDoItems(t)
			mockEvents := db.NewMockEvents(t)

			mockCaller := &auth.UserCaller{
				User: &models.User{
					Metadata: models.ResourceMetadata{
						ID: userID,
					},
					Admin: test.isAdmin,
				},
			}

			mockEventChannel := make(chan db.Event, 1)
			var roEventChan <-chan db.Event = mockEventChannel
			mockEvents.On("Listen", mock.Anything).Return(roEventChan, make(<-chan error)).Maybe()

			// Only require a membership check for non-admin.
			var membershipRequirement *db.ToDoItemMembershipRequirement
			if !test.isAdmin {
				membershipRequirement = &db.ToDoItemMembershipRequirement{
					UserID: &userID,
				}
			}

			for _, e := range test.sendEvents {
				mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(1),
					},
					Filter: &db.ToDoItemFilter{
						UserID:                &userID,
						ToDoItemIDs:           []string{e.ToDoItem.Metadata.ID},
						MembershipRequirement: membershipRequirement,
					},
				},
				).Return(func(_ context.Context, _ *db.GetToDoItemsInput) (*db.ToDoItemsResult, error) {
					result := &db.ToDoItemsResult{
						PageInfo:  &pagination.PageInfo{},
						ToDoItems: []*models.ToDoItem{},
					}

					if test.todoItem != nil {
						result.PageInfo.TotalCount++
						result.ToDoItems = append(result.ToDoItems, test.todoItem)
					}

					return result, nil
				}).Maybe()
			}

			if l := len(test.expectedEvents); l > 0 {
				mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
					PaginationOptions: &pagination.Options{
						First: ptr.Int32(0),
					},
					Filter: &db.ToDoItemFilter{
						Resolved:              ptr.Bool(false),
						UserID:                &userID,
						MembershipRequirement: membershipRequirement,
					},
				},
				).Return(&db.ToDoItemsResult{
					PageInfo: &pagination.PageInfo{
						// Just hardcode number of unresolved items for simplicity.
						TotalCount: 5,
					},
				}, nil).Times(l)
			}

			dbClient := &db.Client{
				Events:    mockEvents,
				ToDoItems: mockToDoItems,
			}

			logger, _ := logger.NewForTest()
			eventManager := events.NewEventManager(dbClient, logger)
			eventManager.Start(ctx)

			service := &service{
				dbClient:     dbClient,
				eventManager: eventManager,
				logger:       logger,
			}

			events, err := service.CreateToDoItemSubscription(auth.WithCaller(ctx, mockCaller))

			require.NoError(t, err)

			go func() {
				for _, e := range test.sendEvents {
					mockEventChannel <- db.Event{
						Table:  "todo_items",
						Action: e.Action,
						ID:     e.ToDoItem.Metadata.ID,
					}
				}
			}()

			receivedEvents := []*Event{}

			if len(test.expectedEvents) > 0 {
				for e := range events {
					receivedEvents = append(receivedEvents, e)

					if len(receivedEvents) == len(test.expectedEvents) {
						break
					}
				}
			}

			require.Equal(t, len(test.expectedEvents), len(receivedEvents))
			for i, e := range test.expectedEvents {
				assert.Equal(t, e, *receivedEvents[i])
			}
		})
	}
}

func TestUpdateToDoItem(t *testing.T) {
	itemVersion := 1
	userID := "user-1"
	itemID := "todo-item-1"

	type testCase struct {
		name            string
		item            *models.ToDoItem
		expectErrorCode errors.CodeType
		isAdmin         bool
	}

	testCases := []testCase{
		{
			name: "user successfully updates todo item",
			item: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID:      itemID,
					Version: itemVersion,
				},
				Resolvable: true,
			},
		},
		{
			name: "admin user successfully updates todo item",
			item: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID:      itemID,
					Version: itemVersion,
				},
				Resolvable: true,
			},
			isAdmin: true,
		},
		{
			name:            "todo item not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "todo item is not resolvable",
			item: &models.ToDoItem{
				Metadata: models.ResourceMetadata{
					ID:      itemID,
					Version: itemVersion,
				},
				Resolvable: false,
			},
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockToDoItems := db.NewMockToDoItems(t)

			var membershipRequirement *db.ToDoItemMembershipRequirement
			if !test.isAdmin {
				membershipRequirement = &db.ToDoItemMembershipRequirement{
					UserID: &userID,
				}
			}

			mockToDoItems.On("GetToDoItems", mock.Anything, &db.GetToDoItemsInput{
				Filter: &db.ToDoItemFilter{
					UserID:                &userID,
					ToDoItemIDs:           []string{itemID},
					MembershipRequirement: membershipRequirement,
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
			}).Return(func(_ context.Context, _ *db.GetToDoItemsInput) (*db.ToDoItemsResult, error) {
				result := &db.ToDoItemsResult{
					PageInfo: &pagination.PageInfo{
						TotalCount: 0,
					},
					ToDoItems: []*models.ToDoItem{},
				}

				if test.item != nil {
					result.ToDoItems = append(result.ToDoItems, test.item)
					result.PageInfo.TotalCount++
				}

				return result, nil
			})

			if test.expectErrorCode == "" {
				mockToDoItems.On("UpdateToDoItem", mock.Anything, mock.MatchedBy(func(ti *models.ToDoItem) bool {
					return ti.IsResolved(userID) && ti.Metadata.Version == itemVersion
				})).Return(test.item, nil)
			}

			dbClient := &db.Client{
				ToDoItems: mockToDoItems,
			}

			service := &service{
				dbClient: dbClient,
			}

			var mockCaller = &auth.UserCaller{
				User: &models.User{
					Metadata: models.ResourceMetadata{
						ID: "user-1",
					},
					Email: "some-user@domain.tld",
					Admin: test.isAdmin,
				},
			}

			updatedItem, err := service.UpdateToDoItem(auth.WithCaller(ctx, mockCaller), &UpdateToDoItemInput{
				Resolved: ptr.Bool(true),
				Version:  &itemVersion,
				ID:       itemID,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, updatedItem.Metadata.Version, itemVersion)
		})
	}
}
