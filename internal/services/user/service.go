// Package user implements functionality relating to users CRUD.
package user

import (
	"context"
	"strings"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
)

// GetUsersInput is the input for listing users
type GetUsersInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.UserSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Search filters user list by username
	Search *string
}

// Service implements all user related functionality
type Service interface {
	GetUserByID(ctx context.Context, userID string) (*models.User, error)
	GetUserByPRN(ctx context.Context, prn string) (*models.User, error)
	GetUserByUsername(ctx context.Context, username string) (*models.User, error)
	GetUsers(ctx context.Context, input *GetUsersInput) (*db.UsersResult, error)
	GetUsersByIDs(ctx context.Context, idList []string) ([]models.User, error)
}

type service struct {
	logger   logger.Logger
	dbClient *db.Client
}

// NewService creates an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
) Service {
	return &service{logger, dbClient}
}

func (s *service) GetUserByID(ctx context.Context, userID string) (*models.User, error) {
	ctx, span := tracer.Start(ctx, "svc.GetUserByID")
	span.SetAttributes(attribute.Key("user ID").String(userID))
	defer span.End()

	// Any authenticated user can view basic user information
	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	user, err := s.dbClient.Users.GetUserByID(ctx, userID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user", errors.WithSpan(span))
	}

	if user == nil {
		return nil, errors.New(
			"User with ID %s not found", userID,
			errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound),
		)
	}

	return user, nil
}

func (s *service) GetUserByPRN(ctx context.Context, prn string) (*models.User, error) {
	ctx, span := tracer.Start(ctx, "svc.GetUserByPRN")
	span.SetAttributes(attribute.Key("prn").String(prn))
	defer span.End()

	// Any authenticated user can view basic user information
	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	user, err := s.dbClient.Users.GetUserByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user", errors.WithSpan(span))
	}

	if user == nil {
		return nil, errors.New(
			"User with PRN %s not found", prn,
			errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound),
		)
	}

	return user, nil
}

func (s *service) GetUserByUsername(ctx context.Context, username string) (*models.User, error) {
	ctx, span := tracer.Start(ctx, "svc.GetUserByUsername")
	span.SetAttributes(attribute.Key("username").String(username))
	defer span.End()

	// Any authenticated user can view basic user information
	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	user, err := s.dbClient.Users.GetUserByUsername(ctx, username)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user by username", errors.WithSpan(span))
	}

	if user == nil {
		return nil, errors.New(
			"User with username %s not found", username,
			errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound),
		)
	}

	return user, nil
}

func (s *service) GetUsers(ctx context.Context, input *GetUsersInput) (*db.UsersResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetUsers")
	if input.Search != nil {
		span.SetAttributes(attribute.Key("search").String(*input.Search))
	}
	defer span.End()

	// Any authenticated user can view basic user information
	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	usersResult, err := s.dbClient.Users.GetUsers(ctx, &db.GetUsersInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.UserFilter{
			Search: input.Search,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get users", errors.WithSpan(span))
	}

	return usersResult, nil
}

func (s *service) GetUsersByIDs(ctx context.Context, idList []string) ([]models.User, error) {
	ctx, span := tracer.Start(ctx, "svc.GetUserByUsersByIDs")
	span.SetAttributes(attribute.Key("ID list").String(strings.Join(idList, ", ")))
	defer span.End()

	// Any authenticated user can view basic user information
	if _, err := auth.AuthorizeCaller(ctx); err != nil {
		return nil, err
	}

	resp, err := s.dbClient.Users.GetUsers(ctx, &db.GetUsersInput{Filter: &db.UserFilter{UserIDs: idList}})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get users by IDs", errors.WithSpan(span))
	}

	return resp.Users, nil
}
