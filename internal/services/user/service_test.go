package user

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}

	expect := &service{
		logger:   logger,
		dbClient: dbClient,
	}

	assert.Equal(t, expect, NewService(logger, dbClient))
}

func TestGetUserByID(t *testing.T) {
	searchID := "user-1"

	type testCase struct {
		expectUser      *models.User
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get a user",
			expectUser: &models.User{
				Username: "some-user",
			},
		},
		{
			name:            "user does not exist",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUsers := db.NewMockUsers(t)
			mockCaller := auth.NewMockCaller(t)

			mockUsers.On("GetUserByID", mock.Anything, searchID).Return(test.expectUser, nil)

			dbClient := &db.Client{
				Users: mockUsers,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualUser, err := service.GetUserByID(auth.WithCaller(ctx, mockCaller), searchID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectUser, actualUser)
		})
	}
}

func TestGetUserByPRN(t *testing.T) {
	searchPRN := "prn:user:some-user"

	type testCase struct {
		expectUser      *models.User
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get a user",
			expectUser: &models.User{
				Username: "some-user",
			},
		},
		{
			name:            "user does not exist",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUsers := db.NewMockUsers(t)
			mockCaller := auth.NewMockCaller(t)

			mockUsers.On("GetUserByPRN", mock.Anything, searchPRN).Return(test.expectUser, nil)

			dbClient := &db.Client{
				Users: mockUsers,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualUser, err := service.GetUserByPRN(auth.WithCaller(ctx, mockCaller), searchPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectUser, actualUser)
		})
	}
}

func TestGetUserByUsername(t *testing.T) {
	search := "user"

	type testCase struct {
		expectUser      *models.User
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get a user",
			expectUser: &models.User{
				Username: "some-user",
			},
		},
		{
			name:            "user does not exist",
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUsers := db.NewMockUsers(t)
			mockCaller := auth.NewMockCaller(t)

			mockUsers.On("GetUserByUsername", mock.Anything, search).Return(test.expectUser, nil)

			dbClient := &db.Client{
				Users: mockUsers,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualUser, err := service.GetUserByUsername(auth.WithCaller(ctx, mockCaller), search)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectUser, actualUser)
		})
	}
}

func TestGetUsers(t *testing.T) {
	search := "user"

	type testCase struct {
		expectResult    *db.UsersResult
		name            string
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully get a list of users",
			expectResult: &db.UsersResult{
				Users: []models.User{
					{Username: search},
				},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUsers := db.NewMockUsers(t)
			mockCaller := auth.NewMockCaller(t)

			dbInput := &db.GetUsersInput{
				Filter: &db.UserFilter{
					Search: &search,
				},
			}

			mockUsers.On("GetUsers", mock.Anything, dbInput).Return(test.expectResult, nil)

			dbClient := &db.Client{
				Users: mockUsers,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetUsers(auth.WithCaller(ctx, mockCaller), &GetUsersInput{Search: &search})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestGetUsersByIDs(t *testing.T) {
	searchID := "user-1"

	type testCase struct {
		name            string
		expectErrorCode errors.CodeType
		expectResult    []models.User
	}

	testCases := []testCase{
		{
			name: "successfully get a list of users",
			expectResult: []models.User{
				{Username: "user"},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockUsers := db.NewMockUsers(t)
			mockCaller := auth.NewMockCaller(t)

			dbInput := &db.GetUsersInput{
				Filter: &db.UserFilter{
					UserIDs: []string{searchID},
				},
			}

			mockUsers.On("GetUsers", mock.Anything, dbInput).Return(&db.UsersResult{Users: test.expectResult}, nil)

			dbClient := &db.Client{
				Users: mockUsers,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetUsersByIDs(auth.WithCaller(ctx, mockCaller), []string{searchID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}
