package releaselifecycle

import (
	"context"
	"io"
	"strings"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	lifecycleTemplateDataStore := lifecycletemplate.NewMockDataStore(t)
	limitChecker := limits.NewMockLimitChecker(t)
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:                     logger,
		dbClient:                   dbClient,
		lifecycleTemplateDataStore: lifecycleTemplateDataStore,
		limitChecker:               limitChecker,
		activityService:            activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, lifecycleTemplateDataStore, limitChecker, activityService))
}

func TestGetReleaseLifecycleByID(t *testing.T) {
	orgID := "org-1"
	projID := "proj-1"
	orgLifecycleID := "org-lifecycle-1"
	projLifecycleID := "proj-lifecycle-1"
	invalidID := "invalid-id"

	orgReleaseLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: orgLifecycleID,
		},
		Scope:          models.OrganizationScope,
		OrganizationID: orgID,
	}

	projReleaseLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: projLifecycleID,
		},
		Scope:          models.ProjectScope,
		OrganizationID: orgID,
		ProjectID:      &projID,
	}

	type testCase struct {
		injectPermissionError  error
		find                   string
		expectReleaseLifecycle *models.ReleaseLifecycle
		name                   string
		expectErrorCode        errors.CodeType
		withCaller             bool
	}

	/*
		test case template

		type testCase struct {
			name                   string
			withCaller             bool
			find                   string
			injectPermissionError  error
			expectErrorCode        errors.CodeType
			expectReleaseLifecycle *models.ReleaseLifecycle
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "release lifecycle not found",
			withCaller:      true,
			find:            invalidID,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "org lifecycle, permission check failed",
			withCaller:            true,
			find:                  orgLifecycleID,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:                  "proj lifecycle, permission check failed",
			withCaller:            true,
			find:                  projLifecycleID,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:                   "successfully return an org release lifecycle",
			withCaller:             true,
			find:                   orgLifecycleID,
			expectReleaseLifecycle: orgReleaseLifecycle,
		},
		{
			name:                   "successfully return a proj release lifecycle",
			withCaller:             true,
			find:                   projLifecycleID,
			expectReleaseLifecycle: projReleaseLifecycle,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockOrganizations := db.NewMockOrganizations(t)
			mockReleaseLifecycles := db.NewMockReleaseLifecycles(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockReleaseLifecycles.On("GetReleaseLifecycleByID", mock.Anything, test.find).
					Return(func(_ context.Context, id string) (*models.ReleaseLifecycle, error) {
						switch id {
						case invalidID:
							return nil, errors.New("mock returns invalid ID error", errors.WithErrorCode(errors.ENotFound))
						case orgLifecycleID:
							return orgReleaseLifecycle, nil
						case projLifecycleID:
							return projReleaseLifecycle, nil
						default:
							return nil, errors.New("unexpected mock input", errors.WithErrorCode(errors.EInvalid))
						}
					})

				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ReleaseLifecycleResource, mock.Anything).
					Return(test.injectPermissionError).Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.ViewReleaseLifecycle, mock.Anything).
					Return(test.injectPermissionError).Maybe()
			}

			dbClient := &db.Client{
				Organizations:     mockOrganizations,
				ReleaseLifecycles: mockReleaseLifecycles,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualReleaseLifecycle, err := service.GetReleaseLifecycleByID(ctx, test.find)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectReleaseLifecycle, actualReleaseLifecycle)
		})
	}
}

func TestGetReleaseLifecycleByPRN(t *testing.T) {
	orgID := "org-1"
	projID := "proj-1"
	orgLifecyclePRN := "prn:release_lifecycle:some-org/some-release-lifecycle"
	projLifecyclePRN := "prn:release_lifecycle:some-org/some-proj/some-release-lifecycle"
	invalidID := "invalid-id"

	orgReleaseLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			PRN: orgLifecyclePRN,
		},
		Scope:          models.OrganizationScope,
		OrganizationID: orgID,
	}

	projReleaseLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			PRN: projLifecyclePRN,
		},
		Scope:          models.ProjectScope,
		OrganizationID: orgID,
		ProjectID:      &projID,
	}

	type testCase struct {
		injectPermissionError  error
		find                   string
		expectReleaseLifecycle *models.ReleaseLifecycle
		name                   string
		expectErrorCode        errors.CodeType
		withCaller             bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "template not found",
			withCaller:      true,
			find:            invalidID,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "org lifecycle, permission check failed",
			withCaller:            true,
			find:                  orgLifecyclePRN,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:                  "proj lifecycle, permission check failed",
			withCaller:            true,
			find:                  projLifecyclePRN,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:                   "successfully return an org release lifecycle",
			withCaller:             true,
			find:                   orgLifecyclePRN,
			expectReleaseLifecycle: orgReleaseLifecycle,
		},
		{
			name:                   "successfully return a proj release lifecycle",
			withCaller:             true,
			find:                   projLifecyclePRN,
			expectReleaseLifecycle: projReleaseLifecycle,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockReleaseLifecycles := db.NewMockReleaseLifecycles(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockReleaseLifecycles.On("GetReleaseLifecycleByPRN", mock.Anything, test.find).
					Return(func(_ context.Context, prn string) (*models.ReleaseLifecycle, error) {
						switch prn {
						case invalidID:
							return nil, errors.New("mock returns invalid ID error", errors.WithErrorCode(errors.ENotFound))
						case orgLifecyclePRN:
							return orgReleaseLifecycle, nil
						case projLifecyclePRN:
							return projReleaseLifecycle, nil
						default:
							return nil, errors.New("unexpected mock input", errors.WithErrorCode(errors.EInvalid))
						}
					})

				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ReleaseLifecycleResource, mock.Anything).
					Return(test.injectPermissionError).Maybe()
				mockCaller.On("RequirePermission", mock.Anything, models.ViewReleaseLifecycle, mock.Anything).
					Return(test.injectPermissionError).Maybe()
			}

			dbClient := &db.Client{
				ReleaseLifecycles: mockReleaseLifecycles,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualReleaseLifecycle, err := service.GetReleaseLifecycleByPRN(ctx, test.find)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectReleaseLifecycle, actualReleaseLifecycle)
		})
	}
}

func TestGetReleaseLifecyclesByIDs(t *testing.T) {
	orgLifecycleID := "org-lifecycle-1"
	projLifecycleID := "proj-lifecycle-1"
	orgID := "org-1"
	projID := "proj-1"
	inputList := []string{orgLifecycleID, projLifecycleID}

	lifecycles := []*models.ReleaseLifecycle{
		{
			Metadata: models.ResourceMetadata{
				ID: orgLifecycleID,
			},
			Scope:          models.OrganizationScope,
			OrganizationID: orgID,
		},
		{
			Metadata: models.ResourceMetadata{
				ID: projLifecycleID,
			},
			Scope:          models.ProjectScope,
			OrganizationID: orgID,
			ProjectID:      &projID,
		},
	}

	type testCase struct {
		orgAuthError     error
		projAuthError    error
		name             string
		expectErrorCode  errors.CodeType
		injectLifecycles []*models.ReleaseLifecycle
		expectLifecycles []*models.ReleaseLifecycle
	}

	testCases := []testCase{
		{
			name:             "successfully return lifecycles",
			injectLifecycles: lifecycles,
			expectLifecycles: lifecycles,
		},
		{
			name:             "caller does not have permission to view org lifecycle",
			injectLifecycles: lifecycles,
			orgAuthError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:  errors.EForbidden,
			expectLifecycles: []*models.ReleaseLifecycle{},
		},
		{
			name:             "caller does not have permission to view proj lifecycle",
			injectLifecycles: lifecycles,
			projAuthError:    errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:  errors.EForbidden,
			expectLifecycles: []*models.ReleaseLifecycle{},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleaseLifecycles := db.NewMockReleaseLifecycles(t)

			dbInput := &db.GetReleaseLifecyclesInput{
				Filter: &db.ReleaseLifecycleFilter{
					ReleaseLifecycleIDs: inputList,
				},
			}
			dbResult := &db.ReleaseLifecyclesResult{
				ReleaseLifecycles: test.injectLifecycles,
			}

			mockReleaseLifecycles.On("GetReleaseLifecycles", mock.Anything, dbInput).Return(dbResult, nil)

			mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.ReleaseLifecycleResource, mock.Anything).
				Return(test.orgAuthError).Maybe()
			mockCaller.On("RequirePermission", mock.Anything, models.ViewReleaseLifecycle, mock.Anything).
				Return(test.projAuthError).Maybe()

			dbClient := &db.Client{
				ReleaseLifecycles: mockReleaseLifecycles,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				logger:   logger,
				dbClient: dbClient,
			}

			actualReleaseLifecycles, err := service.GetReleaseLifecyclesByIDs(auth.WithCaller(ctx, mockCaller), inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.ElementsMatch(t, test.expectLifecycles, actualReleaseLifecycles)
		})
	}
}

func TestGetReleaseLifecycles(t *testing.T) {
	orgID1 := "organization-ID-1"
	projectID1 := "project-ID-1"
	sort := db.ReleaseLifecycleSortableFieldUpdatedAtAsc

	type testCase struct {
		input           *GetReleaseLifecyclesInput
		expectResult    *db.ReleaseLifecyclesResult
		name            string
		expectErrorCode errors.CodeType
		withCaller      bool
	}

	/*
		test case template

		type testCase struct {
			name            string
			withCaller      bool
			input           *GetReleaseLifecyclesInput
			expectResult    *db.ReleaseLifecyclesResult
			expectErrorCode errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name: "successfully return release lifecycles result, by search, positive",
			input: &GetReleaseLifecyclesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Search: ptr.String("release-lifecycle-1"),
			},
			expectResult: &db.ReleaseLifecyclesResult{
				ReleaseLifecycles: []*models.ReleaseLifecycle{
					{
						Name:  "release-lifecycle-1",
						Scope: models.OrganizationScope,
					},
				},
			},
			withCaller: true,
		},
		{
			name: "successfully return release lifecycles result, by search, negative",
			input: &GetReleaseLifecyclesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Search: ptr.String("release-lifecycle-1"),
			},
			expectResult: &db.ReleaseLifecyclesResult{
				ReleaseLifecycles: []*models.ReleaseLifecycle{},
			},
			withCaller: true,
		},
		{
			name: "successfully return release lifecycles result, by organization, positive",
			input: &GetReleaseLifecyclesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				OrganizationID:         &orgID1,
				ReleaseLifecycleScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectResult: &db.ReleaseLifecyclesResult{
				ReleaseLifecycles: []*models.ReleaseLifecycle{
					{
						Name:  "release-lifecycle-1",
						Scope: models.OrganizationScope,
					},
				},
			},
			withCaller: true,
		},
		{
			name: "successfully return release lifecycles result, by organization, negative",
			input: &GetReleaseLifecyclesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				OrganizationID:         &orgID1,
				ReleaseLifecycleScopes: []models.ScopeType{models.OrganizationScope},
			},
			expectResult: &db.ReleaseLifecyclesResult{
				ReleaseLifecycles: []*models.ReleaseLifecycle{},
			},
			withCaller: true,
		},
		{
			name: "successfully return release lifecycles result, by project, positive",
			input: &GetReleaseLifecyclesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				ProjectID:              &projectID1,
				ReleaseLifecycleScopes: []models.ScopeType{models.ProjectScope},
			},
			expectResult: &db.ReleaseLifecyclesResult{
				ReleaseLifecycles: []*models.ReleaseLifecycle{
					{
						Name:      "release-lifecycle-1",
						Scope:     models.ProjectScope,
						ProjectID: ptr.String("project-1"),
					},
				},
			},
			withCaller: true,
		},
		{
			name: "successfully return release lifecycles result, by project, negative",
			input: &GetReleaseLifecyclesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				ProjectID:              &projectID1,
				ReleaseLifecycleScopes: []models.ScopeType{models.ProjectScope},
			},
			expectResult: &db.ReleaseLifecyclesResult{
				ReleaseLifecycles: []*models.ReleaseLifecycle{},
			},
			withCaller: true,
		},
		{
			name: "without caller",
			input: &GetReleaseLifecyclesInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Search: ptr.String("release-lifecycle-1"),
			},
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockReleaseLifecycles := db.NewMockReleaseLifecycles(t)

			if test.expectResult != nil {
				var wantScopes []models.ScopeType
				if test.input.OrganizationID != nil {
					wantScopes = append(wantScopes, models.OrganizationScope)
				}
				if test.input.ProjectID != nil {
					wantScopes = append(wantScopes, models.ProjectScope)
				}
				dbInput := &db.GetReleaseLifecyclesInput{
					Sort:              test.input.Sort,
					PaginationOptions: test.input.PaginationOptions,
					Filter: &db.ReleaseLifecycleFilter{
						ReleaseLifecycleScopes: wantScopes,
						OrganizationID:         test.input.OrganizationID,
						ProjectID:              test.input.ProjectID,
						Search:                 test.input.Search,
					},
				}
				if test.input.OrganizationID != nil {
					dbInput.Filter.ReleaseLifecycleScopes = []models.ScopeType{models.OrganizationScope}
				}
				mockReleaseLifecycles.On("GetReleaseLifecycles", mock.Anything, dbInput).Return(test.expectResult, nil)
			}

			if test.withCaller {
				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)
			}

			dbClient := &db.Client{
				ReleaseLifecycles: mockReleaseLifecycles,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetReleaseLifecycles(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestCreateReleaseLifecycle(t *testing.T) {
	orgID := "org-id-1"
	projectID := "project-id-1"

	sampleOrgLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: "release-lifecycle-id",
		},
		CreatedBy:        "sample-user@domain.tld",
		Scope:            models.OrganizationScope,
		OrganizationID:   orgID,
		EnvironmentNames: []string{"account1", "account2"},
	}

	sampleProjectLifecycle := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: "release-lifecycle-id",
		},
		CreatedBy:        "sample-user@domain.tld",
		Scope:            models.ProjectScope,
		OrganizationID:   orgID,
		ProjectID:        &projectID,
		EnvironmentNames: []string{"account1", "account2"},
	}

	orgReleaseLifecycle := &CreateReleaseLifecycleInput{
		Name:           "org-release-lifecycle-input-name-for-unit-test",
		Scope:          models.OrganizationScope,
		OrganizationID: &orgID,
	}

	projectReleaseLifecycle := &CreateReleaseLifecycleInput{
		Name:      "project-release-lifecycle-input-name-for-unit-test",
		Scope:     models.ProjectScope,
		ProjectID: &projectID,
	}

	lifecycleTemplatePending := &models.LifecycleTemplate{
		Status: models.LifecycleTemplatePending,
	}

	lifecycleTemplateGood := &models.LifecycleTemplate{
		Status:         models.LifecycleTemplateUploaded,
		OrganizationID: orgID,
	}

	type testCase struct {
		injectOrganizationError      error
		injectPermissionError        error
		injectCreateError            error
		injectLifecycleTemplateError error
		limitError                   error
		input                        *CreateReleaseLifecycleInput
		expectCreated                *models.ReleaseLifecycle
		injectLifecycleTemplate      *models.LifecycleTemplate
		name                         string
		expectErrorCode              errors.CodeType
		injectCount                  int32
		withCaller                   bool
		exceedsLimit                 bool
		shouldTryToCreate            bool
	}

	/*
		test case template

		type testCase struct {
			name                         string
			input                        *CreateReleaseLifecycleInput
			withCaller                   bool
			injectLifecycleTemplate      *models.LifecycleTemplate
			injectLifecycleTemplateError error
			injectPermissionError        error
			injectOrganizationError      error
			shouldTryToCreate            bool
			injectCreateError            error
			injectCount                  int32
			exceedsLimit                 bool
			limitError                   error
			expectErrorCode              errors.CodeType
			expectCreated                *models.ReleaseLifecycle
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			input:           orgReleaseLifecycle,
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:                    "permission check failed",
			input:                   orgReleaseLifecycle,
			withCaller:              true,
			injectLifecycleTemplate: lifecycleTemplateGood,
			injectPermissionError:   errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:         errors.EForbidden,
		},
		{
			name:                         "lifecycle template fetch failed due to internal error",
			input:                        orgReleaseLifecycle,
			withCaller:                   true,
			injectLifecycleTemplateError: errors.New("injected error, lifecycle template fetch failed"),
			expectErrorCode:              errors.EInternal,
		},
		{
			name:                         "lifecycle template fetch returned nothing",
			input:                        orgReleaseLifecycle,
			withCaller:                   true,
			injectLifecycleTemplateError: nil,
			injectLifecycleTemplate:      nil,
			expectErrorCode:              errors.ENotFound,
		},
		{
			name:                    "lifecycle template was not uploaded",
			input:                   orgReleaseLifecycle,
			withCaller:              true,
			injectLifecycleTemplate: lifecycleTemplatePending,
			expectErrorCode:         errors.EInvalid,
		},
		{
			name:                    "creation failed due to unspecified internal problem",
			input:                   orgReleaseLifecycle,
			withCaller:              true,
			injectLifecycleTemplate: lifecycleTemplateGood,
			shouldTryToCreate:       true,
			injectCreateError:       errors.New("injected error, creation failed"),
			expectErrorCode:         errors.EInternal,
		},
		{
			name:       "not allowed to create an org-scope release lifecycle with a project-scope lifecycle template",
			input:      orgReleaseLifecycle,
			withCaller: true,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status: models.LifecycleTemplateUploaded,
				Scope:  models.ProjectScope,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "not allowed to create a project-scope release lifecycle with a org-scope lifecycle template",
			input:      projectReleaseLifecycle,
			withCaller: true,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status: models.LifecycleTemplateUploaded,
				Scope:  models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "not allowed: org release lifecycle, org lifecycle template, different orgs",
			input:      orgReleaseLifecycle,
			withCaller: true,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status:         models.LifecycleTemplateUploaded,
				Scope:          models.OrganizationScope,
				OrganizationID: "other-organization",
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:       "not allowed: project release lifecycle, project lifecycle template, different projects",
			input:      projectReleaseLifecycle,
			withCaller: true,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status:    models.LifecycleTemplateUploaded,
				Scope:     models.ProjectScope,
				ProjectID: ptr.String("other-project"),
			},
			expectErrorCode: errors.EInvalid,
		},
		{
			name:                    "exceeds limit",
			input:                   orgReleaseLifecycle,
			withCaller:              true,
			injectLifecycleTemplate: lifecycleTemplateGood,
			shouldTryToCreate:       true,
			injectCount:             5,
			limitError:              errors.New("mocked limit violation", errors.WithErrorCode(errors.EInvalid)),
			exceedsLimit:            true,
			expectErrorCode:         errors.EInvalid,
		},
		{
			name:                    "successfully create a new organization release lifecycle",
			input:                   orgReleaseLifecycle,
			withCaller:              true,
			injectLifecycleTemplate: lifecycleTemplateGood,
			shouldTryToCreate:       true,
			injectCount:             4,
			exceedsLimit:            false,
			expectCreated:           sampleOrgLifecycle,
		},
		{
			name:                    "successfully create a new project release lifecycle",
			input:                   orgReleaseLifecycle,
			withCaller:              true,
			injectLifecycleTemplate: lifecycleTemplateGood,
			shouldTryToCreate:       true,
			injectCount:             4,
			exceedsLimit:            false,
			expectCreated:           sampleProjectLifecycle,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockOrganizations := db.NewMockOrganizations(t)
			mockProjects := db.NewMockProjects(t)
			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)
			mockReleaseLifecycles := db.NewMockReleaseLifecycles(t)
			mockUsers := db.NewMockUsers(t)
			mockTransactions := db.NewMockTransactions(t)
			mockLimitChecker := limits.NewMockLimitChecker(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				if test.injectOrganizationError == nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)

					if test.injectPermissionError == nil {
						mockLifecycleTemplates.On("GetLifecycleTemplateByID", mock.Anything, mock.Anything).
							Return(test.injectLifecycleTemplate, test.injectLifecycleTemplateError)

						if test.input.Scope.Equals(models.ProjectScope) {
							mockProjects.On("GetProjectByID", mock.Anything, *test.input.ProjectID).
								Return(&models.Project{
									Metadata: models.ResourceMetadata{
										ID: *test.input.ProjectID,
									},
								}, nil)
						}

						if test.shouldTryToCreate {
							mockCaller.On("GetSubject").Return("mock-caller-subject")

							mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
							mockTransactions.On("RollbackTx", mock.Anything).Return(nil)

							mockReleaseLifecycles.On("CreateReleaseLifecycle", mock.Anything, mock.Anything).
								Return(test.expectCreated, test.injectCreateError)

							if test.injectCreateError == nil {
								mockReleaseLifecycles.On("GetReleaseLifecycles", mock.Anything, mock.Anything).
									Return(&db.ReleaseLifecyclesResult{
										PageInfo: &pagination.PageInfo{
											TotalCount: test.injectCount,
										},
									}, nil)

								mockLimitChecker.On("CheckLimit", mock.Anything, limits.ResourceLimitReleaseLifecyclesPerOrganization, test.injectCount).
									Return(test.limitError)

								if !test.exceedsLimit {
									mockTransactions.On("CommitTx", mock.Anything).Return(nil)

									if test.expectCreated != nil {
										mockActivityEvents.On("CreateActivityEvent", mock.Anything,
											&activityevent.CreateActivityEventInput{
												OrganizationID: &orgID,
												ProjectID:      test.expectCreated.ProjectID,
												Action:         models.ActionCreate,
												TargetType:     models.TargetReleaseLifecycle,
												TargetID:       &sampleOrgLifecycle.Metadata.ID,
											},
										).Return(&models.ActivityEvent{}, nil)
									}
								}
							}
						}
					}
				}
			}

			dbClient := &db.Client{
				Users:              mockUsers,
				Organizations:      mockOrganizations,
				Projects:           mockProjects,
				LifecycleTemplates: mockLifecycleTemplates,
				ReleaseLifecycles:  mockReleaseLifecycles,
				Transactions:       mockTransactions,
			}

			mockLifecycleDataStore := lifecycletemplate.NewMockDataStore(t)

			mockLifecycleDataStore.On("GetData", mock.Anything, test.input.LifecycleTemplateID).
				Return(io.NopCloser(strings.NewReader(`
				stage dev {
					deployment {
						environment = "account1"
					}
					deployment {
						environment = "account2"
					}
				}`)), nil).Maybe()

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:                   dbClient,
				lifecycleTemplateDataStore: mockLifecycleDataStore,
				logger:                     logger,
				limitChecker:               mockLimitChecker,
				activityService:            mockActivityEvents,
			}

			actualCreated, err := service.CreateReleaseLifecycle(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreated, actualCreated)
		})
	}
}

func TestUpdateReleaseLifecycle(t *testing.T) {
	releaseLifecycleID := "release-lifecycle-id"
	originalLifecycleTemplateID := "original-lifecycle-template-id"
	updatedLifecycleTemplateID := "updated-lifecycle-template-id"
	organizationID := "organization-id"
	projectID := "project-id-1"

	updateInput := &UpdateReleaseLifecycleInput{
		ID:                  releaseLifecycleID,
		LifecycleTemplateID: updatedLifecycleTemplateID,
	}

	injectOrganizationOriginal := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: releaseLifecycleID,
		},
		Name:                "release-lifecycle-name",
		CreatedBy:           "created-by-someone",
		Scope:               models.OrganizationScope,
		OrganizationID:      organizationID,
		LifecycleTemplateID: originalLifecycleTemplateID,
	}

	expectOrganizationUpdated := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: releaseLifecycleID,
		},
		Name:                "release-lifecycle-name",
		CreatedBy:           "created-by-someone",
		Scope:               models.OrganizationScope,
		OrganizationID:      organizationID,
		LifecycleTemplateID: updatedLifecycleTemplateID,
		EnvironmentNames:    []string{"account1", "account2"},
	}

	injectProjectOriginal := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: releaseLifecycleID,
		},
		Name:                "release-lifecycle-name",
		CreatedBy:           "created-by-someone",
		Scope:               models.ProjectScope,
		OrganizationID:      organizationID,
		ProjectID:           &projectID,
		LifecycleTemplateID: originalLifecycleTemplateID,
	}

	expectProjectUpdated := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: releaseLifecycleID,
		},
		Name:                "release-lifecycle-name",
		CreatedBy:           "created-by-someone",
		Scope:               models.ProjectScope,
		OrganizationID:      organizationID,
		ProjectID:           &projectID,
		LifecycleTemplateID: updatedLifecycleTemplateID,
		EnvironmentNames:    []string{"account1", "account2"},
	}

	lifecycleTemplatePending := &models.LifecycleTemplate{
		Status: models.LifecycleTemplatePending,
	}

	lifecycleTemplateGoodOrg := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			ID: updatedLifecycleTemplateID,
		},
		Status:         models.LifecycleTemplateUploaded,
		Scope:          models.OrganizationScope,
		OrganizationID: organizationID,
	}

	lifecycleTemplateGoodProject := &models.LifecycleTemplate{
		Metadata: models.ResourceMetadata{
			ID: updatedLifecycleTemplateID,
		},
		Status:    models.LifecycleTemplateUploaded,
		Scope:     models.ProjectScope,
		ProjectID: &projectID,
	}

	type testCase struct {
		injectGetByIDError             error
		injectPermissionError          error
		injectLifecycleTemplateError   error
		input                          *UpdateReleaseLifecycleInput
		injectOriginalReleaseLifecycle *models.ReleaseLifecycle
		injectLifecycleTemplate        *models.LifecycleTemplate
		expectResult                   *models.ReleaseLifecycle
		name                           string
		expectErrorCode                errors.CodeType
		withCaller                     bool
		shouldTryToUpdate              bool
	}

	/*
		test case template

		type testCase struct {
			name                           string
			withCaller                     bool
			input                          *UpdateReleaseLifecycleInput
			injectGetByIDError             error
			injectOriginalReleaseLifecycle *models.ReleaseLifecycle
			injectPermissionError          error
			injectLifecycleTemplate        *models.LifecycleTemplate
			injectLifecycleTemplateError   error
			shouldTryToUpdate              bool
			expectErrorCode                errors.CodeType
			expectResult                   *models.ReleaseLifecycle
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			input:           updateInput,
			expectErrorCode: errors.EUnauthorized,
		},

		{
			name:               "getReleaseLifecycleByID failed",
			withCaller:         true,
			input:              updateInput,
			injectGetByIDError: errors.New("injected error, release lifecycle not found", errors.WithErrorCode(errors.ENotFound)),
			expectErrorCode:    errors.ENotFound,
		},

		{
			name:                           "permission check failed",
			withCaller:                     true,
			input:                          updateInput,
			injectOriginalReleaseLifecycle: injectOrganizationOriginal,
			injectPermissionError:          errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:                errors.EForbidden,
		},

		{
			name:                           "lifecycle template fetch failed due to internal error",
			withCaller:                     true,
			input:                          updateInput,
			injectOriginalReleaseLifecycle: injectOrganizationOriginal,
			injectLifecycleTemplateError:   errors.New("injected error, lifecycle template fetch failed"),
			expectErrorCode:                errors.EInternal,
		},

		{
			name:                           "lifecycle template fetch returned nothing",
			withCaller:                     true,
			input:                          updateInput,
			injectOriginalReleaseLifecycle: injectOrganizationOriginal,
			injectLifecycleTemplateError:   nil,
			injectLifecycleTemplate:        nil,
			expectErrorCode:                errors.ENotFound,
		},

		{
			name:                           "lifecycle template was not uploaded",
			withCaller:                     true,
			input:                          updateInput,
			injectOriginalReleaseLifecycle: injectOrganizationOriginal,
			injectLifecycleTemplate:        lifecycleTemplatePending,
			expectErrorCode:                errors.EInvalid,
		},

		{
			name:                           "not allowed to update an org-scope release lifecycle with a project-scope lifecycle template",
			withCaller:                     true,
			input:                          updateInput,
			injectOriginalReleaseLifecycle: injectOrganizationOriginal,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status: models.LifecycleTemplateUploaded,
				Scope:  models.ProjectScope,
			},
			expectErrorCode: errors.EInvalid,
		},

		{
			name:                           "not allowed to update a project-scope release lifecycle with a org-scope lifecycle template",
			input:                          updateInput,
			withCaller:                     true,
			injectOriginalReleaseLifecycle: injectProjectOriginal,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status: models.LifecycleTemplateUploaded,
				Scope:  models.OrganizationScope,
			},
			expectErrorCode: errors.EInvalid,
		},

		{
			name:                           "not allowed: update: org release lifecycle, org lifecycle template, different orgs",
			input:                          updateInput,
			withCaller:                     true,
			injectOriginalReleaseLifecycle: injectOrganizationOriginal,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status:         models.LifecycleTemplateUploaded,
				Scope:          models.OrganizationScope,
				OrganizationID: "other-organization",
			},
			expectErrorCode: errors.EInvalid,
		},

		{
			name:                           "not allowed: update: project release lifecycle, project lifecycle template, different projects",
			input:                          updateInput,
			withCaller:                     true,
			injectOriginalReleaseLifecycle: injectProjectOriginal,
			injectLifecycleTemplate: &models.LifecycleTemplate{
				Status:    models.LifecycleTemplateUploaded,
				Scope:     models.ProjectScope,
				ProjectID: ptr.String("other-project"),
			},
			expectErrorCode: errors.EInvalid,
		},

		{
			name:                           "successfully update of an organization release lifecycle",
			withCaller:                     true,
			input:                          updateInput,
			injectOriginalReleaseLifecycle: injectOrganizationOriginal,
			injectLifecycleTemplate:        lifecycleTemplateGoodOrg,
			shouldTryToUpdate:              true,
			expectResult:                   expectOrganizationUpdated,
		},

		{
			name:                           "successfully update of a project release lifecycle",
			withCaller:                     true,
			input:                          updateInput,
			injectOriginalReleaseLifecycle: injectProjectOriginal,
			injectLifecycleTemplate:        lifecycleTemplateGoodProject,
			shouldTryToUpdate:              true,
			expectResult:                   expectProjectUpdated,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleaseLifecycles := db.NewMockReleaseLifecycles(t)
			mockLifecycleTemplates := db.NewMockLifecycleTemplates(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockReleaseLifecycles.On("GetReleaseLifecycleByID", mock.Anything, releaseLifecycleID).
					Return(test.injectOriginalReleaseLifecycle, nil)

				if test.injectOriginalReleaseLifecycle != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)

					if test.injectPermissionError == nil {

						mockLifecycleTemplates.On("GetLifecycleTemplateByID", mock.Anything, mock.Anything).
							Return(test.injectLifecycleTemplate, test.injectLifecycleTemplateError)

						if test.shouldTryToUpdate {

							mockCaller.On("GetSubject").Return("some-subject")

							// Make sure the DB is being given the updated lifecycle template ID.
							originalPlus := *test.injectOriginalReleaseLifecycle
							originalPlus.LifecycleTemplateID = updatedLifecycleTemplateID
							originalPlus.EnvironmentNames = []string{"account1", "account2"}
							mockReleaseLifecycles.On("UpdateReleaseLifecycle", mock.Anything, &originalPlus).Return(test.expectResult, nil)

							mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
							mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
							mockTransactions.On("CommitTx", mock.Anything).Return(nil)

							mockActivityEvents.On("CreateActivityEvent", mock.Anything,
								&activityevent.CreateActivityEventInput{
									OrganizationID: &originalPlus.OrganizationID,
									ProjectID:      originalPlus.ProjectID,
									Action:         models.ActionUpdate,
									TargetType:     models.TargetReleaseLifecycle,
									TargetID:       &originalPlus.Metadata.ID,
								},
							).Return(&models.ActivityEvent{}, nil)
						}
					}
				}
			}

			dbClient := &db.Client{
				ReleaseLifecycles:  mockReleaseLifecycles,
				LifecycleTemplates: mockLifecycleTemplates,
				Transactions:       mockTransactions,
			}
			mockLifecycleDataStore := lifecycletemplate.NewMockDataStore(t)

			mockLifecycleDataStore.On("GetData", mock.Anything, test.input.LifecycleTemplateID).
				Return(io.NopCloser(strings.NewReader(`
				stage dev {
					deployment {
						environment = "account1"
					}
					deployment {
						environment = "account2"
					}
				}`)), nil).Maybe()

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:                   dbClient,
				lifecycleTemplateDataStore: mockLifecycleDataStore,
				logger:                     logger,
				activityService:            mockActivityEvents,
			}

			actualUpdated, err := service.UpdateReleaseLifecycle(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualUpdated)
		})
	}
}

func TestDeleteReleaseLifecycle(t *testing.T) {
	orgID := "org-1"
	projID := "proj-1"
	orgReleaseLifecycleID := "org-release-lifecycle-id"
	projReleaseLifecycleID := "proj-release-lifecycle-id"

	orgToDelete := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: orgReleaseLifecycleID,
		},
		Name:           "org-release-lifecycle-name",
		CreatedBy:      "org-created-by-someone",
		Scope:          models.OrganizationScope,
		OrganizationID: orgID,
	}

	projToDelete := &models.ReleaseLifecycle{
		Metadata: models.ResourceMetadata{
			ID: projReleaseLifecycleID,
		},
		Name:           "proj-release-lifecycle-name",
		CreatedBy:      "proj-created-by-someone",
		Scope:          models.ProjectScope,
		OrganizationID: orgID,
		ProjectID:      &projID,
	}

	orgDeleteInput := &DeleteReleaseLifecycleInput{
		ID: orgReleaseLifecycleID,
	}

	projDeleteInput := &DeleteReleaseLifecycleInput{
		ID: projReleaseLifecycleID,
	}

	type testCase struct {
		injectGetByIDError             error
		injectPermissionError          error
		input                          *DeleteReleaseLifecycleInput
		injectOriginalReleaseLifecycle *models.ReleaseLifecycle
		name                           string
		expectErrorCode                errors.CodeType
		withCaller                     bool
	}

	/*
		test case template

		type testCase struct {
			name                           string
			withCaller                     bool
			input                          *DeleteReleaseLifecycleInput
			injectGetByIDError             error
			injectOriginalReleaseLifecycle *models.ReleaseLifecycle
			injectPermissionError          error
			expectErrorCode                errors.CodeType
		}
	*/

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			input:           orgDeleteInput,
			expectErrorCode: errors.EUnauthorized,
		},

		{
			name:               "getReleaseLifecycleByID failed",
			withCaller:         true,
			input:              orgDeleteInput,
			injectGetByIDError: errors.New("injected error, release lifecycle not found", errors.WithErrorCode(errors.ENotFound)),
			expectErrorCode:    errors.ENotFound,
		},

		{
			name:                           "permission check failed",
			withCaller:                     true,
			input:                          orgDeleteInput,
			injectOriginalReleaseLifecycle: orgToDelete,
			injectPermissionError:          errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:                errors.EForbidden,
		},

		{
			name:                           "successfully delete of an org release lifecycle",
			withCaller:                     true,
			input:                          orgDeleteInput,
			injectOriginalReleaseLifecycle: orgToDelete,
		},

		{
			name:                           "successfully delete of a proj release lifecycle",
			withCaller:                     true,
			input:                          projDeleteInput,
			injectOriginalReleaseLifecycle: projToDelete,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockReleaseLifecycles := db.NewMockReleaseLifecycles(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockReleaseLifecycles.On("GetReleaseLifecycleByID", mock.Anything, test.input.ID).
					Return(test.injectOriginalReleaseLifecycle, nil)

				if test.injectOriginalReleaseLifecycle != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)

					if test.injectPermissionError == nil {
						mockCaller.On("GetSubject").Return("some-subject")

						mockReleaseLifecycles.On("DeleteReleaseLifecycle", mock.Anything, mock.Anything).Return(nil)

						mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
						mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
						mockTransactions.On("CommitTx", mock.Anything).Return(nil)

						mockActivityEvents.On("CreateActivityEvent", mock.Anything,
							&activityevent.CreateActivityEventInput{
								OrganizationID: &test.injectOriginalReleaseLifecycle.OrganizationID,
								ProjectID:      test.injectOriginalReleaseLifecycle.ProjectID,
								Action:         models.ActionDelete,
								TargetType:     models.TargetOrganization,
								TargetID:       &test.injectOriginalReleaseLifecycle.OrganizationID,
								Payload: &models.ActivityEventDeleteResourcePayload{
									Name: &test.injectOriginalReleaseLifecycle.Name,
									ID:   test.injectOriginalReleaseLifecycle.Metadata.ID,
									Type: models.TargetReleaseLifecycle.String(),
								},
							},
						).Return(&models.ActivityEvent{}, nil)
					}
				}
			}

			dbClient := &db.Client{
				ReleaseLifecycles: mockReleaseLifecycles,
				Transactions:      mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			err := service.DeleteReleaseLifecycle(ctx, test.input)

			assert.Equal(t, (test.expectErrorCode != ""), (err != nil))
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}
		})
	}
}
