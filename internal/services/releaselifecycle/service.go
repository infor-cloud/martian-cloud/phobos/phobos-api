// Package releaselifecycle implements functionality related to Phobos release lifecycles.
package releaselifecycle

import (
	"context"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/limits"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/lifecycletemplate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetReleaseLifecyclesInput is the input for querying a list of release lifecycles
type GetReleaseLifecyclesInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ReleaseLifecycleSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Search filters the release lifecycles by the name prefix
	Search *string
	// OrganizationID filters the release lifecycles by the specified organization.
	OrganizationID *string
	// ProjectID filters the release lifecycles by the specified project.
	ProjectID *string
	// ReleaseLifecycleScopes will filter the release lifecycles that are scope to specific level
	ReleaseLifecycleScopes []models.ScopeType
}

// CreateReleaseLifecycleInput is the input for creating a release lifecycle
type CreateReleaseLifecycleInput struct {
	Scope               models.ScopeType
	OrganizationID      *string
	ProjectID           *string
	Name                string
	LifecycleTemplateID string
}

// UpdateReleaseLifecycleInput is the input for updating the lifecycle template ID of a release lifecycle
type UpdateReleaseLifecycleInput struct {
	Version             *int
	ID                  string
	LifecycleTemplateID string
}

// DeleteReleaseLifecycleInput is the input for deleting a release lifecycle
type DeleteReleaseLifecycleInput struct {
	Version *int
	ID      string
}

// Service implements all release lifecycle related functionality
// Please note the HCL data is NOT returned by the Get... methods other than GetReleaseLifecycleData.
type Service interface {
	GetReleaseLifecycleByID(ctx context.Context, id string) (*models.ReleaseLifecycle, error)
	GetReleaseLifecycleByPRN(ctx context.Context, prn string) (*models.ReleaseLifecycle, error)
	GetReleaseLifecyclesByIDs(ctx context.Context, idList []string) ([]*models.ReleaseLifecycle, error)
	GetReleaseLifecycles(ctx context.Context, input *GetReleaseLifecyclesInput) (*db.ReleaseLifecyclesResult, error)
	CreateReleaseLifecycle(ctx context.Context, input *CreateReleaseLifecycleInput) (*models.ReleaseLifecycle, error)
	UpdateReleaseLifecycle(ctx context.Context, input *UpdateReleaseLifecycleInput) (*models.ReleaseLifecycle, error)
	DeleteReleaseLifecycle(ctx context.Context, input *DeleteReleaseLifecycleInput) error
}

type service struct {
	logger                     logger.Logger
	dbClient                   *db.Client
	lifecycleTemplateDataStore lifecycletemplate.DataStore
	limitChecker               limits.LimitChecker
	activityService            activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	lifecycleTemplateDataStore lifecycletemplate.DataStore,
	limitChecker limits.LimitChecker,
	activityService activityevent.Service,
) Service {
	return &service{
		logger:                     logger,
		dbClient:                   dbClient,
		lifecycleTemplateDataStore: lifecycleTemplateDataStore,
		limitChecker:               limitChecker,
		activityService:            activityService,
	}
}

func (s *service) GetReleaseLifecycleByID(ctx context.Context, id string) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleaseLifecycleByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	releaseLifecycle, err := s.getReleaseLifecycleByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this release lifecycle.
	switch releaseLifecycle.Scope {
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.ReleaseLifecycleResource,
			auth.WithOrganizationID(releaseLifecycle.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewReleaseLifecycle, auth.WithProjectID(*releaseLifecycle.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid release lifecycle scope: %s", releaseLifecycle.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return releaseLifecycle, nil
}

func (s *service) GetReleaseLifecycleByPRN(ctx context.Context, prn string) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleaseLifecycleByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	releaseLifecycle, err := s.dbClient.ReleaseLifecycles.GetReleaseLifecycleByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release lifecycle by PRN", errors.WithSpan(span))
	}

	if releaseLifecycle == nil {
		return nil, errors.New(
			"release lifecycle with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	switch releaseLifecycle.Scope {
	case models.OrganizationScope:
		err = caller.RequireAccessToInheritableResource(ctx, models.ReleaseLifecycleResource,
			auth.WithOrganizationID(releaseLifecycle.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.ViewReleaseLifecycle, auth.WithProjectID(*releaseLifecycle.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid release lifecycle scope: %s", releaseLifecycle.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	return releaseLifecycle, nil
}

func (s *service) GetReleaseLifecyclesByIDs(ctx context.Context, idList []string) ([]*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleaseLifecyclesByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetReleaseLifecyclesInput{
		Filter: &db.ReleaseLifecycleFilter{
			ReleaseLifecycleIDs: idList,
		},
	}

	resp, err := s.dbClient.ReleaseLifecycles.GetReleaseLifecycles(ctx,
		dbInput,
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release lifecycles", errors.WithSpan(span))
	}

	// Make sure caller has permission to view release lifecycles in all the parent organizations and/or projects.
	for _, rl := range resp.ReleaseLifecycles {
		switch rl.Scope {
		case models.OrganizationScope:
			err = caller.RequireAccessToInheritableResource(ctx, models.ReleaseLifecycleResource,
				auth.WithOrganizationID(rl.OrganizationID))
			if err != nil {
				return nil, err
			}
		case models.ProjectScope:
			err = caller.RequirePermission(ctx, models.ViewReleaseLifecycle, auth.WithProjectID(*rl.ProjectID))
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("invalid release lifecycle scope: %s", rl.Scope,
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	return resp.ReleaseLifecycles, nil
}

func (s *service) GetReleaseLifecycles(ctx context.Context, input *GetReleaseLifecyclesInput) (*db.ReleaseLifecyclesResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetReleaseLifecycles")
	if input.OrganizationID != nil {
		span.SetAttributes(attribute.String("organization", *input.OrganizationID))
	}
	if input.ProjectID != nil {
		span.SetAttributes(attribute.String("project", *input.ProjectID))
	}
	if input.Search != nil {
		span.SetAttributes(attribute.String("search", *input.Search))
	}

	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetReleaseLifecyclesInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ReleaseLifecycleFilter{
			OrganizationID:         input.OrganizationID,
			ProjectID:              input.ProjectID,
			Search:                 input.Search,
			ReleaseLifecycleScopes: input.ReleaseLifecycleScopes,
		},
	}

	resp, err := s.dbClient.ReleaseLifecycles.GetReleaseLifecycles(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release lifecycles", errors.WithSpan(span))
	}

	// Make sure caller has permission to view release lifecycles in all the parent organizations and/or projects.
	for _, rl := range resp.ReleaseLifecycles {
		switch rl.Scope {
		case models.OrganizationScope:
			err = caller.RequireAccessToInheritableResource(ctx, models.ReleaseLifecycleResource,
				auth.WithOrganizationID(rl.OrganizationID))
			if err != nil {
				return nil, err
			}
		case models.ProjectScope:
			err = caller.RequirePermission(ctx, models.ViewReleaseLifecycle, auth.WithProjectID(*rl.ProjectID))
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("invalid release lifecycle scope: %s", rl.Scope,
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	}

	return resp, nil
}

func (s *service) CreateReleaseLifecycle(ctx context.Context, input *CreateReleaseLifecycleInput) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateReleaseLifecycle")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	var orgID string
	switch input.Scope {
	case models.OrganizationScope:
		if input.OrganizationID == nil {
			return nil, errors.New("to create a release lifecycle of organization scope, must supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.ProjectID != nil {
			return nil, errors.New("to create a release lifecycle of organization scope, not allowed to supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		orgID = *input.OrganizationID
		err = caller.RequirePermission(ctx, models.CreateReleaseLifecycle, auth.WithOrganizationID(orgID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		if input.ProjectID == nil {
			return nil, errors.New("to create a release lifecycle of project scope, must supply project ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if input.OrganizationID != nil {
			return nil, errors.New("to create a release lifecycle of project scope, not allowed to supply organization ID",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		projectID := *input.ProjectID
		err = caller.RequirePermission(ctx, models.CreateReleaseLifecycle, auth.WithProjectID(projectID))
		if err != nil {
			return nil, err
		}

		// Get the project in order to have the organization ID.
		project, pErr := s.dbClient.Projects.GetProjectByID(ctx, projectID)
		if pErr != nil {
			return nil, pErr
		}
		if project == nil {
			return nil, errors.New("failed to find project with ID %s", projectID, errors.WithSpan(span))
		}

		orgID = project.OrgID
	default:
		return nil, errors.New("not allowed to create a release lifecycle with %s scope",
			input.Scope, errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	lifecycleTemplate, err := s.verifyLifecycleTemplateExistsAndUploaded(ctx, span, input.LifecycleTemplateID)
	if err != nil {
		// Error has already been recorded in span.
		return nil, err
	}

	switch input.Scope {
	case models.OrganizationScope:
		if lifecycleTemplate.Scope.Equals(models.ProjectScope) {
			return nil, errors.New("not allowed to create an organization release lifecycle with a project lifecycle template",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if *input.OrganizationID != lifecycleTemplate.OrganizationID {
			return nil, errors.New("release lifecycle and lifecycle template must be in the same organization",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	case models.ProjectScope:
		if lifecycleTemplate.Scope.Equals(models.OrganizationScope) {
			return nil, errors.New("not allowed to create a project release lifecycle with an organization lifecycle template",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if *input.ProjectID != *lifecycleTemplate.ProjectID {
			return nil, errors.New("release lifecycle and lifecycle template must be in the same project",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
		// Default case was already checked above.
	}

	rdr, err := s.lifecycleTemplateDataStore.GetData(ctx, input.LifecycleTemplateID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle template data", errors.WithSpan(span))
	}
	defer rdr.Close()

	// Parse the lifecycle template to ensure it is valid.
	lifecycleConfig, _, err := config.NewLifecycleTemplate(rdr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse lifecycle template", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	toCreate := &models.ReleaseLifecycle{
		Scope:               input.Scope,
		OrganizationID:      orgID,
		ProjectID:           input.ProjectID,
		Name:                input.Name,
		CreatedBy:           caller.GetSubject(),
		LifecycleTemplateID: input.LifecycleTemplateID,
		EnvironmentNames:    lifecycleConfig.GetEnvironmentNames(),
	}

	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate release lifecycle", errors.WithSpan(span))
	}

	// Must do a transaction in order to roll back if the limit was exceeded.
	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateReleaseLifecycle: %v", txErr)
		}
	}()

	releaseLifecycle, err := s.dbClient.ReleaseLifecycles.CreateReleaseLifecycle(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create release lifecycle", errors.WithSpan(span))
	}

	// Check limit if an org release lifecycle.
	if input.Scope.Equals(models.OrganizationScope) {
		releaseLifecyclesResult, gErr := s.dbClient.ReleaseLifecycles.GetReleaseLifecycles(
			txContext,
			&db.GetReleaseLifecyclesInput{
				Filter: &db.ReleaseLifecycleFilter{
					OrganizationID:         &orgID,
					ReleaseLifecycleScopes: []models.ScopeType{models.OrganizationScope},
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
			},
		)
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get organization's release lifecycles", errors.WithSpan(span))
		}

		if cErr := s.limitChecker.CheckLimit(txContext, limits.ResourceLimitReleaseLifecyclesPerOrganization,
			releaseLifecyclesResult.PageInfo.TotalCount); cErr != nil {
			return nil, cErr
		}
	}

	// Check limit if an project release lifecycle.
	if input.Scope.Equals(models.ProjectScope) {
		releaseLifecyclesResult, gErr := s.dbClient.ReleaseLifecycles.GetReleaseLifecycles(
			txContext,
			&db.GetReleaseLifecyclesInput{
				Filter: &db.ReleaseLifecycleFilter{
					ProjectID:              releaseLifecycle.ProjectID,
					ReleaseLifecycleScopes: []models.ScopeType{models.ProjectScope},
				},
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(0),
				},
			},
		)
		if gErr != nil {
			return nil, errors.Wrap(gErr, "failed to get project's release lifecycles", errors.WithSpan(span))
		}

		if cErr := s.limitChecker.CheckLimit(txContext, limits.ResourceLimitReleaseLifecyclesPerProject,
			releaseLifecyclesResult.PageInfo.TotalCount); cErr != nil {
			return nil, cErr
		}
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &releaseLifecycle.OrganizationID,
			ProjectID:      releaseLifecycle.ProjectID,
			Action:         models.ActionCreate,
			TargetType:     models.TargetReleaseLifecycle,
			TargetID:       &releaseLifecycle.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a release lifecycle.",
		"caller", caller.GetSubject(),
		"releaseLifecycleID", releaseLifecycle.Metadata.ID,
		"organizationID", releaseLifecycle.OrganizationID,
	)

	return releaseLifecycle, nil
}

func (s *service) UpdateReleaseLifecycle(ctx context.Context, input *UpdateReleaseLifecycleInput) (*models.ReleaseLifecycle, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateReleaseLifecycle")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	releaseLifecycle, err := s.getReleaseLifecycleByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	switch releaseLifecycle.Scope {
	case models.OrganizationScope:
		err = caller.RequirePermission(ctx, models.UpdateReleaseLifecycle,
			auth.WithOrganizationID(releaseLifecycle.OrganizationID))
		if err != nil {
			return nil, err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.UpdateReleaseLifecycle,
			auth.WithProjectID(*releaseLifecycle.ProjectID))
		if err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("invalid release lifecycle scope: %s", releaseLifecycle.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	lifecycleTemplate, err := s.verifyLifecycleTemplateExistsAndUploaded(ctx, span, input.LifecycleTemplateID)
	if err != nil {
		// Error has already been recorded in span.
		return nil, err
	}

	switch releaseLifecycle.Scope {
	case models.OrganizationScope:
		if lifecycleTemplate.Scope.Equals(models.ProjectScope) {
			return nil, errors.New("not allowed to update an organization release lifecycle with a project lifecycle template",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if releaseLifecycle.OrganizationID != lifecycleTemplate.OrganizationID {
			return nil, errors.New("release lifecycle and lifecycle template must be in the same organization",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
	case models.ProjectScope:
		if lifecycleTemplate.Scope.Equals(models.OrganizationScope) {
			return nil, errors.New("not allowed to update a project release lifecycle with an organization lifecycle template",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}

		if *releaseLifecycle.ProjectID != *lifecycleTemplate.ProjectID {
			return nil, errors.New("release lifecycle and lifecycle template must be in the same project",
				errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
		}
		// Default for other scopes has already been checked above.
	}

	rdr, err := s.lifecycleTemplateDataStore.GetData(ctx, input.LifecycleTemplateID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle template data", errors.WithSpan(span))
	}
	defer rdr.Close()

	// Parse the lifecycle template to ensure it is valid.
	lifecycleConfig, _, err := config.NewLifecycleTemplate(rdr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse lifecycle template", errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	// If this lifecycle template ID is not valid, the DB operation will fail.
	releaseLifecycle.LifecycleTemplateID = input.LifecycleTemplateID

	if input.Version != nil {
		releaseLifecycle.Metadata.Version = *input.Version
	}

	releaseLifecycle.EnvironmentNames = lifecycleConfig.GetEnvironmentNames()

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateReleaseLifecycle: %v", txErr)
		}
	}()

	updatedReleaseLifecycle, err := s.dbClient.ReleaseLifecycles.UpdateReleaseLifecycle(txContext, releaseLifecycle)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &releaseLifecycle.OrganizationID,
			ProjectID:      releaseLifecycle.ProjectID,
			Action:         models.ActionUpdate,
			TargetType:     models.TargetReleaseLifecycle,
			TargetID:       &releaseLifecycle.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a release lifecycle.",
		"caller", caller.GetSubject(),
		"organizationID", releaseLifecycle.OrganizationID,
		"releaseLifecycleName", releaseLifecycle.Name,
		"releaseLifecycleID", releaseLifecycle.Metadata.ID,
		"lifecycleTemplateID", releaseLifecycle.LifecycleTemplateID,
	)

	return updatedReleaseLifecycle, nil
}

func (s *service) DeleteReleaseLifecycle(ctx context.Context, input *DeleteReleaseLifecycleInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteReleaseLifecycle")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	releaseLifecycle, err := s.getReleaseLifecycleByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	switch releaseLifecycle.Scope {
	case models.OrganizationScope:
		err = caller.RequirePermission(ctx, models.DeleteReleaseLifecycle,
			auth.WithOrganizationID(releaseLifecycle.OrganizationID))
		if err != nil {
			return err
		}
	case models.ProjectScope:
		err = caller.RequirePermission(ctx, models.DeleteReleaseLifecycle,
			auth.WithProjectID(*releaseLifecycle.ProjectID))
		if err != nil {
			return err
		}
	default:
		return errors.New("invalid release lifecycle scope: %s", releaseLifecycle.Scope,
			errors.WithErrorCode(errors.EInvalid), errors.WithSpan(span))
	}

	if input.Version != nil {
		releaseLifecycle.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeleteReleaseLifecycle: %v", txErr)
		}
	}()

	if err = s.dbClient.ReleaseLifecycles.DeleteReleaseLifecycle(txContext, releaseLifecycle); err != nil {
		return err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &releaseLifecycle.OrganizationID,
			ProjectID:      releaseLifecycle.ProjectID,
			Action:         models.ActionDelete,
			TargetType:     models.TargetOrganization,
			TargetID:       &releaseLifecycle.OrganizationID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &releaseLifecycle.Name,
				ID:   releaseLifecycle.Metadata.ID,
				Type: models.TargetReleaseLifecycle.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deleted a release lifecycle.",
		"caller", caller.GetSubject(),
		"releaseLifecycleName", releaseLifecycle.Name,
		"releaseLifecycleID", releaseLifecycle.Metadata.ID,
	)

	return nil
}

// getReleaseLifecycleByID returns a non-nil release lifecycle.
// If there is an error or no such release lifecycle exists, it records the error in the span.
func (s *service) getReleaseLifecycleByID(ctx context.Context, span trace.Span, id string) (*models.ReleaseLifecycle, error) {
	gotReleaseLifecycle, err := s.dbClient.ReleaseLifecycles.GetReleaseLifecycleByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get release lifecycle by ID", errors.WithSpan(span))
	}

	if gotReleaseLifecycle == nil {
		return nil, errors.New(
			"release lifecycle with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return gotReleaseLifecycle, nil
}

// verifyLifecycleTemplateExistsAndUploaded verifies that the lifecycle template exists and has been uploaded.
// If there is an error, it records the error to the span.
func (s *service) verifyLifecycleTemplateExistsAndUploaded(
	ctx context.Context, span trace.Span, id string) (*models.LifecycleTemplate, error) {

	// Verify that the lifecycle template exists and has been uploaded.
	lifecycleTemplate, err := s.dbClient.LifecycleTemplates.GetLifecycleTemplateByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get lifecycle template to verify it exists and has been uploaded", errors.WithSpan(span))
	}
	if lifecycleTemplate == nil {
		return nil, errors.New(
			"lifecycle template with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}
	if lifecycleTemplate.Status != models.LifecycleTemplateUploaded {
		return nil, errors.New(
			"lifecycle template with id %s has not been uploaded", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.EInvalid))
	}

	return lifecycleTemplate, nil
}
