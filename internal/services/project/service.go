// Package project implements functionality related to Phobos projects.
package project

import (
	"context"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

// GetProjectsInput is the input for querying a list of projects
type GetProjectsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.ProjectSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// Organization filters the projects by the specified organization
	Organization *models.Organization
	// Search is used to search for a project by name
	Search *string
}

// CreateProjectInput is the input for creating a project
type CreateProjectInput struct {
	Name        string
	Description string
	OrgID       string
}

// UpdateProjectInput is the input for updating a project
// For now, a project cannot move between orgs or into or out of an org.
type UpdateProjectInput struct {
	Version     *int
	Description *string
	ID          string
}

// DeleteProjectInput is the input for deleting a project
type DeleteProjectInput struct {
	Version *int
	ID      string
}

// Service implements all project related functionality
type Service interface {
	GetProjectByID(ctx context.Context, id string) (*models.Project, error)
	GetProjectsByIDs(ctx context.Context, idList []string) ([]models.Project, error)
	GetProjectByPRN(ctx context.Context, prn string) (*models.Project, error)
	GetProjects(ctx context.Context, input *GetProjectsInput) (*db.ProjectsResult, error)
	CreateProject(ctx context.Context, input *CreateProjectInput) (*models.Project, error)
	UpdateProject(ctx context.Context, input *UpdateProjectInput) (*models.Project, error)
	DeleteProject(ctx context.Context, input *DeleteProjectInput) error
}

type service struct {
	logger          logger.Logger
	dbClient        *db.Client
	activityService activityevent.Service
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	activeService activityevent.Service,
) Service {
	return &service{
		logger:          logger,
		dbClient:        dbClient,
		activityService: activeService,
	}
}

func (s *service) GetProjectByID(ctx context.Context, id string) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectByID")
	span.SetAttributes(attribute.String("id", id))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	project, err := s.getProjectByID(ctx, span, id)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	// Check whether the caller is allowed to view this project.
	err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(project.Metadata.ID))
	if err != nil {
		return nil, err
	}

	return project, nil
}

func (s *service) GetProjectByPRN(ctx context.Context, prn string) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	project, err := s.dbClient.Projects.GetProjectByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project by PRN", errors.WithSpan(span))
	}

	if project == nil {
		return nil, errors.New(
			"project with prn %s not found", prn, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	// Check whether the caller is allowed to view this project.
	err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(project.Metadata.ID))
	if err != nil {
		return nil, err
	}

	return project, nil
}

func (s *service) GetProjectsByIDs(ctx context.Context, idList []string) ([]models.Project, error) {
	ctx, span := tracer.Start(ctx, "svc.GetProjectsByIDs")
	span.SetAttributes(attribute.StringSlice("ID list", idList))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetProjectsInput{
		Filter: &db.ProjectFilter{
			ProjectIDs: idList,
		},
	}

	resp, err := s.dbClient.Projects.GetProjects(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get projects", errors.WithSpan(span))
	}

	for _, proj := range resp.Projects {
		err = caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(proj.Metadata.ID))
		if err != nil {
			return nil, err
		}
	}

	return resp.Projects, nil
}

func (s *service) GetProjects(ctx context.Context, input *GetProjectsInput) (*db.ProjectsResult, error) {
	var orgID *string

	ctx, span := tracer.Start(ctx, "svc.GetProjects")
	if input.Organization != nil {
		span.SetAttributes(attribute.String("organization", input.Organization.Name))
		orgID = &input.Organization.Metadata.ID
	}
	if input.Search != nil {
		span.SetAttributes(attribute.String("search", *input.Search))
	}
	defer span.End()

	_, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	dbInput := &db.GetProjectsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.ProjectFilter{
			OrgID:  orgID,
			Search: input.Search,
		},
	}

	// Limit returns to projects inside organizations that the caller is a member of.
	if err = auth.HandleCaller(
		ctx,
		func(_ context.Context, c *auth.UserCaller) error {
			if !c.User.Admin {
				dbInput.Filter.UserMemberID = &c.User.Metadata.ID
			}
			return nil
		},
		func(_ context.Context, c *auth.ServiceAccountCaller) error {
			dbInput.Filter.ServiceAccountMemberID = &c.ServiceAccountID
			return nil
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to handle caller", errors.WithSpan(span))
	}

	resp, err := s.dbClient.Projects.GetProjects(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get projects", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) CreateProject(ctx context.Context, input *CreateProjectInput) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "svc.CreateProject")
	span.SetAttributes(attribute.String("name", input.Name))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.CreateProject, auth.WithOrganizationID(input.OrgID))
	if err != nil {
		return nil, err
	}

	toCreate := &models.Project{
		Name:        input.Name,
		Description: input.Description,
		OrgID:       input.OrgID,
		CreatedBy:   caller.GetSubject(),
	}

	// Validate model
	if err = toCreate.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate a project model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CreateProject: %v", txErr)
		}
	}()

	project, err := s.dbClient.Projects.CreateProject(txContext, toCreate)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create project", errors.WithSpan(span))
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &project.Metadata.ID,
			Action:     models.ActionCreate,
			TargetType: models.TargetProject,
			TargetID:   &project.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Created a project.",
		"caller", caller.GetSubject(),
		"name", project.Name,
		"projectID", project.Metadata.ID,
	)

	return project, nil
}

func (s *service) UpdateProject(ctx context.Context, input *UpdateProjectInput) (*models.Project, error) {
	ctx, span := tracer.Start(ctx, "svc.UpdateProject")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	gotProj, err := s.getProjectByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.UpdateProject, auth.WithProjectID(gotProj.Metadata.ID))
	if err != nil {
		return nil, err
	}

	if input.Description != nil {
		gotProj.Description = *input.Description
	}

	if input.Version != nil {
		gotProj.Metadata.Version = *input.Version
	}

	if err = gotProj.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate project model", errors.WithSpan(span))
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer UpdateProject: %v", txErr)
		}
	}()

	updatedProj, err := s.dbClient.Projects.UpdateProject(txContext, gotProj)
	if err != nil {
		return nil, err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			ProjectID:  &updatedProj.Metadata.ID,
			Action:     models.ActionUpdate,
			TargetType: models.TargetProject,
			TargetID:   &updatedProj.Metadata.ID,
		},
	); err != nil {
		return nil, errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Updated a project.",
		"caller", caller.GetSubject(),
		"projectName", gotProj.Name,
		"projectID", gotProj.Metadata.ID,
	)

	return updatedProj, nil
}

func (s *service) DeleteProject(ctx context.Context, input *DeleteProjectInput) error {
	ctx, span := tracer.Start(ctx, "svc.DeleteProject")
	span.SetAttributes(attribute.String("id", input.ID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return err
	}

	gotProj, err := s.getProjectByID(ctx, span, input.ID)
	if err != nil {
		// error has already been recorded in the span
		return err
	}

	err = caller.RequirePermission(ctx, models.DeleteProject, auth.WithProjectID(gotProj.Metadata.ID))
	if err != nil {
		return err
	}

	if input.Version != nil {
		gotProj.Metadata.Version = *input.Version
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer DeletedProject: %v", txErr)
		}
	}()

	if err = s.dbClient.Projects.DeleteProject(txContext, gotProj); err != nil {
		return err
	}

	if _, err = s.activityService.CreateActivityEvent(txContext,
		&activityevent.CreateActivityEventInput{
			OrganizationID: &gotProj.OrgID,
			Action:         models.ActionDelete,
			TargetType:     models.TargetOrganization,
			TargetID:       &gotProj.OrgID,
			Payload: &models.ActivityEventDeleteResourcePayload{
				Name: &gotProj.Name,
				ID:   gotProj.Metadata.ID,
				Type: models.TargetProject.String(),
			},
		},
	); err != nil {
		return errors.Wrap(err, "failed to create activity event", errors.WithSpan(span))
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Deletion a project.",
		"caller", caller.GetSubject(),
		"projectName", gotProj.Name,
		"projectID", gotProj.Metadata.ID,
	)

	return nil
}

func (s *service) getProjectByID(ctx context.Context, span trace.Span, id string) (*models.Project, error) {
	gotProj, err := s.dbClient.Projects.GetProjectByID(ctx, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get project by ID", errors.WithSpan(span))
	}

	if gotProj == nil {
		return nil, errors.New(
			"project with id %s not found", id, errors.WithSpan(span),
			errors.WithErrorCode(errors.ENotFound))
	}

	return gotProj, nil
}
