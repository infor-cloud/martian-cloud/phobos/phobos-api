package project

import (
	"context"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/activityevent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	activityService := activityevent.NewMockService(t)

	expect := &service{
		logger:          logger,
		dbClient:        dbClient,
		activityService: activityService,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, activityService))
}

func TestGetProjectByID(t *testing.T) {
	projectID := "project-1"

	type testCase struct {
		expectProject   *models.Project
		name            string
		injectAuthError error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return a project",
			expectProject: &models.Project{
				Metadata: models.ResourceMetadata{
					ID: projectID,
				},
			},
		},
		{
			name:            "project does not exist",
			injectAuthError: errors.New("injected error, user is not an org owner", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.ENotFound,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)
			mockCaller := auth.NewMockCaller(t)

			mockProjects.On("GetProjectByID", mock.Anything, projectID).Return(test.expectProject, nil)

			if test.injectAuthError == nil {
				mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything).Return(test.injectAuthError)
			}

			dbClient := &db.Client{
				Projects: mockProjects,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualProject, err := service.GetProjectByID(auth.WithCaller(ctx, mockCaller), projectID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectProject, actualProject)
		})
	}
}

func TestGetProjectByPRN(t *testing.T) {
	projectPRN := "prn:project:org/project-1"

	type testCase struct {
		expectProject   *models.Project
		name            string
		injectAuthError error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully return a project",
			expectProject: &models.Project{
				Metadata: models.ResourceMetadata{
					PRN: projectPRN,
				},
			},
		},
		{
			name:            "project does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to view project",
			expectProject: &models.Project{
				Metadata: models.ResourceMetadata{
					PRN: projectPRN,
				},
			},
			injectAuthError: errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)
			mockCaller := auth.NewMockCaller(t)

			mockProjects.On("GetProjectByPRN", mock.Anything, projectPRN).Return(test.expectProject, nil)

			if test.expectProject != nil {
				mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything).Return(test.injectAuthError)
			}

			dbClient := &db.Client{
				Projects: mockProjects,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualProject, err := service.GetProjectByPRN(auth.WithCaller(ctx, mockCaller), projectPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectProject, actualProject)
		})
	}
}

func TestGetProjectsByIDs(t *testing.T) {
	inputList := []string{"proj-1"}

	type testCase struct {
		authError       error
		name            string
		expectErrorCode errors.CodeType
		expectProjects  []models.Project
	}

	testCases := []testCase{
		{
			name: "successfully return projects",
			expectProjects: []models.Project{
				{Metadata: models.ResourceMetadata{ID: "proj-1"}},
			},
		},
		{
			name:            "caller does not have permission to view projects",
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
			expectProjects: []models.Project{
				{Metadata: models.ResourceMetadata{ID: "proj-1"}},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockProjects := db.NewMockProjects(t)

			dbInput := &db.GetProjectsInput{
				Filter: &db.ProjectFilter{
					ProjectIDs: inputList,
				},
			}
			dbResult := &db.ProjectsResult{
				Projects: test.expectProjects,
			}

			mockProjects.On("GetProjects", mock.Anything, dbInput).Return(dbResult, nil)

			if len(test.expectProjects) > 0 {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).Return(test.authError)
			}

			dbClient := &db.Client{
				Projects: mockProjects,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualProjects, err := service.GetProjectsByIDs(auth.WithCaller(ctx, mockCaller), inputList)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectProjects, actualProjects)
		})
	}
}

func TestGetProjects(t *testing.T) {
	sort := db.ProjectSortableFieldUpdatedAtAsc

	type testCase struct {
		input           *GetProjectsInput
		expectResult    *db.ProjectsResult
		name            string
		expectErrorCode errors.CodeType
		withCaller      bool
	}

	testCases := []testCase{
		{
			name: "successfully return projects result, by search, positive",
			input: &GetProjectsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Search: ptr.String("proj-1"),
			},
			expectResult: &db.ProjectsResult{
				Projects: []models.Project{{Name: "proj-1"}},
			},
			withCaller: true,
		},
		{
			name: "successfully return projects result, by search, negative",
			input: &GetProjectsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Search: ptr.String("proj-1"),
			},
			expectResult: &db.ProjectsResult{
				Projects: []models.Project{},
			},
			withCaller: true,
		},
		{
			name: "successfully return projects result, by organization, positive",
			input: &GetProjectsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Organization: &models.Organization{
					Metadata: models.ResourceMetadata{
						ID: "org-id-1",
					},
					Name: "org-1",
				},
			},
			expectResult: &db.ProjectsResult{
				Projects: []models.Project{{Name: "proj-1"}},
			},
			withCaller: true,
		},
		{
			name: "successfully return projects result, by organization, negative",
			input: &GetProjectsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Organization: &models.Organization{
					Metadata: models.ResourceMetadata{
						ID: "org-id-1",
					},
					Name: "org-1",
				},
			},
			expectResult: &db.ProjectsResult{
				Projects: []models.Project{},
			},
			withCaller: true,
		},
		{
			name: "without caller",
			input: &GetProjectsInput{
				Sort: &sort,
				PaginationOptions: &pagination.Options{
					First: ptr.Int32(1),
				},
				Search: ptr.String("proj-1"),
			},
			expectErrorCode: errors.EUnauthorized,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockProjects := db.NewMockProjects(t)

			if test.expectResult != nil {
				var orgID *string
				if test.input.Organization != nil {
					orgID = &test.input.Organization.Metadata.ID
				}
				dbInput := &db.GetProjectsInput{
					Sort:              test.input.Sort,
					PaginationOptions: test.input.PaginationOptions,
					Filter: &db.ProjectFilter{
						OrgID:  orgID,
						Search: test.input.Search,
					},
				}
				mockProjects.On("GetProjects", mock.Anything, dbInput).Return(test.expectResult, nil)
			}

			if test.withCaller {
				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)
			}

			dbClient := &db.Client{
				Projects: mockProjects,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetProjects(ctx, test.input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestCreateProject(t *testing.T) {
	parentOrg := &models.Organization{
		Metadata: models.ResourceMetadata{
			ID: "org-1",
		},
	}

	sampleUser := &models.User{
		Metadata: models.ResourceMetadata{
			ID: "user-1",
		},
		Email: "sample-user@domain.tld",
	}

	sampleProject := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: "project-1",
		},
		Name:        "some-project",
		Description: "This is a new project",
		CreatedBy:   sampleUser.Email,
		OrgID:       parentOrg.Metadata.ID,
	}

	input := &CreateProjectInput{
		Name:        sampleProject.Name,
		Description: sampleProject.Description,
		OrgID:       parentOrg.Metadata.ID,
	}

	type testCase struct {
		expectCreated   *models.Project
		name            string
		injectAuthError error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:          "successfully create a new project",
			expectCreated: sampleProject,
		},
		{
			name:            "user is not an org owner",
			injectAuthError: errors.New("injected error, user is not an org owner", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ctx = auth.WithCaller(ctx, &auth.UserCaller{User: sampleUser})

			mockCaller := auth.MockCaller{}
			mockCaller.Test(t)

			mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything).Return(test.injectAuthError)

			mockCaller.On("GetSubject").Return(sampleUser.Email)

			mockProjects := db.NewMockProjects(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			if test.expectCreated != nil {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				toCreate := &models.Project{
					Name:        input.Name,
					Description: input.Description,
					OrgID:       input.OrgID,
					CreatedBy:   sampleUser.Email,
				}

				mockProjects.On("CreateProject", mock.Anything, toCreate).Return(test.expectCreated, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &sampleProject.Metadata.ID,
						Action:     models.ActionCreate,
						TargetType: models.TargetProject,
						TargetID:   &sampleProject.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Projects:     mockProjects,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			actualCreated, err := service.CreateProject(auth.WithCaller(ctx, &mockCaller), input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectCreated, actualCreated)
		})
	}
}

func TestUpdateProject(t *testing.T) {
	description := "This is a new description"
	version := 5

	input := &UpdateProjectInput{
		ID:          "project-id",
		Description: &description,
		Version:     &version,
	}

	existingProject := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: input.ID,
		},
		Name:        "project-name",
		Description: "This is the old description",
	}

	type testCase struct {
		existingProject *models.Project
		expectUpdated   *models.Project
		name            string
		injectAuthError error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:            "successfully update a project",
			existingProject: existingProject,
			expectUpdated: &models.Project{
				Metadata: models.ResourceMetadata{
					ID:      input.ID,
					Version: version,
				},
				Name:        existingProject.Name,
				Description: description,
			},
		},
		{
			name:            "project does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "user it not an org owner",
			existingProject: existingProject,
			injectAuthError: errors.New("injected error, user is not an org owner", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ctx = auth.WithCaller(ctx, &auth.UserCaller{
				User: &models.User{
					Email: "user@domain.tld",
				},
			})

			mockCaller := auth.MockCaller{}
			mockCaller.Test(t)

			mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything).Return(test.injectAuthError)

			mockCaller.On("GetSubject").Return("someone@somewhere.invalid")

			mockProjects := db.NewMockProjects(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockProjects.On("GetProjectByID", mock.Anything, input.ID).Return(test.existingProject, nil)

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockProjects.On("UpdateProject", mock.Anything, test.expectUpdated).Return(test.expectUpdated, nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						ProjectID:  &test.existingProject.Metadata.ID,
						Action:     models.ActionUpdate,
						TargetType: models.TargetProject,
						TargetID:   &test.existingProject.Metadata.ID,
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Projects:     mockProjects,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			actualUpdated, err := service.UpdateProject(auth.WithCaller(ctx, &mockCaller), input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.existingProject, actualUpdated)
		})
	}
}

func TestDeleteProject(t *testing.T) {
	projectID := "project-1"
	orgID := "org-1"

	input := &DeleteProjectInput{
		ID: projectID,
	}

	sampleProject := &models.Project{
		Metadata: models.ResourceMetadata{
			ID: projectID,
		},
		OrgID: orgID,
	}

	type testCase struct {
		existingProject *models.Project
		name            string
		injectAuthError error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name:            "successfully delete a project",
			existingProject: sampleProject,
		},
		{
			name:            "project does not exist",
			expectErrorCode: errors.ENotFound,
		},
		{
			name:            "user is not an org owner",
			existingProject: sampleProject,
			injectAuthError: errors.New("injected error, user is not an org owner", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			ctx = auth.WithCaller(ctx, &auth.UserCaller{
				User: &models.User{
					Email: "user@domain.tld",
				},
			})

			mockCaller := auth.MockCaller{}
			mockCaller.Test(t)

			mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything).Return(test.injectAuthError)

			mockCaller.On("GetSubject").Return("someone@somewhere.invalid")

			mockProjects := db.NewMockProjects(t)
			mockTransactions := db.NewMockTransactions(t)
			mockActivityEvents := activityevent.NewMockService(t)

			mockProjects.On("GetProjectByID", mock.Anything, input.ID).Return(test.existingProject, nil)

			if test.expectErrorCode == "" {
				mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil)
				mockTransactions.On("RollbackTx", mock.Anything).Return(nil)
				mockTransactions.On("CommitTx", mock.Anything).Return(nil)

				mockProjects.On("DeleteProject", mock.Anything, test.existingProject).Return(nil)

				mockActivityEvents.On("CreateActivityEvent", mock.Anything,
					&activityevent.CreateActivityEventInput{
						OrganizationID: &test.existingProject.OrgID,
						Action:         models.ActionDelete,
						TargetType:     models.TargetOrganization,
						TargetID:       &test.existingProject.OrgID,
						Payload: &models.ActivityEventDeleteResourcePayload{
							Name: &test.existingProject.Name,
							ID:   test.existingProject.Metadata.ID,
							Type: models.TargetProject.String(),
						},
					},
				).Return(&models.ActivityEvent{}, nil)
			}

			dbClient := &db.Client{
				Projects:     mockProjects,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				activityService: mockActivityEvents,
			}

			err := service.DeleteProject(auth.WithCaller(ctx, &mockCaller), input)
			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}
