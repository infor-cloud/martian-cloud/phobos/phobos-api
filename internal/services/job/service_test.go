package job

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/jws"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
)

func TestNewService(t *testing.T) {
	logger, _ := logger.NewForTest()
	dbClient := &db.Client{}
	identityProvider := auth.NewIdentityProvider(jws.NewMockProvider(t), "http://phobos.domain")
	logStreamManager := logstream.NewMockManager(t)
	eventManager := events.NewEventManager(dbClient, logger)
	pipelineUpdater := pipeline.NewMockUpdater(t)

	expect := &service{
		logger:           logger,
		dbClient:         dbClient,
		idp:              identityProvider,
		eventManager:     eventManager,
		logStreamManager: logStreamManager,
		pipelineUpdater:  pipelineUpdater,
	}

	assert.Equal(t, expect, NewService(logger, dbClient, identityProvider, logStreamManager, eventManager, pipelineUpdater))
}

func TestSetJobStatus(t *testing.T) {
	jobID := "job-1"
	projectID := "project-1"

	jobTaskData := &models.JobTaskData{
		PipelineID: "pipeline-1",
		TaskPath:   "pipeline.stage.one.task.one",
	}

	type testCase struct {
		authError       error
		existingJob     *models.Job
		expectJob       *models.Job
		name            string
		expectErrorCode errors.CodeType
		oldStatus       models.JobStatus
		status          models.JobStatus
	}

	testCases := []testCase{
		{
			name: "caller authorization failed",
			existingJob: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name:            "job not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "set status to existing status",
			existingJob: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
			},
			oldStatus: models.JobRunning,
			status:    models.JobRunning,
			expectJob: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
			},
		},
		{
			name: "update status of pending job to running",
			existingJob: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
				Data:      jobTaskData,
			},
			oldStatus: models.JobPending,
			status:    models.JobRunning,
			expectJob: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
				Data:      jobTaskData,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockTransactions := db.NewMockTransactions(t)
			mockCaller := auth.NewMockCaller(t)
			mockPipelineUpdater := pipeline.NewMockUpdater(t)

			mockTransactions.On("BeginTx", mock.Anything).Return(nil, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()
			mockTransactions.On("CommitTx", mock.Anything).Return(nil).Maybe()

			mockPipelineUpdater.On("ProcessEvent", mock.Anything, jobTaskData.PipelineID, &pipeline.JobStatusChangeEvent{
				Job: test.expectJob,
			}).Return(nil, nil).Maybe()

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateJobState, mock.Anything, mock.Anything).Return(test.authError).Maybe()

			mockJobs.On("GetJobByID", mock.Anything, jobID).Return(test.existingJob, nil)

			if test.expectJob != nil {
				matcher := mock.MatchedBy(func(job *models.Job) bool {
					return job.SetStatus(test.status) == nil
				})
				mockJobs.On("UpdateJob", mock.Anything, matcher).Return(test.expectJob, nil).Maybe()
			}

			dbClient := &db.Client{
				Jobs:         mockJobs,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				pipelineUpdater: mockPipelineUpdater,
			}

			response, err := service.SetJobStatus(auth.WithCaller(ctx, mockCaller), jobID, test.status)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectJob.GetStatus(), response.GetStatus())
		})
	}
}

func TestClaimJob(t *testing.T) {
	jobID := "job-1"
	agentID := "agent-1"
	projectID := "project-1"
	organizationID := "organization-1"
	token := "job-token"

	sampleQueuedJob := models.Job{
		Metadata: models.ResourceMetadata{
			ID: jobID,
		},
		ProjectID: projectID,
	}

	type testCase struct {
		existingAgent   *models.Agent
		expectResponse  *ClaimJobResponse
		name            string
		authError       error
		expectErrorCode errors.CodeType
	}

	testCases := []testCase{
		{
			name: "successfully claim a job with shared agent",
			existingAgent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type: models.SharedAgent,
				Name: "shared-agent",
			},
			expectResponse: &ClaimJobResponse{
				Job: &models.Job{
					Metadata: models.ResourceMetadata{
						ID: jobID,
					},
					ProjectID: projectID,
				},
				Token: token,
			},
		},
		{
			name: "successfully claim a job with an organization agent",
			existingAgent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type:           models.OrganizationAgent,
				Name:           "org-agent",
				OrganizationID: &organizationID,
			},
			expectResponse: &ClaimJobResponse{
				Job: &models.Job{
					Metadata: models.ResourceMetadata{
						ID: jobID,
					},
					ProjectID: projectID,
				},
				Token: token,
			},
		},
		{
			name:            "agent not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject does not have permissions to claim job",
			existingAgent: &models.Agent{
				Metadata: models.ResourceMetadata{
					ID: agentID,
				},
				Type: models.SharedAgent,
				Name: "shared-agent",
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockAgents := db.NewMockAgents(t)
			mockCaller := auth.NewMockCaller(t)
			mockJWSProvider := jws.NewMockProvider(t)

			mockAgents.On("GetAgentByID", mock.Anything, agentID).Return(test.existingAgent, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ClaimJob, mock.Anything).Return(test.authError).Maybe()

			if test.expectErrorCode == "" {
				mockCaller.On("GetSubject").Return("testSubject")

				// Mock jobs
				sortBy := db.JobSortableFieldCreatedAtAsc
				jobsInput := &db.GetJobsInput{
					Sort: &sortBy,
					Filter: &db.JobFilter{
						JobStatuses: []models.JobStatus{models.JobQueued},
						TagFilter:   &db.JobTagFilter{TagSuperset: []string{}, ExcludeUntaggedJobs: ptr.Bool(true)},
						OrgID:       test.existingAgent.OrganizationID,
					},
				}
				mockJobs.On("GetJobs", mock.Anything, jobsInput).Return(&db.JobsResult{Jobs: []models.Job{sampleQueuedJob}}, nil)

				mockJobs.On("GetJobCountForAgent", mock.Anything, agentID).Return(1, nil)

				mockJobs.On("UpdateJob", mock.Anything, mock.Anything).Return(&sampleQueuedJob, nil)

				mockJWSProvider.On("Sign", mock.Anything, mock.Anything).Return([]byte(token), nil)
			}

			dbClient := &db.Client{
				Jobs:   mockJobs,
				Agents: mockAgents,
			}

			identityProvider := auth.NewIdentityProvider(mockJWSProvider, "http://phobos.domain")

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:     dbClient,
				logger:       logger,
				idp:          identityProvider,
				eventManager: events.NewEventManager(dbClient, logger),
			}

			actualResponse, err := service.ClaimJob(auth.WithCaller(ctx, mockCaller), agentID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResponse, actualResponse)
		})
	}
}

func TestGetNextAvailableJob(t *testing.T) {
	agentID := "agent-1"
	organizationID := "org-1"
	jobID := "job-1"

	type testCase struct {
		jobFilter            *db.JobFilter
		expectJob            *models.Job
		agent                *models.Agent
		name                 string
		queuedJobs           []models.Job
		currentAgentJobCount int
	}

	testCases := []testCase{
		{
			name:  "shared agent should get untagged jobs",
			agent: &models.Agent{Metadata: models.ResourceMetadata{ID: agentID}, Type: models.SharedAgent, RunUntaggedJobs: true},
			jobFilter: &db.JobFilter{
				JobStatuses: []models.JobStatus{models.JobQueued},
				TagFilter:   &db.JobTagFilter{TagSuperset: []string{}},
			},
			queuedJobs: []models.Job{
				{Metadata: models.ResourceMetadata{ID: jobID}},
			},
			expectJob: &models.Job{Metadata: models.ResourceMetadata{ID: jobID}},
		},
		{
			name:  "org agent should get untagged jobs and tagged jobs",
			agent: &models.Agent{Metadata: models.ResourceMetadata{ID: agentID}, Type: models.OrganizationAgent, OrganizationID: &organizationID, RunUntaggedJobs: true, Tags: []string{"golang", "phobos"}},
			jobFilter: &db.JobFilter{
				JobStatuses: []models.JobStatus{models.JobQueued},
				OrgID:       &organizationID,
				TagFilter:   &db.JobTagFilter{TagSuperset: []string{"golang", "phobos"}},
			},
			queuedJobs: []models.Job{
				{Metadata: models.ResourceMetadata{ID: jobID}},
			},
			expectJob: &models.Job{Metadata: models.ResourceMetadata{ID: jobID}},
		},
		{
			name:  "org agent should get only tagged jobs",
			agent: &models.Agent{Metadata: models.ResourceMetadata{ID: agentID}, Type: models.OrganizationAgent, OrganizationID: &organizationID, RunUntaggedJobs: false, Tags: []string{"golang", "phobos"}},
			jobFilter: &db.JobFilter{
				JobStatuses: []models.JobStatus{models.JobQueued},
				OrgID:       &organizationID,
				TagFilter:   &db.JobTagFilter{TagSuperset: []string{"golang", "phobos"}, ExcludeUntaggedJobs: ptr.Bool(true)},
			},
			queuedJobs: []models.Job{
				{Metadata: models.ResourceMetadata{ID: jobID}},
			},
			expectJob: &models.Job{Metadata: models.ResourceMetadata{ID: jobID}},
		},
		{
			name:  "shared agent should not get queued job since agent has reached max job count",
			agent: &models.Agent{Metadata: models.ResourceMetadata{ID: agentID}, Type: models.SharedAgent, RunUntaggedJobs: true},
			jobFilter: &db.JobFilter{
				JobStatuses: []models.JobStatus{models.JobQueued},
				TagFilter:   &db.JobTagFilter{TagSuperset: []string{}},
			},
			queuedJobs: []models.Job{
				{Metadata: models.ResourceMetadata{ID: jobID}},
			},
			currentAgentJobCount: 100,
		},
		{
			name:  "shared agent should not get queued job since agent is disabled",
			agent: &models.Agent{Metadata: models.ResourceMetadata{ID: agentID}, Type: models.SharedAgent, Disabled: true},
			queuedJobs: []models.Job{
				{Metadata: models.ResourceMetadata{ID: jobID}},
			},
		},
		{
			name: "no job exists for agent",
			agent: &models.Agent{
				Metadata:        models.ResourceMetadata{ID: agentID},
				Type:            models.OrganizationAgent,
				OrganizationID:  ptr.String("org-2"),
				RunUntaggedJobs: true,
			},
			jobFilter: &db.JobFilter{
				JobStatuses: []models.JobStatus{models.JobQueued},
				OrgID:       ptr.String("org-2"),
				TagFilter:   &db.JobTagFilter{TagSuperset: []string{}},
			},
			queuedJobs: []models.Job{},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockAgents := db.NewMockAgents(t)

			mockAgents.On("GetAgentByID", mock.Anything, test.agent.Metadata.ID).Return(test.agent, nil)

			if test.agent != nil && !test.agent.Disabled {
				sortBy := db.JobSortableFieldCreatedAtAsc
				mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{
					Sort:   &sortBy,
					Filter: test.jobFilter,
				}).Return(&db.JobsResult{Jobs: test.queuedJobs}, nil)
			}

			mockJobs.On("GetJobCountForAgent", mock.Anything, agentID).Return(test.currentAgentJobCount, nil).Maybe()

			dbClient := &db.Client{
				Jobs:   mockJobs,
				Agents: mockAgents,
			}

			service := &service{dbClient: dbClient}

			actualJob, err := service.getNextAvailableJob(ctx, test.agent.Metadata.ID)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectJob, actualJob)
		})
	}
}

func TestGetJobByPRN(t *testing.T) {
	jobPRN := "job-1"

	goodJob := &models.Job{
		Metadata: models.ResourceMetadata{
			PRN: jobPRN,
		},
	}

	type testCase struct {
		injectGetError        error
		injectPermissionError error
		findJob               *models.Job
		expectJob             *models.Job
		name                  string
		expectErrorCode       errors.CodeType
		withCaller            bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "get failed for unspecified reason",
			withCaller:      true,
			injectGetError:  errors.New("injected error, get failed for unspecified reason"),
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "job not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			findJob:               goodJob,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:       "successfully return a job",
			withCaller: true,
			findJob:    goodJob,
			expectJob:  goodJob,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockJobs.On("GetJobByPRN", mock.Anything, jobPRN).
					Return(test.findJob, test.injectGetError)

				if (test.injectGetError == nil) && (test.findJob != nil) {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				Jobs: mockJobs,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualJob, err := service.GetJobByPRN(ctx, jobPRN)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectJob, actualJob)
		})
	}
}

func TestGetJobByID(t *testing.T) {
	jobID := "job-1"

	goodJob := &models.Job{
		Metadata: models.ResourceMetadata{
			ID: jobID,
		},
	}

	type testCase struct {
		injectGetError        error
		injectPermissionError error
		findJob               *models.Job
		expectJob             *models.Job
		name                  string
		expectErrorCode       errors.CodeType
		withCaller            bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "get failed for unspecified reason",
			withCaller:      true,
			injectGetError:  errors.New("injected error, get failed for unspecified reason"),
			expectErrorCode: errors.EInternal,
		},
		{
			name:            "job not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			findJob:               goodJob,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:       "successfully return a job",
			withCaller: true,
			findJob:    goodJob,
			expectJob:  goodJob,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockJobs.On("GetJobByID", mock.Anything, jobID).
					Return(test.findJob, test.injectGetError)

				if (test.injectGetError == nil) && (test.findJob != nil) {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				Jobs: mockJobs,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualJob, err := service.GetJobByID(ctx, jobID)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectJob, actualJob)
		})
	}
}

func TestGetJobsByIDs(t *testing.T) {
	jobIDs := []string{"job-1", "job-2"}

	goodJobs := []models.Job{
		{
			Metadata: models.ResourceMetadata{
				ID: jobIDs[0],
			},
		},
		{
			Metadata: models.ResourceMetadata{
				ID: jobIDs[1],
			},
		},
	}
	emptyJobsResult := &db.JobsResult{Jobs: []models.Job{}}
	goodJobsResult := &db.JobsResult{Jobs: goodJobs}

	type testCase struct {
		injectPermissionError error
		injectJobs            *db.JobsResult
		name                  string
		expectErrorCode       errors.CodeType
		expectJobs            []models.Job
		withCaller            bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "no jobs found",
			withCaller: true,
			injectJobs: emptyJobsResult,
			expectJobs: []models.Job{},
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			injectJobs:            goodJobsResult,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:       "successfully return a job",
			withCaller: true,
			injectJobs: goodJobsResult,
			expectJobs: goodJobs,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{Filter: &db.JobFilter{JobIDs: jobIDs}}).
					Return(test.injectJobs, nil)

				if (test.injectJobs != nil) && (len(test.injectJobs.Jobs) > 0) {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				Jobs: mockJobs,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualJobs, err := service.GetJobsByIDs(ctx, jobIDs)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectJobs, actualJobs)
		})
	}
}

func TestGetJobs(t *testing.T) {
	jobID := "job-1"
	sort := db.JobSortableFieldCreatedAtAsc

	input := &GetJobsInput{
		Sort: &sort,
		PaginationOptions: &pagination.Options{
			First: ptr.Int32(1),
		},
		ProjectID:  ptr.String("proj-id-1"),
		PipelineID: ptr.String("pipeline-id-1"),
	}

	goodJob := models.Job{
		Metadata: models.ResourceMetadata{
			ID: jobID,
		},
	}

	type testCase struct {
		expectResult    *db.JobsResult
		name            string
		expectErrorCode errors.CodeType
		withCaller      bool
		keepPipelineID  bool // There is no keepProjectID field, because ProjectID is required.
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:       "jobs not found, possibly due to lack of permissions",
			withCaller: true,
		},
		{
			name:       "successfully return jobs result, by project, positive",
			withCaller: true,
			expectResult: &db.JobsResult{
				Jobs: []models.Job{goodJob},
			},
		},
		{
			name:           "successfully return jobs result, by pipeline, positive",
			withCaller:     true,
			keepPipelineID: true,
			expectResult: &db.JobsResult{
				Jobs: []models.Job{goodJob},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)

			dbInput := &db.GetJobsInput{
				// No need to set Sort and PaginationOptions for this test.
				Filter: &db.JobFilter{
					// Will be filled in later.
				},
			}

			// ProjectID is required.
			dbInput.Filter.ProjectID = input.ProjectID

			if test.keepPipelineID {
				dbInput.Filter.PipelineID = input.PipelineID
			}

			if test.withCaller {
				mockJobs.On("GetJobs", mock.Anything, mock.Anything).Return(test.expectResult, nil)

				testCaller := auth.NewUserCaller(
					&models.User{
						Metadata: models.ResourceMetadata{
							ID: "test-user-caller-ID",
						},
						Admin: true,
					},
					nil,
					nil,
				)
				ctx = auth.WithCaller(ctx, testCaller)
			}

			dbClient := &db.Client{
				Jobs: mockJobs,
			}

			service := &service{
				dbClient: dbClient,
			}

			actualResult, err := service.GetJobs(ctx, input)

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectResult, actualResult)
		})
	}
}

func TestSubscribeToCancellationEvent(t *testing.T) {
	jobID := "job-1"

	goodJob := &models.Job{
		Metadata: models.ResourceMetadata{
			ID: jobID,
		},
	}

	type testCase struct {
		injectPermissionError error
		injectJob             *models.Job
		name                  string
		expectErrorCode       errors.CodeType
		withCaller            bool
		expectChan            bool
	}

	testCases := []testCase{
		{
			name:            "caller authorization failed",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:            "job not found",
			withCaller:      true,
			expectErrorCode: errors.ENotFound,
		},
		{
			name:                  "permission check failed",
			withCaller:            true,
			injectJob:             goodJob,
			injectPermissionError: errors.New("injected error, permission check failed", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:       "successfully return a job",
			withCaller: true,
			injectJob:  goodJob,
			expectChan: true,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockJobs.On("GetJobByID", mock.Anything, jobID).
					Return(test.injectJob, nil)

				if test.injectJob != nil {
					mockCaller.On("RequirePermission", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
						Return(test.injectPermissionError)
				}
			}

			dbClient := &db.Client{
				Jobs: mockJobs,
			}

			logger, _ := logger.NewForTest()
			service := &service{
				dbClient:     dbClient,
				logger:       logger,
				eventManager: events.NewEventManager(dbClient, logger),
			}

			actualChan, err := service.SubscribeToCancellationEvent(ctx, &CancellationSubscriptionsOptions{JobID: jobID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expectChan, actualChan != nil)
		})
	}
}

func TestWriteLogs(t *testing.T) {
	jobID := "job1"
	streamID := "stream1"

	// Test cases
	tests := []struct {
		authError   error
		name        string
		logs        string
		startOffset int
	}{
		{
			name:        "write logs with 0 offset",
			startOffset: 0,
			logs:        "this is a test log",
		},
		{
			name:        "write logs with offset greater than 0",
			startOffset: 100,
			logs:        "this is a test log",
		},
		{
			name:        "write an empty log",
			startOffset: 0,
			logs:        "",
		},
		{
			name:      "auth error",
			authError: errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)
			mockLogStreamManager := logstream.NewMockManager(t)
			mockLogStreams := db.NewMockLogStreams(t)

			mockJobs.On("GetJobByID", mock.Anything, mock.Anything).Return(&models.Job{}, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.UpdateJobState, mock.Anything, mock.Anything).Return(test.authError)

			if test.authError == nil {
				mockLogStreams.On("GetLogStreamByJobID", mock.Anything, jobID).Return(&models.LogStream{
					Metadata: models.ResourceMetadata{
						ID: streamID,
					},
				}, nil)

				mockLogStreamManager.On("WriteLogs", mock.Anything, streamID, test.startOffset, []byte(test.logs)).Return(&models.LogStream{
					Metadata: models.ResourceMetadata{
						ID: streamID,
					},
					Size: test.startOffset + len(test.logs),
				}, nil)
			}

			dbClient := &db.Client{
				Jobs:       mockJobs,
				LogStreams: mockLogStreams,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:         dbClient,
				logger:           logger,
				logStreamManager: mockLogStreamManager,
			}

			size, err := service.WriteLogs(auth.WithCaller(ctx, mockCaller), jobID, test.startOffset, []byte(test.logs))
			if test.authError != nil {
				assert.Equal(t, errors.ErrorCode(test.authError), errors.ErrorCode(err))
			} else if err != nil {
				t.Fatal(err)
			} else {
				assert.Equal(t, test.startOffset+len(test.logs), size)
			}
		})
	}
}

func TestReadLogs(t *testing.T) {
	jobID := "job1"
	streamID := "stream1"

	// Test cases
	tests := []struct {
		authError   error
		name        string
		startOffset int
		limit       int
	}{
		{
			name:        "read logs with 0 offset",
			startOffset: 0,
			limit:       10,
		},
		{
			name:        "read logs with offset greater than 0",
			startOffset: 5,
			limit:       10,
		},
		{
			name:        "read with limit set to 0",
			startOffset: 0,
			limit:       0,
		},
		{
			name:      "auth error",
			authError: errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)
			mockLogStreamManager := logstream.NewMockManager(t)
			mockLogStreams := db.NewMockLogStreams(t)

			mockJobs.On("GetJobByID", mock.Anything, mock.Anything).Return(&models.Job{}, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewJob, mock.Anything, mock.Anything).Return(test.authError)

			if test.authError == nil {
				mockLogStreams.On("GetLogStreamByJobID", mock.Anything, jobID).Return(&models.LogStream{
					Metadata: models.ResourceMetadata{
						ID: streamID,
					},
				}, nil)

				mockLogStreamManager.On("ReadLogs", mock.Anything, streamID, test.startOffset, test.limit).Return(make([]byte, test.limit-test.startOffset), nil)
			}

			dbClient := &db.Client{
				Jobs:       mockJobs,
				LogStreams: mockLogStreams,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:         dbClient,
				logger:           logger,
				logStreamManager: mockLogStreamManager,
			}

			logs, err := service.ReadLogs(auth.WithCaller(ctx, mockCaller), jobID, test.startOffset, test.limit)
			if test.authError != nil {
				assert.Equal(t, errors.ErrorCode(test.authError), errors.ErrorCode(err))
			} else if err != nil {
				t.Fatal(err)
			} else {
				assert.Equal(t, test.limit-test.startOffset, len(logs))
			}
		})
	}
}

func TestSubscribeToLogStreamEvents(t *testing.T) {
	jobID := "job1"
	streamID := "stream1"

	// Test cases
	tests := []struct {
		name         string
		authError    error
		lastSeenSize *int
	}{
		{
			name:      "auth error",
			authError: errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)
			mockStore := logstream.NewMockManager(t)
			mockLogStreams := db.NewMockLogStreams(t)

			mockJobs.On("GetJobByID", mock.Anything, mock.Anything).
				Return(&models.Job{Metadata: models.ResourceMetadata{ID: jobID}}, nil)

			mockCaller.On("RequirePermission", mock.Anything, models.ViewJob, mock.Anything, mock.Anything).Return(test.authError)

			if test.authError == nil {
				mockLogStreams.On("GetLogStreamByJobID", mock.Anything, jobID).Return(&models.LogStream{
					Metadata: models.ResourceMetadata{
						ID: streamID,
					},
					Size: 2,
				}, nil)

				eventChannel := make(chan *LogEvent)
				mockStore.On("Subscribe", mock.Anything, &logstream.SubscriptionOptions{
					LastSeenLogSize: test.lastSeenSize,
					LogStreamID:     streamID,
				}).Return(eventChannel, nil)
			}

			dbClient := &db.Client{
				Jobs:       mockJobs,
				LogStreams: mockLogStreams,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:         dbClient,
				logger:           logger,
				logStreamManager: mockStore,
			}

			_, err := service.SubscribeToLogStreamEvents(auth.WithCaller(ctx, mockCaller), &LogStreamEventSubscriptionOptions{
				JobID:           jobID,
				LastSeenLogSize: test.lastSeenSize,
			})
			if test.authError != nil {
				assert.Equal(t, errors.ErrorCode(test.authError), errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}
		})
	}
}

func TestGetLogStreamByJobIDs(t *testing.T) {
	jobID := "job1"
	streamID := "stream1"

	goodJob := &models.Job{
		Metadata: models.ResourceMetadata{
			ID: jobID,
		},
		ProjectID: "project-1",
	}
	goodStream := &models.LogStream{
		Metadata: models.ResourceMetadata{
			ID: streamID,
		},
		JobID:     &jobID,
		Size:      42,
		Completed: false,
	}

	// Test cases
	tests := []struct {
		injectGetJobError         error
		injectPermissionError     error
		injectGetJobsResult       *db.JobsResult
		injectGetLogStreamsResult *db.LogStreamsResult
		name                      string
		expectErrorCode           errors.CodeType
		withCaller                bool
	}{
		{
			name:            "auth error",
			withCaller:      false,
			expectErrorCode: errors.EUnauthorized,
		},
		{
			name:              "get job failed for unspecified reason",
			withCaller:        true,
			injectGetJobError: errors.New("injected error, get job failed for unspecified reason"),
			expectErrorCode:   errors.EInternal,
		},
		{
			name:       "job not found",
			withCaller: true,
			injectGetJobsResult: &db.JobsResult{
				Jobs: []models.Job{},
			},
			injectGetLogStreamsResult: &db.LogStreamsResult{
				LogStreams: []models.LogStream{},
			},
		},
		{
			name:                  "permission denied",
			withCaller:            true,
			injectGetJobsResult:   &db.JobsResult{Jobs: []models.Job{*goodJob}},
			injectPermissionError: errors.New("forbidden", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode:       errors.EForbidden,
		},
		{
			name:                "positive",
			withCaller:          true,
			injectGetJobsResult: &db.JobsResult{Jobs: []models.Job{*goodJob}},
			injectGetLogStreamsResult: &db.LogStreamsResult{
				LogStreams: []models.LogStream{*goodStream},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()

			mockJobs := db.NewMockJobs(t)
			mockCaller := auth.NewMockCaller(t)
			mockLogStreams := db.NewMockLogStreams(t)

			if test.withCaller {
				ctx = auth.WithCaller(ctx, mockCaller)

				mockJobs.On("GetJobs", mock.Anything, &db.GetJobsInput{
					Filter: &db.JobFilter{
						JobIDs: []string{jobID},
					},
				}).
					Return(test.injectGetJobsResult, test.injectGetJobError)

				mockCaller.On("RequirePermission", mock.Anything, models.ViewJob, mock.Anything, mock.Anything).
					Return(test.injectPermissionError).Maybe()

				mockLogStreams.On("GetLogStreams", mock.Anything, &db.GetLogStreamsInput{
					Filter: &db.LogStreamFilter{
						JobIDs: []string{jobID},
					},
				}).
					Return(test.injectGetLogStreamsResult, nil).Maybe()
			}

			dbClient := &db.Client{
				Jobs:       mockJobs,
				LogStreams: mockLogStreams,
			}
			logger, _ := logger.NewForTest()
			service := &service{
				dbClient: dbClient,
				logger:   logger,
			}

			actualLogStreams, err := service.GetLogStreamsByJobIDs(ctx, []string{jobID})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.injectGetLogStreamsResult.LogStreams, actualLogStreams)
		})
	}
}

func TestSubscribeToJobs(t *testing.T) {
	// Test cases
	tests := []struct {
		authError      error
		input          *SubscribeToJobsInput
		name           string
		expectErrCode  errors.CodeType
		agent          *models.Agent
		sendEventData  []*db.JobEventData
		expectedEvents []Event
		isAdmin        bool
	}{
		{
			name: "subscribe to job events for a project",
			input: &SubscribeToJobsInput{
				ProjectID: ptr.String("project1"),
			},
			sendEventData: []*db.JobEventData{
				{
					ID:        "job1",
					ProjectID: "project1",
				},
				{
					ID:        "job2",
					ProjectID: "project2",
				},
			},
			expectedEvents: []Event{
				{
					Job: &models.Job{
						Metadata: models.ResourceMetadata{
							ID: "job1",
						},
						ProjectID: "project1",
					},
					Action: "UPDATE",
				},
			},
		},
		{
			name: "not authorized to subscribe to job events for a project",
			input: &SubscribeToJobsInput{
				ProjectID: ptr.String("project1"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subscribe to job events for an organization agent",
			input: &SubscribeToJobsInput{
				AgentID: ptr.String("agent1"),
			},
			sendEventData: []*db.JobEventData{
				{
					ID:      "job1",
					AgentID: ptr.String("agent1"),
				},
				{
					ID:      "job2",
					AgentID: ptr.String("agent2"),
				},
			},
			expectedEvents: []Event{
				{
					Job: &models.Job{
						Metadata: models.ResourceMetadata{
							ID: "job1",
						},
						AgentID: ptr.String("agent1"),
					},
					Action: "UPDATE",
				},
			},
			agent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: "agent1"},
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("org1"),
			},
		},
		{
			name: "not authorized to subscribe to job events for an organization agent",
			input: &SubscribeToJobsInput{
				AgentID: ptr.String("agent1"),
			},
			agent: &models.Agent{
				Metadata:       models.ResourceMetadata{ID: "agent1"},
				Type:           models.OrganizationAgent,
				OrganizationID: ptr.String("org1"),
			},
			authError:     errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrCode: errors.EForbidden,
		},
		{
			name: "subscribe to job events for a shared agent",
			input: &SubscribeToJobsInput{
				AgentID: ptr.String("agent1"),
			},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{ID: "agent1"},
				Type:     models.SharedAgent,
			},
			isAdmin: true,
		},
		{
			name: "not authorized to subscribe to job events for a shared agent",
			input: &SubscribeToJobsInput{
				AgentID: ptr.String("agent1"),
			},
			agent: &models.Agent{
				Metadata: models.ResourceMetadata{ID: "agent1"},
				Type:     models.SharedAgent,
			},
			expectErrCode: errors.EForbidden,
		},
		{
			name:    "subscribe to all job events",
			input:   &SubscribeToJobsInput{},
			isAdmin: true,
			sendEventData: []*db.JobEventData{
				{
					ID:      "job1",
					AgentID: ptr.String("agent1"),
				},
				{
					ID:      "job2",
					AgentID: ptr.String("agent2"),
				},
			},
			expectedEvents: []Event{
				{
					Job: &models.Job{
						Metadata: models.ResourceMetadata{
							ID: "job1",
						},
						AgentID: ptr.String("agent1"),
					},
					Action: "UPDATE",
				},
				{
					Job: &models.Job{
						Metadata: models.ResourceMetadata{
							ID: "job2",
						},
						AgentID: ptr.String("agent2"),
					},
					Action: "UPDATE",
				},
			},
		},
		{
			name:          "not authorized to subscribe to all job events",
			input:         &SubscribeToJobsInput{},
			expectErrCode: errors.EForbidden,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockAgents := db.NewMockAgents(t)
			mockJobs := db.NewMockJobs(t)
			mockEvents := db.NewMockEvents(t)

			mockEventChannel := make(chan db.Event, 1)
			var roEventChan <-chan db.Event = mockEventChannel
			mockEvents.On("Listen", mock.Anything).Return(roEventChan, make(<-chan error)).Maybe()

			if test.input.AgentID != nil {
				mockAgents.On("GetAgentByID", mock.Anything, *test.input.AgentID).Return(test.agent, nil)
			}

			if test.input.ProjectID != nil {
				mockCaller.On("RequirePermission", mock.Anything, models.ViewProject, mock.Anything).Return(test.authError)
			} else if test.input.AgentID != nil {
				mockCaller.On("RequireAccessToInheritableResource", mock.Anything, models.AgentResource, mock.Anything, mock.Anything).
					Return(test.authError).Maybe()
				mockCaller.On("IsAdmin").Return(test.isAdmin).Maybe()
			} else {
				mockCaller.On("IsAdmin").Return(test.isAdmin)
			}

			for _, d := range test.sendEventData {
				dCopy := d
				mockJobs.On("GetJobByID", mock.Anything, dCopy.ID).
					Return(func(_ context.Context, _ string) (*models.Job, error) {
						result := &models.Job{
							Metadata: models.ResourceMetadata{
								ID: dCopy.ID,
							},
							ProjectID: dCopy.ProjectID,
						}

						if dCopy.AgentID != nil {
							result.AgentID = dCopy.AgentID
						}

						return result, nil
					}).Maybe()
			}

			dbClient := db.Client{
				Agents: mockAgents,
				Jobs:   mockJobs,
				Events: mockEvents,
			}

			logger, _ := logger.NewForTest()
			eventManager := events.NewEventManager(&dbClient, logger)
			eventManager.Start(ctx)

			service := &service{
				dbClient:     &dbClient,
				eventManager: eventManager,
				logger:       logger,
			}

			eventChannel, err := service.SubscribeToJobs(auth.WithCaller(ctx, mockCaller), test.input)

			if test.expectErrCode != "" {
				assert.Equal(t, test.expectErrCode, errors.ErrorCode(err))
				return
			}

			if err != nil {
				t.Fatal(err)
			}

			receivedEvents := []*Event{}

			go func() {
				for _, d := range test.sendEventData {
					encoded, err := json.Marshal(d)
					require.Nil(t, err)

					mockEventChannel <- db.Event{
						Table:  "jobs",
						Action: string(events.UpdateAction),
						ID:     d.ID,
						Data:   encoded,
					}
				}
			}()

			if len(test.expectedEvents) > 0 {
				for e := range eventChannel {
					eCopy := e
					receivedEvents = append(receivedEvents, eCopy)

					if len(receivedEvents) == len(test.expectedEvents) {
						break
					}
				}
			}

			require.Equal(t, len(test.expectedEvents), len(receivedEvents))
			for i, e := range test.expectedEvents {
				assert.Equal(t, e, *receivedEvents[i])
			}
		})
	}
}

func TestCancelJob(t *testing.T) {
	jobID := "job1"
	jobVersion := 1
	projectID := "project1"
	pipelineID := "pipeline1"
	testSubject := "test-subject"

	sampleTaskData := &models.JobTaskData{
		PipelineID: pipelineID,
		TaskPath:   "task1",
	}

	type testCase struct {
		authError       error
		job             *models.Job
		jobStatus       models.JobStatus
		name            string
		expectErrorCode errors.CodeType
		isForceCancel   bool
	}

	tests := []testCase{
		{
			name: "gracefully cancel a job",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
				Data:      sampleTaskData,
				Type:      models.JobTaskType,
			},
			jobStatus: models.JobRunning,
		},
		{
			name: "forcefully cancel a job",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID:                projectID,
				CancelRequestedTimestamp: ptr.Time(time.Now().UTC().Add(-5 * time.Hour)),
				Data:                     sampleTaskData,
				Type:                     models.JobTaskType,
			},
			jobStatus:     models.JobCanceling,
			isForceCancel: true,
		},
		{
			name:            "job not found",
			expectErrorCode: errors.ENotFound,
		},
		{
			name: "subject is not authorized to cancel job",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
			},
			authError:       errors.New("Unauthorized", errors.WithErrorCode(errors.EForbidden)),
			expectErrorCode: errors.EForbidden,
		},
		{
			name: "job is not in a cancelable state",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
				Data:      sampleTaskData,
				Type:      models.JobTaskType,
			},
			jobStatus:       models.JobSucceeded,
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "cannot attempt a force cancel before a graceful cancel",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
				Data:      sampleTaskData,
				Type:      models.JobTaskType,
			},
			jobStatus:       models.JobRunning,
			isForceCancel:   true,
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "force cancellation is not yet available",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID:                projectID,
				CancelRequestedTimestamp: ptr.Time(time.Now().UTC()),
				Data:                     sampleTaskData,
				Type:                     models.JobTaskType,
			},
			jobStatus:       models.JobCanceling,
			isForceCancel:   true,
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "job has invalid task data",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID:                projectID,
				CancelRequestedTimestamp: ptr.Time(time.Now().UTC().Add(-5 * time.Hour)),
				Data:                     "invalid",
				Type:                     models.JobTaskType,
			},
			jobStatus:       models.JobRunning,
			isForceCancel:   true,
			expectErrorCode: errors.EInternal,
		},
		{
			name: "job type not supported",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID: projectID,
				Type:      models.JobType("invalid"),
			},
			jobStatus:       models.JobRunning,
			expectErrorCode: errors.EInvalid,
		},
		{
			name: "graceful cancellation is already in progress",
			job: &models.Job{
				Metadata: models.ResourceMetadata{
					ID: jobID,
				},
				ProjectID:                projectID,
				CancelRequestedTimestamp: ptr.Time(time.Now().UTC()),
				Data:                     sampleTaskData,
				Type:                     models.JobTaskType,
			},
			jobStatus:       models.JobCanceling,
			expectErrorCode: errors.EInvalid,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockCaller := auth.NewMockCaller(t)
			mockJobs := db.NewMockJobs(t)
			mockTransactions := db.NewMockTransactions(t)
			mockUpdater := pipeline.NewMockUpdater(t)

			mockCaller.On("GetSubject").Return(testSubject).Maybe()

			mockJobs.On("GetJobByID", mock.Anything, jobID).Return(test.job, nil)

			if test.job != nil {
				require.NoError(t, test.job.SetStatus(test.jobStatus))

				mockCaller.On("RequirePermission", mock.Anything, models.CancelPipeline, mock.Anything, mock.Anything).Return(test.authError)
			}

			mockTransactions.On("BeginTx", mock.Anything).Return(ctx, nil).Maybe()
			mockTransactions.On("RollbackTx", mock.Anything).Return(nil).Maybe()

			if test.expectErrorCode == "" {
				mockUpdater.On("ProcessEvent", mock.Anything, pipelineID, &pipeline.JobStatusChangeEvent{
					Job: test.job,
				}).Return(&models.Pipeline{}, nil)

				mockJobs.On("UpdateJob", mock.Anything, mock.Anything).Return(test.job, nil)

				mockTransactions.On("CommitTx", mock.Anything).Return(nil)
			}

			dbClient := &db.Client{
				Jobs:         mockJobs,
				Transactions: mockTransactions,
			}

			logger, _ := logger.NewForTest()

			service := &service{
				dbClient:        dbClient,
				logger:          logger,
				pipelineUpdater: mockUpdater,
			}

			job, err := service.CancelJob(auth.WithCaller(ctx, mockCaller), &CancelJobInput{
				ID:      jobID,
				Force:   test.isForceCancel,
				Version: &jobVersion,
			})

			if test.expectErrorCode != "" {
				assert.Equal(t, test.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.job, job)
		})
	}
}
