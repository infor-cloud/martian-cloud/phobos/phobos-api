// Package job implements functionality releated to Phobos jobs
package job

import (
	"context"
	goerrors "errors"
	"fmt"
	"slices"
	"time"

	"github.com/aws/smithy-go/ptr"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/auth"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/events"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/gid"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/logstream"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/metric"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/models"
	agnt "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/agent"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/services/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/tracing"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/pagination"
	"go.opentelemetry.io/otel/attribute"
)

const (
	// Number of concurrent jobs a given agent can execute.
	agentJobsLimit int = 100
)

var (
	jobQueuedDuration         = metric.NewHistogram("job_queued_duration", "Amount of time a job was in the queued state.", 1, 5, 10)
	jobPendingDuration        = metric.NewHistogram("job_pending_duration", "Amount of time a job was in the pending state.", 1, 5, 10)
	jobTimeToFirstLogDuration = metric.NewHistogram("job_time_to_first_log_duration", "Amount of time for a job to send its first log after moving to the running state", 1, 5, 10)
)

// GetJobsInput is the input for querying a list of jobs
type GetJobsInput struct {
	// Sort specifies the field to sort on and direction
	Sort *db.JobSortableField
	// PaginationOptions supports cursor based pagination
	PaginationOptions *pagination.Options
	// PipelineID filters the jobs by the specified pipeline.
	PipelineID *string
	// PipelineTaskPath filters the jobs by the specified pipeline task path.
	PipelineTaskPath *string
	// ProjectID filters the jobs by the specified project.
	ProjectID *string
	// AgentID filters the jobs by the specified agent ID
	AgentID *string
	// JobStatuses filters the jobs by their status.
	JobStatuses []models.JobStatus
}

// ClaimJobResponse is returned when a agent claims a Job
type ClaimJobResponse struct {
	Job   *models.Job
	Token string
}

// LogStreamEventSubscriptionOptions includes options for setting up a log event subscription
type LogStreamEventSubscriptionOptions struct {
	LastSeenLogSize *int
	JobID           string
}

// LogEvent represents a log stream event
type LogEvent struct {
	Size      int
	Completed bool
}

// CancellationEvent represents a job cancellation event
type CancellationEvent struct {
	Job *models.Job
}

// CancellationSubscriptionsOptions includes options for setting up a cancellation event subscription
type CancellationSubscriptionsOptions struct {
	JobID string
}

// SubscribeToJobsInput is the input for subscribing to jobs
type SubscribeToJobsInput struct {
	ProjectID  *string
	AgentID    *string
	PipelineID *string
}

// Event is a job event
type Event struct {
	Job    *models.Job
	Action string
}

// CancelJobInput includes options for canceling a job
type CancelJobInput struct {
	Version *int
	ID      string
	Force   bool
}

// Service implements all organization related functionality
type Service interface {
	ClaimJob(ctx context.Context, agentID string) (*ClaimJobResponse, error)
	GetJobByPRN(ctx context.Context, prn string) (*models.Job, error)
	GetJobByID(ctx context.Context, jobID string) (*models.Job, error)
	SetJobStatus(ctx context.Context, jobID string, status models.JobStatus) (*models.Job, error)
	GetJobsByIDs(ctx context.Context, idList []string) ([]models.Job, error)
	GetJobs(ctx context.Context, input *GetJobsInput) (*db.JobsResult, error)
	SubscribeToCancellationEvent(ctx context.Context, options *CancellationSubscriptionsOptions) (<-chan *CancellationEvent, error)
	WriteLogs(ctx context.Context, jobID string, startOffset int, logs []byte) (int, error)
	ReadLogs(ctx context.Context, jobID string, startOffset int, limit int) ([]byte, error)
	SubscribeToLogStreamEvents(ctx context.Context, options *LogStreamEventSubscriptionOptions) (<-chan *logstream.LogEvent, error)
	GetLogStreamsByJobIDs(ctx context.Context, idList []string) ([]models.LogStream, error)
	SubscribeToJobs(ctx context.Context, options *SubscribeToJobsInput) (<-chan *Event, error)
	CancelJob(ctx context.Context, input *CancelJobInput) (*models.Job, error)
}

type service struct {
	logger           logger.Logger
	dbClient         *db.Client
	idp              *auth.IdentityProvider
	logStreamManager logstream.Manager
	eventManager     *events.EventManager
	pipelineUpdater  pipeline.Updater
}

// NewService returns an instance of Service
func NewService(
	logger logger.Logger,
	dbClient *db.Client,
	idp *auth.IdentityProvider,
	logStreamManager logstream.Manager,
	eventManager *events.EventManager,
	pipelineUpdater pipeline.Updater,
) Service {
	return &service{
		logger:           logger,
		dbClient:         dbClient,
		idp:              idp,
		logStreamManager: logStreamManager,
		eventManager:     eventManager,
		pipelineUpdater:  pipelineUpdater,
	}
}

func (s *service) GetLogStreamsByJobIDs(ctx context.Context, idList []string) ([]models.LogStream, error) {
	ctx, span := tracer.Start(ctx, "svc.GetLogStreamsByJobIDs")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	resp, err := s.dbClient.Jobs.GetJobs(ctx, &db.GetJobsInput{Filter: &db.JobFilter{JobIDs: idList}})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get jobs", errors.WithSpan(span))
	}

	// Verify user has access to all returned jobs
	for _, job := range resp.Jobs {
		err = caller.RequirePermission(ctx, models.ViewJob, auth.WithJobID(job.Metadata.ID), auth.WithProjectID(job.ProjectID))
		if err != nil {
			return nil, err
		}
	}

	if len(resp.Jobs) > 0 {
		result, err := s.dbClient.LogStreams.GetLogStreams(ctx, &db.GetLogStreamsInput{
			Filter: &db.LogStreamFilter{
				JobIDs: idList,
			},
		})
		if err != nil {
			return nil, err
		}

		return result.LogStreams, nil
	}

	return []models.LogStream{}, nil
}

func (s *service) GetJobByPRN(ctx context.Context, prn string) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "svc.GetJobByPRN")
	span.SetAttributes(attribute.String("prn", prn))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	job, err := s.dbClient.Jobs.GetJobByPRN(ctx, prn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get job by PRN", errors.WithSpan(span))
	}

	if job == nil {
		return nil, errors.New("job not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	err = caller.RequirePermission(ctx, models.ViewJob, auth.WithJobID(job.Metadata.ID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return nil, err
	}

	return job, nil
}

func (s *service) SetJobStatus(ctx context.Context, jobID string, status models.JobStatus) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "svc.SetJobStatus")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	job, err := s.getJobByID(ctx, jobID)
	if err != nil {
		tracing.RecordError(span, err, "failed to get job by ID")
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.UpdateJobState, auth.WithJobID(job.Metadata.ID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return nil, err
	}

	// Check if job status already equals the desired status
	if job.GetStatus() == status {
		return job, nil
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, err
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for SetJobStatus: %v", txErr)
		}
	}()

	now := time.Now().UTC()

	// Update timestamps if transitioning to running or completed
	switch status {
	case models.JobRunning:
		job.Timestamps.RunningTimestamp = &now

		// Add metric for pending duration, verify pending timestamp is not nill even though this should
		// never happen
		if job.Timestamps.PendingTimestamp != nil {
			jobPendingDuration.Observe(float64(time.Since(*job.Timestamps.PendingTimestamp).Seconds()))
		}

	case models.JobFailed,
		models.JobSucceeded,
		models.JobCanceled:
		job.Timestamps.FinishedTimestamp = &now
	}

	if err = job.SetStatus(status); err != nil {
		return nil, err
	}

	job, err = s.dbClient.Jobs.UpdateJob(txContext, job)
	if err != nil {
		return nil, err
	}

	switch typed := job.Data.(type) {
	case *models.JobTaskData:
		// Process job status change event if job is a task
		_, err := s.pipelineUpdater.ProcessEvent(txContext, typed.PipelineID, &pipeline.JobStatusChangeEvent{
			Job: job,
		})
		if err != nil {
			return nil, err
		}
	}

	// If job is completed, mark log stream as completed
	if job.GetStatus().IsFinishedStatus() {
		logStream, err := s.dbClient.LogStreams.GetLogStreamByJobID(txContext, job.Metadata.ID)
		if err != nil {
			return nil, err
		}

		if logStream != nil {
			logStream.Completed = true
			if _, err = s.dbClient.LogStreams.UpdateLogStream(txContext, logStream); err != nil {
				return nil, err
			}
		}
	}

	if err := s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, err
	}

	return job, nil
}

func (s *service) SubscribeToLogStreamEvents(ctx context.Context, options *LogStreamEventSubscriptionOptions) (<-chan *logstream.LogEvent, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToLogStreamEvents")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	job, err := s.getJobByID(ctx, options.JobID)
	if err != nil {
		tracing.RecordError(span, err, "failed to get job by ID")
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewJob, auth.WithJobID(job.Metadata.ID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return nil, err
	}

	logStream, err := s.dbClient.LogStreams.GetLogStreamByJobID(ctx, job.Metadata.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get log stream by job ID", errors.WithSpan(span))
	}

	if logStream == nil {
		return nil, fmt.Errorf("log stream not found for job %s", job.Metadata.ID)
	}

	return s.logStreamManager.Subscribe(ctx, &logstream.SubscriptionOptions{
		LastSeenLogSize: options.LastSeenLogSize,
		LogStreamID:     logStream.Metadata.ID,
	})
}

func (s *service) GetJobByID(ctx context.Context, jobID string) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "svc.GetJobByID")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	job, err := s.getJobByID(ctx, jobID)
	if err != nil {
		tracing.RecordError(span, err, "failed to get job by ID")
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewJob, auth.WithJobID(jobID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return nil, err
	}

	return job, nil
}

func (s *service) GetJobsByIDs(ctx context.Context, idList []string) ([]models.Job, error) {
	ctx, span := tracer.Start(ctx, "svc.GetJobsByIDs")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	resp, err := s.dbClient.Jobs.GetJobs(ctx, &db.GetJobsInput{Filter: &db.JobFilter{JobIDs: idList}})
	if err != nil {
		return nil, errors.Wrap(err, "failed to get jobs", errors.WithSpan(span))
	}

	// Verify user has access to all returned jobs
	for _, job := range resp.Jobs {
		err = caller.RequirePermission(ctx, models.ViewJob, auth.WithJobID(job.Metadata.ID), auth.WithProjectID(job.ProjectID))
		if err != nil {
			return nil, err
		}
	}

	return resp.Jobs, nil
}

// ClaimJob claims a job and returns it.
func (s *service) ClaimJob(ctx context.Context, agentID string) (*ClaimJobResponse, error) {
	ctx, span := tracer.Start(ctx, "svc.ClaimJob")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	agent, err := s.dbClient.Agents.GetAgentByID(ctx, agentID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get agent", errors.WithSpan(span))
	}

	if agent == nil {
		return nil, errors.New("agent not found", errors.WithErrorCode(errors.ENotFound), errors.WithSpan(span))
	}

	err = caller.RequirePermission(ctx, models.ClaimJob, auth.WithAgentID(agent.Metadata.ID))
	if err != nil {
		return nil, err
	}

	for {
		job, err := s.getNextAvailableQueuedJob(ctx, agent)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get next available queued job", errors.WithSpan(span))
		}

		// Attempt to claim job
		now := time.Now().UTC()
		job.Timestamps.PendingTimestamp = &now
		job.AgentID = &agent.Metadata.ID
		job.AgentName = &agent.Name
		job.AgentType = ptr.String(string(agent.Type))

		// Add metric for queued duration, verify queued timestamp is not nill even though this should
		// never happen
		if job.Timestamps.QueuedTimestamp != nil {
			jobQueuedDuration.Observe(float64(time.Since(*job.Timestamps.QueuedTimestamp).Seconds()))
		}

		if err = job.SetStatus(models.JobPending); err != nil {
			return nil, err
		}

		job, err = s.dbClient.Jobs.UpdateJob(ctx, job)
		if err != nil {
			if errors.ErrorCode(err) == errors.EOptimisticLock {
				// Another agent claimed the job before this one
				continue
			}
			return nil, err
		}

		if job != nil {
			maxJobDuration := time.Duration(job.MaxJobDuration) * time.Minute
			expiration := time.Now().Add(maxJobDuration + time.Hour)
			token, err := s.idp.GenerateToken(ctx, &auth.TokenInput{
				// Expiration is job timeout plus 1 hour to give the job time to gracefully exit
				Expiration: &expiration,
				Subject:    fmt.Sprintf("job-%s", job.Metadata.ID),
				Audience:   "phobos",
				Claims: map[string]interface{}{
					"job_id":     gid.ToGlobalID(gid.JobType, job.Metadata.ID),
					"project_id": gid.ToGlobalID(gid.ProjectType, job.ProjectID),
					"type":       auth.JobTokenType,
				},
			})
			if err != nil {
				return nil, errors.Wrap(err, "failed to generate token", errors.WithSpan(span))
			}

			s.logger.Infow("Claimed a job.",
				"caller", caller.GetSubject(),
				"projectID", job.ProjectID,
				"jobID", job.Metadata.ID,
			)

			return &ClaimJobResponse{Job: job, Token: string(token)}, nil
		}
	}
}

func (s *service) SubscribeToCancellationEvent(ctx context.Context, options *CancellationSubscriptionsOptions) (<-chan *CancellationEvent, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToCancellationEvent")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	job, err := s.getJobByID(ctx, options.JobID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get job", errors.WithSpan(span))
	}

	err = caller.RequirePermission(ctx, models.ViewJob, auth.WithJobID(options.JobID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return nil, err
	}

	subscription := events.Subscription{
		Type:    events.JobSubscription,
		ID:      options.JobID,
		Actions: []events.SubscriptionAction{events.UpdateAction},
	}

	subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})

	outgoing := make(chan *CancellationEvent)
	go func() {
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		// Query for the job after the subscription is setup to ensure no events are missed
		job, err := s.getJobByID(ctx, options.JobID)
		if err != nil {
			s.logger.Errorf("Error occurred while checking for job cancellation: %v", err)
			return
		}

		if job.GetStatus() == models.JobCanceling {
			outgoing <- &CancellationEvent{Job: job}
			return
		}

		// Wait for job updates
		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) && !goerrors.Is(err, context.DeadlineExceeded) {
					s.logger.Errorf("Error occurred while waiting for job cancellation events: %v", err)
				}
				return
			}

			job, err := s.getJobByID(ctx, event.ID)
			if err != nil {
				if !errors.IsContextCanceledError(err) && !goerrors.Is(err, context.DeadlineExceeded) {
					s.logger.Errorf("Error occurred while querying for job associated with cancellation event %s: %v", event.ID, err)
				}
				return
			}

			if job.GetStatus() != models.JobCanceling {
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- &CancellationEvent{Job: job}:
			}
		}
	}()

	return outgoing, nil
}

func (s *service) WriteLogs(ctx context.Context, jobID string, startOffset int, logs []byte) (int, error) {
	ctx, span := tracer.Start(ctx, "svc.WriteLogs")
	span.SetAttributes(attribute.String("job_id", jobID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return 0, err
	}

	job, err := s.getJobByID(ctx, jobID)
	if err != nil {
		tracing.RecordError(span, err, "failed to get job by ID")
		return 0, err
	}

	err = caller.RequirePermission(ctx, models.UpdateJobState, auth.WithJobID(jobID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return 0, err
	}

	stream, err := s.dbClient.LogStreams.GetLogStreamByJobID(ctx, jobID)
	if err != nil {
		return 0, err
	}

	if stream == nil {
		return 0, errors.New("log stream not found for job %s", jobID)
	}

	// This is the first log we have received for this job if sream size is 0
	if stream.Size == 0 {
		// Add metric for time to first log duration, verify running timestamp is not nill even though this should
		// never happen
		if job.Timestamps.RunningTimestamp != nil {
			duration := time.Since(*job.Timestamps.RunningTimestamp).Seconds()
			jobTimeToFirstLogDuration.Observe(float64(duration))
			if duration > 60 {
				// This is a temporary log to identity jobs that are taking longer than expected to send logs
				s.logger.Errorf("Job took too long to send logs: %s", gid.ToGlobalID(gid.JobType, job.Metadata.ID))
			}
		}
	}

	// Write logs to store
	updatedStream, err := s.logStreamManager.WriteLogs(ctx, stream.Metadata.ID, startOffset, logs)
	if err != nil {
		return 0, err
	}

	return updatedStream.Size, nil
}

func (s *service) ReadLogs(ctx context.Context, jobID string, startOffset int, limit int) ([]byte, error) {
	ctx, span := tracer.Start(ctx, "svc.ReadLogs")
	span.SetAttributes(attribute.String("job_id", jobID))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	job, err := s.getJobByID(ctx, jobID)
	if err != nil {
		tracing.RecordError(span, err, "failed to get job by ID")
		return nil, err
	}

	err = caller.RequirePermission(ctx, models.ViewJob, auth.WithJobID(jobID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return nil, err
	}

	// Find log stream associated with job
	stream, err := s.dbClient.LogStreams.GetLogStreamByJobID(ctx, jobID)
	if err != nil {
		return nil, err
	}
	if stream == nil {
		return nil, errors.New("log stream not found %s", jobID)
	}

	return s.logStreamManager.ReadLogs(ctx, stream.Metadata.ID, startOffset, limit)
}

func (s *service) GetJobs(ctx context.Context, input *GetJobsInput) (*db.JobsResult, error) {
	ctx, span := tracer.Start(ctx, "svc.GetJobs")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	if input.AgentID != nil {
		agent, aErr := s.dbClient.Agents.GetAgentByID(ctx, *input.AgentID)
		if aErr != nil {
			return nil, aErr
		}
		if agent == nil {
			return nil, errors.New("agent not found with ID: %s", *input.AgentID, errors.WithErrorCode(errors.ENotFound))
		}
		if aErr = agnt.RequireViewerAccessToAgentResource(ctx, agent); aErr != nil {
			return nil, aErr
		}
	} else if input.ProjectID != nil {
		// Permission checks by project.
		err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithProjectID(*input.ProjectID))
		if err != nil {
			return nil, err
		}
	} else {
		if !caller.IsAdmin() {
			return nil, errors.New("caller must be an admin to query jobs without filters", errors.WithErrorCode(errors.EForbidden))
		}
	}

	dbInput := &db.GetJobsInput{
		Sort:              input.Sort,
		PaginationOptions: input.PaginationOptions,
		Filter: &db.JobFilter{
			ProjectID:        input.ProjectID,
			PipelineID:       input.PipelineID,
			PipelineTaskPath: input.PipelineTaskPath,
			AgentID:          input.AgentID,
			JobStatuses:      input.JobStatuses,
		},
	}

	resp, err := s.dbClient.Jobs.GetJobs(ctx, dbInput)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get jobs", errors.WithSpan(span))
	}

	return resp, nil
}

func (s *service) SubscribeToJobs(ctx context.Context, options *SubscribeToJobsInput) (<-chan *Event, error) {
	ctx, span := tracer.Start(ctx, "svc.SubscribeToJobs")
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	switch {
	case options.ProjectID != nil:
		if err := caller.RequirePermission(ctx, models.ViewProject, auth.WithProjectID(*options.ProjectID)); err != nil {
			return nil, err
		}
	case options.PipelineID != nil:
		pipeline, err := s.dbClient.Pipelines.GetPipelineByID(ctx, *options.PipelineID)
		if err != nil {
			return nil, err
		}
		if pipeline == nil {
			return nil, errors.New("pipeline not found with ID: %s", *options.PipelineID, errors.WithErrorCode(errors.ENotFound))
		}
		err = caller.RequirePermission(ctx, models.ViewPipeline, auth.WithPipelineID(pipeline.Metadata.ID), auth.WithProjectID(pipeline.ProjectID))
		if err != nil {
			return nil, err
		}
	case options.AgentID != nil:
		agent, aErr := s.dbClient.Agents.GetAgentByID(ctx, *options.AgentID)
		if aErr != nil {
			return nil, aErr
		}
		if agent == nil {
			return nil, errors.New("agent not found with ID: %s", *options.AgentID, errors.WithErrorCode(errors.ENotFound))
		}
		if aErr = agnt.RequireViewerAccessToAgentResource(ctx, agent); aErr != nil {
			return nil, aErr
		}
	default:
		if !caller.IsAdmin() {
			return nil, errors.New(
				"Only system admins can subscribe to all job events without filters",
				errors.WithErrorCode(errors.EForbidden),
			)
		}
	}

	subscription := events.Subscription{
		Type: events.JobSubscription,
		Actions: []events.SubscriptionAction{
			events.CreateAction,
			events.UpdateAction,
		},
	}

	subscriber := s.eventManager.Subscribe([]events.Subscription{subscription})

	outgoing := make(chan *Event)
	go func() {
		// Defer close of outgoing channel
		defer close(outgoing)
		defer s.eventManager.Unsubscribe(subscriber)

		// Wait for agent session updates
		for {
			event, err := subscriber.GetEvent(ctx)
			if err != nil {
				if !errors.IsContextCanceledError(err) {
					s.logger.Errorf("Error occurred while waiting for job events: %v", err)
				}
				return
			}

			eventData, err := event.ToJobEventData()
			if err != nil {
				s.logger.Errorf("failed to get job event data in job subscription: %v", err)
				continue
			}

			// Check if this event is for the agent we're interested in
			if options.AgentID != nil && (eventData.AgentID == nil || *eventData.AgentID != *options.AgentID) {
				continue
			}

			// Check if this event is for the project we're interested in
			if options.ProjectID != nil && eventData.ProjectID != *options.ProjectID {
				continue
			}

			// Check if this event is for the pipeline we're interested in
			if options.PipelineID != nil {
				switch eventData.Type {
				case models.JobTaskType:
					if eventData.Data.PipelineID != *options.PipelineID {
						continue
					}
				default:
					continue
				}
			}

			job, err := s.dbClient.Jobs.GetJobByID(ctx, event.ID)
			if err != nil {
				s.logger.Errorf("Error querying for job in subscription goroutine: %v", err)
				continue
			}
			if job == nil {
				s.logger.Errorf("Received event for job that does not exist %s", event.ID)
				continue
			}

			select {
			case <-ctx.Done():
				return
			case outgoing <- &Event{Job: job, Action: event.Action}:
			}
		}
	}()

	return outgoing, nil
}

func (s *service) CancelJob(ctx context.Context, input *CancelJobInput) (*models.Job, error) {
	ctx, span := tracer.Start(ctx, "svc.CancelJob")
	span.SetAttributes(attribute.String("job id", input.ID))
	span.SetAttributes(attribute.Bool("force", input.Force))
	defer span.End()

	caller, err := auth.AuthorizeCaller(ctx)
	if err != nil {
		return nil, err
	}

	job, err := s.getJobByID(ctx, input.ID)
	if err != nil {
		tracing.RecordError(span, err, "failed to get job by ID")
		return nil, err
	}

	// CancelPipeline perm will allow the caller to cancel
	// jobs as well since jobs are associated with a pipeline.
	err = caller.RequirePermission(ctx, models.CancelPipeline, auth.WithJobID(input.ID), auth.WithProjectID(job.ProjectID))
	if err != nil {
		return nil, err
	}

	if job.GetStatus().IsFinishedStatus() {
		return nil, errors.New(
			"job has already %s, so it cannot be canceled", job.GetStatus(),
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	if input.Version != nil {
		job.Metadata.Version = *input.Version
	}

	var pipelineID string
	switch job.Type {
	case models.JobTaskType:
		data, ok := job.Data.(*models.JobTaskData)
		if !ok {
			return nil, errors.New("job does not contain valid task data", errors.WithSpan(span))
		}

		pipelineID = data.PipelineID
	default:
		return nil, errors.New(
			"job type %s is not supported", job.Type,
			errors.WithErrorCode(errors.EInvalid),
			errors.WithSpan(span),
		)
	}

	if input.Force {
		if err = job.ForceCancel(caller.GetSubject()); err != nil {
			return nil, err
		}
	} else {
		if err = job.GracefullyCancel(); err != nil {
			return nil, err
		}
	}

	txContext, err := s.dbClient.Transactions.BeginTx(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to begin DB transaction", errors.WithSpan(span))
	}

	defer func() {
		if txErr := s.dbClient.Transactions.RollbackTx(txContext); txErr != nil {
			s.logger.Errorf("failed to rollback tx for service layer CancelJob: %v", txErr)
		}
	}()

	if _, err = s.pipelineUpdater.ProcessEvent(txContext, pipelineID, &pipeline.JobStatusChangeEvent{
		Job: job,
	}); err != nil {
		return nil, err
	}

	// Update the job to reflect the new status
	if _, err = s.dbClient.Jobs.UpdateJob(txContext, job); err != nil {
		return nil, errors.Wrap(err, "failed to update job", errors.WithSpan(span))
	}

	if err = s.dbClient.Transactions.CommitTx(txContext); err != nil {
		return nil, errors.Wrap(err, "failed to commit DB transaction", errors.WithSpan(span))
	}

	s.logger.Infow("Requested to cancel a job.",
		"caller", caller.GetSubject(),
		"jobID", job.Metadata.ID,
		"force", input.Force,
	)

	return job, nil
}

// getJobByID returns a non-nil job.
func (s *service) getJobByID(ctx context.Context, jobID string) (*models.Job, error) {
	job, err := s.dbClient.Jobs.GetJobByID(ctx, jobID)
	if err != nil {
		return nil, err
	}

	if job == nil {
		return nil, errors.New("Job with ID %s not found", jobID, errors.WithErrorCode(errors.ENotFound))
	}

	return job, nil
}

func (s *service) getNextAvailableQueuedJob(ctx context.Context, agent *models.Agent) (*models.Job, error) {
	// Subscribe to job create and update events
	jobSubscription := events.Subscription{
		Type:    events.JobSubscription,
		Actions: []events.SubscriptionAction{events.CreateAction, events.UpdateAction},
	}
	// Subscribe to agent events because an agent may become enabled
	agentSubscription := events.Subscription{
		Type:    events.AgentSubscription,
		Actions: []events.SubscriptionAction{},
	}
	// Subscribe to project delete events because deleting a project may cause a job in a different project to become available
	projectSubscription := events.Subscription{
		Type:    events.ProjectSubscription,
		Actions: []events.SubscriptionAction{events.DeleteAction},
	}

	// Subscribe to job and run events
	subscriber := s.eventManager.Subscribe([]events.Subscription{jobSubscription, agentSubscription, projectSubscription})
	defer s.eventManager.Unsubscribe(subscriber)

	checkForJobs := true
	jobStatusesToCheck := []models.JobStatus{models.JobQueued, models.JobSucceeded, models.JobFailed, models.JobCanceled}

	// Wait for next available run
	for {
		if checkForJobs {
			job, err := s.getNextAvailableJob(ctx, agent.Metadata.ID)
			if err != nil {
				return nil, err
			}

			if job != nil {
				return job, nil
			}
		}

		evt, err := subscriber.GetEvent(ctx)
		if err != nil {
			return nil, err
		}

		if agent.Type == models.OrganizationAgent {
			switch events.SubscriptionType(evt.Table) {
			case events.JobSubscription:
				// Must do a query due to calls to GetOrgName and GetStatus.
				job, err := s.dbClient.Jobs.GetJobByID(ctx, evt.ID)
				if err != nil {
					return nil, err
				}
				checkForJobs = job != nil && job.GetOrgName() == agent.GetOrgName() &&
					slices.Contains(jobStatusesToCheck, job.GetStatus())
			case events.AgentSubscription:
				checkForJobs = evt.ID == agent.Metadata.ID
			case events.ProjectSubscription:
				checkForJobs = true
			default:
				checkForJobs = false
			}
		} else {
			switch events.SubscriptionType(evt.Table) {
			case events.JobSubscription:
				// Must do a query due to call to GetStatus.
				job, err := s.dbClient.Jobs.GetJobByID(ctx, evt.ID)
				if err != nil {
					return nil, err
				}
				checkForJobs = job != nil && slices.Contains(jobStatusesToCheck, job.GetStatus())
			default:
				checkForJobs = true
			}
		}
	}
}

// getNextAvailableJob returns a new job when project doesn't have an active job
// and the agent is not full.
func (s *service) getNextAvailableJob(ctx context.Context, agentID string) (*models.Job, error) {
	agent, err := s.dbClient.Agents.GetAgentByID(ctx, agentID)
	if err != nil {
		return nil, err
	}

	if agent == nil {
		return nil, errors.New("agent was deleted", errors.WithErrorCode(errors.ENotFound))
	}

	if agent.Disabled {
		// Return nil since agent is disabled
		return nil, nil
	}

	agentTags := agent.Tags
	if agentTags == nil {
		agentTags = []string{}
	}

	tagFilter := &db.JobTagFilter{
		TagSuperset: agentTags,
	}

	if !agent.RunUntaggedJobs {
		tagFilter.ExcludeUntaggedJobs = ptr.Bool(true)
	}

	// Request next available Job
	sortBy := db.JobSortableFieldCreatedAtAsc
	jobsResult, err := s.dbClient.Jobs.GetJobs(ctx, &db.GetJobsInput{
		Sort: &sortBy,
		Filter: &db.JobFilter{
			JobStatuses: []models.JobStatus{models.JobQueued},
			OrgID:       agent.OrganizationID, // This will be non-nil for org agents
			TagFilter:   tagFilter,
		},
	})
	if err != nil {
		return nil, fmt.Errorf("failed to request next job: %w", err)
	}

	for _, job := range jobsResult.Jobs {
		below, err := s.isAgentBelowJobsLimit(ctx, agent)
		if err != nil {
			return nil, err
		}

		if below {
			return &job, nil
		}
	}

	return nil, nil
}

// isAgentBelowJobsLimit determines if agent is full.
func (s *service) isAgentBelowJobsLimit(ctx context.Context, agent *models.Agent) (bool, error) {
	agentJobsCount, err := s.dbClient.Jobs.GetJobCountForAgent(ctx, agent.Metadata.ID)
	if err != nil {
		return false, err
	}

	return agentJobsCount < agentJobsLimit, nil
}
