// Package logstream is used to buffer and write logs to a log stream server
package logstream

import (
	"context"
	"sync"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

const (
	defaultMaxBytesPerPatch = 1024 * 1024 // in bytes
	defaultUpdateInterval   = 3 * time.Second
	maxRetryCount           = 10
)

// LogSink represents a sink for writing logs to
type LogSink interface {
	Write(ctx context.Context, startOffset int32, buffer []byte) error
}

// Writer is responsible for buffering and writing logs to the specified log sink
type Writer struct {
	sentTime         time.Time
	sink             LogSink
	logger           logger.Logger
	buffer           *LogBuffer
	finished         chan bool
	updateInterval   time.Duration
	lock             sync.RWMutex
	bytesSent        int32
	maxBytesPerPatch int32
}

// NewWriter returns a new writer instance
func NewWriter(sink LogSink, logger logger.Logger) (*Writer, error) {
	buffer, err := NewLogBuffer()
	if err != nil {
		return nil, err
	}

	return &Writer{
		buffer:           buffer,
		maxBytesPerPatch: defaultMaxBytesPerPatch,
		updateInterval:   defaultUpdateInterval,
		sink:             sink,
		logger:           logger,
	}, nil
}

// Close flushes the logger
func (j *Writer) Close() {
	j.finish()
}

// Write will append the data to the log buffer
func (j *Writer) Write(data []byte) (n int, err error) {
	j.logger.Infof("JOB OUTPUT: %s", string(data))
	return j.buffer.Write(data)
}

// nolint:unused
func (j *Writer) checksum() string {
	return j.buffer.Checksum()
}

// nolint:unused
func (j *Writer) bytesize() int32 {
	return j.buffer.Size()
}

// Start will start the goroutine which sends logs to the log sink
func (j *Writer) Start() {
	j.finished = make(chan bool)
	go j.run()
}

// Flush will attempt to send all logs currently stored in the buffer
func (j *Writer) Flush() {
	retryCount := 0
	for j.anyLogsToSend() {
		if err := j.sendPatch(); err != nil {
			j.logger.Errorf("Received error when flushing logs (retry count = %d) %v", retryCount, err)
			if retryCount >= maxRetryCount {
				j.logger.Errorf("Failed to send logs, reached max retry count of %d", maxRetryCount)
				return
			}

			retryCount++

			time.Sleep(10 * time.Second)
		}
	}
}

func (j *Writer) finish() {
	j.finished <- true
	j.Flush()
	j.buffer.Close()
}

func (j *Writer) anyLogsToSend() bool {
	j.lock.RLock()
	defer j.lock.RUnlock()

	return j.buffer.Size() != j.bytesSent
}

func (j *Writer) sendPatch() error {
	j.lock.RLock()
	content, err := j.buffer.Bytes(j.bytesSent, j.maxBytesPerPatch)
	bytesSent := j.bytesSent
	j.lock.RUnlock()

	if err != nil {
		return err
	}

	if len(content) == 0 {
		return nil
	}

	if err := j.sink.Write(context.Background(), j.bytesSent, content); err != nil {
		return err
	}

	j.lock.Lock()
	j.sentTime = time.Now()
	j.bytesSent = bytesSent + int32(len(content))
	j.lock.Unlock()

	return nil
}

func (j *Writer) run() {
	for {
		select {
		case <-time.After(j.updateInterval):
			if err := j.sendPatch(); err != nil {
				j.logger.Errorf("Failed to send log patch %v", err)
			}
		case <-j.finished:
			return
		}
	}
}
