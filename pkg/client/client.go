// Package client contains a client implementation for interfacing with the Phobos server
package client

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/hashicorp/go-retryablehttp"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/keepalive"
)

// Retry configuration with maximum of 4 attempts. Add more services and methods under 'name'.
// It will only retry for UNAVAILABLE and UNKNOWN status codes.
// See: https://github.com/grpc/grpc-go/blob/master/examples/features/retry/README.md#define-your-retry-policy
const retryPolicy = `{
	"methodConfig": [{
		"name": [
			{"service": "martiancloud.phobos.api.job.Jobs"},
			{"service": "martiancloud.phobos.api.organization.Organizations"},
			{"service": "martiancloud.phobos.api.project.Projects"},
			{"service": "martiancloud.phobos.api.authsettings.AuthSettings"},
			{"service": "martiancloud.phobos.api.team.Teams"},
			{"service": "martiancloud.phobos.api.pipelinetemplate.PipelineTemplates"},
			{"service": "martiancloud.phobos.api.pipeline.Pipelines"},
			{"service": "martiancloud.phobos.api.agent.Agents"},
			{"service": "martiancloud.phobos.api.environment.Environments"},
			{"service": "martiancloud.phobos.api.serviceaccount.ServiceAccounts"},
			{"service": "martiancloud.phobos.api.lifecycletemplate.LifecycleTemplates"},
			{"service": "martiancloud.phobos.api.releaselifecycle.ReleaseLifecycles"},
			{"service": "martiancloud.phobos.api.release.Releases"},
			{"service": "martiancloud.phobos.api.pluginregistry.PluginRegistry"},
			{"service": "martiancloud.phobos.api.vcsprovider.VCSProviders"},
			{"service": "martiancloud.phobos.api.version.Version"}
		],

		"waitForReady": true,

		"retryPolicy": {
			"MaxAttempts": 4,
			"InitialBackoff": ".5s",
			"MaxBackoff": "30s",
			"BackoffMultiplier": 2,
			"RetryableStatusCodes": [ "UNAVAILABLE", "UNKNOWN" ]
		}
	}]
}`

// contextCredentials implements the credentials.PerRPCCredentials interface and
// allows us to use a token passed into the RPC request metadata.
type contextCredentials struct {
	getter TokenGetter
}

// GetRequestMetadata sets the token on the context metadata.
func (c *contextCredentials) GetRequestMetadata(ctx context.Context, _ ...string) (map[string]string, error) {
	newToken, err := c.getter.Token(ctx)
	if err != nil {
		return nil, err
	}

	return map[string]string{
		"authorization": string(newToken),
	}, nil
}

// RequireTransportSecurity indicates if transport security is required.
func (c *contextCredentials) RequireTransportSecurity() bool {
	// If true, it won't be possible to use token auth without TLS (locally).
	return false
}

// TokenGetter is an interface for retrieving and renewing a service account token.
type TokenGetter interface {
	Token(ctx context.Context) (string, error)
}

// LeveledLogger is an interface that can be implemented by any logger or a
// logger wrapper to provide leveled logging. The methods accept a message
// string and a variadic number of key-value pairs. For log.Printf style
// formatting where message string contains a format specifier, use Logger
// interface.
// This interface has been copied from the retryablehttp package.
type LeveledLogger interface {
	Error(msg string, keysAndValues ...interface{})
	Info(msg string, keysAndValues ...interface{})
	Debug(msg string, keysAndValues ...interface{})
	Warn(msg string, keysAndValues ...interface{})
}

// Client is the gateway to interact with the Phobos API.
type Client struct {
	connection               *grpc.ClientConn
	AuthSettingsClient       pb.AuthSettingsClient
	OrganizationsClient      pb.OrganizationsClient
	JobsClient               pb.JobsClient
	TeamsClient              pb.TeamsClient
	ProjectsClient           pb.ProjectsClient
	PipelinesClient          pb.PipelinesClient
	PipelineTemplatesClient  pb.PipelineTemplatesClient
	AgentsClient             pb.AgentsClient
	EnvironmentsClient       pb.EnvironmentsClient
	ServiceAccountsClient    pb.ServiceAccountsClient
	LifecycleTemplatesClient pb.LifecycleTemplatesClient
	ReleaseLifecyclesClient  pb.ReleaseLifecyclesClient
	ReleasesClient           pb.ReleasesClient
	PluginRegistryClient     pb.PluginRegistryClient
	VCSProvidersClient       pb.VCSProvidersClient
	VersionClient            pb.VersionClient
}

// Config is used to configure the client
type Config struct {
	TokenGetter   TokenGetter
	HTTPEndpoint  string
	TLSSkipVerify bool
	Logger        LeveledLogger
}

// New returns a new Client struct.
func New(ctx context.Context, c *Config) (*Client, error) {
	dialOptions := []grpc.DialOption{
		// Block until the underlying connection is up.
		grpc.WithBlock(),
		// Set Retry policy.
		grpc.WithDefaultServiceConfig(retryPolicy),
		// Configure keepalive
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			// After a duration of this time if the client doesn't see any activity it pings the server to see if the transport is still alive.
			Time: 2 * time.Minute,
			// After having pinged for keepalive check, the client waits for a duration of Timeout before deciding it is dead.
			Timeout: 30 * time.Second,
			// Send keepalive pings when there are no active RPCs.
			PermitWithoutStream: true,
		}),
	}

	if c.TokenGetter != nil {
		// Add token based auth since we're using it.
		dialOptions = append(
			dialOptions,
			grpc.WithPerRPCCredentials(&contextCredentials{
				getter: c.TokenGetter,
			}),
		)
	}

	if c.TLSSkipVerify {
		// Override the default transport to skip TLS verification.
		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true} // #nosec G402 -- This is used in development
	}

	// Fetch discovery document from API.
	discoveryDocument, err := NewGRPCDiscoveryDocument(ctx, c.HTTPEndpoint, WithLogger(c.Logger))
	if err != nil && errors.Is(err, context.DeadlineExceeded) {
		// The context deadline was exceeded, try one more time before returning an error
		discoveryDocument, err = NewGRPCDiscoveryDocument(ctx, c.HTTPEndpoint, WithLogger(c.Logger))
	}
	if err != nil {
		return nil, fmt.Errorf("failed to get discovery document: %w", err)
	}

	if !discoveryDocument.HasTransportSecurity() {
		// Disable TLS since it isn't being used on this connection.
		dialOptions = append(
			dialOptions,
			grpc.WithTransportCredentials(insecure.NewCredentials()),
		)
	} else if c.TLSSkipVerify {
		// Use TLS but don't verify server's certificate chain and hostname. For testing only.
		dialOptions = append(
			dialOptions,
			grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{InsecureSkipVerify: true})),
		)
	} else {
		// Use TLS by default.
		dialOptions = append(
			dialOptions,
			grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{})),
		)
	}

	if c.Logger != nil {
		c.Logger.Info("dialing GRPC connection", "host", discoveryDocument.Host, "port", discoveryDocument.Port)
	}

	// Use context timeout for dialing the grpc connection.
	ctx, cancel := context.WithTimeout(ctx, 60*time.Second)
	defer cancel()

	clientConn, err := grpc.DialContext(ctx,
		fmt.Sprintf("%s:%s", discoveryDocument.Host, discoveryDocument.Port),
		dialOptions...,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to gRPC endpoint: %w", err)
	}

	if c.Logger != nil {
		c.Logger.Info("successfully initialized GRPC connection")
	}

	return &Client{
		connection:               clientConn,
		AuthSettingsClient:       pb.NewAuthSettingsClient(clientConn),
		OrganizationsClient:      pb.NewOrganizationsClient(clientConn),
		JobsClient:               pb.NewJobsClient(clientConn),
		TeamsClient:              pb.NewTeamsClient(clientConn),
		ProjectsClient:           pb.NewProjectsClient(clientConn),
		PipelinesClient:          pb.NewPipelinesClient(clientConn),
		PipelineTemplatesClient:  pb.NewPipelineTemplatesClient(clientConn),
		AgentsClient:             pb.NewAgentsClient(clientConn),
		EnvironmentsClient:       pb.NewEnvironmentsClient(clientConn),
		ServiceAccountsClient:    pb.NewServiceAccountsClient(clientConn),
		LifecycleTemplatesClient: pb.NewLifecycleTemplatesClient(clientConn),
		ReleaseLifecyclesClient:  pb.NewReleaseLifecyclesClient(clientConn),
		ReleasesClient:           pb.NewReleasesClient(clientConn),
		PluginRegistryClient:     pb.NewPluginRegistryClient(clientConn),
		VCSProvidersClient:       pb.NewVCSProvidersClient(clientConn),
		VersionClient:            pb.NewVersionClient(clientConn),
	}, nil
}

// Close closes any underlying connections for this client.
func (c *Client) Close() error {
	return c.connection.Close()
}

// ServiceDiscoveryPath is the path to the service discovery document.
const ServiceDiscoveryPath = "/.well-known/phobos.json"

// GRPCDiscoveryDocument represents the contents of the GRPC discovery document.
type GRPCDiscoveryDocument struct {
	Host              string `json:"host"`
	TransportSecurity string `json:"transport_security"`
	Port              string `json:"port"`
}

// grpcDiscoveryOptions represents the options for the GRPC discovery document.
type grpcDiscoveryOptions struct {
	logger LeveledLogger
}

// GRPCDiscoveryOption is a function that sets options for the GRPC discovery document.
type GRPCDiscoveryOption func(*grpcDiscoveryOptions)

// WithLogger sets the logger for the GRPC discovery document.
func WithLogger(logger LeveledLogger) GRPCDiscoveryOption {
	return func(o *grpcDiscoveryOptions) {
		o.logger = logger
	}
}

// NewGRPCDiscoveryDocument returns a new GRPC discovery document.
// The HTTP get request of the discovery document is done in this function.
func NewGRPCDiscoveryDocument(ctx context.Context, endpoint string, options ...GRPCDiscoveryOption) (*GRPCDiscoveryDocument, error) {
	opts := &grpcDiscoveryOptions{}
	for _, o := range options {
		o(opts)
	}

	discoveryURL, err := url.JoinPath(endpoint, ServiceDiscoveryPath)
	if err != nil {
		return nil, err
	}

	fetchCtx, cancel := context.WithTimeout(ctx, 60*time.Second)
	defer cancel()

	req, err := retryablehttp.NewRequestWithContext(fetchCtx, http.MethodGet, discoveryURL, nil) // nosemgrep: gosec.G107-1
	if err != nil {
		return nil, err
	}

	retryableClient := retryablehttp.NewClient()
	retryableClient.RetryMax = 5

	if opts.logger != nil {
		// Allow overriding the logger for the client.
		retryableClient.Logger = opts.logger
	}

	resp, err := retryableClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received status code %d from well-known URL", resp.StatusCode)
	}

	var discoveryDocument struct {
		GRPCDiscoveryDocument *GRPCDiscoveryDocument `json:"grpc"`
	}

	if err = json.NewDecoder(resp.Body).Decode(&discoveryDocument); err != nil {
		return nil, err
	}

	return discoveryDocument.GRPCDiscoveryDocument, nil
}

// HasTransportSecurity returns true if the GRPC endpoint has transport security enabled.
func (d *GRPCDiscoveryDocument) HasTransportSecurity() bool {
	return d.TransportSecurity != "plaintext"
}
