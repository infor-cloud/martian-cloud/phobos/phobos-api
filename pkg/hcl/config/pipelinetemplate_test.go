package config

import (
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/hcl/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

func TestPipelineVolume_Validate(t *testing.T) {
	type testCase struct {
		volume      *Volume
		name        string
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid volume",
			volume: &Volume{
				Name: "v1",
				Type: "vcs",
				VCSOptions: &VCSOptions{
					ProviderID:     "some-provider",
					RepositoryPath: "some-repository",
				},
			},
		},
		{
			name: "invalid name",
			volume: &Volume{
				Name: "not/supported",
				Type: "vcs",
			},
			expectError: true,
		},
		{
			name: "volume type is not supported",
			volume: &Volume{
				Name: "v1",
				Type: "unknown",
			},
			expectError: true,
		},
		{
			name: "vcs_options block is not specified for a vcs volume type",
			volume: &Volume{
				Name: "v1",
				Type: "vcs",
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.volume.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipelineAction_Validate(t *testing.T) {
	type testCase struct {
		action      *PipelineAction
		name        string
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid action",
			action: &PipelineAction{
				Alias: ptr.String("valid-action"),
				Label: "exec_command",
			},
		},
		{
			name: "invalid action name",
			action: &PipelineAction{
				Alias: ptr.String("invalid name"),
				Label: "exec_command",
			},
			expectError: true,
		},
		{
			name: "invalid provider action id",
			action: &PipelineAction{
				Label: "invalid",
			},
			expectError: true,
		},
		{
			name: "action alias is a reserved keyword",
			action: &PipelineAction{
				Alias: ptr.String("action"),
				Label: "exec_command",
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.action.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipelineAction_PluginName(t *testing.T) {
	action := &PipelineAction{
		Label: "exec_command",
	}

	assert.Equal(t, "exec", action.PluginName())
}

func TestPipelineAction_PluginActionName(t *testing.T) {
	action := &PipelineAction{
		Label: "exec_command",
	}

	assert.Equal(t, "command", action.PluginActionName())
}

func TestNestedPipeline_Validate(t *testing.T) {
	lowerFailOnError := "fail"
	lowerContinueOnError := "continue"

	// Mock template id expression.
	templateIDExpr := hcl.StaticExpr(cty.StringVal("template_id = \"id\""), hcl.Range{Start: hcl.Pos{Byte: 1}, End: hcl.Pos{Byte: 5}})

	type testCase struct {
		pipeline    *NestedPipeline
		name        string
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid nested pipeline",
			pipeline: &NestedPipeline{
				Name:       "valid-nested-pipeline",
				TemplateID: templateIDExpr,
			},
		},
		{
			name: "valid nested pipeline with environment name",
			pipeline: &NestedPipeline{
				PipelineType:    ptr.String("deployment"),
				Name:            "valid-nested-pipeline",
				EnvironmentName: ptr.String("valid-environment"),
				TemplateID:      templateIDExpr,
			},
		},
		{
			name: "valid nested pipeline with auto when condition",
			pipeline: &NestedPipeline{
				PipelineType: ptr.String("nested"),
				Name:         "valid-nested-pipeline",
				When:         ptr.String("auto"),
				TemplateID:   templateIDExpr,
			},
		},
		{
			name: "valid nested pipeline with manual when condition",
			pipeline: &NestedPipeline{
				PipelineType: ptr.String("nested"),
				Name:         "valid-nested-pipeline",
				When:         ptr.String("manual"),
				TemplateID:   templateIDExpr,
			},
		},
		{
			name: "valid cron schedule",
			pipeline: &NestedPipeline{
				Name:       "test",
				TemplateID: templateIDExpr,
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * *",
					},
				},
			},
		},
		{
			name: "when cannot be manual when schedule is set",
			pipeline: &NestedPipeline{
				Name:       "test",
				TemplateID: templateIDExpr,
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * *",
					},
				},
				When: ptr.String("manual"),
			},
			expectError: true,
		},
		{
			name: "valid datetime schedule",
			pipeline: &NestedPipeline{
				Name:       "test",
				TemplateID: templateIDExpr,
				Schedule: &PipelineNodeSchedule{
					Type: DatetimeScheduleType,
					Options: map[string]string{
						"value": "2024-11-25T14:50:00.000Z",
					},
				},
			},
		},
		{
			name: "invalid cron schedule",
			pipeline: &NestedPipeline{
				Name:       "test",
				TemplateID: templateIDExpr,
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * * *",
					},
				},
			},
			expectError: true,
		},
		{
			name: "invalid datetime schedule",
			pipeline: &NestedPipeline{
				Name:       "test",
				TemplateID: templateIDExpr,
				Schedule: &PipelineNodeSchedule{
					Type: DatetimeScheduleType,
					Options: map[string]string{
						"value": "1733238119",
					},
				},
			},
			expectError: true,
		},
		{
			name: "invalid schedule type",
			pipeline: &NestedPipeline{
				Name:       "test",
				TemplateID: templateIDExpr,
				Schedule: &PipelineNodeSchedule{
					Type: PipelineNodeScheduleType("invalid"),
				},
			},
			expectError: true,
		},
		{
			name: "invalid nested pipeline name",
			pipeline: &NestedPipeline{
				PipelineType: ptr.String("nested"),
				Name:         "invalid name",
				TemplateID:   templateIDExpr,
			},
			expectError: true,
		},
		{
			name: "invalid nested pipeline environment name",
			pipeline: &NestedPipeline{
				PipelineType:    ptr.String("deployment"),
				Name:            "valid-nested-pipeline",
				EnvironmentName: ptr.String("invalid name"),
				TemplateID:      templateIDExpr,
			},
			expectError: true,
		},
		{
			name: "invalid nested pipeline when condition",
			pipeline: &NestedPipeline{
				PipelineType: ptr.String("nested"),
				Name:         "valid-nested-pipeline",
				When:         ptr.String("invalid"),
				TemplateID:   templateIDExpr,
			},
			expectError: true,
		},
		{
			name: "no template id provided",
			pipeline: &NestedPipeline{
				PipelineType: ptr.String("nested"),
				Name:         "valid-nested-pipeline",
			},
			expectError: true,
		},
		{
			name: "provided template id expression is empty",
			pipeline: &NestedPipeline{
				PipelineType: ptr.String("nested"),
				Name:         "valid-nested-pipeline",
				TemplateID:   hcl.StaticExpr(cty.NilVal, hcl.Range{}),
			},
			expectError: true,
		},
		{
			name: "valid OnError fail",
			pipeline: &NestedPipeline{
				Name:       "valid-nested-pipeline",
				TemplateID: templateIDExpr,
				OnError:    &lowerFailOnError,
			},
		},
		{
			name: "valid OnError continue",
			pipeline: &NestedPipeline{
				Name:       "valid-nested-pipeline",
				TemplateID: templateIDExpr,
				OnError:    &lowerContinueOnError,
			},
		},
		{
			name: "invalid OnError value",
			pipeline: &NestedPipeline{
				Name:       "valid-nested-pipeline",
				TemplateID: templateIDExpr,
				OnError:    ptr.String("invalid"),
			},
			expectError: true,
		},
		{
			name: "nested pipeline name is a reserved keyword",
			pipeline: &NestedPipeline{
				Name:       "pipeline",
				TemplateID: templateIDExpr,
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.pipeline.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipelineTask_Validate(t *testing.T) {
	lowerFailOnError := "fail"
	lowerContinueOnError := "continue"

	type testCase struct {
		task         *PipelineTask
		name         string
		actionsCount int
		expectError  bool
	}

	testCases := []testCase{
		{
			name: "valid task",
			task: &PipelineTask{
				Name: "valid-task",
			},
		},
		{
			name: "valid task with auto when condition",
			task: &PipelineTask{
				Name: "valid-task",
				When: ptr.String("auto"),
			},
			actionsCount: MaximumAllowedNodes,
		},
		{
			name: "valid task with manual when condition",
			task: &PipelineTask{
				Name: "valid-task",
				When: ptr.String("manual"),
			},
		},
		{
			name: "valid task with attempts greater than 1",
			task: &PipelineTask{
				Name:     "valid-task",
				Interval: ptr.String("1m"),
				Attempts: ptr.Int(1),
			},
		},
		{
			name: "cannot specify interval without maxAttempts",
			task: &PipelineTask{
				Name:     "valid-task",
				When:     ptr.String("auto"),
				Interval: ptr.String("1m"),
			},
			expectError: true,
		},
		{
			name: "interval is not a valid duration",
			task: &PipelineTask{
				Name:     "valid-task",
				Interval: ptr.String("invalid"),
				Attempts: ptr.Int(1),
			},
			expectError: true,
		},
		{
			name: "interval is less than minimum allowed duration",
			task: &PipelineTask{
				Name:     "valid-task",
				Interval: ptr.String(fmt.Sprintf("%fs", minTaskInterval.Seconds()-1)),
				Attempts: ptr.Int(1),
			},
			expectError: true,
		},
		{
			name: "attempts is less than minimum allowed attempts",
			task: &PipelineTask{
				Name:     "valid-task",
				Interval: ptr.String("1m"),
				Attempts: ptr.Int(0),
			},
			expectError: true,
		},
		{
			name: "invalid when condition",
			task: &PipelineTask{
				Name: "valid-task",
				When: ptr.String("invalid"),
			},
			expectError: true,
		},
		{
			name: "invalid task name",
			task: &PipelineTask{
				Name: "invalid name",
			},
			expectError: true,
		},
		{
			name: "max actions exceeded",
			task: &PipelineTask{
				Name: "valid-task",
			},
			actionsCount: MaximumAllowedNodes + 1,
			expectError:  true,
		},
		{
			name: "duplicate mount point",
			task: &PipelineTask{
				Name: "valid-task",
				MountPoints: []*MountPoint{
					{Volume: "v1"},
					{Volume: "v1"},
				},
			},
			expectError: true,
		},
		{
			name: "image name is too long",
			task: &PipelineTask{
				Name:  "valid-task",
				Image: ptr.String(strings.Repeat("a", maxTaskImageLength+1)),
			},
			expectError: true,
		},
		{
			name: "valid OnError fail",
			task: &PipelineTask{
				Name:    "valid-pipeline-task-on-error-fail",
				OnError: &lowerFailOnError,
			},
		},
		{
			name: "valid OnError continue",
			task: &PipelineTask{
				Name:    "valid-pipeline-task-on-error-continue",
				OnError: &lowerContinueOnError,
			},
		},
		{
			name: "valid cron schedule",
			task: &PipelineTask{
				Name: "valid-task",
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * *",
					},
				},
			},
		},
		{
			name: "valid datetime schedule",
			task: &PipelineTask{
				Name: "valid-task",
				Schedule: &PipelineNodeSchedule{
					Type: DatetimeScheduleType,
					Options: map[string]string{
						"value": "2024-11-25T14:50:00.000Z",
					},
				},
			},
		},
		{
			name: "invalid cron schedule",
			task: &PipelineTask{
				Name: "invalid-task",
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * * *",
					},
				},
			},
			expectError: true,
		},
		{
			name: "invalid datetime schedule",
			task: &PipelineTask{
				Name: "invalid-task",
				Schedule: &PipelineNodeSchedule{
					Type: DatetimeScheduleType,
					Options: map[string]string{
						"value": "1733238119",
					},
				},
			},
			expectError: true,
		},
		{
			name: "invalid schedule type",
			task: &PipelineTask{
				Name: "invalid-task",
				Schedule: &PipelineNodeSchedule{
					Type: PipelineNodeScheduleType("invalid"),
				},
			},
			expectError: true,
		},
		{
			name: "invalid OnError value",
			task: &PipelineTask{
				Name:    "valid-pipeline-task-on-error-invalid",
				OnError: ptr.String("invalid"),
			},
			expectError: true,
		},
		{
			name: "task name is a reserved keyword",
			task: &PipelineTask{
				Name: "task",
			},
			expectError: true,
		},
		{
			name: "task name is a reserved keyword",
			task: &PipelineTask{
				Name: "pre",
			},
			expectError: true,
		},
		{
			name: "task name is a reserved keyword",
			task: &PipelineTask{
				Name: "post",
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			for i := 0; i < tc.actionsCount; i++ {
				tc.task.Actions = append(tc.task.Actions, &PipelineAction{
					Alias: ptr.String(fmt.Sprintf("confirm-%d", i)),
					Label: "exec_command",
				})
			}

			err := tc.task.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipelineCondition_Validate(t *testing.T) {
	type testCase struct {
		name        string
		tasksCount  int
		expectError bool
	}

	testCases := []testCase{
		{
			name:       "valid condition with one task",
			tasksCount: 1,
		},
		{
			name:       "valid condition with maximum allowed tasks",
			tasksCount: MaximumAllowedNodes,
		},
		{
			name:        "invalid condition with no tasks",
			expectError: true,
		},
		{
			name:        "invalid condition with more than maximum allowed tasks",
			tasksCount:  MaximumAllowedNodes + 1,
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			condition := &PipelineCondition{}

			for i := 0; i < tc.tasksCount; i++ {
				condition.Tasks = append(condition.Tasks, &PipelineTask{
					Name: fmt.Sprintf("confirm-%d", i),
				})
			}

			err := condition.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipelineStage_Validate(t *testing.T) {
	type testCase struct {
		name                 string
		stageName            string
		tasksCount           int
		nestedPipelinesCount int
		expectError          bool
	}

	testCases := []testCase{
		{
			name:       "valid stage with one task",
			stageName:  "dev",
			tasksCount: 1,
		},
		{
			name:       "valid stage with maximum allowed tasks",
			stageName:  "dev",
			tasksCount: maxNodesPerStage,
		},
		{
			name:                 "valid stage with one nested pipeline",
			stageName:            "dev",
			nestedPipelinesCount: 1,
		},
		{
			name:                 "valid stage with maximum allowed nested pipelines",
			stageName:            "dev",
			nestedPipelinesCount: maxNodesPerStage,
		},
		{
			name:                 "valid stage with maximum allowed nested pipelines & tasks",
			stageName:            "dev",
			nestedPipelinesCount: maxNodesPerStage / 2,
			tasksCount:           maxNodesPerStage / 2,
		},
		{
			name:        "invalid stage with no tasks",
			stageName:   "valid-stage",
			expectError: true,
		},
		{
			name:        "invalid stage with more than maximum allowed tasks",
			stageName:   "dev",
			tasksCount:  maxNodesPerStage + 1,
			expectError: true,
		},
		{
			name:                 "invalid stage with more than maximum allowed nested pipelines",
			stageName:            "dev",
			nestedPipelinesCount: maxNodesPerStage + 1,
			expectError:          true,
		},
		{
			name:                 "invalid stage with one more nested pipeline & tasks than allowed",
			stageName:            "dev",
			nestedPipelinesCount: maxNodesPerStage/2 + 1,
			tasksCount:           maxNodesPerStage / 2,
			expectError:          true,
		},
		{
			name:                 "invalid stage with one more task & nested pipelines than allowed",
			stageName:            "dev",
			nestedPipelinesCount: maxNodesPerStage / 2,
			tasksCount:           maxNodesPerStage/2 + 1,
			expectError:          true,
		},
		{
			name:        "invalid stage with no nested pipelines or tasks",
			stageName:   "dev",
			expectError: true,
		},
		{
			name:        "invalid stage name",
			stageName:   "invalid name",
			expectError: true,
		},
		{
			name:        "stage name is a reserved keyword",
			stageName:   "stage",
			expectError: true,
		},
		{
			name:        "stage name is a reserved keyword",
			stageName:   "pre",
			expectError: true,
		},
		{
			name:        "stage name is a reserved keyword",
			stageName:   "post",
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			stage := &PipelineStage{
				Name: tc.stageName,
			}

			for i := 0; i < tc.tasksCount; i++ {
				stage.Tasks = append(stage.Tasks, &PipelineTask{
					Name: fmt.Sprintf("confirm-%d", i),
				})
			}

			for i := 0; i < tc.nestedPipelinesCount; i++ {
				stage.NestedPipelines = append(stage.NestedPipelines, &NestedPipeline{
					Name:         fmt.Sprintf("nested-%d", i),
					PipelineType: ptr.String("nested"),
					TemplateID:   hcl.StaticExpr(cty.StringVal("template_id = \"id\""), hcl.Range{Start: hcl.Pos{Byte: 1}, End: hcl.Pos{Byte: 5}}),
				})
			}

			err := stage.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipeline_Validate(t *testing.T) {
	type testCase struct {
		name        string
		stages      int
		expectError bool
	}

	testCases := []testCase{
		{
			name:   "valid with only one stage",
			stages: 1,
		},
		{
			name:   "valid with maximum allowed stages",
			stages: MaximumAllowedNodes - 1,
		},
		{
			name:        "invalid with no stages",
			expectError: true,
		},
		{
			name:        "invalid with too many stages",
			stages:      MaximumAllowedNodes + 1,
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipeline := &PipelineTemplate{}

			for i := 0; i < tc.stages; i++ {
				stageName := fmt.Sprintf("stage-%d", i)
				pipeline.StageOrder = append(pipeline.StageOrder, stageName)
				pipeline.Stages = append(pipeline.Stages, &PipelineStage{
					Name: stageName,
					Tasks: []*PipelineTask{
						{
							Name: fmt.Sprintf("confirm-%d", i),
						},
					},
				})
			}

			err := pipeline.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPluginRequirement_Validate(t *testing.T) {
	type testCase struct {
		plugin      *PluginRequirement
		name        string
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid with only shorthand source",
			plugin: &PluginRequirement{
				Source: "neutron/gitlab",
			},
		},
		{
			name: "source has three parts",
			plugin: &PluginRequirement{
				Source: "registry.tld/neutron/gitlab",
			},
		},
		{
			name: "valid with source and version",
			plugin: &PluginRequirement{
				Source:  "neutron/gitlab",
				Version: ptr.String("1.0.0"),
			},
		},
		{
			name: "source has less than two parts",
			plugin: &PluginRequirement{
				Source: "neutron",
			},
			expectError: true,
		},
		{
			name: "source has more than three parts",
			plugin: &PluginRequirement{
				Source: "registry.tld/neutron/gitlab/invalid",
			},
			expectError: true,
		},
		{
			name: "invalid source hostname",
			plugin: &PluginRequirement{
				Source: "invalid0-/neutron/gitlab",
			},
			expectError: true,
		},
		{
			name: "invalid source organization",
			plugin: &PluginRequirement{
				Source: "neutron/invalid name/gitlab",
			},
			expectError: true,
		},
		{
			name: "invalid source name",
			plugin: &PluginRequirement{
				Source: "neutron/invalid name",
			},
			expectError: true,
		},
		{
			name: "invalid version",
			plugin: &PluginRequirement{
				Source:  "neutron/gitlab",
				Version: ptr.String("invalid version"),
			},
			expectError: true,
		},
		{
			name: "no source",
			plugin: &PluginRequirement{
				Version: ptr.String("1.0.0"),
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.plugin.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPluginRequirement_IsLocal(t *testing.T) {
	// Local
	req := &PluginRequirement{Replace: ptr.String("/some/path")}
	assert.True(t, req.IsLocal())

	// Remote
	req = &PluginRequirement{}
	assert.False(t, req.IsLocal())
}

func TestPlugins_Requirements(t *testing.T) {
	type testCase struct {
		attributes   map[string]cty.Value
		expectParsed map[string]PluginRequirement
		name         string
		expectError  bool
	}

	testCases := []testCase{
		{
			name: "valid attributes",
			attributes: map[string]cty.Value{
				"gitlab": cty.ObjectVal(map[string]cty.Value{
					"source": cty.StringVal("neutron/gitlab"),
				}),
				"aws": cty.ObjectVal(map[string]cty.Value{
					"source":  cty.StringVal("neutron/aws"),
					"version": cty.StringVal("1.0.0"),
				}),
			},
			expectParsed: map[string]PluginRequirement{
				"gitlab": {
					Source: "neutron/gitlab",
				},
				"aws": {
					Source:  "neutron/aws",
					Version: ptr.String("1.0.0"),
				},
			},
		},
		{
			name: "not an object",
			attributes: map[string]cty.Value{
				"gitlab": cty.StringVal("neutron/gitlab"),
			},
			expectError: true,
		},
		{
			name: "attribute value is not a string",
			attributes: map[string]cty.Value{
				"gitlab": cty.ObjectVal(map[string]cty.Value{
					"source": cty.NumberIntVal(1),
				}),
			},
			expectError: true,
		},
		{
			name: "not a valid plugin source",
			attributes: map[string]cty.Value{
				"gitlab": cty.ObjectVal(map[string]cty.Value{
					"source": cty.StringVal("invalid source"),
				}),
			},
			expectError: true,
		},
		{
			name: "not a valid plugin version",
			attributes: map[string]cty.Value{
				"gitlab": cty.ObjectVal(map[string]cty.Value{
					"source":  cty.StringVal("neutron/gitlab"),
					"version": cty.StringVal("invalid version"),
				}),
			},
			expectError: true,
		},
		{
			name: "invalid plugin requirement key",
			attributes: map[string]cty.Value{
				"gitlab": cty.ObjectVal(map[string]cty.Value{
					"invalid": cty.StringVal("neutron/gitlab"),
				}),
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			plugins := &PluginRequirements{
				Attributes: tc.attributes,
			}

			actualParsed, err := plugins.Requirements()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)

			for name, plugin := range tc.expectParsed {
				require.Contains(t, actualParsed, name)
				assert.Equal(t, plugin, actualParsed[name])
			}
		})
	}
}

func TestVariable_Validate(t *testing.T) {
	type testCase struct {
		name         string
		variableName string
		expectError  bool
	}

	testCases := []testCase{
		{
			name:         "valid variable",
			variableName: "ports",
		},
		{
			name:         "invalid variable name",
			variableName: "invalid name",
			expectError:  true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			variable := &Variable{
				Name: tc.variableName,
			}

			err := variable.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestJWT_Validate(t *testing.T) {
	type testCase struct {
		name        string
		jwtName     string
		expectError bool
	}

	testCases := []testCase{
		{
			name:    "valid jwt",
			jwtName: "gitlab",
		},
		{
			name:        "invalid jwt name",
			jwtName:     "github invalid",
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			jwt := &JWT{
				Name: tc.jwtName,
			}

			err := jwt.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestVCSToken_Validate(t *testing.T) {
	type testCase struct {
		name        string
		tokenName   string
		expectError bool
	}

	testCases := []testCase{
		{
			name:      "valid vcs token",
			tokenName: "gitlab",
		},
		{
			name:        "invalid vcs token name",
			tokenName:   "github invalid",
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			token := &VCSToken{
				Name: tc.tokenName,
			}

			err := token.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipelineTemplate_Validate(t *testing.T) {
	type testCase struct {
		template    *PipelineTemplate
		name        string
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid pipeline template",
			template: &PipelineTemplate{
				Variables: []*Variable{
					{
						Name: "ports",
					},
				},
				JSONWebTokens: []*JWT{
					{
						Name: "gitlab",
					},
				},
				StageOrder: []string{"dev"},
				Stages: []*PipelineStage{
					{
						Name: "dev",
						Tasks: []*PipelineTask{
							{
								Name: "confirm",
							},
						},
					},
				},
			},
		},
		{
			name:        "missing pipeline block",
			template:    &PipelineTemplate{},
			expectError: true,
		},
		{
			name: "duplicate stage",
			template: &PipelineTemplate{
				StageOrder: []string{"dev"},
				Stages: []*PipelineStage{
					{
						Name: "dev",
						Tasks: []*PipelineTask{
							{
								Name: "test",
							},
						},
					},
					{
						Name: "dev",
						Tasks: []*PipelineTask{
							{
								Name: "deploy",
							},
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "stage has duplicate tasks",
			template: &PipelineTemplate{
				StageOrder: []string{"dev"},
				Stages: []*PipelineStage{
					{
						Name: "dev",
						Tasks: []*PipelineTask{
							{
								Name: "test",
							},
							{
								Name: "test",
							},
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "task and nested pipeline names must be unique within a stage",
			template: &PipelineTemplate{
				StageOrder: []string{"dev"},
				Stages: []*PipelineStage{
					{
						Name: "dev",
						Tasks: []*PipelineTask{
							{
								Name: "test",
							},
						},
						NestedPipelines: []*NestedPipeline{
							{
								Name: "test",
							},
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "pipeline condition has duplicate task",
			template: &PipelineTemplate{
				StageOrder: []string{"dev"},
				Stages: []*PipelineStage{
					{
						Name: "dev",
						PreTasks: &PipelineCondition{
							Tasks: []*PipelineTask{
								{
									Name: "scan",
								},
								{
									Name: "scan",
								},
							},
						},
						Tasks: []*PipelineTask{
							{
								Name: "test",
							},
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "task cannot depend on an unknown task",
			template: &PipelineTemplate{
				StageOrder: []string{"dev"},
				Stages: []*PipelineStage{
					{
						Name: "dev",
						Tasks: []*PipelineTask{
							{
								Name:         "scan",
								Dependencies: []string{"not-known"},
							},
							{
								Name: "clean",
							},
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "circular dependencies are not allowed",
			template: &PipelineTemplate{
				StageOrder: []string{"dev"},
				Stages: []*PipelineStage{
					{
						Name: "dev",
						Tasks: []*PipelineTask{
							{
								Name: "execute",
							},
							{
								Name:         "scan",
								Dependencies: []string{"fix"},
							},
							{
								Name:         "fix",
								Dependencies: []string{"clean"},
							},
							{
								Name:         "clean",
								Dependencies: []string{"scan"},
							},
						},
					},
				},
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.template.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestPipelineTemplate_GetPlugins(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	require.NotEmpty(t, pipelineTemplate.JSONWebTokens)
	assert.Len(t, pipelineTemplate.GetPlugins(), len(pipelineTemplate.Plugins))
}

func TestPipelineTemplate_GetVariables(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	require.NotEmpty(t, pipelineTemplate.JSONWebTokens)
	assert.Len(t, pipelineTemplate.Variables, len(pipelineTemplate.Variables))
}

func TestPipelineTemplate_GetJSONWebTokens(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	require.NotEmpty(t, pipelineTemplate.JSONWebTokens)
	assert.Len(t, pipelineTemplate.GetJSONWebTokens(), len(pipelineTemplate.JSONWebTokens))
}

func TestPipelineTemplate_GetVCSTokens(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	require.NotEmpty(t, pipelineTemplate.JSONWebTokens)
	assert.Len(t, pipelineTemplate.GetVCSTokens(), len(pipelineTemplate.VCSTokens))
}

func TestPipelineTemplate_GetVolumes(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	require.NotEmpty(t, pipelineTemplate.Volumes)
	for _, volume := range pipelineTemplate.Volumes {
		actual, ok := pipelineTemplate.GetVolume(volume.Name)
		require.True(t, ok)
		require.NotNil(t, actual)
	}
}

func TestPipelineTemplate_BuildStateMachine(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	stateMachine, err := pipelineTemplate.BuildStateMachine()
	require.NoError(t, err)
	require.NotNil(t, stateMachine)

	// Get the nodes so we can check how many were parsed.
	nodes, err := stateMachine.GetNodes(&statemachine.GetNodesInput{})
	require.NoError(t, err)

	assert.Len(t, nodes, 13)
}

func TestPipelineTemplate_GetAction(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	type testCase struct {
		name        string
		actionPath  string
		expectFound bool
	}

	testCases := []testCase{
		{
			name:        "valid pre task action",
			actionPath:  "stage.dev.pre.task.check_for_changes.action.find_changes",
			expectFound: true,
		},
		{
			name:        "valid post task action",
			actionPath:  "stage.dev.post.task.cleanup_resources.action.perform_cleanup",
			expectFound: true,
		},
		{
			name:        "valid task action",
			actionPath:  "stage.dev.task.execute_a_command.action.perform_action",
			expectFound: true,
		},
		{
			name:       "invalid pre task action",
			actionPath: "stage.dev.pre.task.check_for_changes.action.invalid",
		},
		{
			name:       "invalid post task action",
			actionPath: "stage.dev.post.task.cleanup_resources.action.invalid",
		},
		{
			name:       "invalid task action",
			actionPath: "stage.dev.task.execute_a_command.action.invalid",
		},
		{
			name:       "not an action",
			actionPath: "stage.dev.pre.task.check_for_changes",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			action, found := pipelineTemplate.GetAction(test.actionPath)
			require.Equal(t, test.expectFound, found)

			if test.expectFound {
				require.NotNil(t, action)
			} else {
				assert.Nil(t, action)
			}
		})
	}
}

func TestPipelineTemplate_GetTask(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	type testCase struct {
		name        string
		taskPath    string
		expectFound bool
	}

	testCases := []testCase{
		{
			name:        "valid pre task",
			taskPath:    "stage.dev.pre.task.check_for_changes",
			expectFound: true,
		},
		{
			name:        "valid post task",
			taskPath:    "stage.dev.post.task.cleanup_resources",
			expectFound: true,
		},
		{
			name:        "valid task",
			taskPath:    "stage.dev.task.execute_a_command",
			expectFound: true,
		},
		{
			name:     "invalid pre task",
			taskPath: "stage.dev.pre.task.invalid",
		},
		{
			name:     "invalid post task",
			taskPath: "stage.dev.post.task.invalid",
		},
		{
			name:     "invalid task",
			taskPath: "stage.dev.task.invalid",
		},
		{
			name:     "not a task",
			taskPath: "stage.dev.pre.task.check_for_changes.action.find_changes",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			task, found := pipelineTemplate.GetTask(test.taskPath)
			require.Equal(t, test.expectFound, found)

			if test.expectFound {
				require.NotNil(t, task)
			} else {
				assert.Nil(t, task)
			}
		})
	}
}

func TestPipelineTemplate_GetNestedPipeline(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	type testCase struct {
		name         string
		pipelinePath string
		expectFound  bool
	}

	testCases := []testCase{
		{
			name:         "valid pipeline",
			pipelinePath: "stage.dev.pipeline.deploy",
			expectFound:  true,
		},
		{
			name:         "invalid pipeline",
			pipelinePath: "stage.dev.pipeline.invalid",
		},
		{
			name:         "not a pipeline",
			pipelinePath: "stage.dev.pre.task.check_for_changes.action.find_changes",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipeline, found := pipelineTemplate.GetNestedPipeline(test.pipelinePath)
			require.Equal(t, test.expectFound, found)

			if test.expectFound {
				require.NotNil(t, pipeline)
			} else {
				assert.Nil(t, pipeline)
			}
		})
	}
}

func TestPipelineTemplate_GetPipelineNode(t *testing.T) {
	pipelineTemplate, _, err := loadPipelineTemplate("valid_full.hcl", nil)
	require.NoError(t, err)

	type testCase struct {
		expectNodeType any
		name           string
		path           string
		expectFound    bool
	}

	testCases := []testCase{
		{
			name:           "valid stage",
			path:           "stage.dev",
			expectNodeType: &PipelineStage{},
			expectFound:    true,
		},
		{
			name:           "valid pre task",
			path:           "stage.dev.pre.task.check_for_changes",
			expectNodeType: &PipelineTask{},
			expectFound:    true,
		},
		{
			name:           "valid pre task action",
			path:           "stage.dev.pre.task.check_for_changes.action.find_changes",
			expectNodeType: &PipelineAction{},
			expectFound:    true,
		},
		{
			name:           "valid post task",
			path:           "stage.dev.post.task.cleanup_resources",
			expectNodeType: &PipelineTask{},
			expectFound:    true,
		},
		{
			name:           "valid post task action",
			path:           "stage.dev.post.task.cleanup_resources.action.perform_cleanup",
			expectNodeType: &PipelineAction{},
			expectFound:    true,
		},
		{
			name:           "valid task",
			path:           "stage.dev.task.execute_a_command",
			expectNodeType: &PipelineTask{},
			expectFound:    true,
		},
		{
			name:           "valid task action",
			path:           "stage.dev.task.execute_a_command.action.perform_action",
			expectNodeType: &PipelineAction{},
			expectFound:    true,
		},
		{
			name: "invalid action",
			path: "stage.dev.task.execute_a_command.action.invalid",
		},
		{
			name: "invalid task",
			path: "stage.dev.task.invalid",
		},
		{
			name: "invalid stage",
			path: "stage.invalid",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			node, found := pipelineTemplate.GetPipelineNode(tc.path)
			require.Equal(t, tc.expectFound, found)

			if tc.expectFound {
				require.NotNil(t, node)
				assert.IsType(t, tc.expectNodeType, node)
			} else {
				assert.Nil(t, node)
			}
		})
	}
}

func TestPipelineTemplate_NewPipelineTemplate(t *testing.T) {

	type testCase struct {
		vars     map[string]string
		f        func(t *testing.T, template *PipelineTemplate)
		name     string
		filename string
	}

	testCases := []testCase{
		{
			name:     "valid full template",
			filename: "valid_full.hcl",
			f: func(t *testing.T, template *PipelineTemplate) {
				require.Len(t, template.Variables, 3)
				require.Len(t, template.JSONWebTokens, 1)
				require.Len(t, template.Stages, 1)
				require.Len(t, template.Stages[0].Tasks, 2)
				require.Len(t, template.Stages[0].Tasks[0].Actions, 1)
				require.Len(t, template.Stages[0].Tasks[1].Actions, 1)
				require.NotNil(t, template.Stages[0].PostTasks)
				require.Len(t, template.Stages[0].PostTasks.Tasks, 1)
				require.Len(t, template.Stages[0].PostTasks.Tasks[0].Actions, 1)
				require.NotNil(t, template.Stages[0].PreTasks)
				require.Len(t, template.Stages[0].PreTasks.Tasks, 1)
				require.Len(t, template.Stages[0].PreTasks.Tasks[0].Actions, 1)
			},
		},
		{
			name:     "valid template with dynamic blocks",
			filename: "valid_dynamic.hcl",
			vars: map[string]string{
				"tasks": "[\"task1\", \"task2\"]",
			},
			f: func(t *testing.T, template *PipelineTemplate) {
				require.Len(t, template.Stages, 1)
				require.Len(t, template.Stages[0].Tasks, 2)
				require.Len(t, template.Stages[0].Tasks[0].Actions, 1)
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipelineTemplate, buf, err := loadPipelineTemplate(test.filename, test.vars)
			require.NoError(t, err)

			// Lightly verify all nodes were parsed
			require.NotEmpty(t, buf)
			require.NotNil(t, pipelineTemplate)
			require.NotNil(t, pipelineTemplate.Stages)

			test.f(t, pipelineTemplate)
		})
	}
}

// loadPipelineTemplate loads a pipeline template from the testdata directory.
func loadPipelineTemplate(filename string, variables map[string]string, opts ...Option) (*PipelineTemplate, []byte, error) {
	template := fmt.Sprintf("../../../testdata/hcl/pipeline/%s", filename)

	f, err := os.Open(template)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to load pipeline template: %w", err)
	}

	defer f.Close()

	pipelineTemplate, buf, err := NewPipelineTemplate(f, variables, opts...)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to load pipeline template: %w", err)
	}

	return pipelineTemplate, buf, nil
}
