package config

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/hashicorp/cronexpr"
	"github.com/hashicorp/hcl/v2"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

const (
	// MaximumAllowedNodes is the maximum number of nodes allowed in an HCL config
	MaximumAllowedNodes = 10
	// minTaskInterval is the minimum interval between task events
	minTaskInterval = time.Minute
	// maxAgentTags is the maximum number of agent tags allowed
	maximumAgentTags = 20
	// maxAgentTagLength is the max length of agent tags
	maxAgentTagLength = 50
	// maxNodesPerStage is the maximum number of numbers allowed per stage
	maxNodesPerStage = 50
	// maxTaskImageLength is the maximum length of the task image
	maxTaskImageLength = 256
)

// NameRegex allows letters, numbers with - and _ allowed in non leading or trailing positions, max length is 64
var NameRegex = regexp.MustCompile("^[0-9a-z](?:[0-9a-z-_]{0,62}[0-9a-z])?$")

// reservedKeywords is a list of reserved names (HCL keywords) that cannot be used in the config.
var reservedKeywords = map[string]struct{}{
	"pipeline":   {},
	"task":       {},
	"action":     {},
	"stage":      {},
	"deployment": {},
	"pre":        {},
	"post":       {},
}

// PipelineNodeScheduleType is the type of schedule for a pipeline node
type PipelineNodeScheduleType string

// PipelineNodeScheduleType constants
const (
	CronScheduleType     PipelineNodeScheduleType = "cron"
	DatetimeScheduleType PipelineNodeScheduleType = "datetime"
)

// CronScheduleOptions represents the options for a cron schedule
type CronScheduleOptions struct {
	Timezone   *string
	Expression string
}

// Validate validates the cron schedule options
func (c *CronScheduleOptions) Validate() error {
	// Verify that the cron expression is valid
	if _, err := cronexpr.Parse(c.Expression); err != nil {
		return fmt.Errorf("%s pattern in schedule is invalid %s: %v", CronScheduleType, c.Expression, err)
	}

	if len(strings.Split(c.Expression, " ")) > 5 {
		return fmt.Errorf("%s pattern in schedule is invalid: only 5 fields are allowed (minutes, hours, day of month, month, day of week)", CronScheduleType)
	}

	if c.Timezone != nil {
		if *c.Timezone == "" {
			return fmt.Errorf("timezone in %s schedule cannot be empty", CronScheduleType)
		}

		if *c.Timezone == "Local" {
			return fmt.Errorf("timezone in %s schedule cannot be Local", CronScheduleType)
		}

		// Verify that the timezone is valid
		_, err := time.LoadLocation(*c.Timezone)
		if err != nil {
			return fmt.Errorf("timezone in %s schedule is invalid %s: %v", CronScheduleType, *c.Timezone, err)
		}
	}

	return nil
}

// DatetimeScheduleOptions represents the options for a datetime schedule
type DatetimeScheduleOptions struct {
	Timestamp time.Time
}

// PipelineNodeSchedule represents the schedule for a pipeline node
type PipelineNodeSchedule struct {
	Options map[string]string        `hcl:"options,attr"`
	Type    PipelineNodeScheduleType `hcl:"type,attr"`
}

// GetCronOptions returns the cron options for the schedule
func (s *PipelineNodeSchedule) GetCronOptions() (*CronScheduleOptions, error) {
	if s.Type != CronScheduleType {
		return nil, fmt.Errorf("schedule type is not %s", CronScheduleType)
	}

	exp, ok := s.Options["expression"]
	if !ok {
		return nil, fmt.Errorf("%s schedule type must include the expression option", CronScheduleType)
	}

	options := &CronScheduleOptions{
		Expression: exp,
	}

	tz, ok := s.Options["timezone"]
	if ok {
		options.Timezone = &tz
	}

	if (len(s.Options) == 2 && options.Timezone == nil) || len(s.Options) > 2 {
		return nil, fmt.Errorf("%s schedule type must only include the expression and timezone options", CronScheduleType)
	}

	return options, nil
}

// GetDatetimeOptions returns the datetime options for the schedule
func (s *PipelineNodeSchedule) GetDatetimeOptions() (*DatetimeScheduleOptions, error) {
	if s.Type != DatetimeScheduleType {
		return nil, fmt.Errorf("schedule type is not %s", DatetimeScheduleType)
	}

	timestamp, ok := s.Options["value"]
	if !ok {
		return nil, fmt.Errorf("%s schedule type must include the value option", DatetimeScheduleType)
	}

	parsedTimestamp, err := time.Parse(time.RFC3339, timestamp)
	if err != nil || parsedTimestamp.IsZero() {
		return nil, fmt.Errorf("%s schedule type value is invalid %s: must be a valid datetime in RFC3339 format: %v", DatetimeScheduleType, timestamp, err)
	}

	options := &DatetimeScheduleOptions{
		Timestamp: parsedTimestamp,
	}

	if len(s.Options) > 1 {
		return nil, fmt.Errorf("%s schedule type must only include the value option", DatetimeScheduleType)
	}

	return options, nil
}

// Validate validates the schedule
func (s *PipelineNodeSchedule) Validate() error {
	switch s.Type {
	case CronScheduleType:
		options, err := s.GetCronOptions()
		if err != nil {
			return err
		}

		if err := options.Validate(); err != nil {
			return err
		}
	case DatetimeScheduleType:
		if _, err := s.GetDatetimeOptions(); err != nil {
			return err
		}
	default:
		return fmt.Errorf("invalid schedule type: %s", s.Type)
	}

	return nil
}

// HCLConfig is an interface that represents a HCL config
type HCLConfig interface {
	Validate() error
	BuildStateMachine() (*statemachine.StateMachine, error)
	GetPlugins() []*Plugin
	GetVariableBodies() map[string]hcl.Body
	GetAction(path string) (*PipelineAction, bool)
	GetTask(path string) (*PipelineTask, bool)
	GetNestedPipeline(path string) (*NestedPipeline, bool)
	GetJSONWebTokens() []*JWT
	GetVCSTokens() []*VCSToken
}
