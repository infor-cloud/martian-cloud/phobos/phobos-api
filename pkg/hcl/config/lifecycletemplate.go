package config

import (
	"errors"
	"fmt"
	"io"
	"strings"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/zclconf/go-cty/cty"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

var _ HCLConfig = (*LifecycleTemplate)(nil)

// Deployment represents a deployment of a release (a pipeline)
type Deployment struct {
	If              hcl.Expression        `hcl:"if,optional"`
	ApprovalRules   hcl.Expression        `hcl:"approval_rules,optional"`
	When            *string               `hcl:"when,optional"`
	OnError         *string               `hcl:"on_error,optional"`
	Schedule        *PipelineNodeSchedule `hcl:"schedule,block"`
	EnvironmentName string                `hcl:"environment"`
	Dependencies    []string              `hcl:"dependencies,optional"`
}

// Validate validates the deployment
func (d *Deployment) Validate() error {
	if !NameRegex.MatchString(d.EnvironmentName) {
		return errors.New("invalid environment name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	if _, ok := reservedKeywords[d.EnvironmentName]; ok {
		return fmt.Errorf("environment name %q is a reserved keyword", d.EnvironmentName)
	}

	if d.When != nil {
		switch *d.When {
		case "auto", "manual", "never":
		default:
			return fmt.Errorf("invalid deployment when value %q, must be auto or manual", *d.When)
		}
	}

	if d.OnError != nil {
		switch *d.OnError {
		case "fail", "continue":
		default:
			return fmt.Errorf("invalid on_error value %q, must be fail or continue", *d.OnError)
		}
	}

	if d.Schedule != nil {
		if err := d.Schedule.Validate(); err != nil {
			return err
		}
		if d.When == nil || *d.When != "auto" {
			return errors.New("'when' field must be set to 'auto' when using a schedule")
		}
	}

	return nil
}

// LifecycleStage is a stage in a lifecycle
type LifecycleStage struct {
	PreTasks    *PipelineCondition `hcl:"pre,block"`
	PostTasks   *PipelineCondition `hcl:"post,block"`
	Name        string             `hcl:",label"`
	Deployments []*Deployment      `hcl:"deployment,block"`
}

// Validate validates the lifecycle stage
func (s *LifecycleStage) Validate() error {
	if !NameRegex.MatchString(s.Name) {
		return errors.New("invalid stage name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	if _, ok := reservedKeywords[s.Name]; ok {
		return fmt.Errorf("stage name %q is a reserved keyword", s.Name)
	}

	if l := len(s.Deployments); l == 0 || l > maxNodesPerStage {
		return fmt.Errorf("number of deployments must be between 1 and %d", maxNodesPerStage)
	}

	deploymentDependencyGraph := map[string][]string{}
	for _, deployment := range s.Deployments {
		if err := deployment.Validate(); err != nil {
			return err
		}

		deploymentDependencyGraph[deployment.EnvironmentName] = deployment.Dependencies
	}

	if err := validateDependencies(deploymentDependencyGraph); err != nil {
		return fmt.Errorf("stage %q has invalid deployment dependencies: %w", s.Name, err)
	}

	if s.PreTasks != nil {
		if err := s.PreTasks.Validate(); err != nil {
			return fmt.Errorf("stage %q has invalid pre tasks: %w", s.Name, err)
		}
	}

	if s.PostTasks != nil {
		if err := s.PostTasks.Validate(); err != nil {
			return fmt.Errorf("stage %q has invalid post tasks: %w", s.Name, err)
		}
	}

	return nil
}

// LifecycleTemplate is an HCL definition of a lifecycle
type LifecycleTemplate struct {
	Body               hcl.Body            `hcl:",body"`
	Stages             []*LifecycleStage   `hcl:"stage,block"`
	StageOrder         []string            `hcl:"stage_order,optional"`
	Variables          []*Variable         `hcl:"variable,block"`
	Plugins            []*Plugin           `hcl:"plugin,block"`
	PluginRequirements *PluginRequirements `hcl:"plugin_requirements,block"`
	JSONWebTokens      []*JWT              `hcl:"jwt,block"`
	Volumes            []*Volume           `hcl:"volume,block"`
	VCSTokens          []*VCSToken         `hcl:"vcs_token,block"`
}

// Validate validates the lifecycle template
func (t *LifecycleTemplate) Validate() error {
	for _, variable := range t.Variables {
		if err := variable.Validate(); err != nil {
			return err
		}
	}

	for _, jwt := range t.JSONWebTokens {
		if err := jwt.Validate(); err != nil {
			return err
		}
	}

	for _, vcsToken := range t.VCSTokens {
		if err := vcsToken.Validate(); err != nil {
			return err
		}
	}

	plugins := map[string]struct{}{}
	for _, plugin := range t.Plugins {
		if _, ok := plugins[plugin.Name]; ok {
			return fmt.Errorf("plugin block for %q is defined more than once", plugin.Name)
		}

		plugins[plugin.Name] = struct{}{}
	}

	if t.PluginRequirements != nil {
		if _, err := t.PluginRequirements.Requirements(); err != nil {
			return err
		}
	}

	seenVolumes := map[string]struct{}{}
	for ix, volume := range t.Volumes {
		if _, ok := seenVolumes[volume.Name]; ok {
			return fmt.Errorf("volume block %q defined more than once", volume.Name)
		}

		seenVolumes[volume.Name] = struct{}{}
		if vErr := volume.Validate(); vErr != nil {
			return vErr
		}

		if ix == MaximumAllowedNodes {
			return fmt.Errorf("volumes exceeds a limit of %d", MaximumAllowedNodes)
		}
	}

	if l := len(t.Stages); l == 0 || l > MaximumAllowedNodes {
		return fmt.Errorf("number of stages must be between 1 and %d", MaximumAllowedNodes)
	}

	if t.StageOrder != nil && len(t.StageOrder) != len(t.Stages) {
		return errors.New("either a stage is missing from the 'stage_order' list or an unknown stage name was included")
	}

	// Must de-duplicate since the same stage cannot be
	// sequenced more than once.
	seenSequence := map[string]struct{}{}
	for _, stageName := range t.StageOrder {
		if _, ok := seenSequence[stageName]; ok {
			return fmt.Errorf("stage %q is listed more than once within the sequence", stageName)
		}
		seenSequence[stageName] = struct{}{}
	}

	seenStage := map[string]struct{}{}
	seenEnvironments := map[string]struct{}{}
	for _, stage := range t.Stages {
		if err := stage.Validate(); err != nil {
			return err
		}

		if _, ok := seenStage[stage.Name]; ok {
			return fmt.Errorf("stage %q is declared more than once", stage.Name)
		}
		seenStage[stage.Name] = struct{}{}

		if t.StageOrder != nil {
			if _, ok := seenSequence[stage.Name]; !ok {
				return fmt.Errorf("stage %q is missing from the 'stages' sequence list", stage.Name)
			}
		}

		// Verify each deployment is for a unique environment since it
		// wouldn't make sense to deploy to the same environment twice.
		for _, d := range stage.Deployments {
			if _, ok := seenEnvironments[d.EnvironmentName]; ok {
				return fmt.Errorf("environment names must be unique across deployments, %q is duplicated", d.EnvironmentName)
			}

			seenEnvironments[d.EnvironmentName] = struct{}{}
		}

		// Ensure unknown volumes aren't being supplied for mount points.
		if stage.PreTasks != nil {
			for _, task := range stage.PreTasks.Tasks {
				for _, mp := range task.MountPoints {
					if _, ok := seenVolumes[mp.Volume]; !ok {
						return fmt.Errorf("mount point for stage %q pre task %q is using an unknown volume %q",
							stage.Name,
							task.Name,
							mp.Volume,
						)
					}
				}
			}
		}
		if stage.PostTasks != nil {
			for _, task := range stage.PostTasks.Tasks {
				for _, mp := range task.MountPoints {
					if _, ok := seenVolumes[mp.Volume]; !ok {
						return fmt.Errorf("mount point for stage %q post task %q is using an unknown volume %q",
							stage.Name,
							task.Name,
							mp.Volume,
						)
					}
				}
			}
		}
	}

	return nil
}

// GetVariableBodies returns the HCL body for each variable definition
func (t *LifecycleTemplate) GetVariableBodies() map[string]hcl.Body {
	response := map[string]hcl.Body{}
	for _, v := range t.Variables {
		response[v.Name] = v.Body
	}
	return response
}

// GetPlugins returns the plugins defined in the lifecycle template
func (t *LifecycleTemplate) GetPlugins() []*Plugin {
	return t.Plugins
}

// GetJSONWebTokens returns the JWTs defined in the lifecycle template
func (t *LifecycleTemplate) GetJSONWebTokens() []*JWT {
	return t.JSONWebTokens
}

// GetEnvironmentNames returns the environment names defined in the lifecycle template
func (t *LifecycleTemplate) GetEnvironmentNames() []string {
	environmentNames := []string{}
	for _, stage := range t.Stages {
		for _, deployment := range stage.Deployments {
			environmentNames = append(environmentNames, deployment.EnvironmentName)
		}
	}
	return environmentNames
}

// GetStageOrder returns the specified stage order or the default ordering of the stages.
func (t *LifecycleTemplate) GetStageOrder() []string {
	if len(t.StageOrder) > 0 {
		return t.StageOrder
	}

	stageOrder := []string{}
	for _, stage := range t.Stages {
		stageOrder = append(stageOrder, stage.Name)
	}

	return stageOrder
}

// GetVCSTokens returns the VCS tokens defined in the lifecycle template
func (t *LifecycleTemplate) GetVCSTokens() []*VCSToken {
	return t.VCSTokens
}

// BuildStateMachine builds the lifecycle state machine.
func (t *LifecycleTemplate) BuildStateMachine() (*statemachine.StateMachine, error) {
	createdStatus := statemachine.CreatedNodeStatus

	pipelineNode := statemachine.NewPipelineNode(&statemachine.NewPipelineNodeInput{
		Path:   "lifecycle",
		Status: createdStatus,
	})

	// Build a map of stages, so we can look it up easier.
	stageMap := map[string]*LifecycleStage{}
	for _, stage := range t.Stages {
		stageMap[stage.Name] = stage
	}

	for _, stageName := range t.GetStageOrder() {
		stage, ok := stageMap[stageName]
		if !ok {
			return nil, fmt.Errorf("failed to find stage %q while building state machine", stageName)
		}

		if stage.PreTasks != nil {
			stageName := fmt.Sprintf("%s.pre", stage.Name)
			preStageNode := statemachine.NewStageNode(fmt.Sprintf("lifecycle.stage.%s", stageName), pipelineNode, createdStatus)
			for _, task := range stage.PreTasks.Tasks {
				taskNode := statemachine.NewTaskNode(&statemachine.NewTaskNodeInput{
					Path:   fmt.Sprintf("lifecycle.stage.%s.task.%s", stageName, task.Name),
					Parent: preStageNode,
					Status: &createdStatus,
				})

				for _, action := range task.Actions {
					actionNode := statemachine.NewActionNode(
						fmt.Sprintf("lifecycle.stage.%s.task.%s.action.%s", stageName, task.Name, action.Name()),
						taskNode,
						createdStatus,
					)
					taskNode.AddActionNode(actionNode)
				}
				preStageNode.AddTaskNode(taskNode)
			}
			pipelineNode.AddStageNode(preStageNode)
		}

		stageNode := statemachine.NewStageNode(fmt.Sprintf("lifecycle.stage.%s", stage.Name), pipelineNode, createdStatus)

		for _, deployment := range stage.Deployments {
			nestedPipelineNode := statemachine.NewPipelineNode(&statemachine.NewPipelineNodeInput{
				Path:   fmt.Sprintf("lifecycle.stage.%s.pipeline.%s", stage.Name, deployment.EnvironmentName),
				Status: createdStatus,
				Parent: stageNode,
			})
			stageNode.AddNestedPipelineNode(nestedPipelineNode)
		}

		pipelineNode.AddStageNode(stageNode)

		if stage.PostTasks != nil {
			stageName := fmt.Sprintf("%s.post", stage.Name)
			postStageNode := statemachine.NewStageNode(fmt.Sprintf("lifecycle.stage.%s", stageName), pipelineNode, createdStatus)
			for _, task := range stage.PostTasks.Tasks {
				taskNode := statemachine.NewTaskNode(&statemachine.NewTaskNodeInput{
					Path:   fmt.Sprintf("lifecycle.stage.%s.task.%s", stageName, task.Name),
					Parent: postStageNode,
					Status: &createdStatus,
				})

				for _, action := range task.Actions {
					actionNode := statemachine.NewActionNode(
						fmt.Sprintf("lifecycle.stage.%s.task.%s.action.%s", stageName, task.Name, action.Name()),
						taskNode,
						createdStatus,
					)
					taskNode.AddActionNode(actionNode)
				}
				postStageNode.AddTaskNode(taskNode)
			}
			pipelineNode.AddStageNode(postStageNode)
		}
	}

	return statemachine.New(pipelineNode), nil
}

// GetTask returns a lifecycle task based on the provided path
func (t *LifecycleTemplate) GetTask(path string) (*PipelineTask, bool) {
	node, ok := t.GetLifecycleNode(path)
	if !ok {
		return nil, false
	}
	task, ok := node.(*PipelineTask)
	if !ok {
		return nil, false
	}
	return task, true
}

// GetAction returns a lifecycle action based on the provided path.
func (t *LifecycleTemplate) GetAction(path string) (*PipelineAction, bool) {
	node, ok := t.GetLifecycleNode(path)
	if !ok {
		return nil, false
	}
	action, ok := node.(*PipelineAction)
	if !ok {
		return nil, false
	}
	return action, true
}

// GetNestedPipeline returns a lifecycle pipeline (deployment) based on the provided path.
func (t *LifecycleTemplate) GetNestedPipeline(path string) (*NestedPipeline, bool) {
	node, ok := t.GetLifecycleNode(path)
	if !ok {
		return nil, false
	}
	pipeline, ok := node.(*NestedPipeline)
	if !ok {
		return nil, false
	}
	return pipeline, true
}

// GetLifecycleNode returns a lifecycle node based on the path.
func (t *LifecycleTemplate) GetLifecycleNode(path string) (interface{}, bool) {
	var curNodeType string
	var curNode interface{}

	// Example path: lifecycle.stage.s1.pre.task.t1.action.a1
	parts := strings.Split(path, ".")
	for idx, part := range parts {
		switch part {
		case "pre", "post":
			curNodeType = part
		case "stage", "task", "action", "pipeline":
			curNodeType = part
			continue
		}

		isLast := idx == len(parts)-1

		switch curNodeType {
		case "stage":
			// Find stage with name
			for _, stage := range t.Stages {
				stageCopy := stage
				if stage.Name == part {
					if isLast {
						return stageCopy, true
					}
					curNode = stageCopy
					break
				}
			}
		case "pre":
			stage := curNode.(*LifecycleStage)
			curNode = stage.PreTasks
		case "post":
			stage := curNode.(*LifecycleStage)
			curNode = stage.PostTasks
		case "task":
			condition := curNode.(*PipelineCondition)
			for _, task := range condition.Tasks {
				taskCopy := task
				if task.Name == part {
					if isLast {
						return taskCopy, true
					}
					curNode = taskCopy
					break
				}
			}
		case "pipeline":
			stage := curNode.(*LifecycleStage)
			for _, deployment := range stage.Deployments {
				pipeline := &NestedPipeline{
					Name:            fmt.Sprintf("deployment_%s", deployment.EnvironmentName),
					When:            deployment.When,
					ApprovalRules:   deployment.ApprovalRules,
					EnvironmentName: &deployment.EnvironmentName,
					PipelineType:    ptr.String("deployment"),
					Dependencies:    deployment.Dependencies,
					If:              deployment.If,
					OnError:         deployment.OnError,
					Schedule:        deployment.Schedule,
				}

				if deployment.EnvironmentName == part {
					if isLast {
						return pipeline, true
					}
					curNode = pipeline
					break
				}
			}
		case "action":
			task := curNode.(*PipelineTask)
			for _, action := range task.Actions {
				actionCopy := action
				if action.Name() == part {
					if isLast {
						return actionCopy, true
					}
					curNode = actionCopy
					break
				}
			}
		}
	}
	return nil, false
}

// NewLifecycleTemplate creates a new lifecycle template
func NewLifecycleTemplate(reader io.Reader) (*LifecycleTemplate, []byte, error) {
	buf, err := io.ReadAll(reader)
	if err != nil {
		return nil, nil, err
	}

	// Parse config
	file, diags := hclsyntax.ParseConfig(buf, "lifecycle.hcl", hcl.Pos{Line: 0, Column: 0})
	if diags.HasErrors() {
		return nil, nil, diags
	}

	// Get top-level schema
	schema, _ := gohcl.ImpliedBodySchema(&PipelineTemplate{})

	// Parse contents using schema
	content, diags := file.Body.Content(schema)
	if diags.HasErrors() {
		return nil, nil, diags
	}

	// Get variable HCL bodies
	variableBodies := map[string]hcl.Body{}
	for _, block := range content.Blocks {
		if block.Type == "variable" {
			variableBodies[block.Labels[0]] = block.Body
		}
	}

	// Convert HCL variables to cty values
	// TODO: Update this once lifecycles support input variables.
	ctyValues, err := variable.GetCtyValuesForVariables(variableBodies, nil, true)
	if err != nil {
		return nil, nil, err
	}

	// Create HCL context
	evalCtx := hclctx.New(map[string]cty.Value{
		"var": cty.ObjectVal(ctyValues),
	})

	var hclConfig LifecycleTemplate
	if err := hclsimple.Decode("lifecycle.hcl", buf, evalCtx.Context(), &hclConfig); err != nil {
		return nil, nil, err
	}

	if err := hclConfig.Validate(); err != nil {
		return nil, nil, err
	}

	return &hclConfig, buf, nil
}
