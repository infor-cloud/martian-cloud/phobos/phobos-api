// Package config contains the HCL config structs
package config

import (
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-version"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/ext/dynblock"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	svchost "github.com/hashicorp/terraform-svchost"
	"github.com/zclconf/go-cty/cty"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

var _ HCLConfig = (*PipelineTemplate)(nil)

// VCSOptions are the configuration options for a vcs volume type.
type VCSOptions struct {
	Ref            *string `hcl:"ref,optional"`
	RepositoryPath string  `hcl:"repository_path,attr"`
	ProviderID     string  `hcl:"provider_id,attr"`
}

// Volume allows mounting directories from external file systems
// like a VCS provider into the pipeline.
type Volume struct {
	VCSOptions *VCSOptions `hcl:"vcs_options,block"`
	Type       string      `hcl:"type,attr"`
	Name       string      `hcl:",label"`
}

// Validate validates the volume.
func (v *Volume) Validate() error {
	if !NameRegex.MatchString(v.Name) {
		return errors.New("invalid volume name, names can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	switch v.Type {
	case "vcs":
		if v.VCSOptions == nil {
			return errors.New("a 'vcs_options' block is required when using a vcs volume type")
		}
	default:
		return fmt.Errorf("%q volume type is not supported", v.Type)
	}

	return nil
}

// MountPoint defines the location where a Volume will be mounted
// within a pipeline construct.
type MountPoint struct {
	Path   *string `hcl:"path,optional"`
	Volume string  `hcl:"volume,attr"`
}

// Agent represents configuration related to an agent
type Agent struct {
	Tags []string `hcl:"tags,optional"`
}

// Validate validates the agent
func (p *Agent) Validate() error {
	if len(p.Tags) > maximumAgentTags {
		return fmt.Errorf("agent tags exceeds max limit of %d", maximumAgentTags)
	}

	// Validate tags
	tagMap := map[string]struct{}{}
	for _, tag := range p.Tags {
		if _, ok := tagMap[tag]; ok {
			return fmt.Errorf("duplicate tag values are not allowed: tag %q has been duplicated", tag)
		}
		tagMap[tag] = struct{}{}

		if len(tag) > maxAgentTagLength {
			return fmt.Errorf("tag %q exceeds max length of %d", tag, maxAgentTagLength)
		}
	}

	return nil
}

// PipelineAction represents an action that will be executed using a plugin
type PipelineAction struct {
	Alias  *string  `hcl:"alias,optional"`
	Body   hcl.Body `hcl:",body"`
	Remain hcl.Body `hcl:",remain"`
	Label  string   `hcl:",label"`
}

// Validate validates the pipeline action
func (p *PipelineAction) Validate() error {
	if p.Alias != nil {
		if !NameRegex.MatchString(*p.Alias) {
			return errors.New("invalid action alias, alias can only include lowercase letters and numbers with - and _ supported " +
				"in non leading or trailing positions. Max length is 64 characters")
		}

		if _, ok := reservedKeywords[*p.Alias]; ok {
			return fmt.Errorf("action alias %q is a reserved keyword", *p.Alias)
		}
	}

	if !strings.Contains(p.Label, "_") {
		return errors.New("invalid plugin action ID, must be in the format <plugin-name>_<action-name>")
	}

	return nil
}

// Name returns the action name
func (p *PipelineAction) Name() string {
	if p.Alias != nil {
		return *p.Alias
	}
	return p.Label
}

// PluginName returns the plugin name for this action
func (p *PipelineAction) PluginName() string {
	parts := strings.SplitN(p.Label, "_", 2)
	return parts[0]
}

// PluginActionName returns the action name within the plugin
func (p *PipelineAction) PluginActionName() string {
	parts := strings.SplitN(p.Label, "_", 2)
	return parts[1]
}

// NestedPipeline represents a nested pipeline
type NestedPipeline struct {
	Variables       hcl.Expression        `hcl:"variables,optional"`
	TemplateID      hcl.Expression        `hcl:"template_id"`
	ApprovalRules   hcl.Expression        `hcl:"approval_rules,optional"`
	If              hcl.Expression        `hcl:"if,optional"`
	EnvironmentName *string               `hcl:"environment,optional"`
	When            *string               `hcl:"when,optional"`
	PipelineType    *string               `hcl:"pipeline_type,optional"`
	OnError         *string               `hcl:"on_error,optional"`
	Schedule        *PipelineNodeSchedule `hcl:"schedule,block"`
	Name            string                `hcl:",label"`
	Dependencies    []string              `hcl:"dependencies,optional"`
}

// Validate validates the nested pipeline
func (p *NestedPipeline) Validate() error {
	if !NameRegex.MatchString(p.Name) {
		return errors.New("invalid nested pipeline name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	if _, ok := reservedKeywords[p.Name]; ok {
		return fmt.Errorf("nested pipeline name %q is a reserved keyword", p.Name)
	}

	if p.EnvironmentName != nil && !NameRegex.MatchString(*p.EnvironmentName) {
		return errors.New("invalid environment name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	if p.TemplateID == nil || p.TemplateID.Range().Empty() {
		return errors.New("nested pipeline must have a 'template_id' attribute")
	}

	pipelineType := "nested"
	if p.PipelineType != nil {
		pipelineType = *p.PipelineType
	}

	if pipelineType != "deployment" && pipelineType != "nested" {
		return fmt.Errorf("invalid pipeline type %q, must be nested or deployment", pipelineType)
	}

	if pipelineType == "deployment" && p.EnvironmentName == nil {
		return errors.New("deployment pipeline must have an 'environment' attribute")
	}

	if pipelineType == "nested" && p.EnvironmentName != nil {
		return errors.New("nested pipeline must not have an 'environment' attribute; only deployment pipelines can have an environment")
	}

	if p.When != nil {
		switch *p.When {
		case "auto", "manual", "never":
		default:
			return fmt.Errorf("invalid nested pipeline when value %q, must be auto or manual", *p.When)
		}
	}

	if p.OnError != nil {
		switch *p.OnError {
		case "fail", "continue":
		default:
			return fmt.Errorf("invalid on_error value %q, must be fail or continue", *p.OnError)
		}
	}

	if p.Schedule != nil {
		if err := p.Schedule.Validate(); err != nil {
			return err
		}
		if p.When == nil {
			p.When = ptr.String("auto")
		}
		if *p.When != "auto" {
			return errors.New("'when' field must be set to 'auto' when using a schedule")
		}
	}

	return nil
}

// PipelineTask defines a unit of work to be performed within a pipeline
type PipelineTask struct {
	SuccessCondition hcl.Expression        `hcl:"success_condition,optional"`
	ApprovalRules    hcl.Expression        `hcl:"approval_rules,optional"`
	If               hcl.Expression        `hcl:"if,optional"`
	Agent            *Agent                `hcl:"agent,block"`
	Interval         *string               `hcl:"interval,optional"`
	Attempts         *int                  `hcl:"attempts,optional"`
	Image            *string               `hcl:"image,optional"`
	When             *string               `hcl:"when,optional"`
	OnError          *string               `hcl:"on_error,optional"`
	Schedule         *PipelineNodeSchedule `hcl:"schedule,block"`
	Name             string                `hcl:",label"`
	Dependencies     []string              `hcl:"dependencies,optional"`
	MountPoints      []*MountPoint         `hcl:"mount_point,block"`
	Actions          []*PipelineAction     `hcl:"action,block"`
}

// Validate validates the pipeline task
func (p *PipelineTask) Validate() error {
	if !NameRegex.MatchString(p.Name) {
		return fmt.Errorf("invalid task name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions (max length is 64 characters)")
	}

	if _, ok := reservedKeywords[p.Name]; ok {
		return fmt.Errorf("task name %q is a reserved keyword", p.Name)
	}

	if p.Attempts != nil && *p.Attempts < 1 {
		return fmt.Errorf("attempts must be greater than 0")
	}

	if p.Interval != nil {
		if p.Attempts == nil {
			return fmt.Errorf("attempts is required if interval is set")
		}
		parsedDuration, err := time.ParseDuration(*p.Interval)
		if err != nil {
			return fmt.Errorf("invalid interval %q, must be a valid duration", *p.Interval)
		}
		if parsedDuration.Seconds() < minTaskInterval.Seconds() {
			return fmt.Errorf("interval must be greater than %q", minTaskInterval)
		}
	}

	if p.When != nil {
		switch *p.When {
		case "auto", "manual", "never":
		default:
			return fmt.Errorf("invalid pipeline task when condition %q, must be auto, manual or never", *p.When)
		}
	}

	if len(p.Actions) > MaximumAllowedNodes {
		return fmt.Errorf("maximum number of actions allowed is %d", MaximumAllowedNodes)
	}

	actionNames := map[string]interface{}{}
	for _, action := range p.Actions {
		if err := action.Validate(); err != nil {
			return err
		}

		// Detect duplicate action names
		if _, ok := actionNames[action.Name()]; ok {
			return fmt.Errorf(
				"task %q has a duplicate action name %q. If multiple actions of the same type are declared then an alias must be specified",
				p.Name,
				action.Name(),
			)
		}
		actionNames[action.Name()] = nil
	}

	if p.Agent != nil {
		if err := p.Agent.Validate(); err != nil {
			return err
		}
	}

	mountPointNames := map[string]struct{}{}
	for _, mp := range p.MountPoints {
		if _, ok := mountPointNames[mp.Volume]; ok {
			return fmt.Errorf("task %q is mounting volume %q more than once", p.Name, mp.Volume)
		}
		mountPointNames[mp.Volume] = struct{}{}
	}

	if p.Image != nil && len(*p.Image) > maxTaskImageLength {
		return fmt.Errorf("task %q image exceeds max length of %d", p.Name, maxTaskImageLength)
	}

	if p.OnError != nil {
		switch *p.OnError {
		case "fail", "continue":
		default:
			return fmt.Errorf("invalid on_error value %q, must be fail or continue", *p.OnError)
		}
	}

	if p.Schedule != nil {
		if err := p.Schedule.Validate(); err != nil {
			return err
		}
		if p.When == nil {
			p.When = ptr.String("auto")
		}
		if *p.When != "auto" {
			return errors.New("'when' field must be set to 'auto' when using a schedule")
		}
	}

	return nil
}

// PipelineCondition represents a condition that must be met for a pipeline task to be executed (pre/post)
type PipelineCondition struct {
	Tasks []*PipelineTask `hcl:"task,block"`
}

// Validate validates the pipeline condition
func (p *PipelineCondition) Validate() error {
	if l := len(p.Tasks); l == 0 || l > MaximumAllowedNodes {
		return fmt.Errorf("number of pre or post tasks must be between 1 and %d", MaximumAllowedNodes)
	}

	taskDependencyGraph := map[string][]string{}
	for _, task := range p.Tasks {
		if err := task.Validate(); err != nil {
			return err
		}
		if _, ok := taskDependencyGraph[task.Name]; ok {
			return fmt.Errorf("pre or post condition has task %q declared more than once; task name must be unique within a condition", task.Name)
		}
		taskDependencyGraph[task.Name] = task.Dependencies
	}

	if err := validateDependencies(taskDependencyGraph); err != nil {
		return fmt.Errorf("pre or post condition has a task with an invalid dependency: %w", err)
	}

	return nil
}

// PipelineStage is a stage in a pipeline
type PipelineStage struct {
	NestedPipelines []*NestedPipeline  `hcl:"pipeline,block"`
	PreTasks        *PipelineCondition `hcl:"pre,block"`
	PostTasks       *PipelineCondition `hcl:"post,block"`
	Name            string             `hcl:",label"`
	Tasks           []*PipelineTask    `hcl:"task,block"`
}

// Validate validates the pipeline stage
func (p *PipelineStage) Validate() error {
	if !NameRegex.MatchString(p.Name) {
		return errors.New("invalid stage name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	if _, ok := reservedKeywords[p.Name]; ok {
		return fmt.Errorf("stage name %q is a reserved keyword", p.Name)
	}

	if p.PreTasks != nil {
		if err := p.PreTasks.Validate(); err != nil {
			return fmt.Errorf("stage %q has invalid pre tasks: %w", p.Name, err)
		}
	}

	if p.PostTasks != nil {
		if err := p.PostTasks.Validate(); err != nil {
			return fmt.Errorf("stage %q has invalid post tasks: %w", p.Name, err)
		}
	}

	if (len(p.Tasks) + len(p.NestedPipelines)) > maxNodesPerStage {
		return fmt.Errorf("maximum number of tasks and nested pipelines allowed per stage is %d", maxNodesPerStage)
	}

	if len(p.Tasks) == 0 && len(p.NestedPipelines) == 0 {
		return errors.New("stage must have at least one task or nested pipeline")
	}

	childDependencyGraph := map[string][]string{}
	for _, task := range p.Tasks {
		if err := task.Validate(); err != nil {
			return fmt.Errorf("stage %q has invalid task: %w", p.Name, err)
		}
		if _, ok := childDependencyGraph[task.Name]; ok {
			return fmt.Errorf("stage %q has task %q with a conflicting name; task and nested pipeline names must be unique within a stage", p.Name, task.Name)
		}
		childDependencyGraph[task.Name] = task.Dependencies
	}

	for _, nestedPipeline := range p.NestedPipelines {
		if err := nestedPipeline.Validate(); err != nil {
			return fmt.Errorf("stage %q has invalid nested pipeline: %w", p.Name, err)
		}
		if _, ok := childDependencyGraph[nestedPipeline.Name]; ok {
			return fmt.Errorf("stage %q has nested pipeline %q with a conflicting name; task and nested pipeline names must be unique within a stage", p.Name, nestedPipeline.Name)
		}
		childDependencyGraph[nestedPipeline.Name] = nestedPipeline.Dependencies
	}

	if err := validateDependencies(childDependencyGraph); err != nil {
		return fmt.Errorf("stage %q has task or nested pipeline with an invalid dependency: %w", p.Name, err)
	}

	return nil
}

// Plugin represents a plugin that is used in the pipeline
type Plugin struct {
	Body   hcl.Body `hcl:",body"`
	Remain hcl.Body `hcl:",remain"`
	Name   string   `hcl:",label"`
}

// PluginRequirement represents a required plugin
type PluginRequirement struct {
	Replace *string
	Version *string
	Source  string
}

// Validate validates a required plugin
func (p *PluginRequirement) Validate() error {
	if p.Version != nil {
		if _, err := version.NewConstraint(*p.Version); err != nil {
			return fmt.Errorf("invalid version constraint: %w", err)
		}
	}

	parts := strings.Split(p.Source, "/")
	length := len(parts)

	if length < 2 || length > 3 {
		return errors.New("invalid source, must be in the format <organization>/<name> or <hostname>/<organization>/<name> if using a custom registry")
	}

	if !NameRegex.MatchString(parts[length-1]) {
		return errors.New("invalid source name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	if !NameRegex.MatchString(parts[length-2]) {
		return errors.New("invalid source organization, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	if length == 3 && !svchost.IsValid(parts[0]) {
		return fmt.Errorf("invalid source hostname %q", parts[0])
	}

	return nil
}

// IsLocal returns true if the plugin is being used from a local source.
func (p *PluginRequirement) IsLocal() bool {
	// Replace being set means we're using it from a local directory.
	return p.Replace != nil
}

// PluginRequirements is a map of required plugins.
type PluginRequirements struct {
	Attributes map[string]cty.Value `hcl:",remain"`
}

// Requirements returns the plugins block as a map for easy access.
func (p *PluginRequirements) Requirements() (map[string]PluginRequirement, error) {
	result := map[string]PluginRequirement{}
	for pluginName, pluginReqs := range p.Attributes {
		if !pluginReqs.Type().IsObjectType() {
			return nil, fmt.Errorf("invalid attribute for plugin %q, must be an object", pluginName)
		}

		pluginReq := PluginRequirement{}
		for key, value := range pluginReqs.AsValueMap() {
			if !value.Type().Equals(cty.String) {
				return nil, fmt.Errorf("invalid value for attribute %q for plugin %q, must be a string", key, pluginName)
			}

			switch key {
			case "version":
				pluginReq.Version = ptr.String(value.AsString())
			case "source":
				pluginReq.Source = value.AsString()
			case "replace":
				pluginReq.Replace = ptr.String(value.AsString())
			default:
				return nil, fmt.Errorf("invalid attribute %q for plugin %q", key, pluginName)
			}
		}

		if err := pluginReq.Validate(); err != nil {
			return nil, fmt.Errorf("invalid plugin %q: %v", pluginName, err)
		}

		result[pluginName] = pluginReq
	}

	return result, nil
}

// Variable represents a pipeline variable
type Variable struct {
	Body    hcl.Body       `hcl:",body"`
	Type    hcl.Expression `hcl:"type,optional"`
	Default hcl.Expression `hcl:"default,optional"`
	Name    string         `hcl:",label"`
}

// Validate validates a variable
func (v *Variable) Validate() error {
	if !NameRegex.MatchString(v.Name) {
		return errors.New("invalid variable name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	return nil
}

// JWT represents a pipeline JSON Web Token
type JWT struct {
	Name     string `hcl:",label"`
	Audience string `hcl:"audience,attr"`
}

// Validate validates a JWT
func (j *JWT) Validate() error {
	if !NameRegex.MatchString(j.Name) {
		return errors.New("invalid JWT name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	return nil
}

// VCSToken represents a VCS token
type VCSToken struct {
	Name       string `hcl:",label"`
	ProviderID string `hcl:"provider_id,attr"`
}

// Validate validates a VCS token
func (v *VCSToken) Validate() error {
	if !NameRegex.MatchString(v.Name) {
		return errors.New("invalid VCS token name, name can only include lowercase letters and numbers with - and _ supported " +
			"in non leading or trailing positions. Max length is 64 characters")
	}

	return nil
}

// PipelineTemplate is an HCL definition of a pipeline
type PipelineTemplate struct {
	Body               hcl.Body            `hcl:",body"`
	Stages             []*PipelineStage    `hcl:"stage,block"`
	StageOrder         []string            `hcl:"stage_order,optional"`
	Agent              *Agent              `hcl:"agent,block"`
	Variables          []*Variable         `hcl:"variable,block"`
	Plugins            []*Plugin           `hcl:"plugin,block"`
	PluginRequirements *PluginRequirements `hcl:"plugin_requirements,block"`
	JSONWebTokens      []*JWT              `hcl:"jwt,block"`
	Volumes            []*Volume           `hcl:"volume,block"`
	VCSTokens          []*VCSToken         `hcl:"vcs_token,block"`
}

// Validate validates the pipeline template
func (pc *PipelineTemplate) Validate() error {
	for _, variable := range pc.Variables {
		if err := variable.Validate(); err != nil {
			return err
		}
	}

	for _, jwt := range pc.JSONWebTokens {
		if err := jwt.Validate(); err != nil {
			return err
		}
	}

	for _, vcsToken := range pc.VCSTokens {
		if err := vcsToken.Validate(); err != nil {
			return err
		}
	}

	if pc.Agent != nil {
		if err := pc.Agent.Validate(); err != nil {
			return err
		}
	}

	plugins := map[string]struct{}{}
	for _, p := range pc.Plugins {
		if _, ok := plugins[p.Name]; ok {
			return fmt.Errorf("plugin block for %q is defined more than once", p.Name)
		}

		plugins[p.Name] = struct{}{}
	}

	if pc.PluginRequirements != nil {
		if _, err := pc.PluginRequirements.Requirements(); err != nil {
			return err
		}
	}

	seenVolumes := map[string]struct{}{}
	for ix, volume := range pc.Volumes {
		if _, ok := seenVolumes[volume.Name]; ok {
			return fmt.Errorf("volume block %q defined more than once", volume.Name)
		}

		seenVolumes[volume.Name] = struct{}{}
		if vErr := volume.Validate(); vErr != nil {
			return vErr
		}

		if ix == MaximumAllowedNodes {
			return fmt.Errorf("volumes exceeds a limit of %d", MaximumAllowedNodes)
		}
	}

	if l := len(pc.Stages); l == 0 || l > MaximumAllowedNodes {
		return fmt.Errorf("number of stages must be between 1 and %d", MaximumAllowedNodes)
	}

	if pc.StageOrder != nil && len(pc.StageOrder) != len(pc.Stages) {
		return errors.New("either a stage is missing from the 'stage_order' list or an unknown stage name was included")
	}

	// Convert the stage sequence to a map for easier lookup.
	seenSequence := map[string]struct{}{}
	for _, stageName := range pc.StageOrder {
		if _, ok := seenSequence[stageName]; ok {
			return fmt.Errorf("stage %q is listed more than once within the sequence", stageName)
		}
		seenSequence[stageName] = struct{}{}
	}

	seenStage := map[string]struct{}{}
	for _, stage := range pc.Stages {
		if err := stage.Validate(); err != nil {
			return err
		}

		if _, ok := seenStage[stage.Name]; ok {
			return fmt.Errorf("stage %q is declared more than once", stage.Name)
		}
		seenStage[stage.Name] = struct{}{}

		if pc.StageOrder != nil {
			if _, ok := seenSequence[stage.Name]; !ok {
				return fmt.Errorf("stage %q is missing from the 'stage_order' order list", stage.Name)
			}
		}

		// Ensure unknown volumes aren't being supplied for mount points.
		for _, task := range stage.Tasks {
			for _, mp := range task.MountPoints {
				if _, ok := seenVolumes[mp.Volume]; !ok {
					return fmt.Errorf("mount point for stage %q task %q is using an unknown volume %q",
						stage.Name,
						task.Name,
						mp.Volume,
					)
				}
			}
		}
	}

	return nil
}

// GetVariableBodies returns the HCL body for each variable definition
func (pc *PipelineTemplate) GetVariableBodies() map[string]hcl.Body {
	response := map[string]hcl.Body{}
	for _, v := range pc.Variables {
		response[v.Name] = v.Body
	}
	return response
}

// GetPlugins returns the plugins defined in the pipeline template
func (pc *PipelineTemplate) GetPlugins() []*Plugin {
	return pc.Plugins
}

// GetJSONWebTokens returns the JWTs defined in the pipeline template
func (pc *PipelineTemplate) GetJSONWebTokens() []*JWT {
	return pc.JSONWebTokens
}

// GetVolume returns the specified volume by name.
func (pc *PipelineTemplate) GetVolume(name string) (*Volume, bool) {
	for _, volume := range pc.Volumes {
		if volume.Name == name {
			return volume, true
		}
	}

	return nil, false
}

// GetStageOrder returns the specified stage order or the default ordering of the stages.
func (pc *PipelineTemplate) GetStageOrder() []string {
	if len(pc.StageOrder) > 0 {
		return pc.StageOrder
	}

	stages := []string{}
	for _, stage := range pc.Stages {
		stages = append(stages, stage.Name)
	}

	return stages
}

// GetVCSTokens returns the VCS tokens defined in the pipeline template
func (pc *PipelineTemplate) GetVCSTokens() []*VCSToken {
	return pc.VCSTokens
}

// BuildStateMachine builds a state machine based on the pipeline template
func (pc *PipelineTemplate) BuildStateMachine() (*statemachine.StateMachine, error) {
	createdStatus := statemachine.CreatedNodeStatus

	pipelineNode := statemachine.NewPipelineNode(&statemachine.NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})

	// Build a map of stages, so we can look it up easier.
	stageMap := map[string]*PipelineStage{}
	for _, stage := range pc.Stages {
		stageMap[stage.Name] = stage
	}

	for _, stageName := range pc.GetStageOrder() {
		stage, ok := stageMap[stageName]
		if !ok {
			return nil, fmt.Errorf("failed to find stage %q while building state machine", stageName)
		}

		if stage.PreTasks != nil {
			stageName := fmt.Sprintf("%s.pre", stage.Name)
			preStageNode := statemachine.NewStageNode(fmt.Sprintf("pipeline.stage.%s", stageName), pipelineNode, createdStatus)
			for _, task := range stage.PreTasks.Tasks {
				taskNode := statemachine.NewTaskNode(&statemachine.NewTaskNodeInput{
					Path:         fmt.Sprintf("pipeline.stage.%s.task.%s", stageName, task.Name),
					Parent:       preStageNode,
					Dependencies: task.Dependencies,
					Status:       &createdStatus,
				})

				for _, action := range task.Actions {
					actionNode := statemachine.NewActionNode(
						fmt.Sprintf("pipeline.stage.%s.task.%s.action.%s", stageName, task.Name, action.Name()),
						taskNode,
						createdStatus,
					)
					taskNode.AddActionNode(actionNode)
				}
				preStageNode.AddTaskNode(taskNode)
			}
			pipelineNode.AddStageNode(preStageNode)
		}

		stageNode := statemachine.NewStageNode(fmt.Sprintf("pipeline.stage.%s", stage.Name), pipelineNode, createdStatus)

		for _, nestedPipeline := range stage.NestedPipelines {
			nestedPipelineNode := statemachine.NewPipelineNode(&statemachine.NewPipelineNodeInput{
				Path:         fmt.Sprintf("pipeline.stage.%s.pipeline.%s", stage.Name, nestedPipeline.Name),
				Status:       createdStatus,
				Parent:       stageNode,
				Dependencies: nestedPipeline.Dependencies,
			})
			stageNode.AddNestedPipelineNode(nestedPipelineNode)
		}

		for _, task := range stage.Tasks {
			taskNode := statemachine.NewTaskNode(&statemachine.NewTaskNodeInput{
				Path:         fmt.Sprintf("pipeline.stage.%s.task.%s", stage.Name, task.Name),
				Parent:       stageNode,
				Status:       &createdStatus,
				Dependencies: task.Dependencies,
			})

			for _, action := range task.Actions {
				actionNode := statemachine.NewActionNode(
					fmt.Sprintf("pipeline.stage.%s.task.%s.action.%s", stage.Name, task.Name, action.Name()),
					taskNode,
					createdStatus,
				)
				taskNode.AddActionNode(actionNode)
			}
			stageNode.AddTaskNode(taskNode)
		}

		pipelineNode.AddStageNode(stageNode)

		if stage.PostTasks != nil {
			stageName := fmt.Sprintf("%s.post", stage.Name)
			postStageNode := statemachine.NewStageNode(fmt.Sprintf("pipeline.stage.%s", stageName), pipelineNode, createdStatus)
			for _, task := range stage.PostTasks.Tasks {
				taskNode := statemachine.NewTaskNode(&statemachine.NewTaskNodeInput{
					Path:         fmt.Sprintf("pipeline.stage.%s.task.%s", stageName, task.Name),
					Parent:       postStageNode,
					Status:       &createdStatus,
					Dependencies: task.Dependencies,
				})

				for _, action := range task.Actions {
					actionNode := statemachine.NewActionNode(
						fmt.Sprintf("pipeline.stage.%s.task.%s.action.%s", stageName, task.Name, action.Name()),
						taskNode,
						createdStatus,
					)
					taskNode.AddActionNode(actionNode)
				}
				postStageNode.AddTaskNode(taskNode)
			}
			pipelineNode.AddStageNode(postStageNode)
		}
	}

	return statemachine.New(pipelineNode), nil
}

// GetAction returns a pipeline action based on the provided path
func (pc *PipelineTemplate) GetAction(path string) (*PipelineAction, bool) {
	node, ok := pc.GetPipelineNode(path)
	if !ok {
		return nil, false
	}
	action, ok := node.(*PipelineAction)
	if !ok {
		return nil, false
	}
	return action, true
}

// GetTask returns a pipeline task based on the provided path
func (pc *PipelineTemplate) GetTask(path string) (*PipelineTask, bool) {
	node, ok := pc.GetPipelineNode(path)
	if !ok {
		return nil, false
	}
	task, ok := node.(*PipelineTask)
	if !ok {
		return nil, false
	}
	return task, true
}

// GetNestedPipeline returns a nested pipeline based on the provided path
func (pc *PipelineTemplate) GetNestedPipeline(path string) (*NestedPipeline, bool) {
	node, ok := pc.GetPipelineNode(path)
	if !ok {
		return nil, false
	}
	nestedPipeline, ok := node.(*NestedPipeline)
	if !ok {
		return nil, false
	}
	return nestedPipeline, true
}

// GetPipelineNode returns a pipeline node based on the provided path
func (pc *PipelineTemplate) GetPipelineNode(path string) (interface{}, bool) {
	var curNodeType string
	var curNode interface{}

	// Example path: stage.s1.task.t1.action.a1
	parts := strings.Split(path, ".")
	for idx, part := range parts {
		switch part {
		case "pre", "post":
			curNodeType = part
		case "stage", "task", "action", "pipeline":
			curNodeType = part
			continue
		}

		isLast := idx == len(parts)-1

		switch curNodeType {
		case "stage":
			// Find stage with name
			for _, stage := range pc.Stages {
				stageCopy := stage
				if stage.Name == part {
					if isLast {
						return stageCopy, true
					}
					curNode = stageCopy
					break
				}
			}
		case "pre":
			stage := curNode.(*PipelineStage)
			curNode = stage.PreTasks
		case "post":
			stage := curNode.(*PipelineStage)
			curNode = stage.PostTasks
		case "pipeline":
			stage := curNode.(*PipelineStage)
			for _, nestedPipeline := range stage.NestedPipelines {
				nestedPipelineCopy := nestedPipeline
				if nestedPipeline.Name == part {
					if isLast {
						return nestedPipelineCopy, true
					}
					curNode = nestedPipelineCopy
					break
				}
			}
		case "task":
			switch typed := curNode.(type) {
			case *PipelineStage:
				for _, task := range typed.Tasks {
					taskCopy := task
					if task.Name == part {
						if isLast {
							return taskCopy, true
						}
						curNode = taskCopy
						break
					}
				}
			case *PipelineCondition:
				for _, task := range typed.Tasks {
					taskCopy := task
					if task.Name == part {
						if isLast {
							return taskCopy, true
						}
						curNode = taskCopy
						break
					}
				}
			}
		case "action":
			task := curNode.(*PipelineTask)
			for _, action := range task.Actions {
				actionCopy := action
				if action.Name() == part {
					if isLast {
						return actionCopy, true
					}
					curNode = actionCopy
					break
				}
			}
		}
	}
	return nil, false
}

type options struct {
	unknownVars bool
}

// Option for creating a template
type Option func(*options)

// WithUnknownVars will use a cty unknown type for any variables that are not defined
func WithUnknownVars() Option {
	return func(o *options) {
		o.unknownVars = true
	}
}

// NewPipelineTemplate creates a new pipeline template
func NewPipelineTemplate(reader io.Reader, variables map[string]string, opts ...Option) (*PipelineTemplate, []byte, error) {
	// Set options
	options := &options{}
	for _, o := range opts {
		o(options)
	}

	// Read contents
	buf, err := io.ReadAll(reader)
	if err != nil {
		return nil, nil, err
	}

	// Parse config
	file, diags := hclsyntax.ParseConfig(buf, "pipeline.hcl", hcl.Pos{Line: 0, Column: 0})
	if diags.HasErrors() {
		return nil, nil, diags
	}

	// Get top-level schema
	schema, _ := gohcl.ImpliedBodySchema(&PipelineTemplate{})

	// Parse contents using schema
	content, diags := file.Body.Content(schema)
	if diags.HasErrors() {
		return nil, nil, diags
	}

	// Get variable HCL bodies
	variableBodies := map[string]hcl.Body{}
	for _, block := range content.Blocks {
		if block.Type == "variable" {
			variableBodies[block.Labels[0]] = block.Body
		}
	}

	// Convert HCL variables to cty values
	ctyValues, err := variable.GetCtyValuesForVariables(variableBodies, variables, options.unknownVars)
	if err != nil {
		return nil, nil, err
	}

	// Create HCL context
	evalCtx := hclctx.New(map[string]cty.Value{
		"var": cty.ObjectVal(ctyValues),
	})

	// Expand dynamic blocks
	expandedBody := dynblock.Expand(file.Body, evalCtx.Context())

	// Decode expanded template
	var hclConfig PipelineTemplate
	if diag := gohcl.DecodeBody(expandedBody, evalCtx.Context(), &hclConfig); diag.HasErrors() {
		return nil, nil, diag
	}

	// Validate template
	if err := hclConfig.Validate(); err != nil {
		return nil, nil, err
	}

	return &hclConfig, buf, nil
}

// validateDependencies validates dependencies for a stage child node (task or nested pipeline).
func validateDependencies(dependencyGraph map[string][]string) error {
	for nodeName := range dependencyGraph {
		visitedNodes := map[string]struct{}{}
		hasCycle, err := hasDependencyCycle(nodeName, visitedNodes, dependencyGraph)
		if err != nil {
			return err
		}

		if hasCycle {
			return errors.New("dependency cycle found")
		}
	}

	return nil
}

// hasDependencyCycle determines if node dependency graph has a cycle (A -> B -> C -> A).
func hasDependencyCycle(nodeName string, visited map[string]struct{}, dependencyGraph map[string][]string) (bool, error) {
	if _, ok := visited[nodeName]; ok {
		// Node was already visited, this is a cycle.
		return true, nil
	}

	visited[nodeName] = struct{}{}

	dependencies, ok := dependencyGraph[nodeName]
	if !ok {
		// Likely the user specified an unknown node.
		return false, fmt.Errorf("%q not found", nodeName)
	}

	for _, dependency := range dependencies {
		hasCycle, err := hasDependencyCycle(dependency, visited, dependencyGraph)
		if err != nil {
			return false, err
		}

		if hasCycle {
			return hasCycle, nil
		}
	}

	return false, nil
}
