package config

import (
	"fmt"
	"os"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

func TestLifecycleDeployment_Validate(t *testing.T) {
	lowerFailOnError := "fail"
	lowerContinueOnError := "continue"

	type testCase struct {
		deployment  *Deployment
		name        string
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid",
			deployment: &Deployment{
				EnvironmentName: "dev",
			},
		},
		{
			name: "when auto",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("auto"),
			},
		},
		{
			name: "when manual",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("manual"),
			},
		},
		{
			name: "valid cron schedule",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("auto"),
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * *",
					},
				},
			},
		},
		{
			name: "when cannot be manual when schedule is set",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("manual"),
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * *",
					},
				},
			},
			expectError: true,
		},
		{
			name: "valid datetime schedule",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("auto"),
				Schedule: &PipelineNodeSchedule{
					Type: DatetimeScheduleType,
					Options: map[string]string{
						"value": "2024-11-25T14:50:00.000Z",
					},
				},
			},
		},
		{
			name: "invalid cron schedule",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("auto"),
				Schedule: &PipelineNodeSchedule{
					Type: CronScheduleType,
					Options: map[string]string{
						"expression": "* * * * * *",
					},
				},
			},
			expectError: true,
		},
		{
			name: "invalid datetime schedule",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("auto"),
				Schedule: &PipelineNodeSchedule{
					Type: DatetimeScheduleType,
					Options: map[string]string{
						"value": "1733238119",
					},
				},
			},
			expectError: true,
		},
		{
			name: "invalid schedule type",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("auto"),
				Schedule: &PipelineNodeSchedule{
					Type: PipelineNodeScheduleType("invalid"),
				},
			},
			expectError: true,
		},
		{
			name: "invalid environment name",
			deployment: &Deployment{
				EnvironmentName: "invalid name",
			},
			expectError: true,
		},
		{
			name: "invalid when value",
			deployment: &Deployment{
				EnvironmentName: "dev",
				When:            ptr.String("invalid"),
			},
			expectError: true,
		},
		{
			name: "valid OnError fail",
			deployment: &Deployment{
				EnvironmentName: "dev",
				OnError:         &lowerFailOnError,
			},
		},
		{
			name: "valid OnError continue",
			deployment: &Deployment{
				EnvironmentName: "dev",
				OnError:         &lowerContinueOnError,
			},
		},
		{
			name: "invalid OnError value",
			deployment: &Deployment{
				EnvironmentName: "dev",
				OnError:         ptr.String("invalid"),
			},
			expectError: true,
		},
		{
			name: "environment name is a reserved keyword",
			deployment: &Deployment{
				EnvironmentName: "pipeline",
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.deployment.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestLifecycleStage_Validate(t *testing.T) {
	type testCase struct {
		name        string
		stageName   string
		deployments int
		expectError bool
	}

	testCases := []testCase{
		{
			name:        "valid with only one deployment",
			stageName:   "dev",
			deployments: 1,
		},
		{
			name:        "valid with maximum allowed deployments",
			stageName:   "dev",
			deployments: maxNodesPerStage,
		},
		{
			name:        "invalid with no deployments",
			stageName:   "dev",
			expectError: true,
		},
		{
			name:        "invalid with too many deployments",
			stageName:   "dev",
			deployments: maxNodesPerStage + 1,
			expectError: true,
		},
		{
			name:        "invalid stage name",
			stageName:   "invalid name",
			deployments: 1,
			expectError: true,
		},
		{
			name:        "stage name is a reserved keyword",
			stageName:   "stage",
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			stage := &LifecycleStage{Name: tc.stageName}

			for i := 0; i < tc.deployments; i++ {
				stage.Deployments = append(stage.Deployments, &Deployment{
					EnvironmentName: "dev",
				})
			}

			err := stage.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestLifecycleValidate_deployments(t *testing.T) {
	type testCase struct {
		name        string
		stages      []*LifecycleStage
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid",
			stages: []*LifecycleStage{
				{
					Name: "deploy",
					Deployments: []*Deployment{
						{
							EnvironmentName: "dev",
						},
					},
				},
			},
		},
		{
			name: "invalid stage name",
			stages: []*LifecycleStage{
				{
					Name: "invalid name",
					Deployments: []*Deployment{
						{
							EnvironmentName: "dev",
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "duplicate deployments",
			stages: []*LifecycleStage{
				{
					Name: "deploy",
					Deployments: []*Deployment{
						{
							EnvironmentName: "dev",
						},
						{
							EnvironmentName: "dev",
						},
					},
				},
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			template := &LifecycleTemplate{
				Stages: tc.stages,
			}

			for _, stage := range tc.stages {
				template.StageOrder = append(template.StageOrder, stage.Name)
			}

			err := template.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestLifecycleValidate_stages(t *testing.T) {
	type testCase struct {
		name        string
		stages      int
		expectError bool
	}

	testCases := []testCase{
		{
			name:   "valid with only one stage",
			stages: 1,
		},
		{
			name:   "valid with maximum allowed stages",
			stages: MaximumAllowedNodes - 1,
		},
		{
			name:        "invalid with no stages",
			expectError: true,
		},
		{
			name:        "invalid with too many stages",
			stages:      MaximumAllowedNodes + 1,
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			template := &LifecycleTemplate{}

			for i := 0; i < tc.stages; i++ {
				stageName := fmt.Sprintf("stage-%d", i)
				template.Stages = append(template.Stages, &LifecycleStage{
					Name: stageName,
					Deployments: []*Deployment{
						{
							EnvironmentName: fmt.Sprintf("dev-%d", i),
						},
					},
				})
				template.StageOrder = append(template.StageOrder, stageName)
			}

			err := template.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestLifecycleTemplate_Validate(t *testing.T) {
	type testCase struct {
		template    *LifecycleTemplate
		name        string
		expectError bool
	}

	testCases := []testCase{
		{
			name: "valid",
			template: &LifecycleTemplate{
				Variables: []*Variable{
					{
						Name: "port",
					},
				},
				JSONWebTokens: []*JWT{
					{
						Name:     "gitlab",
						Audience: "some-audience",
					},
				},
				StageOrder: []string{"deploy"},
				Stages: []*LifecycleStage{
					{
						Name: "deploy",
						Deployments: []*Deployment{
							{
								EnvironmentName: "dev",
							},
						},
					},
				},
			},
		},
		{
			name:        "missing lifecycle block",
			template:    &LifecycleTemplate{},
			expectError: true,
		},
		{
			name: "duplicate stage",
			template: &LifecycleTemplate{
				StageOrder: []string{"dev_deployment"},
				Stages: []*LifecycleStage{
					{
						Name: "dev_deployment",
						Deployments: []*Deployment{
							{
								EnvironmentName: "dev",
							},
						},
					},
					{
						Name: "dev_deployment",
						Deployments: []*Deployment{
							{
								EnvironmentName: "dev",
							},
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "stage has duplicate tasks",
			template: &LifecycleTemplate{
				StageOrder: []string{"dev"},
				Stages: []*LifecycleStage{
					{
						Name: "dev",
						PreTasks: &PipelineCondition{
							Tasks: []*PipelineTask{
								{
									Name: "pre_check",
								},
								{
									Name: "pre_check",
								},
							},
						},
						Deployments: []*Deployment{
							{
								EnvironmentName: "dev",
							},
						},
					},
				},
			},
			expectError: true,
		},
		{
			name: "task cannot have circular dependency",
			template: &LifecycleTemplate{
				StageOrder: []string{"dev"},
				Stages: []*LifecycleStage{
					{
						Name: "dev",
						PreTasks: &PipelineCondition{
							Tasks: []*PipelineTask{
								{
									Name:         "testing",
									Dependencies: []string{"compiling"},
								},
								{
									Name:         "compiling",
									Dependencies: []string{"testing"},
								},
							},
						},
					},
				},
			},
			expectError: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.template.Validate()

			if tc.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}

func TestLifecycleTemplate_GetPlugins(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	require.NotEmpty(t, lifecycleTemplate.Plugins)
	assert.Len(t, lifecycleTemplate.GetPlugins(), len(lifecycleTemplate.Plugins))
}

func TestLifecycleTemplate_GetVariables(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	require.NotEmpty(t, lifecycleTemplate.Variables)
	assert.Len(t, lifecycleTemplate.Variables, len(lifecycleTemplate.Variables))
}

func TestLifecycleTemplate_GetJSONWebTokens(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	require.NotEmpty(t, lifecycleTemplate.JSONWebTokens)
	assert.Len(t, lifecycleTemplate.GetJSONWebTokens(), len(lifecycleTemplate.JSONWebTokens))
}

func TestLifecycleTemplate_GetVCSTokens(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	require.NotEmpty(t, lifecycleTemplate.VCSTokens)
	assert.Len(t, lifecycleTemplate.GetVCSTokens(), len(lifecycleTemplate.VCSTokens))
}

func TestLifecycleTemplate_GetEnvironmentNames(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	require.NotEmpty(t, lifecycleTemplate.Stages)
	assert.Equal(t, []string{"account1"}, lifecycleTemplate.GetEnvironmentNames())
}

func TestLifecycleTemplate_BuildStateMachine(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	stateMachine, err := lifecycleTemplate.BuildStateMachine()
	require.NoError(t, err)
	require.NotNil(t, stateMachine)

	// Get the nodes so we can check how many were parsed.
	nodes, err := stateMachine.GetNodes(&statemachine.GetNodesInput{})
	require.NoError(t, err)

	assert.Len(t, nodes, 9)
}

func TestLifecycleTemplate_GetTask(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	type testCase struct {
		name        string
		taskPath    string
		expectFound bool
	}

	testCases := []testCase{
		{
			name:        "valid pre task",
			taskPath:    "stage.dev.pre.task.check_for_changes",
			expectFound: true,
		},
		{
			name:        "valid post task",
			taskPath:    "stage.dev.post.task.cleanup_resources",
			expectFound: true,
		},
		{
			name:     "invalid pre task",
			taskPath: "stage.dev.pre.task.invalid",
		},
		{
			name:     "invalid post task",
			taskPath: "stage.dev.post.task.invalid",
		},
		{
			name:     "not a task",
			taskPath: "stage.dev.pre.task.check_for_changes.action.find_changes",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			task, found := lifecycleTemplate.GetTask(test.taskPath)
			require.Equal(t, test.expectFound, found)

			if test.expectFound {
				require.NotNil(t, task)
			} else {
				assert.Nil(t, task)
			}
		})
	}
}

func TestLifecycleTemplate_GetAction(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	type testCase struct {
		name        string
		actionPath  string
		expectFound bool
	}

	testCases := []testCase{
		{
			name:        "valid pre task action",
			actionPath:  "stage.dev.pre.task.check_for_changes.action.find_changes",
			expectFound: true,
		},
		{
			name:        "valid post task action",
			actionPath:  "stage.dev.post.task.cleanup_resources.action.perform_cleanup",
			expectFound: true,
		},
		{
			name:       "invalid action",
			actionPath: "stage.dev.pre.task.check_for_changes.action.invalid",
		},
		{
			name:       "not an action",
			actionPath: "stage.dev.pre.task.check_for_changes",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			action, found := lifecycleTemplate.GetAction(test.actionPath)
			require.Equal(t, test.expectFound, found)

			if test.expectFound {
				require.NotNil(t, action)
			} else {
				assert.Nil(t, action)
			}
		})
	}
}

func TestLifecycleTemplate_GetNestedPipeline(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	type testCase struct {
		name         string
		pipelinePath string
		expectFound  bool
	}

	testCases := []testCase{
		{
			name:         "valid pipeline",
			pipelinePath: "stage.dev.pipeline.account1",
			expectFound:  true,
		},
		{
			name:         "invalid pipeline",
			pipelinePath: "stage.dev.pipeline.invalid",
		},
		{
			name:         "not a pipeline",
			pipelinePath: "stage.dev.pre.task.check_for_changes.action.find_changes",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipeline, found := lifecycleTemplate.GetNestedPipeline(test.pipelinePath)
			require.Equal(t, test.expectFound, found)

			if test.expectFound {
				require.NotNil(t, pipeline)
			} else {
				assert.Nil(t, pipeline)
			}
		})
	}
}

func TestLifecycleTemplate_GetLifecycleNode(t *testing.T) {
	lifecycleTemplate, _, err := loadLifecycleTemplate()
	require.NoError(t, err)

	type testCase struct {
		expectNodeType any
		name           string
		path           string
		expectFound    bool
	}

	testCases := []testCase{
		{
			name:           "valid stage",
			path:           "stage.dev",
			expectNodeType: &LifecycleStage{},
			expectFound:    true,
		},
		{
			name:           "valid pre task",
			path:           "stage.dev.pre.task.check_for_changes",
			expectNodeType: &PipelineTask{},
			expectFound:    true,
		},
		{
			name:           "valid pre task action",
			path:           "stage.dev.pre.task.check_for_changes.action.find_changes",
			expectNodeType: &PipelineAction{},
			expectFound:    true,
		},
		{
			name:           "valid post task",
			path:           "stage.dev.post.task.cleanup_resources",
			expectNodeType: &PipelineTask{},
			expectFound:    true,
		},
		{
			name:           "valid post task action",
			path:           "stage.dev.post.task.cleanup_resources.action.perform_cleanup",
			expectNodeType: &PipelineAction{},
			expectFound:    true,
		},
		{
			name:           "valid deployment pipeline",
			expectNodeType: &NestedPipeline{},
			path:           "stage.dev.pipeline.account1",
			expectFound:    true,
		},
		{
			name: "invalid action",
			path: "stage.dev.pre.task.check_for_changes.action.invalid",
		},
		{
			name: "invalid task",
			path: "stage.dev.pre.task.invalid",
		},
		{
			name: "invalid deployment pipeline",
			path: "stage.dev.pipeline.invalid",
		},
		{
			name: "invalid stage",
			path: "stage.invalid",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			node, found := lifecycleTemplate.GetLifecycleNode(tc.path)
			require.Equal(t, tc.expectFound, found)

			if tc.expectFound {
				require.NotNil(t, node)
				assert.IsType(t, tc.expectNodeType, node)
			} else {
				assert.Nil(t, node)
			}
		})
	}
}

func TestNewLifecycleTemplate(t *testing.T) {
	lifecycleTemplate, buf, err := loadLifecycleTemplate()
	require.NoError(t, err)

	// Ensure we can parse the template
	require.NotEmpty(t, buf)
	require.NotNil(t, lifecycleTemplate)
	require.NotNil(t, lifecycleTemplate)
	require.Len(t, lifecycleTemplate.Plugins, 1)
	require.Len(t, lifecycleTemplate.Variables, 3)
	require.Len(t, lifecycleTemplate.JSONWebTokens, 1)
	require.Len(t, lifecycleTemplate.Stages, 1)
	require.Len(t, lifecycleTemplate.Stages[0].Deployments, 1)
	require.NotNil(t, lifecycleTemplate.Stages[0].PreTasks)
	require.NotNil(t, lifecycleTemplate.Stages[0].PostTasks)
	require.Len(t, lifecycleTemplate.Stages[0].PreTasks.Tasks, 1)
	require.Len(t, lifecycleTemplate.Stages[0].PostTasks.Tasks, 1)
	require.Len(t, lifecycleTemplate.Stages[0].PreTasks.Tasks[0].Actions, 1)
	require.Len(t, lifecycleTemplate.Stages[0].PostTasks.Tasks[0].Actions, 1)
}

// loadLifecycleTemplate loads a lifecycle template from the testdata directory.
func loadLifecycleTemplate() (*LifecycleTemplate, []byte, error) {
	template := "../../../testdata/hcl/lifecycle/valid_full.hcl"

	f, err := os.Open(template)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to load lifecycle template: %w", err)
	}

	defer f.Close()

	lifecycleTemplate, buf, err := NewLifecycleTemplate(f)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to load lifecycle template: %w", err)
	}

	return lifecycleTemplate, buf, nil
}
