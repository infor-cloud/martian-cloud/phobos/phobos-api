package context

import (
	"testing"

	"github.com/hashicorp/hcl/v2"
	"github.com/stretchr/testify/assert"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/function"
	"github.com/zclconf/go-cty/cty/function/stdlib"
)

func TestNew(t *testing.T) {
	variables := map[string]cty.Value{
		"foo": cty.StringVal("bar"),
	}

	expected := &EvalContext{
		EvalContext: &hcl.EvalContext{
			Variables: variables,
			Functions: getBuiltInFunctions(),
		},
	}

	assert.Equal(t, expected, New(variables))
}

func TestContext(t *testing.T) {
	variables := map[string]cty.Value{
		"foo": cty.StringVal("bar"),
	}

	expected := &hcl.EvalContext{
		Variables: variables,
		Functions: getBuiltInFunctions(),
	}

	assert.Equal(t, expected, New(variables).Context())
}

func TestNewActionContext(t *testing.T) {
	variables := map[string]cty.Value{
		"foo": cty.StringVal("bar"),
	}

	actionOutputs := map[string]map[string]cty.Value{
		"pipeline.stage.s1.task.t1.action.a1": {
			"exit_code": cty.NumberIntVal(0),
		},
	}

	parentCtx := &EvalContext{
		EvalContext: &hcl.EvalContext{
			Variables: variables,
			Functions: map[string]function.Function{
				"jsondecode": stdlib.JSONDecodeFunc,
				"jsonencode": stdlib.JSONEncodeFunc,
			},
		},
	}

	expectedActionCtx := parentCtx.NewChild()

	expectedActionCtx.Variables = map[string]cty.Value{
		"action_outputs": cty.ObjectVal(map[string]cty.Value{
			"stage": cty.ObjectVal(map[string]cty.Value{
				"s1": cty.ObjectVal(map[string]cty.Value{
					"task": cty.ObjectVal(map[string]cty.Value{
						"t1": cty.ObjectVal(map[string]cty.Value{
							"action": cty.ObjectVal(map[string]cty.Value{
								"a1": cty.ObjectVal(map[string]cty.Value{
									"exit_code": cty.NumberIntVal(0),
								}),
							}),
						}),
					}),
				}),
			}),
		}),
	}

	// With outputs
	assert.Equal(t, &EvalContext{expectedActionCtx}, parentCtx.NewActionContext(actionOutputs))

	// Remove outputs
	expectedActionCtx.Variables = map[string]cty.Value{}

	// Without outputs
	assert.Equal(t, &EvalContext{expectedActionCtx}, parentCtx.NewActionContext(nil))
}
