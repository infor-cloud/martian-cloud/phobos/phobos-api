// Package context contains functionality for constructing HCL eval contexts
package context

import (
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/waypoint/pkg/config/funcs"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/function"
	"github.com/zclconf/go-cty/cty/function/stdlib"
)

// EvalContext wraps an HCL eval context and is used for decoding pipeline template HCL files
type EvalContext struct {
	*hcl.EvalContext
}

// New creates a new instance of an EvalContext
func New(variables map[string]cty.Value) *EvalContext {
	return &EvalContext{
		EvalContext: &hcl.EvalContext{
			Variables: variables,
			Functions: getBuiltInFunctions(),
		},
	}
}

// Context returns the HCL eval context
func (e *EvalContext) Context() *hcl.EvalContext {
	return e.EvalContext
}

// NewActionContext creates a new action context as a child of this eval context
func (e *EvalContext) NewActionContext(actionOutputs map[string]map[string]cty.Value) *EvalContext {
	variables := map[string]interface{}{}
	for key, outputs := range actionOutputs {
		if strings.HasPrefix(key, "pipeline.") {
			// Replace the "pipeline" with "action_outputs" since "pipeline"
			// doesn't exist as a top level HCL construct.
			key = strings.Replace(key, "pipeline", "action_outputs", 1)
		}

		if strings.HasPrefix(key, "lifecycle.") {
			// Replace the "lifecycle" with "action_outputs" since "lifecycle"
			// doesn't exist as a top level HCL construct.
			key = strings.Replace(key, "lifecycle", "action_outputs", 1)
		}

		// Parse action path
		parts := strings.Split(key, ".")

		vars := variables
		for i, part := range parts {
			if i == (len(parts) - 1) {
				vars[part] = outputs
			} else {
				_, ok := vars[part]
				if !ok {
					vars[part] = map[string]interface{}{}
				}
				vars = vars[part].(map[string]interface{})
			}
		}
	}

	actionCtx := e.NewChild()

	actionCtx.Variables = buildActionEvalContextVariables(variables)

	return &EvalContext{
		EvalContext: actionCtx,
	}
}

func buildActionEvalContextVariables(variables map[string]interface{}) map[string]cty.Value {
	response := map[string]cty.Value{}
	for k, v := range variables {
		switch t := v.(type) {
		case map[string]interface{}:
			response[k] = cty.ObjectVal(buildActionEvalContextVariables(t))
		case map[string]cty.Value:
			response[k] = cty.ObjectVal(t)
		}

	}
	return response
}

// getBuiltInFunctions returns a map of supported built-in functions.
func getBuiltInFunctions() map[string]function.Function {
	return map[string]function.Function{
		// Collection functions
		"chunklist":       stdlib.ChunklistFunc,
		"coalesce":        stdlib.CoalesceFunc,
		"coalescelist":    stdlib.CoalesceListFunc,
		"compact":         stdlib.CompactFunc,
		"concat":          stdlib.ConcatFunc,
		"contains":        stdlib.ContainsFunc,
		"distinct":        stdlib.DistinctFunc,
		"element":         stdlib.ElementFunc,
		"flatten":         stdlib.FlattenFunc,
		"index":           stdlib.IndexFunc,
		"keys":            stdlib.KeysFunc,
		"length":          stdlib.LengthFunc,
		"lookup":          stdlib.LookupFunc,
		"merge":           stdlib.MergeFunc,
		"range":           stdlib.RangeFunc,
		"reverse":         stdlib.ReverseFunc,
		"setintersection": stdlib.SetIntersectionFunc,
		"setproduct":      stdlib.SetProductFunc,
		"setunion":        stdlib.SetUnionFunc,
		"slice":           stdlib.SliceFunc,
		"sort":            stdlib.SortFunc,
		"values":          stdlib.ValuesFunc,
		"zipmap":          stdlib.ZipmapFunc,

		// Data and time functions
		"formatdate": stdlib.FormatDateFunc,
		"timeadd":    stdlib.TimeAddFunc,
		"timestamp":  funcs.TimestampFunc,

		// Encode functions
		"jsondecode": stdlib.JSONDecodeFunc,
		"jsonencode": stdlib.JSONEncodeFunc,

		// String functions
		"chomp":         stdlib.ChompFunc,
		"format":        stdlib.FormatFunc,
		"formatlist":    stdlib.FormatListFunc,
		"indent":        stdlib.IndentFunc,
		"join":          stdlib.JoinFunc,
		"lower":         stdlib.LowerFunc,
		"regex_replace": stdlib.RegexReplaceFunc,
		"replace":       stdlib.ReplaceFunc,
		"split":         stdlib.SplitFunc,
		"strlen":        stdlib.StrlenFunc,
		"strrev":        stdlib.ReverseFunc,
		"substr":        stdlib.SubstrFunc,
		"title":         stdlib.TitleFunc,
		"trim":          stdlib.TrimFunc,
		"trimprefix":    stdlib.TrimPrefixFunc,
		"trimsuffix":    stdlib.TrimSuffixFunc,
		"upper":         stdlib.UpperFunc,

		// Numeric functions
		"abs":      stdlib.AbsoluteFunc,
		"ceil":     stdlib.CeilFunc,
		"floor":    stdlib.FloorFunc,
		"log":      stdlib.LogFunc,
		"max":      stdlib.MaxFunc,
		"min":      stdlib.MinFunc,
		"parseint": stdlib.ParseIntFunc,
		"pow":      stdlib.PowFunc,
		"signum":   stdlib.SignumFunc,
	}
}
