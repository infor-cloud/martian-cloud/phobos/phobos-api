package validate

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component/mocks"
)

func TestVisitForAction(t *testing.T) {
	pipelineTemplate, err := os.Open(filepath.Join(testHCLDir, "pipeline/valid_full.hcl"))
	require.NoErrorf(t, err, "failed to open test HCL file")
	defer pipelineTemplate.Close()

	pipelineConfig, _, err := config.NewPipelineTemplate(pipelineTemplate, nil)
	require.NoError(t, err)

	lifecycleTemplate, err := os.Open(filepath.Join(testHCLDir, "lifecycle/valid_full.hcl"))
	require.NoErrorf(t, err, "failed to open test HCL file")
	defer lifecycleTemplate.Close()

	lifecycleConfig, _, err := config.NewLifecycleTemplate(lifecycleTemplate)
	require.NoError(t, err)

	type testCase struct {
		name         string
		actionPath   string
		templateType string
		expectError  string
	}

	testCases := []testCase{
		{
			name:         "valid pipeline pre task action",
			templateType: "pipeline",
			actionPath:   "pipeline.stage.dev.pre.task.check_for_changes.action.find_changes",
		},
		{
			name:         "valid pipeline post task action",
			templateType: "pipeline",
			actionPath:   "pipeline.stage.dev.post.task.cleanup_resources.action.perform_cleanup",
		},
		{
			name:         "valid pipeline task action",
			templateType: "pipeline",
			actionPath:   "pipeline.stage.dev.task.execute_a_command.action.perform_action",
			// Since there's no way inject visited actions into the traversal context,
			// this test will fail because the action cannot reference the output of
			// the pre-task action. See template for more details.
			expectError: "Unknown variable; There is no variable named \"action_outputs\"",
		},
		{
			name:         "pipeline action does not exist",
			templateType: "pipeline",
			actionPath:   "pipeline.stage.dev.pre.task.check_for_changes.action.does_not_exist",
			expectError:  "action with path pipeline.stage.dev.pre.task.check_for_changes.action.does_not_exist not found in hcl config",
		},
		{
			name:         "valid lifecycle pre task action",
			templateType: "lifecycle",
			actionPath:   "lifecycle.stage.dev.pre.task.check_for_changes.action.find_changes",
		},
		{
			name:         "valid lifecycle post task action",
			templateType: "lifecycle",
			actionPath:   "lifecycle.stage.dev.post.task.cleanup_resources.action.perform_cleanup",
			// Since there's no way inject visited actions into the traversal context,
			// this test will fail because the action cannot reference the output of
			// the pre-task action. See template for more details.
			expectError: "Unknown variable; There is no variable named \"action_outputs\"",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			log := hclog.NewNullLogger()
			mockPluginManager := plugin.NewMockManager(t)
			mockPluginProvider := mocks.NewPipelinePlugin(t)
			mockProvider := plugin.NewPipelinePlugin(mockPluginProvider, log, func() {})

			mockPluginManager.On("GetPlugin", mock.Anything).Return(func(name string) (*plugin.PipelinePlugin, error) {
				if name == "exec" {
					return mockProvider, nil
				}
				return nil, fmt.Errorf("plugin with name %s is not available", name)
			}).Maybe()

			mockPluginProvider.On("ActionInput", mock.Anything, mock.Anything).Return(&struct {
				Command string `hcl:"command,attr"`
			}{}, nil).Maybe()

			mockPluginProvider.On("ActionOutput", mock.Anything, mock.Anything).Return(&struct {
				ExitCode int
			}{}, nil).Maybe()

			var hclConfig config.HCLConfig
			switch tc.templateType {
			case "pipeline":
				hclConfig = pipelineConfig
			case "lifecycle":
				hclConfig = lifecycleConfig
			default:
				t.Fatalf("unknown template type %s", tc.templateType)
			}

			// Add variables to the eval context
			variables := map[string]cty.Value{
				"var": cty.ObjectVal(map[string]cty.Value{
					"sleep_interval": cty.NumberIntVal(15),
					"account_name":   cty.StringVal("account1"),
				}),
				"jwt": cty.ObjectVal(map[string]cty.Value{
					"gitlab": cty.StringVal("token"),
				}),
			}

			v := &visitor{
				actionOutputMap: map[string]map[string]cty.Value{},
				pluginManager:   mockPluginManager,
				hclConfig:       hclConfig,
				evalCtx:         hclctx.New(variables),
			}

			// For the purposes of this test, we only care about the action node since
			// the visitor will only ever use the node's path.
			actionNode := statemachine.NewActionNode(tc.actionPath, nil, statemachine.CreatedNodeStatus)

			err = v.VisitForAction(actionNode, &statemachine.TraversalContext{})

			if tc.expectError != "" {
				assert.ErrorContains(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
			require.Equal(t, 1, len(v.actionOutputMap))
			assert.Equal(t, 1, len(v.actionOutputMap[tc.actionPath]))
		})
	}
}

func TestVisitForTask(t *testing.T) {
	pipelineTemplate, err := os.Open(filepath.Join(testHCLDir, "pipeline/valid_full.hcl"))
	require.NoErrorf(t, err, "failed to open test HCL file")
	defer pipelineTemplate.Close()

	pipelineConfig, _, err := config.NewPipelineTemplate(pipelineTemplate, nil)
	require.NoError(t, err)

	lifecycleTemplate, err := os.Open(filepath.Join(testHCLDir, "lifecycle/valid_full.hcl"))
	require.NoErrorf(t, err, "failed to open test HCL file")
	defer lifecycleTemplate.Close()

	lifecycleConfig, _, err := config.NewLifecycleTemplate(lifecycleTemplate)
	require.NoError(t, err)

	type testCase struct {
		name         string
		taskPath     string
		templateType string
		expectError  string
	}

	testCases := []testCase{
		{
			name:         "valid pipeline pre-task",
			templateType: "pipeline",
			taskPath:     "pipeline.stage.dev.pre.task.check_for_changes",
		},
		{
			name:         "valid pipeline post-task",
			templateType: "pipeline",
			taskPath:     "pipeline.stage.dev.post.task.cleanup_resources",
		},
		{
			name:         "valid pipeline task",
			templateType: "pipeline",
			taskPath:     "pipeline.stage.dev.task.execute_a_command",
		},
		{
			name:         "pipeline task does not exist",
			templateType: "pipeline",
			taskPath:     "pipeline.stage.dev.pre.task.does_not_exist",
			expectError:  "task with path pipeline.stage.dev.pre.task.does_not_exist not found in hcl config",
		},
		{
			name:         "valid lifecycle pre task",
			templateType: "lifecycle",
			taskPath:     "lifecycle.stage.dev.pre.task.check_for_changes",
		},
		{
			name:         "valid lifecycle post task",
			templateType: "lifecycle",
			taskPath:     "lifecycle.stage.dev.post.task.cleanup_resources",
		},
		{
			name:         "lifecycle task does not exist",
			templateType: "lifecycle",
			taskPath:     "lifecycle.stage.dev.pre.task.does_not_exist",
			expectError:  "task with path lifecycle.stage.dev.pre.task.does_not_exist not found in hcl config",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var hclConfig config.HCLConfig
			switch tc.templateType {
			case "pipeline":
				hclConfig = pipelineConfig
			case "lifecycle":
				hclConfig = lifecycleConfig
			default:
				t.Fatalf("unknown template type %s", tc.templateType)
			}

			// Add variables to the eval context
			variables := map[string]cty.Value{
				"var": cty.ObjectVal(map[string]cty.Value{
					"sleep_interval":    cty.NumberIntVal(15),
					"approval_required": cty.BoolVal(true),
				}),
				"jwt": cty.ObjectVal(map[string]cty.Value{
					"gitlab": cty.StringVal("token"),
				}),
			}

			v := &visitor{
				actionOutputMap: map[string]map[string]cty.Value{},
				hclConfig:       hclConfig,
				evalCtx:         hclctx.New(variables),
			}

			// For the purposes of this test, we only care about the task node since
			// the visitor will only ever use the node's path.
			taskNode := statemachine.NewTaskNode(&statemachine.NewTaskNodeInput{
				Path: tc.taskPath,
			})

			err = v.VisitForTask(taskNode, &statemachine.TraversalContext{})

			if tc.expectError != "" {
				assert.ErrorContains(t, err, tc.expectError)
				return
			}

			require.NoError(t, err)
		})
	}
}
