package validate

import (
	"context"
	"fmt"

	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

type visitor struct {
	statemachine.PartialVisitor
	actionOutputMap map[string]map[string]cty.Value
	evalCtx         *hclctx.EvalContext
	pluginManager   plugin.Manager
	hclConfig       config.HCLConfig
}

func (v *visitor) VisitForAction(action *statemachine.ActionNode, traversalCtx *statemachine.TraversalContext) error {
	ctx := context.Background()
	hclAction, ok := v.hclConfig.GetAction(action.Path())
	if !ok {
		return fmt.Errorf("action with path %s not found in hcl config", action.Path())
	}

	providerPlugin, err := v.pluginManager.GetPlugin(hclAction.PluginName())
	if err != nil {
		return err
	}

	actionCtx := v.evalCtx.NewActionContext(v.getActionOutputsForVisitedActions(traversalCtx))

	if diag := providerPlugin.ValidateActionInput(ctx, hclAction.PluginActionName(), hclAction.Remain, actionCtx.Context()); diag.HasErrors() {
		return fmt.Errorf("failed to validate action config %v", diag.Error())
	}

	outputs, err := providerPlugin.GetImpliedActionOutputs(ctx, hclAction.PluginActionName())
	if err != nil {
		return err
	}

	// Add completed action outputs to map
	v.actionOutputMap[action.Path()] = outputs

	return nil
}

func (v *visitor) VisitForTask(task *statemachine.TaskNode, traversalCtx *statemachine.TraversalContext) error {
	taskPath := task.Path()

	hclTask, ok := v.hclConfig.GetTask(taskPath)
	if !ok {
		return fmt.Errorf("task with path %s not found in hcl config", task.Path())
	}

	evalCtx := v.evalCtx.NewActionContext(v.getActionOutputsForVisitedActions(traversalCtx))

	if !hclTask.SuccessCondition.Range().Empty() {
		if _, diag := hclTask.SuccessCondition.Value(evalCtx.Context()); diag.HasErrors() {
			return fmt.Errorf("task with path %s has an invalid success condition: %v", taskPath, diag.Error())
		}
	}

	if !hclTask.ApprovalRules.Range().Empty() {
		if _, diag := hclTask.ApprovalRules.Value(evalCtx.Context()); diag.HasErrors() {
			return fmt.Errorf("task with path %s has an invalid approval rule expression: %v", taskPath, diag.Error())
		}
	}

	if !hclTask.If.Range().Empty() {
		if _, diag := hclTask.If.Value(evalCtx.Context()); diag.HasErrors() {
			return fmt.Errorf("task with path %s has an invalid if expression: %v", taskPath, diag.Error())
		}
	}

	return nil
}

func (v *visitor) VisitForPipeline(pipeline *statemachine.PipelineNode, traversalCtx *statemachine.TraversalContext) error {
	pipelinePath := pipeline.Path()

	hclPipeline, ok := v.hclConfig.GetNestedPipeline(pipelinePath)
	if !ok {
		return fmt.Errorf("pipeline with path %s not found in hcl config", pipelinePath)
	}

	evalCtx := v.evalCtx.NewActionContext(v.getActionOutputsForVisitedActions(traversalCtx))

	if !hclPipeline.ApprovalRules.Range().Empty() {
		if _, diag := hclPipeline.ApprovalRules.Value(evalCtx.Context()); diag.HasErrors() {
			return fmt.Errorf("pipeline with path %s has an invalid approval rule expression: %v", pipelinePath, diag.Error())
		}
	}

	if !hclPipeline.If.Range().Empty() {
		if _, diag := hclPipeline.If.Value(evalCtx.Context()); diag.HasErrors() {
			return fmt.Errorf("pipeline with path %s has an invalid if expression: %v", pipelinePath, diag.Error())
		}
	}

	return nil
}

func (v *visitor) getActionOutputsForVisitedActions(traversalCtx *statemachine.TraversalContext) map[string]map[string]cty.Value {
	filteredOutputs := map[string]map[string]cty.Value{}
	for _, action := range traversalCtx.VisitedActions() {
		path := action.Path()
		filteredOutputs[path] = v.actionOutputMap[path]
	}
	return filteredOutputs
}
