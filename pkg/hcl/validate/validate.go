// Package validate contains functionality for validating
// that a pipeline template HCL file is valid
package validate

import (
	"context"

	"github.com/hashicorp/hcl/v2"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/variable"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/statemachine"
)

// Validator can validate a pipeline template
type Validator struct {
	pluginManager plugin.Manager
}

// New creates a new instance of a Validator
func New(pluginManager plugin.Manager) *Validator {
	return &Validator{pluginManager: pluginManager}
}

// Validate will validate a pipeline template and return diagnostics if it's not valid
func (v *Validator) Validate(ctx context.Context, hclConfig config.HCLConfig, variableValues map[string]string) hcl.Diagnostics {
	if err := hclConfig.Validate(); err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	variablePlaceholders, err := variable.GetCtyValuesForVariables(hclConfig.GetVariableBodies(), variableValues, true)
	if err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	jwtMap := map[string]cty.Value{}
	for _, jwt := range hclConfig.GetJSONWebTokens() {
		jwtMap[jwt.Name] = cty.UnknownVal(cty.String)
	}

	vcsTokenMap := map[string]cty.Value{}
	for _, vcsToken := range hclConfig.GetVCSTokens() {
		vcsTokenMap[vcsToken.Name] = cty.ObjectVal(map[string]cty.Value{
			"value":     cty.UnknownVal(cty.String),
			"file_path": cty.UnknownVal(cty.String),
		})
	}

	evalCtx := hclctx.New(map[string]cty.Value{
		"var":       cty.ObjectVal(variablePlaceholders),
		"jwt":       cty.ObjectVal(jwtMap),
		"vcs_token": cty.ObjectVal(vcsTokenMap),
	})

	for _, provider := range hclConfig.GetPlugins() {
		providerPlugin, gErr := v.pluginManager.GetPlugin(provider.Name)
		if gErr != nil {
			return hcl.Diagnostics{
				&hcl.Diagnostic{
					Severity: hcl.DiagError,
					Summary:  gErr.Error(),
					Detail:   "",
				},
			}
		}

		diag := providerPlugin.ValidateConfig(ctx, provider.Body, evalCtx.Context())
		if diag != nil {
			return diag
		}
	}

	validationVisitor := visitor{
		evalCtx:         evalCtx,
		actionOutputMap: map[string]map[string]cty.Value{},
		pluginManager:   v.pluginManager,
		hclConfig:       hclConfig,
	}

	stateMachine, err := hclConfig.BuildStateMachine()
	if err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	traverser := statemachine.NewSimulatedTraverser(stateMachine)
	if err := traverser.Accept(&validationVisitor); err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	return nil
}
