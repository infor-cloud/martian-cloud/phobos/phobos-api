package validate

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component/mocks"
)

// Directory that contains the test data for the HCL pipeline templates.
const testHCLDir = "../../../testdata/hcl/"

func TestNew(t *testing.T) {
	mockPluginManager := plugin.NewMockManager(t)
	expected := &Validator{pluginManager: mockPluginManager}
	assert.Equal(t, expected, New(mockPluginManager))
}

func TestValidator_Validate(t *testing.T) {
	type testCase struct {
		name        string
		rawTemplate string
		expectError string
	}

	testCases := []testCase{
		{
			name:        "valid pipeline template",
			rawTemplate: "pipeline/valid_full.hcl",
		},
		{
			name:        "valid lifecycle template",
			rawTemplate: "lifecycle/valid_full.hcl",
		},
		{
			name:        "action references unknown output",
			rawTemplate: "pipeline/invalid_action_references_unknown_output.hcl",
			expectError: "Unknown variable; There is no variable named \"action_outputs\".",
		},
		{
			name:        "missing variable block",
			rawTemplate: "pipeline/invalid_missing_variable_block.hcl",
			expectError: "This object does not have an attribute named \"unknown_variable\"",
		},
		{
			name:        "missing jwt block",
			rawTemplate: "pipeline/invalid_missing_jwt_block.hcl",
			expectError: "This object does not have an attribute named \"unknown_jwt\"",
		},
		{
			name:        "missing vcs token block",
			rawTemplate: "pipeline/invalid_missing_vcs_token_block.hcl",
			expectError: "This object does not have an attribute named \"unknown_vcs_token\"",
		},
		{
			name:        "missing plugin block",
			rawTemplate: "pipeline/invalid_missing_plugin_block.hcl",
			expectError: "plugin with name http is not available",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			log := hclog.NewNullLogger()
			mockPluginManager := plugin.NewMockManager(t)
			mockPluginProvider := mocks.NewPipelinePlugin(t)
			mockProvider := plugin.NewPipelinePlugin(mockPluginProvider, log, func() {})

			var template *os.File
			var err error
			if tc.rawTemplate != "" {
				template, err = os.Open(filepath.Join(testHCLDir, tc.rawTemplate))
				require.NoErrorf(t, err, "failed to open test HCL file %s", tc.rawTemplate)
				defer template.Close()
			}

			var hclConfig config.HCLConfig

			if strings.HasPrefix(tc.rawTemplate, "pipeline") {
				hclConfig, _, err = config.NewPipelineTemplate(template, nil, config.WithUnknownVars())
			} else if strings.HasPrefix(tc.rawTemplate, "lifecycle") {
				hclConfig, _, err = config.NewLifecycleTemplate(template)
			} else {
				t.Fatalf("unknown template type %s", tc.rawTemplate)
			}

			require.NoError(t, err)

			mockPluginManager.On("GetPlugin", mock.Anything).Return(func(name string) (*plugin.PipelinePlugin, error) {
				if name == "exec" {
					return mockProvider, nil
				}
				return nil, fmt.Errorf("plugin with name %s is not available", name)
			}).Maybe()

			mockPluginManager.On("ValidateConfig", ctx, mock.Anything, mock.Anything).Return(nil).Maybe()

			// Emulate the "exec" provider
			mockPluginProvider.On("Config", mock.Anything).Return(&struct{}{}, nil).Maybe()

			mockPluginProvider.On("ActionInput", mock.Anything, mock.Anything).Return(&struct {
				Command string `hcl:"command,attr"`
			}{}, nil).Maybe()

			mockPluginProvider.On("ActionOutput", mock.Anything, mock.Anything).Return(&struct {
				ExitCode int
			}{}, nil).Maybe()

			validator := &Validator{
				pluginManager: mockPluginManager,
			}

			diags := validator.Validate(ctx, hclConfig, nil)

			if tc.expectError != "" {
				assert.ErrorContains(t, diags, tc.expectError)
			} else {
				assert.Empty(t, diags)
			}
		})
	}
}
