package funcs

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
)

func TestTimestampFunc(t *testing.T) {
	timestamp, err := TimestampFunc.Call([]cty.Value{})
	require.NoError(t, err)
	require.IsType(t, cty.String, timestamp.Type())

	actual, err := time.Parse(time.RFC3339, timestamp.AsString())
	require.NoError(t, err)
	require.WithinDuration(t, actual, time.Now(), time.Second)
}
