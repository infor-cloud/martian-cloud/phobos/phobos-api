// Package funcs contains custom functions used for the HCL eval context.
package funcs

import (
	"time"

	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/function"
)

// TimestampFunc returns the current timestamp in RFC 3339 format.
var TimestampFunc = function.New(&function.Spec{
	Description: `Returns the current timestamp in RFC 3339 format.`,
	Params:      []function.Parameter{},
	Type:        function.StaticReturnType(cty.String),
	Impl: func(_ []cty.Value, _ cty.Type) (cty.Value, error) {
		return cty.StringVal(time.Now().Format(time.RFC3339)), nil
	},
})
