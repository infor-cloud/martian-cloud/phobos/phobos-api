// Package variable provides a way to parse and validate variables in a HCL file.
// Portions of this file are derived from the Waypoint project.
// Modifications have been made to meet the needs of the Phobos project.
// Source: https://github.com/hashicorp/waypoint/blob/7128fba03891df32b77e341d2380fe8f48a0b508/internal/config/variables/variables.go
// License: Mozilla Public License 2.0
package variable

import (
	"fmt"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/ext/typeexpr"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/convert"
	ctyjson "github.com/zclconf/go-cty/cty/json"
)

var (
	// variableBlockSchema is the schema for a variable block
	variableBlockSchema = &hcl.BodySchema{
		Attributes: []hcl.AttributeSchema{
			{Name: "default"},
			{Name: "type"},
		},
	}
)

// Variable represents a decoded variable from a HCL config.
type Variable struct {
	Type    cty.Type
	Default *cty.Value
	Name    string
}

// Value returns the cty value of the variable given the raw value. The default value is returned if the
// raw value is nil.
func (v *Variable) Value(rawValue *string) (*cty.Value, error) {
	if rawValue != nil {
		if v.Type == cty.String {
			// Type is a string so it can be directly converted to a cty string value
			ctyVal := cty.StringVal(*rawValue)
			return &ctyVal, nil
		}

		if v.Type == cty.NilType {
			// If the type is nil, we need to check if the raw value is a string w/o quotes
			_, err := ctyjson.ImpliedType([]byte(*rawValue))
			if err != nil {
				// The raw value is a string w/o quotes so it can be converted to a cty string value
				ctyVal := cty.StringVal(*rawValue)
				return &ctyVal, nil
			}
		}

		fakeFileName := fmt.Sprintf("<value for var.%s>", v.Name)
		expression, diags := hclsyntax.ParseExpression([]byte(*rawValue), fakeFileName, hcl.Pos{Line: 1, Column: 1})
		if diags.HasErrors() {
			errMessages := []string{}
			for _, diag := range diags {
				// Only use summary for more concise error messages
				errMessages = append(errMessages, diag.Summary)
			}
			return nil, fmt.Errorf("failed to parse value %q for hcl variable %q: %v", *rawValue, v.Name, strings.Join(errMessages, ": "))
		}

		ctyVal, diags := expression.Value(nil)
		if diags.HasErrors() {
			return nil, fmt.Errorf("failed to evaluate value for hcl variable %q: %v", v.Name, diags.Error())
		}

		if v.Type != cty.NilType {
			// Attempt to convert value to the variable's type.
			var err error
			ctyVal, err = convert.Convert(ctyVal, v.Type)
			if err != nil {
				return nil, fmt.Errorf("value for variable %q is not valid: %w", v.Name, err)
			}
		}

		return &ctyVal, nil
	}

	return v.Default, nil
}

// GetCtyValuesForVariables returns a map of cty values for the given variable definitions and raw string values.
func GetCtyValuesForVariables(
	hclVariables map[string]hcl.Body,
	variableValues map[string]string,
	unknownVars bool,
) (map[string]cty.Value, error) {
	ctyValues := map[string]cty.Value{}

	if variableValues == nil {
		variableValues = map[string]string{}
	}

	// Decode variables from HCL config
	decodedVariables, diags := DecodeVariables(hclVariables)
	if diags.HasErrors() {
		return nil, diags
	}

	for name, decoded := range decodedVariables {
		var rawVal *string
		if v, found := variableValues[name]; found {
			rawVal = &v
		}

		val, err := decoded.Value(rawVal)
		if err != nil {
			return nil, err
		}

		if val == nil && unknownVars {
			unknownVal := cty.UnknownVal(decoded.Type)
			val = &unknownVal
		}

		if val == nil {
			// We have a variable defined in the HCL config but no value.
			return nil, fmt.Errorf("hcl variable %q defined in configuration but no value provided", name)
		}

		// Set variable value
		ctyValues[name] = *val
	}

	return ctyValues, nil
}

// DecodeVariables decodes variables from a HCL config and returns a map of
// variables and any diagnostics.
func DecodeVariables(hclVariables map[string]hcl.Body) (map[string]*Variable, hcl.Diagnostics) {
	vs := map[string]*Variable{}

	for k, v := range hclVariables {
		decodedVariable, diags := decodeVariable(k, v)
		if diags.HasErrors() {
			return nil, diags
		}

		vs[k] = decodedVariable
	}

	return vs, nil
}

// decodeVariable decodes a single variable from a HCL config and returns the
// variable and any diagnostics.
func decodeVariable(name string, body hcl.Body) (*Variable, hcl.Diagnostics) {
	v := Variable{
		Name: name,
	}

	content, diags := body.Content(variableBlockSchema)
	if diags.HasErrors() {
		return nil, diags
	}

	if attr, ok := content.Attributes["type"]; ok {
		// TypeConstraint allows "any", and it's OK if users opt out of
		// waypoint type checking here.
		t, moreDiags := typeexpr.TypeConstraint(attr.Expr)
		diags = append(diags, moreDiags...)
		if moreDiags.HasErrors() {
			return nil, diags
		}
		v.Type = t
	}

	if attr, exists := content.Attributes["default"]; exists {
		val, valDiags := attr.Expr.Value(nil)
		diags = append(diags, valDiags...)
		if diags.HasErrors() {
			return nil, diags
		}

		// Convert the default to the expected type so we can catch invalid
		// defaults early and allow later code to assume validity.
		// Note that this depends on us having already processed any "type"
		// attribute above.
		if v.Type != cty.NilType {
			var err error
			val, err = convert.Convert(val, v.Type)
			if err != nil {
				diags = append(diags, &hcl.Diagnostic{
					Severity: hcl.DiagError,
					Summary:  fmt.Sprintf("Invalid default value for variable %q", name),
					Detail:   fmt.Sprintf("This default value is not compatible with the variable's type constraint: %s.", err),
					Subject:  attr.Expr.Range().Ptr(),
				})
				val = cty.DynamicVal
			}
		}

		v.Default = &val

		// It's possible no type attribute was assigned so lets make sure we
		// have a valid type otherwise there could be issues parsing the value.
		if v.Type == cty.NilType {
			v.Type = val.Type()
		}
	}

	return &v, diags
}
