package variable

import (
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
)

// variable represents a an HCL variable
type variable struct {
	Body    hcl.Body       `hcl:",body"`
	Type    hcl.Expression `hcl:"type,optional"`
	Default hcl.Expression `hcl:"default,optional"`
	Name    string         `hcl:",label"`
}

// testConfig is a test config used to parse variable blocks.
type testConfig struct {
	Variables []*variable `hcl:"variable,block"`
}

func (tc testConfig) GetVariableBodies() map[string]hcl.Body {
	variableBodies := map[string]hcl.Body{}
	for _, v := range tc.Variables {
		variableBodies[v.Name] = v.Body
	}
	return variableBodies
}

// testVariable is a test variable used to create a test HCL variable.
type testVariable struct {
	Type    *string
	Default *cty.Value
	Name    string
}

func TestGetVariableValue(t *testing.T) {
	type testCase struct {
		expectError string
		variable    *Variable
		rawValue    *string
		expectValue cty.Value
		name        string
	}

	testCases := []testCase{
		{
			name: "primitive HCL variable with matching type and value",
			variable: &Variable{
				Name: "test",
				Type: cty.String,
			},
			rawValue:    ptr.String("value"),
			expectValue: cty.StringVal("value"),
		},
		{
			name: "complex HCL variable with matching type and value",
			variable: &Variable{
				Name: "test",
				Type: cty.Map(cty.String),
			},
			rawValue: ptr.String(`{"key": "value"}`),
			expectValue: cty.MapVal(map[string]cty.Value{
				"key": cty.StringVal("value"),
			}),
		},
		{
			name: "provided value can be converted to HCL variable type",
			variable: &Variable{
				Name: "test",
				Type: cty.Bool,
			},
			rawValue:    ptr.String("false"),
			expectValue: cty.BoolVal(false),
		},
		{
			name: "no value provided for HCL variable so default is used",
			variable: &Variable{
				Name:    "test",
				Default: ptrCtyVal(cty.StringVal("value")),
			},
			expectValue: cty.StringVal("value"),
		},
		{
			name: "provided value cannot be converted to HCL variable type",
			variable: &Variable{
				Name: "test",
				Type: cty.List(cty.Number),
			},
			rawValue:    ptr.String("1, 2, 3]"),
			expectError: "failed to parse value \"1, 2, 3]\" for hcl variable \"test\": Extra characters after expression",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			val, err := test.variable.Value(test.rawValue)

			if test.expectError != "" {
				require.NotNil(t, err)
				assert.Contains(t, err.Error(), test.expectError)
				return
			}

			require.NoError(t, err)
			require.NotNil(t, val)
			assert.Equal(t, test.expectValue, *val)
		})
	}
}

func TestGetCtyValuesForVariables(t *testing.T) {
	type testCase struct {
		rawValues      map[string]string
		expectResponse map[string]cty.Value
		expectError    string
		name           string
		hclVariables   []testVariable
		unknownVars    bool
	}

	testCases := []testCase{
		{
			name: "primitive HCL variable with matching type and value",
			hclVariables: []testVariable{
				{
					Name: "test",
					Type: ptr.String("string"),
				},
				{
					Name: "test2",
					Type: ptr.String("bool"),
				},
			},
			rawValues: map[string]string{
				"test":  "value",
				"test2": "false",
			},
			expectResponse: map[string]cty.Value{
				"test":  cty.StringVal("value"),
				"test2": cty.BoolVal(false),
			},
		},
		{
			name: "hcl variable with unknown value but defined type",
			hclVariables: []testVariable{
				{
					Name: "test",
					Type: ptr.String("bool"),
				},
			},
			rawValues: map[string]string{},
			expectResponse: map[string]cty.Value{
				"test": cty.UnknownVal(cty.Bool),
			},
			unknownVars: true,
		},
		{
			name: "hcl variable with unknown value and unknown type",
			hclVariables: []testVariable{
				{
					Name: "test",
				},
			},
			rawValues: map[string]string{},
			expectResponse: map[string]cty.Value{
				"test": cty.UnknownVal(cty.NilType),
			},
			unknownVars: true,
		},
		{
			name: "no value or default provided for HCL variable",
			hclVariables: []testVariable{
				{
					Name: "test",
					Type: ptr.String("string"),
				},
			},
			expectError: "hcl variable \"test\" defined in configuration but no value provided",
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// Create a test config.
			configData, err := createTestConfig(test.hclVariables)
			require.NoError(t, err)

			// Decode the test config.
			var config testConfig
			err = hclsimple.Decode("test.hcl", configData, nil, &config)
			require.NoError(t, err)

			createdVariables, err := GetCtyValuesForVariables(config.GetVariableBodies(), test.rawValues, test.unknownVars)

			if test.expectError != "" {
				require.NotNil(t, err)
				assert.Contains(t, err.Error(), test.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectResponse, createdVariables)
		})
	}
}

func TestDecodeVariables(t *testing.T) {
	type testCase struct {
		expectError     string
		expectVariables map[string]*Variable
		name            string
		inputVariables  []testVariable
	}

	testCases := []testCase{
		{
			name: "primitive variables",
			inputVariables: []testVariable{
				{
					Name: "type",
					Type: ptr.String("string"),
				},
				{
					Name:    "default",
					Default: ptrCtyVal(cty.StringVal("value")),
				},
				{
					Name:    "type_and_default",
					Type:    ptr.String("string"),
					Default: ptrCtyVal(cty.StringVal("value")),
				},
			},
			expectVariables: map[string]*Variable{
				"type": {
					Name: "type",
					Type: cty.String,
				},
				"default": {
					Name:    "default",
					Type:    cty.String, // Type is inferred from the default value.
					Default: ptrCtyVal(cty.StringVal("value")),
				},
				"type_and_default": {
					Name:    "type_and_default",
					Type:    cty.String,
					Default: ptrCtyVal(cty.StringVal("value")),
				},
			},
		},
		{
			name: "complex variables",
			inputVariables: []testVariable{
				{
					Name: "type",
					Type: ptr.String("map(string)"),
				},
				{
					Name:    "default",
					Default: ptrCtyVal(cty.MapVal(map[string]cty.Value{"foo": cty.StringVal("bar")})),
				},
				{
					Name:    "type_and_default",
					Type:    ptr.String("map(string)"),
					Default: ptrCtyVal(cty.MapVal(map[string]cty.Value{"foo": cty.StringVal("bar")})),
				},
			},
			expectVariables: map[string]*Variable{
				"type": {
					Name: "type",
					Type: cty.Map(cty.String),
				},
				"default": {
					Name:    "default",
					Type:    cty.Object(map[string]cty.Type{"foo": cty.String}), // Type is inferred from the default value.
					Default: ptrCtyVal(cty.ObjectVal(map[string]cty.Value{"foo": cty.StringVal("bar")})),
				},
				"type_and_default": {
					Name:    "type_and_default",
					Type:    cty.Map(cty.String),
					Default: ptrCtyVal(cty.MapVal(map[string]cty.Value{"foo": cty.StringVal("bar")})),
				},
			},
		},
		{
			name: "default doesn't match type",
			inputVariables: []testVariable{
				{
					Name:    "invalid_default",
					Type:    ptr.String("string"),
					Default: ptrCtyVal(cty.ListVal([]cty.Value{cty.StringVal("value")})),
				},
			},
			expectError: "Invalid default value for variable \"invalid_default\"",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Create a test config.
			configData, err := createTestConfig(tc.inputVariables)
			require.NoError(t, err)

			// Decode the test config.
			var config testConfig
			err = hclsimple.Decode("test.hcl", configData, nil, &config)
			require.NoError(t, err)

			decodedMap, diags := DecodeVariables(config.GetVariableBodies())

			if tc.expectError != "" {
				assert.Contains(t, diags.Error(), tc.expectError)
				return
			}

			if diags.HasErrors() {
				t.Fatal(diags.Error())
			}

			assert.Equal(t, tc.expectVariables, decodedMap)
		})
	}
}

// createTestConfig creates a test config with the given variables.
// Produces:
//
//	variable "example" {
//	   type    = string
//	   default = "value"
//	}
func createTestConfig(variables []testVariable) ([]byte, error) {
	f := hclwrite.NewEmptyFile()
	root := f.Body()

	for _, v := range variables {
		variableBlock := hclwrite.NewBlock("variable", []string{v.Name})
		variableBody := variableBlock.Body()

		if v.Type != nil {
			variableBody.SetAttributeRaw("type", hclwrite.TokensForIdentifier(*v.Type))
		}

		if v.Default != nil {
			variableBody.SetAttributeValue("default", *v.Default)
		}

		root.AppendBlock(variableBlock)
	}

	return f.Bytes(), nil
}

// ptrCtyVal returns a pointer to the given cty.Value.
func ptrCtyVal(v cty.Value) *cty.Value {
	return &v
}
