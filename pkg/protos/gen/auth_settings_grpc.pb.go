// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.5.1
// - protoc             v4.25.6
// source: auth_settings.proto

package gen

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.64.0 or later.
const _ = grpc.SupportPackageIsVersion9

const (
	AuthSettings_GetAuthSettings_FullMethodName = "/martiancloud.phobos.api.authsettings.AuthSettings/GetAuthSettings"
)

// AuthSettingsClient is the client API for AuthSettings service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
//
// AuthSettings implements functionality related to Phobos IDP auth settings.
type AuthSettingsClient interface {
	// GetAuthSettings returns the IDP auth settings.
	GetAuthSettings(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetAuthSettingsResponse, error)
}

type authSettingsClient struct {
	cc grpc.ClientConnInterface
}

func NewAuthSettingsClient(cc grpc.ClientConnInterface) AuthSettingsClient {
	return &authSettingsClient{cc}
}

func (c *authSettingsClient) GetAuthSettings(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetAuthSettingsResponse, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(GetAuthSettingsResponse)
	err := c.cc.Invoke(ctx, AuthSettings_GetAuthSettings_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AuthSettingsServer is the server API for AuthSettings service.
// All implementations must embed UnimplementedAuthSettingsServer
// for forward compatibility.
//
// AuthSettings implements functionality related to Phobos IDP auth settings.
type AuthSettingsServer interface {
	// GetAuthSettings returns the IDP auth settings.
	GetAuthSettings(context.Context, *emptypb.Empty) (*GetAuthSettingsResponse, error)
	mustEmbedUnimplementedAuthSettingsServer()
}

// UnimplementedAuthSettingsServer must be embedded to have
// forward compatible implementations.
//
// NOTE: this should be embedded by value instead of pointer to avoid a nil
// pointer dereference when methods are called.
type UnimplementedAuthSettingsServer struct{}

func (UnimplementedAuthSettingsServer) GetAuthSettings(context.Context, *emptypb.Empty) (*GetAuthSettingsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAuthSettings not implemented")
}
func (UnimplementedAuthSettingsServer) mustEmbedUnimplementedAuthSettingsServer() {}
func (UnimplementedAuthSettingsServer) testEmbeddedByValue()                      {}

// UnsafeAuthSettingsServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AuthSettingsServer will
// result in compilation errors.
type UnsafeAuthSettingsServer interface {
	mustEmbedUnimplementedAuthSettingsServer()
}

func RegisterAuthSettingsServer(s grpc.ServiceRegistrar, srv AuthSettingsServer) {
	// If the following call pancis, it indicates UnimplementedAuthSettingsServer was
	// embedded by pointer and is nil.  This will cause panics if an
	// unimplemented method is ever invoked, so we test this at initialization
	// time to prevent it from happening at runtime later due to I/O.
	if t, ok := srv.(interface{ testEmbeddedByValue() }); ok {
		t.testEmbeddedByValue()
	}
	s.RegisterService(&AuthSettings_ServiceDesc, srv)
}

func _AuthSettings_GetAuthSettings_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthSettingsServer).GetAuthSettings(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: AuthSettings_GetAuthSettings_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthSettingsServer).GetAuthSettings(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

// AuthSettings_ServiceDesc is the grpc.ServiceDesc for AuthSettings service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var AuthSettings_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "martiancloud.phobos.api.authsettings.AuthSettings",
	HandlerType: (*AuthSettingsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetAuthSettings",
			Handler:    _AuthSettings_GetAuthSettings_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "auth_settings.proto",
}
