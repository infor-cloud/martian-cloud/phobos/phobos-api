// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.36.5
// 	protoc        v4.25.6
// source: team.proto

package gen

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
	unsafe "unsafe"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// TeamSortableField defines an enum that allows sorting a paginated list of
// Teams.
type TeamSortableField int32

const (
	TeamSortableField_NAME_ASC        TeamSortableField = 0
	TeamSortableField_NAME_DESC       TeamSortableField = 1
	TeamSortableField_UPDATED_AT_ASC  TeamSortableField = 2
	TeamSortableField_UPDATED_AT_DESC TeamSortableField = 3
)

// Enum value maps for TeamSortableField.
var (
	TeamSortableField_name = map[int32]string{
		0: "NAME_ASC",
		1: "NAME_DESC",
		2: "UPDATED_AT_ASC",
		3: "UPDATED_AT_DESC",
	}
	TeamSortableField_value = map[string]int32{
		"NAME_ASC":        0,
		"NAME_DESC":       1,
		"UPDATED_AT_ASC":  2,
		"UPDATED_AT_DESC": 3,
	}
)

func (x TeamSortableField) Enum() *TeamSortableField {
	p := new(TeamSortableField)
	*p = x
	return p
}

func (x TeamSortableField) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (TeamSortableField) Descriptor() protoreflect.EnumDescriptor {
	return file_team_proto_enumTypes[0].Descriptor()
}

func (TeamSortableField) Type() protoreflect.EnumType {
	return &file_team_proto_enumTypes[0]
}

func (x TeamSortableField) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use TeamSortableField.Descriptor instead.
func (TeamSortableField) EnumDescriptor() ([]byte, []int) {
	return file_team_proto_rawDescGZIP(), []int{0}
}

// GetTeamByIdRequest is the input for retrieving a Team by
// its ID.
type GetTeamByIdRequest struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	Id            string                 `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *GetTeamByIdRequest) Reset() {
	*x = GetTeamByIdRequest{}
	mi := &file_team_proto_msgTypes[0]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *GetTeamByIdRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetTeamByIdRequest) ProtoMessage() {}

func (x *GetTeamByIdRequest) ProtoReflect() protoreflect.Message {
	mi := &file_team_proto_msgTypes[0]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetTeamByIdRequest.ProtoReflect.Descriptor instead.
func (*GetTeamByIdRequest) Descriptor() ([]byte, []int) {
	return file_team_proto_rawDescGZIP(), []int{0}
}

func (x *GetTeamByIdRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

// GetTeamsRequest is the input for retrieving a paginated list of
// Teams.
type GetTeamsRequest struct {
	state             protoimpl.MessageState `protogen:"open.v1"`
	PaginationOptions *PaginationOptions     `protobuf:"bytes,1,opt,name=pagination_options,json=paginationOptions,proto3,oneof" json:"pagination_options,omitempty"`
	Sort              *TeamSortableField     `protobuf:"varint,2,opt,name=sort,proto3,enum=martiancloud.phobos.api.team.TeamSortableField,oneof" json:"sort,omitempty"`
	Search            *string                `protobuf:"bytes,3,opt,name=search,proto3,oneof" json:"search,omitempty"`
	unknownFields     protoimpl.UnknownFields
	sizeCache         protoimpl.SizeCache
}

func (x *GetTeamsRequest) Reset() {
	*x = GetTeamsRequest{}
	mi := &file_team_proto_msgTypes[1]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *GetTeamsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetTeamsRequest) ProtoMessage() {}

func (x *GetTeamsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_team_proto_msgTypes[1]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetTeamsRequest.ProtoReflect.Descriptor instead.
func (*GetTeamsRequest) Descriptor() ([]byte, []int) {
	return file_team_proto_rawDescGZIP(), []int{1}
}

func (x *GetTeamsRequest) GetPaginationOptions() *PaginationOptions {
	if x != nil {
		return x.PaginationOptions
	}
	return nil
}

func (x *GetTeamsRequest) GetSort() TeamSortableField {
	if x != nil && x.Sort != nil {
		return *x.Sort
	}
	return TeamSortableField_NAME_ASC
}

func (x *GetTeamsRequest) GetSearch() string {
	if x != nil && x.Search != nil {
		return *x.Search
	}
	return ""
}

// Team is a collection of human users that interact with the API.
type Team struct {
	state          protoimpl.MessageState `protogen:"open.v1"`
	Metadata       *ResourceMetadata      `protobuf:"bytes,1,opt,name=metadata,proto3" json:"metadata,omitempty"`
	Name           string                 `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Description    string                 `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	ScimExternalId *string                `protobuf:"bytes,4,opt,name=scim_external_id,json=scimExternalId,proto3,oneof" json:"scim_external_id,omitempty"`
	unknownFields  protoimpl.UnknownFields
	sizeCache      protoimpl.SizeCache
}

func (x *Team) Reset() {
	*x = Team{}
	mi := &file_team_proto_msgTypes[2]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *Team) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Team) ProtoMessage() {}

func (x *Team) ProtoReflect() protoreflect.Message {
	mi := &file_team_proto_msgTypes[2]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Team.ProtoReflect.Descriptor instead.
func (*Team) Descriptor() ([]byte, []int) {
	return file_team_proto_rawDescGZIP(), []int{2}
}

func (x *Team) GetMetadata() *ResourceMetadata {
	if x != nil {
		return x.Metadata
	}
	return nil
}

func (x *Team) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Team) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *Team) GetScimExternalId() string {
	if x != nil && x.ScimExternalId != nil {
		return *x.ScimExternalId
	}
	return ""
}

// GetTeamsResponse is the paginated list of Teams.
type GetTeamsResponse struct {
	state         protoimpl.MessageState `protogen:"open.v1"`
	PageInfo      *PageInfo              `protobuf:"bytes,1,opt,name=page_info,json=pageInfo,proto3" json:"page_info,omitempty"`
	Teams         []*Team                `protobuf:"bytes,2,rep,name=teams,proto3" json:"teams,omitempty"`
	unknownFields protoimpl.UnknownFields
	sizeCache     protoimpl.SizeCache
}

func (x *GetTeamsResponse) Reset() {
	*x = GetTeamsResponse{}
	mi := &file_team_proto_msgTypes[3]
	ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
	ms.StoreMessageInfo(mi)
}

func (x *GetTeamsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetTeamsResponse) ProtoMessage() {}

func (x *GetTeamsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_team_proto_msgTypes[3]
	if x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetTeamsResponse.ProtoReflect.Descriptor instead.
func (*GetTeamsResponse) Descriptor() ([]byte, []int) {
	return file_team_proto_rawDescGZIP(), []int{3}
}

func (x *GetTeamsResponse) GetPageInfo() *PageInfo {
	if x != nil {
		return x.PageInfo
	}
	return nil
}

func (x *GetTeamsResponse) GetTeams() []*Team {
	if x != nil {
		return x.Teams
	}
	return nil
}

var File_team_proto protoreflect.FileDescriptor

var file_team_proto_rawDesc = string([]byte{
	0x0a, 0x0a, 0x74, 0x65, 0x61, 0x6d, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1c, 0x6d, 0x61,
	0x72, 0x74, 0x69, 0x61, 0x6e, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68, 0x6f, 0x62, 0x6f,
	0x73, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x74, 0x65, 0x61, 0x6d, 0x1a, 0x0e, 0x6d, 0x65, 0x74, 0x61,
	0x64, 0x61, 0x74, 0x61, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x10, 0x70, 0x61, 0x67, 0x69,
	0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x24, 0x0a, 0x12,
	0x47, 0x65, 0x74, 0x54, 0x65, 0x61, 0x6d, 0x42, 0x79, 0x49, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02,
	0x69, 0x64, 0x22, 0x8e, 0x02, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x54, 0x65, 0x61, 0x6d, 0x73, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x69, 0x0a, 0x12, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x35, 0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e, 0x63, 0x6c, 0x6f, 0x75,
	0x64, 0x2e, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x70, 0x61, 0x67,
	0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x4f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x48, 0x00, 0x52, 0x11, 0x70, 0x61, 0x67,
	0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x4f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x88, 0x01,
	0x01, 0x12, 0x48, 0x0a, 0x04, 0x73, 0x6f, 0x72, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32,
	0x2f, 0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70,
	0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x74, 0x65, 0x61, 0x6d, 0x2e, 0x54,
	0x65, 0x61, 0x6d, 0x53, 0x6f, 0x72, 0x74, 0x61, 0x62, 0x6c, 0x65, 0x46, 0x69, 0x65, 0x6c, 0x64,
	0x48, 0x01, 0x52, 0x04, 0x73, 0x6f, 0x72, 0x74, 0x88, 0x01, 0x01, 0x12, 0x1b, 0x0a, 0x06, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x48, 0x02, 0x52, 0x06, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x88, 0x01, 0x01, 0x42, 0x15, 0x0a, 0x13, 0x5f, 0x70, 0x61, 0x67,
	0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x42,
	0x07, 0x0a, 0x05, 0x5f, 0x73, 0x6f, 0x72, 0x74, 0x42, 0x09, 0x0a, 0x07, 0x5f, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x22, 0xd0, 0x01, 0x0a, 0x04, 0x54, 0x65, 0x61, 0x6d, 0x12, 0x4e, 0x0a, 0x08,
	0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x32,
	0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68,
	0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74,
	0x61, 0x2e, 0x52, 0x65, 0x73, 0x6f, 0x75, 0x72, 0x63, 0x65, 0x4d, 0x65, 0x74, 0x61, 0x64, 0x61,
	0x74, 0x61, 0x52, 0x08, 0x6d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0x12, 0x12, 0x0a, 0x04,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65,
	0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x12, 0x2d, 0x0a, 0x10, 0x73, 0x63, 0x69, 0x6d, 0x5f, 0x65, 0x78, 0x74, 0x65, 0x72,
	0x6e, 0x61, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x48, 0x00, 0x52, 0x0e,
	0x73, 0x63, 0x69, 0x6d, 0x45, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x49, 0x64, 0x88, 0x01,
	0x01, 0x42, 0x13, 0x0a, 0x11, 0x5f, 0x73, 0x63, 0x69, 0x6d, 0x5f, 0x65, 0x78, 0x74, 0x65, 0x72,
	0x6e, 0x61, 0x6c, 0x5f, 0x69, 0x64, 0x22, 0x97, 0x01, 0x0a, 0x10, 0x47, 0x65, 0x74, 0x54, 0x65,
	0x61, 0x6d, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x49, 0x0a, 0x09, 0x70,
	0x61, 0x67, 0x65, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2c,
	0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68,
	0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x2e, 0x50, 0x61, 0x67, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x52, 0x08, 0x70, 0x61,
	0x67, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x38, 0x0a, 0x05, 0x74, 0x65, 0x61, 0x6d, 0x73, 0x18,
	0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e, 0x63,
	0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x74, 0x65, 0x61, 0x6d, 0x2e, 0x54, 0x65, 0x61, 0x6d, 0x52, 0x05, 0x74, 0x65, 0x61, 0x6d, 0x73,
	0x2a, 0x59, 0x0a, 0x11, 0x54, 0x65, 0x61, 0x6d, 0x53, 0x6f, 0x72, 0x74, 0x61, 0x62, 0x6c, 0x65,
	0x46, 0x69, 0x65, 0x6c, 0x64, 0x12, 0x0c, 0x0a, 0x08, 0x4e, 0x41, 0x4d, 0x45, 0x5f, 0x41, 0x53,
	0x43, 0x10, 0x00, 0x12, 0x0d, 0x0a, 0x09, 0x4e, 0x41, 0x4d, 0x45, 0x5f, 0x44, 0x45, 0x53, 0x43,
	0x10, 0x01, 0x12, 0x12, 0x0a, 0x0e, 0x55, 0x50, 0x44, 0x41, 0x54, 0x45, 0x44, 0x5f, 0x41, 0x54,
	0x5f, 0x41, 0x53, 0x43, 0x10, 0x02, 0x12, 0x13, 0x0a, 0x0f, 0x55, 0x50, 0x44, 0x41, 0x54, 0x45,
	0x44, 0x5f, 0x41, 0x54, 0x5f, 0x44, 0x45, 0x53, 0x43, 0x10, 0x03, 0x32, 0xdb, 0x01, 0x0a, 0x05,
	0x54, 0x65, 0x61, 0x6d, 0x73, 0x12, 0x65, 0x0a, 0x0b, 0x47, 0x65, 0x74, 0x54, 0x65, 0x61, 0x6d,
	0x42, 0x79, 0x49, 0x44, 0x12, 0x30, 0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e, 0x63, 0x6c,
	0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x74,
	0x65, 0x61, 0x6d, 0x2e, 0x47, 0x65, 0x74, 0x54, 0x65, 0x61, 0x6d, 0x42, 0x79, 0x49, 0x64, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e,
	0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70, 0x69,
	0x2e, 0x74, 0x65, 0x61, 0x6d, 0x2e, 0x54, 0x65, 0x61, 0x6d, 0x22, 0x00, 0x12, 0x6b, 0x0a, 0x08,
	0x47, 0x65, 0x74, 0x54, 0x65, 0x61, 0x6d, 0x73, 0x12, 0x2d, 0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69,
	0x61, 0x6e, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61,
	0x70, 0x69, 0x2e, 0x74, 0x65, 0x61, 0x6d, 0x2e, 0x47, 0x65, 0x74, 0x54, 0x65, 0x61, 0x6d, 0x73,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x2e, 0x2e, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61,
	0x6e, 0x63, 0x6c, 0x6f, 0x75, 0x64, 0x2e, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2e, 0x61, 0x70,
	0x69, 0x2e, 0x74, 0x65, 0x61, 0x6d, 0x2e, 0x47, 0x65, 0x74, 0x54, 0x65, 0x61, 0x6d, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x47, 0x5a, 0x45, 0x67, 0x69, 0x74,
	0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x69, 0x6e, 0x66, 0x6f, 0x72, 0x2d, 0x63, 0x6c,
	0x6f, 0x75, 0x64, 0x2f, 0x6d, 0x61, 0x72, 0x74, 0x69, 0x61, 0x6e, 0x2d, 0x63, 0x6c, 0x6f, 0x75,
	0x64, 0x2f, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2f, 0x70, 0x68, 0x6f, 0x62, 0x6f, 0x73, 0x2d,
	0x61, 0x70, 0x69, 0x2f, 0x70, 0x6b, 0x67, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x73, 0x2f, 0x67,
	0x65, 0x6e, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
})

var (
	file_team_proto_rawDescOnce sync.Once
	file_team_proto_rawDescData []byte
)

func file_team_proto_rawDescGZIP() []byte {
	file_team_proto_rawDescOnce.Do(func() {
		file_team_proto_rawDescData = protoimpl.X.CompressGZIP(unsafe.Slice(unsafe.StringData(file_team_proto_rawDesc), len(file_team_proto_rawDesc)))
	})
	return file_team_proto_rawDescData
}

var file_team_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_team_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_team_proto_goTypes = []any{
	(TeamSortableField)(0),     // 0: martiancloud.phobos.api.team.TeamSortableField
	(*GetTeamByIdRequest)(nil), // 1: martiancloud.phobos.api.team.GetTeamByIdRequest
	(*GetTeamsRequest)(nil),    // 2: martiancloud.phobos.api.team.GetTeamsRequest
	(*Team)(nil),               // 3: martiancloud.phobos.api.team.Team
	(*GetTeamsResponse)(nil),   // 4: martiancloud.phobos.api.team.GetTeamsResponse
	(*PaginationOptions)(nil),  // 5: martiancloud.phobos.api.pagination.PaginationOptions
	(*ResourceMetadata)(nil),   // 6: martiancloud.phobos.api.metadata.ResourceMetadata
	(*PageInfo)(nil),           // 7: martiancloud.phobos.api.pagination.PageInfo
}
var file_team_proto_depIdxs = []int32{
	5, // 0: martiancloud.phobos.api.team.GetTeamsRequest.pagination_options:type_name -> martiancloud.phobos.api.pagination.PaginationOptions
	0, // 1: martiancloud.phobos.api.team.GetTeamsRequest.sort:type_name -> martiancloud.phobos.api.team.TeamSortableField
	6, // 2: martiancloud.phobos.api.team.Team.metadata:type_name -> martiancloud.phobos.api.metadata.ResourceMetadata
	7, // 3: martiancloud.phobos.api.team.GetTeamsResponse.page_info:type_name -> martiancloud.phobos.api.pagination.PageInfo
	3, // 4: martiancloud.phobos.api.team.GetTeamsResponse.teams:type_name -> martiancloud.phobos.api.team.Team
	1, // 5: martiancloud.phobos.api.team.Teams.GetTeamByID:input_type -> martiancloud.phobos.api.team.GetTeamByIdRequest
	2, // 6: martiancloud.phobos.api.team.Teams.GetTeams:input_type -> martiancloud.phobos.api.team.GetTeamsRequest
	3, // 7: martiancloud.phobos.api.team.Teams.GetTeamByID:output_type -> martiancloud.phobos.api.team.Team
	4, // 8: martiancloud.phobos.api.team.Teams.GetTeams:output_type -> martiancloud.phobos.api.team.GetTeamsResponse
	7, // [7:9] is the sub-list for method output_type
	5, // [5:7] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_team_proto_init() }
func file_team_proto_init() {
	if File_team_proto != nil {
		return
	}
	file_metadata_proto_init()
	file_pagination_proto_init()
	file_team_proto_msgTypes[1].OneofWrappers = []any{}
	file_team_proto_msgTypes[2].OneofWrappers = []any{}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: unsafe.Slice(unsafe.StringData(file_team_proto_rawDesc), len(file_team_proto_rawDesc)),
			NumEnums:      1,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_team_proto_goTypes,
		DependencyIndexes: file_team_proto_depIdxs,
		EnumInfos:         file_team_proto_enumTypes,
		MessageInfos:      file_team_proto_msgTypes,
	}.Build()
	File_team_proto = out.File
	file_team_proto_goTypes = nil
	file_team_proto_depIdxs = nil
}
