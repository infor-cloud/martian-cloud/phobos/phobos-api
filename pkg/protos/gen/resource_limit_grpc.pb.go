// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.5.1
// - protoc             v4.25.6
// source: resource_limit.proto

package gen

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.64.0 or later.
const _ = grpc.SupportPackageIsVersion9

const (
	ResourceLimits_GetResourceLimits_FullMethodName   = "/martiancloud.phobos.api.resourcelimit.ResourceLimits/GetResourceLimits"
	ResourceLimits_UpdateResourceLimit_FullMethodName = "/martiancloud.phobos.api.resourcelimit.ResourceLimits/UpdateResourceLimit"
)

// ResourceLimitsClient is the client API for ResourceLimits service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
//
// ResourceLimits implements all functionality related to a Phobos
// ResourceLimit.
type ResourceLimitsClient interface {
	// GetResourceLimits returns a slice of ResourceLimits.
	GetResourceLimits(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetResourceLimitsResponse, error)
	// UpdateResourceLimit returns the updated ResourceLimit.
	UpdateResourceLimit(ctx context.Context, in *UpdateResourceLimitRequest, opts ...grpc.CallOption) (*ResourceLimit, error)
}

type resourceLimitsClient struct {
	cc grpc.ClientConnInterface
}

func NewResourceLimitsClient(cc grpc.ClientConnInterface) ResourceLimitsClient {
	return &resourceLimitsClient{cc}
}

func (c *resourceLimitsClient) GetResourceLimits(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetResourceLimitsResponse, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(GetResourceLimitsResponse)
	err := c.cc.Invoke(ctx, ResourceLimits_GetResourceLimits_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *resourceLimitsClient) UpdateResourceLimit(ctx context.Context, in *UpdateResourceLimitRequest, opts ...grpc.CallOption) (*ResourceLimit, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(ResourceLimit)
	err := c.cc.Invoke(ctx, ResourceLimits_UpdateResourceLimit_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ResourceLimitsServer is the server API for ResourceLimits service.
// All implementations must embed UnimplementedResourceLimitsServer
// for forward compatibility.
//
// ResourceLimits implements all functionality related to a Phobos
// ResourceLimit.
type ResourceLimitsServer interface {
	// GetResourceLimits returns a slice of ResourceLimits.
	GetResourceLimits(context.Context, *emptypb.Empty) (*GetResourceLimitsResponse, error)
	// UpdateResourceLimit returns the updated ResourceLimit.
	UpdateResourceLimit(context.Context, *UpdateResourceLimitRequest) (*ResourceLimit, error)
	mustEmbedUnimplementedResourceLimitsServer()
}

// UnimplementedResourceLimitsServer must be embedded to have
// forward compatible implementations.
//
// NOTE: this should be embedded by value instead of pointer to avoid a nil
// pointer dereference when methods are called.
type UnimplementedResourceLimitsServer struct{}

func (UnimplementedResourceLimitsServer) GetResourceLimits(context.Context, *emptypb.Empty) (*GetResourceLimitsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetResourceLimits not implemented")
}
func (UnimplementedResourceLimitsServer) UpdateResourceLimit(context.Context, *UpdateResourceLimitRequest) (*ResourceLimit, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateResourceLimit not implemented")
}
func (UnimplementedResourceLimitsServer) mustEmbedUnimplementedResourceLimitsServer() {}
func (UnimplementedResourceLimitsServer) testEmbeddedByValue()                        {}

// UnsafeResourceLimitsServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ResourceLimitsServer will
// result in compilation errors.
type UnsafeResourceLimitsServer interface {
	mustEmbedUnimplementedResourceLimitsServer()
}

func RegisterResourceLimitsServer(s grpc.ServiceRegistrar, srv ResourceLimitsServer) {
	// If the following call pancis, it indicates UnimplementedResourceLimitsServer was
	// embedded by pointer and is nil.  This will cause panics if an
	// unimplemented method is ever invoked, so we test this at initialization
	// time to prevent it from happening at runtime later due to I/O.
	if t, ok := srv.(interface{ testEmbeddedByValue() }); ok {
		t.testEmbeddedByValue()
	}
	s.RegisterService(&ResourceLimits_ServiceDesc, srv)
}

func _ResourceLimits_GetResourceLimits_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ResourceLimitsServer).GetResourceLimits(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: ResourceLimits_GetResourceLimits_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ResourceLimitsServer).GetResourceLimits(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _ResourceLimits_UpdateResourceLimit_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateResourceLimitRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ResourceLimitsServer).UpdateResourceLimit(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: ResourceLimits_UpdateResourceLimit_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ResourceLimitsServer).UpdateResourceLimit(ctx, req.(*UpdateResourceLimitRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// ResourceLimits_ServiceDesc is the grpc.ServiceDesc for ResourceLimits service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ResourceLimits_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "martiancloud.phobos.api.resourcelimit.ResourceLimits",
	HandlerType: (*ResourceLimitsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetResourceLimits",
			Handler:    _ResourceLimits_GetResourceLimits_Handler,
		},
		{
			MethodName: "UpdateResourceLimit",
			Handler:    _ResourceLimits_UpdateResourceLimit_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "resource_limit.proto",
}
