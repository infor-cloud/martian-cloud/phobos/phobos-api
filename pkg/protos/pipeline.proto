syntax = "proto3";

package martiancloud.phobos.api.pipeline;

import "metadata.proto";
import "pagination.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

option go_package = "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen";

// Pipelines implements all functionality related to a Phobos Pipeline.
service Pipelines {
  // GetPipelineByID retrieves a Pipeline by it's ID.
  rpc GetPipelineByID(GetPipelineByIdRequest) returns (Pipeline) {};
  // GetPipelines retrieves a paginated list of Pipelines.
  rpc GetPipelines(GetPipelinesRequest) returns (GetPipelinesResponse) {};
  // CreatePipeline creates a new Pipeline.
  rpc CreatePipeline(CreatePipelineRequest) returns (Pipeline) {};
  // CancelPipeline cancels a Pipeline.
  rpc CancelPipeline(CancelPipelineRequest) returns (google.protobuf.Empty) {};
  rpc GetPipelineEventsStreaming(GetPipelineEventsStreamingRequest)
      returns (stream GetPipelineEventsStreamingResponse) {};
  // SetPipelineActionStatus sets the status of a Pipeline Action.
  rpc SetPipelineActionStatus(SetPipelineActionStatusRequest)
      returns (Pipeline) {};
  // SetPipelineActionOutputs sets the outputs of a Pipeline Action.
  rpc SetPipelineActionOutputs(SetPipelineActionOutputsRequest)
      returns (google.protobuf.Empty) {};
  // GetPipelineActionOutputs retrieves the outputs of a Pipeline Action.
  rpc GetPipelineActionOutputs(GetPipelineActionOutputsRequest)
      returns (GetPipelineActionOutputsResponse) {};
  // GetPipelineVariables retrieves the variables of a Pipeline.
  rpc GetPipelineVariables(GetPipelineVariablesRequest)
      returns (GetPipelineVariablesResponse) {};
  // CreatePipelineJWT creates a new Pipeline JWT
  rpc CreatePipelineJWT(CreatePipelineJWTRequest)
      returns (CreatePipelineJWTResponse) {};
  // RetryPipelineTask retries a Pipeline Task
  rpc RetryPipelineTask(RetryPipelineTaskRequest)
      returns (google.protobuf.Empty) {};
  // RunPipelineTask starts a Pipeline Task
  rpc RunPipelineTask(RunPipelineTaskRequest) returns (google.protobuf.Empty) {
  };
  // RunPipeline starts a Pipeline
  rpc RunPipeline(RunPipelineRequest) returns (google.protobuf.Empty) {};
  // RetryNestedPipeline retries a nested pipeline
  rpc RetryNestedPipeline(RetryNestedPipelineRequest)
      returns (google.protobuf.Empty) {};
  // ApprovePipeline approves a pipeline
  rpc ApprovePipeline(ApprovePipelineRequest) returns (google.protobuf.Empty) {
  };
  // RevokePipelineApproval revokes a pipeline approval
  rpc RevokePipelineApproval(RevokePipelineApprovalRequest)
      returns (google.protobuf.Empty) {};
  // ApprovePipelineTask approves a pipeline task
  rpc ApprovePipelineTask(ApprovePipelineTaskRequest)
      returns (google.protobuf.Empty) {};
  // RevokePipelineTaskApproval revokes a pipeline task approval
  rpc RevokePipelineTaskApproval(RevokePipelineTaskApprovalRequest)
      returns (google.protobuf.Empty) {};
  // SchedulePipelineNode schedules a pipeline node
  rpc SchedulePipelineNode(SchedulePipelineNodeRequest)
      returns (google.protobuf.Empty) {};
  // CancelPipelineNodeSchedule cancels a scheduled pipeline node
  rpc CancelPipelineNodeSchedule(CancelPipelineNodeScheduleRequest)
      returns (google.protobuf.Empty) {};
  // DeferPipelineNode defers a pipeline node
  rpc DeferPipelineNode(DeferPipelineNodeRequest)
      returns (google.protobuf.Empty) {};
}

// PipelineSortableField defines enums that allow sorting a paginated list
// of Pipelines.
enum PipelineSortableField {
  UPDATED_AT_ASC = 0;
  UPDATED_AT_DESC = 1;
}

// PipelineNodeStatus defines the status of a Pipeline Node.
enum PipelineNodeStatus {
  CREATED = 0;
  APPROVAL_PENDING = 1;
  WAITING = 2;
  READY = 3;
  PENDING = 4;
  RUNNING = 5;
  FAILED = 6;
  CANCELED = 7;
  SKIPPED = 8;
  SUCCEEDED = 9;
  CANCELING = 10;
  DEFERRED = 11;
}

// PipelineType defines the type of a Pipeline.
enum PipelineType {
  RELEASE_LIFECYCLE = 0;
  RUNBOOK = 1;
  NESTED = 2;
  DEPLOYMENT = 3;
}

// PipelineOnErrorStrategy defines the strategy to follow when an error occurs
// in a Pipeline node.
enum PipelineOnErrorStrategy {
  FAIL = 0;
  CONTINUE = 1;
}

// PipelineNodeType defines the type of a Pipeline Node.
enum PipelineNodeType {
  PIPELINE = 0;
  STAGE = 1;
  TASK = 2;
  ACTION = 3;
}

// CreatePipelineJWTRequest is the input for creating a new Pipeline JWT.
message CreatePipelineJWTRequest {
  string pipeline_id = 1;
  string audience = 2;
  google.protobuf.Timestamp expiration = 3;
}

// GetPipelineEventsStreamingRequest is the input for streaming Pipeline events.
message GetPipelineEventsStreamingRequest {
  string pipeline_id = 1;
  optional string last_seen_version = 2;
}

// SetPipelineActionStatusRequest is the input for setting the status of a
// Pipeline Action.
message SetPipelineActionStatusRequest {
  string pipeline_id = 1;
  string action_path = 2;
  PipelineNodeStatus status = 3;
}

// GetPipelineActionOutputsRequest is the input for retrieving the outputs of a
// Pipeline Action.
message GetPipelineActionOutputsRequest {
  string pipeline_id = 1;
  repeated string action_paths = 2;
}

// SetPipelineActionOutputsRequest is the input for setting the outputs of a
// Pipeline Action.
message SetPipelineActionOutputsRequest {
  string pipeline_id = 1;
  string action_path = 2;
  repeated PipelineActionOutput outputs = 3;
}

// GetPipelineByIdRequest is the input for retrieving a Pipeline by
// it's ID.
message GetPipelineByIdRequest { string id = 1; }

// GetPipelinesRequest is the input for retrieving a paginated list of
// Pipelines.
message GetPipelinesRequest {
  string project_id = 1;
  optional pagination.PaginationOptions pagination_options = 2;
  optional PipelineSortableField sort = 3;
  optional string pipeline_template_id = 4;
}

// CreatePipelineRequest is the input for creating a new Pipeline.
message CreatePipelineRequest {
  PipelineType type = 1;
  string pipeline_template_id = 2;
  repeated PipelineVariable hcl_variables = 3;
  optional string environment_name = 4;
  repeated PipelineAnnotation annotations = 5;
  repeated PipelineVariable environment_variables = 6;
  optional string variable_set_revision = 7;
}

// CancelPipelineRequest is the input for canceling a Pipeline.
message CancelPipelineRequest {
  string id = 1;
  optional string version = 2;
  bool force = 3;
}

// GetPipelineVariablesRequest is the input for retrieving the variables
// of a Pipeline.
message GetPipelineVariablesRequest { string pipeline_id = 1; }

// RetryPipelineTaskRequest is the input for retrying a Pipeline Task.
message RetryPipelineTaskRequest {
  string pipeline_id = 1;
  string task_path = 2;
}

// RunPipelineTaskRequest is the input for starting a Pipeline Task.
message RunPipelineTaskRequest {
  string pipeline_id = 1;
  string task_path = 2;
}

// RunPipelineRequest is the input for starting a Pipeline.
message RunPipelineRequest { string id = 1; }

// RetryNestedPipelineRequest is the input for retrying a nested pipeline.
message RetryNestedPipelineRequest {
  string parent_pipeline_id = 1;
  string parent_nested_pipeline_node_path = 2;
}

// ApprovePipelineRequest is the input for approving a pipeline.
message ApprovePipelineRequest { string pipeline_id = 1; }

// RevokePipelineApprovalRequest is the input for revoking a pipeline approval.
message RevokePipelineApprovalRequest { string pipeline_id = 1; }

// ApprovePipelineTaskRequest is the input for approving a pipeline task.
message ApprovePipelineTaskRequest {
  string pipeline_id = 1;
  string task_path = 2;
}

// RevokePipelineTaskApprovalRequest is the input for revoking a pipeline task
// approval.
message RevokePipelineTaskApprovalRequest {
  string pipeline_id = 1;
  string task_path = 2;
}

// CronScheduleInput is the input for a cron schedule.
message CronScheduleInput {
  string expression = 1;
  optional string timezone = 2;
}

// SchedulePipelineNodeRequest is the input for scheduling a pipeline node.
message SchedulePipelineNodeRequest {
  string pipeline_id = 1;
  string node_path = 2;
  PipelineNodeType node_type = 3;
  optional google.protobuf.Timestamp scheduled_start_time = 4;
  optional CronScheduleInput cron_schedule = 5;
}

// CancelPipelineNodeScheduleRequest is the input for canceling a scheduled
// pipeline node.
message CancelPipelineNodeScheduleRequest {
  string pipeline_id = 1;
  string node_path = 2;
  PipelineNodeType node_type = 3;
}

// DeferPipelineNodeRequest is the input for deferring a pipeline node.
message DeferPipelineNodeRequest {
  string pipeline_id = 1;
  string node_path = 2;
  PipelineNodeType node_type = 3;
  string reason = 4;
}

// Pipelines define the orchestration that is required to deploy a release to a
// particular environment.
message Pipeline {
  metadata.ResourceMetadata metadata = 1;
  string created_by = 2;
  string project_id = 3;
  string pipeline_template_id = 4;
  repeated PipelineStage stages = 5;
  PipelineNodeStatus status = 6;
  optional string parent_pipeline_id = 7;
  optional string release_id = 8;
  string type = 9;
  string when = 10;
  optional string parent_pipeline_node_path = 11;
  bool superseded = 12;
  repeated string approval_rule_ids = 13;
  optional string approval_status = 14;
}

// PipelineAction defines an action in a Pipeline.
message PipelineAction {
  string path = 1;
  string name = 2;
  PipelineNodeStatus status = 3;
}

// PipelineTask defines a task in a Pipeline.
message PipelineTask {
  string path = 1;
  string name = 2;
  string when = 3;
  bool has_success_condition = 4;
  PipelineNodeStatus status = 5;
  repeated PipelineAction actions = 6;
  optional int32 interval = 7;
  optional string latest_job_id = 8;
  repeated string approval_rule_ids = 9;
  optional string approval_status = 10;
  repeated string dependencies = 11;
  repeated string errors = 12;
  optional string image = 13;
  PipelineOnErrorStrategy on_error = 14;
  optional PipelineCronSchedule cron_schedule = 15;
  int32 attempt_count = 16;
}

// NestedPipeline defines a nested pipeline in a Pipeline.
message NestedPipeline {
  string path = 1;
  string name = 2;
  optional string environment_name = 3;
  string latest_pipeline_id = 4;
  PipelineNodeStatus status = 5;
  optional string approval_status = 6;
  repeated string dependencies = 7;
  repeated string errors = 8;
  PipelineOnErrorStrategy on_error = 9;
  optional PipelineCronSchedule cron_schedule = 10;
}

// PipelineStage defines a stage in a Pipeline.
message PipelineStage {
  string path = 1;
  string name = 2;
  PipelineNodeStatus status = 3;
  repeated PipelineTask tasks = 4;
  repeated NestedPipeline nested_pipelines = 5;
}

// PipelineActionOutput defines the output of a Pipeline Action.
message PipelineActionOutput {
  string name = 2;
  bytes type = 3;
  bytes value = 4;
}

// GetPipelineActionOutputsResponse is the output for retrieving the outputs of
// a
message PipelineActionOutputs {
  string action_path = 1;
  repeated PipelineActionOutput outputs = 2;
}

// PipelineVariable defines a key/value pair that can be used to
// parameterize a Pipeline.
message PipelineVariable {
  string key = 1;
  string value = 2;
}

// PipelineAnnotation defines a key/value pair that can be used to
// annotate a Pipeline.
message PipelineAnnotation {
  string key = 1;
  string value = 2;
  optional string link = 3;
}

// PipelineCronSchedule defines a cron expression and optional timezone for
// scheduling a Pipeline Node.
message PipelineCronSchedule {
  string expression = 1;
  string timezone = 2;
}

// CreatePipelineJWTResponse is the output for creating a new Pipeline JWT.
message CreatePipelineJWTResponse { string token = 1; }

// GetPipelineEventsStreamingResponse is the output for streaming Pipeline
// events.
message GetPipelineEventsStreamingResponse { Pipeline pipeline = 1; }

// GetPipelineActionOutputsResponse is the output for retrieving the outputs of
// a Pipeline Action.
message GetPipelineActionOutputsResponse {
  repeated PipelineActionOutputs actions = 1;
}

// GetPipelinesResponse is the paginated list of Pipelines
message GetPipelinesResponse {
  pagination.PageInfo page_info = 1;
  repeated Pipeline pipelines = 2;
}

// GetPipelineVariablesResponse is the output for retrieving the variables
// of a Pipeline.
message GetPipelineVariablesResponse {
  repeated PipelineVariable hcl_variables = 1;
  repeated PipelineVariable environment_variables = 2;
}
