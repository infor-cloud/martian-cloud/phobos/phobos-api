syntax = "proto3";

package martiancloud.phobos.api.pluginregistry;

import "metadata.proto";
import "pagination.proto";
import "google/protobuf/empty.proto";

option go_package = "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen";

// PluginRegistry is the service that provides methods to manage plugins.
service PluginRegistry {
  // GetPluginByID returns a plugin by its ID.
  rpc GetPluginByID(GetPluginByIdRequest) returns (Plugin) {};
  // GetPlugins returns a paginated list of plugins.
  rpc GetPlugins(GetPluginsRequest) returns (GetPluginsResponse) {};
  // GetPluginVersionByID returns a plugin version by its ID.
  rpc GetPluginVersionByID(GetPluginVersionByIdRequest) returns (PluginVersion);
  // GetPluginVersions returns a list of plugin versions.
  rpc GetPluginVersions(GetPluginVersionsRequest)
      returns (GetPluginVersionsResponse);
  // GetPluginPlatformByID returns a plugin platform by its ID.
  rpc GetPluginPlatformByID(GetPluginPlatformByIdRequest)
      returns (PluginPlatform);
  // GetPluginPlatforms returns a list of plugin platforms.
  rpc GetPluginPlatforms(GetPluginPlatformsRequest)
      returns (GetPluginPlatformsResponse);
  // CreatePlugin creates a new plugin.
  rpc CreatePlugin(CreatePluginRequest) returns (Plugin) {};
  // CreatePluginVersion creates a new plugin version.
  rpc CreatePluginVersion(CreatePluginVersionRequest) returns (PluginVersion);
  // CreatePluginPlatform creates a new plugin platform.
  rpc CreatePluginPlatform(CreatePluginPlatformRequest)
      returns (PluginPlatform);
  // UpdatePlugin updates a plugin.
  rpc UpdatePlugin(UpdatePluginRequest) returns (Plugin) {};
  rpc UploadPluginVersionDocFile(stream UploadPluginVersionDocFileRequest)
      returns (google.protobuf.Empty);
  // UploadPluginVersionReadme uploads a readme for a plugin version.
  rpc UploadPluginVersionReadme(stream UploadPluginVersionReadmeRequest)
      returns (google.protobuf.Empty);
  rpc UploadPluginVersionSchema(stream UploadPluginVersionSchemaRequest)
      returns (google.protobuf.Empty);
  // UploadPluginVersionShaSums uploads SHA sums for a plugin version.
  rpc UploadPluginVersionShaSums(stream UploadPluginVersionShaSumsRequest)
      returns (google.protobuf.Empty);
  // UploadPluginPlatformBinary uploads a plugin platform binary.
  rpc UploadPluginPlatformBinary(stream UploadPluginPlatformBinaryRequest)
      returns (google.protobuf.Empty);
  // DeletePlugin deletes a plugin.
  rpc DeletePlugin(DeletePluginRequest) returns (google.protobuf.Empty) {};
  // DeletePluginVersion deletes a plugin version by its ID.
  rpc DeletePluginVersion(DeletePluginVersionRequest)
      returns (google.protobuf.Empty);
  // DeletePluginPlatform deletes a plugin platform by its ID.
  rpc DeletePluginPlatform(DeletePluginPlatformRequest)
      returns (google.protobuf.Empty);
}

// PluginSortableField is the enum that represents the fields that can be used
// to sort plugins.
enum PluginSortableField {
  Plugin_NAME_ASC = 0;
  Plugin_NAME_DESC = 1;
  Plugin_UPDATED_AT_ASC = 2;
  Plugin_UPDATED_AT_DESC = 3;
}

// PluginVersionSortableField is the enum that represents the fields that can be
// used to sort plugin versions.
enum PluginVersionSortableField {
  PluginVersion_UPDATED_AT_ASC = 0;
  PluginVersion_UPDATED_AT_DESC = 1;
  PluginVersion_CREATED_AT_ASC = 2;
  PluginVersion_CREATED_AT_DESC = 3;
}

// PluginPlatformSortableField is the enum that represents the fields that can
// be used to sort plugin platforms.
enum PluginPlatformSortableField {
  PluginPlatform_UPDATED_AT_ASC = 0;
  PluginPlatform_UPDATED_AT_DESC = 1;
}

// GetPluginByIdRequest is the request for the GetPluginByID method.
message GetPluginByIdRequest { string id = 1; }

// GetPluginsRequest is the request for the GetPlugins method.
message GetPluginsRequest {
  optional pagination.PaginationOptions pagination_options = 1;
  optional PluginSortableField sort = 2;
  optional string search = 3;
  optional string organization_id = 4;
}

// GetPluginVersionByIdRequest is the request for the GetPluginVersionByID RPC.
message GetPluginVersionByIdRequest { string id = 1; }

// GetPluginVersionsRequest is the request for the GetPluginVersions method.
message GetPluginVersionsRequest {
  optional pagination.PaginationOptions pagination_options = 1;
  optional PluginVersionSortableField sort = 2;
  string plugin_id = 3;
  optional string semantic_version = 4;
  optional bool sha_sums_uploaded = 5;
  optional bool latest = 6;
}

// GetPluginPlatformByIdRequest is the request for the GetPluginPlatformByID
// method.
message GetPluginPlatformByIdRequest { string id = 1; }

// GetPluginPlatformsRequest is the request for the GetPluginPlatforms method.
message GetPluginPlatformsRequest {
  optional pagination.PaginationOptions pagination_options = 1;
  optional PluginPlatformSortableField sort = 2;
  optional string plugin_version_id = 3;
  optional string plugin_id = 4;
  optional string operating_system = 5;
  optional string architecture = 6;
  optional bool binary_uploaded = 7;
}

// CreatePluginRequest is the request for the CreatePlugin method.
message CreatePluginRequest {
  string name = 1;
  string organization_id = 2;
  string repository_url = 3;
  bool private = 4;
}

// CreatePluginVersionRequest is the request for the CreatePluginVersion method.
message CreatePluginVersionRequest {
  string plugin_id = 1;
  string semantic_version = 2;
  repeated string protocols = 3;
}

// CreatePluginPlatformRequest is the request for the CreatePluginPlatform
// method.
message CreatePluginPlatformRequest {
  string plugin_version_id = 1;
  string operating_system = 2;
  string architecture = 3;
  string sha_sum = 4;
  string filename = 5;
}

// UpdatePluginRequest is the request for the UpdatePlugin method.
message UpdatePluginRequest {
  string id = 1;
  optional string version = 2;
  optional string repository_url = 3;
  optional bool private = 4;
}

message UploadPluginVersionDocFileRequest {
  oneof data {
    UploadPluginVersionDocFileMetadata info = 1;
    bytes chunk_data = 2;
  };
}

enum PluginVersionDocFileCategory {
  OVERVIEW = 0;
  ACTION = 1;
  GUIDE = 2;
}

message UploadPluginVersionDocFileMetadata {
  string plugin_version_id = 1;
  string name = 2;
  PluginVersionDocFileCategory category = 3;
  string subcategory = 4;
  string title = 5;
}

// UploadPluginVersionReadmeRequest is the request for the
// UploadPluginVersionReadme method.
message UploadPluginVersionReadmeRequest {
  oneof data {
    UploadPluginVersionReadmeMetadata info = 1;
    bytes chunk_data = 2;
  };
}

// UploadPluginVersionReadmeMetadata is the metadata for the
// UploadPluginVersionReadmeRequest.
message UploadPluginVersionReadmeMetadata { string plugin_version_id = 1; }

message UploadPluginVersionSchemaRequest {
  oneof data {
    UploadPluginVersionReadmeMetadata info = 1;
    bytes chunk_data = 2;
  };
}

// UploadPluginVersionReadmeMetadata is the metadata for the
// UploadPluginVersionReadmeRequest.
message UploadPluginVersionSchemaMetadata { string plugin_version_id = 1; }

// UploadPluginVersionShaSumsRequest is the request for the
// UploadPluginVersionShaSums method.
message UploadPluginVersionShaSumsRequest {
  oneof data {
    UploadPluginVersionShaSumsMetadata info = 1;
    bytes chunk_data = 2;
  };
}

// UploadPluginVersionShaSumsMetadata is the metadata for the
// UploadPluginVersionShaSumsRequest.
message UploadPluginVersionShaSumsMetadata { string plugin_version_id = 1; }

// UploadPluginPlatformBinaryRequest is the request for the
// UploadPluginPlatformBinary method.
message UploadPluginPlatformBinaryRequest {
  oneof data {
    UploadPluginPlatformBinaryMetadata info = 1;
    bytes chunk_data = 2;
  };
}

// UploadPluginPlatformBinaryMetadata is the metadata for the binary upload.
message UploadPluginPlatformBinaryMetadata { string plugin_platform_id = 1; }

// DeletePluginRequest is the request for the DeletePlugin method.
message DeletePluginRequest {
  string id = 1;
  optional string version = 2;
}

// DeletePluginVersionRequest is the request for the DeletePluginVersion method.
message DeletePluginVersionRequest {
  string id = 1;
  optional string version = 2;
}

// DeletePluginPlatformRequest is the request for the DeletePluginPlatform
// method.
message DeletePluginPlatformRequest {
  string id = 1;
  optional string version = 2;
}

// Plugin is the model that represents a plugin.
message Plugin {
  metadata.ResourceMetadata metadata = 1;
  string name = 2;
  string created_by = 3;
  string organization_id = 4;
  string repository_url = 5;
  bool private = 6;
}

// PluginVersion is a version of a plugin.
message PluginVersion {
  metadata.ResourceMetadata metadata = 1;
  string plugin_id = 2;
  string created_by = 3;
  string semantic_version = 4;
  repeated string protocols = 5;
  bool sha_sums_uploaded = 6;
  bool readme_uploaded = 7;
  bool latest = 8;
}

// PluginPlatform is a platform that a plugin can run on.
message PluginPlatform {
  metadata.ResourceMetadata metadata = 1;
  string plugin_version_id = 2;
  string created_by = 3;
  string operating_system = 4;
  string architecture = 5;
  string sha_sum = 6;
  string filename = 7;
  bool binary_uploaded = 8;
}

// GetPluginsResponse is the response for the GetPlugins method.
message GetPluginsResponse {
  pagination.PageInfo page_info = 1;
  repeated Plugin plugins = 2;
}

// GetPluginVersionsResponse is the response for the GetPluginVersions method.
message GetPluginVersionsResponse {
  pagination.PageInfo page_info = 1;
  repeated PluginVersion plugin_versions = 2;
}

// GetPluginPlatformsResponse is the response for the GetPluginPlatforms method.
message GetPluginPlatformsResponse {
  pagination.PageInfo page_info = 1;
  repeated PluginPlatform plugin_platforms = 2;
}
