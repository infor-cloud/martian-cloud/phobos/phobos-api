syntax = "proto3";

package martiancloud.phobos.api.vcsprovider;

import "metadata.proto";
import "pagination.proto";
import "scope.proto";
import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";

option go_package = "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen";

// VCSProviders implements all functionality related to Version Control System
// Providers.
service VCSProviders {
  // GetVCSProviderByID returns a VCSProvider by its ID.
  rpc GetVCSProviderByID(GetVCSProviderByIDRequest) returns (VCSProvider) {};
  // GetVCSProviders returns a paginated list of VCSProviders.
  rpc GetVCSProviders(GetVCSProvidersRequest)
      returns (GetVCSProvidersResponse) {};
  // GetRepositoryArchive returns the archive for a given repository.
  rpc GetRepositoryArchive(GetRepositoryArchiveRequest)
      returns (stream GetRepositoryArchiveResponse) {};
  // CreateVCSProvider creates a new VCSProvider.
  rpc CreateVCSProvider(CreateVCSProviderRequest)
      returns (CreateVCSProviderResponse) {};
  // UpdateVCSProvider updates a existing VCSProvider.
  rpc UpdateVCSProvider(UpdateVCSProviderRequest) returns (VCSProvider) {};
  // DeleteVCSProvider deletes a VCSProvider.
  rpc DeleteVCSProvider(DeleteVCSProviderRequest)
      returns (google.protobuf.Empty) {};
  // ResetVCSProviderOAuthToken resets the OAuth token used to authenticate with
  // the remote VCS system.
  rpc ResetVCSProviderOAuthToken(ResetVCSProviderOAuthTokenRequest)
      returns (ResetVCSProviderOAuthTokenResponse) {};
  // CreateVCSTokenForPipeline creates a new VCS token for a pipeline.
  rpc CreateVCSTokenForPipeline(CreateVCSTokenForPipelineRequest)
      returns (CreateVCSTokenForPipelineResponse) {};
}

// VcsProviderSortableField defines enums that allow sorting a paginated list
// of VCSProviders.
enum VcsProviderSortableField {
  CREATED_AT_ASC = 0;
  CREATED_AT_DESC = 1;
  UPDATED_AT_ASC = 2;
  UPDATED_AT_DESC = 3;
}

// VcsProviderType represents the type of VCSProvider.
enum VcsProviderType {
  GITLAB = 0;
  GITHUB = 1;
}

// VcsProviderAuthType represents the authentication schema a VCSProvider uses.
enum VcsProviderAuthType {
  ACCESS_TOKEN = 0;
  OAUTH = 1;
}

// GetVCSProviderByIDRequest represents a request to get a VCSProvider by its
// ID.
message GetVCSProviderByIDRequest { string id = 1; }

// GetVCSProvidersRequest represents the request to get a paginated list of
// VCSProviders.
message GetVCSProvidersRequest {
  optional pagination.PaginationOptions pagination_options = 1;
  optional VcsProviderSortableField sort = 2;
  optional string search = 3;
  optional string organization_id = 4;
  optional string project_id = 5;
}

// GetRepositoryArchiveRequest represents the request to get a repository
// archive using the configuration of a VCSProvider.
message GetRepositoryArchiveRequest {
  string provider_id = 1;
  string repository_path = 2;
  optional string ref = 3;
}

// CreateVCSProviderRequest represents the request for creating a VCSProvider.
message CreateVCSProviderRequest {
  string name = 1;
  string description = 2;
  VcsProviderType type = 3;
  scope.ScopeType scope = 4;
  VcsProviderAuthType auth_type = 5;
  optional string organization_id = 6;
  optional string project_id = 7;
  optional string url = 8;
  optional string oauth_client_id = 9;
  optional string oauth_client_secret = 10;
  optional string personal_access_token = 11;
  repeated string extra_oauth_scopes = 12;
}

// UpdateVCSProviderRequest represents the request for updating a VCSProvider.
message UpdateVCSProviderRequest {
  string id = 1;
  optional string version = 2;
  optional string description = 3;
  optional string oauth_client_id = 4;
  optional string oauth_client_secret = 5;
  optional string personal_access_token = 6;
  repeated string extra_oauth_scopes = 7;
}

// DeleteVCSProviderRequest represents the request for deleting a VCSProvider.
message DeleteVCSProviderRequest {
  string id = 1;
  optional string version = 2;
}

// ResetVCSProviderOAuthTokenRequest represents the request for resetting the
// OAuth token used for authenticating with the remote VCS system.
message ResetVCSProviderOAuthTokenRequest { string provider_id = 1; }

// CreateVCSTokenForPipelineRequest represents the request for creating a VCS
// token for a pipeline.
message CreateVCSTokenForPipelineRequest {
  string provider_id = 1;
  string pipeline_id = 2;
}

// VCSProvider is a mechanism to interface with external Git or version control
// systems and leverage some of their functionality within Phobos.
message VCSProvider {
  metadata.ResourceMetadata metadata = 1;
  string name = 2;
  string description = 3;
  string type = 4;
  string scope = 5;
  string auth_type = 6;
  string created_by = 7;
  string url = 8;
  string organization_id = 9;
  optional string project_id = 10;
  repeated string extra_oauth_scopes = 11;
}

// GetVCSProvidersResponse represents a paginated list of VCSProviders.
message GetVCSProvidersResponse {
  pagination.PageInfo page_info = 1;
  repeated VCSProvider vcs_providers = 2;
}

// GetRepositoryArchiveResponse is the response for retrieving a repository
// archive.
message GetRepositoryArchiveResponse { bytes chunk_data = 1; }

// CreateVCSProviderResponse represents the response for creating a VCSProvider.
message CreateVCSProviderResponse {
  VCSProvider vcs_provider = 1;
  optional string oauth_authorization_url = 2;
}

// ResetVCSProviderOAuthTokenResponse represents a response for resetting the
// OAuth token used for authenticating with the remote VCS system.
message ResetVCSProviderOAuthTokenResponse {
  VCSProvider vcs_provider = 1;
  string oauth_authorization_url = 2;
}

// CreateVCSTokenForPipelineResponse represents the response for creating a VCS
// token for a pipeline.
message CreateVCSTokenForPipelineResponse {
  string access_token = 1;
  optional google.protobuf.Timestamp expires_at = 3;
}
