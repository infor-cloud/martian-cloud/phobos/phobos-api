package statemachine

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPartialVisitorVisitForPipeline(t *testing.T) {
	v := &PartialVisitor{}
	assert.NoError(t, v.VisitForPipeline(nil, nil))
}

func TestPartialVisitorVisitForStage(t *testing.T) {
	v := &PartialVisitor{}
	assert.NoError(t, v.VisitForStage(nil))
}

func TestPartialVisitorVisitForTask(t *testing.T) {
	v := &PartialVisitor{}
	assert.NoError(t, v.VisitForTask(nil, nil))
}

func TestPartialVisitorVisitForAction(t *testing.T) {
	v := &PartialVisitor{}
	assert.NoError(t, v.VisitForAction(nil, nil))
}

func TestPartialVisitorVisitForAny(t *testing.T) {
	v := &PartialVisitor{}
	assert.NoError(t, v.VisitForAny(nil, nil))
}

func TestPrettyPrintVisitorVisitForPipeline(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	v := &PrettyPrintVisitor{}
	assert.NoError(t, v.VisitForPipeline(pipelineNode, nil))
}

func TestPrettyPrintVisitorVisitForStage(t *testing.T) {
	stageNode := NewStageNode("pipeline.stage.s1", nil, CreatedNodeStatus)
	v := &PrettyPrintVisitor{}
	assert.NoError(t, v.VisitForStage(stageNode))
}

func TestPrettyPrintVisitorVisitForTask(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: nil,
		Status: &createdStatus,
	})
	v := &PrettyPrintVisitor{}
	assert.NoError(t, v.VisitForTask(taskNode, nil))
}

func TestPrettyPrintVisitorVisitForAction(t *testing.T) {
	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", nil, CreatedNodeStatus)
	v := &PrettyPrintVisitor{}
	traversalCtx := &TraversalContext{}
	assert.NoError(t, v.VisitForAction(actionNode, traversalCtx))
}
