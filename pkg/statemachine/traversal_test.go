package statemachine

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTraversalContextCopy(t *testing.T) {
	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", nil, CreatedNodeStatus)

	ctx := &TraversalContext{visitedActions: []*ActionNode{actionNode}}

	copied := ctx.Copy()

	require.NotNil(t, copied)
	assert.Equal(t, ctx.visitedActions, copied.visitedActions)
	assert.NotSame(t, ctx, copied)
}

func TestTraversalContextAddVisited(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: nil,
		Status: &createdStatus,
	})
	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	taskNode.AddActionNode(actionNode)

	ctx := &TraversalContext{visitedActions: []*ActionNode{}}

	ctx.addVisited(actionNode)
	ctx.addVisited(taskNode)

	// Should only add the action node
	assert.Equal(t, []*ActionNode{actionNode}, ctx.visitedActions)
}

func TestTraversalContextVisitedActions(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: nil,
		Status: &createdStatus,
	})
	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	taskNode.AddActionNode(actionNode)

	ctx := &TraversalContext{visitedActions: []*ActionNode{actionNode}}

	assert.Equal(t, []*ActionNode{actionNode}, ctx.VisitedActions())
}

func TestNewSimulatedTraverser(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})
	stateMachine := New(pipelineNode)

	traverser := NewSimulatedTraverser(stateMachine)

	require.NotNil(t, traverser)
	assert.Equal(t, stateMachine, traverser.stateMachine)
	assert.NotSame(t, stateMachine, traverser.stateMachine)
}

func TestSimulatedTraverserAccept(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline.stage.s1.pipeline.p1",
		Status: createdStatus,
		Parent: stageNode,
	})

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	actionNode2 := NewActionNode("pipeline.stage.s1.task.t1.action.a2", taskNode, createdStatus)

	taskNode.AddActionNode(actionNode)
	taskNode.AddActionNode(actionNode2)
	stageNode.AddTaskNode(taskNode)
	stageNode.AddNestedPipelineNode(nestedPipelineNode)
	pipelineNode.AddStageNode(stageNode)

	stateMachine := New(pipelineNode)
	traverser := NewSimulatedTraverser(stateMachine)

	// Verify pipeline has succeeded
	require.NoError(t, traverser.Accept(&PartialVisitor{}))
	assert.Equal(t, SucceededNodeStatus, traverser.stateMachine.GetPipelineNode().Status())
}

func TestSimpleTraverserAccept(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline.stage.s1.pipeline.p1",
		Status: createdStatus,
		Parent: stageNode,
	})

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	actionNode2 := NewActionNode("pipeline.stage.s1.task.t1.action.a2", taskNode, createdStatus)

	taskNode.AddActionNode(actionNode)
	taskNode.AddActionNode(actionNode2)
	stageNode.AddTaskNode(taskNode)
	stageNode.AddNestedPipelineNode(nestedPipelineNode)
	pipelineNode.AddStageNode(stageNode)

	stateMachine := New(pipelineNode)
	traverser := &SimpleTraverser{stateMachine: stateMachine}

	traversalContext := &TraversalContext{visitedActions: []*ActionNode{}}

	// Verify all actions are visited
	require.NoError(t, traverser.accept(pipelineNode, &PartialVisitor{}, traversalContext))
	assert.Equal(t, []*ActionNode{actionNode, actionNode2}, traversalContext.VisitedActions())
}
