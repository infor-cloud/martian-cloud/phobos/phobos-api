// Package statemachine contains the state machine logic for a pipeline in the
// form of an acyclic graph
package statemachine

import (
	"fmt"
	"slices"
)

type nodeResetter struct {
	PartialVisitor
}

func (n *nodeResetter) VisitForAny(node Node, _ *TraversalContext) error {
	return node.SetStatus(CreatedNodeStatus)
}

type nodeFinder struct {
	PartialVisitor
	status    *NodeStatus
	nodeTypes []NodeType
	nodePath  *string
	nodes     []Node
}

func (n *nodeFinder) VisitForAny(node Node, _ *TraversalContext) error {
	if len(n.nodeTypes) > 0 && !slices.Contains(n.nodeTypes, node.Type()) {
		return nil
	}
	if n.status != nil && *n.status != node.Status() {
		return nil
	}
	if n.nodePath != nil && *n.nodePath != node.Path() {
		return nil
	}

	n.nodes = append(n.nodes, node)

	return nil
}

type traversalContextBuilder struct {
	PartialVisitor
	traversalCtx *TraversalContext
	nodePath     string
}

func (v *traversalContextBuilder) VisitForAny(node Node, traversalCtx *TraversalContext) error {
	if node.Path() == v.nodePath {
		v.traversalCtx = traversalCtx.Copy()
	}
	return nil
}

// GetNodesInput defines the input for GetNodes
type GetNodesInput struct {
	Status    *NodeStatus
	NodeTypes []NodeType
}

// StateMachine encapsulates pipeline state updates
type StateMachine struct {
	pipeline *PipelineNode
}

// New creates a new StateMachine instance
func New(pipeline *PipelineNode) *StateMachine {
	pipeline.init()
	return &StateMachine{pipeline: pipeline}
}

// Copy creates a copy of the state machine
func (s *StateMachine) Copy() *StateMachine {
	return New(s.GetPipelineNode().Copy())
}

// Reset resets the status of each node in the state machine to CreatedNodeStatus
func (s *StateMachine) Reset() error {
	v := &nodeResetter{}
	traverser := &SimpleTraverser{stateMachine: s}
	return traverser.Accept(v)
}

// GetPipelineNode returns the pipeline node
func (s *StateMachine) GetPipelineNode() *PipelineNode {
	return s.pipeline
}

// GetNodes returns a list of nodes based on the provided node type and state
func (s *StateMachine) GetNodes(input *GetNodesInput) ([]Node, error) {
	v := &nodeFinder{
		nodes:     []Node{},
		status:    input.Status,
		nodeTypes: input.NodeTypes,
	}
	traverser := &SimpleTraverser{stateMachine: s}
	if err := traverser.Accept(v); err != nil {
		return nil, err
	}
	return v.nodes, nil
}

// GetNode returns a node by path
func (s *StateMachine) GetNode(path string) (Node, bool) {
	v := &nodeFinder{
		nodes:    []Node{},
		nodePath: &path,
	}
	traverser := &SimpleTraverser{stateMachine: s}
	if err := traverser.Accept(v); err != nil {
		// Panic here since an error should never occur
		panic(err)
	}

	if len(v.nodes) == 0 {
		return nil, false
	}

	return v.nodes[0], true
}

// GetTraversalContextForNode returns the traversal context for a given node
func (s *StateMachine) GetTraversalContextForNode(nodePath string) (*TraversalContext, error) {
	v := &traversalContextBuilder{
		nodePath: nodePath,
	}
	traverser := &SimulatedTraverser{stateMachine: s}
	if err := traverser.Accept(v); err != nil {
		return nil, err
	}

	if v.traversalCtx == nil {
		return nil, fmt.Errorf("traversal context for node with path %s not found", nodePath)
	}

	return v.traversalCtx, nil
}

// statusChangeFinder is a visitor that finds all status changes in the state machine
type statusChangeFinder struct {
	PartialVisitor
	statusChanges []NodeStatusChange
}

func (v *statusChangeFinder) VisitForAny(node Node, _ *TraversalContext) error {
	v.statusChanges = append(v.statusChanges, node.GetStatusChanges()...)
	return nil
}

// GetStatusChanges traverses the state machine and returns all status changes
func (s *StateMachine) GetStatusChanges() ([]NodeStatusChange, error) {
	v := &statusChangeFinder{
		statusChanges: []NodeStatusChange{},
	}
	traverser := &SimpleTraverser{stateMachine: s}
	if err := traverser.Accept(v); err != nil {
		return nil, err
	}

	return v.statusChanges, nil
}
