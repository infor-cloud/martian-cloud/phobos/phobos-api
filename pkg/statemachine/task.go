package statemachine

import (
	"slices"
)

var _ StageChildNode = (*TaskNode)(nil)

// NewTaskNodeInput defines the input for creating a new task node
type NewTaskNodeInput struct {
	Parent       Node
	Status       *NodeStatus
	OnError      OnErrorStrategy
	Path         string
	Dependencies []string
}

// TaskNode represents a task in a pipeline
type TaskNode struct {
	onError OnErrorStrategy
	nodeImpl
	actions      []*ActionNode
	dependencies []string
}

// NewTaskNode creates a new task node
func NewTaskNode(input *NewTaskNodeInput) *TaskNode {
	status := CreatedNodeStatus
	if input.Status != nil {
		status = *input.Status
	}
	return &TaskNode{
		nodeImpl: nodeImpl{
			path:          input.Path,
			typ:           TaskNodeType,
			parent:        input.Parent,
			status:        status,
			statusChanges: []NodeStatusChange{},
		},
		actions:      []*ActionNode{},
		dependencies: input.Dependencies,
		onError:      input.OnError,
	}
}

// Copy returns a copy of the node
func (n *TaskNode) Copy() *TaskNode {
	node := &TaskNode{
		nodeImpl: nodeImpl{
			path:          n.path,
			typ:           n.typ,
			parent:        n.parent,
			status:        n.status,
			statusChanges: n.statusChanges,
		},
		actions:      []*ActionNode{},
		dependencies: n.dependencies,
		onError:      n.onError,
	}
	for _, action := range n.actions {
		node.AddActionNode(action.Copy())
	}
	return node
}

// Actions returns a list of actions for this task
func (n *TaskNode) Actions() []*ActionNode {
	return n.actions
}

func (n *TaskNode) init() {
	for _, action := range n.actions {
		// Action failed
		action.registerListener(FailedNodeStatus, n.handleActionFailed)
		// Action canceled
		action.registerListener(CanceledNodeStatus, n.handleActionCanceled)
		// Action succeeded
		action.registerListener(SucceededNodeStatus, n.handleActionSucceeded)
	}
}

// SetStatus sets the node status
func (n *TaskNode) SetStatus(status NodeStatus) error {
	if err := n.nodeImpl.SetStatus(status); err != nil {
		return err
	}

	switch status {
	case ReadyNodeStatus:
		// Reset all actions since the task is being retried
		if err := n.resetAllActions(); err != nil {
			return err
		}

		// Set first child to ready
		if len(n.actions) > 0 {
			if err := n.actions[0].SetStatus(ReadyNodeStatus); err != nil {
				return err
			}
		}
	case DeferredNodeStatus, SkippedNodeStatus:
		// Set all actions as skipped
		for _, a := range n.actions {
			if err := a.SetStatus(SkippedNodeStatus); err != nil {
				return err
			}
		}
	case WaitingNodeStatus:
		// Set all actions as waiting
		for _, a := range n.actions {
			if err := a.SetStatus(WaitingNodeStatus); err != nil {
				return err
			}
		}
	case FailedNodeStatus:
		// Set any remaining actions to failed
		for _, a := range n.actions {
			if !a.status.IsFinalStatus() {
				if err := a.SetStatus(FailedNodeStatus); err != nil {
					return err
				}
			}
		}
	case CreatedNodeStatus, InitializingNodeStatus, ApprovalPendingNodeStatus:
		// Reset all actions
		if err := n.resetAllActions(); err != nil {
			return err
		}
	}

	return nil
}

// GetInitialStatus returns the initial status of the node
func (n *TaskNode) GetInitialStatus() NodeStatus {
	return InitializingNodeStatus
}

// GetDependencies returns the dependencies for this node.
func (n *TaskNode) GetDependencies() []string {
	return n.dependencies
}

// AddActionNode adds a new action node
func (n *TaskNode) AddActionNode(a *ActionNode) {
	n.actions = append(n.actions, a)
}

// ChildrenNodes returns a list of this nodes children
func (n *TaskNode) ChildrenNodes() []Node {
	nodes := []Node{}

	for _, a := range n.actions {
		nodes = append(nodes, a)
	}

	return nodes
}

// Accept a visitor
func (n *TaskNode) Accept(v Visitor, traversalCtx *TraversalContext) error {
	if err := v.VisitForAny(n, traversalCtx); err != nil {
		return err
	}
	return v.VisitForTask(n, traversalCtx)
}

// Cancel cancels the node
func (n *TaskNode) Cancel() error {
	if err := n.nodeImpl.Cancel(); err != nil {
		return err
	}

	// Cancel all actions
	for _, a := range n.actions {
		if err := a.Cancel(); err != nil {
			return err
		}
	}

	return nil
}

// GetOnErrorStrategy returns the on error behavior.
func (n *TaskNode) GetOnErrorStrategy() OnErrorStrategy {
	return n.onError
}

// HasUnfinishedDependencies returns true if the node has unfinished dependencies
func (n *TaskNode) HasUnfinishedDependencies() bool {
	if len(n.dependencies) == 0 {
		return false
	}

	for _, child := range n.parent.ChildrenNodes() {
		if child.Path() == n.path {
			// Skip the task itself
			continue
		}

		if !slices.Contains(n.dependencies, child.Name()) {
			// Dependency not found, skip.
			continue
		}

		if !child.Status().IsFinalStatus() {
			// Dependency is not finished.
			return true
		}
	}

	return false
}

/* Helper functions */

// resetAllActions resets all actions
func (n *TaskNode) resetAllActions() error {
	for _, a := range n.actions {
		if err := a.SetStatus(CreatedNodeStatus); err != nil {
			return err
		}
	}

	return nil
}

/* Action event listeners */

// handleActionFailed handles a failed action
func (n *TaskNode) handleActionFailed() error {
	return n.handleActionUnsuccessfulStatus(FailedNodeStatus)
}

// handleActionCanceled handles a canceled action
func (n *TaskNode) handleActionCanceled() error {
	return n.handleActionUnsuccessfulStatus(CanceledNodeStatus)
}

// handleActionUnsuccessfulStatus handles an unsuccessful action (failed or canceled)
func (n *TaskNode) handleActionUnsuccessfulStatus(newStatus NodeStatus) error {
	if n.status.IsFinalStatus() {
		// Task is already in a final state (likely canceled task or pipeline directly).
		return nil
	}

	// Skip the remaining actions
	for _, a := range n.actions {
		if a.status.Equals(CreatedNodeStatus) {
			if err := a.SetStatus(SkippedNodeStatus); err != nil {
				return err
			}
		}
	}

	return n.SetStatus(newStatus)
}

// handleActionSucceeded handles a succeeded action
func (n *TaskNode) handleActionSucceeded() error {
	for _, t := range n.actions {
		if t.status.Equals(CreatedNodeStatus) {
			// Set next action to ready
			return t.SetStatus(ReadyNodeStatus)
		}
	}

	return nil
}
