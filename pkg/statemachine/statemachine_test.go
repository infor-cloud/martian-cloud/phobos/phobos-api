package statemachine

import (
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNodeResetterVisitForAny(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})

	nodeResetter := &nodeResetter{}
	require.NoError(t, nodeResetter.VisitForAny(pipelineNode, nil))
	assert.Equal(t, CreatedNodeStatus, pipelineNode.Status())
}

func TestNodeFinderVisitForAny(t *testing.T) {
	createdStatus := CreatedNodeStatus

	type testCase struct {
		nodePath      *string
		name          string
		status        *NodeStatus
		nodeTypes     []NodeType
		expectedNodes int
	}

	testCases := []testCase{
		{
			name:          "find all nodes",
			expectedNodes: 4,
		},
		{
			name:          "find all nodes with status",
			status:        &createdStatus,
			expectedNodes: 3,
		},
		{
			name:          "find all nodes with node type",
			nodeTypes:     []NodeType{PipelineNodeType, StageNodeType},
			expectedNodes: 2,
		},
		{
			name:          "find all nodes with node type and status",
			nodeTypes:     []NodeType{PipelineNodeType, StageNodeType},
			status:        &createdStatus,
			expectedNodes: 2,
		},
		{
			name:          "find all nodes with node path",
			nodePath:      ptr.String("pipeline.stage.s1.task.t1.action.a1"),
			expectedNodes: 1,
		},
		{
			name:          "find all nodes with node path and status",
			nodePath:      ptr.String("pipeline.stage.s1.task.t1.action.a1"),
			status:        &createdStatus,
			expectedNodes: 0, // node is not in created status
		},
		{
			name:          "find all nodes with node path and node type",
			nodePath:      ptr.String("pipeline.stage.s1.task.t1.action.a1"),
			nodeTypes:     []NodeType{ActionNodeType},
			expectedNodes: 1,
		},
		{
			name:          "find all nodes with node path, node type and status",
			nodePath:      ptr.String("pipeline.stage.s1.task.t1"),
			nodeTypes:     []NodeType{TaskNodeType},
			status:        &createdStatus,
			expectedNodes: 1,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: createdStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: &createdStatus,
			})
			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, FailedNodeStatus)

			taskNode.AddActionNode(actionNode)
			stageNode.AddTaskNode(taskNode)
			pipelineNode.AddStageNode(stageNode)

			nodeFinder := &nodeFinder{
				status:    test.status,
				nodeTypes: test.nodeTypes,
				nodePath:  test.nodePath,
			}

			require.NoError(t, nodeFinder.VisitForAny(pipelineNode, nil))
			require.NoError(t, nodeFinder.VisitForAny(stageNode, nil))
			require.NoError(t, nodeFinder.VisitForAny(taskNode, nil))
			require.NoError(t, nodeFinder.VisitForAny(actionNode, nil))

			assert.Len(t, nodeFinder.nodes, test.expectedNodes)
		})
	}
}

func TestTraversalContextBuilderVisitForAny(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})
	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	taskNode.AddActionNode(actionNode)
	stageNode.AddTaskNode(taskNode)
	pipelineNode.AddStageNode(stageNode)

	traversalContext := &TraversalContext{
		visitedActions: []*ActionNode{actionNode},
	}

	traversalContextBuilder := &traversalContextBuilder{
		nodePath: actionNode.path,
	}

	// Should find the traversal context for the action node
	require.NoError(t, traversalContextBuilder.VisitForAny(actionNode, traversalContext))
	assert.Equal(t, traversalContext, traversalContextBuilder.traversalCtx)

	// Reset traversal context
	traversalContextBuilder.traversalCtx = nil

	// Should not find the traversal context for the task node
	require.NoError(t, traversalContextBuilder.VisitForAny(taskNode, traversalContext))
	assert.Nil(t, traversalContextBuilder.traversalCtx)
}

func TestStateMachineNew(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})
	stateMachine := New(pipelineNode)

	require.NotNil(t, stateMachine)
	assert.Equal(t, pipelineNode, stateMachine.pipeline)
}

func TestStateMachineCopy(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})
	stateMachine := New(pipelineNode)

	copied := stateMachine.Copy()

	require.NotNil(t, copied)
	assert.Equal(t, pipelineNode, copied.pipeline)
	assert.NotSame(t, stateMachine, copied)
}

func TestStateMachineReset(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})
	stateMachine := New(pipelineNode)

	require.NoError(t, stateMachine.Reset())
	assert.Equal(t, CreatedNodeStatus, pipelineNode.Status())
}

func TestStateMachineGetPipelineNode(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})
	stateMachine := New(pipelineNode)

	assert.Equal(t, pipelineNode, stateMachine.GetPipelineNode())
}

func TestStateMachineGetNodes(t *testing.T) {
	createdStatus := CreatedNodeStatus

	type testCase struct {
		status        *NodeStatus
		name          string
		nodeTypes     []NodeType
		expectedNodes int
	}

	testCases := []testCase{
		{
			name:          "find all nodes",
			expectedNodes: 4,
		},
		{
			name:          "find all nodes with status",
			status:        &createdStatus,
			expectedNodes: 3,
		},
		{
			name:          "find all nodes with node type",
			nodeTypes:     []NodeType{PipelineNodeType, StageNodeType},
			expectedNodes: 2,
		},
		{
			name:          "find all nodes with node type and status",
			nodeTypes:     []NodeType{PipelineNodeType, StageNodeType},
			status:        &createdStatus,
			expectedNodes: 2,
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: createdStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: &createdStatus,
			})
			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, FailedNodeStatus)

			taskNode.AddActionNode(actionNode)
			stageNode.AddTaskNode(taskNode)
			pipelineNode.AddStageNode(stageNode)

			stateMachine := New(pipelineNode)

			nodes, err := stateMachine.GetNodes(&GetNodesInput{
				Status:    test.status,
				NodeTypes: test.nodeTypes,
			})
			require.NoError(t, err)
			assert.Len(t, nodes, test.expectedNodes)
		})
	}
}

func TestStateMachineGetNode(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})
	stateMachine := New(pipelineNode)

	node, ok := stateMachine.GetNode("pipeline")
	require.True(t, ok)
	assert.Equal(t, pipelineNode, node)

	node, ok = stateMachine.GetNode("pipeline.stage.s1")
	require.False(t, ok)
	assert.Nil(t, node)
}

func TestGetTraversalContextForNode(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})
	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	action2Node := NewActionNode("pipeline.stage.s1.task.t1.action.a2", taskNode, createdStatus)

	taskNode.AddActionNode(actionNode)
	taskNode.AddActionNode(action2Node)
	stageNode.AddTaskNode(taskNode)
	pipelineNode.AddStageNode(stageNode)

	stateMachine := New(pipelineNode)

	traversalContext, err := stateMachine.GetTraversalContextForNode(actionNode.path)
	require.NoError(t, err)
	assert.Empty(t, traversalContext.VisitedActions())

	// Check that the traversal context is updated after visiting the node
	traversalContext, err = stateMachine.GetTraversalContextForNode(action2Node.path)
	require.NoError(t, err)
	assert.Equal(t, []*ActionNode{actionNode}, traversalContext.VisitedActions())
}

func TestStateMachineGetStatusChanges(t *testing.T) {
	pipelineNodePath := "pipeline"
	stageNodePath := "pipeline.stage.s1"
	taskNodePath := "pipeline.stage.s1.task.t1"
	actionNodePath := "pipeline.stage.s1.task.action.t1"
	nestedPipelineNodePath := "pipeline.stage.s1.pipeline.p1"

	type testCase struct {
		name                string
		newStatus           NodeStatus
		targetNodePath      string
		initialNodeStatuses map[string]NodeStatus
		expectStatusChanges []NodeStatusChange
	}

	// Although these test cases won't cover every scenario, they should, however,
	// test for all the scenarios that are most common. These have been derived by
	// looking at the pipeline package's "updater.go" and "supervisor.go" modules.
	testCases := []testCase{
		{
			name:           "pipeline goes from CREATED to RUNNING",
			newStatus:      RunningNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       CreatedNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
			},
		},
		{
			name:           "pipeline goes from READY to RUNNING",
			newStatus:      RunningNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       ReadyNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
			},
		},
		{
			name:           "pipeline goes from CREATED to SKIPPED",
			newStatus:      SkippedNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       CreatedNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
			},
		},
		{
			name:           "pipeline goes from READY to DEFERRED",
			newStatus:      DeferredNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       ReadyNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: DeferredNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
			},
		},
		{
			name:           "pipeline goes from CREATED to INITIALIZING",
			newStatus:      InitializingNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       CreatedNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				// Rest of the nodes shouldn't change.
			},
		},
		{
			name:           "pipeline goes from INITIALIZING to WAITING",
			newStatus:      WaitingNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       InitializingNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: InitializingNodeStatus,
					NewStatus: WaitingNodeStatus,
				},
				// Rest of the nodes shouldn't change.
			},
		},
		{
			name:           "pipeline goes from INITIALIZING to APPROVAL_PENDING",
			newStatus:      ApprovalPendingNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       InitializingNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: InitializingNodeStatus,
					NewStatus: ApprovalPendingNodeStatus,
				},
				// Rest of the nodes shouldn't change.
			},
		},
		{
			name:           "pipeline goes from READY to APPROVAL_PENDING",
			newStatus:      ApprovalPendingNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       ReadyNodeStatus,
				stageNodePath:          CreatedNodeStatus,
				taskNodePath:           CreatedNodeStatus,
				nestedPipelineNodePath: CreatedNodeStatus,
				actionNodePath:         CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: ApprovalPendingNodeStatus,
				},
				// Rest of the nodes shouldn't change.
			},
		},
		{
			name:           "pipeline goes from FAILED to INITIALIZING",
			newStatus:      InitializingNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       FailedNodeStatus,
				stageNodePath:          FailedNodeStatus,
				taskNodePath:           FailedNodeStatus,
				nestedPipelineNodePath: FailedNodeStatus,
				actionNodePath:         FailedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
			},
		},
		{
			name:           "pipeline goes from SUCCEEDED to INITIALIZING",
			newStatus:      InitializingNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       SucceededNodeStatus,
				stageNodePath:          SucceededNodeStatus,
				taskNodePath:           SucceededNodeStatus,
				nestedPipelineNodePath: SucceededNodeStatus,
				actionNodePath:         SucceededNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
			},
		},
		{
			name:           "pipeline goes from CANCELED to INITIALIZING",
			newStatus:      InitializingNodeStatus,
			targetNodePath: pipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       CanceledNodeStatus,
				stageNodePath:          CanceledNodeStatus,
				taskNodePath:           CanceledNodeStatus,
				nestedPipelineNodePath: CanceledNodeStatus,
				actionNodePath:         CanceledNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
			},
		},
		{
			name:           "nested pipeline goes from WAITING to DEFERRED",
			newStatus:      DeferredNodeStatus,
			targetNodePath: nestedPipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       RunningNodeStatus,
				stageNodePath:          RunningNodeStatus,
				nestedPipelineNodePath: WaitingNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: WaitingNodeStatus,
					NewStatus: DeferredNodeStatus,
				},
			},
		},
		{
			name:           "nested pipeline goes from READY to DEFERRED",
			newStatus:      DeferredNodeStatus,
			targetNodePath: nestedPipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       RunningNodeStatus,
				stageNodePath:          RunningNodeStatus,
				nestedPipelineNodePath: ReadyNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: DeferredNodeStatus,
				},
			},
		},
		{
			name:           "nested pipeline goes from FAILED to RUNNING",
			newStatus:      RunningNodeStatus,
			targetNodePath: nestedPipelineNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath:       FailedNodeStatus,
				stageNodePath:          FailedNodeStatus,
				nestedPipelineNodePath: FailedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  nestedPipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
			},
		},
		{
			name:           "task goes from WAITING to DEFERRED",
			newStatus:      DeferredNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     WaitingNodeStatus,
				actionNodePath:   WaitingNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: WaitingNodeStatus,
					NewStatus: DeferredNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: WaitingNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
			},
		},
		{
			name:           "task goes from READY to DEFERRED",
			newStatus:      DeferredNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     ReadyNodeStatus,
				actionNodePath:   ReadyNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: DeferredNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: SkippedNodeStatus,
				},
			},
		},
		{
			name:           "task goes from READY to PENDING",
			newStatus:      PendingNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     ReadyNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: PendingNodeStatus,
				},
			},
		},
		{
			name:           "task goes from RUNNING to SUCCEEDED",
			newStatus:      SucceededNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     RunningNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
			},
		},
		{
			name:           "task goes from CANCELED to INITIALIZING",
			newStatus:      InitializingNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: CanceledNodeStatus,
				stageNodePath:    CanceledNodeStatus,
				taskNodePath:     CanceledNodeStatus,
				actionNodePath:   CanceledNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: CanceledNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
			},
		},
		{
			name:           "task goes from FAILED to INITIALIZING",
			newStatus:      InitializingNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: FailedNodeStatus,
				stageNodePath:    FailedNodeStatus,
				taskNodePath:     FailedNodeStatus,
				actionNodePath:   FailedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: FailedNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
			},
		},
		{
			name:           "task goes from SUCCEEDED to INITIALIZING",
			newStatus:      InitializingNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: SucceededNodeStatus,
				stageNodePath:    SucceededNodeStatus,
				taskNodePath:     SucceededNodeStatus,
				actionNodePath:   SucceededNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: RunningNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: InitializingNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: SucceededNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
			},
		},
		{
			name:           "task goes from INITIALIZING to APPROVAL_PENDING",
			newStatus:      ApprovalPendingNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     InitializingNodeStatus,
				actionNodePath:   CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: InitializingNodeStatus,
					NewStatus: ApprovalPendingNodeStatus,
				},
				// Rest of the nodes shouldn't change.
			},
		},
		{
			name:           "task goes from APPROVAL_PENDING to READY",
			newStatus:      ReadyNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    ReadyNodeStatus,
				taskNodePath:     ApprovalPendingNodeStatus,
				actionNodePath:   CreatedNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				// Pipeline and stage node shouldn't change.
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: ApprovalPendingNodeStatus,
					NewStatus: ReadyNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: CreatedNodeStatus,
					NewStatus: ReadyNodeStatus,
				},
			},
		},
		{
			name:           "task goes from READY to APPROVAL_PENDING",
			newStatus:      ApprovalPendingNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     ReadyNodeStatus,
				actionNodePath:   ReadyNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				// Pipeline and stage node shouldn't change.
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: ApprovalPendingNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: ReadyNodeStatus,
					NewStatus: CreatedNodeStatus,
				},
			},
		},
		{
			name:           "task goes from PENDING to RUNNING",
			newStatus:      RunningNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     PendingNodeStatus,
				actionNodePath:   ReadyNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				// Pipeline, stage and action shouldn't change.
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: PendingNodeStatus,
					NewStatus: RunningNodeStatus,
				},
			},
		},
		{
			name:           "task goes from RUNNING to FAILED",
			newStatus:      FailedNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     RunningNodeStatus,
				actionNodePath:   RunningNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
			},
		},
		{
			name:           "task goes from RUNNING to CANCELING",
			newStatus:      CancelingNodeStatus,
			targetNodePath: taskNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     RunningNodeStatus,
				actionNodePath:   RunningNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: CancelingNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: CancelingNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: CancelingNodeStatus,
				},
				// Action should be independent of task.
			},
		},
		{
			name:           "action goes from RUNNING to SUCCEEDED",
			newStatus:      SucceededNodeStatus,
			targetNodePath: actionNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     RunningNodeStatus,
				actionNodePath:   RunningNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				// Other nodes should only change when task is succeeded.
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: SucceededNodeStatus,
				},
			},
		},
		{
			name:           "action goes from RUNNING to FAILED",
			newStatus:      FailedNodeStatus,
			targetNodePath: actionNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     RunningNodeStatus,
				actionNodePath:   RunningNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: FailedNodeStatus,
				},
			},
		},
		{
			name:           "action goes from RUNNING to CANCELED",
			newStatus:      CanceledNodeStatus,
			targetNodePath: actionNodePath,
			initialNodeStatuses: map[string]NodeStatus{
				pipelineNodePath: RunningNodeStatus,
				stageNodePath:    RunningNodeStatus,
				taskNodePath:     RunningNodeStatus,
				actionNodePath:   RunningNodeStatus,
			},
			expectStatusChanges: []NodeStatusChange{
				{
					NodePath:  pipelineNodePath,
					NodeType:  PipelineNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: CanceledNodeStatus,
				},
				{
					NodePath:  stageNodePath,
					NodeType:  StageNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: CanceledNodeStatus,
				},
				{
					NodePath:  taskNodePath,
					NodeType:  TaskNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: CanceledNodeStatus,
				},
				{
					NodePath:  actionNodePath,
					NodeType:  ActionNodeType,
					OldStatus: RunningNodeStatus,
					NewStatus: CanceledNodeStatus,
				},
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   pipelineNodePath,
				Status: test.initialNodeStatuses[pipelineNodePath],
			})
			stageNode := NewStageNode(stageNodePath, pipelineNode, test.initialNodeStatuses[stageNodePath])

			if initialStatus, ok := test.initialNodeStatuses[nestedPipelineNodePath]; ok {
				// Only add a nested pipeline if we're using it. This will allow us to test
				// scenarios where a stage only has task(s).
				nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
					Parent: stageNode,
					Path:   nestedPipelineNodePath,
					Status: initialStatus,
				})
				stageNode.AddNestedPipelineNode(nestedPipelineNode)
			}

			if initialStatus, ok := test.initialNodeStatuses[taskNodePath]; ok {
				// Only add a nested pipeline if we're using it. This will allow us to test
				// scenarios where a stage only has nested pipeline(s).
				taskNode := NewTaskNode(&NewTaskNodeInput{
					Path:   taskNodePath,
					Parent: stageNode,
					Status: &initialStatus,
				})

				if initialStatus, ok := test.initialNodeStatuses[actionNodePath]; ok {
					// Only add an action if we're using it. This will allow us to test
					// scenarios where a task has no actions.
					actionNode := NewActionNode(actionNodePath, taskNode, initialStatus)
					taskNode.AddActionNode(actionNode)
				}

				stageNode.AddTaskNode(taskNode)
			}

			// Link the stage node to its respective parent.
			pipelineNode.AddStageNode(stageNode)

			// Build a state machine.
			sm := New(pipelineNode)

			// Get the target node we want to set the status of.
			targetNode, ok := sm.GetNode(test.targetNodePath)
			require.True(t, ok, "failed to get %s target node", test.targetNodePath)

			// Set the status of the target node to what we want.
			err := targetNode.SetStatus(test.newStatus)
			require.NoError(t, err, "failed to set node %s status", test.targetNodePath)

			// Get the status changes, so we can compare them with the expected.
			actualStatusChanges, err := sm.GetStatusChanges()
			require.NoError(t, err, "failed to get state machine status changes")

			// Make sure they match.
			assert.ElementsMatch(t, test.expectStatusChanges, actualStatusChanges)
		})
	}
}
