package statemachine

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewActionNode(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	expected := &ActionNode{
		nodeImpl: nodeImpl{
			path:          "path",
			typ:           ActionNodeType,
			parent:        taskNode,
			status:        createdStatus,
			statusChanges: []NodeStatusChange{},
		},
	}

	assert.Equal(t, expected, NewActionNode("path", taskNode, createdStatus))
}

func TestActionNodeCopy(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	copied := actionNode.Copy()

	assert.Equal(t, actionNode, copied)
	assert.NotSame(t, actionNode, copied)
}

func TestActionNodeAccept(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	traversalCtx := &TraversalContext{}

	visitor := &PartialVisitor{}

	assert.NoError(t, actionNode.Accept(visitor, traversalCtx))
}

func TestActionNodeCancel(t *testing.T) {
	type testCase struct {
		name     string
		status   NodeStatus
		expected NodeStatus
	}

	testCases := []testCase{
		{
			name:     "created status should become skipped",
			status:   CreatedNodeStatus,
			expected: SkippedNodeStatus,
		},
		{
			name:     "running status should become canceling",
			status:   RunningNodeStatus,
			expected: CancelingNodeStatus,
		},
		{
			name:     "ready status should become canceling",
			status:   ReadyNodeStatus,
			expected: CancelingNodeStatus,
		},
		{
			name:     "final status should not be changed",
			status:   SkippedNodeStatus,
			expected: SkippedNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.status,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, tc.status)
			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: &tc.status,
			})

			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, tc.status)

			assert.NoError(t, actionNode.Cancel())
			assert.Equal(t, tc.expected, actionNode.Status())
		})
	}
}

func TestActionNodeChildrenNodes(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	assert.Equal(t, []Node{}, actionNode.ChildrenNodes())
}

func TestActionNodeParent(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	assert.Equal(t, taskNode, actionNode.Parent())
}

func TestActionNodePath(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	assert.Equal(t, "pipeline.stage.s1.task.t1.action.a1", actionNode.Path())
}

func TestActionNodeStatus(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	assert.Equal(t, createdStatus, actionNode.Status())
}

func TestActionNodeType(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	assert.Equal(t, ActionNodeType, actionNode.Type())
}

func TestActionNodeGetInitialStatus(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	assert.Equal(t, ReadyNodeStatus, actionNode.GetInitialStatus())
}

func TestActionNodeGetStatusChanges(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)

	// Arbitrary status changes.
	require.NoError(t, actionNode.SetStatus(ReadyNodeStatus))
	require.NoError(t, actionNode.SetStatus(CanceledNodeStatus))

	expect := []NodeStatusChange{
		{
			OldStatus: createdStatus,
			NewStatus: ReadyNodeStatus,
			NodePath:  actionNode.path,
			NodeType:  actionNode.typ,
		},
		{
			OldStatus: ReadyNodeStatus,
			NewStatus: CanceledNodeStatus,
			NodePath:  actionNode.path,
			NodeType:  actionNode.typ,
		},
	}

	assert.Equal(t, expect, actionNode.GetStatusChanges())
}

func TestActionNodeEventListeners(t *testing.T) {
	// Test that the status event listeners are called when the status changes and
	// parent node status is updated accordingly

	type testCase struct {
		name                      string
		newStatus                 NodeStatus
		expectTaskStatus          NodeStatus
		expectSiblingActionStatus NodeStatus // status of the subsequent action (if any)
		totalActions              int
	}

	testCases := []testCase{
		{
			name:             "task should be set to failed since the only action failed",
			newStatus:        FailedNodeStatus,
			expectTaskStatus: FailedNodeStatus,
			totalActions:     1,
		},
		{
			name:                      "task should be set to failed since the only action failed and the subsequent actions should be skipped",
			newStatus:                 FailedNodeStatus,
			expectTaskStatus:          FailedNodeStatus,
			expectSiblingActionStatus: SkippedNodeStatus,
			totalActions:              2,
		},
		{
			name:             "task should be set to canceled since the only action was canceled",
			newStatus:        CanceledNodeStatus,
			expectTaskStatus: CanceledNodeStatus,
			totalActions:     1,
		},
		{
			name:                      "task should be set to canceled since the only action was canceled and the subsequent actions should be skipped",
			newStatus:                 CanceledNodeStatus,
			expectTaskStatus:          CanceledNodeStatus,
			expectSiblingActionStatus: SkippedNodeStatus,
			totalActions:              2,
		},
		// Note: task succeeded status is set independently of the action event listeners
		{
			name:                      "task status should not change and subsequent actions should be set to ready since an action succeeded",
			newStatus:                 SucceededNodeStatus,
			expectTaskStatus:          RunningNodeStatus,
			expectSiblingActionStatus: ReadyNodeStatus,
			totalActions:              2,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			runningStatus := RunningNodeStatus

			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: nil,
				Status: &runningStatus,
			})

			actions := []*ActionNode{}
			for i := 0; i < tc.totalActions; i++ {
				actionStatus := CreatedNodeStatus
				if i == 0 {
					// Only simulate the first action as running.
					actionStatus = runningStatus
				}

				actionNode := NewActionNode(
					fmt.Sprintf("pipeline.stage.s1.task.t1.action.a%d", i),
					taskNode,
					actionStatus,
				)
				taskNode.AddActionNode(actionNode)
				actions = append(actions, actionNode)
			}

			// Initialize the task node, this will register the event listeners
			taskNode.init()

			// Update the first action status to the new status
			require.NoError(t, actions[0].SetStatus(tc.newStatus))

			// Check that the task status is updated accordingly
			assert.Equal(t, tc.expectTaskStatus, taskNode.Status())

			// Check that the sibling actions are updated accordingly
			for i := 1; i < tc.totalActions; i++ {
				assert.Equal(t, tc.expectSiblingActionStatus, actions[i].Status())
			}
		})
	}
}
