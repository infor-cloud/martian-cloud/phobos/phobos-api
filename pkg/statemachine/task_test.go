package statemachine

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewTaskNode(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	expected := &TaskNode{
		nodeImpl: nodeImpl{
			path:          "path",
			typ:           TaskNodeType,
			parent:        stageNode,
			status:        createdStatus,
			statusChanges: []NodeStatusChange{},
		},
		actions:      []*ActionNode{},
		dependencies: []string{"t2"},
		onError:      FailOnError,
	}

	assert.Equal(t, expected, NewTaskNode(&NewTaskNodeInput{
		Path:         "path",
		Parent:       stageNode,
		Status:       &createdStatus,
		Dependencies: []string{"t2"},
		OnError:      FailOnError,
	}))
}

func TestTaskNodeCopy(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:         "pipeline.stage.s1.task.t1",
		Parent:       stageNode,
		Status:       &createdStatus,
		Dependencies: []string{"t2"},
		OnError:      FailOnError,
	})

	taskNode.AddActionNode(NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus))

	copied := taskNode.Copy()

	assert.Equal(t, taskNode, copied)
	assert.NotSame(t, taskNode, copied)
}

func TestTaskNodeSetStatusReady(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	action1Node := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	action2Node := NewActionNode("pipeline.stage.s1.task.t1.action.a2", taskNode, createdStatus)
	taskNode.AddActionNode(action1Node)
	taskNode.AddActionNode(action2Node)

	require.NoError(t, taskNode.SetStatus(ReadyNodeStatus))
	assert.Equal(t, ReadyNodeStatus, taskNode.Status())
	assert.Equal(t, ReadyNodeStatus, action1Node.Status())
	assert.Equal(t, CreatedNodeStatus, action2Node.Status()) // Should not change

	// Task retry
	taskNode.status = FailedNodeStatus
	action1Node.status = FailedNodeStatus
	action2Node.status = FailedNodeStatus

	require.NoError(t, taskNode.SetStatus(ReadyNodeStatus))
	assert.Equal(t, ReadyNodeStatus, taskNode.Status())
	assert.Equal(t, ReadyNodeStatus, action1Node.Status())
	assert.Equal(t, CreatedNodeStatus, action2Node.Status()) // Should be reset
}

func TestTaskNodeSetStatusSkipped(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)

	require.NoError(t, taskNode.SetStatus(SkippedNodeStatus))
	assert.Equal(t, SkippedNodeStatus, taskNode.Status())
	assert.Equal(t, SkippedNodeStatus, actionNode.Status())
}

func TestTaskNodeSetStatusFailed(t *testing.T) {
	runningStatus := RunningNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: runningStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, runningStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &runningStatus,
	})

	action1Node := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, FailedNodeStatus)
	action2Node := NewActionNode("pipeline.stage.s1.task.t1.action.a2", taskNode, CreatedNodeStatus)

	taskNode.AddActionNode(action1Node)
	taskNode.AddActionNode(action2Node)

	require.NoError(t, taskNode.SetStatus(FailedNodeStatus))
	assert.Equal(t, FailedNodeStatus, taskNode.Status())
	assert.Equal(t, FailedNodeStatus, action1Node.Status())
	assert.Equal(t, FailedNodeStatus, action2Node.Status()) // Should be marked as failed
}

func TestTaskNodeSetStatusReset(t *testing.T) {
	// Created or Approval Pending aka, task is being reset

	canceled := CanceledNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: canceled,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, canceled)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &canceled,
	})

	action1Node := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, canceled)

	taskNode.AddActionNode(action1Node)

	require.NoError(t, taskNode.SetStatus(CreatedNodeStatus))
	assert.Equal(t, CreatedNodeStatus, taskNode.Status())
	assert.Equal(t, CreatedNodeStatus, action1Node.Status())

	require.NoError(t, taskNode.SetStatus(InitializingNodeStatus))
	assert.Equal(t, InitializingNodeStatus, taskNode.Status())
	assert.Equal(t, CreatedNodeStatus, action1Node.Status())

	require.NoError(t, taskNode.SetStatus(ApprovalPendingNodeStatus))
	assert.Equal(t, ApprovalPendingNodeStatus, taskNode.Status())
	assert.Equal(t, CreatedNodeStatus, action1Node.Status())
}

func TestTaskNodeSetStatusDeferred(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	action1Node := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	action2Node := NewActionNode("pipeline.stage.s1.task.t1.action.a2", taskNode, createdStatus)
	taskNode.AddActionNode(action1Node)
	taskNode.AddActionNode(action2Node)

	require.NoError(t, taskNode.SetStatus(DeferredNodeStatus))
	assert.Equal(t, DeferredNodeStatus, taskNode.Status())
	assert.Equal(t, SkippedNodeStatus, action1Node.Status())
	assert.Equal(t, SkippedNodeStatus, action2Node.Status())
}

func TestTaskNodeActions(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)

	assert.Equal(t, []*ActionNode{actionNode}, taskNode.Actions())
}

func TestTaskNodeGetInitialStatus(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	stageNode.AddTaskNode(taskNode)
	pipelineNode.AddStageNode(stageNode)

	assert.Equal(t, InitializingNodeStatus, taskNode.GetInitialStatus())
}

func TestTaskGetDependencies(t *testing.T) {
	taskNode := NewTaskNode(&NewTaskNodeInput{
		Dependencies: []string{"t2"},
	})

	assert.Equal(t, taskNode.GetDependencies(), []string{"t2"})
}

func TestTaskGetStatusChanges(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	// Arbitrary status changes.
	require.NoError(t, taskNode.SetStatus(ReadyNodeStatus))
	require.NoError(t, taskNode.SetStatus(CanceledNodeStatus))

	expect := []NodeStatusChange{
		{
			OldStatus: createdStatus,
			NewStatus: ReadyNodeStatus,
			NodePath:  taskNode.path,
			NodeType:  taskNode.typ,
		},
		{
			OldStatus: ReadyNodeStatus,
			NewStatus: CanceledNodeStatus,
			NodePath:  taskNode.path,
			NodeType:  taskNode.typ,
		},
	}

	assert.Equal(t, expect, taskNode.GetStatusChanges())
}

func TestTaskAddActionNode(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)

	assert.Equal(t, []*ActionNode{actionNode}, taskNode.Actions())
}

func TestTaskNodeChildrenNodes(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)

	assert.Equal(t, []Node{actionNode}, taskNode.ChildrenNodes())
}

func TestTaskNodePath(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Status: &createdStatus,
	})

	assert.Equal(t, "pipeline.stage.s1.task.t1", taskNode.Path())
}

func TestTaskNodeStatus(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Status: &createdStatus,
	})

	assert.Equal(t, createdStatus, taskNode.Status())
}

func TestTaskNodeCancel(t *testing.T) {
	type testCase struct {
		name     string
		status   NodeStatus
		expected NodeStatus
	}

	testCases := []testCase{
		{
			name:     "created should return skipped",
			status:   CreatedNodeStatus,
			expected: SkippedNodeStatus,
		},
		{
			name:     "initializing should return skipped",
			status:   InitializingNodeStatus,
			expected: SkippedNodeStatus,
		},
		{
			name:     "ready should return canceled",
			status:   ReadyNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "running should return canceling",
			status:   RunningNodeStatus,
			expected: CancelingNodeStatus,
		},
		{
			name:     "canceling should return canceled",
			status:   CancelingNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "approval pending should return canceled",
			status:   ApprovalPendingNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "waiting should return canceled",
			status:   WaitingNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "final status should not be changed",
			status:   SkippedNodeStatus,
			expected: SkippedNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Status: &tc.status,
			})

			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, CreatedNodeStatus)
			taskNode.AddActionNode(actionNode)

			require.NoError(t, taskNode.Cancel())
			assert.Equal(t, tc.expected, taskNode.Status())
			assert.Equal(t, SkippedNodeStatus, actionNode.Status())
		})
	}
}

func TestTaskNodeAccept(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	traversalCtx := &TraversalContext{}
	visitor := &PartialVisitor{}

	require.NoError(t, taskNode.Accept(visitor, traversalCtx))
}

func TestTaskNodeParent(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	assert.Equal(t, stageNode, taskNode.Parent())
}

func TestTaskNodeResetAllActions(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)

	require.NoError(t, taskNode.resetAllActions())
	assert.Equal(t, CreatedNodeStatus, actionNode.Status())
}

func TestTaskNodeGetOnError(t *testing.T) {
	taskNode := NewTaskNode(&NewTaskNodeInput{
		OnError: FailOnError,
	})

	assert.Equal(t, FailOnError, taskNode.GetOnErrorStrategy())
}

func TestTaskNodeHandleActionFailed(t *testing.T) {
	type testCase struct {
		name             string
		taskStatus       NodeStatus
		expectTaskStatus NodeStatus
	}

	testCases := []testCase{
		{
			name:             "running task should become failed",
			taskStatus:       RunningNodeStatus,
			expectTaskStatus: FailedNodeStatus,
		},
		{
			name:             "task in final status should not change",
			taskStatus:       FailedNodeStatus,
			expectTaskStatus: FailedNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Status: &tc.taskStatus,
			})

			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, CreatedNodeStatus)
			taskNode.AddActionNode(actionNode)

			require.NoError(t, taskNode.handleActionFailed())
			assert.Equal(t, tc.expectTaskStatus, taskNode.Status())
		})
	}
}

func TestTaskNodeHandleActionCanceled(t *testing.T) {
	type testCase struct {
		name               string
		taskStatus         NodeStatus
		expectStatusChange bool
	}

	testCases := []testCase{
		{
			name:               "canceling task should become canceled",
			taskStatus:         CancelingNodeStatus,
			expectStatusChange: true,
		},
		{
			name:       "task in final status should not change",
			taskStatus: FailedNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Status: &tc.taskStatus,
			})

			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, CanceledNodeStatus)
			actionNode2 := NewActionNode("pipeline.stage.s1.task.t1.action.a2", taskNode, CreatedNodeStatus)
			taskNode.AddActionNode(actionNode)
			taskNode.AddActionNode(actionNode2)

			require.NoError(t, taskNode.handleActionCanceled())

			if tc.expectStatusChange {
				assert.Equal(t, CanceledNodeStatus, taskNode.Status())
				assert.Equal(t, SkippedNodeStatus, actionNode2.Status())
			} else {
				assert.Equal(t, tc.taskStatus, taskNode.Status())
				assert.Equal(t, CreatedNodeStatus, actionNode2.Status())
			}
		})
	}
}

func TestTaskNodeHandleActionSucceeded(t *testing.T) {
	createdStatus := CreatedNodeStatus

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)

	require.NoError(t, taskNode.handleActionSucceeded())
	assert.Equal(t, ReadyNodeStatus, actionNode.Status())
}

func TestTaskNodeEventListeners(t *testing.T) {
	// Test that the status event listeners are called when the status changes and
	// parent node status is updated accordingly

	type testCase struct {
		name               string
		newStatus          NodeStatus
		siblingTaskStatus  NodeStatus
		currentStageStatus NodeStatus
		expectStageStatus  NodeStatus
	}

	testCases := []testCase{
		{
			name:               "stage should become canceling when all tasks become canceling",
			newStatus:          CancelingNodeStatus,
			currentStageStatus: RunningNodeStatus,
			expectStageStatus:  CancelingNodeStatus,
		},
		{
			name:               "stage status should remain unchanged when task becomes canceling and sibling task is still running",
			newStatus:          CancelingNodeStatus,
			currentStageStatus: RunningNodeStatus,
			siblingTaskStatus:  RunningNodeStatus,
			expectStageStatus:  RunningNodeStatus,
		},
		{
			name:               "stage should become canceled when all tasks become canceled",
			newStatus:          CanceledNodeStatus,
			currentStageStatus: RunningNodeStatus,
			expectStageStatus:  CanceledNodeStatus,
		},
		{
			name:               "stage status should remain unchanged when task becomes canceled and sibling task is still running",
			newStatus:          CanceledNodeStatus,
			currentStageStatus: RunningNodeStatus,
			siblingTaskStatus:  RunningNodeStatus,
			expectStageStatus:  RunningNodeStatus,
		},
		{
			name:               "stage should become failed when all tasks become failed",
			newStatus:          FailedNodeStatus,
			currentStageStatus: RunningNodeStatus,
			expectStageStatus:  FailedNodeStatus,
		},
		{
			name:               "stage status should remain unchanged when task become failed and sibling task is still running",
			newStatus:          FailedNodeStatus,
			currentStageStatus: RunningNodeStatus,
			siblingTaskStatus:  RunningNodeStatus,
			expectStageStatus:  RunningNodeStatus,
		},
		{
			name:               "failed task status should take precedence over canceled task status",
			newStatus:          FailedNodeStatus,
			currentStageStatus: RunningNodeStatus,
			siblingTaskStatus:  CanceledNodeStatus,
			expectStageStatus:  FailedNodeStatus,
		},
		{
			name:               "canceled task should take precedence over succeeded task status",
			newStatus:          SucceededNodeStatus,
			currentStageStatus: RunningNodeStatus,
			siblingTaskStatus:  CanceledNodeStatus,
			expectStageStatus:  CanceledNodeStatus,
		},
		{
			name:               "stage should remain in final state when task status is unsuccessful",
			newStatus:          CanceledNodeStatus,
			currentStageStatus: CanceledNodeStatus,
			expectStageStatus:  CanceledNodeStatus,
		},
		{
			name:               "stage should become succeeded when all tasks become succeeded",
			newStatus:          SucceededNodeStatus,
			currentStageStatus: RunningNodeStatus,
			expectStageStatus:  SucceededNodeStatus,
		},
		{
			name:               "stage status should remain unchanged when task become succeeded and sibling task is still running",
			newStatus:          SucceededNodeStatus,
			currentStageStatus: RunningNodeStatus,
			siblingTaskStatus:  RunningNodeStatus,
			expectStageStatus:  RunningNodeStatus,
		},
		{
			name:               "stage should be succeeded if all tasks are deferred",
			newStatus:          DeferredNodeStatus,
			currentStageStatus: CreatedNodeStatus,
			expectStageStatus:  SucceededNodeStatus,
		},
		{
			name:               "stage status should remain unchanged when task is undefered and stage is not deferred",
			newStatus:          CreatedNodeStatus,
			currentStageStatus: RunningNodeStatus,
			expectStageStatus:  RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			stageNode := NewStageNode("pipeline.stage.s1", nil, tc.currentStageStatus)

			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Status: &tc.currentStageStatus,
				Parent: stageNode,
			})

			stageNode.AddTaskNode(taskNode)

			// Add a sibling task node if needed
			if tc.siblingTaskStatus != "" {
				siblingTaskNode := NewTaskNode(&NewTaskNodeInput{
					Path:   "pipeline.stage.s1.task.t2",
					Status: &tc.siblingTaskStatus,
					Parent: stageNode,
				})

				stageNode.AddTaskNode(siblingTaskNode)
			}

			// Initialize the stage node, this will register the event listeners
			stageNode.init()

			// Set the new status of first task
			require.NoError(t, stageNode.tasks[0].SetStatus(tc.newStatus))

			// Check that the stage status is as expected
			assert.Equal(t, tc.expectStageStatus, stageNode.Status())
		})
	}
}
