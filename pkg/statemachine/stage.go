package statemachine

import (
	"fmt"
)

var _ Node = (*StageNode)(nil)

// StageNode represents a stage in a pipeline
type StageNode struct {
	nodeImpl
	tasks           []*TaskNode
	nestedPipelines []*PipelineNode
}

// NewStageNode creates a new stage node
func NewStageNode(path string, parent Node, status NodeStatus) *StageNode {
	return &StageNode{
		nodeImpl: nodeImpl{
			path:          path,
			typ:           StageNodeType,
			parent:        parent,
			status:        status,
			statusChanges: []NodeStatusChange{},
		},
		tasks:           []*TaskNode{},
		nestedPipelines: []*PipelineNode{},
	}
}

// Copy returns a copy of the node
func (n *StageNode) Copy() *StageNode {
	node := &StageNode{
		nodeImpl: nodeImpl{
			path:          n.path,
			typ:           n.typ,
			parent:        n.parent,
			status:        n.status,
			statusChanges: n.statusChanges,
		},
		tasks:           []*TaskNode{},
		nestedPipelines: []*PipelineNode{},
	}
	for _, task := range n.tasks {
		node.AddTaskNode(task.Copy())
	}
	for _, pipeline := range n.nestedPipelines {
		node.AddNestedPipelineNode(pipeline.Copy())
	}
	return node
}

// SetStatus sets the node status
func (n *StageNode) SetStatus(status NodeStatus) error {
	if err := n.nodeImpl.SetStatus(status); err != nil {
		return err
	}

	switch status {
	case RunningNodeStatus:
		for _, child := range n.ChildrenNodes() {
			if len(child.(StageChildNode).GetDependencies()) > 0 {
				// Child is dependent on its siblings, it should be
				// started after they're completed.
				continue
			}

			if child.Status().Equals(CreatedNodeStatus) {
				if err := child.SetStatus(child.GetInitialStatus()); err != nil {
					return err
				}
			}
		}
	case DeferredNodeStatus, SkippedNodeStatus:
		if err := n.skipRemainingChildren(); err != nil {
			return err
		}
	case CreatedNodeStatus:
		for _, child := range n.ChildrenNodes() {
			if err := child.SetStatus(CreatedNodeStatus); err != nil {
				return err
			}
		}
	}

	return nil
}

// ChildrenNodes returns a list of this stage node's children.
// Each child returned by this function is a StageChildNode, not just a Node, so it has a GetOnErrorStrategy method.
func (n *StageNode) ChildrenNodes() []Node {
	nodes := make([]Node, len(n.tasks)+len(n.nestedPipelines))
	for i, t := range n.tasks {
		nodes[i] = t
	}
	for i, p := range n.nestedPipelines {
		nodes[i+len(n.tasks)] = p
	}

	return nodes
}

// AddTaskNode adds a task node
func (n *StageNode) AddTaskNode(t *TaskNode) {
	t.init()
	n.tasks = append(n.tasks, t)
}

// AddNestedPipelineNode adds a nested pipeline
func (n *StageNode) AddNestedPipelineNode(p *PipelineNode) {
	p.init()
	n.nestedPipelines = append(n.nestedPipelines, p)
}

func (n *StageNode) init() {
	for _, node := range n.ChildrenNodes() {
		// Task or nested pipeline skipped
		node.registerListener(SkippedNodeStatus, n.handleChildSkipped)
		// Task or nested pipeline running
		node.registerListener(RunningNodeStatus, n.handleChildRunning)
		// Task or nested pipeline initializing
		node.registerListener(InitializingNodeStatus, n.handleChildInitializing)
		// Task or nested pipeline canceling
		node.registerListener(CancelingNodeStatus, n.handleChildCanceling)
		// Task or nested pipeline deferred
		node.registerListener(DeferredNodeStatus, n.handleChildDeferred)
		// Task or nested pipeline failed
		node.registerListener(FailedNodeStatus, n.handleChildUnsuccessfulStatus)
		// Task or nested pipeline canceled
		node.registerListener(CanceledNodeStatus, n.handleChildUnsuccessfulStatus)
		// Task or nested pipeline succeeded
		node.registerListener(SucceededNodeStatus, n.handleChildSucceeded)
	}
}

// Accept a visitor
func (n *StageNode) Accept(v Visitor, traversalCtx *TraversalContext) error {
	if err := v.VisitForAny(n, traversalCtx); err != nil {
		return err
	}

	return v.VisitForStage(n)
}

// Cancel cancels the node
func (n *StageNode) Cancel() error {
	if err := n.nodeImpl.Cancel(); err != nil {
		return err
	}

	// Cancel all children
	for _, child := range n.ChildrenNodes() {
		if err := child.Cancel(); err != nil {
			return err
		}
	}

	return nil
}

/* Helper functions */

// skipRemainingChildren skips the remaining children
func (n *StageNode) skipRemainingChildren() error {
	for _, child := range n.ChildrenNodes() {
		if child.Status().IsSkippableStatus() {
			if err := child.SetStatus(SkippedNodeStatus); err != nil {
				return err
			}
		}
	}

	return nil
}

/* Child event listeners */

// handleChildSkipped handles a skipped child event
func (n *StageNode) handleChildSkipped() error {
	if n.status.IsFinalStatus() {
		// No need to process children if the stage is already in a final state.
		return nil
	}

	if err := n.setDependentNodeStatus(
		func(s NodeStatus) bool {
			// Only target children that haven't started yet.
			return s.Equals(CreatedNodeStatus)
		},
		func(node Node) bool {
			// A dependent should only be skipped if all its dependencies are skipped.
			return node.Status().Equals(SkippedNodeStatus)
		},
		func(node Node) error {
			// Skip the child node since all its dependencies are skipped.
			return node.SetStatus(SkippedNodeStatus)
		},
	); err != nil {
		return err
	}

	return n.setFinalStatusBasedOnChildren()
}

// handleChildRunning handles a running child event
func (n *StageNode) handleChildRunning() error {
	if err := n.setDependentNodeStatus(
		func(s NodeStatus) bool {
			// We only need to reset children that are skipped.
			return s.Equals(SkippedNodeStatus)
		},
		func(node Node) bool {
			// We're also passing in the created status since a dependency of a dependency
			// will be reset to created if it's deferred.
			return node.Status().Equals(RunningNodeStatus) || node.Status().Equals(CreatedNodeStatus)
		},
		func(node Node) error {
			// Reset the child node since its dependency has been set to running
			return node.SetStatus(CreatedNodeStatus)
		},
	); err != nil {
		return err
	}

	return n.SetStatus(RunningNodeStatus)
}

// handleChildInitializing handles a ready child event
func (n *StageNode) handleChildInitializing() error {
	if err := n.setDependentNodeStatus(
		func(s NodeStatus) bool {
			// We only need to reset children that are skipped.
			return s.Equals(SkippedNodeStatus)
		},
		func(node Node) bool {
			// We're also passing in the created status since a dependency of a dependency
			// will be reset to created if it's deferred.
			return node.Status().Equals(InitializingNodeStatus) || node.Status().Equals(CreatedNodeStatus)
		},
		func(node Node) error {
			// Reset the child node since its dependency is being reset.
			return node.SetStatus(CreatedNodeStatus)
		},
	); err != nil {
		return err
	}

	return n.SetStatus(RunningNodeStatus)
}

// handleChildCanceling handles a canceling child event
func (n *StageNode) handleChildCanceling() error {
	for _, child := range n.ChildrenNodes() {
		if !child.Status().IsFinalStatus() && !child.Status().Equals(CancelingNodeStatus) {
			// Not all children are canceling or complete.
			return nil
		}
	}

	return n.SetStatus(CancelingNodeStatus)
}

// handleChildDeferred handles a deferred child event
func (n *StageNode) handleChildDeferred() error {
	if n.status.IsFinalStatus() {
		// No need to process children if the stage is already in a final state.
		return nil
	}

	if err := n.setDependentNodeStatus(
		func(s NodeStatus) bool {
			// Only target children that haven't started yet.
			return s.Equals(CreatedNodeStatus)
		},
		func(node Node) bool {
			// A dependent should only be deferred if all its dependencies are deferred.
			return node.Status().Equals(DeferredNodeStatus)
		},
		func(node Node) error {
			// Skip the child node since all its dependencies are deferred.
			return node.SetStatus(SkippedNodeStatus)
		},
	); err != nil {
		return err
	}

	for _, child := range n.ChildrenNodes() {
		if !child.Status().IsFinalStatus() {
			// At least one child is not in a final state.
			return nil
		}
	}

	return n.setFinalStatusBasedOnChildren()
}

// handleChildUnsuccessfulStatus handles a unsuccessful (fail, cancel) task or nested pipeline event
func (n *StageNode) handleChildUnsuccessfulStatus() error {
	if n.status.IsFinalStatus() {
		// Stage was already marked unsuccessful (could've been canceled elsewhere).
		return nil
	}

	// Must initialize dependents since this failed child could allow failure.
	if err := n.initializeDependents(); err != nil {
		return err
	}

	return n.setFinalStatusBasedOnChildren()
}

// handleChildSucceeded handles a succeeded child event.
func (n *StageNode) handleChildSucceeded() error {
	if err := n.initializeDependents(); err != nil {
		return err
	}

	return n.setFinalStatusBasedOnChildren()
}

// initializeDependents initializes the children nodes that are dependent on their siblings.
// The children nodes will be put into their initial state once their dependencies have succeeded.
func (n *StageNode) initializeDependents() error {
	return n.setDependentNodeStatus(
		func(s NodeStatus) bool {
			// Only target children that haven't started yet
			return s.Equals(CreatedNodeStatus)
		},
		func(node Node) bool {
			// A dependent should only be initialized if all its dependencies are successful.
			// Successful dependencies are those that have either succeeded or are allowed
			// to continue on error (failed, canceled).
			return node.Status().Equals(SucceededNodeStatus) ||
				(node.Status().Equals(FailedNodeStatus) && node.(StageChildNode).GetOnErrorStrategy().Equals(ContinueOnError)) ||
				(node.Status().Equals(CanceledNodeStatus) && node.(StageChildNode).GetOnErrorStrategy().Equals(ContinueOnError))
		},
		func(node Node) error {
			// Initialize the child node since all its dependencies have succeeded.
			return node.SetStatus(node.GetInitialStatus())
		},
	)
}

// setDependentNodeStatus sets the status of dependent nodes based on their dependencies.
// Dependencies will only be processed for children nodes that are in the statusFilter state.
// The isDependencySuccessful function is used to determine if a dependency is successful.
// The setDependentNodeStatus function is used to update the status of the dependent node.
func (n *StageNode) setDependentNodeStatus(
	statusFilter func(s NodeStatus) bool,
	isDependencySuccessful func(node Node) bool,
	setDependentNodeStatus func(node Node) error,
) error {
	childrenNodes := n.ChildrenNodes()

	// Create a map of child nodes using their names for easy lookup
	childNodeMap := make(map[string]Node)
	for _, child := range childrenNodes {
		childNodeMap[child.Name()] = child
	}

	for _, child := range childrenNodes {
		if !statusFilter(child.Status()) {
			continue
		}

		var (
			dependenciesSucceeded int
			dependencies          = child.(StageChildNode).GetDependencies()
		)

		for _, dependencyName := range dependencies {
			dependencyNode, exists := childNodeMap[dependencyName]
			if !exists {
				return fmt.Errorf("node with name %s not found in stage %s", dependencyName, n.Name())
			}

			if isDependencySuccessful(dependencyNode) {
				dependenciesSucceeded++
			}
		}

		if dependenciesSucceeded > 0 && dependenciesSucceeded == len(dependencies) {
			if err := setDependentNodeStatus(child); err != nil {
				return err
			}
		}
	}

	return nil
}

// setFinalStatusBasedOnChildren evaluates the children nodes to determine the final status of the stage.
// The stage will be marked as failed if any of the children have failed, or canceled unless all children
// allow failure. Otherwise, the stage will be marked as succeeded once all children have completed.
func (n *StageNode) setFinalStatusBasedOnChildren() error {
	var hasFailedChild, hasCanceledChild = false, false

	// Check all children for an unsuccessful status.
	for _, c := range n.ChildrenNodes() {
		if !c.Status().IsFinalStatus() && !c.Status().Equals(CreatedNodeStatus) {
			// Must wait for all children to complete before setting final status.
			return nil
		}

		if c.(StageChildNode).GetOnErrorStrategy().Equals(ContinueOnError) {
			continue
		}

		switch c.Status() {
		case FailedNodeStatus:
			hasFailedChild = true
		case CanceledNodeStatus:
			hasCanceledChild = true
		}
	}

	// Failed status > Canceled status > Succeeded status.
	switch {
	case hasFailedChild:
		if err := n.skipRemainingChildren(); err != nil {
			return err
		}

		if err := n.SetStatus(FailedNodeStatus); err != nil {
			return err
		}
	case hasCanceledChild:
		if err := n.skipRemainingChildren(); err != nil {
			return err
		}

		if err := n.SetStatus(CanceledNodeStatus); err != nil {
			return err
		}
	}

	if n.status.IsFinalStatus() {
		// Stage has been marked as failed or canceled.
		return nil
	}

	return n.SetStatus(SucceededNodeStatus)
}
