package statemachine

import "fmt"

// Visitor is used to visit each node in the pipeline graph
type Visitor interface {
	VisitForPipeline(pipeline *PipelineNode, traversalCtx *TraversalContext) error
	VisitForStage(stage *StageNode) error
	VisitForTask(task *TaskNode, traversalCtx *TraversalContext) error
	VisitForAction(action *ActionNode, traversalCtx *TraversalContext) error
	VisitForAny(node Node, traversalCtx *TraversalContext) error
}

// PartialVisitor provides a noop implementation of the required visitor functions for
// visitors that only want to implement a subset of the functions
type PartialVisitor struct{}

// VisitForPipeline visits a pipeline node
func (v *PartialVisitor) VisitForPipeline(_ *PipelineNode, _ *TraversalContext) error {
	return nil
}

// VisitForStage visits a stage node
func (v *PartialVisitor) VisitForStage(_ *StageNode) error {
	return nil
}

// VisitForTask visits a task node
func (v *PartialVisitor) VisitForTask(_ *TaskNode, _ *TraversalContext) error {
	return nil
}

// VisitForAction visits an action node
func (v *PartialVisitor) VisitForAction(_ *ActionNode, _ *TraversalContext) error {
	return nil
}

// VisitForAny visits all nodes
func (v *PartialVisitor) VisitForAny(_ Node, _ *TraversalContext) error {
	return nil
}

// PrettyPrintVisitor prints the state of each node in the pipeline graph
type PrettyPrintVisitor struct {
	PartialVisitor
	currentStage *StageNode
}

// VisitForPipeline visits a pipeline node
func (v *PrettyPrintVisitor) VisitForPipeline(pipeline *PipelineNode, _ *TraversalContext) error {
	fmt.Printf("pipeline: state=%s\n", pipeline.Status())
	return nil
}

// VisitForStage visits a stage node
func (v *PrettyPrintVisitor) VisitForStage(stage *StageNode) error {
	fmt.Printf("   stage: path=%s state=%s\n", stage.Path(), stage.Status())
	v.currentStage = stage
	return nil
}

// VisitForTask visits a task node
func (v *PrettyPrintVisitor) VisitForTask(task *TaskNode, _ *TraversalContext) error {
	fmt.Printf("      task: path=%s state=%s\n", task.Path(), task.Status())
	return nil
}

// VisitForAction visits an action node
func (v *PrettyPrintVisitor) VisitForAction(action *ActionNode, traversalCtx *TraversalContext) error {
	fmt.Printf("         action: path=%s state=%s (dependent_on=%d actions )\n", action.Path(), action.Status(), len(traversalCtx.VisitedActions()))
	return nil
}
