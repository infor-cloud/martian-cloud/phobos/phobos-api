package statemachine

import "fmt"

// TraversalContext tracks state as a visitor traverses the pipeline graph
type TraversalContext struct {
	visitedActions []*ActionNode
}

// Copy creates a copy of the traversal context
func (t *TraversalContext) Copy() *TraversalContext {
	visitedActionsCopy := make([]*ActionNode, len(t.visitedActions))
	copy(visitedActionsCopy, t.visitedActions)
	return &TraversalContext{visitedActions: visitedActionsCopy}
}

func (t *TraversalContext) addVisited(n Node) {
	switch typed := n.(type) {
	case *ActionNode:
		t.visitedActions = append(t.visitedActions, typed)
	}
}

// VisitedActions returns a list of visited actions
func (t *TraversalContext) VisitedActions() []*ActionNode {
	return t.visitedActions
}

// SimulatedTraverser is a traverser that simulates a traversal of the pipeline graph in the order
// that the nodes would be executed in a real pipeline run
type SimulatedTraverser struct {
	stateMachine *StateMachine
}

// NewSimulatedTraverser creates a new simulated traverser
func NewSimulatedTraverser(stateMachine *StateMachine) *SimulatedTraverser {
	// Create a copy of the state machine so that we don't mutate the original
	smCopy := stateMachine.Copy()
	return &SimulatedTraverser{stateMachine: smCopy}
}

// Accept accepts a visitor and visits each node in the graph
func (t *SimulatedTraverser) Accept(v Visitor) error {
	traversalCtx := &TraversalContext{visitedActions: []*ActionNode{}}

	// Reset state machine
	if err := t.stateMachine.Reset(); err != nil {
		return err
	}

	// Get all tasks and pipelines that are in the INITIALIZING state
	targetStatus := InitializingNodeStatus

	if err := t.stateMachine.GetPipelineNode().SetStatus(RunningNodeStatus); err != nil {
		return err
	}

	// Continue until pipeline is completed
	for t.stateMachine.GetPipelineNode().Status() != SucceededNodeStatus {
		// Get all tasks and pipelines in the INITIALIZING state
		initializingNodes, err := t.stateMachine.GetNodes(&GetNodesInput{
			Status:    &targetStatus,
			NodeTypes: []NodeType{TaskNodeType, PipelineNodeType},
		})
		if err != nil {
			return err
		}

		visitedNodes := []Node{}

		for _, n := range initializingNodes {
			// Mark the node as READY since we'll be running it.
			if err := n.SetStatus(ReadyNodeStatus); err != nil {
				return err
			}

			switch typed := n.(type) {
			case *TaskNode:
				taskTraversalContext := traversalCtx.Copy()

				// Visit actions
				for _, a := range typed.actions {
					if err := a.Accept(v, taskTraversalContext); err != nil {
						return err
					}
					// Update node state to simulate that it has completed
					if err := a.SetStatus(SucceededNodeStatus); err != nil {
						return err
					}

					taskTraversalContext.addVisited(a)
					visitedNodes = append(visitedNodes, a)
				}

				if err := n.Accept(v, taskTraversalContext); err != nil {
					return err
				}

				// Update node state to simulate that it has completed
				if err := n.SetStatus(SucceededNodeStatus); err != nil {
					return err
				}
			case *PipelineNode:
				// Visit pipeline
				if len(typed.ChildrenNodes()) == 0 {
					if err := n.Accept(v, traversalCtx); err != nil {
						return err
					}

					if err := n.SetStatus(SucceededNodeStatus); err != nil {
						return err
					}
				}
			default:
				return fmt.Errorf("unexpected node type: %T", n)
			}

			visitedNodes = append(visitedNodes, n)
		}

		// Update traversal context after all nodes in the current batch have been visited
		for _, n := range visitedNodes {
			traversalCtx.addVisited(n)
		}
	}

	return nil
}

// SimpleTraverser will visit each node in the graph
type SimpleTraverser struct {
	stateMachine *StateMachine
}

// Accept accepts a visitor
func (t *SimpleTraverser) Accept(v Visitor) error {
	traversalCtx := &TraversalContext{visitedActions: []*ActionNode{}}

	err := t.accept(t.stateMachine.pipeline, v, traversalCtx)
	if err != nil {
		return err
	}

	traversalCtx.addVisited(t.stateMachine.pipeline)

	return nil
}

func (t *SimpleTraverser) accept(node Node, v Visitor, traversalCtx *TraversalContext) error {
	if err := node.Accept(v, traversalCtx); err != nil {
		return err
	}

	traversalCtx.addVisited(node)

	for _, c := range node.ChildrenNodes() {
		if err := t.accept(c, v, traversalCtx); err != nil {
			return err
		}
	}

	return nil
}
