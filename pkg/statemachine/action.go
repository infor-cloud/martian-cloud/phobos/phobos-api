package statemachine

var _ Node = (*ActionNode)(nil)

// ActionNode represents a pipeline action
type ActionNode struct {
	nodeImpl
}

// NewActionNode creates a new action node
func NewActionNode(path string, parent Node, status NodeStatus) *ActionNode {
	return &ActionNode{
		nodeImpl: nodeImpl{
			path:          path,
			typ:           ActionNodeType,
			parent:        parent,
			status:        status,
			statusChanges: []NodeStatusChange{},
		},
	}
}

// Copy returns a copy of the node
func (n *ActionNode) Copy() *ActionNode {
	return &ActionNode{
		nodeImpl: nodeImpl{
			path:          n.path,
			typ:           n.typ,
			parent:        n.parent,
			status:        n.status,
			statusChanges: n.statusChanges,
		},
	}
}

// Accept a visitor
func (n *ActionNode) Accept(v Visitor, traversalCtx *TraversalContext) error {
	if err := v.VisitForAny(n, traversalCtx); err != nil {
		return err
	}
	return v.VisitForAction(n, traversalCtx)
}

// Cancel cancels the node
func (n *ActionNode) Cancel() error {
	if n.Status().IsFinalStatus() {
		return nil
	}

	var status NodeStatus
	switch n.status {
	case CreatedNodeStatus:
		// Nodes with the created status transition to skipped since
		// they were never started
		status = SkippedNodeStatus
	case CancelingNodeStatus,
		ApprovalPendingNodeStatus,
		WaitingNodeStatus:
		// These statuses can all transition directly to canceled
		// because they have not been started
		status = CanceledNodeStatus
	default:
		// Running and pending nodes transition to canceling since
		// the cancellation process is asynchronous
		status = CancelingNodeStatus
	}

	return n.SetStatus(status)
}

// ChildrenNodes returns a list of child nodes
func (n *ActionNode) ChildrenNodes() []Node {
	return []Node{}
}
