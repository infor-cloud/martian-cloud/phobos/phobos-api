package statemachine

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/errors"
)

func TestNewStageNode(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})

	expectedStageNode := &StageNode{
		nodeImpl: nodeImpl{
			path:          "pipeline.stage.s1",
			typ:           StageNodeType,
			parent:        pipelineNode,
			status:        CreatedNodeStatus,
			statusChanges: []NodeStatusChange{},
		},
		tasks:           []*TaskNode{},
		nestedPipelines: []*PipelineNode{},
	}

	assert.Equal(t, expectedStageNode, NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus))
}

func TestStageNodeCopy(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)
	stageNode.AddTaskNode(taskNode)

	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline.stage.s1.pipeline.p1",
		Status: createdStatus,
		Parent: stageNode,
	})
	stageNode.AddNestedPipelineNode(nestedPipelineNode)

	copied := stageNode.Copy()

	assert.Len(t, copied.tasks, len(stageNode.tasks))
	assert.Len(t, copied.nestedPipelines, len(stageNode.nestedPipelines))
	assert.NotSame(t, stageNode, copied)
}

func TestStageNodeSetStatusRunning(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})
	stageNode.AddTaskNode(taskNode)

	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:         "pipeline.stage.s1.pipeline.p1",
		Status:       createdStatus,
		Parent:       stageNode,
		Dependencies: []string{taskNode.Name()},
	})
	stageNode.AddNestedPipelineNode(nestedPipelineNode)

	pipelineNode.AddStageNode(stageNode)

	require.NoError(t, stageNode.SetStatus(RunningNodeStatus))
	assert.Equal(t, RunningNodeStatus, stageNode.Status())
	assert.Equal(t, InitializingNodeStatus, taskNode.Status())
	assert.Equal(t, CreatedNodeStatus, nestedPipelineNode.Status()) // Dependent nodes shouldn't change.
}

func TestStageNodeSetStatusSkipped(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})
	stageNode.AddTaskNode(taskNode)

	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:         "pipeline.stage.s1.pipeline.p1",
		Status:       createdStatus,
		Parent:       stageNode,
		Dependencies: []string{taskNode.Name()}, // Nested pipeline should be skipped automatically since it depends on the task.
	})
	stageNode.AddNestedPipelineNode(nestedPipelineNode)

	pipelineNode.AddStageNode(stageNode)

	require.NoError(t, stageNode.SetStatus(SkippedNodeStatus))
	assert.Equal(t, SkippedNodeStatus, stageNode.Status())
	assert.Equal(t, SkippedNodeStatus, taskNode.Status())
	assert.Equal(t, SkippedNodeStatus, nestedPipelineNode.Status())
}

func TestStageNodeSetStatusCreated(t *testing.T) {
	canceledStatus := CanceledNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: canceledStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, canceledStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &canceledStatus,
	})
	stageNode.AddTaskNode(taskNode)

	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline.stage.s1.pipeline.p1",
		Status: canceledStatus,
		Parent: stageNode,
	})
	stageNode.AddNestedPipelineNode(nestedPipelineNode)

	pipelineNode.AddStageNode(stageNode)

	require.NoError(t, stageNode.SetStatus(CreatedNodeStatus))
	assert.Equal(t, CreatedNodeStatus, stageNode.Status())
	assert.Equal(t, CreatedNodeStatus, taskNode.Status())
	assert.Equal(t, CreatedNodeStatus, nestedPipelineNode.Status())
}

func TestStageGetStatusChanges(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	// Arbitrary status changes.
	require.NoError(t, stageNode.SetStatus(ReadyNodeStatus))
	require.NoError(t, stageNode.SetStatus(CanceledNodeStatus))

	expect := []NodeStatusChange{
		{
			OldStatus: createdStatus,
			NewStatus: ReadyNodeStatus,
			NodePath:  stageNode.path,
			NodeType:  stageNode.typ,
		},
		{
			OldStatus: ReadyNodeStatus,
			NewStatus: CanceledNodeStatus,
			NodePath:  stageNode.path,
			NodeType:  stageNode.typ,
		},
	}

	assert.Equal(t, expect, stageNode.GetStatusChanges())
}

func TestStageNodeChildrenNodes(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	stageNode.AddTaskNode(taskNode)

	assert.Len(t, stageNode.ChildrenNodes(), 1)
	assert.Equal(t, taskNode, stageNode.ChildrenNodes()[0])
}

func TestStageNodeAddTaskNode(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	stageNode.AddTaskNode(taskNode)

	assert.Len(t, stageNode.tasks, 1)
	assert.Equal(t, taskNode, stageNode.tasks[0])
}

func TestStageNodeAddNestedPipelineNode(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline.stage.s1.pipeline.p1",
		Status: createdStatus,
		Parent: stageNode,
	})
	stageNode.AddNestedPipelineNode(nestedPipelineNode)

	assert.Len(t, stageNode.nestedPipelines, 1)
	assert.Equal(t, nestedPipelineNode, stageNode.nestedPipelines[0])
}

func TestStageNodeAccept(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	stageNode.AddTaskNode(taskNode)

	visitor := &PartialVisitor{}
	traversalContext := &TraversalContext{}

	require.NoError(t, stageNode.Accept(visitor, traversalContext))
}

func TestStageNodeCancel(t *testing.T) {
	type testCase struct {
		name     string
		status   NodeStatus
		expected NodeStatus
	}

	testCases := []testCase{
		{
			name:     "created should return skipped",
			status:   CreatedNodeStatus,
			expected: SkippedNodeStatus,
		},
		{
			name:     "initializing should return skipped",
			status:   InitializingNodeStatus,
			expected: SkippedNodeStatus,
		},
		{
			name:     "ready should return canceled",
			status:   ReadyNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "running should return canceling",
			status:   RunningNodeStatus,
			expected: CancelingNodeStatus,
		},
		{
			name:     "canceling should return canceled",
			status:   CancelingNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "final status should not be changed",
			status:   SucceededNodeStatus,
			expected: SucceededNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: CreatedNodeStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, tc.status)

			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: &tc.status,
			})

			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, CreatedNodeStatus)
			taskNode.AddActionNode(actionNode)
			stageNode.AddTaskNode(taskNode)

			nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline.stage.s1.pipeline.p1",
				Status: tc.status,
				Parent: stageNode,
			})
			stageNode.AddNestedPipelineNode(nestedPipelineNode)

			pipelineNode.AddStageNode(stageNode)

			require.NoError(t, stageNode.Cancel())
			assert.Equal(t, tc.expected, stageNode.Status())
			assert.Equal(t, tc.expected, taskNode.Status())
			assert.Equal(t, SkippedNodeStatus, actionNode.Status())
			assert.Equal(t, tc.expected, nestedPipelineNode.Status())
		})
	}
}

func TestStageNodeParent(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)

	assert.Equal(t, pipelineNode, stageNode.Parent())
}

func TestStageNodePath(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)

	assert.Equal(t, "pipeline.stage.s1", stageNode.Path())
}

func TestStageNodeStatus(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)

	assert.Equal(t, CreatedNodeStatus, stageNode.Status())
}

func TestStageGetInitialStatus(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)

	assert.Equal(t, ReadyNodeStatus, stageNode.GetInitialStatus())
}

func TestStageNodeSkipRemainingChildren(t *testing.T) {
	type testCase struct {
		name            string
		initialStatuses []NodeStatus
		expectSkipped   bool
	}

	testCases := []testCase{
		{
			name: "created and initializing should be skipped",
			initialStatuses: []NodeStatus{
				CreatedNodeStatus,
				InitializingNodeStatus,
			},
			expectSkipped: true,
		},
		{
			name: "all others should remain unchanged",
			initialStatuses: []NodeStatus{
				ApprovalPendingNodeStatus,
				CanceledNodeStatus,
				CancelingNodeStatus,
				FailedNodeStatus,
				PendingNodeStatus,
				ReadyNodeStatus,
				RunningNodeStatus,
				SkippedNodeStatus,
				SucceededNodeStatus,
				WaitingNodeStatus,
			},
		},
	}

	for _, test := range testCases {
		t.Run(test.name, func(t *testing.T) {
			// Iterate through the statuses.
			for _, initialStatus := range test.initialStatuses {
				pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
					Path:   "pipeline",
					Status: initialStatus,
				})
				stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, initialStatus)

				taskNode := NewTaskNode(&NewTaskNodeInput{
					Path:   "pipeline.stage.s1.task.t1",
					Parent: stageNode,
					Status: &initialStatus,
				})

				actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, initialStatus)
				taskNode.AddActionNode(actionNode)
				stageNode.AddTaskNode(taskNode)

				nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
					Path:   "pipeline.stage.s1.pipeline.p1",
					Status: initialStatus,
					Parent: stageNode,
				})
				stageNode.AddNestedPipelineNode(nestedPipelineNode)

				pipelineNode.AddStageNode(stageNode)

				// Call the function.
				require.NoError(t, stageNode.skipRemainingChildren())

				if test.expectSkipped {
					assert.Equal(t, SkippedNodeStatus, taskNode.Status())
					assert.Equal(t, SkippedNodeStatus, actionNode.Status())
					assert.Equal(t, SkippedNodeStatus, nestedPipelineNode.Status())
					return
				}

				require.Equal(t, initialStatus, taskNode.Status())
				require.Equal(t, initialStatus, actionNode.Status())
				require.Equal(t, initialStatus, nestedPipelineNode.Status())
			}
		})
	}
}

func TestStageNodeHandleChildSkipped(t *testing.T) {
	ptrStatus := func(status NodeStatus) *NodeStatus {
		return &status
	}

	type testCase struct {
		name                string
		currentNodeStatuses map[string]NodeStatus
		expectNodeStatuses  map[string]NodeStatus
	}

	testCases := []testCase{
		{
			name: "nothing should happen since stage is already in final status",
			currentNodeStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": SkippedNodeStatus,
				"pipeline.stage.s1.task.t2": SkippedNodeStatus,
			},
			expectNodeStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": SkippedNodeStatus,
				"pipeline.stage.s1.task.t2": SkippedNodeStatus,
			},
		},
		{
			name: "stage should be set to success if all children are skipped",
			currentNodeStatuses: map[string]NodeStatus{
				"pipeline.stage.s1":         RunningNodeStatus,
				"pipeline.stage.s1.task.t1": SkippedNodeStatus,
				"pipeline.stage.s1.task.t2": SkippedNodeStatus,
			},
			expectNodeStatuses: map[string]NodeStatus{
				"pipeline.stage.s1":         SucceededNodeStatus,
				"pipeline.stage.s1.task.t1": SkippedNodeStatus,
				"pipeline.stage.s1.task.t2": SkippedNodeStatus,
			},
		},
		{
			name: "dependents should be skipped if their dependencies are skipped",
			currentNodeStatuses: map[string]NodeStatus{
				"pipeline.stage.s1":         RunningNodeStatus,
				"pipeline.stage.s1.task.t1": SkippedNodeStatus,
				"pipeline.stage.s1.task.t2": CreatedNodeStatus,
			},
			expectNodeStatuses: map[string]NodeStatus{
				"pipeline.stage.s1":         SucceededNodeStatus,
				"pipeline.stage.s1.task.t1": SkippedNodeStatus,
				"pipeline.stage.s1.task.t2": SkippedNodeStatus,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: RunningNodeStatus,
			})

			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, tc.currentNodeStatuses["pipeline.stage.s1"])

			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: ptrStatus(tc.currentNodeStatuses["pipeline.stage.s1.task.t1"]),
			})

			task2Node := NewTaskNode(&NewTaskNodeInput{
				Path:         "pipeline.stage.s1.task.t2",
				Parent:       stageNode,
				Status:       ptrStatus(tc.currentNodeStatuses["pipeline.stage.s1.task.t2"]),
				Dependencies: []string{"t1"},
			})

			stageNode.AddTaskNode(taskNode)
			stageNode.AddTaskNode(task2Node)
			pipelineNode.AddStageNode(stageNode)

			require.NoError(t, stageNode.handleChildSkipped())

			for path, expectedStatus := range tc.expectNodeStatuses {
				switch path {
				case "pipeline.stage.s1.task.t1":
					assert.Equal(t, expectedStatus, taskNode.Status())
				case "pipeline.stage.s1.task.t2":
					assert.Equal(t, expectedStatus, task2Node.Status())
				case "pipeline.stage.s1":
					assert.Equal(t, expectedStatus, stageNode.Status())
				}
			}
		})
	}
}

func TestStageNodeHandleChildInitializing(t *testing.T) {
	ptrStatus := func(status NodeStatus) *NodeStatus {
		return &status
	}

	type testCase struct {
		name               string
		currentStageStatus NodeStatus
		expectStatuses     map[string]NodeStatus
		children           []Node
	}

	testCases := []testCase{
		{
			name:               "stage should remain unchanged if already in running status",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status: ptrStatus(InitializingNodeStatus),
					Path:   "pipeline.stage.s1.task.t1",
				}),
			},
			expectStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": InitializingNodeStatus,
				"pipeline.stage.s1":         RunningNodeStatus,
			},
		},
		{
			name:               "stage should be set to running if all children are initializing",
			currentStageStatus: FailedNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status: ptrStatus(InitializingNodeStatus),
					Path:   "pipeline.stage.s1.task.t1",
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status: InitializingNodeStatus,
					Path:   "pipeline.stage.s1.pipeline.p1",
				}),
			},
			expectStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     InitializingNodeStatus,
				"pipeline.stage.s1.pipeline.p1": InitializingNodeStatus,
				"pipeline.stage.s1":             RunningNodeStatus,
			},
		},
		{
			name:               "any skipped dependents should be reset to created",
			currentStageStatus: SucceededNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status: ptrStatus(InitializingNodeStatus),
					Path:   "pipeline.stage.s1.task.t1",
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:       ptrStatus(SkippedNodeStatus),
					Path:         "pipeline.stage.s1.task.t2",
					Dependencies: []string{"t1"},
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:       ptrStatus(SkippedNodeStatus),
					Path:         "pipeline.stage.s1.task.t3",
					Dependencies: []string{"t2"},
				}),
			},
			expectStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": InitializingNodeStatus,
				"pipeline.stage.s1.task.t2": CreatedNodeStatus,
				"pipeline.stage.s1.task.t3": CreatedNodeStatus,
				"pipeline.stage.s1":         RunningNodeStatus,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			parentPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.currentStageStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", parentPipelineNode, tc.currentStageStatus)

			for _, child := range tc.children {
				switch child.Type() {
				case TaskNodeType:
					taskNode := child.(*TaskNode)
					taskNode.parent = stageNode
					stageNode.AddTaskNode(taskNode)
				case PipelineNodeType:
					pipelineNode := child.(*PipelineNode)
					pipelineNode.parent = stageNode
					stageNode.AddNestedPipelineNode(pipelineNode)
				default:
					t.Fatalf("unexpected node type: %s", child.Type())
				}
			}

			parentPipelineNode.AddStageNode(stageNode)

			require.NoError(t, stageNode.handleChildInitializing())

			for path, expectedStatus := range tc.expectStatuses {
				switch path {
				case "pipeline.stage.s1":
					assert.Equal(t, expectedStatus, stageNode.Status())
				default:
					for _, child := range stageNode.ChildrenNodes() {
						if child.Path() == path {
							assert.Equal(t, expectedStatus, child.Status())
						}
					}
				}
			}
		})
	}
}

func TestStageNodeHandleChildCanceling(t *testing.T) {
	type testCase struct {
		name                 string
		taskStatus           NodeStatus
		nestedPipelineStatus NodeStatus
		expectedStatusChange bool
	}

	testCases := []testCase{
		{
			name:                 "stage should be set to canceling since all children are in canceling status",
			taskStatus:           CancelingNodeStatus,
			nestedPipelineStatus: CancelingNodeStatus,
			expectedStatusChange: true,
		},
		{
			name:                 "expect no change since not all children are not in canceling status",
			taskStatus:           CancelingNodeStatus,
			nestedPipelineStatus: RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: RunningNodeStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, RunningNodeStatus)

			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: &tc.taskStatus,
			})

			stageNode.AddTaskNode(taskNode)

			nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline.stage.s1.pipeline.p1",
				Status: tc.nestedPipelineStatus,
				Parent: stageNode,
			})
			stageNode.AddNestedPipelineNode(nestedPipelineNode)

			require.NoError(t, stageNode.handleChildCanceling())

			if tc.expectedStatusChange {
				assert.Equal(t, CancelingNodeStatus, stageNode.Status())
			} else {
				assert.Equal(t, RunningNodeStatus, stageNode.Status())
			}
		})
	}
}

func TestStageNodeHandleChildDeferred(t *testing.T) {
	ptrStatus := func(status NodeStatus) *NodeStatus {
		return &status
	}

	type testCase struct {
		name                string
		currentStageStatus  NodeStatus
		expectedStageStatus NodeStatus
		expectChildStatuses map[string]NodeStatus
		children            []Node
	}

	testCases := []testCase{
		{
			name:               "stage should remain unchanged if already in final status",
			currentStageStatus: SkippedNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status: ptrStatus(SkippedNodeStatus),
					Path:   "pipeline.stage.s1.task.t1",
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": SkippedNodeStatus,
			},
			expectedStageStatus: SkippedNodeStatus,
		},
		{
			name:               "stage should be set to succeeded if all children are deferred",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status: ptrStatus(DeferredNodeStatus),
					Path:   "pipeline.stage.s1.task.t1",
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status: DeferredNodeStatus,
					Path:   "pipeline.stage.s1.pipeline.p1",
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     DeferredNodeStatus,
				"pipeline.stage.s1.pipeline.p1": DeferredNodeStatus,
			},
			expectedStageStatus: SucceededNodeStatus,
		},
		{
			name:               "stage should remain unchanged if not all children are deferred",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status: ptrStatus(DeferredNodeStatus),
					Path:   "pipeline.stage.s1.task.t1",
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status: RunningNodeStatus,
					Path:   "pipeline.stage.s1.pipeline.p1",
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     DeferredNodeStatus,
				"pipeline.stage.s1.pipeline.p1": RunningNodeStatus,
			},
			expectedStageStatus: RunningNodeStatus,
		},
		{
			name:               "dependents should be skipped if their dependencies are deferred",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status: ptrStatus(DeferredNodeStatus),
					Path:   "pipeline.stage.s1.task.t1",
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:       ptrStatus(CreatedNodeStatus),
					Path:         "pipeline.stage.s1.task.t2",
					Dependencies: []string{"t1"},
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:       CreatedNodeStatus,
					Path:         "pipeline.stage.s1.pipeline.p1",
					Dependencies: []string{"t1"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     DeferredNodeStatus,
				"pipeline.stage.s1.task.t2":     SkippedNodeStatus,
				"pipeline.stage.s1.pipeline.p1": SkippedNodeStatus,
			},
			expectedStageStatus: SucceededNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			parentPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.currentStageStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", parentPipelineNode, tc.currentStageStatus)

			// Collect and sort child paths
			childPaths := make([]string, 0, len(tc.children))
			for _, child := range tc.children {
				childPaths = append(childPaths, child.Path())
			}
			sort.Strings(childPaths)

			// Create and add children in sorted order
			for _, path := range childPaths {
				for _, child := range tc.children {
					if child.Path() == path {
						switch child.Type() {
						case TaskNodeType:
							taskNode := child.(*TaskNode)
							taskNode.parent = stageNode
							stageNode.AddTaskNode(taskNode)
						case PipelineNodeType:
							pipelineNode := child.(*PipelineNode)
							pipelineNode.parent = stageNode
							stageNode.AddNestedPipelineNode(pipelineNode)
						default:
							t.Fatalf("unexpected node type: %s", child.Type())
						}
					}
				}
			}

			parentPipelineNode.AddStageNode(stageNode)

			require.NoError(t, stageNode.handleChildDeferred())

			require.Equal(t, tc.expectedStageStatus, stageNode.Status())
			require.Len(t, tc.children, len(tc.expectChildStatuses)) // Sanity check to ensure all children are accounted for.

			for _, child := range tc.children {
				expectStatus, ok := tc.expectChildStatuses[child.Path()]
				require.True(t, ok, "child %s not found in expected statuses", child.Path())
				require.Equal(t, expectStatus, child.Status())
			}
		})
	}
}

func TestStageNodeHandleChildUnsuccessfulStatus(t *testing.T) {
	ptrStatus := func(status NodeStatus) *NodeStatus {
		return &status
	}

	type testCase struct {
		expectChildStatuses map[string]NodeStatus
		name                string
		currentStageStatus  NodeStatus
		expectStageStatus   NodeStatus
		children            []Node
	}

	testCases := []testCase{
		{
			name:               "stage should be set to failed since all children are in failed status",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: FailOnError,
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": FailedNodeStatus,
			},
			expectStageStatus: FailedNodeStatus,
		},
		{
			name:               "failures with OnError=continue should be treated as successful",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.a",
					OnError: ContinueOnError,
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.a": FailedNodeStatus,
			},
			expectStageStatus: SucceededNodeStatus,
		},
		{
			name:               "stage status change should not happen since not all children are finished",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: FailOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:  RunningNodeStatus,
					Path:    "pipeline.stage.s1.pipeline.p1",
					OnError: FailOnError,
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     FailedNodeStatus,
				"pipeline.stage.s1.pipeline.p1": RunningNodeStatus,
			},
			expectStageStatus: RunningNodeStatus,
		},
		{
			name:               "expect no change since stage is already in final status",
			currentStageStatus: CanceledNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(CanceledNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: FailOnError,
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": CanceledNodeStatus,
			},
			expectStageStatus: CanceledNodeStatus,
		},
		{
			name:               "failures with OnError=fail should set the stage to failed and skip dependent tasks",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.a",
					OnError: FailOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:       ptrStatus(CreatedNodeStatus),
					Path:         "pipeline.stage.s1.task.c",
					OnError:      FailOnError,
					Dependencies: []string{"a"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.a": FailedNodeStatus,
				"pipeline.stage.s1.task.c": SkippedNodeStatus,
			},
			expectStageStatus: FailedNodeStatus,
		},
		{
			name:               "failures with OnError=continue should be treated as successful and dependent tasks should run",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.a",
					OnError: ContinueOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:       CreatedNodeStatus,
					Path:         "pipeline.stage.s1.pipeline.c",
					OnError:      FailOnError,
					Dependencies: []string{"a"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.a":     FailedNodeStatus,
				"pipeline.stage.s1.pipeline.c": InitializingNodeStatus,
			},
			expectStageStatus: RunningNodeStatus,
		},
		{
			name:               "failures with OnError=fail should take precedence over OnError=continue and skip dependent tasks",
			currentStageStatus: RunningNodeStatus,
			children: []Node{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.a",
					OnError: ContinueOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:  FailedNodeStatus,
					Path:    "pipeline.stage.s1.pipeline.b",
					OnError: FailOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:       ptrStatus(CreatedNodeStatus),
					Path:         "pipeline.stage.s1.task.c",
					OnError:      FailOnError,
					Dependencies: []string{"a", "b"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.a":     FailedNodeStatus,
				"pipeline.stage.s1.pipeline.b": FailedNodeStatus,
				"pipeline.stage.s1.task.c":     SkippedNodeStatus,
			},
			expectStageStatus: FailedNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			parentPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.currentStageStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", parentPipelineNode, tc.currentStageStatus)

			for _, child := range tc.children {
				switch child.Type() {
				case TaskNodeType:
					taskNode := child.(*TaskNode)
					taskNode.parent = stageNode
					stageNode.AddTaskNode(taskNode)
				case PipelineNodeType:
					pipelineNode := child.(*PipelineNode)
					pipelineNode.parent = stageNode
					stageNode.AddNestedPipelineNode(pipelineNode)
				default:
					t.Fatalf("unexpected node type: %s", child.Type())
				}
			}

			parentPipelineNode.AddStageNode(stageNode)

			require.NoError(t, stageNode.handleChildUnsuccessfulStatus())

			require.Equal(t, tc.expectStageStatus, stageNode.Status())
			require.Len(t, tc.children, len(tc.expectChildStatuses)) // Sanity check to ensure all children are accounted for.

			for _, child := range tc.children {
				expectStatus, ok := tc.expectChildStatuses[child.Path()]
				require.True(t, ok, "child %s not found in expected statuses", child.Path())
				require.Equal(t, expectStatus, child.Status())
			}
		})
	}
}

func TestStageNodeHandleChildRunning(t *testing.T) {
	ptrStatus := func(status NodeStatus) *NodeStatus {
		return &status
	}

	type testCase struct {
		expectChildStatuses map[string]NodeStatus
		name                string
		currentStageStatus  NodeStatus
		expectStageStatus   NodeStatus
		children            []StageChildNode
	}

	testCases := []testCase{
		{
			name:               "skipped dependent nodes should be reset back to the created state when a failed dependency is retried",
			currentStageStatus: FailedNodeStatus,
			children: []StageChildNode{
				NewPipelineNode(&NewPipelineNodeInput{
					Status:  RunningNodeStatus,
					Path:    "pipeline.stage.s1.pipeline.p1",
					OnError: FailOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:       ptrStatus(SkippedNodeStatus),
					Path:         "pipeline.stage.s1.task.t1",
					OnError:      ContinueOnError,
					Dependencies: []string{"p1"},
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:       SkippedNodeStatus,
					Path:         "pipeline.stage.s1.pipeline.p2",
					OnError:      FailOnError,
					Dependencies: []string{"p1"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.pipeline.p1": RunningNodeStatus,
				"pipeline.stage.s1.task.t1":     CreatedNodeStatus,
				"pipeline.stage.s1.pipeline.p2": CreatedNodeStatus,
			},
			expectStageStatus: RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			parentPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.currentStageStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", parentPipelineNode, tc.currentStageStatus)

			for _, child := range tc.children {
				switch child.Type() {
				case TaskNodeType:
					taskNode := child.(*TaskNode)
					taskNode.parent = stageNode
					stageNode.AddTaskNode(taskNode)
				case PipelineNodeType:
					pipelineNode := child.(*PipelineNode)
					pipelineNode.parent = stageNode
					stageNode.AddNestedPipelineNode(pipelineNode)
				default:
					t.Fatalf("unexpected node type: %s", child.Type())
				}
			}

			parentPipelineNode.AddStageNode(stageNode)

			require.NoError(t, stageNode.handleChildRunning())

			require.Equal(t, tc.expectStageStatus, stageNode.Status())
			require.Len(t, tc.children, len(tc.expectChildStatuses)) // Sanity check to ensure all children are accounted for.

			for _, child := range tc.children {
				expectStatus, ok := tc.expectChildStatuses[child.Path()]
				require.True(t, ok, "child %s not found in expected statuses", child.Path())
				require.Equal(t, expectStatus, child.Status())
			}
		})
	}
}

func TestStageNodeHandleChildSucceeded(t *testing.T) {
	ptrStatus := func(status NodeStatus) *NodeStatus {
		return &status
	}

	type testCase struct {
		expectChildStatuses map[string]NodeStatus
		name                string
		currentStageStatus  NodeStatus
		expectStageStatus   NodeStatus
		children            []StageChildNode
	}

	testCases := []testCase{
		{
			name:               "stage should be set to succeeded since all children are in succeeded status",
			currentStageStatus: RunningNodeStatus,
			children: []StageChildNode{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(SucceededNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: FailOnError,
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1": SucceededNodeStatus,
			},
			expectStageStatus: SucceededNodeStatus,
		},
		{
			name:               "success should start dependent tasks",
			currentStageStatus: RunningNodeStatus,
			children: []StageChildNode{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(SucceededNodeStatus),
					Path:    "pipeline.stage.s1.task.a",
					OnError: FailOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:       CreatedNodeStatus,
					Path:         "pipeline.stage.s1.pipeline.b",
					OnError:      FailOnError,
					Dependencies: []string{"a"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.a":     SucceededNodeStatus,
				"pipeline.stage.s1.pipeline.b": InitializingNodeStatus,
			},
			expectStageStatus: RunningNodeStatus,
		},
		{
			name:               "expect no change since not all children are finished",
			currentStageStatus: RunningNodeStatus,
			children: []StageChildNode{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(SucceededNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: FailOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:  RunningNodeStatus,
					Path:    "pipeline.stage.s1.pipeline.p1",
					OnError: FailOnError,
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     SucceededNodeStatus,
				"pipeline.stage.s1.pipeline.p1": RunningNodeStatus,
			},
			expectStageStatus: RunningNodeStatus,
		},
		{
			name:               "a failure with OnError=fail should set the stage to failed and skip dependent tasks",
			currentStageStatus: RunningNodeStatus,
			children: []StageChildNode{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: FailOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(SucceededNodeStatus),
					Path:    "pipeline.stage.s1.task.t2",
					OnError: FailOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:       CreatedNodeStatus,
					Path:         "pipeline.stage.s1.pipeline.p1",
					OnError:      FailOnError,
					Dependencies: []string{"t1", "t2"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     FailedNodeStatus,
				"pipeline.stage.s1.task.t2":     SucceededNodeStatus,
				"pipeline.stage.s1.pipeline.p1": SkippedNodeStatus,
			},
			expectStageStatus: FailedNodeStatus,
		},
		{
			name:               "a cancellation with OnError=fail should set the stage to canceled and skip dependent tasks",
			currentStageStatus: RunningNodeStatus,
			children: []StageChildNode{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(CanceledNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: FailOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(SucceededNodeStatus),
					Path:    "pipeline.stage.s1.task.t2",
					OnError: FailOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:       CreatedNodeStatus,
					Path:         "pipeline.stage.s1.pipeline.p1",
					OnError:      FailOnError,
					Dependencies: []string{"t1", "t2"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     CanceledNodeStatus,
				"pipeline.stage.s1.task.t2":     SucceededNodeStatus,
				"pipeline.stage.s1.pipeline.p1": SkippedNodeStatus,
			},
			expectStageStatus: CanceledNodeStatus,
		},
		{
			name:               "failures with OnError=continue should be treated as successful and dependent tasks should run",
			currentStageStatus: RunningNodeStatus,
			children: []StageChildNode{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: ContinueOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(SucceededNodeStatus),
					Path:    "pipeline.stage.s1.task.t2",
					OnError: FailOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:       CreatedNodeStatus,
					Path:         "pipeline.stage.s1.pipeline.p1",
					OnError:      FailOnError,
					Dependencies: []string{"t1", "t2"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     FailedNodeStatus,
				"pipeline.stage.s1.task.t2":     SucceededNodeStatus,
				"pipeline.stage.s1.pipeline.p1": InitializingNodeStatus,
			},
			expectStageStatus: RunningNodeStatus,
		},
		{
			name:               "failures with OnError=fail should take precedence over OnError=continue and skip dependent tasks",
			currentStageStatus: RunningNodeStatus,
			children: []StageChildNode{
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(FailedNodeStatus),
					Path:    "pipeline.stage.s1.task.t1",
					OnError: ContinueOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:  ptrStatus(SucceededNodeStatus),
					Path:    "pipeline.stage.s1.task.t2",
					OnError: FailOnError,
				}),
				NewPipelineNode(&NewPipelineNodeInput{
					Status:  FailedNodeStatus,
					Path:    "pipeline.stage.s1.pipeline.p1",
					OnError: FailOnError,
				}),
				NewTaskNode(&NewTaskNodeInput{
					Status:       ptrStatus(CreatedNodeStatus),
					Path:         "pipeline.stage.s1.task.t3",
					OnError:      FailOnError,
					Dependencies: []string{"t1", "p1"},
				}),
			},
			expectChildStatuses: map[string]NodeStatus{
				"pipeline.stage.s1.task.t1":     FailedNodeStatus,
				"pipeline.stage.s1.task.t2":     SucceededNodeStatus,
				"pipeline.stage.s1.pipeline.p1": FailedNodeStatus,
				"pipeline.stage.s1.task.t3":     SkippedNodeStatus,
			},
			expectStageStatus: FailedNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			parentPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.currentStageStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", parentPipelineNode, tc.currentStageStatus)

			for _, child := range tc.children {
				switch child.Type() {
				case TaskNodeType:
					taskNode := child.(*TaskNode)
					taskNode.parent = stageNode
					stageNode.AddTaskNode(taskNode)
				case PipelineNodeType:
					pipelineNode := child.(*PipelineNode)
					pipelineNode.parent = stageNode
					stageNode.AddNestedPipelineNode(pipelineNode)
				default:
					t.Fatalf("unexpected node type: %s", child.Type())
				}
			}

			parentPipelineNode.AddStageNode(stageNode)

			require.NoError(t, stageNode.handleChildSucceeded())

			require.Equal(t, tc.expectStageStatus, stageNode.Status())
			require.Len(t, tc.children, len(tc.expectChildStatuses)) // Sanity check to ensure all children are accounted for.

			for _, child := range tc.children {
				expectStatus, ok := tc.expectChildStatuses[child.Path()]
				require.True(t, ok, "child %s not found in expected statuses", child.Path())
				require.Equal(t, expectStatus, child.Status())
			}
		})
	}
}

func TestStageNode_setFinalStatusBasedOnChildren(t *testing.T) {
	type testCase struct {
		name                 string
		taskStatus           NodeStatus
		nestedPipelineStatus NodeStatus
		expectedStatus       NodeStatus
	}

	testCases := []testCase{
		{
			name:                 "failed status should take precedence over canceled status",
			taskStatus:           FailedNodeStatus,
			nestedPipelineStatus: CanceledNodeStatus,
			expectedStatus:       FailedNodeStatus,
		},
		{
			name:                 "canceled status should take precedence over succeeded status",
			taskStatus:           CanceledNodeStatus,
			nestedPipelineStatus: SucceededNodeStatus,
			expectedStatus:       CanceledNodeStatus,
		},
		{
			name:                 "no change since nothing is in failed or canceled status",
			taskStatus:           SucceededNodeStatus,
			nestedPipelineStatus: SucceededNodeStatus,
			expectedStatus:       SucceededNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: RunningNodeStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, RunningNodeStatus)

			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: &tc.taskStatus,
			})

			// This task is just to ensure it gets skipped.
			createdStatus := CreatedNodeStatus
			task2Node := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t2",
				Parent: stageNode,
				Status: &createdStatus,
			})

			stageNode.AddTaskNode(taskNode)
			stageNode.AddTaskNode(task2Node)

			nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline.stage.s1.pipeline.p1",
				Status: tc.nestedPipelineStatus,
				Parent: stageNode,
			})
			stageNode.AddNestedPipelineNode(nestedPipelineNode)

			require.NoError(t, stageNode.setFinalStatusBasedOnChildren())
			assert.Equal(t, tc.expectedStatus, stageNode.Status())

			if tc.expectedStatus == FailedNodeStatus || tc.expectedStatus == CanceledNodeStatus {
				// Ensure remaining children are skipped.
				assert.Equal(t, SkippedNodeStatus, task2Node.Status())
			}
		})
	}
}

func TestStageNodeEventListeners(t *testing.T) {
	// Test stage event listeners to make sure parent node is updated accordingly and
	// sibling stages are updated as well.

	type testCase struct {
		name                     string
		newStatus                NodeStatus
		siblingStageStatus       NodeStatus
		expectSiblingStageStatus NodeStatus
		currentPipelineStatus    NodeStatus
		expectPipelineStatus     NodeStatus
		expectErrorCode          errors.CodeType
	}

	testCases := []testCase{
		{
			name:                     "pipeline should become running when stage becomes running and all skipped sibling stages should reset to created",
			newStatus:                RunningNodeStatus,
			siblingStageStatus:       SkippedNodeStatus,
			currentPipelineStatus:    FailedNodeStatus,
			expectSiblingStageStatus: CreatedNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                  "pipeline should become canceling when all stages are canceling",
			currentPipelineStatus: RunningNodeStatus,
			newStatus:             CancelingNodeStatus,
			expectPipelineStatus:  CancelingNodeStatus,
		},
		{
			name:                     "pipeline status should not change when stage becomes canceling and there are other stages are not in canceling status",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                CancelingNodeStatus,
			siblingStageStatus:       RunningNodeStatus,
			expectSiblingStageStatus: RunningNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline should become failed when stage becomes failed and sibling stages have not started",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       CreatedNodeStatus,
			expectSiblingStageStatus: SkippedNodeStatus,
			expectPipelineStatus:     FailedNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes failed and sibling stages are not in final status",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       RunningNodeStatus,
			expectSiblingStageStatus: RunningNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "failed status should take precedence over canceled status",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       CanceledNodeStatus,
			expectSiblingStageStatus: CanceledNodeStatus,
			expectPipelineStatus:     FailedNodeStatus,
		},
		{
			name:                     "failed should take precedence over succeeded status",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                SucceededNodeStatus,
			siblingStageStatus:       FailedNodeStatus,
			expectSiblingStageStatus: FailedNodeStatus,
			expectPipelineStatus:     FailedNodeStatus,
		},
		{
			name:                     "canceled should take precedence over succeeded status",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                CanceledNodeStatus,
			siblingStageStatus:       SucceededNodeStatus,
			expectSiblingStageStatus: SucceededNodeStatus,
			expectPipelineStatus:     CanceledNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes failed and other stages are running",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       RunningNodeStatus,
			expectSiblingStageStatus: RunningNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes failed and other stages are ready",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       ReadyNodeStatus,
			expectSiblingStageStatus: ReadyNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes failed and other stages are ready",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       ReadyNodeStatus,
			expectSiblingStageStatus: ReadyNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes canceled and other stages are running",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       RunningNodeStatus,
			expectSiblingStageStatus: RunningNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes canceled and other stages are ready",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       ReadyNodeStatus,
			expectSiblingStageStatus: ReadyNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes canceled and other stages are ready",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                FailedNodeStatus,
			siblingStageStatus:       ReadyNodeStatus,
			expectSiblingStageStatus: ReadyNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                  "pipeline status should remain unchanged if it is already in a final status",
			currentPipelineStatus: CanceledNodeStatus,
			newStatus:             FailedNodeStatus,
			expectPipelineStatus:  CanceledNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes succeeded and other stages are running",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                SucceededNodeStatus,
			siblingStageStatus:       RunningNodeStatus,
			expectSiblingStageStatus: RunningNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes succeeded and other stages are not in final status",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                SucceededNodeStatus,
			siblingStageStatus:       ReadyNodeStatus,
			expectSiblingStageStatus: ReadyNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged when stage becomes succeeded and next stage should be set to running",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                SucceededNodeStatus,
			siblingStageStatus:       CreatedNodeStatus,
			expectSiblingStageStatus: RunningNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
		{
			name:                  "pipeline should be set to succeeded when all stages are succeeded",
			currentPipelineStatus: RunningNodeStatus,
			newStatus:             SucceededNodeStatus,
			expectPipelineStatus:  SucceededNodeStatus,
		},
		{
			name:                  "pipeline should be set to succeeded when all stages are deferred",
			currentPipelineStatus: CreatedNodeStatus,
			newStatus:             DeferredNodeStatus,
			expectPipelineStatus:  SucceededNodeStatus,
		},
		{
			name:                     "pipeline status should remain unchanged if a stage is deferred and another stage needs to be set to running",
			currentPipelineStatus:    RunningNodeStatus,
			newStatus:                DeferredNodeStatus,
			siblingStageStatus:       CreatedNodeStatus,
			expectSiblingStageStatus: RunningNodeStatus,
			expectPipelineStatus:     RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.currentPipelineStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, tc.currentPipelineStatus)
			pipelineNode.AddStageNode(stageNode)

			var siblingStageNode *StageNode
			if tc.siblingStageStatus != "" {
				siblingStageNode = NewStageNode("pipeline.stage.s2", pipelineNode, tc.siblingStageStatus)
				pipelineNode.AddStageNode(siblingStageNode)
			}

			// Initialize the pipeline node, which will register the event listeners.
			pipelineNode.init()

			// Set the stage node status.
			err := stageNode.SetStatus(tc.newStatus)

			if tc.expectErrorCode != "" {
				assert.Equal(t, tc.expectErrorCode, errors.ErrorCode(err))
				return
			}

			require.NoError(t, err)

			// Ensure the pipeline node status is expected.
			assert.Equal(t, tc.expectPipelineStatus, pipelineNode.Status())

			// Ensure the sibling stage status is expected.
			if siblingStageNode != nil {
				assert.Equal(t, tc.expectSiblingStageStatus, siblingStageNode.Status())
			}
		})
	}
}
