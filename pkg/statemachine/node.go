package statemachine

import "strings"

// NodeStatus constant defines the possible states for a node in a pipeline
type NodeStatus string

// NodeStatusConstants
const (
	CreatedNodeStatus         NodeStatus = "CREATED"
	ApprovalPendingNodeStatus NodeStatus = "APPROVAL_PENDING"
	WaitingNodeStatus         NodeStatus = "WAITING"
	ReadyNodeStatus           NodeStatus = "READY"
	PendingNodeStatus         NodeStatus = "PENDING"
	RunningNodeStatus         NodeStatus = "RUNNING"
	FailedNodeStatus          NodeStatus = "FAILED"
	SkippedNodeStatus         NodeStatus = "SKIPPED"
	SucceededNodeStatus       NodeStatus = "SUCCEEDED"
	CanceledNodeStatus        NodeStatus = "CANCELED"
	CancelingNodeStatus       NodeStatus = "CANCELING"
	InitializingNodeStatus    NodeStatus = "INITIALIZING"
	DeferredNodeStatus        NodeStatus = "DEFERRED"
)

// IsFinalStatus returns true if the status is a final status
func (n NodeStatus) IsFinalStatus() bool {
	return n == FailedNodeStatus || n == SkippedNodeStatus || n == SucceededNodeStatus || n == CanceledNodeStatus || n == DeferredNodeStatus
}

// IsRetryableStatus returns true if status allows a node to be retried
func (n NodeStatus) IsRetryableStatus() bool {
	return n == FailedNodeStatus || n == CanceledNodeStatus || n == SucceededNodeStatus
}

// IsSkippableStatus returns true is the status is skippable.
func (n NodeStatus) IsSkippableStatus() bool {
	return n == CreatedNodeStatus || n == InitializingNodeStatus
}

// IsDeferrableStatus returns true if the status is deferrable.
func (n NodeStatus) IsDeferrableStatus() bool {
	return n == ReadyNodeStatus || n == WaitingNodeStatus
}

// Equals returns true if the status is equal to the given status
func (n NodeStatus) Equals(s NodeStatus) bool {
	return n == s
}

// NodeType constant defines the types of pipeline graph nodes
type NodeType string

// NodeType constants
const (
	PipelineNodeType NodeType = "PIPELINE"
	StageNodeType    NodeType = "STAGE"
	TaskNodeType     NodeType = "TASK"
	ActionNodeType   NodeType = "ACTION"
)

// String returns the string representation of the node type
func (n NodeType) String() string {
	return string(n)
}

// OnErrorStrategy represents the behavior of a node when an error occurs
type OnErrorStrategy string

// NodeOnError constants
const (
	ContinueOnError OnErrorStrategy = "CONTINUE"
	FailOnError     OnErrorStrategy = "FAIL"
)

// Equals returns true if the error behavior is equal to the given error behavior
func (n OnErrorStrategy) Equals(s OnErrorStrategy) bool {
	return n == s
}

// NodeStatusChange represents a change in the status of a node
type NodeStatusChange struct {
	OldStatus NodeStatus
	NewStatus NodeStatus
	NodePath  string
	NodeType  NodeType
}

type listenerFunc func() error

// Node represents a node in the pipeline graph
type Node interface {
	Name() string
	Path() string
	Type() NodeType
	Status() NodeStatus
	Cancel() error
	GetStatusChanges() []NodeStatusChange
	SetStatus(status NodeStatus) error
	Parent() Node
	ChildrenNodes() []Node
	GetInitialStatus() NodeStatus
	Accept(v Visitor, traversalCtx *TraversalContext) error
	registerListener(status NodeStatus, l listenerFunc)
}

type nodeImpl struct {
	parent        Node
	listeners     map[NodeStatus]listenerFunc
	path          string
	typ           NodeType
	status        NodeStatus
	statusChanges []NodeStatusChange
}

// StageChildNode represents a child of a stage node, such as a task or nested pipeline.
type StageChildNode interface {
	Node
	HasUnfinishedDependencies() bool
	GetDependencies() []string
	GetOnErrorStrategy() OnErrorStrategy
}

func (n *nodeImpl) Name() string {
	if n.parent == nil {
		// Root node
		return n.path
	}

	// Last part of the path is the name
	return n.path[strings.LastIndex(n.path, ".")+1:]
}

func (n *nodeImpl) Path() string {
	return n.path
}

func (n *nodeImpl) Type() NodeType {
	return n.typ
}

func (n *nodeImpl) Parent() Node {
	return n.parent
}

func (n *nodeImpl) Status() NodeStatus {
	return n.status
}

func (n *nodeImpl) SetStatus(status NodeStatus) error {
	if n.status.Equals(status) {
		// No change in status
		return nil
	}

	n.statusChanges = append(n.statusChanges, NodeStatusChange{
		OldStatus: n.status,
		NewStatus: status,
		NodePath:  n.path,
		NodeType:  n.typ,
	})

	n.status = status

	return n.fireEvent(status)
}

func (n *nodeImpl) GetStatusChanges() []NodeStatusChange {
	return n.statusChanges
}

func (n *nodeImpl) GetDependencies() []string {
	return []string{}
}

func (n *nodeImpl) GetInitialStatus() NodeStatus {
	return ReadyNodeStatus
}

func (n *nodeImpl) Cancel() error {
	if n.Status().IsFinalStatus() {
		return nil
	}

	var status NodeStatus
	switch n.status {
	case CreatedNodeStatus,
		InitializingNodeStatus:
		// Nodes with the created or initializing status transition to skipped
		// since they were never started
		status = SkippedNodeStatus
	case ReadyNodeStatus,
		CancelingNodeStatus,
		ApprovalPendingNodeStatus,
		WaitingNodeStatus:
		// These statuses can all transition directly to canceled
		// because they have not been started
		status = CanceledNodeStatus
	default:
		// Running and pending nodes transition to canceling since
		// the cancellation process is asynchronous
		status = CancelingNodeStatus
	}

	return n.SetStatus(status)
}

func (n *nodeImpl) registerListener(status NodeStatus, l listenerFunc) {
	if n.listeners == nil {
		// Initialize listeners map
		n.listeners = map[NodeStatus]listenerFunc{}
	}

	n.listeners[status] = l
}

func (n *nodeImpl) fireEvent(newStatus NodeStatus) error {
	if listener, ok := n.listeners[newStatus]; ok {
		return listener()
	}

	return nil
}
