package statemachine

import (
	"slices"
)

var _ StageChildNode = (*PipelineNode)(nil)

// NewPipelineNodeInput is the input for creating a new pipeline node
type NewPipelineNodeInput struct {
	Parent       Node
	OnError      OnErrorStrategy
	Status       NodeStatus
	Path         string
	Dependencies []string
}

// PipelineNode represents a pipeline node
type PipelineNode struct {
	onError OnErrorStrategy
	nodeImpl
	stages       []*StageNode
	dependencies []string
}

// NewPipelineNode creates a new pipeline node
func NewPipelineNode(input *NewPipelineNodeInput) *PipelineNode {
	return &PipelineNode{
		nodeImpl: nodeImpl{
			parent:        input.Parent,
			path:          input.Path,
			typ:           PipelineNodeType,
			status:        input.Status,
			statusChanges: []NodeStatusChange{},
		},
		stages:       []*StageNode{},
		dependencies: input.Dependencies,
		onError:      input.OnError,
	}
}

// Copy returns a copy of the node
func (n *PipelineNode) Copy() *PipelineNode {
	node := &PipelineNode{
		nodeImpl: nodeImpl{
			parent:        n.parent,
			path:          n.path,
			typ:           n.typ,
			status:        n.status,
			statusChanges: n.statusChanges,
		},
		stages:       []*StageNode{},
		dependencies: n.dependencies,
		onError:      n.onError,
	}
	for _, stage := range n.stages {
		node.AddStageNode(stage.Copy())
	}
	return node
}

// SetStatus sets the node status
func (n *PipelineNode) SetStatus(status NodeStatus) error {
	if err := n.nodeImpl.SetStatus(status); err != nil {
		return err
	}

	switch status {
	case RunningNodeStatus:
		// Set first child to ready
		if len(n.stages) > 0 {
			if n.stages[0].status.Equals(CreatedNodeStatus) {
				if err := n.stages[0].SetStatus(RunningNodeStatus); err != nil {
					return err
				}
			}
		}
	case DeferredNodeStatus, SkippedNodeStatus, FailedNodeStatus:
		// Set all children to skipped
		if err := n.skipRemainingChildren(); err != nil {
			return err
		}
	case ReadyNodeStatus, CreatedNodeStatus, InitializingNodeStatus, ApprovalPendingNodeStatus:
		// Set all children to created
		for _, s := range n.stages {
			if err := s.SetStatus(CreatedNodeStatus); err != nil {
				return err
			}
		}
	}

	return nil
}

// AddStageNode adds a new stage node to the pipeline
func (n *PipelineNode) AddStageNode(s *StageNode) {
	s.init()
	n.stages = append(n.stages, s)
}

// GetDependencies returns the dependencies for this node
func (n *PipelineNode) GetDependencies() []string {
	return n.dependencies
}

// GetInitialStatus returns the initial status of the node
func (n *PipelineNode) GetInitialStatus() NodeStatus {
	return InitializingNodeStatus
}

func (n *PipelineNode) init() {
	for _, stage := range n.stages {
		// Stage running
		stage.registerListener(RunningNodeStatus, n.handleStageRunning)
		// Stage failed
		stage.registerListener(FailedNodeStatus, n.handleStageFailed)
		// Stage deferred
		stage.registerListener(DeferredNodeStatus, n.handleStageSucceeded)
		// Stage canceled
		stage.registerListener(CanceledNodeStatus, n.handleStageCanceled)
		// Stage canceling
		stage.registerListener(CancelingNodeStatus, n.handleStageCanceling)
		// Stage succeeded
		stage.registerListener(SucceededNodeStatus, n.handleStageSucceeded)
	}
}

// ChildrenNodes returns a list of this node's children
func (n *PipelineNode) ChildrenNodes() []Node {
	nodes := []Node{}

	for _, s := range n.stages {
		nodes = append(nodes, s)
	}

	return nodes
}

// Accept a visitor
func (n *PipelineNode) Accept(v Visitor, traversalCtx *TraversalContext) error {
	if err := v.VisitForAny(n, traversalCtx); err != nil {
		return err
	}
	return v.VisitForPipeline(n, traversalCtx)
}

// Cancel cancels the node
func (n *PipelineNode) Cancel() error {
	if err := n.nodeImpl.Cancel(); err != nil {
		return err
	}

	// Cancel all stages
	for _, s := range n.stages {
		if err := s.Cancel(); err != nil {
			return err
		}
	}

	return nil
}

// GetOnErrorStrategy returns the on error behavior.
func (n *PipelineNode) GetOnErrorStrategy() OnErrorStrategy {
	return n.onError
}

// HasUnfinishedDependencies returns true if the node has unfinished dependencies
func (n *PipelineNode) HasUnfinishedDependencies() bool {
	if n.parent == nil || len(n.dependencies) == 0 {
		return false
	}

	for _, child := range n.parent.ChildrenNodes() {
		if child.Path() == n.path {
			// Skip the pipeline itself.
			continue
		}

		if !slices.Contains(n.dependencies, child.Name()) {
			// Dependency not found, skip.
			continue
		}

		if !child.Status().IsFinalStatus() {
			// One of the dependencies is not finished.
			return true
		}
	}

	return false
}

/* Helper functions */

// skipRemainingChildren skips all remaining children
func (n *PipelineNode) skipRemainingChildren() error {
	for _, s := range n.stages {
		if s.status.IsSkippableStatus() {
			if err := s.SetStatus(SkippedNodeStatus); err != nil {
				return err
			}
		}
	}

	return nil
}

/* Stage event listeners */

// handleStageRunning handles a stage running event
func (n *PipelineNode) handleStageRunning() error {
	if n.status.Equals(RunningNodeStatus) {
		// Pipeline is already running.
		return nil
	}

	// Reset any skipped stages in case of a retry
	for _, s := range n.stages {
		if s.status.Equals(SkippedNodeStatus) {
			if err := s.SetStatus(CreatedNodeStatus); err != nil {
				return err
			}
		}
	}

	return n.SetStatus(RunningNodeStatus)
}

// handleStageCanceling handles a stage canceling event
func (n *PipelineNode) handleStageCanceling() error {
	for _, child := range n.stages {
		if !child.status.IsFinalStatus() && !child.status.Equals(CancelingNodeStatus) {
			// Not all children are canceling or complete.
			return nil
		}
	}

	return n.SetStatus(CancelingNodeStatus)
}

// handleStageFailed handles a stage failure event
func (n *PipelineNode) handleStageFailed() error {
	return n.evaluateChildrenForUnsuccessfulStatus()
}

// handleStageCanceled handles a stage canceled event
func (n *PipelineNode) handleStageCanceled() error {
	return n.evaluateChildrenForUnsuccessfulStatus()
}

// handleStageSucceeded handles a stage succeeded event
func (n *PipelineNode) handleStageSucceeded() error {
	if err := n.evaluateChildrenForUnsuccessfulStatus(); err != nil {
		return err
	}

	if n.status.IsFinalStatus() {
		// Pipeline was unsuccessful
		return nil
	}

	for _, s := range n.stages {
		if s.status.Equals(CreatedNodeStatus) {
			// Set next stage to running
			return s.SetStatus(RunningNodeStatus)
		}

		if !s.status.IsFinalStatus() {
			// A stage is not complete yet.
			return nil
		}
	}

	return n.SetStatus(SucceededNodeStatus)
}

// evaluateChildrenForUnsuccessfulStatus evaluates the children for an unsuccessful status.
func (n *PipelineNode) evaluateChildrenForUnsuccessfulStatus() error {
	if n.status.IsFinalStatus() {
		// Pipeline is already in a final state (likely canceled pipeline directly).
		return nil
	}

	var hasFailedChild, hasCanceledChild = false, false

	// Check all children for an unsuccessful status.
	for _, c := range n.stages {
		switch c.status {
		case FailedNodeStatus:
			hasFailedChild = true
		case CanceledNodeStatus:
			hasCanceledChild = true
		case ReadyNodeStatus,
			RunningNodeStatus:
			// A stage is still running or expected to run.
			return nil
		}
	}

	// Failed status > Canceled status.
	switch {
	case hasFailedChild:
		if err := n.SetStatus(FailedNodeStatus); err != nil {
			return err
		}
	case hasCanceledChild:
		if err := n.skipRemainingChildren(); err != nil {
			return err
		}

		if err := n.SetStatus(CanceledNodeStatus); err != nil {
			return err
		}
	}

	return nil
}
