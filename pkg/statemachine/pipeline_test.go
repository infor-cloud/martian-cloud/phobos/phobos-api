package statemachine

import (
	"sort"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewPipelineNode(t *testing.T) {
	expected := &PipelineNode{
		nodeImpl: nodeImpl{
			parent:        nil,
			path:          "pipeline",
			typ:           PipelineNodeType,
			status:        CreatedNodeStatus,
			statusChanges: []NodeStatusChange{},
		},
		stages:       []*StageNode{},
		dependencies: []string{"t1"},
		onError:      FailOnError,
	}

	assert.Equal(t, expected, NewPipelineNode(&NewPipelineNodeInput{
		Path:         "pipeline",
		Status:       CreatedNodeStatus,
		OnError:      FailOnError,
		Dependencies: []string{"t1"},
	}))
}

func TestPipelineNodeCopy(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:         "pipeline",
		Status:       CreatedNodeStatus,
		OnError:      FailOnError,
		Dependencies: []string{"t1"},
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)
	pipelineNode.AddStageNode(stageNode)

	copied := pipelineNode.Copy()

	assert.Equal(t, pipelineNode, copied)
	assert.NotSame(t, pipelineNode, copied)
}

func TestPipelineNodeAddStageNode(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)
	pipelineNode.AddStageNode(stageNode)

	assert.Equal(t, []*StageNode{stageNode}, pipelineNode.stages)
}

func TestPipelineNodeChildrenNodes(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)
	pipelineNode.AddStageNode(stageNode)

	assert.Equal(t, []Node{stageNode}, pipelineNode.ChildrenNodes())
}

func TestPipelineNodeAccept(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)
	pipelineNode.AddStageNode(stageNode)

	visitor := &PartialVisitor{}
	traversalContext := &TraversalContext{}

	require.NoError(t, pipelineNode.Accept(visitor, traversalContext))
}

func TestPipelineNodePath(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	assert.Equal(t, "pipeline", pipelineNode.Path())
}

func TestPipelineNodeStatus(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	assert.Equal(t, CreatedNodeStatus, pipelineNode.Status())
}

func TestPipelineNodeParent(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})
	assert.Nil(t, pipelineNode.Parent())

	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, CreatedNodeStatus)
	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline.stage.s1.pipeline.p1",
		Status: CreatedNodeStatus,
		Parent: stageNode,
	})

	stageNode.AddNestedPipelineNode(nestedPipelineNode)
	pipelineNode.AddStageNode(stageNode)

	assert.Equal(t, stageNode, nestedPipelineNode.Parent())
}

func TestPipelineNodeGetInitialStatus(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})

	assert.Equal(t, InitializingNodeStatus, pipelineNode.GetInitialStatus())
}

func TestPipelineGetDependencies(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Dependencies: []string{"t2"},
	})

	assert.Equal(t, pipelineNode.GetDependencies(), []string{"t2"})
}

func TestPipelineGetStatusChanges(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: CreatedNodeStatus,
	})

	// Arbitrary status changes.
	require.NoError(t, pipelineNode.SetStatus(ReadyNodeStatus))
	require.NoError(t, pipelineNode.SetStatus(CanceledNodeStatus))

	expect := []NodeStatusChange{
		{
			OldStatus: createdStatus,
			NewStatus: ReadyNodeStatus,
			NodePath:  pipelineNode.path,
			NodeType:  pipelineNode.typ,
		},
		{
			OldStatus: ReadyNodeStatus,
			NewStatus: CanceledNodeStatus,
			NodePath:  pipelineNode.path,
			NodeType:  pipelineNode.typ,
		},
	}

	assert.Equal(t, expect, pipelineNode.GetStatusChanges())
}

func TestPipelineNodeSetStatusRunning(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	stage2Node := NewStageNode("pipeline.stage.s2", pipelineNode, createdStatus)
	pipelineNode.AddStageNode(stageNode)
	pipelineNode.AddStageNode(stage2Node)

	require.NoError(t, pipelineNode.SetStatus(RunningNodeStatus))
	assert.Equal(t, RunningNodeStatus, pipelineNode.Status())
	assert.Equal(t, RunningNodeStatus, stageNode.Status())
	assert.Equal(t, CreatedNodeStatus, stage2Node.Status()) // Stage2 should not be changed
}
func TestPipelineNodeSetStatusSkipped(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	pipelineNode.AddStageNode(stageNode)

	require.NoError(t, pipelineNode.SetStatus(SkippedNodeStatus))
	assert.Equal(t, SkippedNodeStatus, pipelineNode.Status())
	assert.Equal(t, SkippedNodeStatus, stageNode.Status())
}

func TestPipelineNodeSetStatusFailed(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	pipelineNode.AddStageNode(stageNode)

	require.NoError(t, pipelineNode.SetStatus(FailedNodeStatus))
	assert.Equal(t, FailedNodeStatus, pipelineNode.Status())
	assert.Equal(t, SkippedNodeStatus, stageNode.Status())
}

func TestPipelineNodeSetStatusReset(t *testing.T) {
	// Created or Approval Pending aka, pipeline is being reset

	canceledStatus := CanceledNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: canceledStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, canceledStatus)
	pipelineNode.AddStageNode(stageNode)

	require.NoError(t, pipelineNode.SetStatus(CreatedNodeStatus))
	assert.Equal(t, CreatedNodeStatus, pipelineNode.Status())
	assert.Equal(t, CreatedNodeStatus, stageNode.Status())

	require.NoError(t, pipelineNode.SetStatus(ApprovalPendingNodeStatus))
	assert.Equal(t, ApprovalPendingNodeStatus, pipelineNode.Status())
	assert.Equal(t, CreatedNodeStatus, stageNode.Status())
}

func TestPipelineNodeSetStatusDeferred(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)
	stage2Node := NewStageNode("pipeline.stage.s2", pipelineNode, createdStatus)
	pipelineNode.AddStageNode(stageNode)
	pipelineNode.AddStageNode(stage2Node)

	require.NoError(t, pipelineNode.SetStatus(DeferredNodeStatus))
	assert.Equal(t, DeferredNodeStatus, pipelineNode.Status())
	assert.Equal(t, SkippedNodeStatus, stageNode.Status())
	assert.Equal(t, SkippedNodeStatus, stage2Node.Status())
}

func TestPipelineCancel(t *testing.T) {
	type testCase struct {
		name     string
		status   NodeStatus
		expected NodeStatus
	}

	testCases := []testCase{
		{
			name:     "created should return skipped",
			status:   CreatedNodeStatus,
			expected: SkippedNodeStatus,
		},
		{
			name:     "ready should return canceled",
			status:   ReadyNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "running should return canceling",
			status:   RunningNodeStatus,
			expected: CancelingNodeStatus,
		},
		{
			name:     "canceling should return canceled",
			status:   CancelingNodeStatus,
			expected: CanceledNodeStatus,
		},
		{
			name:     "final status should not be changed",
			status:   SucceededNodeStatus,
			expected: SucceededNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.status,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, tc.status)

			taskNode := NewTaskNode(&NewTaskNodeInput{
				Path:   "pipeline.stage.s1.task.t1",
				Parent: stageNode,
				Status: &tc.status,
			})

			actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, CreatedNodeStatus)
			taskNode.AddActionNode(actionNode)
			stageNode.AddTaskNode(taskNode)

			nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline.stage.s1.pipeline.p1",
				Status: tc.status,
				Parent: stageNode,
			})
			stageNode.AddNestedPipelineNode(nestedPipelineNode)
			pipelineNode.AddStageNode(stageNode)

			require.NoError(t, pipelineNode.Cancel())
			assert.Equal(t, tc.expected, stageNode.Status())
			assert.Equal(t, tc.expected, taskNode.Status())
			assert.Equal(t, SkippedNodeStatus, actionNode.Status())
			assert.Equal(t, tc.expected, nestedPipelineNode.Status())
		})
	}
}

func TestPipelineNodeGetOnError(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		OnError: FailOnError,
	})

	assert.Equal(t, FailOnError, pipelineNode.GetOnErrorStrategy())
}

func TestPipelineNodeSkipRemainingChildren(t *testing.T) {
	createdStatus := CreatedNodeStatus

	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: createdStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, createdStatus)

	taskNode := NewTaskNode(&NewTaskNodeInput{
		Path:   "pipeline.stage.s1.task.t1",
		Parent: stageNode,
		Status: &createdStatus,
	})

	actionNode := NewActionNode("pipeline.stage.s1.task.t1.action.a1", taskNode, createdStatus)
	taskNode.AddActionNode(actionNode)
	stageNode.AddTaskNode(taskNode)

	nestedPipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline.stage.s1.pipeline.p1",
		Status: createdStatus,
		Parent: stageNode,
	})
	stageNode.AddNestedPipelineNode(nestedPipelineNode)

	pipelineNode.AddStageNode(stageNode)

	require.NoError(t, pipelineNode.skipRemainingChildren())
	assert.Equal(t, SkippedNodeStatus, stageNode.Status())
	assert.Equal(t, SkippedNodeStatus, taskNode.Status())
	assert.Equal(t, SkippedNodeStatus, actionNode.Status())
	assert.Equal(t, SkippedNodeStatus, nestedPipelineNode.Status())
}

func TestPipelineHandleStageRunning(t *testing.T) {
	pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
		Path:   "pipeline",
		Status: FailedNodeStatus,
	})
	stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, RunningNodeStatus)
	stage2Node := NewStageNode("pipeline.stage.s2", pipelineNode, SkippedNodeStatus)
	pipelineNode.AddStageNode(stageNode)
	pipelineNode.AddStageNode(stage2Node)

	require.NoError(t, pipelineNode.handleStageRunning())
	assert.Equal(t, RunningNodeStatus, stageNode.Status())
	assert.Equal(t, CreatedNodeStatus, stage2Node.Status()) // Skipped stage should be reset to created
}

func TestPipelineNodeHandleStageCanceling(t *testing.T) {
	type testCase struct {
		name                 string
		stage1status         NodeStatus
		stage2status         NodeStatus
		expectedStatusChange bool
	}

	testCases := []testCase{
		{
			name:                 "pipeline should be set to canceling since all children are in canceling status",
			stage1status:         CancelingNodeStatus,
			stage2status:         CancelingNodeStatus,
			expectedStatusChange: true,
		},
		{
			name:         "expect no change since not all children are not in canceling status",
			stage1status: CancelingNodeStatus,
			stage2status: RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: RunningNodeStatus,
			})
			stage1Node := NewStageNode("pipeline.stage.s1", pipelineNode, tc.stage1status)
			stage2Node := NewStageNode("pipeline.stage.s2", pipelineNode, tc.stage2status)

			pipelineNode.AddStageNode(stage1Node)
			pipelineNode.AddStageNode(stage2Node)

			require.NoError(t, pipelineNode.handleStageCanceling())

			if tc.expectedStatusChange {
				assert.Equal(t, CancelingNodeStatus, pipelineNode.Status())
			} else {
				assert.Equal(t, RunningNodeStatus, pipelineNode.Status())
			}
		})
	}
}

func TestPipelineNodeHandleStageDeferred(t *testing.T) {
	type testCase struct {
		name                    string
		targetStagePath         string
		statuses                map[string]NodeStatus
		targetStageExpectStatus NodeStatus
		expectPipelineStatus    NodeStatus
	}

	testCases := []testCase{
		{
			name:            "should set the next stage to running when a stage is deferred",
			targetStagePath: "pipeline.stage.s2",
			statuses: map[string]NodeStatus{
				"pipeline":          RunningNodeStatus,
				"pipeline.stage.s1": DeferredNodeStatus,
				"pipeline.stage.s2": CreatedNodeStatus,
			},
			targetStageExpectStatus: RunningNodeStatus,
			expectPipelineStatus:    RunningNodeStatus,
		},
		{
			name:            "pipeline status should remain unchanged if a stage is still running",
			targetStagePath: "pipeline.stage.s2",
			statuses: map[string]NodeStatus{
				"pipeline":          RunningNodeStatus,
				"pipeline.stage.s1": RunningNodeStatus,
				"pipeline.stage.s2": DeferredNodeStatus,
			},
			targetStageExpectStatus: DeferredNodeStatus,
			expectPipelineStatus:    RunningNodeStatus,
		},
		{
			name:            "pipeline status should reflect the status of other stages if not all stages are deferred",
			targetStagePath: "pipeline.stage.s2",
			statuses: map[string]NodeStatus{
				"pipeline":          RunningNodeStatus,
				"pipeline.stage.s1": SucceededNodeStatus,
				"pipeline.stage.s2": DeferredNodeStatus,
			},
			targetStageExpectStatus: DeferredNodeStatus,
			expectPipelineStatus:    SucceededNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: tc.statuses["pipeline"],
			})

			// Collect and sort stage paths
			stagePaths := make([]string, 0, len(tc.statuses))
			for path := range tc.statuses {
				if strings.Contains(path, "stage") {
					stagePaths = append(stagePaths, path)
				}
			}
			sort.Strings(stagePaths)

			// Create and add stages in sorted order
			for _, path := range stagePaths {
				status := tc.statuses[path]
				stageNode := NewStageNode(path, pipelineNode, status)
				pipelineNode.AddStageNode(stageNode)
			}

			// A deferred stage should be treated same as a successful stage, hence we're calling that handler here.
			require.NoError(t, pipelineNode.handleStageSucceeded())
			assert.Equal(t, tc.expectPipelineStatus, pipelineNode.Status())

			for _, stage := range pipelineNode.stages {
				if stage.Path() == tc.targetStagePath {
					assert.Equal(t, tc.targetStageExpectStatus, stage.Status())
				}
			}
		})
	}
}

func TestPipelineNodeHandleStageSucceeded(t *testing.T) {
	type testCase struct {
		name                string
		childStatus         NodeStatus
		child2Status        NodeStatus
		expectStatus        NodeStatus
		allChildrenComplete bool
	}

	testCases := []testCase{
		{
			name:                "pipeline should be set to succeeded since all children are in succeeded status",
			childStatus:         SucceededNodeStatus,
			child2Status:        SucceededNodeStatus,
			expectStatus:        SucceededNodeStatus,
			allChildrenComplete: true,
		},
		{
			name:         "expect no change since not all children are not in succeeded status",
			childStatus:  RunningNodeStatus,
			child2Status: SucceededNodeStatus,
		},
		{
			name:                "a failed child should set the pipeline to failed",
			childStatus:         FailedNodeStatus,
			child2Status:        CanceledNodeStatus,
			expectStatus:        FailedNodeStatus,
			allChildrenComplete: true,
		},
		{
			name:                "a canceled child should set the pipeline to canceled",
			childStatus:         CanceledNodeStatus,
			child2Status:        SucceededNodeStatus,
			expectStatus:        CanceledNodeStatus,
			allChildrenComplete: true,
		},
		{
			name:         "a child succeeded and next child should be set to running",
			childStatus:  SucceededNodeStatus,
			child2Status: CreatedNodeStatus,
			expectStatus: RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: RunningNodeStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, tc.childStatus)
			stage2Node := NewStageNode("pipeline.stage.s2", pipelineNode, tc.child2Status)
			pipelineNode.AddStageNode(stageNode)
			pipelineNode.AddStageNode(stage2Node)

			require.NoError(t, pipelineNode.handleStageSucceeded())

			if tc.allChildrenComplete {
				assert.Equal(t, tc.expectStatus, pipelineNode.Status())
			} else {
				assert.Equal(t, RunningNodeStatus, pipelineNode.Status())

				if tc.child2Status == CreatedNodeStatus && tc.childStatus == SucceededNodeStatus {
					// Expect the next child to be set to ready if the previous child is succeeded
					assert.Equal(t, RunningNodeStatus, stage2Node.Status())
				}
			}
		})
	}
}

func TestPipelineNodeEvaluateChildrenForUnsuccessfulStatus(t *testing.T) {
	type testCase struct {
		name           string
		childStatus    NodeStatus
		child2Status   NodeStatus
		expectedStatus NodeStatus
	}

	testCases := []testCase{
		{
			name:           "failed status should take precedence over canceled status",
			childStatus:    FailedNodeStatus,
			child2Status:   CanceledNodeStatus,
			expectedStatus: FailedNodeStatus,
		},
		{
			name:           "canceled status should take precedence over succeeded status",
			childStatus:    CanceledNodeStatus,
			child2Status:   SucceededNodeStatus,
			expectedStatus: CanceledNodeStatus,
		},
		{
			name:           "no change since nothing is in failed or canceled status",
			childStatus:    SucceededNodeStatus,
			child2Status:   SucceededNodeStatus,
			expectedStatus: RunningNodeStatus,
		},
		{
			name:           "no change since a stage is still running",
			childStatus:    RunningNodeStatus,
			child2Status:   SucceededNodeStatus,
			expectedStatus: RunningNodeStatus,
		},
		{
			name:           "no change still stage is ready",
			childStatus:    ReadyNodeStatus,
			child2Status:   SucceededNodeStatus,
			expectedStatus: RunningNodeStatus,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			pipelineNode := NewPipelineNode(&NewPipelineNodeInput{
				Path:   "pipeline",
				Status: RunningNodeStatus,
			})
			stageNode := NewStageNode("pipeline.stage.s1", pipelineNode, tc.childStatus)
			stage2Node := NewStageNode("pipeline.stage.s2", pipelineNode, tc.child2Status)
			stage3Node := NewStageNode("pipeline.stage.s3", pipelineNode, CreatedNodeStatus) // Should be skipped if pipeline is failed or canceled
			pipelineNode.AddStageNode(stageNode)
			pipelineNode.AddStageNode(stage2Node)
			pipelineNode.AddStageNode(stage3Node)

			require.NoError(t, pipelineNode.evaluateChildrenForUnsuccessfulStatus())
			assert.Equal(t, tc.expectedStatus, pipelineNode.Status())

			if tc.expectedStatus == FailedNodeStatus || tc.expectedStatus == CanceledNodeStatus {
				// Ensure remaining children are skipped.
				assert.Equal(t, SkippedNodeStatus, stage3Node.Status())
			}
		})
	}
}
