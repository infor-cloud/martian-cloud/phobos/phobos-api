package plugin

import (
	"context"
	"errors"
	"io"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/zclconf/go-cty/cty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	hclctx "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/context"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component/mocks"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
)

// testActionResponse is a sample action response struct for the test pipelinePlugin
type testActionResponse struct {
	ExitCode int
}

// Outputs implements the component.ActionResponse interface.
func (r *testActionResponse) Outputs() map[string]cty.Value {
	return map[string]cty.Value{
		"exit_code": cty.NumberIntVal(int64(r.ExitCode)),
	}
}

func TestNewPipelinePlugin(t *testing.T) {
	mockInstance := mocks.NewPipelinePlugin(t)
	log := hclog.NewNullLogger()

	expected := &PipelinePlugin{
		instance: mockInstance,
		log:      log,
		// Leave close nil as function equality assertion does not work
	}

	assert.Equal(t, expected, NewPipelinePlugin(mockInstance, log, nil))
}

func TestPipelinePluginClose(t *testing.T) {
	called := false
	pipelinePlugin := &PipelinePlugin{
		close: func() {
			called = true
		},
	}

	pipelinePlugin.Close()
	assert.True(t, called)
}

func TestPipelinePluginGracefulShutdown(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	mockPipelinePluginInstance := mocks.NewPipelinePlugin(t)

	mockPipelinePluginInstance.On("Shutdown", mock.Anything).Return(nil)

	pipelinePlugin := &PipelinePlugin{
		instance: mockPipelinePluginInstance,
		log:      hclog.NewNullLogger(),
		close:    func() {},
	}

	assert.NoError(t, pipelinePlugin.GracefulShutdown(ctx))
}

func TestPipelinePluginConfigure(t *testing.T) {
	sampleConfig := `
variable "port" {
  type = string
}

plugin "test" {
  port    = var.port
  retries = 3
}`
	type testCase struct {
		name           string
		configError    error
		configSetError error
		expectError    string
	}

	testCases := []testCase{
		{
			name: "success",
		},
		{
			name:        "config error",
			configError: errors.New("config error"),
			expectError: "config error",
		},
		{
			name:           "config set error",
			configSetError: errors.New("config set error"),
			expectError:    "config set error",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelinePluginInstance := mocks.NewPipelinePlugin(t)

			mockPipelinePluginInstance.On("Config", mock.Anything).Return(&struct {
				Port    string `hcl:"port"`
				Retries int    `hcl:"retries"`
			}{}, tc.configError)

			if tc.configError == nil {
				mockPipelinePluginInstance.On("ConfigSet", mock.Anything, mock.Anything).Return(tc.configSetError)
			}

			pipelinePlugin := &PipelinePlugin{
				instance: mockPipelinePluginInstance,
				log:      hclog.NewNullLogger(),
				close:    func() {},
			}

			type hclTemplate struct {
				PipelinePlugin *config.Plugin     `hcl:"plugin,block"`
				Variables      []*config.Variable `hcl:"variable,block"`
			}

			// Use hclsimple to decode the config into a struct.
			var template hclTemplate
			require.NoError(t, hclsimple.Decode("pipelinePlugin.hcl", []byte(sampleConfig), nil, &template))

			// Create an eval context with a variable value to ensure that the
			// variable is correctly resolved.
			evalCtx := hclctx.New(map[string]cty.Value{
				"var": cty.ObjectVal(map[string]cty.Value{
					"port": cty.StringVal("8080"),
				}),
			})

			diags := pipelinePlugin.Configure(ctx, template.PipelinePlugin.Body, evalCtx.Context())

			if tc.expectError != "" {
				assert.ErrorContains(t, diags, tc.expectError)
			} else {
				assert.Empty(t, diags)
			}
		})
	}
}

func TestPipelinePluginExecuteAction(t *testing.T) {
	type testCase struct {
		name           string
		expectExitCode int
	}

	testCases := []testCase{
		{
			name:           "success",
			expectExitCode: 0,
		},
		{
			name:           "failure",
			expectExitCode: 1,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelinePluginInstance := mocks.NewPipelinePlugin(t)

			actionContext := hclctx.New(map[string]cty.Value{}).NewActionContext(map[string]map[string]cty.Value{})

			mockActionFunc := func(
				ctx context.Context,
				ui terminal.UI,
				logger hclog.Logger,
			) (*testActionResponse, error) {
				// Ensure inputs are passed correctly.
				require.NotNil(t, ctx)
				require.NotNil(t, ui)
				require.NotNil(t, logger)

				return &testActionResponse{ExitCode: tc.expectExitCode}, nil
			}

			sampleAction := `
			action "test_command" {
			 command = "echo hello world"
			}`

			type hclTemplate struct {
				Action *config.PipelineAction `hcl:"action,block"`
			}

			// Use hclsimple to decode the config into a struct.
			var template hclTemplate
			require.NoError(t, hclsimple.Decode("action.hcl", []byte(sampleAction), nil, &template))

			mockPipelinePluginInstance.On("ExecuteAction", mock.Anything, &component.PipelinePluginActionInput{
				ActionName: template.Action.PluginActionName(),
				InputData:  template.Action.Body,
			}, actionContext.Context()).Return(mockActionFunc)

			pipelinePlugin := &PipelinePlugin{
				instance: mockPipelinePluginInstance,
				log:      hclog.NewNullLogger(),
				close:    func() {},
			}

			ui := terminal.NonInteractiveUI(ctx, io.Discard)

			outputs, err := pipelinePlugin.ExecuteAction(ctx, template.Action.PluginActionName(), template.Action.Body, actionContext.Context(), ui)
			require.NoError(t, err)

			// Check that the exit code is returned as an output.
			assert.Len(t, outputs, 1)
			assert.Equal(t, cty.NumberIntVal(int64(tc.expectExitCode)), outputs["exit_code"])
		})
	}
}

func TestPipelinePluginValidateConfig(t *testing.T) {
	type testCase struct {
		name        string
		config      string
		expectError string
	}

	testCases := []testCase{
		{
			name: "valid config",
			config: `
			variable "port" {
				type = string
			}

			plugin "test" {
				port    = var.port
				retries = 3
				hosts {
					host "local" {
						ip = "localhost"
					}
				}
			}`,
		},
		{
			name: "config missing variable block",
			config: `
			plugin "test" {
				port    = var.port
				retries = 3
			}`,
			expectError: "Unsupported attribute; This object does not have an attribute named \"port\".",
		},
		{
			name: "pipelinePlugin block missing required field",
			config: `
			plugin "test" {
				retries = 3
			}`,
			expectError: "argument \"port\" is required, but no definition was found",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelinePluginInstance := mocks.NewPipelinePlugin(t)

			mockPipelinePluginInstance.On("Config", mock.Anything).Return(&struct {
				Port       string `hcl:"port"`
				Retries    int    `hcl:"retries"`
				HostsBlock *struct {
					Hosts []*struct {
						Name string `hcl:"name,label"`
						IP   string `hcl:"ip,attr"`
					} `hcl:"host,block"`
				} `hcl:"hosts,block"`
				OptionalBlock *struct{} `hcl:"optional_block,block"`
			}{}, nil)

			pipelinePlugin := &PipelinePlugin{
				instance: mockPipelinePluginInstance,
				log:      hclog.NewNullLogger(),
				close:    func() {},
			}

			type hclTemplate struct {
				PipelinePlugin *config.Plugin     `hcl:"plugin,block"`
				Variables      []*config.Variable `hcl:"variable,block"`
			}

			// Use hclsimple to decode the config into a struct.
			var template hclTemplate
			require.NoError(t, hclsimple.Decode("plugin.hcl", []byte(tc.config), nil, &template))

			variables := map[string]cty.Value{"var": cty.ObjectVal(map[string]cty.Value{})}

			// Add a variable to the eval context if the template has variables.
			if len(template.Variables) > 0 {
				variables = map[string]cty.Value{
					"var": cty.ObjectVal(map[string]cty.Value{
						"port": cty.StringVal("8080"),
					}),
				}
			}

			// Create an eval context with a variable value to ensure that the
			// variable is correctly resolved.
			evalCtx := hclctx.New(variables)

			diags := pipelinePlugin.ValidateConfig(ctx, template.PipelinePlugin.Body, evalCtx.Context())

			if tc.expectError != "" {
				assert.ErrorContains(t, diags, tc.expectError)
			} else {
				assert.Empty(t, diags)
			}
		})
	}
}

func TestPipelinePluginGetImpliedActionOutputs(t *testing.T) {
	actionName := "action_1"

	type testCase struct {
		outputs     interface{}
		expectError error
		name        string
	}

	testCases := []testCase{
		{
			name: "successfully get outputs",
			outputs: &testActionResponse{
				ExitCode: 0,
			},
		},
		{
			name:        "error getting outputs",
			expectError: errors.New("error getting outputs"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelinePluginInstance := mocks.NewPipelinePlugin(t)

			mockPipelinePluginInstance.On("ActionOutput", mock.Anything, actionName).Return(tc.outputs, tc.expectError)

			pipelinePlugin := &PipelinePlugin{
				instance: mockPipelinePluginInstance,
				log:      hclog.NewNullLogger(),
				close:    func() {},
			}

			outputs, err := pipelinePlugin.GetImpliedActionOutputs(ctx, actionName)

			if tc.expectError != nil {
				assert.Equal(t, tc.expectError, err)
				return
			}

			require.NoError(t, err)
			require.NotEmpty(t, outputs)
			assert.Equal(t, cty.UnknownVal(cty.Number), outputs["exit_code"])
		})
	}
}

func TestPipelinePluginValidateActionInput(t *testing.T) {
	actionName := "action_1"

	type testCase struct {
		name                  string
		actionHCL             string
		actionInputStruct     interface{}
		variables             cty.Value
		previousActionOutputs map[string]cty.Value
		expectError           string
	}

	testCases := []testCase{
		{
			name: "valid action input",
			actionHCL: `
			action "test_command" {
			 command       = "echo hello world"
			 http_method   = "GET"
			 https_enabled = true
			}`,
			actionInputStruct: &struct {
				Command      string `hcl:"command,attr"`
				HTTPMethod   string `hcl:"http_method,attr"`
				HTTPSEnabled bool   `hcl:"https_enabled,attr"`
			}{},
		},
		{
			name: "valid action input with variables",
			actionHCL: `
			action "test_command" {
			 command       = var.command
			 http_method   = var.http_method
			}`,
			actionInputStruct: &struct {
				Command    string `hcl:"command,attr"`
				HTTPMethod string `hcl:"http_method,attr"`
			}{},
			variables: cty.ObjectVal(map[string]cty.Value{
				"command":     cty.StringVal("echo hello world"),
				"http_method": cty.StringVal("GET"),
			}),
		},
		{
			name: "valid action input with optional field",
			actionHCL: `
			action "test_command" {
			 command       = "echo hello world"
			}`,
			actionInputStruct: &struct {
				HTTPSEnabled *bool  `hcl:"https_enabled,optional"`
				Command      string `hcl:"command,attr"`
			}{},
		},
		{
			name: "primitive type gets converted to string",
			actionHCL: `
			action "test_command" {
			 command       = "echo hello world"
			 http_method   = 1
			}`,
			actionInputStruct: &struct {
				Command    string `hcl:"command,attr"`
				HTTPMethod string `hcl:"http_method,attr"`
			}{},
		},
		{
			name: "empty input struct",
			actionHCL: `
			action "test_command" {}`,
			actionInputStruct: &struct{}{},
		},
		{
			name: "action input struct is nil",
			actionHCL: `
			action "test_command" {}`,
		},
		{
			name: "action input with previous action outputs",
			actionHCL: `
			action "test_command" {
			 command              = "echo hello world"
			 previous_exit_code   = action_outputs.stage.s1.task.t1.action.a1.exit_code
			}`,
			actionInputStruct: &struct {
				Command          string `hcl:"command,attr"`
				PreviousExitCode int    `hcl:"previous_exit_code,attr"`
			}{},
			previousActionOutputs: map[string]cty.Value{
				"exit_code": cty.NumberIntVal(0),
			},
		},
		{
			name: "action input field provided but not required",
			actionHCL: `
			action "test_command" {
			 command       = "echo hello world"
			}`,
			actionInputStruct: &struct{}{},
			expectError:       "An argument named \"command\" is not expected here.",
		},
		{
			name: "incorrect value type",
			actionHCL: `
			action "test_command" {
			 command       = "echo hello world"
			 https_enabled = 1
			}`,
			actionInputStruct: &struct {
				Command      string `hcl:"command,attr"`
				HTTPSEnabled bool   `hcl:"https_enabled,attr"`
			}{},
			expectError: "Inappropriate value for attribute \"https_enabled\": bool required.",
		},
		{
			name: "action input is missing required field",
			actionHCL: `
			action "test_command" {
			 command       = "echo hello world"
			}`,
			actionInputStruct: &struct {
				Command    string `hcl:"command"`
				HTTPMethod string `hcl:"http_method"`
			}{},
			expectError: "The argument \"http_method\" is required, but no definition was found.",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			mockPipelinePluginInstance := mocks.NewPipelinePlugin(t)

			mockPipelinePluginInstance.On("ActionInput", mock.Anything, actionName).Return(tc.actionInputStruct, nil)

			pipelinePlugin := &PipelinePlugin{
				instance: mockPipelinePluginInstance,
				log:      hclog.NewNullLogger(),
				close:    func() {},
			}

			type hclTemplate struct {
				Action *config.PipelineAction `hcl:"action,block"`
			}

			// Use hclsimple to decode the config into a struct.
			var template hclTemplate
			require.NoError(t, hclsimple.Decode("action.hcl", []byte(tc.actionHCL), nil, &template))

			evalContext := hclctx.New(map[string]cty.Value{
				"var": tc.variables,
			}).NewActionContext(map[string]map[string]cty.Value{
				"pipeline.stage.s1.task.t1.action.a1": tc.previousActionOutputs,
			})

			diags := pipelinePlugin.ValidateActionInput(ctx, actionName, template.Action.Body, evalContext.Context())

			if tc.expectError != "" {
				assert.ErrorContains(t, diags, tc.expectError)
			} else {
				assert.Empty(t, diags)
			}
		})
	}
}
