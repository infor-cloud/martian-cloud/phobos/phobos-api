package plugin

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"slices"
	"sort"
	"strings"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/hashicorp/go-version"
	svchost "github.com/hashicorp/terraform-svchost"
	"github.com/mitchellh/go-homedir"
)

const (
	// serviceDiscoveryEndpoint is the endpoint for the plugin registry discovery.
	serviceDiscoveryEndpoint = "/.well-known/phobos.json"
)

var (
	// supportedProtocols is a map of supported protocol
	// versions for Phobos plugins indicated as MAJOR.MINOR.
	supportedProtocols = map[string]struct{}{
		"1.0": {},
	}
)

// RegistryPluginPlatform represents a platform for a plugin version
type RegistryPluginPlatform struct {
	OperatingSystem string `json:"os"`
	Arch            string `json:"arch"`
}

// RegistryPluginVersion represents a plugin version
type RegistryPluginVersion struct {
	Version   string                   `json:"version"`
	Protocols []string                 `json:"protocols"`
	Platforms []RegistryPluginPlatform `json:"platforms"`
}

// RegistryPluginVersionList contains a list of plugin versions
type RegistryPluginVersionList struct {
	Versions []RegistryPluginVersion `json:"versions"`
}

// RegistryPluginDownloadResponse is the response for downloading a plugin.
type RegistryPluginDownloadResponse struct {
	OperatingSystem string   `json:"os"`
	Arch            string   `json:"arch"`
	Filename        string   `json:"filename"`
	DownloadURL     string   `json:"download_url"`
	SHASumsURL      string   `json:"shasums_url"`
	SHASum          string   `json:"shasum"`
	Protocols       []string `json:"protocols"`
}

// Source defines the plugin Source with three parts (hostname, organization, name).
// The hostname is optional and defaults to the default registry URL.
type Source struct {
	Hostname     svchost.Hostname
	Organization string
	Name         string
}

// String returns the string representation of the source.
func (s *Source) String() string {
	return fmt.Sprintf("%s/%s/%s", s.Hostname, s.Organization, s.Name)
}

// installerOptions are the options for configuring the installer.
type installerOptions struct {
	constraints *string
	token       *string
}

// InstallerOption is a function that configures the installer.
type InstallerOption func(*installerOptions)

// WithConstraints sets the version constraints for the installer.
// Only applicable for EnsureLatestVersion.
func WithConstraints(constraints *string) InstallerOption {
	return func(o *installerOptions) {
		o.constraints = constraints
	}
}

// WithToken sets the token for the installer.
func WithToken(token *string) InstallerOption {
	return func(o *installerOptions) {
		o.token = token
	}
}

// Installer is the interface for installing plugins
type Installer interface {
	EnsureLatestVersion(ctx context.Context, source string, opts ...InstallerOption) (string, error)
	InstallPlugin(ctx context.Context, source, version string, opts ...InstallerOption) error
}

// installer installs plugins.
type installer struct {
	logger             hclog.Logger
	homeDirFunc        func() (string, error) // For overriding in tests.
	client             *retryablehttp.Client
	defaultRegistryURL *url.URL
}

// NewInstaller returns a new plugin installer.
func NewInstaller(logger hclog.Logger, defaultRegistryURL string) (Installer, error) {
	// Use a retryable client to handle transient network errors.
	retryableClient := retryablehttp.NewClient()
	retryableClient.Logger = nil // Disable noise from logger.

	registryURL, err := url.Parse(defaultRegistryURL)
	if err != nil {
		return nil, fmt.Errorf("failed to parse default registry URL: %w", err)
	}

	return &installer{
		logger:             logger,
		homeDirFunc:        homedir.Dir,
		defaultRegistryURL: registryURL,
		client:             retryableClient,
	}, nil
}

// EnsureLatestVersion ensures the latest plugin version is available meeting the constraints (if any).
func (i *installer) EnsureLatestVersion(ctx context.Context, source string, opts ...InstallerOption) (string, error) {
	options := &installerOptions{}
	for _, o := range opts {
		o(options)
	}

	pluginSource, err := ParseSource(source, ptr.String(i.defaultRegistryURL.String()))
	if err != nil {
		return "", fmt.Errorf("failed to parse plugin source: %w", err)
	}

	i.logger.Debug("parsed plugin", "hostname", pluginSource.Hostname, "organization", pluginSource.Organization, "name", pluginSource.Name)

	// Parse the constraints.
	var requirements version.Constraints
	if options.constraints != nil {
		requirements, err = version.NewConstraint(*options.constraints)
		if err != nil {
			return "", fmt.Errorf("failed to parse version constraint: %w", err)
		}
	}

	registryURL, err := i.resolveServiceURL(ctx, pluginSource.Hostname.String(), "plugins.v1")
	if err != nil {
		return "", fmt.Errorf("failed to discover plugin registry: %w", err)
	}

	versionsURL, err := url.Parse(path.Join(pluginSource.Organization, pluginSource.Name, "versions"))
	if err != nil {
		return "", fmt.Errorf("failed to parse versions URL: %w", err)
	}

	listVersionURL := registryURL.ResolveReference(versionsURL).String()

	i.logger.Debug("resolved versions URL", "url", listVersionURL)

	response, err := i.get(ctx, listVersionURL, options.token)
	if err != nil {
		return "", fmt.Errorf("failed to get plugin versions: %w", err)
	}
	defer response.Body.Close()

	var versions RegistryPluginVersionList
	if err := json.NewDecoder(response.Body).Decode(&versions); err != nil {
		return "", fmt.Errorf("failed to decode version list response: %w", err)
	}

	i.logger.Debug("plugin versions", "versions", len(versions.Versions))

	parsedVersions := make(version.Collection, 0)
	for _, v := range versions.Versions {
		ver, err := version.NewVersion(v.Version)
		if err != nil {
			// Skip invalid versions.
			continue
		}

		if requirements != nil && !requirements.Check(ver) {
			// Skip versions that don't meet the constraints.
			continue
		}

		if !slices.Contains(v.Platforms, RegistryPluginPlatform{OperatingSystem: runtime.GOOS, Arch: runtime.GOARCH}) {
			// Skip versions that don't support the current platform.
			continue
		}

		if err := AreProtocolsSupported(v.Protocols); err != nil {
			// Skip versions that don't support the current protocols.
			continue
		}

		parsedVersions = append(parsedVersions, ver)
	}

	if parsedVersions.Len() == 0 {
		return "", fmt.Errorf("no suitable versions found for plugin %q", pluginSource)
	}

	// Sort the versions.
	sort.Stable(parsedVersions)

	// Return the newest version available since no constraints were provided.
	return parsedVersions[len(parsedVersions)-1].String(), nil
}

// InstallPlugin downloads and installs the specified plugin binary.
func (i *installer) InstallPlugin(ctx context.Context, source, version string, opts ...InstallerOption) error {
	options := &installerOptions{}
	for _, o := range opts {
		o(options)
	}

	provider, err := ParseSource(source, ptr.String(i.defaultRegistryURL.String()))
	if err != nil {
		return fmt.Errorf("failed to parse plugin source: %w", err)
	}

	prettyProvider := provider.String()

	i.logger.Debug("parsed plugin", "hostname", provider.Hostname, "organization", provider.Organization, "name", provider.Name)

	homedir, err := i.homeDirFunc()
	if err != nil {
		return fmt.Errorf("failed to get home directory: %w", err)
	}

	// Plugin directory is created in the following format:
	// ~/.phobos.d/plugins/<api-hostname>/<source>/<version>/<os>_<arch>
	pluginDirectory := filepath.Join(
		homedir,
		".phobos.d",
		"plugins",
		prettyProvider,
		version,
		runtime.GOOS+"_"+runtime.GOARCH,
	)

	i.logger.Debug("resolved install directory", "directory", pluginDirectory)

	// The binary name is in the format: "phobos-plugin-<plugin-name>"
	binaryName := getBinaryName("phobos-plugin-" + provider.Name)
	binaryPath := filepath.Join(pluginDirectory, binaryName)

	if _, err = os.Stat(binaryPath); err == nil {
		// Plugin is already installed.
		i.logger.Info("plugin already installed", "source", prettyProvider, "version", version, "os", runtime.GOOS, "arch", runtime.GOARCH)
		return nil
	}

	registryURL, err := i.resolveServiceURL(ctx, provider.Hostname.String(), "plugins.v1")
	if err != nil {
		return fmt.Errorf("failed to discover plugin registry: %w", err)
	}

	versionURL, err := url.Parse(path.Join(provider.Organization, provider.Name, version, "download", runtime.GOOS, runtime.GOARCH))
	if err != nil {
		return fmt.Errorf("failed to parse download URL: %w", err)
	}

	downloadURL := registryURL.ResolveReference(versionURL).String()

	i.logger.Debug("resolved download URL", "url", downloadURL)

	resp, err := i.get(ctx, downloadURL, options.token)
	if err != nil {
		return fmt.Errorf("failed to get plugin download information: %w", err)
	}
	defer resp.Body.Close()

	var platform RegistryPluginDownloadResponse
	if err = json.NewDecoder(resp.Body).Decode(&platform); err != nil {
		return fmt.Errorf("failed to decode response: %w", err)
	}

	i.logger.Debug("plugin platform", "os", platform.OperatingSystem, "arch", platform.Arch, "filename", platform.Filename)

	// Ensure the plugin platform supports the current protocols.
	if err = AreProtocolsSupported(platform.Protocols); err != nil {
		return err
	}

	checksumResp, err := i.get(ctx, platform.SHASumsURL, nil)
	if err != nil {
		return fmt.Errorf("failed to download plugin checksums: %w", err)
	}
	defer checksumResp.Body.Close()

	i.logger.Debug("downloaded plugin checksums", "source", prettyProvider, "version", version, "os", runtime.GOOS, "arch", runtime.GOARCH)

	checksumMap, err := fileMapFromChecksums(checksumResp.Body)
	if err != nil {
		return fmt.Errorf("failed to read checksums: %w", err)
	}

	checksum, ok := checksumMap[platform.Filename]
	if !ok {
		return fmt.Errorf("checksum not found for file %s", platform.Filename)
	}

	binaryResp, err := i.get(ctx, platform.DownloadURL, nil)
	if err != nil {
		return fmt.Errorf("failed to download plugin: %w", err)
	}
	defer binaryResp.Body.Close()

	i.logger.Debug("downloaded plugin binary", "source", prettyProvider, "version", version, "os", runtime.GOOS, "arch", runtime.GOARCH)

	// Check the content type of the plugin response since we expect a zip archive.
	contentType := binaryResp.Header.Get("Content-Type")
	if !slices.Contains(zipMimeTypes, contentType) {
		return fmt.Errorf("unexpected content type %s when attempting to download plugin zip file, supported types: %s", contentType, strings.Join(zipMimeTypes, ", "))
	}

	var (
		packageReader io.Reader
		buffer        bytes.Buffer
	)

	reader := io.TeeReader(binaryResp.Body, &buffer)
	packageReader = &buffer

	if err = compareChecksum(reader, checksum, binaryResp.ContentLength); err != nil {
		return fmt.Errorf("failed to verify plugin checksum: %w", err)
	}

	if err = os.MkdirAll(pluginDirectory, 0700); err != nil {
		return fmt.Errorf("failed to create plugin directory: %w", err)
	}

	// Unzip and only copy the binary.
	if err = unzip(packageReader, pluginDirectory, platform.Filename, &binaryName); err != nil {
		return fmt.Errorf("failed to unzip plugin: %w", err)
	}

	i.logger.Info("installed plugin", "source", prettyProvider, "version", version, "os", runtime.GOOS, "arch", runtime.GOARCH)

	// Make the binary an executable. Gives full permissions to owner.
	return os.Chmod(binaryPath, 0700) // nosemgrep: gosec.G304-1, gosec.G302-1
}

// AreProtocolsSupported checks if the given protocols are supported.
func AreProtocolsSupported(protocols []string) error {
	if len(protocols) == 0 {
		return fmt.Errorf("no protocols found; must be one of %v", supportedProtocols)
	}

	for _, protocol := range protocols {
		if _, ok := supportedProtocols[protocol]; !ok {
			return fmt.Errorf("unsupported protocol version %q; must be one of %v", protocol, supportedProtocols)
		}
	}

	return nil
}

// resolveServiceURL discovers the specified service URL.
func (i *installer) resolveServiceURL(ctx context.Context, hostname, serviceID string) (*url.URL, error) {
	discoveryURL := &url.URL{
		Scheme: "https",
		Host:   hostname,
		Path:   serviceDiscoveryEndpoint,
	}

	if hostname == i.defaultRegistryURL.Hostname() {
		// Use the default registry URL if the hostname matches.
		discoveryURL = i.defaultRegistryURL
		discoveryURL.Path = serviceDiscoveryEndpoint
	}

	// Get the service discovery document.
	discoverResponse, err := i.get(ctx, discoveryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	var result map[string]any
	if err = json.NewDecoder(discoverResponse.Body).Decode(&result); err != nil {
		return nil, fmt.Errorf("failed to decode discovery response: %w", err)
	}

	serviceEndpoint, ok := result[serviceID].(string)
	if !ok {
		return nil, fmt.Errorf("service %q not found in discovery document", serviceID)
	}

	// Parse the registry URL.
	registryURL, err := url.Parse(serviceEndpoint)
	if err != nil {
		return nil, fmt.Errorf("failed to parse registry URL: %w", err)
	}

	if registryURL.Scheme == "" {
		// Use the discovery URL if the registry URL is relative.
		discoveryURL.Path = serviceEndpoint
		registryURL = discoveryURL
	}

	i.logger.Debug("resolved service url", serviceID, registryURL.String())

	return registryURL, nil
}

// get performs a GET request to the given endpoint and returns the response
// after checking for errors.
func (i *installer) get(ctx context.Context, endpoint string, token *string) (*http.Response, error) {
	endpointURL, err := url.ParseRequestURI(endpoint)
	if err != nil {
		return nil, fmt.Errorf("failed to parse URL: %w", err)
	}

	r, err := retryablehttp.NewRequestWithContext(ctx, http.MethodGet, endpointURL.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create request: %w", err)
	}

	if token != nil {
		// Add the token to the request header.
		r.Header.Add("Authorization", "Bearer "+*token)
	}

	resp, err := i.client.Do(r)
	if err != nil {
		return nil, fmt.Errorf("failed to perform request: %w", err)
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close() // Avoid leaking resources.
		return nil, fmt.Errorf("endpoint returned %s status", resp.Status)
	}

	return resp, nil
}

// ParseSource parses the plugin source and returns the provider.
func ParseSource(s string, defaultRegistryURL *string) (*Source, error) {
	switch strings.Count(s, "/") {
	case 1:
		// If the source has one slash, we assume the default registry hostname.
		if defaultRegistryURL == nil {
			return nil, fmt.Errorf("default registry hostname is required when using the shorthand source format")
		}

		registryURL, err := url.Parse(*defaultRegistryURL)
		if err != nil {
			return nil, fmt.Errorf("failed to parse default registry URL: %w", err)
		}

		// Prepend the default registry hostname to the source.
		s = fmt.Sprintf("%s/%s", registryURL.Hostname(), s)
	case 2:
		// If the source has two slashes, we assume the source is fully qualified.
	default:
		return nil, fmt.Errorf("invalid plugin source: %q; must be in format <organization>/<name> or <hostname>/<organization>/<name>", s)
	}

	parts := strings.Split(s, "/")

	hostname, err := svchost.ForComparison(parts[0])
	if err != nil {
		return nil, fmt.Errorf("invalid hostname: %w", err)
	}

	return &Source{
		Hostname:     hostname,
		Organization: parts[1],
		Name:         parts[2],
	}, nil
}

// sanitizeArchivePath returns the sanitized path of the file to be extracted.
func sanitizedArchivePath(destination, filePath string) (string, error) {
	destPath := filepath.Join(destination, filePath)
	if !strings.HasPrefix(destPath, filepath.Clean(destination)+string(os.PathSeparator)) {
		return "", errors.New(filePath + ": illegal file path")
	}
	return destPath, nil
}

// getBinaryName returns the Plugin executable name.
func getBinaryName(pluginName string) string {
	if runtime.GOOS == "windows" {
		return pluginName + ".exe"
	}

	return pluginName
}
