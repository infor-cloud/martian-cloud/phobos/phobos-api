package plugin

import (
	"archive/zip"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/aws/smithy-go/ptr"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	pluginsV1Endpoint        = "/v1/plugin-registry/plugins/"
	defaultDiscoveryEndpoint = "http://localhost/.well-known/phobos.json"
)

// roundTripFunc implements the RoundTripper interface.
type roundTripFunc func(r *http.Request) *http.Response

// RoundTrip executes a single HTTP transaction, returning
// a Response for the provided Request.
func (f roundTripFunc) RoundTrip(r *http.Request) (*http.Response, error) {
	return f(r), nil
}

// newTestClient returns *http.Client with Transport replaced to avoid making real calls.
func newTestClient(fn roundTripFunc) *http.Client {
	return &http.Client{
		Transport: roundTripFunc(fn),
	}
}

func TestNewInstaller(t *testing.T) {
	instance, err := NewInstaller(hclog.NewNullLogger(), "http://localhost")
	assert.NoError(t, err)
	assert.NotNil(t, instance)
}

// fakeHTTPResponse is a fake HTTP response for testing.
type fakeHTTPResponse struct {
	body        any
	contentType string
	statusCode  int
}

func TestEnsureLatestVersion(t *testing.T) {
	shorthandSource := "neutron/gitlab"
	defaultVersionsEndpoint := "http://localhost/v1/plugin-registry/plugins/neutron/gitlab/versions"
	token := "token"

	type testCase struct {
		constraints   *string
		source        string
		responses     map[string]fakeHTTPResponse
		name          string
		expectVersion string
		expectError   string
	}

	tests := []testCase{
		{
			name:   "registry returns single version",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.0",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
						},
					},
				},
			},
			expectVersion: "1.0.0",
		},
		{
			name:   "registry returns multiple versions",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.0",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
							{
								Version:   "1.0.1",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
						},
					},
				},
			},
			expectVersion: "1.0.1",
		},
		{
			name:   "custom registry returns single version",
			source: "some-registry.tld/neutron/gitlab",
			responses: map[string]fakeHTTPResponse{
				"https://some-registry.tld/.well-known/phobos.json": {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": "/plugins-registry/v1/",
					},
				},
				"https://some-registry.tld/plugins-registry/v1/neutron/gitlab/versions": {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.0",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
						},
					},
				},
			},
			expectVersion: "1.0.0",
		},
		{
			name:        "version matches constraints",
			source:      shorthandSource,
			constraints: ptr.String(">= 1.0.1"),
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.1",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
							{
								Version:   "1.0.2",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
						},
					},
				},
			},
			expectVersion: "1.0.2",
		},
		{
			name:   "service discovery returns full URL",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": "https://some-registry.tld/plugins-registry/v1/",
					},
				},
				"https://some-registry.tld/plugins-registry/v1/neutron/gitlab/versions": {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.0",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
						},
					},
				},
			},
			expectVersion: "1.0.0",
		},
		{
			name:        "version does not match constraints",
			source:      shorthandSource,
			constraints: ptr.String(">= 1.0.1"),
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.0",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
						},
					},
				},
			},
			expectError: "no suitable versions found",
		},
		{
			name:   "version does not support current platform",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.0",
								Protocols: []string{"1.0"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: "random-os",
										Arch:            "random-arch",
									},
								},
							},
						},
					},
				},
			},
			expectError: "no suitable versions found",
		},
		{
			name:   "version does not support current protocols",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginVersionList{
						Versions: []RegistryPluginVersion{
							{
								Version:   "1.0.0",
								Protocols: []string{"0.1"},
								Platforms: []RegistryPluginPlatform{
									{
										OperatingSystem: runtime.GOOS,
										Arch:            runtime.GOARCH,
									},
								},
							},
						},
					},
				},
			},
			expectError: "no suitable versions found",
		},
		{
			name:   "registry returns no versions",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body:        &RegistryPluginVersionList{},
				},
			},
			expectError: "no suitable versions found",
		},
		{
			name:   "service discovery endpoint returns error",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusNotFound,
				},
			},
			expectError: "failed to discover plugin registry: endpoint returned Not Found status",
		},
		{
			name:   "registry endpoint returns error",
			source: shorthandSource,
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultVersionsEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusNotFound,
				},
			},
			expectError: "failed to get plugin versions: endpoint returned Not Found status",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			// Configure the client to avoid retries and logging.
			retryableClient := retryablehttp.NewClient()
			retryableClient.Logger = nil
			retryableClient.RetryMax = 0
			retryableClient.HTTPClient = newTestClient(func(r *http.Request) *http.Response {
				if r.URL.Path != serviceDiscoveryEndpoint {
					// Require token for all requests not to the service discovery endpoint.
					require.Equal(t, "Bearer "+token, r.Header.Get("Authorization"))
				}

				response, ok := test.responses[r.URL.String()]
				if !ok {
					t.Fatalf("unexpected request to %s", r.URL.String())
				}

				buffer, err := json.Marshal(response.body)
				if err != nil {
					t.Fatalf("failed to marshal response body: %v", err)
				}

				return &http.Response{
					Status:     http.StatusText(response.statusCode),
					StatusCode: response.statusCode,
					Body:       io.NopCloser(bytes.NewBuffer(buffer)),
				}
			})

			i := &installer{
				logger: hclog.NewNullLogger(),
				client: retryableClient,
				defaultRegistryURL: &url.URL{
					Scheme: "http",
					Host:   "localhost",
				},
			}

			actual, err := i.EnsureLatestVersion(ctx, test.source, WithConstraints(test.constraints), WithToken(&token))

			if test.expectError != "" {
				assert.ErrorContains(t, err, test.expectError)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, test.expectVersion, actual)
		})
	}
}

func TestInstallPlugin(t *testing.T) {
	token := "token"
	zipFilename := fmt.Sprintf("phobos-plugin-test_1.0.0_%s_%s.zip", runtime.GOOS, runtime.GOARCH)
	zipShaSum := "864aa0d15a3a72cae3c59708ad51f55984f4a6b4af5e021e17f4dfa88686b4c2"
	defaultDownloadEndpoint := fmt.Sprintf("http://localhost/v1/plugin-registry/plugins/neutron/test/1.0.0/download/%s/%s", runtime.GOOS, runtime.GOARCH)
	defaultRegistryEndpoint := &url.URL{Scheme: "http", Host: "localhost"}

	// Create a temporary directory to avoid writing to the real filesystem.
	testPluginDir, err := os.MkdirTemp("", "installer-test")
	require.NoError(t, err)
	defer os.RemoveAll(testPluginDir)

	// Create a zip file with fake plugin "binary".
	zipData, err := createPluginZip()
	require.NoError(t, err)

	type testCase struct {
		name        string
		responses   map[string]fakeHTTPResponse
		expectError string
	}

	tests := []testCase{
		{
			name: "success",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginDownloadResponse{
						OperatingSystem: runtime.GOOS,
						Arch:            runtime.GOARCH,
						Protocols:       []string{"1.0"},
						Filename:        zipFilename,
						SHASum:          zipShaSum,
						DownloadURL:     "http://object-store.tld/binary",
						SHASumsURL:      "http://object-store.tld/shasums",
					},
				},
				"http://object-store.tld/shasums": {
					contentType: "text/plain",
					statusCode:  http.StatusOK,
					body:        bytes.NewBufferString(zipShaSum + "  " + zipFilename + "\n"),
				},
				"http://object-store.tld/binary": {
					contentType: "application/zip",
					statusCode:  http.StatusOK,
					body:        zipData,
				},
			},
		},
		{
			name: "discovery endpoint returns error",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusNotFound,
				},
			},
			expectError: "failed to discover plugin registry: endpoint returned Not Found status",
		},
		{
			name: "download url endpoint returns error",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusNotFound,
				},
			},
			expectError: "failed to get plugin download information: endpoint returned Not Found status",
		},
		{
			name: "sha sums endpoint returns error",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginDownloadResponse{
						OperatingSystem: runtime.GOOS,
						Arch:            runtime.GOARCH,
						Protocols:       []string{"1.0"},
						Filename:        zipFilename,
						SHASum:          zipShaSum,
						DownloadURL:     "http://object-store.tld/binary",
						SHASumsURL:      "http://object-store.tld/shasums",
					},
				},
				"http://object-store.tld/shasums": {
					contentType: "text/plain",
					statusCode:  http.StatusNotFound,
				},
			},
			expectError: "failed to download plugin checksums: endpoint returned Not Found status",
		},
		{
			name: "binary download endpoint returns error",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginDownloadResponse{
						OperatingSystem: runtime.GOOS,
						Arch:            runtime.GOARCH,
						Protocols:       []string{"1.0"},
						Filename:        zipFilename,
						SHASum:          zipShaSum,
						DownloadURL:     "http://object-store.tld/binary",
						SHASumsURL:      "http://object-store.tld/shasums",
					},
				},
				"http://object-store.tld/shasums": {
					contentType: "text/plain",
					statusCode:  http.StatusOK,
					body:        bytes.NewBufferString(zipShaSum + "  " + zipFilename + "\n"),
				},
				"http://object-store.tld/binary": {
					contentType: "application/zip",
					statusCode:  http.StatusNotFound,
				},
			},
			expectError: "failed to download plugin: endpoint returned Not Found status",
		},
		{
			name: "platform does not support protocols",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginDownloadResponse{
						OperatingSystem: runtime.GOOS,
						Arch:            runtime.GOARCH,
						Protocols:       []string{"0.1"},
						Filename:        zipFilename,
						SHASum:          zipShaSum,
						DownloadURL:     "http://object-store.tld/binary",
						SHASumsURL:      "http://object-store.tld/shasums",
					},
				},
			},
			expectError: "unsupported protocol version",
		},
		{
			name: "checksum does not match",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginDownloadResponse{
						OperatingSystem: runtime.GOOS,
						Arch:            runtime.GOARCH,
						Protocols:       []string{"1.0"},
						Filename:        zipFilename,
						SHASum:          zipShaSum,
						DownloadURL:     "http://object-store.tld/binary",
						SHASumsURL:      "http://object-store.tld/shasums",
					},
				},
				"http://object-store.tld/shasums": {
					contentType: "text/plain",
					statusCode:  http.StatusOK,
					body:        bytes.NewBufferString(zipShaSum + "  " + zipFilename + "\n"),
				},
				"http://object-store.tld/binary": {
					contentType: "application/zip",
					statusCode:  http.StatusOK,
					body:        bytes.NewBufferString("invalid-zip-data"),
				},
			},
			expectError: "checksum mismatch",
		},
		{
			name: "checksum does not exist",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginDownloadResponse{
						OperatingSystem: runtime.GOOS,
						Arch:            runtime.GOARCH,
						Protocols:       []string{"1.0"},
						Filename:        zipFilename,
						SHASum:          zipShaSum,
						DownloadURL:     "http://object-store.tld/binary",
						SHASumsURL:      "http://object-store.tld/shasums",
					},
				},
				"http://object-store.tld/shasums": {
					contentType: "text/plain",
					statusCode:  http.StatusOK,
					body:        bytes.NewBufferString(""),
				},
			},
			expectError: "checksum not found",
		},
		{
			name: "unknown content type when downloading binary",
			responses: map[string]fakeHTTPResponse{
				defaultDiscoveryEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: map[string]any{
						"plugins.v1": pluginsV1Endpoint,
					},
				},
				defaultDownloadEndpoint: {
					contentType: "application/json",
					statusCode:  http.StatusOK,
					body: &RegistryPluginDownloadResponse{
						OperatingSystem: runtime.GOOS,
						Arch:            runtime.GOARCH,
						Protocols:       []string{"1.0"},
						Filename:        zipFilename,
						SHASum:          zipShaSum,
						DownloadURL:     "http://object-store.tld/binary",
						SHASumsURL:      "http://object-store.tld/shasums",
					},
				},
				"http://object-store.tld/shasums": {
					contentType: "text/plain",
					statusCode:  http.StatusOK,
					body:        bytes.NewBufferString(zipShaSum + "  " + zipFilename + "\n"),
				},
				"http://object-store.tld/binary": {
					contentType: "application/unknown",
					statusCode:  http.StatusOK,
					body:        bytes.NewBufferString("invalid-zip-data"),
				},
			},
			expectError: "unexpected content type",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			pluginSource, err := ParseSource("neutron/test", ptr.String(defaultRegistryEndpoint.String()))
			require.NoError(t, err)

			pluginDirectory := filepath.Join(
				testPluginDir,
				".phobos.d",
				"plugins",
				pluginSource.String(),
				"1.0.0",
				runtime.GOOS+"_"+runtime.GOARCH,
			)

			// Clean up the plugin directory after the test.
			defer os.RemoveAll(pluginDirectory)

			binaryPath := filepath.Join(pluginDirectory, "phobos-plugin-test")

			// Configure the client to avoid retries and logging.
			client := retryablehttp.NewClient()
			client.Logger = nil
			client.RetryMax = 0

			// Replace the client with a test client to avoid making real calls.
			client.HTTPClient = newTestClient(func(r *http.Request) *http.Response {
				if r.Header.Get("Authorization") != "" {
					require.Equal(t, "Bearer "+token, r.Header.Get("Authorization"))
				}

				response, ok := test.responses[r.URL.String()]
				if !ok {
					t.Fatalf("unexpected request to %s", r.URL.String())
				}

				// Marshal the response body if it's not already a bytes.Buffer.
				if _, ok := response.body.(*bytes.Buffer); !ok {
					buffer, mErr := json.Marshal(response.body)
					if mErr != nil {
						t.Fatalf("failed to marshal response body: %v", mErr)
					}

					response.body = bytes.NewBuffer(buffer)
				}

				header := make(http.Header)
				header.Set("Content-Type", response.contentType)

				return &http.Response{
					Header:     header,
					Status:     http.StatusText(response.statusCode),
					StatusCode: response.statusCode,
					Body:       io.NopCloser(response.body.(*bytes.Buffer)),
				}
			})

			i := &installer{
				logger:             hclog.NewNullLogger(),
				client:             client,
				defaultRegistryURL: defaultRegistryEndpoint,
				homeDirFunc: func() (string, error) {
					return testPluginDir, nil
				},
			}

			err = i.InstallPlugin(ctx, "neutron/test", "1.0.0", WithToken(&token))

			if test.expectError != "" {
				assert.ErrorContains(t, err, test.expectError)
				return
			}

			require.NoError(t, err)

			// Check that the plugin directory was created.
			require.DirExists(t, pluginDirectory)

			// Check that the binary exists and is an executable.
			info, err := os.Stat(binaryPath)
			require.NoError(t, err)
			assert.Equal(t, os.FileMode(0700), info.Mode())
		})
	}
}

// createPluginZip creates a zip file with fake plugin "binary".
func createPluginZip() (*bytes.Buffer, error) {
	buffer := &bytes.Buffer{}
	zipWriter := zip.NewWriter(buffer)
	defer zipWriter.Close()

	header := &zip.FileHeader{
		Name:   "phobos-plugin-test",
		Method: zip.Deflate,
	}

	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return nil, err
	}

	// Write some fake data into the zip file.
	if _, err = writer.Write([]byte("test-plugin-data")); err != nil {
		return nil, err
	}

	return buffer, nil
}

func TestAreProtocolsSupported(t *testing.T) {
	supportedProtocols := func() (protocols []string) {
		for p := range supportedProtocols {
			protocols = append(protocols, p)
		}
		return
	}()

	type testCase struct {
		name        string
		protocols   []string
		expectError bool
	}

	tests := []testCase{
		{
			name:      "supported protocols",
			protocols: supportedProtocols,
		},
		{
			name:        "unsupported protocols",
			protocols:   []string{"0.1"},
			expectError: true,
		},
		{
			name:        "mixed protocols",
			protocols:   []string{"0.1", "1.0"},
			expectError: true,
		},
		{
			name:        "no protocols",
			expectError: true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := AreProtocolsSupported(test.protocols)

			if test.expectError {
				assert.Error(t, err)
				return
			}

			assert.NoError(t, err)
		})
	}
}
