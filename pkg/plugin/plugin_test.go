package plugin

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	// pluginList is a list of plugins that are returned for the "plugin list" command
	pluginList = map[string]interface{}{
		"test": "A plugin for testing",
	}
)

func TestManagerNew(t *testing.T) {
	logger := hclog.NewNullLogger()

	expected := &manager{
		plugins: map[string]*PipelinePlugin{},
		logger:  logger,
	}

	assert.Equal(t, expected, New(logger))
}

func TestManagerClose(t *testing.T) {
	called := false
	plugin := &PipelinePlugin{
		close: func() {
			called = true
		},
	}

	manager := &manager{
		plugins: map[string]*PipelinePlugin{
			"test": plugin,
		},
	}

	manager.Close()
	assert.True(t, called)
}

func TestManagerGetPipelinePlugins(t *testing.T) {
	plugin := &PipelinePlugin{}
	manager := &manager{
		plugins: map[string]*PipelinePlugin{
			"test": plugin,
		},
	}

	assert.Equal(t, []*PipelinePlugin{plugin}, manager.GetPlugins())
}

func TestManagerGetPipelinePlugin(t *testing.T) {
	plugin := &PipelinePlugin{}
	manager := &manager{
		plugins: map[string]*PipelinePlugin{
			"test": plugin,
		},
	}

	// Valid
	actual, err := manager.GetPlugin("test")
	require.NoError(t, err)
	assert.Equal(t, plugin, actual)

	// Invalid
	actual, err = manager.GetPlugin("invalid")
	require.Error(t, err)
	assert.Nil(t, actual)
}

func TestManagerGetVersionForBuiltInPlugins(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Use a fake execCommandContext and restore it at the end of the test
	execCommandContext = fakeExecCommandContext
	defer func() { execCommandContext = exec.CommandContext }()

	// Create a new manager
	manager := &manager{
		logger:        hclog.NewNullLogger(),
		phobosCLIPath: "some/cli/path",
	}

	_, err := manager.GetVersionForBuiltInPlugins(ctx)
	require.NoError(t, err)
}

func TestManagerLaunchPipelinePlugin(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Use a fake execCommandContext and restore it at the end of the test
	execCommandContext = fakeExecCommandContext
	defer func() { execCommandContext = exec.CommandContext }()

	// Create a new manager
	manager := &manager{
		logger: hclog.NewNullLogger(),
		plugins: map[string]*PipelinePlugin{
			"test": {
				log: hclog.NewNullLogger(),
			},
		},
		phobosCLIPath: "some/cli/path",
	}

	// Close the manager at the end of the test
	defer manager.Close()

	// Launch a valid plugin
	require.NoError(t, manager.LaunchPlugin(ctx, "test"))
	assert.NotNil(t, manager.plugins["test"])

	// Launch an invalid plugin
	require.Error(t, manager.LaunchPlugin(ctx, "invalid"))
}

// fakeExecCommandContext is a helper function to mock exec.CommandContext in tests
func fakeExecCommandContext(ctx context.Context, command string, args ...string) *exec.Cmd {
	cs := []string{"-test.run=TestHelperProcess", "--", command}
	cs = append(cs, args...)
	cmd := exec.CommandContext(ctx, os.Args[0], cs...)
	cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}
	return cmd
}

// TestHelperProcess is used to emulate the Phobos CLI output during exec commands.
func TestHelperProcess(*testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}

	args := os.Args
	for len(args) > 0 {
		if args[0] == "--" {
			args = args[1:]
			break
		}

		args = args[1:]
	}

	if len(args) == 0 {
		fmt.Fprintf(os.Stderr, "No command supplied\n")
		os.Exit(1)
	}

	defer os.Exit(0)

	commandName := strings.Join(args[1:3], " ")
	commandArgs := strings.Join(args[3:], " ") // Args after the command name (plugin name, etc.).

	switch commandName {
	case "plugin list":
		buf, err := json.Marshal(pluginList)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to marshal plugin list: %s\n", err)
			os.Exit(1)
		}
		fmt.Println(string(buf))
	case "plugin launch":
		if commandArgs != "test" {
			fmt.Fprintf(os.Stderr, "Unknown plugin: %s\n", commandArgs)
			os.Exit(1)
		}

		fmt.Printf("%d|1|tcp|:1234|grpc\n", plugin.CoreProtocolVersion)
		<-make(chan int)
	default:
		fmt.Fprintf(os.Stderr, "Unknown command: %s\n", args[0])
		os.Exit(1)
	}
}
