package plugin

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"strings"

	"github.com/hashicorp/go-argmapper"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hcldec"
	"github.com/iancoleman/strcase"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/gocty"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/protomappers"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// PipelinePlugin wraps a provider plugin instance and encapsulates functionality for
// interfacing with a provider
type PipelinePlugin struct {
	instance component.PipelinePlugin
	log      hclog.Logger
	close    func()
}

// NewPipelinePlugin creates a new instance of a PipelinePlugin
func NewPipelinePlugin(instance component.PipelinePlugin, log hclog.Logger, close func()) *PipelinePlugin {
	return &PipelinePlugin{
		instance: instance,
		log:      log,
		close:    close,
	}
}

// Close releases plugin resources
func (p *PipelinePlugin) Close() {
	p.close()
}

// GracefulShutdown gracefully stops the provider actions
func (p *PipelinePlugin) GracefulShutdown(ctx context.Context) error {
	return p.instance.Shutdown(ctx)
}

// Configure configures a provider instance with an hcl config
func (p *PipelinePlugin) Configure(ctx context.Context, body hcl.Body, evalCtx *hcl.EvalContext) hcl.Diagnostics {
	v, err := p.instance.Config(ctx)
	if err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	// If the configuration structure is nil then we behave as if the
	// component is not configurable.
	if v == nil {
		return nil
	}

	// Decode
	if diag := gohcl.DecodeBody(body, evalCtx, v); len(diag) > 0 {
		return diag
	}

	if err := p.instance.ConfigSet(ctx, v); err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	return nil
}

// ExecuteAction executes a provider action and returns the outputs
func (p *PipelinePlugin) ExecuteAction(ctx context.Context, actionName string, body hcl.Body, evalCtx *hcl.EvalContext, ui terminal.UI) (map[string]cty.Value, error) {
	f := p.instance.ExecuteAction(ctx, &component.PipelinePluginActionInput{ActionName: actionName, InputData: body}, evalCtx)

	outputs, err := p.invokeActionFunc(ctx, f, ui)
	if err != nil {
		if code := status.Code(err); code == codes.Canceled || code == codes.DeadlineExceeded {
			// Ignore context cancellation errors
			return map[string]cty.Value{}, nil
		}

		return nil, err
	}

	return outputs, nil
}

// ValidateConfig validates a provider hcl config
func (p *PipelinePlugin) ValidateConfig(ctx context.Context, body hcl.Body, evalCtx *hcl.EvalContext) hcl.Diagnostics {
	c, err := p.instance.Config(ctx)
	if err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	spec, partial, err := buildSpec(c)
	if err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	if partial {
		_, _, diag := hcldec.PartialDecode(body, spec, evalCtx)
		return diag
	}

	_, diag := hcldec.Decode(body, spec, evalCtx)
	return diag
}

// GetImpliedActionOutputs returns the implied outputs for an action without executing it. This is used primarily
// for validation
func (p *PipelinePlugin) GetImpliedActionOutputs(ctx context.Context, actionName string) (map[string]cty.Value, error) {
	outputStruct, err := p.instance.ActionOutput(ctx, actionName)
	if err != nil {
		return nil, err
	}

	outputs := map[string]cty.Value{}

	e := reflect.ValueOf(outputStruct).Elem()
	for i := 0; i < e.NumField(); i++ {
		f := e.Field(i)
		ty, err := gocty.ImpliedType(f.Interface())
		if err != nil {
			return nil, err
		}
		outputName := strcase.ToSnake(e.Type().Field(i).Name)
		outputs[outputName] = cty.UnknownVal(ty)
	}

	return outputs, nil
}

// ValidateActionInput validates the hcl config against the action input struct
func (p *PipelinePlugin) ValidateActionInput(ctx context.Context, actionName string, body hcl.Body, evalCtx *hcl.EvalContext) hcl.Diagnostics {
	inputStruct, err := p.instance.ActionInput(ctx, actionName)
	if err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	if inputStruct == nil {
		_, diag := body.Content(&hcl.BodySchema{})
		return diag
	}

	spec, partial, err := buildSpec(inputStruct)
	if err != nil {
		return hcl.Diagnostics{
			&hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  err.Error(),
				Detail:   "",
			},
		}
	}

	if partial {
		_, _, diag := hcldec.PartialDecode(body, spec, evalCtx)
		return diag
	}

	_, diag := hcldec.Decode(body, spec, evalCtx)
	return diag
}

func (p *PipelinePlugin) invokeActionFunc(ctx context.Context, f interface{}, ui terminal.UI) (map[string]cty.Value, error) {
	rawFunc, ok := f.(*argmapper.Func)
	if !ok {
		var err error
		rawFunc, err = argmapper.NewFunc(f, argmapper.Logger(p.log))
		if err != nil {
			return nil, err
		}
	}

	mappers, err := argmapper.NewFuncList(protomappers.All,
		argmapper.Logger(p.log),
	)
	if err != nil {
		return nil, err
	}

	// Make sure we have access to our context and logger and default args
	args := []argmapper.Arg{
		argmapper.ConverterFunc(mappers...),
		argmapper.Typed(
			ctx,
			p.log,
			ui,
		),
	}

	// Build the chain and call it
	callResult := rawFunc.Call(args...)
	if err := callResult.Err(); err != nil {
		if e, ok := status.FromError(err); ok {
			return nil, errors.New(e.Message())
		}
		return nil, err
	}
	raw := callResult.Out(0)

	// Verify
	interfaceType := reflect.TypeOf((*component.PipelinePluginActionResponse)(nil)).Elem()
	if rawType := reflect.TypeOf(raw); !rawType.Implements(interfaceType) {
		return nil, fmt.Errorf("failed to cast response type")
	}

	buildResp := raw.(component.PipelinePluginActionResponse)

	return buildResp.Outputs(), nil
}

func buildSpec(v interface{}) (hcldec.Spec, bool, error) {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr {
		return nil, false, fmt.Errorf("target value must be a pointer, not %s", rv.Type().String())
	}

	return buildSpecFromValue(rv.Elem())
}

func buildSpecFromValue(val reflect.Value) (hcldec.Spec, bool, error) {
	et := val.Type()

	if et.Kind() != reflect.Struct {
		return nil, false, fmt.Errorf("a struct is expected, not %s", et.String())
	}

	spec := hcldec.ObjectSpec{}

	tags := getFieldTags(et)

	// handle attribute tags
	for name, fieldIdx := range tags.Attributes {
		fieldV := val.Field(fieldIdx)

		typ, err := gocty.ImpliedType(fieldV.Interface())
		if err != nil {
			return nil, false, err
		}

		optional := tags.Optional[name]
		spec[name] = &hcldec.AttrSpec{Name: name, Type: typ, Required: !optional}
	}

	// handle labels
	for _, label := range tags.Labels {
		spec[label.Name] = &hcldec.BlockLabelSpec{Name: label.Name, Index: label.FieldIndex}
	}

	// handle block tags
	for name, fieldIdx := range tags.Blocks {
		fieldV := val.Field(fieldIdx)

		required := true
		// Pointer structs are optional blocks
		if fieldV.Type().Kind() == reflect.Ptr {
			// If this is a pointer we need to create a new instance of the type that it points to
			fieldV = reflect.New(fieldV.Type().Elem()).Elem()
			required = false
		}

		// Check if this is a list of blocks
		if fieldV.Kind() == reflect.Slice {
			nestedV := reflect.New(fieldV.Type().Elem()).Elem()

			if nestedV.Type().Kind() == reflect.Ptr {
				// If this is a pointer we need to create a new instance of the type that it points to
				nestedV = reflect.New(nestedV.Type().Elem()).Elem()
			}

			nested, _, err := buildSpecFromValue(nestedV)
			if err != nil {
				return nil, false, err
			}
			spec[name] = &hcldec.BlockListSpec{TypeName: name, Nested: nested}
		} else {
			nested, _, err := buildSpecFromValue(fieldV)
			if err != nil {
				return nil, false, err
			}

			spec[name] = &hcldec.BlockSpec{TypeName: name, Nested: nested, Required: required}
		}
	}

	return spec, tags.Remain != nil, nil
}

type fieldTags struct {
	Attributes map[string]int
	Blocks     map[string]int
	Remain     *int
	Body       *int
	Optional   map[string]bool
	Labels     []*labelField
}

type labelField struct {
	Name       string
	FieldIndex int
}

// This function is copied from https://github.com/hashicorp/hcl/blob/7208bce57fadb72db3a328ebc9aa86489cd06fce/gohcl/schema.go#L128
// Copied code is licensed under Mozilla Public License, version 2.0
func getFieldTags(ty reflect.Type) *fieldTags {
	ret := &fieldTags{
		Attributes: map[string]int{},
		Blocks:     map[string]int{},
		Optional:   map[string]bool{},
	}

	ct := ty.NumField()
	for i := 0; i < ct; i++ {
		field := ty.Field(i)
		tag := field.Tag.Get("hcl")
		if tag == "" {
			continue
		}

		comma := strings.Index(tag, ",")
		var name, kind string
		if comma != -1 {
			name = tag[:comma]
			kind = tag[comma+1:]
		} else {
			name = tag
			kind = "attr"
		}

		switch kind {
		case "attr":
			ret.Attributes[name] = i
		case "block":
			ret.Blocks[name] = i
		case "label":
			ret.Labels = append(ret.Labels, &labelField{
				FieldIndex: i,
				Name:       name,
			})
		case "remain":
			if ret.Remain != nil {
				panic("only one 'remain' tag is permitted")
			}
			idx := i // copy, because this loop will continue assigning to i
			ret.Remain = &idx
		case "body":
			if ret.Body != nil {
				panic("only one 'body' tag is permitted")
			}
			idx := i // copy, because this loop will continue assigning to i
			ret.Body = &idx
		case "optional":
			ret.Attributes[name] = i
			ret.Optional[name] = true
		default:
			panic(fmt.Sprintf("invalid hcl field tag kind %q on %s %q", kind, field.Type.String(), field.Name))
		}
	}

	return ret
}
