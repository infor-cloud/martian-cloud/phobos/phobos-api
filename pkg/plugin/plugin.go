// Package plugin is used to manage the execution of Phobos plugins
package plugin

//go:generate go tool mockery --name Manager --inpackage --case underscore

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	hcInstall "github.com/hashicorp/hc-install"
	"github.com/hashicorp/hc-install/fs"
	"github.com/hashicorp/hc-install/product"
	"github.com/hashicorp/hc-install/src"
	"github.com/mitchellh/go-homedir"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/component"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/pluginclient"
)

// execCommandContext is a variable so it can be mocked out in tests
// See: https://npf.io/2015/06/testing-exec-command/
var execCommandContext = exec.CommandContext

type launchOptions struct {
	version    *string
	dir        *string
	binaryPath *string
}

// LaunchOption is a function that modifies the launch options
type LaunchOption func(*launchOptions)

// WithVersion sets the plugin version. Required when
// using a plugin from a remote registry.
func WithVersion(version string) LaunchOption {
	return func(o *launchOptions) {
		o.version = &version
	}
}

// WithDir will become the working directory of the
// plugin's command.
func WithDir(dir string) LaunchOption {
	return func(o *launchOptions) {
		o.dir = &dir
	}
}

// WithBinaryPath indicates the path for a local
// plugin binary which will be used to launch
// the plugin instead.
func WithBinaryPath(path string) LaunchOption {
	return func(o *launchOptions) {
		o.binaryPath = &path
	}
}

// Manager manages plugin execution and lifecycle
type Manager interface {
	Close()
	GetPlugins() []*PipelinePlugin
	GetPlugin(name string) (*PipelinePlugin, error)
	GetVersionForBuiltInPlugins(ctx context.Context) (string, error)
	LaunchPlugin(ctx context.Context, source string, options ...LaunchOption) error
}

// manager is the default implementation of the plugin manager
type manager struct {
	logger        hclog.Logger
	plugins       map[string]*PipelinePlugin
	phobosCLIPath string
}

// New creates a new plugin manager
func New(logger hclog.Logger) Manager {
	return &manager{
		logger:  logger,
		plugins: map[string]*PipelinePlugin{},
	}
}

// Close releases plugin manager resources
func (p *manager) Close() {
	for _, plugin := range p.plugins {
		plugin.Close()
	}
}

// GetPlugins returns a list of plugins
func (p *manager) GetPlugins() []*PipelinePlugin {
	response := []*PipelinePlugin{}
	for _, plugin := range p.plugins {
		response = append(response, plugin)
	}
	return response
}

// GetPlugin returns a plugin
func (p *manager) GetPlugin(name string) (*PipelinePlugin, error) {
	instance, ok := p.plugins[name]
	if !ok {
		return nil, fmt.Errorf("plugin with name %s is not available", name)
	}
	return instance, nil
}

// GetVersionForBuiltInPlugins returns the built in plugin version this plugin manager uses.
func (p *manager) GetVersionForBuiltInPlugins(ctx context.Context) (string, error) {
	// Ensure the CLI binary is located first.
	if err := p.ensurePhobosCLIBinary(ctx); err != nil {
		return "", fmt.Errorf("failed to ensure Phobos CLI binary: %w", err)
	}

	out, err := execCommandContext(ctx, p.phobosCLIPath, "-version").Output() // nosemgrep: gosec.G204-1
	if err != nil {
		return "", fmt.Errorf("failed to get phobos CLI version: %w", err)
	}

	return string(out), nil
}

// LaunchPlugin launches a new plugin
func (p *manager) LaunchPlugin(ctx context.Context, source string, options ...LaunchOption) error {
	opts := &launchOptions{}
	for _, o := range options {
		o(opts)
	}

	// discover the plugin in the default locations
	cmd, err := p.discoverPlugin(ctx, source, opts)
	if err != nil {
		return fmt.Errorf("failed to discover plugin: %w", err)
	}

	if opts.dir != nil {
		// Set the passed in directory as the current working directory.
		cmd.Dir = filepath.Clean(*opts.dir)
	}

	typ := component.PipelineType

	// We have to copy the command because go-plugin will set some
	// fields on it.
	cmdCopy := *cmd

	config := pluginclient.ClientConfig(p.logger)
	config.Cmd = &cmdCopy
	config.Logger = p.logger

	// Stderr contains the logs
	config.Stderr = io.Discard
	// SynStderr is the stderr for the plugin process
	config.SyncStderr = io.Discard
	// SynStdout is the stdout for the plugin process
	config.SyncStdout = io.Discard

	// Log that we're going to launch this
	p.logger.Info("launching plugin", "type", typ, "path", cmd.Path, "args", cmd.Args)

	// Connect to the plugin
	client := plugin.NewClient(config)
	rpcClient, err := client.Client()
	if err != nil {
		p.logger.Error("error creating plugin client", "err", err)
		client.Kill()
		return err
	}

	var raw interface{}
	raw, err = rpcClient.Dispense(strings.ToLower(typ.String()))
	if err != nil {
		p.logger.Error("error requesting plugin", "type", typ, "id", strings.ToLower(typ.String()), "err", err)
		client.Kill()
		return err
	}

	p.logger.Debug("plugin successfully launched and connected")

	plugin := raw.(component.PipelinePlugin)

	// Use the plugin name as the key
	pluginName := filepath.Base(source)

	p.plugins[pluginName] = NewPipelinePlugin(
		plugin,
		p.logger,
		func() { client.Kill() },
	)

	return nil
}

// discoverPlugin discovers a plugin in the default locations
// and returns a command to launch it or an error if the plugin
// is not found.
func (p *manager) discoverPlugin(ctx context.Context, source string, options *launchOptions) (*exec.Cmd, error) {
	// Check if we have a binary path as we can short-circuit if that's the case.
	if options.binaryPath != nil {
		pluginPath, err := filepath.Abs(*options.binaryPath)
		if err != nil {
			return nil, fmt.Errorf("failed to parse plugin binary path: %v", err)
		}

		// For good measure, see if there's a file there.
		if _, err := os.Stat(pluginPath); err != nil {
			if os.IsNotExist(err) {
				return nil, fmt.Errorf("no plugin binary found at location %q: %v", pluginPath, err)
			}

			return nil, fmt.Errorf("failed to stat binary path %s: %w", pluginPath, err)
		}

		cmd := exec.CommandContext(ctx, pluginPath) // nosemgrep: gosec.G204-1
		return cmd, nil
	}

	// No binary path available, so let's see if this plugin is built-in.

	if err := p.ensurePhobosCLIBinary(ctx); err != nil {
		return nil, fmt.Errorf("failed to ensure Phobos CLI binary: %w", err)
	}

	// Check if plugin is built-in.
	out, err := execCommandContext(ctx, p.phobosCLIPath, "plugin", "list", "--built-in", "--json").Output() // nosemgrep: gosec.G204-1
	if err != nil {
		return nil, fmt.Errorf("failed to list plugins using Phobos CLI: %w", err)
	}

	var plugins map[string]any
	if err = json.Unmarshal(out, &plugins); err != nil {
		return nil, fmt.Errorf("failed to unmarshal list of plugins: %w", err)
	}

	if _, ok := plugins[source]; ok {
		// Found it. Plugin will be launched by the Phobos CLI. Must be in PATH.
		cmd := execCommandContext(ctx, p.phobosCLIPath, "plugin", "launch", source) // nosemgrep: gosec.G204-1
		return cmd, nil
	}

	// Don't have binary path, and nor is it built-in.
	// Let's check if it is available in the default locations.

	if options.version == nil {
		return nil, fmt.Errorf("plugin %q is not built-in so source and plugin version must be provided", source)
	}

	plugin, err := ParseSource(source, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to parse plugin: %w", err)
	}

	homeDir, err := homedir.Dir()
	if err != nil {
		return nil, fmt.Errorf("failed to get user's home directory: %w", err)
	}

	pn := fmt.Sprintf("phobos-plugin-%s", plugin.Name)

	if runtime.GOOS == "windows" {
		// add .exe extension for windows
		pn += ".exe"
	}

	defaultPaths := []string{
		// ~/.phobos.d/plugins/<registry-hostname>/<source>/<version>/<os>_<arch>/<plugin-name>
		filepath.Join(
			homeDir,
			".phobos.d/plugins",
			plugin.String(),
			*options.version,
			runtime.GOOS+"_"+runtime.GOARCH,
			pn,
		),
	}

	// discover the plugin in the default locations
	for _, path := range defaultPaths {
		if _, err = os.Stat(path); err != nil {
			if os.IsNotExist(err) {
				continue
			}

			return nil, fmt.Errorf("failed to stat file path %s: %w", path, err)
		}

		cmd := exec.CommandContext(ctx, path) // nosemgrep: gosec.G204-1
		return cmd, nil
	}

	return nil, fmt.Errorf("plugin %s not found in home directory %s", plugin.Name, homeDir)
}

// ensurePhobosCLIBinary ensures the Phobos CLI binary is available.
func (p *manager) ensurePhobosCLIBinary(ctx context.Context) error {
	if p.phobosCLIPath != "" {
		// Already ensured.
		return nil
	}

	path, err := hcInstall.NewInstaller().Ensure(ctx, []src.Source{&fs.AnyVersion{
		Product: &product.Product{
			Name: "phobos",
			BinaryName: func() string {
				if runtime.GOOS == "windows" {
					return "phobos.exe"
				}
				return "phobos"
			},
		},
	}})
	if err != nil {
		return err
	}

	p.phobosCLIPath = path

	return nil
}
