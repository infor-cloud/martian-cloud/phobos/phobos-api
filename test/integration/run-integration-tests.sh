#!/bin/bash

# run-integration-tests.sh

# This script runs the DB layer integration tests on behalf of the (top-level) Makefile.

# This is set based on the path Go puts in the binary for the variables:
ldflagVarPrefix='gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/db'

: ${PHOBOS_DB_TEST_HOST:="localhost"}
: ${PHOBOS_DB_TEST_PORT:="29432"}
: ${PHOBOS_DB_TEST_NAME:="phobosdbtest"}
: ${PHOBOS_DB_TEST_SSL_MODE:="disable"}
: ${PHOBOS_DB_TEST_USERNAME:="postgres"}
: ${PHOBOS_DB_TEST_PASSWORD:="postgres"}

PHOBOS_DB_TEST_CONTAINER_PORT=5432
PHOBOS_DB_TEST_INSTANCE_NAME=postgres-integration-test-server

docker kill ${PHOBOS_DB_TEST_INSTANCE_NAME} &> /dev/null || true
docker run -d --rm --name ${PHOBOS_DB_TEST_INSTANCE_NAME}        \
	-e POSTGRES_DB=${PHOBOS_DB_TEST_NAME}                        \
	-e POSTGRES_USER=${PHOBOS_DB_TEST_USERNAME}                  \
	-e POSTGRES_PASSWORD=${PHOBOS_DB_TEST_PASSWORD}              \
	-p ${PHOBOS_DB_TEST_PORT}:${PHOBOS_DB_TEST_CONTAINER_PORT}   \
	postgres

LIMIT=40
SLEEP=1
READY=
for ((i=1;i<=LIMIT;i++)); do
	if docker exec ${PHOBOS_DB_TEST_INSTANCE_NAME} pg_isready -U ${PHOBOS_DB_TEST_USERNAME} -d ${PHOBOS_DB_TEST_NAME} &> /dev/null; then
		READY=1
		break
	fi
	sleep ${SLEEP}
done

if [ -z "${READY}" ]; then
	echo "Docker container did not start in time."
	docker logs ${PHOBOS_DB_TEST_INSTANCE_NAME}
	docker kill ${PHOBOS_DB_TEST_INSTANCE_NAME}
	exit 1
fi

go test -count=1 -tags=integration --ldflags "-X ${ldflagVarPrefix}.TestDBHost=${PHOBOS_DB_TEST_HOST} -X ${ldflagVarPrefix}.TestDBPort=${PHOBOS_DB_TEST_PORT} -X ${ldflagVarPrefix}.TestDBName=${PHOBOS_DB_TEST_NAME} -X ${ldflagVarPrefix}.TestDBMode=${PHOBOS_DB_TEST_SSL_MODE} -X ${ldflagVarPrefix}.TestDBUser=${PHOBOS_DB_TEST_USERNAME} -X ${ldflagVarPrefix}.TestDBPass=${PHOBOS_DB_TEST_PASSWORD}" ./...
