ARG goversion=1.24

FROM golang:${goversion}-alpine AS builder
RUN apk add --no-cache build-base curl git
COPY go.mod /app/
WORKDIR /app
RUN go mod download
COPY . /app
RUN curl --fail --silent --show-error -L --output iamoidccredhelper https://gitlab.com/api/v4/projects/44551702/packages/generic/iam-oidc-credential-helper/v0.1.1/iamoidccredhelper_v0.1.1_linux_amd64 \
    && curl --fail --silent --show-error -L --output phobos https://gitlab.com/api/v4/projects/59088135/packages/generic/phobos-cli/v0.6.3/phobos_v0.6.3_linux_amd64 \
    && make build-api \
    && make build-job-executor \
    && make build-agent

FROM gcr.io/distroless/static-debian12:nonroot AS distroless-base
WORKDIR /app/

FROM distroless-base AS api
COPY --from=builder /app/apiserver .
USER nonroot
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "curl", "-f", "http://localhost:9000/health", "||", "exit", "1" ]
EXPOSE 9000 9010 9090
CMD ["./apiserver"]

FROM alpine:3.21 AS agent
RUN apk --no-cache add git curl python3 py3-pip jq && \
    apk --no-cache upgrade && \
    adduser phobos -D && \
    addgroup docker && \
    adduser phobos docker && \
    mkdir -p /app /opt/credhelpers && \
    chown phobos:phobos /app
WORKDIR /app/
COPY --from=builder /app/agent .
COPY --chmod=0755 --from=builder /app/iamoidccredhelper /opt/credhelpers/iamoidccredhelper
COPY --chmod=0755 --from=builder /app/phobos /usr/local/bin/phobos
USER phobos
HEALTHCHECK NONE
CMD ["./agent"]

FROM alpine:3.21 AS job-executor
RUN apk --no-cache add git curl python3 py3-pip jq && \
    apk --no-cache upgrade && \
    adduser phobos -D && \
    mkdir -p /app /phobos-data && \
    chown phobos:phobos /app /phobos-data
WORKDIR /app/
COPY --chmod=0755 --from=builder /app/job .
COPY --chmod=0755 --from=builder /app/phobos /usr/local/bin/phobos
COPY --chmod=0755 scripts/custom_container_entrypoint.sh scripts/prepare_for_custom_container.sh ./
USER phobos
HEALTHCHECK NONE
CMD ["./job"]
