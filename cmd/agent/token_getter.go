package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"os/exec"
	"path/filepath"
	"sync"
	"time"

	phobos "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
)

type tokenResponse struct {
	Token string `json:"token"`
}

// TokenGetterInput is the input to create a tokenGetter.
type TokenGetterInput struct {
	BaseURL              *url.URL
	CredentialHelperPath *string
	InitialOIDCToken     *string
	ServiceAccountID     string
	CredentialHelperArgs []string
	TLSSkipVerify        bool
}

// tokenGetter implements TokenGetter
type tokenGetter struct {
	sync.RWMutex         // protects the token and rewewAt fields
	baseURL              *url.URL
	credentialHelperPath *string
	initialOIDCToken     *string
	renewAt              *time.Time
	grpcClient           *phobos.Client
	serviceAccountID     string
	token                string
	credentialHelperArgs []string
	tlsSkipVerify        bool
}

// NewTokenGetter returns a new TokenGetter.
func NewTokenGetter(ctx context.Context, input *TokenGetterInput) (phobos.TokenGetter, error) {
	result := tokenGetter{
		baseURL:              input.BaseURL,
		credentialHelperPath: input.CredentialHelperPath,
		credentialHelperArgs: input.CredentialHelperArgs,
		initialOIDCToken:     input.InitialOIDCToken,
		serviceAccountID:     input.ServiceAccountID,
		tlsSkipVerify:        input.TLSSkipVerify,
	}

	// Fail now if it's going to fail.
	_, err := result.Token(ctx)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (g *tokenGetter) Token(ctx context.Context) (string, error) {

	// If there's enough time before expiration, just return the current token.
	g.RLock()
	if (g.renewAt != nil) && time.Now().Before(*g.renewAt) {
		toReturn := g.token
		g.RUnlock()
		return toReturn, nil
	}
	g.RUnlock()

	// Upgrade the lock to a write lock to protect the rest of this method.
	g.Lock()
	defer g.Unlock()

	// Check again in case the token was renewed by someone else while we waited for the lock.
	if (g.renewAt != nil) && time.Now().Before(*g.renewAt) {
		return g.token, nil
	}

	// If an initial OIDC token was not supplied, use the credentials helper to get one.
	var initialToken string
	if g.initialOIDCToken == nil {

		// To avoid panic, make sure the credential helper path is not nil.
		if g.credentialHelperPath == nil {
			return "", fmt.Errorf("must set either initial OIDC token or credentials helper path")
		}

		// Use the credentials helper to invoke the GRPC endpoint to create an initial token.
		// It uses low-level interaction, because the Phobos client has not yet been created.
		token, err := g.invokeCredentialHelper()
		if err != nil {
			return "", fmt.Errorf("failed to invoke credential helper: %v", err)
		}

		initialToken = token
	} else {
		initialToken = *g.initialOIDCToken
	}

	// Now, use the initial token to create a real service account token.
	serviceAccountToken, renewAt, gErr := g.getServiceAccountToken(ctx, initialToken)
	if gErr != nil {
		return "", fmt.Errorf("failed to get service account token: %v", gErr)
	}

	// Cache the results for later use and release the write lock upon return.
	g.token = serviceAccountToken
	g.renewAt = renewAt
	return serviceAccountToken, nil
}

func (g *tokenGetter) invokeCredentialHelper() (string, error) {
	cleanedPath := filepath.Clean(*g.credentialHelperPath)
	cmd := exec.Command(cleanedPath, g.credentialHelperArgs...) // nosemgrep: gosec.G204-1

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return "", err
	}

	err = cmd.Start()
	if err != nil {
		return "", err
	}

	output, err := io.ReadAll(stdout)
	if err != nil {
		return "", err
	}

	errOutput, err := io.ReadAll(stderr)
	if err != nil {
		return "", err
	}

	if err = cmd.Wait(); err != nil {
		return "", fmt.Errorf("credential helper returned an error %s", string(errOutput))
	}

	var tokenResp tokenResponse
	err = json.Unmarshal(output, &tokenResp)
	if err != nil {
		return "", err
	}

	return tokenResp.Token, nil
}

// getServiceAccountToken uses the Phobos API's GRPC to create a service account token.
func (g *tokenGetter) getServiceAccountToken(ctx context.Context, initialToken string) (string, *time.Time, error) {
	now := time.Now()

	// Create the GRPC client only once.
	if g.grpcClient == nil {
		newClient, err := phobos.New(ctx, &phobos.Config{
			HTTPEndpoint:  g.baseURL.String(),
			TLSSkipVerify: g.tlsSkipVerify,
		})
		if err != nil {
			return "", nil, err
		}

		g.grpcClient = newClient
	}

	tokenResponse, err := g.grpcClient.ServiceAccountsClient.CreateToken(ctx, &pb.CreateTokenRequest{
		ServiceAccountId: g.serviceAccountID,
		Token:            []byte(initialToken),
	})
	if err != nil {
		return "", nil, err
	}

	// Eligible to renew one minute before expiration.
	expires := now.Add(time.Duration(tokenResponse.ExpiresIn) - time.Minute)
	return string(tokenResponse.Token), &expires, nil
}
