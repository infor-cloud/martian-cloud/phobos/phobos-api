// Package main contains the top-level code for the Phobos org agent.
package main

import (
	"context"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// Version is passed in via ldflags at build time
var Version = "1.0.0"

func main() {
	// create root logger tagged with server version
	logger := logger.New().With("version", Version)

	logger.Infof("Starting Agent with version %s...", Version)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	apiURL := os.Getenv("PHOBOS_API_URL")
	if apiURL == "" {
		logger.Errorf("PHOBOS_API_URL environment variable is required")
		return
	}

	agentID := os.Getenv("PHOBOS_AGENT_ID")
	if agentID == "" {
		logger.Errorf("PHOBOS_AGENT_ID environment variable is required")
		return
	}

	serviceAccountID := os.Getenv("PHOBOS_SERVICE_ACCOUNT_ID")
	if serviceAccountID == "" {
		logger.Errorf("PHOBOS_SERVICE_ACCOUNT_ID environment variable is required")
		return
	}

	var serviceAccountToken *string
	if t := os.Getenv("PHOBOS_SERVICE_ACCOUNT_TOKEN"); t != "" {
		serviceAccountToken = &t
	}

	dispatcherType := os.Getenv("PHOBOS_JOB_DISPATCHER_TYPE")
	if dispatcherType == "" {
		logger.Errorf("PHOBOS_DISPATCHER_TYPE environment variable is required")
		return
	}

	var credHelperPath *string
	if p := os.Getenv("PHOBOS_CREDENTIAL_HELPER_CMD_PATH"); p != "" {
		credHelperPath = &p
	}

	credHelperArgs := os.Getenv("PHOBOS_CREDENTIAL_HELPER_CMD_ARGS")

	// Check that exactly one of the service account token and credential helper path is supplied.
	if (serviceAccountToken == nil) && (credHelperPath == nil) {
		logger.Errorf("Must supply either PHOBOS_SERVICE_ACCOUNT_TOKEN or PHOBOS_CREDENTIAL_HELPER_CMD_PATH environment variable.")
		return
	}
	if (serviceAccountToken != nil) && (credHelperPath != nil) {
		logger.Errorf("Must supply only one of PHOBOS_SERVICE_ACCOUNT_TOKEN and PHOBOS_CREDENTIAL_HELPER_CMD_PATH environment variables.")
		return
	}

	tlsSkipVerify := false // Default to NOT skip TLS verifification.
	tlsSkipVerifyString := os.Getenv("PHOBOS_TLS_SKIP_VERIFY")
	if tlsSkipVerifyString != "" {
		var err error
		tlsSkipVerify, err = strconv.ParseBool(tlsSkipVerifyString)
		if err != nil {
			logger.Errorf("failed to parse value of environment variable PHOBOS_TLS_SKIP_VERIFY: %s", tlsSkipVerifyString)
			return

		}
	}

	pluginData := map[string]string{}
	// Load Job Dispatcher plugin data
	for k, v := range loadDispatcherData("PHOBOS_JOB_DISPATCHER_DATA_") {
		pluginData[k] = v
	}

	client, err := NewClient(ctx, apiURL,
		serviceAccountID, serviceAccountToken, credHelperPath, strings.Split(credHelperArgs, " "), tlsSkipVerify,
	)
	if err != nil {
		logger.Errorf("Failed to create client %v", err)
		return
	}

	agent, err := agent.NewAgent(ctx, agentID, logger, client, &agent.JobDispatcherSettings{
		DispatcherType: dispatcherType,
		PluginData:     pluginData,
	})
	if err != nil {
		logger.Errorf("Failed to create agent %v", err)
		return
	}

	go func() {
		sigint := make(chan os.Signal, 1)

		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)

		// Wait for signal
		<-sigint

		logger.Info("Shutting down agent...")

		// Gracefully shutdown server
		cancel()
	}()

	agent.Start(ctx)

	logger.Info("Agent has gracefully shutdown")
}

func loadDispatcherData(envPrefix string) map[string]string {
	pluginData := make(map[string]string)

	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)

		key := pair[0]
		val := pair[1]

		if strings.HasPrefix(key, envPrefix) {
			pluginDataKey := strings.ToLower(key[len(envPrefix):])
			pluginData[pluginDataKey] = val
		}
	}

	return pluginData
}
