package main

import (
	"context"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/agent"
	phobos "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// claimJobTimeout is the duration to wait per attempt to claim a job.
	claimJobTimeout = 1 * time.Minute
)

// Client uses the Phobos GRPC to claim a job
type Client struct {
	phobosClient *phobos.Client
}

// NewClient creates a new Client instance
func NewClient(ctx context.Context, apiURL string, serviceAccountID string, initialOIDCToken *string,
	credentialHelperPath *string, credentialHelperArgs []string,
	tlsSkipVerify bool,
) (*Client, error) {
	baseURL, err := url.Parse(apiURL)
	if err != nil {
		return nil, fmt.Errorf("invalid api endpoint: %v", err)
	}

	newTokenGetter, err := NewTokenGetter(ctx, &TokenGetterInput{
		BaseURL:              baseURL,
		CredentialHelperPath: credentialHelperPath,
		CredentialHelperArgs: credentialHelperArgs,
		InitialOIDCToken:     initialOIDCToken,
		ServiceAccountID:     serviceAccountID,
		TLSSkipVerify:        tlsSkipVerify,
	})
	if err != nil {
		return nil, err
	}

	client, err := phobos.New(ctx, &phobos.Config{
		TokenGetter:   newTokenGetter,
		HTTPEndpoint:  apiURL,
		TLSSkipVerify: tlsSkipVerify,
	})
	if err != nil {
		return nil, err
	}

	return &Client{phobosClient: client}, nil
}

// CreateSession creates a new agent session.
func (c *Client) CreateSession(ctx context.Context, agentID string) (string, error) {
	agentSession, err := c.phobosClient.AgentsClient.CreateAgentSession(ctx, &pb.CreateAgentSessionRequest{
		AgentId: agentID,
	})
	if err != nil {
		return "", err
	}

	return agentSession.Metadata.Id, nil
}

// SendHeartbeat sends an agent session heartbeat for the specified agent session.
func (c *Client) SendHeartbeat(ctx context.Context, agentSessionID string) error {
	_, err := c.phobosClient.AgentsClient.SendAgentSessionHeartbeat(ctx, &pb.AgentSessionHeartbeatRequest{
		AgentSessionId: agentSessionID,
	})
	return err
}

// SendError sends an agent session error for the specified agent session.
func (c *Client) SendError(ctx context.Context, agentSessionID string, err error) error {
	_, err2 := c.phobosClient.AgentsClient.CreateAgentSessionError(ctx, &pb.CreateAgentSessionErrorRequest{
		AgentSessionId: agentSessionID,
		ErrorMessage:   err.Error(),
	})
	return err2
}

// ClaimJob claims the next available job for the specified agent
func (c *Client) ClaimJob(ctx context.Context, input *agent.ClaimJobInput) (*agent.ClaimJobResponse, error) {
	var resp *pb.ClaimJobResponse
	var err error

	// Loop until we get a response rather than a context deadline expiration.
	for {
		resp, err = c.claimJob(ctx, input.AgentID)

		// Keep trying until we get something other than deadline exceeded.
		if err == nil {
			break
		}

		code := status.Code(err)

		if code != codes.DeadlineExceeded {
			break
		}
	}

	if err != nil {
		return nil, fmt.Errorf("failed to request a job: %w", err)
	}

	return &agent.ClaimJobResponse{
		JobID:         resp.Job.Metadata.Id,
		Token:         resp.Token,
		ImageOverride: resp.Job.Image,
	}, nil
}

// claimJob makes one attempt to claim a job by ID--with timeout.
func (c *Client) claimJob(ctx context.Context, agentID string) (*pb.ClaimJobResponse, error) {
	timeoutContext, cancel := context.WithTimeout(ctx, claimJobTimeout)
	defer cancel()

	return c.phobosClient.JobsClient.ClaimJob(timeoutContext, &pb.ClaimJobRequest{
		AgentId: agentID,
	})
}
