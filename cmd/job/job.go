// Package main package
package main

import (
	"context"
	"flag"
	"os"
	"strconv"

	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/internal/jobexecutor/client"
	"gitlab.com/infor-cloud/martian-cloud/tharsis/tharsis-api/pkg/logger"
)

// Version is passed in via ldflags at build time
var Version = "1.0.0"

func main() {
	flag.Parse()
	// create root logger tagged with server version
	logger := logger.New().With("version", Version)

	logger.Infof("Starting Job Executor with version %s...", Version)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	endpoint := os.Getenv("ENDPOINT")
	jobID := os.Getenv("JOB_ID")
	token := os.Getenv("JOB_TOKEN")
	volumeSizeLimit := os.Getenv("VOLUME_SIZE_LIMIT")

	if endpoint == "" || jobID == "" || token == "" {
		logger.Errorf("ENDPOINT, JOB_ID and JOB_TOKEN environment variables are required")
		return
	}

	var vSizeLimit int
	if volumeSizeLimit != "" {
		vLimit, err := strconv.Atoi(volumeSizeLimit)
		if err != nil {
			logger.Errorf("failed to parse volume size limit %s: %v", volumeSizeLimit, err)
		}
		vSizeLimit = vLimit
	}

	logger.Info("Initializing GRPC client...")

	client, err := client.NewGRPCClient(ctx, logger, &client.GRPCClientOptions{
		HTTPEndpoint: endpoint,
		Token:        token,
	})
	if err != nil {
		logger.Errorf("failed to create client %v", err)
		return
	}

	defer func() {
		if err = client.Close(); err != nil {
			logger.Errorf("error closing client %v", err)
		}
	}()

	// Create job config
	cfg := jobexecutor.JobConfig{
		JobID:           jobID,
		JobToken:        token,
		HTTPEndpoint:    endpoint,
		VolumeSizeLimit: vSizeLimit,
	}

	logger.Info("Starting job executor...")

	// Start the run executor
	executor, err := jobexecutor.NewJobExecutor(ctx, &cfg, client, logger)
	if err != nil {
		logger.Errorf("failed to create job executor for job %s: %v", jobID, err)
		return
	}

	if err := executor.Execute(ctx); err != nil {
		logger.Infof("failed to execute job %s: %v", jobID, err)
		return
	}

	logger.Infof("Completed job %s", jobID)
}
