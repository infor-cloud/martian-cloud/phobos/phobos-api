services:
  # Phobos database.
  postgres:
    image: postgres:15-alpine
    container_name: phobos-db
    restart: unless-stopped
    environment:
      - POSTGRES_DB=phobos
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
      - PGDATA=/var/lib/postgresql/data
    volumes:
      - phobos_postgres_data:/var/lib/postgresql/data
    networks:
      - phobos

  # Keycloak database
  kc_postgres:
    image: postgres:15-alpine
    container_name: keycloak-db
    restart: unless-stopped
    environment:
      - POSTGRES_DB=keycloak
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
      - PGDATA=/var/lib/postgresql/data
    volumes:
      - phobos_kc_postgres_data:/var/lib/postgresql/data
    networks:
      - phobos

  # Identity provider.
  keycloak:
    image: quay.io/keycloak/keycloak:26.0
    container_name: phobos-idp
    restart: unless-stopped
    command: "start-dev --db=postgres --db-url-host=keycloak-db --db-username=postgres --db-password=postgres --import-realm"
    environment:
      - KC_BOOTSTRAP_ADMIN_USERNAME=admin
      - KC_BOOTSTRAP_ADMIN_PASSWORD=admin
    volumes:
      - ./phobos-realm.json:/opt/keycloak/data/import/phobos-realm.json:ro
    networks:
      - phobos
    ports:
      - "6530:8080"
    depends_on:
      - kc_postgres

  # Object storage.
  minio:
    image: minio/minio:latest
    container_name: phobos-store
    restart: unless-stopped
    command: "server /data"
    environment:
      - MINIO_ROOT_USER=minioadmin
      - MINIO_ROOT_PASSWORD=miniopassword
      - MINIO_CONSOLE_ADDRESS=:9003
    volumes:
      - phobos_minio:/data
    networks:
      - phobos
    ports:
      - "9002:9000" # API
      - "127.0.0.1:9003:9003" # UI

  ## minioconsole creates object storage bucket.
  minioconsole:
    image: minio/mc:latest
    container_name: phobos-mc
    entrypoint: /bin/sh -c  " /usr/bin/mc config host add minio http://minio:9002 minioadmin miniopassword;  /usr/bin/mc mb --ignore-existing minio/phobos-objects;  exit 0;  "
    networks:
      - phobos
    depends_on:
      - minio

  jaeger:
    image: jaegertracing/all-in-one:1.44
    container_name: phobos-jaeger
    restart: unless-stopped
    environment:
      - COLLECTOR_OTLP_ENABLED=true
    networks:
      - phobos
    ports:
      - "16686:16686" # Jaeger UI
      - "4317:4317" # OTEL port

  api:
    image: registry.gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/api:latest
    container_name: phobos-api
    restart: unless-stopped
    user: 0:0
    environment:
      - PHOBOS_OAUTH_PROVIDERS_0_ISSUER_URL=http://localhost:6530/realms/phobos
      - PHOBOS_OAUTH_PROVIDERS_0_CLIENT_ID=phobos
      - PHOBOS_OAUTH_PROVIDERS_0_USERNAME_CLAIM=preferred_username
      - PHOBOS_OAUTH_PROVIDERS_0_SCOPE=openid profile email
      - PHOBOS_OAUTH_PROVIDERS_0_LOGOUT_URL=http://localhost:6530/realms/phobos/protocol/openid-connect/logout
      - PHOBOS_ADMIN_USER_EMAIL=martian@phobos.local
      - PHOBOS_DB_USERNAME=postgres
      - PHOBOS_DB_NAME=phobos
      - PHOBOS_DB_PASSWORD=postgres
      - PHOBOS_DB_HOST=postgres
      - PHOBOS_DB_PORT=5432
      - PHOBOS_DB_SSL_MODE=disable
      - PHOBOS_OBJECT_STORE_PLUGIN_TYPE=aws_s3
      - PHOBOS_OBJECT_STORE_PLUGIN_DATA_REGION=us-east-1
      - PHOBOS_OBJECT_STORE_PLUGIN_DATA_BUCKET=phobos-objects
      - PHOBOS_OBJECT_STORE_PLUGIN_DATA_AWS_ACCESS_KEY_ID=minioadmin
      - PHOBOS_OBJECT_STORE_PLUGIN_DATA_AWS_SECRET_ACCESS_KEY=miniopassword
      - PHOBOS_OBJECT_STORE_PLUGIN_DATA_ENDPOINT=http://localhost:9002
      - PHOBOS_JWS_PROVIDER_PLUGIN_TYPE=memory
      - PHOBOS_API_URL=http://localhost:9000
      - PHOBOS_TOKEN_ISSUER_URL=http://localhost:9000
      - PHOBOS_OTEL_TRACE_ENABLED=true
      - PHOBOS_OTEL_TRACE_TYPE=otlp
      - PHOBOS_OTEL_TRACE_HOST=localhost
      - PHOBOS_OTEL_TRACE_PORT=4317
      - PHOBOS_INTERNAL_AGENTS_0_NAME=default-shared-runner
      - PHOBOS_INTERNAL_AGENTS_0_JOB_DISPATCHER_TYPE=docker
      - PHOBOS_INTERNAL_AGENTS_0_JOB_DISPATCHER_DATA_ENDPOINT=http://localhost:9000
      - PHOBOS_INTERNAL_AGENTS_0_JOB_DISPATCHER_DATA_HOST=unix:///var/run/docker.sock
      - PHOBOS_INTERNAL_AGENTS_0_JOB_DISPATCHER_DATA_EXTRA_HOSTS=localhost:host-gateway
      - PHOBOS_INTERNAL_AGENTS_0_JOB_DISPATCHER_DATA_IMAGE=registry.gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/job-executor:latest
      - PHOBOS_INTERNAL_AGENTS_0_JOB_DISPATCHER_DATA_LOCAL_IMAGE=false # Set to true if using local image.
      - PHOBOS_INTERNAL_AGENTS_0_JOB_DISPATCHER_DATA_TLS_DISABLED=true
    networks:
      - phobos
    ports:
      - "9000:9000" # HTTP API.
      - "9010:9010" # GRPC.
    extra_hosts:
      - "localhost:host-gateway"
    depends_on:
      - minio
      - keycloak
      - postgres
      - jaeger

  ui:
    image: registry.gitlab.com/infor-cloud/martian-cloud/phobos/phobos-ui:latest
    container_name: phobos-ui
    restart: unless-stopped
    environment:
      - PHOBOS_API_ENDPOINT=http://localhost:9000
    networks:
      - phobos
    ports:
      - "127.0.0.1:9001:9001"
    depends_on:
      - api

  pgadmin:
    image: dpage/pgadmin4
    container_name: phobos-pgadmin
    restart: unless-stopped
    environment:
      - PGADMIN_DEFAULT_EMAIL=admin@phobos.mc
      - PGADMIN_DEFAULT_PASSWORD=admin
    volumes:
      - ./phobos-db.json:/pgadmin4/servers.json:ro
    networks:
      - phobos
    ports:
      - "5050:80"
    depends_on:
      - postgres

networks:
  phobos:
    driver: bridge

volumes:
  phobos_postgres_data:
  phobos_kc_postgres_data:
  phobos_minio:
