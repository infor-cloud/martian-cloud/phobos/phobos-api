vcs_token "my-token" {
  provider_id = "vcs_provider_id"
}

volume "tools" {
  type = "vcs"
  vcs_options {
    provider_id     = "vcs_provider_id"
    repository_path = "martian/tools"
  }
}

variable "sleep_interval" {
  type    = number
  default = 15
}

variable "account_name" {
  type    = string
  default = "account1"
}

variable approval_required {
  type    = string
  default = true
}

jwt "gitlab" {
  audience = "https://gitlab.com"
}

plugin exec {}

stage_order = ["dev"]

stage dev {
  deployment {
    environment = "account1"
    when        = "manual"
  }
  pre {
    task "check_for_changes" {
      mount_point {
        volume = "tools"
        path   = "/"
      }
      on_error = "continue"
      action "exec_command" {
        alias   = "find_changes"
        command = <<EOF
		  	    echo "Will be deploying to ${var.account_name}"
			      echo "sleeping for ${var.sleep_interval} seconds..."
			      sleep 5
		      EOF
      }
      approval_rules = var.approval_required ? [
        "MFYHA4TPOZQWYLLSOVWGKLJRL5AVE", // approval-rule-1_AR
        "MFYHA4TPOZQWYLLSOVWGKLJSL5AVE"  // approval-rule-2_AR
      ] : []
      when = "auto"
    }
  }
  post {
    task "cleanup_resources" {
      if = true
      action "exec_command" {
        alias   = "perform_cleanup"
        command = <<EOF
          echo "sleeping for ${var.sleep_interval} seconds..."
          echo "output for previous action is ${action_outputs.stage.dev.pre.task.check_for_changes.action.find_changes.exit_code}"
          sleep 5
        EOF
      }
    }
  }
}
