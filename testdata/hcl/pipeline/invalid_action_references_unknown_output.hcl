variable "sleep_interval" {
  type    = number
  default = 15
}

plugin exec {}

stage_order = ["dev"]

stage dev {
  task "execute_a_command" {
    action "exec_command" {
      command = <<EOF
          echo "output for previous action is ${action_outputs.stage.dev.pre.task.check_for_changes.action.find_changes.exit_code}"
        EOF
    }
  }
}
