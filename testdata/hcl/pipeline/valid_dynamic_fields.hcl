variable require_t2_approval {
  default = false
}

variable require_p2_approval {
  default = false
}

variable run_t6_or_p6 {
  default = true
}

plugin exec {}

stage s1 {
  task t0 {}
  task t1 {
    approval_rules = ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"]
    action exec_command {
      command = "echo 'This one uses a list'"
    }
  }
  task t2 {
    approval_rules = var.require_t2_approval ? ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"] : []
    dependencies   = ["t1"]
    action exec_command {
      command = "echo 'This one uses a variable'"
    }
  }
  task t3 {
    approval_rules = action_outputs.stage.s1.task.t2.action.exec_command.exit_code == 0 ? ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"] : []
    dependencies   = ["t2"]
    action exec_command {
      command = "echo 'This one uses an action output'"
    }
  }
  task t4 {
    approval_rules = action_outputs.stage.s1.task.t3.action.exec_command.exit_code == 0 ? ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"] : []
    if             = false
    dependencies   = ["t3"]
    action exec_command {
      command = "echo 'This one uses an action output and an if condition'"
    }
  }
  task t5 {
    if           = action_outputs.stage.s1.task.t4.action.exec_command.exit_code == 1
    dependencies = ["t4"]
    action exec_command {
      command = "echo 'This one uses an action output and an if condition'"
    }
  }
  task t6 {
    if           = var.run_t6_or_p6
    dependencies = ["t5"]
    action exec_command {
      command = "echo 'This one uses a variable'"
    }
  }
  pipeline p0 {
    template_id = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA" // PT_pipeline-template-1
  }
  pipeline p1 {
    approval_rules = ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"]
    template_id    = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA" // PT_pipeline-template-1
  }
  pipeline p2 {
    approval_rules = var.require_p2_approval ? ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"] : []
    template_id    = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA" // PT_pipeline-template-1
  }
  pipeline p3 {
    approval_rules = action_outputs.stage.s1.task.t3.action.exec_command.exit_code == 0 ? ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"] : []
    template_id    = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA" // PT_pipeline-template-1
    dependencies   = ["t3"]
  }
  pipeline p4 {
    approval_rules = action_outputs.stage.s1.task.t4.action.exec_command.exit_code == 0 ? ["GQ3TCMZWMYYDCLJRMRRTGLJUGIYGMLJYGI2DILJYG4ZDINDBGBRDEZRXMNPUCUQ"] : []
    if             = false
    template_id    = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA" // PT_pipeline-template-1
    dependencies   = ["t4"]
  }
  pipeline p5 {
    if           = action_outputs.stage.s1.task.t4.action.exec_command.exit_code == 1
    template_id  = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA" // PT_pipeline-template-1
    dependencies = ["t4"]
  }
  pipeline p6 {
    if           = var.run_t6_or_p6
    template_id  = "prn:pipeline_template:test-org/test-project/NZSXG5DFMQWXA2LQMVWGS3TFFV2GK3LQNRQXIZJNGFPVAVA" // PT_pipeline-template-1
    dependencies = ["t5"]
  }
}
