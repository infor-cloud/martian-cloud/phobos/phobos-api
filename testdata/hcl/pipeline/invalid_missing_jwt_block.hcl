plugin exec {}

stage dev {
  task "execute_a_command" {
    action "exec_command" {
      command = <<EOF
          echo "using an unknown variable ${jwt.unknown_jwt}"
        EOF
    }
  }
}
