variable "deploy_connection_retries" {
  type = number
}

variable "testing_post_endpoint" {
  type = string
}

variable "testing_grpc_enabled" {
  type    = bool
  default = false
}

plugin exec {}

stage dev {
  pipeline "deploy" {
    pipeline_type = "deployment"
    environment   = "account1"
    template_id   = "prn:pipeline_template:test-org/test-project/HA2WGZRVHFSGELJXGE4WELJUMNQTSLLCGRSDGLLBGZRTSYJRHFRGCM3GMVPVAVA"
    when          = "manual"
    variables = {
      deploy_connection_retries = var.deploy_connection_retries
      http_protocol             = "https"
    }
  }
  pipeline "testing" {
    pipeline_type = "deployment"
    environment   = "account2"
    template_id   = "HA2WGZRVHFSGELJXGE4WELJUMNQTSLLCGRSDGLLBGZRTSYJRHFRGCM3GMVPVAVA"
    variables = {
      testing_post_endpoint = var.testing_post_endpoint
      testing_grpc_enabled  = var.testing_grpc_enabled
    }
  }
}
