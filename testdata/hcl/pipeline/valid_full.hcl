vcs_token "my-token" {
  provider_id = "vcs_provider_id"
}

volume "tools" {
  type = "vcs"
  vcs_options {
    provider_id     = "vcs_provider_id"
    repository_path = "martian/tools"
  }
}

variable "sleep_interval" {
  type    = number
  default = 15
}

variable "account_name" {
  type    = string
  default = "account1"
}

variable "auto_deploy" {
  default = false
}

jwt "gitlab" {
  audience = "https://gitlab.com"
}

plugin exec {}

stage_order = ["dev"]

stage dev {
  pre {
    task "check_for_changes" {
      on_error = "continue"
      action "exec_command" {
        alias   = "find_changes"
        command = <<EOF
		  	    echo "Will be deploying to ${var.account_name}"
			      echo "sleeping for ${var.sleep_interval} seconds..."
			      sleep 5
		      EOF
      }
      approval_rules = [
        "GFRDIOBUMVQTALLCMYYTKLJUMFTDCLLCGMZTSLJUMFTGGYTCGBSGMOLFHBPUCUQ",
        "GFRDIOBUMVQTALLCMYYTKLJUMFTDCLLCGMZTSLJUMFTGGYTCGBSGMOLFHBPUCUQ"
      ]
      if   = true
      when = "auto"
    }
  }
  task "execute_a_command" {
    interval = "2m"
    attempts = 5
    mount_point {
      volume = "tools"
      path   = "/"
    }
    action "exec_command" {
      alias   = "perform_action"
      command = <<EOF
		      echo "jwt is ${jwt.gitlab}"
		      echo "output for previous action is ${action_outputs.stage.dev.pre.task.check_for_changes.action.find_changes.exit_code}"
		    EOF
    }
  }
  task "security_scan" {
    action "exec_command" {
      command = <<EOF
          echo "performing a security scan..."
          ls -la
          sleep 20
        EOF
    }
    dependencies = ["execute_a_command"]
  }
  pipeline "deploy" {
    pipeline_type = "deployment"
    environment   = "account1"
    template_id   = "HA2WGZRVHFSGELJXGE4WELJUMNQTSLLCGRSDGLLBGZRTSYJRHFRGCM3GMVPVAVA"
    approval_rules = action_outputs.stage.dev.pre.task.check_for_changes.action.find_changes.exit_code == 0 ? [
      "GFRDIOBUMVQTALLCMYYTKLJUMFTDCLLCGMZTSLJUMFTGGYTCGBSGMOLFHBPUCUQ",
      "GFRDIOBUMVQTALLCMYYTKLJUMFTDCLLCGMZTSLJUMFTGGYTCGBSGMOLFHBPUCUQ"
    ] : []
    if   = action_outputs.stage.dev.pre.task.check_for_changes.action.find_changes.exit_code == 0
    when = var.auto_deploy ? "auto" : "manual"
  }
  post {
    task "cleanup_resources" {
      action "exec_command" {
        alias   = "perform_cleanup"
        command = <<EOF
            echo "sleeping for ${var.sleep_interval} seconds..."
            sleep 5
          EOF
      }
    }
  }
}
