variable "tasks" {}

plugin exec {}

stage dev {
  dynamic "task" {
    for_each = var.tasks
    labels   = [task.value]

    content {
      action "exec_command" {
        command = <<EOF
		      echo "this is a dynamic template"
		    EOF
      }
    }
  }
}
